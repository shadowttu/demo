function onStart() {
    const settings = window._CCSettings;
    cc.view.enableRetina(true);
    cc.view.resizeWithBrowserSize(true);

    if (cc.sys.isBrowser) {
        cc.director.once(cc.Director.EVENT_AFTER_SCENE_LAUNCH, () => {
            let splash = document.getElementById("splash");
            splash && splash.parentNode.removeChild(splash);
        });
    }

    if (cc.sys.isMobile) {
        if (settings.orientation === "landscape") {
            cc.view.setOrientation(cc.macro.ORIENTATION_LANDSCAPE);
        } else if (settings.orientation === "portrait") {
            cc.view.setOrientation(cc.macro.ORIENTATION_PORTRAIT);
        }
        cc.view.enableAutoFullScreen(
            [cc.sys.BROWSER_TYPE_BAIDU, cc.sys.BROWSER_TYPE_BAIDU_APP, cc.sys.BROWSER_TYPE_WECHAT, cc.sys.BROWSER_TYPE_MOBILE_QQ, cc.sys.BROWSER_TYPE_MIUI].indexOf(cc.sys.browserType) < 0
        );
    }

    cc.assetManager.downloader.maxConcurrency = 50;
    cc.assetManager.downloader.maxRequestsPerFrame = 50;
    const identify = UrlManager.getUrlParams("fastMode")
    if (identify == null) {
        const launchScene = settings.launchScene;
        const bundle = cc.assetManager.bundles.find((b) => b.getSceneInfo(launchScene));
        bundle.loadScene(
            launchScene,
            null,
            () => { },
            (err, scene) => {
                if (err) {
                    console.error(`[loadScene] Error = ${err}`);
                    return;
                }

                cc.director.runSceneImmediate(scene);
                if (cc.sys.isBrowser) {
                    var canvas = document.getElementById("GameCanvas");
                    canvas.style.visibility = "";
                    console.log("Success to load scene: " + launchScene);
                }
            }
        );
    } else {
        const gameId = UrlManager.getGameId();
        SceneManager.init();
        SceneManager.changeScene(gameId);
    }
}

window.boot = function () {
    const { debug, groupList, collisionMatrix, bundleVers } = window._CCSettings;
    const { INTERNAL, MAIN } = cc.AssetManager.BuiltinBundleName;
    const bundleRoot = [INTERNAL, MAIN];
    const option = {
        id: "GameCanvas",
        debugMode: debug ? cc.debug.DebugMode.INFO : cc.debug.DebugMode.ERROR,
        showFPS: debug,
        frameRate: 60,
        groupList: groupList,
        collisionMatrix: collisionMatrix,
    };

    cc.assetManager.init({ bundleVers: bundleVers });

    if (CC_DEBUG == false) {
        console.log = () => { };
        console.warn = () => { };
        console.info = () => { };
    }

    let count = 0;
    const cb = (err) => {
        if (err) {
            return console.error(err.message, err.stack);
        }
        count++;
        if (count === bundleRoot.length) {
            cc.game.run(option, onStart);
        }
    };

    for (let i = 0; i < bundleRoot.length; i++) {
        cc.assetManager.loadBundle(bundleRoot[i], cb);
    }
};

window.setBg = function () {
    const gameId = UrlManager.getGameId();
    const blurBg = document.getElementById("blurBg");
    blurBg.style.backgroundImage = `url(htmlBG/${gameId}_HTML.png)`
}


window.onDesignResolutionChanged = function () {
    const pixelRatio = cc.view.getDevicePixelRatio();
    const visibleSize = cc.view.getVisibleSizeInPixel();
    visibleSize.width /= pixelRatio;
    visibleSize.height /= pixelRatio;

    const colorBlock = document.getElementById("colorBlock");
    const canvas = document.getElementById("GameCanvas");

    if (cc.view._orientation === cc.macro.ORIENTATION_LANDSCAPE) {
        if (!cc.view._isRotated) {
            let height = colorBlock.style.height = `${visibleSize.height}px`;
            colorBlock.style.width = `${Math.min(+height.split("px")[0] / 720 * 1680, visibleSize.width)}px`;
            canvas.style.width = colorBlock.style.width;
            canvas.style.height = colorBlock.style.height;
            canvas.width = +canvas.style.width.split("px")[0] * pixelRatio;
        } else {
            let height = colorBlock.style.width = `${visibleSize.height}px`;
            colorBlock.style.height = `${Math.min(+height.split("px")[0] / 720 * 1680, visibleSize.width)}px`;
            canvas.style.width = colorBlock.style.height;
            canvas.style.height = colorBlock.style.width;
            canvas.width = +canvas.style.width.split("px")[0] * pixelRatio;
        }
    } else if (cc.view._orientation === cc.macro.ORIENTATION_PORTRAIT) {
        if (!cc.view._isRotated) {
            let width = colorBlock.style.width = `${visibleSize.width}px`;
            colorBlock.style.height = `${Math.min(+width.split("px")[0] / 720 * 1680, visibleSize.height)}px`;
            canvas.style.width = colorBlock.style.width;
            canvas.style.height = colorBlock.style.height;
            canvas.height = +canvas.style.height.split("px")[0] * pixelRatio;
        } else {
            let width = colorBlock.style.height = `${visibleSize.width}px`;
            colorBlock.style.width = `${Math.min(+width.split("px")[0] / 720 * 1680, visibleSize.height)}px`;
            canvas.style.width = colorBlock.style.height;
            canvas.style.height = colorBlock.style.width;
            canvas.height = +canvas.style.height.split("px")[0] * pixelRatio;
        }
    }
}

window.setLogo = function() {
    const materialId = UrlManager.getMaterialId();
    // 目前1是KK 2是印度風
    if(materialId === "2"){
        document.getElementById("logo").src = "./TEENPATTI_LOGO_01_b.2f0d6.webp";
    }
    else{
        document.getElementById("logo").src = "./KK_LOGO.8ef87.webp";
    }
    document.getElementById("logo").style.visibility = "";
}
