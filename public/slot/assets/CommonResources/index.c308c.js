window.__require = function e(t, n, r) {
  function s(o, u) {
    if (!n[o]) {
      if (!t[o]) {
        var b = o.split("/");
        b = b[b.length - 1];
        if (!t[b]) {
          var a = "function" == typeof __require && __require;
          if (!u && a) return a(b, !0);
          if (i) return i(b, !0);
          throw new Error("Cannot find module '" + o + "'");
        }
        o = b;
      }
      var f = n[o] = {
        exports: {}
      };
      t[o][0].call(f.exports, function(e) {
        var n = t[o][1][e];
        return s(n || e);
      }, f, f.exports, e, t, n, r);
    }
    return n[o].exports;
  }
  var i = "function" == typeof __require && __require;
  for (var o = 0; o < r.length; o++) s(r[o]);
  return s;
}({
  CocosDefaultAssets: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "084cat4fn5FEbYo5LUbPQiN", "CocosDefaultAssets");
    "use strict";
    var __extends = this && this.__extends || function() {
      var extendStatics = function(d, b) {
        extendStatics = Object.setPrototypeOf || {
          __proto__: []
        } instanceof Array && function(d, b) {
          d.__proto__ = b;
        } || function(d, b) {
          for (var p in b) Object.prototype.hasOwnProperty.call(b, p) && (d[p] = b[p]);
        };
        return extendStatics(d, b);
      };
      return function(d, b) {
        extendStatics(d, b);
        function __() {
          this.constructor = d;
        }
        d.prototype = null === b ? Object.create(b) : (__.prototype = b.prototype, new __());
      };
    }();
    var __decorate = this && this.__decorate || function(decorators, target, key, desc) {
      var c = arguments.length, r = c < 3 ? target : null === desc ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
      if ("object" === typeof Reflect && "function" === typeof Reflect.decorate) r = Reflect.decorate(decorators, target, key, desc); else for (var i = decorators.length - 1; i >= 0; i--) (d = decorators[i]) && (r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r);
      return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
    var CocosDefaultAssets = function(_super) {
      __extends(CocosDefaultAssets, _super);
      function CocosDefaultAssets() {
        var _this = null !== _super && _super.apply(this, arguments) || this;
        _this.asset = [];
        return _this;
      }
      __decorate([ property([ cc.Asset ]) ], CocosDefaultAssets.prototype, "asset", void 0);
      CocosDefaultAssets = __decorate([ ccclass ], CocosDefaultAssets);
      return CocosDefaultAssets;
    }(cc.Component);
    exports.default = CocosDefaultAssets;
    cc._RF.pop();
  }, {} ],
  ColorAssembler2D: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "4909eEZLTxPxIRpPMiqg0Zh", "ColorAssembler2D");
    "use strict";
    var __extends = this && this.__extends || function() {
      var extendStatics = function(d, b) {
        extendStatics = Object.setPrototypeOf || {
          __proto__: []
        } instanceof Array && function(d, b) {
          d.__proto__ = b;
        } || function(d, b) {
          for (var p in b) Object.prototype.hasOwnProperty.call(b, p) && (d[p] = b[p]);
        };
        return extendStatics(d, b);
      };
      return function(d, b) {
        extendStatics(d, b);
        function __() {
          this.constructor = d;
        }
        d.prototype = null === b ? Object.create(b) : (__.prototype = b.prototype, new __());
      };
    }();
    var __decorate = this && this.__decorate || function(decorators, target, key, desc) {
      var c = arguments.length, r = c < 3 ? target : null === desc ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
      if ("object" === typeof Reflect && "function" === typeof Reflect.decorate) r = Reflect.decorate(decorators, target, key, desc); else for (var i = decorators.length - 1; i >= 0; i--) (d = decorators[i]) && (r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r);
      return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    exports.ColorAssembler2D = void 0;
    var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property, executeInEditMode = _a.executeInEditMode, requireComponent = _a.requireComponent;
    var ColorAssembler2D = function(_super) {
      __extends(ColorAssembler2D, _super);
      function ColorAssembler2D() {
        var _this = null !== _super && _super.apply(this, arguments) || this;
        _this._colors = [ cc.Color.RED, cc.Color.GREEN, cc.Color.BLUE, cc.Color.YELLOW ];
        return _this;
      }
      Object.defineProperty(ColorAssembler2D.prototype, "colors", {
        get: function() {
          return this._colors;
        },
        set: function(colors) {
          this._colors = colors;
          this._updateColors();
        },
        enumerable: false,
        configurable: true
      });
      ColorAssembler2D.prototype.onEnable = function() {
        cc.director.once(cc.Director.EVENT_AFTER_DRAW, this._updateColors, this);
      };
      ColorAssembler2D.prototype.onDisable = function() {
        cc.director.off(cc.Director.EVENT_AFTER_DRAW, this._updateColors, this);
        this.node["_renderFlag"] |= cc["RenderFlow"].FLAG_COLOR;
      };
      ColorAssembler2D.prototype._updateColors = function() {
        var cmp = this.getComponent(cc.RenderComponent);
        if (!cmp) return;
        var assembler = cmp["_assembler"];
        if (!(assembler instanceof cc["Assembler2D"])) return;
        var uint_verts = assembler._renderData.uintVDatas[0];
        if (!uint_verts) return;
        var floats_per_vert = assembler.floatsPerVert;
        var color_offset = assembler.colorOffset;
        var count = 0;
        for (var i = color_offset, l = uint_verts.length; i < l; i += floats_per_vert) uint_verts[i] = (this.colors[count++] || this.node.color)["_val"];
      };
      __decorate([ property({
        type: [ cc.Color ],
        visible: true
      }) ], ColorAssembler2D.prototype, "_colors", void 0);
      ColorAssembler2D = __decorate([ ccclass, requireComponent(cc.RenderComponent), executeInEditMode ], ColorAssembler2D);
      return ColorAssembler2D;
    }(cc.Component);
    exports.ColorAssembler2D = ColorAssembler2D;
    cc._RF.pop();
  }, {} ],
  Curve: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "6c992cP3UtLboTYTf/0zny6", "Curve");
    "use strict";
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    exports.CurveType = void 0;
    var CurveType;
    (function(CurveType) {
      CurveType[CurveType["Line"] = 0] = "Line";
      CurveType[CurveType["CubicInCurve"] = 1] = "CubicInCurve";
      CurveType[CurveType["CubicOutCurve"] = 2] = "CubicOutCurve";
      CurveType[CurveType["CubicInOutCurve"] = 3] = "CubicInOutCurve";
      CurveType[CurveType["QuadIn"] = 4] = "QuadIn";
      CurveType[CurveType["QuadOut"] = 5] = "QuadOut";
      CurveType[CurveType["QuadInOut"] = 6] = "QuadInOut";
      CurveType[CurveType["CircleIn"] = 7] = "CircleIn";
      CurveType[CurveType["CircleOut"] = 8] = "CircleOut";
      CurveType[CurveType["CircleInOut"] = 9] = "CircleInOut";
      CurveType[CurveType["BackIn"] = 10] = "BackIn";
      CurveType[CurveType["BackOut"] = 11] = "BackOut";
      CurveType[CurveType["BackInOut"] = 12] = "BackInOut";
    })(CurveType = exports.CurveType || (exports.CurveType = {}));
    var Curve = function() {
      function Curve() {}
      Curve.GetValue = function(p_type, p_value) {
        this.setSelectCurve(p_type);
        if (p_value >= 1) return this.selectCurve[this.selectCurve.length - 1];
        var _returnValue = 0;
        var _index = 0;
        while (p_value > .05) {
          _index++;
          p_value -= .05;
        }
        var _distance = this.selectCurve[_index + 1] - this.selectCurve[_index];
        _returnValue = _distance * (p_value / .05);
        _returnValue += this.selectCurve[_index];
        return _returnValue;
      };
      Curve.setSelectCurve = function(p_type) {
        switch (p_type) {
         case CurveType.Line:
          this.selectCurve = this.Line;
          break;

         case CurveType.CubicInCurve:
          this.selectCurve = this.CubicInCurve;
          break;

         case CurveType.CubicOutCurve:
          this.selectCurve = this.CubicOutCurve;
          break;

         case CurveType.CubicInOutCurve:
          this.selectCurve = this.CubicInOutCurve;
          break;

         case CurveType.QuadIn:
          this.selectCurve = this.QuadIn;
          break;

         case CurveType.QuadOut:
          this.selectCurve = this.QuadOut;
          break;

         case CurveType.QuadInOut:
          this.selectCurve = this.QuadInOut;
          break;

         case CurveType.CircleIn:
          this.selectCurve = this.CircleIn;
          break;

         case CurveType.CircleOut:
          this.selectCurve = this.CircleOut;
          break;

         case CurveType.CircleInOut:
          this.selectCurve = this.CircleInOut;
          break;

         case CurveType.BackIn:
          this.selectCurve = this.BackIn;
          break;

         case CurveType.BackOut:
          this.selectCurve = this.BackOut;
          break;

         case CurveType.BackInOut:
          this.selectCurve = this.BackInOut;
        }
      };
      Curve.Line = [ 0, .05, .1, .15, .2, .25, .3, .35, .4, .45, .5, .55, .6, .65, .7, .75, .8, .85, .9, .95, 1 ];
      Curve.CubicInCurve = [ 0, 112e-6, .009508, .003263, .007802, .015317, .026556, .042269, .063218, .09014, .123775, .164896, .21425, .27256, .34062, .41915, .508912, .610665, .725125, .853059, 1 ];
      Curve.CubicOutCurve = [ 0, .138132, .266959, .382283, .484826, .575357, .654593, .723307, .782225, .832137, .873772, .907884, .93522, .956527, .972561, .984073, .991806, .996516, .998951, .999863, 1 ];
      Curve.CubicInOutCurve = [ 0, 452e-6, .003805, .013057, .031213, .061279, .106235, .169101, .252893, .36055, .49518, .63155, .740873, .826116, .890262, .936286, .967223, .986067, .995805, .999451, 1 ];
      Curve.QuadIn = [ 0, .002336, .009671, .022008, .03934, .061683, .089015, .121364, .158705, .201033, .24837, .300731, .358052, .420395, .487742, .560084, .637452, .719784, .807121, .899502, 1 ];
      Curve.QuadOut = [ 0, .094314, .186993, .274675, .357352, .435018, .507694, .575358, .638022, .6957, .748361, .796024, .83869, .876362, .909021, .936689, .95935, .977019, .989678, .997338, 1 ];
      Curve.QuadInOut = [ 0, .00467, .019336, .04401, .078679, .123352, .178025, .242705, .317385, .402047, .496722, .592055, .67739, .75271, .81804, .873371, .918704, .954027, .979353, .994673, 1 ];
      Curve.CircleIn = [ 0, .001169, .004847, .011065, .019868, .03133, .045547, .062644, .082778, .106151, .133036, .163775, .198793, .238697, .284289, .336754, .397879, .470664, .56083, .682971, 1 ];
      Curve.CircleOut = [ 0, .307166, .432461, .524109, .597809, .659564, .712527, .758528, .798772, .834087, .865084, .89221, .915813, .936147, .953431, .96783, .979469, .988442, .994826, .998668, 1 ];
      Curve.CircleInOut = [ 0, .002342, .009766, .022513, .041024, .066043, .098768, .141335, .197833, .278737, .459755, .714563, .79783, .855477, .898789, .932075, .957558, .976462, .989568, .99733, 1 ];
      Curve.BackIn = [ 0, -.003669, -.013885, -.028621, -.045863, -.063564, -.079718, -.092283, -.099241, -.098564, -.088223, -.066195, -.030447, .021036, .090295, .179352, .290254, .424941, .585608, .774065, 1 ];
      Curve.BackOut = [ 0, .212609, .403008, .565345, .701737, .814095, .904512, .975006, 1.027588, 1.064296, 1.087165, 1.098204, 1.099452, 1.092932, 1.080664, 1.06469, 1.047023, 1.029694, 1.014731, 1.004158, 1 ];
      Curve.BackInOut = [ 0, -.010503, -.036515, -.067265, -.091971, -.099833, -.080068, -.021892, .085448, .252804, .490924, .733944, .905587, 1.016528, 1.077579, 1.099504, 1.093109, 1.069175, 1.038484, 1.011835, 1 ];
      Curve.selectCurve = [];
      return Curve;
    }();
    exports.default = Curve;
    cc._RF.pop();
  }, {} ],
  DissolveEffect: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "251cfweoNdDA7aBu4cOSAqd", "DissolveEffect");
    "use strict";
    var __extends = this && this.__extends || function() {
      var extendStatics = function(d, b) {
        extendStatics = Object.setPrototypeOf || {
          __proto__: []
        } instanceof Array && function(d, b) {
          d.__proto__ = b;
        } || function(d, b) {
          for (var p in b) Object.prototype.hasOwnProperty.call(b, p) && (d[p] = b[p]);
        };
        return extendStatics(d, b);
      };
      return function(d, b) {
        extendStatics(d, b);
        function __() {
          this.constructor = d;
        }
        d.prototype = null === b ? Object.create(b) : (__.prototype = b.prototype, new __());
      };
    }();
    var __decorate = this && this.__decorate || function(decorators, target, key, desc) {
      var c = arguments.length, r = c < 3 ? target : null === desc ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
      if ("object" === typeof Reflect && "function" === typeof Reflect.decorate) r = Reflect.decorate(decorators, target, key, desc); else for (var i = decorators.length - 1; i >= 0; i--) (d = decorators[i]) && (r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r);
      return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
    var DissolveEffect = function(_super) {
      __extends(DissolveEffect, _super);
      function DissolveEffect() {
        var _this = null !== _super && _super.apply(this, arguments) || this;
        _this.noiseValue = 0;
        _this.mSprite = null;
        _this.mMaterial = null;
        return _this;
      }
      DissolveEffect.prototype.start = function() {
        false;
        this.mSprite = this.getComponent(cc.Sprite);
        cc.isValid(this.mSprite) && (this.mMaterial = this.mSprite.getMaterials()[0]);
      };
      DissolveEffect.prototype.update = function(dt) {
        false;
        this.mMaterial.setProperty("_noiseValue", this.noiseValue);
      };
      __decorate([ property(cc.Float) ], DissolveEffect.prototype, "noiseValue", void 0);
      DissolveEffect = __decorate([ ccclass ], DissolveEffect);
      return DissolveEffect;
    }(cc.Component);
    exports.default = DissolveEffect;
    cc._RF.pop();
  }, {} ],
  UVOffset3D: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "34c30ulP39K35KACfmb9Ejk", "UVOffset3D");
    "use strict";
    var __extends = this && this.__extends || function() {
      var extendStatics = function(d, b) {
        extendStatics = Object.setPrototypeOf || {
          __proto__: []
        } instanceof Array && function(d, b) {
          d.__proto__ = b;
        } || function(d, b) {
          for (var p in b) Object.prototype.hasOwnProperty.call(b, p) && (d[p] = b[p]);
        };
        return extendStatics(d, b);
      };
      return function(d, b) {
        extendStatics(d, b);
        function __() {
          this.constructor = d;
        }
        d.prototype = null === b ? Object.create(b) : (__.prototype = b.prototype, new __());
      };
    }();
    var __decorate = this && this.__decorate || function(decorators, target, key, desc) {
      var c = arguments.length, r = c < 3 ? target : null === desc ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
      if ("object" === typeof Reflect && "function" === typeof Reflect.decorate) r = Reflect.decorate(decorators, target, key, desc); else for (var i = decorators.length - 1; i >= 0; i--) (d = decorators[i]) && (r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r);
      return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property, menu = _a.menu;
    var UVOffset3D = function(_super) {
      __extends(UVOffset3D, _super);
      function UVOffset3D() {
        var _this = null !== _super && _super.apply(this, arguments) || this;
        _this.useTip = "\u5716\u7247\u7684 WrapMode \u5fc5\u9808\u8981\u6539\u6210 Repeat\nMaterial\u9808\u639b\u4e0aUVOffset3DMaterial";
        _this.uvOffsetSpeed = new cc.Vec2(0, 0);
        _this.uvOffset = new cc.Vec2(0, 0);
        _this.meshRenderer = null;
        _this.mMaterial = null;
        return _this;
      }
      UVOffset3D.prototype.onLoad = function() {};
      UVOffset3D.prototype.start = function() {
        false;
        this.meshRenderer = this.getComponent(cc.MeshRenderer);
        cc.isValid(this.meshRenderer) && (this.mMaterial = this.meshRenderer.getMaterials()[0]);
      };
      UVOffset3D.prototype.update = function(dt) {
        false;
        this.uvOffset.x += dt * this.uvOffsetSpeed.x;
        this.uvOffset.y += dt * this.uvOffsetSpeed.y;
        this.mMaterial.setProperty("mainOffset", [ this.uvOffset.x, this.uvOffset.y ]);
      };
      __decorate([ property({
        multiline: true,
        readonly: true
      }) ], UVOffset3D.prototype, "useTip", void 0);
      __decorate([ property({
        tooltip: "UV\u7684\u4f4d\u7f6e\u79fb\u52d5 \u5efa\u8b700~1\u4e4b\u9593",
        displayName: "UV\u504f\u79fb\u901f\u5ea6"
      }) ], UVOffset3D.prototype, "uvOffsetSpeed", void 0);
      UVOffset3D = __decorate([ ccclass, menu("\u81ea\u8a02\u5de5\u5177/3D/Effect/UV\u79fb\u52d5") ], UVOffset3D);
      return UVOffset3D;
    }(cc.Component);
    exports.default = UVOffset3D;
    cc._RF.pop();
  }, {} ],
  UVOffsetEffect: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "74257nLYGhKdKbLpaUK1XQR", "UVOffsetEffect");
    "use strict";
    var __extends = this && this.__extends || function() {
      var extendStatics = function(d, b) {
        extendStatics = Object.setPrototypeOf || {
          __proto__: []
        } instanceof Array && function(d, b) {
          d.__proto__ = b;
        } || function(d, b) {
          for (var p in b) Object.prototype.hasOwnProperty.call(b, p) && (d[p] = b[p]);
        };
        return extendStatics(d, b);
      };
      return function(d, b) {
        extendStatics(d, b);
        function __() {
          this.constructor = d;
        }
        d.prototype = null === b ? Object.create(b) : (__.prototype = b.prototype, new __());
      };
    }();
    var __decorate = this && this.__decorate || function(decorators, target, key, desc) {
      var c = arguments.length, r = c < 3 ? target : null === desc ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
      if ("object" === typeof Reflect && "function" === typeof Reflect.decorate) r = Reflect.decorate(decorators, target, key, desc); else for (var i = decorators.length - 1; i >= 0; i--) (d = decorators[i]) && (r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r);
      return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property, menu = _a.menu;
    var UVOffsetEffect = function(_super) {
      __extends(UVOffsetEffect, _super);
      function UVOffsetEffect() {
        var _this = null !== _super && _super.apply(this, arguments) || this;
        _this.useTip = "\u5716\u7247\u7684 WrapMode \u5fc5\u9808\u8981\u6539\u6210 Repeat\nMaterial\u9808\u639b\u4e0aUVOffsetMaterial";
        _this.uvOffsetSpeed = new cc.Vec2(0, 0);
        _this.uvOffset = new cc.Vec2(0, 0);
        _this.mSprite = null;
        _this.mMaterial = null;
        return _this;
      }
      UVOffsetEffect.prototype.onLoad = function() {};
      UVOffsetEffect.prototype.start = function() {
        false;
        this.mSprite = this.getComponent(cc.Sprite);
        cc.isValid(this.mSprite) && (this.mMaterial = this.mSprite.getMaterials()[0]);
      };
      UVOffsetEffect.prototype.update = function(dt) {
        false;
        this.uvOffset.x += dt * this.uvOffsetSpeed.x;
        this.uvOffset.y += dt * this.uvOffsetSpeed.y;
        this.uvOffset.x > 1 && (this.uvOffset.x -= 1);
        this.uvOffset.x < 0 && (this.uvOffset.x += 1);
        this.uvOffset.y > 1 && (this.uvOffset.y -= 1);
        this.uvOffset.y < 0 && (this.uvOffset.y += 1);
        this.mMaterial.setProperty("OffsetX", this.uvOffset.x);
        this.mMaterial.setProperty("OffsetY", this.uvOffset.y);
      };
      __decorate([ property({
        multiline: true,
        readonly: true
      }) ], UVOffsetEffect.prototype, "useTip", void 0);
      __decorate([ property({
        tooltip: "UV\u7684\u4f4d\u7f6e\u79fb\u52d5 \u5efa\u8b700~1\u4e4b\u9593",
        displayName: "UV\u504f\u79fb\u901f\u5ea6"
      }) ], UVOffsetEffect.prototype, "uvOffsetSpeed", void 0);
      UVOffsetEffect = __decorate([ ccclass, menu("\u81ea\u8a02\u5de5\u5177/UI/Effect/UV\u79fb\u52d5") ], UVOffsetEffect);
      return UVOffsetEffect;
    }(cc.Component);
    exports.default = UVOffsetEffect;
    cc._RF.pop();
  }, {} ],
  UVOffset_V2_Effect: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "46009TQOlVMpbFm2/KF+W6G", "UVOffset_V2_Effect");
    "use strict";
    var __extends = this && this.__extends || function() {
      var extendStatics = function(d, b) {
        extendStatics = Object.setPrototypeOf || {
          __proto__: []
        } instanceof Array && function(d, b) {
          d.__proto__ = b;
        } || function(d, b) {
          for (var p in b) Object.prototype.hasOwnProperty.call(b, p) && (d[p] = b[p]);
        };
        return extendStatics(d, b);
      };
      return function(d, b) {
        extendStatics(d, b);
        function __() {
          this.constructor = d;
        }
        d.prototype = null === b ? Object.create(b) : (__.prototype = b.prototype, new __());
      };
    }();
    var __decorate = this && this.__decorate || function(decorators, target, key, desc) {
      var c = arguments.length, r = c < 3 ? target : null === desc ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
      if ("object" === typeof Reflect && "function" === typeof Reflect.decorate) r = Reflect.decorate(decorators, target, key, desc); else for (var i = decorators.length - 1; i >= 0; i--) (d = decorators[i]) && (r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r);
      return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property, menu = _a.menu;
    var UVOffset_V2_Effect = function(_super) {
      __extends(UVOffset_V2_Effect, _super);
      function UVOffset_V2_Effect() {
        var _this = null !== _super && _super.apply(this, arguments) || this;
        _this.useTip = "\u5716\u7247\u7684 WrapMode \u5fc5\u9808\u8981\u6539\u6210 Repeat\nMaterial\u9808\u639b\u4e0aUVOffsetMaterial";
        _this.uvOffsetSpeed = new cc.Vec2(0, 0);
        _this.scaleMagnification = 1;
        _this.rotate = 1;
        _this.uvOffset = new cc.Vec2(0, 0);
        _this.mSprite = null;
        _this.mMaterial = null;
        return _this;
      }
      UVOffset_V2_Effect.prototype.onLoad = function() {};
      UVOffset_V2_Effect.prototype.start = function() {
        false;
        this.mSprite = this.getComponent(cc.Sprite);
        cc.isValid(this.mSprite) && (this.mMaterial = this.mSprite.getMaterials()[0]);
      };
      UVOffset_V2_Effect.prototype.update = function(dt) {
        false;
        this.uvOffset.x += dt * this.uvOffsetSpeed.x;
        this.uvOffset.y += dt * this.uvOffsetSpeed.y;
        this.uvOffset.x > 1 && (this.uvOffset.x -= 1);
        this.uvOffset.x < 0 && (this.uvOffset.x += 1);
        this.uvOffset.y > 1 && (this.uvOffset.y -= 1);
        this.uvOffset.y < 0 && (this.uvOffset.y += 1);
        this.mMaterial.setProperty("OffsetX", this.uvOffset.x);
        this.mMaterial.setProperty("OffsetY", this.uvOffset.y);
        this.mMaterial.setProperty("scaleMagnification", this.scaleMagnification);
        this.mMaterial.setProperty("rotate", this.rotate);
      };
      __decorate([ property({
        multiline: true,
        readonly: true
      }) ], UVOffset_V2_Effect.prototype, "useTip", void 0);
      __decorate([ property({
        tooltip: "UV\u7684\u4f4d\u7f6e\u79fb\u52d5 \u5efa\u8b700~1\u4e4b\u9593",
        displayName: "UV\u504f\u79fb\u901f\u5ea6"
      }) ], UVOffset_V2_Effect.prototype, "uvOffsetSpeed", void 0);
      __decorate([ property({
        tooltip: "\u5716\u7247\u653e\u5927\u7684\u500d\u7387\uff0c\u4e0d\u5f15\u97ffmask",
        displayName: "\u5716\u7247\u653e\u5927\u7684\u500d\u7387"
      }) ], UVOffset_V2_Effect.prototype, "scaleMagnification", void 0);
      __decorate([ property({
        tooltip: "\u5716\u7247\u7684\u65cb\u8f49\uff0c\u4e0d\u5f15\u97ffmask",
        displayName: "\u5716\u7247\u7684\u65cb\u8f49"
      }) ], UVOffset_V2_Effect.prototype, "rotate", void 0);
      UVOffset_V2_Effect = __decorate([ ccclass ], UVOffset_V2_Effect);
      return UVOffset_V2_Effect;
    }(cc.Component);
    exports.default = UVOffset_V2_Effect;
    cc._RF.pop();
  }, {} ]
}, {}, [ "CocosDefaultAssets", "ColorAssembler2D", "Curve", "DissolveEffect", "UVOffsetEffect", "UVOffset3D", "UVOffset_V2_Effect" ]);