window.__require = function e(t, n, r) {
  function s(o, u) {
    if (!n[o]) {
      if (!t[o]) {
        var b = o.split("/");
        b = b[b.length - 1];
        if (!t[b]) {
          var a = "function" == typeof __require && __require;
          if (!u && a) return a(b, !0);
          if (i) return i(b, !0);
          throw new Error("Cannot find module '" + o + "'");
        }
        o = b;
      }
      var f = n[o] = {
        exports: {}
      };
      t[o][0].call(f.exports, function(e) {
        var n = t[o][1][e];
        return s(n || e);
      }, f, f.exports, e, t, n, r);
    }
    return n[o].exports;
  }
  var i = "function" == typeof __require && __require;
  for (var o = 0; o < r.length; o++) s(r[o]);
  return s;
}({
  G53_AutoGamePanel: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "e1508//GP5BZ7OAA3YQnG/4", "G53_AutoGamePanel");
    "use strict";
    var __extends = this && this.__extends || function() {
      var extendStatics = function(d, b) {
        extendStatics = Object.setPrototypeOf || {
          __proto__: []
        } instanceof Array && function(d, b) {
          d.__proto__ = b;
        } || function(d, b) {
          for (var p in b) Object.prototype.hasOwnProperty.call(b, p) && (d[p] = b[p]);
        };
        return extendStatics(d, b);
      };
      return function(d, b) {
        extendStatics(d, b);
        function __() {
          this.constructor = d;
        }
        d.prototype = null === b ? Object.create(b) : (__.prototype = b.prototype, new __());
      };
    }();
    var __decorate = this && this.__decorate || function(decorators, target, key, desc) {
      var c = arguments.length, r = c < 3 ? target : null === desc ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
      if ("object" === typeof Reflect && "function" === typeof Reflect.decorate) r = Reflect.decorate(decorators, target, key, desc); else for (var i = decorators.length - 1; i >= 0; i--) (d = decorators[i]) && (r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r);
      return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    var Slot_AutoGamePanel_1 = require("../../../SlotFramework/Game/Panel/Slot_AutoGamePanel");
    var G53_DataManager_1 = require("../../Common/G53_DataManager");
    var ccclass = cc._decorator.ccclass;
    var G53_AutoGamePanel = function(_super) {
      __extends(G53_AutoGamePanel, _super);
      function G53_AutoGamePanel() {
        var _this = null !== _super && _super.apply(this, arguments) || this;
        _this.data = null;
        return _this;
      }
      G53_AutoGamePanel.prototype.init = function() {
        this.data = G53_DataManager_1.default.getInstance();
        _super.prototype.init.call(this);
      };
      G53_AutoGamePanel = __decorate([ ccclass ], G53_AutoGamePanel);
      return G53_AutoGamePanel;
    }(Slot_AutoGamePanel_1.default);
    exports.default = G53_AutoGamePanel;
    cc._RF.pop();
  }, {
    "../../../SlotFramework/Game/Panel/Slot_AutoGamePanel": void 0,
    "../../Common/G53_DataManager": "G53_DataManager"
  } ],
  G53_BonusSymbolPanel: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "67a49viygBHp6YX9ABnWEgJ", "G53_BonusSymbolPanel");
    "use strict";
    var __extends = this && this.__extends || function() {
      var extendStatics = function(d, b) {
        extendStatics = Object.setPrototypeOf || {
          __proto__: []
        } instanceof Array && function(d, b) {
          d.__proto__ = b;
        } || function(d, b) {
          for (var p in b) Object.prototype.hasOwnProperty.call(b, p) && (d[p] = b[p]);
        };
        return extendStatics(d, b);
      };
      return function(d, b) {
        extendStatics(d, b);
        function __() {
          this.constructor = d;
        }
        d.prototype = null === b ? Object.create(b) : (__.prototype = b.prototype, new __());
      };
    }();
    var __decorate = this && this.__decorate || function(decorators, target, key, desc) {
      var c = arguments.length, r = c < 3 ? target : null === desc ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
      if ("object" === typeof Reflect && "function" === typeof Reflect.decorate) r = Reflect.decorate(decorators, target, key, desc); else for (var i = decorators.length - 1; i >= 0; i--) (d = decorators[i]) && (r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r);
      return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __awaiter = this && this.__awaiter || function(thisArg, _arguments, P, generator) {
      function adopt(value) {
        return value instanceof P ? value : new P(function(resolve) {
          resolve(value);
        });
      }
      return new (P || (P = Promise))(function(resolve, reject) {
        function fulfilled(value) {
          try {
            step(generator.next(value));
          } catch (e) {
            reject(e);
          }
        }
        function rejected(value) {
          try {
            step(generator["throw"](value));
          } catch (e) {
            reject(e);
          }
        }
        function step(result) {
          result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected);
        }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
      });
    };
    var __generator = this && this.__generator || function(thisArg, body) {
      var _ = {
        label: 0,
        sent: function() {
          if (1 & t[0]) throw t[1];
          return t[1];
        },
        trys: [],
        ops: []
      }, f, y, t, g;
      return g = {
        next: verb(0),
        throw: verb(1),
        return: verb(2)
      }, "function" === typeof Symbol && (g[Symbol.iterator] = function() {
        return this;
      }), g;
      function verb(n) {
        return function(v) {
          return step([ n, v ]);
        };
      }
      function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
          if (f = 1, y && (t = 2 & op[0] ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 
          0) : y.next) && !(t = t.call(y, op[1])).done) return t;
          (y = 0, t) && (op = [ 2 & op[0], t.value ]);
          switch (op[0]) {
           case 0:
           case 1:
            t = op;
            break;

           case 4:
            _.label++;
            return {
              value: op[1],
              done: false
            };

           case 5:
            _.label++;
            y = op[1];
            op = [ 0 ];
            continue;

           case 7:
            op = _.ops.pop();
            _.trys.pop();
            continue;

           default:
            if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (6 === op[0] || 2 === op[0])) {
              _ = 0;
              continue;
            }
            if (3 === op[0] && (!t || op[1] > t[0] && op[1] < t[3])) {
              _.label = op[1];
              break;
            }
            if (6 === op[0] && _.label < t[1]) {
              _.label = t[1];
              t = op;
              break;
            }
            if (t && _.label < t[2]) {
              _.label = t[2];
              _.ops.push(op);
              break;
            }
            t[2] && _.ops.pop();
            _.trys.pop();
            continue;
          }
          op = body.call(thisArg, _);
        } catch (e) {
          op = [ 6, e ];
          y = 0;
        } finally {
          f = t = 0;
        }
        if (5 & op[0]) throw op[1];
        return {
          value: op[0] ? op[1] : void 0,
          done: true
        };
      }
    };
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    var FXController_1 = require("../../../Common/Optional/Crosis/FXController/FXController");
    var AudioManager_1 = require("../../../Common/Tools/AudioManager/AudioManager");
    var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
    var G53_BonusSymbolPanel = function(_super) {
      __extends(G53_BonusSymbolPanel, _super);
      function G53_BonusSymbolPanel() {
        var _this = null !== _super && _super.apply(this, arguments) || this;
        _this._isUpdateScript = false;
        _this.fxFgDoorOpen = null;
        _this.numberLabel = null;
        _this.symbolList = null;
        _this.symbolPosList = [];
        _this.flyTargetPos = cc.Vec3.ZERO;
        return _this;
      }
      Object.defineProperty(G53_BonusSymbolPanel.prototype, "isUpdateScript", {
        get: function() {
          return this._isUpdateScript;
        },
        set: function(_isGetPos) {
          this.updateScript();
        },
        enumerable: false,
        configurable: true
      });
      G53_BonusSymbolPanel.prototype.init = function() {
        this.node.active = false;
        this.node.position = cc.Vec3.ZERO;
        this.numberLabel.node.active = false;
        this.fxFgDoorOpen.getComponent(FXController_1.default).stop();
      };
      G53_BonusSymbolPanel.prototype.showTarget = function(p_targetList) {
        void 0 === p_targetList && (p_targetList = [ 1, 2, 3, 11, 12, 13, 14 ]);
        return __awaiter(this, void 0, Promise, function() {
          var _this = this;
          return __generator(this, function(_a) {
            switch (_a.label) {
             case 0:
              this.setEliminateTarget(p_targetList);
              return [ 4, new Promise(function(resolve) {
                _this.showTargetEffect(resolve);
              }) ];

             case 1:
              _a.sent();
              return [ 2 ];
            }
          });
        });
      };
      G53_BonusSymbolPanel.prototype.hideTarget = function(p_isFast) {
        void 0 === p_isFast && (p_isFast = false);
        return __awaiter(this, void 0, Promise, function() {
          var _this = this;
          return __generator(this, function(_a) {
            switch (_a.label) {
             case 0:
              return [ 4, new Promise(function(resolve) {
                _this.hideTargetEffect(p_isFast, resolve);
              }) ];

             case 1:
              _a.sent();
              return [ 2 ];
            }
          });
        });
      };
      G53_BonusSymbolPanel.prototype.showPassLevel = function(p_level) {
        return __awaiter(this, void 0, Promise, function() {
          var _this = this;
          return __generator(this, function(_a) {
            switch (_a.label) {
             case 0:
              return [ 4, new Promise(function(resolve) {
                _this.showPassLevelEffect(p_level, resolve);
              }) ];

             case 1:
              _a.sent();
              return [ 2 ];
            }
          });
        });
      };
      G53_BonusSymbolPanel.prototype.showShfitNextTarget = function(p_level) {
        return __awaiter(this, void 0, Promise, function() {
          var _this = this;
          return __generator(this, function(_a) {
            switch (_a.label) {
             case 0:
              return [ 4, new Promise(function(resolve) {
                _this.showShfitNextTargetEffect(p_level, resolve);
              }) ];

             case 1:
              _a.sent();
              return [ 2 ];
            }
          });
        });
      };
      G53_BonusSymbolPanel.prototype.updateScript = function() {
        this.symbolList = this.node.getChildByName("SymbolList");
        this.numberLabel = this.node.getChildByName("NumberLabel").getComponent(cc.Label);
        this.symbolPosList = this.node.getChildByName("SymbolList").children.map(function(node) {
          return node.position;
        });
        this.flyTargetPos = this.node.getChildByName("FlySyncParent").getChildByName("FlyTarget").position;
      };
      G53_BonusSymbolPanel.prototype.setEliminateTarget = function(p_targetList) {
        var _this = this;
        var symbolList = [ 1, 2, 3, 11, 12, 13, 14 ];
        this.numberLabel.string = "5";
        this.symbolList.children.forEach(function(child, i) {
          var trail = child.children[0];
          var anLoop = child.children[1];
          var anStart = child.children[2];
          var anWin = child.children[3];
          var anStartAni = anStart.getComponent(cc.Animation);
          anLoop.active = false;
          anWin.active = false;
          trail.active = false;
          anLoop.children.forEach(function(anLoopChild, j) {
            anLoopChild.active = symbolList[j] == p_targetList[i];
          });
          anStartAni.play(anStartAni.getClips()[0 == i ? 0 : 1].name);
          anStart.children[0].children.forEach(function(anLoopChild, j) {
            anLoopChild.active = symbolList[j] == p_targetList[i];
          });
          child.position = _this.symbolPosList[i];
          child.active = true;
        });
      };
      G53_BonusSymbolPanel.prototype.showTargetEffect = function(resolve) {
        var _this = this;
        cc.Tween.stopAllByTarget(this.symbolList);
        cc.tween(this.symbolList).call(function() {
          AudioManager_1.AudioManager.instance.playAudioEvent("FreeGameTargetFlyOut");
          _this.symbolList.scale = 0;
          _this.numberLabel.node.active = false;
          _this.node.active = true;
          _this.fxFgDoorOpen.getComponent(FXController_1.default).play();
        }).to(.15, {
          scale: 1.3
        }, {
          easing: "sineOut"
        }).to(.15, {
          scale: 1
        }, {
          easing: "sineIn"
        }).delay(.15).call(function() {
          _this.numberLabel.node.active = true;
          resolve();
        }).start();
      };
      G53_BonusSymbolPanel.prototype.hideTargetEffect = function(p_isFast, resolve) {
        var _this = this;
        cc.Tween.stopAllByTarget(this.symbolList);
        cc.tween(this.symbolList).call(function() {
          _this.symbolList.scale = 1;
          _this.numberLabel.node.active = false;
        }).to(p_isFast ? 0 : .2, {
          scaleX: 0,
          scaleY: .1
        }, {
          easing: "sineOut"
        }).call(function() {
          _this.node.active = false;
          resolve();
        }).start();
      };
      G53_BonusSymbolPanel.prototype.showPassLevelEffect = function(p_level, resolve) {
        var _this = this;
        var node = this.symbolList.children[p_level - 1];
        var trail = node.children[0];
        var anWin = node.children[3];
        cc.Tween.stopAllByTarget(node);
        cc.tween(node).call(function() {
          AudioManager_1.AudioManager.instance.playAudioEvent("FreeGameTargetWin");
          _this.numberLabel.node.active = false;
          anWin.getComponent(FXController_1.default).play();
        }).delay(.2).call(function() {
          AudioManager_1.AudioManager.instance.playAudioEvent("FreeGameTargetFlyDown");
          trail.getComponent(FXController_1.default).play();
        }).sequence(cc.bezierTo(.5, getParabola(node.position, this.flyTargetPos, 150, 30)).easing(cc.easeInOut(.5)), cc.tween().call(function() {
          trail.getComponent(FXController_1.default).stop();
          node.active = false;
          resolve();
        })).start();
      };
      G53_BonusSymbolPanel.prototype.showShfitNextTargetEffect = function(p_level, resolve) {
        var _this = this;
        var nextNode = this.symbolList.children[p_level];
        var anStartAni = nextNode.children[2].getComponent(cc.Animation);
        var anLoop = nextNode.children[1];
        cc.Tween.stopAllByTarget(nextNode);
        cc.tween(nextNode).call(function() {
          anLoop.active = true;
          anStartAni.play(anStartAni.getClips()[0].name);
          AudioManager_1.AudioManager.instance.playAudioEvent("FreeGameTargetFlyRight");
        }).to(.5, {
          position: this.symbolPosList[0]
        }).call(function() {
          var _nextLevel = 5 + p_level;
          _this.numberLabel.string = _nextLevel > 10 ? "" : "" + _nextLevel;
          _this.numberLabel.node.active = true;
        }).delay(0).call(function() {
          return resolve();
        }).start();
      };
      __decorate([ property({
        visible: false,
        serializable: false
      }) ], G53_BonusSymbolPanel.prototype, "_isUpdateScript", void 0);
      __decorate([ property({
        type: cc.Boolean,
        displayName: "\u66f4\u65b0\u7d44\u4ef6"
      }) ], G53_BonusSymbolPanel.prototype, "isUpdateScript", null);
      __decorate([ property(cc.Node) ], G53_BonusSymbolPanel.prototype, "fxFgDoorOpen", void 0);
      __decorate([ property(cc.Label) ], G53_BonusSymbolPanel.prototype, "numberLabel", void 0);
      __decorate([ property(cc.Node) ], G53_BonusSymbolPanel.prototype, "symbolList", void 0);
      __decorate([ property([ cc.Vec3 ]) ], G53_BonusSymbolPanel.prototype, "symbolPosList", void 0);
      __decorate([ property(cc.Vec3) ], G53_BonusSymbolPanel.prototype, "flyTargetPos", void 0);
      G53_BonusSymbolPanel = __decorate([ ccclass ], G53_BonusSymbolPanel);
      return G53_BonusSymbolPanel;
    }(cc.Component);
    exports.default = G53_BonusSymbolPanel;
    function getParabola(startPoint, endPoint, height, angle) {
      var radian = 3.14159 * angle / 180;
      var q1x = startPoint.x + (endPoint.x - startPoint.x) / 4;
      var q1 = cc.v2(q1x, height + startPoint.y + Math.cos(radian) * q1x);
      var q2x = startPoint.x + (endPoint.x - startPoint.x) / 2;
      var q2 = cc.v2(q2x, height + startPoint.y + Math.cos(radian) * q2x);
      return [ q1, q2, cc.v2(endPoint.x, endPoint.y) ];
    }
    cc._RF.pop();
  }, {
    "../../../Common/Optional/Crosis/FXController/FXController": void 0,
    "../../../Common/Tools/AudioManager/AudioManager": void 0
  } ],
  G53_CommonData: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "f1401ywdvVNprcJC5vtvBMl", "G53_CommonData");
    "use strict";
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    exports.ScatterData = exports.WinJoinData = exports.EliminateData = exports.jackpotBonusData = exports.ResultData = exports.BaseGameSpinResponeData = exports.ReelTable = exports.GetInitialReelInfoData = exports.FastChoseRoomData = exports.RoomData = exports.GetRoomListData = exports.LoginData = void 0;
    var LoginData = function() {
      function LoginData() {
        this.playerId = "";
        this.nickname = "";
        this.language = "";
        this.timestamp = 0;
        this.balance = 0;
        this.gameId = 0;
        this.icon = 0;
      }
      return LoginData;
    }();
    exports.LoginData = LoginData;
    var GetRoomListData = function() {
      function GetRoomListData() {
        this.rooms = [];
      }
      return GetRoomListData;
    }();
    exports.GetRoomListData = GetRoomListData;
    var RoomData = function() {
      function RoomData() {
        this.roomId = "";
        this.status = 0;
      }
      return RoomData;
    }();
    exports.RoomData = RoomData;
    var FastChoseRoomData = function() {
      function FastChoseRoomData() {}
      return FastChoseRoomData;
    }();
    exports.FastChoseRoomData = FastChoseRoomData;
    var GetInitialReelInfoData = function() {
      function GetInitialReelInfoData() {
        this.reels = new ReelTable();
        this.position = [];
      }
      return GetInitialReelInfoData;
    }();
    exports.GetInitialReelInfoData = GetInitialReelInfoData;
    var ReelTable = function() {
      function ReelTable() {
        this.BG = [];
        this.FG = [];
      }
      return ReelTable;
    }();
    exports.ReelTable = ReelTable;
    var BaseGameSpinResponeData = function() {
      function BaseGameSpinResponeData() {
        this.totalBonus = 0;
        this.totalBet = 0;
        this.allReels = [];
        this.resultList = [];
        this.currentCash = 0;
        this.getJackpot = false;
      }
      return BaseGameSpinResponeData;
    }();
    exports.BaseGameSpinResponeData = BaseGameSpinResponeData;
    var ResultData = function() {
      function ResultData() {
        this.totalWinBonus = 0;
        this.randomNumList = [ 0, 0, 0, 0, 0, 0 ];
        this.reels = [];
        this.eliminateList = [];
        this.scatter = new ScatterData();
        this.jackpotBonus = new jackpotBonusData();
      }
      return ResultData;
    }();
    exports.ResultData = ResultData;
    var jackpotBonusData = function() {
      function jackpotBonusData() {
        this.count = 0;
        this.jackpotPos = [];
        this.winBonus = 0;
      }
      return jackpotBonusData;
    }();
    exports.jackpotBonusData = jackpotBonusData;
    var EliminateData = function() {
      function EliminateData() {
        this.isEliminate = false;
        this.winJoinList = [];
        this.wildCoverPos = [];
        this.eliWinBonus = 0;
      }
      return EliminateData;
    }();
    exports.EliminateData = EliminateData;
    var WinJoinData = function() {
      function WinJoinData() {
        this.joinCount = 0;
        this.winNum = 0;
        this.winBonus = 0;
        this.specialMultiple = 0;
      }
      return WinJoinData;
    }();
    exports.WinJoinData = WinJoinData;
    var ScatterData = function() {
      function ScatterData() {
        this.winNum = 20;
        this.count = 0;
        this.winBonus = 0;
        this.scatterPos = [];
      }
      return ScatterData;
    }();
    exports.ScatterData = ScatterData;
    cc._RF.pop();
  }, {} ],
  G53_DataManager: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "73849O/ypxDbLg5vwPbiJOJ", "G53_DataManager");
    "use strict";
    var __extends = this && this.__extends || function() {
      var extendStatics = function(d, b) {
        extendStatics = Object.setPrototypeOf || {
          __proto__: []
        } instanceof Array && function(d, b) {
          d.__proto__ = b;
        } || function(d, b) {
          for (var p in b) Object.prototype.hasOwnProperty.call(b, p) && (d[p] = b[p]);
        };
        return extendStatics(d, b);
      };
      return function(d, b) {
        extendStatics(d, b);
        function __() {
          this.constructor = d;
        }
        d.prototype = null === b ? Object.create(b) : (__.prototype = b.prototype, new __());
      };
    }();
    var __decorate = this && this.__decorate || function(decorators, target, key, desc) {
      var c = arguments.length, r = c < 3 ? target : null === desc ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
      if ("object" === typeof Reflect && "function" === typeof Reflect.decorate) r = Reflect.decorate(decorators, target, key, desc); else for (var i = decorators.length - 1; i >= 0; i--) (d = decorators[i]) && (r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r);
      return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    var Slot_DataManager_1 = require("../../SlotFramework/Slot_DataManager");
    var ccclass = cc._decorator.ccclass;
    var G53_DataManager = function(_super) {
      __extends(G53_DataManager, _super);
      function G53_DataManager() {
        var _this = null !== _super && _super.apply(this, arguments) || this;
        _this.gameVersion = "Dev 0.1.1";
        _this.SDKVersion = "Dev 0.1.1";
        _this.isShowDrawcall = false;
        _this.resultList = [];
        _this.jpData = null;
        _this.getJackpot = false;
        return _this;
      }
      G53_DataManager.prototype.init = function() {
        _super.prototype.init.call(this);
        this.gameID = 53;
        this.path = "Resources";
        this.skin = "Normal";
        this.setMagnificationTable();
      };
      G53_DataManager.prototype.setMagnificationTable = function() {
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H1] = {};
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H1][4] = 20;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H1][5] = 50;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H1][6] = 100;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H1][7] = 500;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H1][8] = 1e3;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H1][9] = 2e3;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H1][10] = 5e3;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H1][11] = 2e4;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H1][12] = 5e4;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H1][13] = 6e4;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H1][14] = 8e4;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H1][15] = 20;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H1][16] = 50;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H1][17] = 100;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H1][18] = 500;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H1][19] = 1e3;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H1][20] = 2e3;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H1][21] = 5e3;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H1][22] = 2e4;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H1][23] = 5e4;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H1][24] = 8e4;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H1][25] = 1e5;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H1][26] = 20;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H1][27] = 50;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H1][28] = 100;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H1][29] = 500;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H1][30] = 1e3;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H1][31] = 2e3;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H1][32] = 5e3;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H1][33] = 2e4;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H1][34] = 5e4;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H1][35] = 1e5;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H1][36] = 1e5;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H2] = {};
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H2][4] = 10;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H2][5] = 30;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H2][6] = 50;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H2][7] = 60;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H2][8] = 100;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H2][9] = 750;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H2][10] = 1e3;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H2][11] = 1e4;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H2][12] = 2e4;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H2][13] = 5e4;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H2][14] = 6e4;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H2][15] = 10;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H2][16] = 30;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H2][17] = 50;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H2][18] = 60;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H2][19] = 100;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H2][20] = 750;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H2][21] = 1e3;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H2][22] = 1e4;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H2][23] = 2e4;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H2][24] = 5e4;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H2][25] = 7e4;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H2][26] = 10;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H2][27] = 30;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H2][28] = 50;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H2][29] = 60;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H2][30] = 100;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H2][31] = 750;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H2][32] = 1e3;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H2][33] = 1e4;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H2][34] = 2e4;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H2][35] = 5e4;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H2][36] = 8e4;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H3] = {};
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H3][4] = 5;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H3][5] = 10;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H3][6] = 20;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H3][7] = 40;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H3][8] = 80;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H3][9] = 160;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H3][10] = 500;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H3][11] = 1e3;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H3][12] = 2e3;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H3][13] = 5e3;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H3][14] = 6e3;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H3][15] = 5;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H3][16] = 10;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H3][17] = 20;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H3][18] = 40;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H3][19] = 80;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H3][20] = 160;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H3][21] = 500;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H3][22] = 1e3;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H3][23] = 2e3;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H3][24] = 5e3;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H3][25] = 7e3;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H3][26] = 5;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H3][27] = 10;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H3][28] = 20;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H3][29] = 40;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H3][30] = 80;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H3][31] = 160;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H3][32] = 500;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H3][33] = 1e3;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H3][34] = 2e3;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H3][35] = 5e3;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H3][36] = 8e3;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H4] = {};
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H4][4] = 4;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H4][5] = 5;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H4][6] = 10;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H4][7] = 20;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H4][8] = 30;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H4][9] = 50;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H4][10] = 100;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H4][11] = 250;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H4][12] = 500;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H4][13] = 750;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H4][14] = 800;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H4][15] = 4;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H4][16] = 5;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H4][17] = 10;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H4][18] = 20;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H4][19] = 30;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H4][20] = 50;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H4][21] = 100;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H4][22] = 250;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H4][23] = 500;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H4][24] = 750;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H4][25] = 1e3;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H4][26] = 4;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H4][27] = 5;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H4][28] = 10;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H4][29] = 20;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H4][30] = 30;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H4][31] = 50;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H4][32] = 100;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H4][33] = 250;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H4][34] = 500;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H4][35] = 750;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H4][36] = 1200;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H5] = {};
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H5][4] = 2;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H5][5] = 4;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H5][6] = 5;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H5][7] = 8;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H5][8] = 10;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H5][9] = 20;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H5][10] = 30;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H5][11] = 50;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H5][12] = 100;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H5][13] = 200;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H5][14] = 400;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H5][15] = 2;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H5][16] = 4;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H5][17] = 5;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H5][18] = 8;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H5][19] = 10;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H5][20] = 20;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H5][21] = 30;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H5][22] = 50;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H5][23] = 100;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H5][24] = 200;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H5][25] = 450;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H5][26] = 2;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H5][27] = 4;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H5][28] = 5;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H5][29] = 8;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H5][30] = 10;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H5][31] = 20;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H5][32] = 30;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H5][33] = 50;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H5][34] = 100;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H5][35] = 200;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H5][36] = 500;
      };
      G53_DataManager = __decorate([ ccclass ], G53_DataManager);
      return G53_DataManager;
    }(Slot_DataManager_1.default);
    exports.default = G53_DataManager;
    cc._RF.pop();
  }, {
    "../../SlotFramework/Slot_DataManager": void 0
  } ],
  G53_DynamicPopUpPanelManager: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "59cb39ltwRCEbUVwrpK4lGK", "G53_DynamicPopUpPanelManager");
    "use strict";
    var __extends = this && this.__extends || function() {
      var extendStatics = function(d, b) {
        extendStatics = Object.setPrototypeOf || {
          __proto__: []
        } instanceof Array && function(d, b) {
          d.__proto__ = b;
        } || function(d, b) {
          for (var p in b) Object.prototype.hasOwnProperty.call(b, p) && (d[p] = b[p]);
        };
        return extendStatics(d, b);
      };
      return function(d, b) {
        extendStatics(d, b);
        function __() {
          this.constructor = d;
        }
        d.prototype = null === b ? Object.create(b) : (__.prototype = b.prototype, new __());
      };
    }();
    var __values = this && this.__values || function(o) {
      var s = "function" === typeof Symbol && Symbol.iterator, m = s && o[s], i = 0;
      if (m) return m.call(o);
      if (o && "number" === typeof o.length) return {
        next: function() {
          o && i >= o.length && (o = void 0);
          return {
            value: o && o[i++],
            done: !o
          };
        }
      };
      throw new TypeError(s ? "Object is not iterable." : "Symbol.iterator is not defined.");
    };
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    var Slot_DynamicPopUpManager_1 = require("../../SlotFramework/Common/DynamicPopUp/Slot_DynamicPopUpManager");
    var G53_DynamicPopUpPanelManager = function(_super) {
      __extends(G53_DynamicPopUpPanelManager, _super);
      function G53_DynamicPopUpPanelManager() {
        return null !== _super && _super.apply(this, arguments) || this;
      }
      G53_DynamicPopUpPanelManager.prototype.prestore = function(prefabs) {
        var e_1, _a;
        try {
          for (var prefabs_1 = __values(prefabs), prefabs_1_1 = prefabs_1.next(); !prefabs_1_1.done; prefabs_1_1 = prefabs_1.next()) {
            var prefab = prefabs_1_1.value;
            prefab.name;
            _super.prototype.prestore.call(this, [ prefab ]);
          }
        } catch (e_1_1) {
          e_1 = {
            error: e_1_1
          };
        } finally {
          try {
            prefabs_1_1 && !prefabs_1_1.done && (_a = prefabs_1.return) && _a.call(prefabs_1);
          } finally {
            if (e_1) throw e_1.error;
          }
        }
      };
      return G53_DynamicPopUpPanelManager;
    }(Slot_DynamicPopUpManager_1.default);
    exports.default = G53_DynamicPopUpPanelManager;
    cc._RF.pop();
  }, {
    "../../SlotFramework/Common/DynamicPopUp/Slot_DynamicPopUpManager": void 0
  } ],
  G53_FreeGameGetScorePanel: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "b61d5SlskZH+b/ZglG6rvuO", "G53_FreeGameGetScorePanel");
    "use strict";
    var __extends = this && this.__extends || function() {
      var extendStatics = function(d, b) {
        extendStatics = Object.setPrototypeOf || {
          __proto__: []
        } instanceof Array && function(d, b) {
          d.__proto__ = b;
        } || function(d, b) {
          for (var p in b) Object.prototype.hasOwnProperty.call(b, p) && (d[p] = b[p]);
        };
        return extendStatics(d, b);
      };
      return function(d, b) {
        extendStatics(d, b);
        function __() {
          this.constructor = d;
        }
        d.prototype = null === b ? Object.create(b) : (__.prototype = b.prototype, new __());
      };
    }();
    var __decorate = this && this.__decorate || function(decorators, target, key, desc) {
      var c = arguments.length, r = c < 3 ? target : null === desc ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
      if ("object" === typeof Reflect && "function" === typeof Reflect.decorate) r = Reflect.decorate(decorators, target, key, desc); else for (var i = decorators.length - 1; i >= 0; i--) (d = decorators[i]) && (r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r);
      return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    var Slot_FreeGameGetScorePanel_1 = require("../../../SlotFramework/Game/Panel/Slot_FreeGameGetScorePanel");
    var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
    var G53_FreeGameGetScorePanel = function(_super) {
      __extends(G53_FreeGameGetScorePanel, _super);
      function G53_FreeGameGetScorePanel() {
        var _this = null !== _super && _super.apply(this, arguments) || this;
        _this.anEnd = null;
        _this.chr = null;
        _this.endBG = null;
        return _this;
      }
      G53_FreeGameGetScorePanel.prototype.open = function(p_freeGameGetScore, p_betScore) {
        var _this = this;
        this.anEnd.getComponentsInChildren(cc.ParticleSystem3D).forEach(function(item) {
          item.play();
        });
        _super.prototype.open.call(this, p_freeGameGetScore, p_betScore);
        this.anEnd.play();
        this.chr.setAnimation(0, "end_start", false);
        this.chr.setCompleteListener(function() {
          _this.chr.setCompleteListener(null);
          _this.chr.setAnimation(0, "end_loop", true);
        });
        this.endBG.setAnimation(0, "Start", false);
        this.endBG.setCompleteListener(function() {
          _this.endBG.setCompleteListener(null);
          _this.endBG.setAnimation(0, "Loop", true);
        });
      };
      G53_FreeGameGetScorePanel.prototype.close = function() {
        var _this = this;
        this.scheduleOnce(function() {
          _this.anEnd.getComponentsInChildren(cc.ParticleSystem3D).forEach(function(item) {
            item.stop();
          });
        }, .25);
        return _super.prototype.close.call(this);
      };
      __decorate([ property(cc.Animation) ], G53_FreeGameGetScorePanel.prototype, "anEnd", void 0);
      __decorate([ property(sp.Skeleton) ], G53_FreeGameGetScorePanel.prototype, "chr", void 0);
      __decorate([ property(sp.Skeleton) ], G53_FreeGameGetScorePanel.prototype, "endBG", void 0);
      G53_FreeGameGetScorePanel = __decorate([ ccclass ], G53_FreeGameGetScorePanel);
      return G53_FreeGameGetScorePanel;
    }(Slot_FreeGameGetScorePanel_1.default);
    exports.default = G53_FreeGameGetScorePanel;
    cc._RF.pop();
  }, {
    "../../../SlotFramework/Game/Panel/Slot_FreeGameGetScorePanel": void 0
  } ],
  G53_FreeGamePanel: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "bb250liO0RDcbnKTw/olsEP", "G53_FreeGamePanel");
    "use strict";
    var __extends = this && this.__extends || function() {
      var extendStatics = function(d, b) {
        extendStatics = Object.setPrototypeOf || {
          __proto__: []
        } instanceof Array && function(d, b) {
          d.__proto__ = b;
        } || function(d, b) {
          for (var p in b) Object.prototype.hasOwnProperty.call(b, p) && (d[p] = b[p]);
        };
        return extendStatics(d, b);
      };
      return function(d, b) {
        extendStatics(d, b);
        function __() {
          this.constructor = d;
        }
        d.prototype = null === b ? Object.create(b) : (__.prototype = b.prototype, new __());
      };
    }();
    var __decorate = this && this.__decorate || function(decorators, target, key, desc) {
      var c = arguments.length, r = c < 3 ? target : null === desc ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
      if ("object" === typeof Reflect && "function" === typeof Reflect.decorate) r = Reflect.decorate(decorators, target, key, desc); else for (var i = decorators.length - 1; i >= 0; i--) (d = decorators[i]) && (r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r);
      return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    var Slot_FreeGamePanel_1 = require("../../../SlotFramework/Game/Panel/Slot_FreeGamePanel");
    var FXController_1 = require("../../../Common/Optional/Crosis/FXController/FXController");
    var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
    var G53_FreeGamePanel = function(_super) {
      __extends(G53_FreeGamePanel, _super);
      function G53_FreeGamePanel() {
        var _this = null !== _super && _super.apply(this, arguments) || this;
        _this.FX_FG_IN = null;
        return _this;
      }
      G53_FreeGamePanel.prototype.open = function(p_freeGameCount) {
        _super.prototype.open.call(this, p_freeGameCount);
        this.FX_FG_IN.play();
      };
      __decorate([ property(FXController_1.default) ], G53_FreeGamePanel.prototype, "FX_FG_IN", void 0);
      G53_FreeGamePanel = __decorate([ ccclass ], G53_FreeGamePanel);
      return G53_FreeGamePanel;
    }(Slot_FreeGamePanel_1.default);
    exports.default = G53_FreeGamePanel;
    cc._RF.pop();
  }, {
    "../../../Common/Optional/Crosis/FXController/FXController": void 0,
    "../../../SlotFramework/Game/Panel/Slot_FreeGamePanel": void 0
  } ],
  G53_GameUI: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "8011agYiX5APL3IUo97TndM", "G53_GameUI");
    "use strict";
    var __extends = this && this.__extends || function() {
      var extendStatics = function(d, b) {
        extendStatics = Object.setPrototypeOf || {
          __proto__: []
        } instanceof Array && function(d, b) {
          d.__proto__ = b;
        } || function(d, b) {
          for (var p in b) Object.prototype.hasOwnProperty.call(b, p) && (d[p] = b[p]);
        };
        return extendStatics(d, b);
      };
      return function(d, b) {
        extendStatics(d, b);
        function __() {
          this.constructor = d;
        }
        d.prototype = null === b ? Object.create(b) : (__.prototype = b.prototype, new __());
      };
    }();
    var __decorate = this && this.__decorate || function(decorators, target, key, desc) {
      var c = arguments.length, r = c < 3 ? target : null === desc ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
      if ("object" === typeof Reflect && "function" === typeof Reflect.decorate) r = Reflect.decorate(decorators, target, key, desc); else for (var i = decorators.length - 1; i >= 0; i--) (d = decorators[i]) && (r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r);
      return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __awaiter = this && this.__awaiter || function(thisArg, _arguments, P, generator) {
      function adopt(value) {
        return value instanceof P ? value : new P(function(resolve) {
          resolve(value);
        });
      }
      return new (P || (P = Promise))(function(resolve, reject) {
        function fulfilled(value) {
          try {
            step(generator.next(value));
          } catch (e) {
            reject(e);
          }
        }
        function rejected(value) {
          try {
            step(generator["throw"](value));
          } catch (e) {
            reject(e);
          }
        }
        function step(result) {
          result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected);
        }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
      });
    };
    var __generator = this && this.__generator || function(thisArg, body) {
      var _ = {
        label: 0,
        sent: function() {
          if (1 & t[0]) throw t[1];
          return t[1];
        },
        trys: [],
        ops: []
      }, f, y, t, g;
      return g = {
        next: verb(0),
        throw: verb(1),
        return: verb(2)
      }, "function" === typeof Symbol && (g[Symbol.iterator] = function() {
        return this;
      }), g;
      function verb(n) {
        return function(v) {
          return step([ n, v ]);
        };
      }
      function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
          if (f = 1, y && (t = 2 & op[0] ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 
          0) : y.next) && !(t = t.call(y, op[1])).done) return t;
          (y = 0, t) && (op = [ 2 & op[0], t.value ]);
          switch (op[0]) {
           case 0:
           case 1:
            t = op;
            break;

           case 4:
            _.label++;
            return {
              value: op[1],
              done: false
            };

           case 5:
            _.label++;
            y = op[1];
            op = [ 0 ];
            continue;

           case 7:
            op = _.ops.pop();
            _.trys.pop();
            continue;

           default:
            if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (6 === op[0] || 2 === op[0])) {
              _ = 0;
              continue;
            }
            if (3 === op[0] && (!t || op[1] > t[0] && op[1] < t[3])) {
              _.label = op[1];
              break;
            }
            if (6 === op[0] && _.label < t[1]) {
              _.label = t[1];
              t = op;
              break;
            }
            if (t && _.label < t[2]) {
              _.label = t[2];
              _.ops.push(op);
              break;
            }
            t[2] && _.ops.pop();
            _.trys.pop();
            continue;
          }
          op = body.call(thisArg, _);
        } catch (e) {
          op = [ 6, e ];
          y = 0;
        } finally {
          f = t = 0;
        }
        if (5 & op[0]) throw op[1];
        return {
          value: op[0] ? op[1] : void 0,
          done: true
        };
      }
    };
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    var FXController_1 = require("../../../Common/Optional/Crosis/FXController/FXController");
    var AudioManager_1 = require("../../../Common/Tools/AudioManager/AudioManager");
    var OrientationToolManager_1 = require("../../../Common/Tools/OrientationTool/OrientationToolManager");
    var Slot_GameUI_1 = require("../../../SlotFramework/Game/view/Slot_GameUI");
    var G53_DataManager_1 = require("../../Common/G53_DataManager");
    var G53_FreeGameGetScorePanel_1 = require("./G53_FreeGameGetScorePanel");
    var G53_FreeGamePanel_1 = require("./G53_FreeGamePanel");
    var G53_JackpotPanel_1 = require("./G53_JackpotPanel");
    var G53_SocketManager_1 = require("../../Common/Socket/G53_SocketManager");
    var G53_DynamicPopUpPanelManager_1 = require("../../Common/G53_DynamicPopUpPanelManager");
    var CommonTool_1 = require("../../../Common/Tools/CommonTool");
    var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
    var G53_GameUI = function(_super) {
      __extends(G53_GameUI, _super);
      function G53_GameUI() {
        var _this = null !== _super && _super.apply(this, arguments) || this;
        _this.freeGamePanel = null;
        _this.freeGameGetScorePanel = null;
        _this.baseGameBG = null;
        _this.freeGameBG = null;
        _this.tip6String = "\u4ee5\u4e0b\u70ba Menu Panel Buttons ";
        _this.exitGameButton = null;
        _this.openHistoryButton = null;
        _this.openRuleButton = null;
        _this.openMusicOptionsButton = null;
        _this.closeMenuButton = null;
        _this.copyVersionButton = null;
        _this.tip318String = "\u4ee5\u4e0b\u70ba G53 \u5ba2\u88fd\u5316\u90e8\u5206";
        _this.jackpotGrand = null;
        _this.jackpotMajor = null;
        _this.jackpotMinor = null;
        _this.jackpotMini = null;
        _this.jackpotPanel = null;
        _this.spinArrowH = null;
        _this.spinClickAni = null;
        _this.mainRewardWinEffect = null;
        _this.freeRewardWinEffect = null;
        _this.freeRewardWinEffect2 = null;
        _this.freeRewardWinSpine = null;
        _this.updateBroadEffect_F = null;
        _this.jackpotNode = null;
        _this.wildReward = null;
        _this.wildRewardMul = null;
        _this.wildRewardScore = null;
        _this.mulGroup = null;
        _this.mulLabelNode = null;
        _this.mulEndFX = null;
        _this.addFlyGroup = null;
        _this.addFlyNode = null;
        _this.AddFGFX2 = null;
        _this.offset = null;
        _this.autoSpinCountH = null;
        _this.isMenuOpen = false;
        _this.mulLabelPool = null;
        _this.addFlyPool = null;
        _this.isFreeGameCollctEffecting = false;
        _this.tempNode = [];
        _this.isFly = false;
        _this._menuClosePosition = cc.v3(0, -450, 0);
        _this._menuClosePositionHorizontal = cc.v3(450, 0, 0);
        return _this;
      }
      Object.defineProperty(G53_GameUI.prototype, "socket", {
        get: function() {
          return G53_SocketManager_1.default.getInstance();
        },
        enumerable: false,
        configurable: true
      });
      Object.defineProperty(G53_GameUI.prototype, "popup", {
        get: function() {
          return G53_DynamicPopUpPanelManager_1.default.getInstance();
        },
        enumerable: false,
        configurable: true
      });
      Object.defineProperty(G53_GameUI.prototype, "data", {
        get: function() {
          return G53_DataManager_1.default.getInstance();
        },
        enumerable: false,
        configurable: true
      });
      G53_GameUI.prototype.init = function(p_data) {
        _super.prototype.init.call(this, p_data);
        this.updateBroadEffect_F.node.active = false;
        this.freeGamePanel.init();
        this.freeGameGetScorePanel.init();
        this.historyPanel.init(this.data, this.popup, this.socket);
        this.webRulePanel.init(this.data, this.popup);
        this.jackpotPanel.init();
        this.wildReward.active = false;
        this.setSpinArrowSpinningSpeed(Slot_GameUI_1.SPIN_ARROW_SPINNIG_TYPE.NORMAL);
        this.updateScoreColor();
        this.changeToBaseGameBackGround();
        window["GameUI"] = this;
        this.mulLabelPool = new cc.NodePool();
        this.mulLabelPool.put(this.mulLabelNode);
        this.addFlyPool = new cc.NodePool();
        this.addFlyPool.put(this.addFlyNode);
      };
      G53_GameUI.prototype.onEnable = function() {
        cc.systemEvent.on(OrientationToolManager_1.ORIENTATION_EVENT.RESIZE, this.switchCanvas, this);
      };
      G53_GameUI.prototype.onDisable = function() {
        _super.prototype.onDisable.call(this);
        cc.systemEvent.off(OrientationToolManager_1.ORIENTATION_EVENT.RESIZE, this.switchCanvas, this);
      };
      G53_GameUI.prototype.closeMenu = function(p_isFast) {
        var _this = this;
        this.isMenuOpen = false;
        var pos = OrientationToolManager_1.default.orientationState == OrientationToolManager_1.ORIENTATION_TYPE.HORIZONTAL ? this._menuClosePositionHorizontal : this._menuClosePosition;
        if (p_isFast) {
          this.menu.setPosition(pos);
          this.menu.active = false;
        } else {
          cc.Tween.stopAllByTarget(this.menu);
          this.menu.setPosition(cc.Vec3.ZERO);
          cc.tween(this.menu).to(this.menuCloseTime, {
            position: pos
          }).call(function() {
            _this.menu.active = false;
          }).start();
        }
      };
      G53_GameUI.prototype.openMenu = function() {
        this.isMenuOpen = true;
        var pos = OrientationToolManager_1.default.orientationState == OrientationToolManager_1.ORIENTATION_TYPE.HORIZONTAL ? this._menuClosePositionHorizontal : this._menuClosePosition;
        cc.Tween.stopAllByTarget(this.menu);
        this.menu.setPosition(pos);
        this.menu.active = true;
        cc.tween(this.menu).to(this.menuOpenTime, {
          position: cc.Vec3.ZERO
        }).start();
      };
      G53_GameUI.prototype.changeToBaseGameBackGround = function() {
        this.baseGameBG.active = true;
        this.freeGameBG.active = false;
        this.setBackBtnState(true);
        this.setOffset();
      };
      G53_GameUI.prototype.changeToFreeGameBackGround = function() {
        this.baseGameBG.active = false;
        this.freeGameBG.active = true;
        this.setBackBtnState(false);
        this.setOffset();
      };
      G53_GameUI.prototype.openFreeGamePanel = function(p_freeGameCount) {
        this.freeGamePanel.open(p_freeGameCount);
      };
      G53_GameUI.prototype.closeFreeGamePanel = function() {
        return __awaiter(this, void 0, Promise, function() {
          return __generator(this, function(_a) {
            this.freeGamePanel.close();
            return [ 2 ];
          });
        });
      };
      G53_GameUI.prototype.openFreeGameGetScorePanel = function(p_freeGameGetScore, p_betScore) {
        this.freeGameGetScorePanel.open(p_freeGameGetScore, p_betScore);
      };
      G53_GameUI.prototype.closeFreeGameGetScorePanel = function() {
        return this.freeGameGetScorePanel.close();
      };
      G53_GameUI.prototype.showSpinButtonClickEffect = function() {
        this.spinClickAni.node.active = true;
        this.spinClickAni.play();
      };
      G53_GameUI.prototype.setSpinArrowSpinningSpeed = function(p_type) {
        this.spinArrow.stopAllActions();
        this.spinArrowH.stopAllActions();
        if (p_type == Slot_GameUI_1.SPIN_ARROW_SPINNIG_TYPE.STOP) ; else if (p_type == Slot_GameUI_1.SPIN_ARROW_SPINNIG_TYPE.NORMAL) {
          cc.tween(this.spinArrow).by(this.spinArrowRotateDuration_Normal, {
            angle: -360
          }).repeatForever().start();
          cc.tween(this.spinArrowH).by(this.spinArrowRotateDuration_Normal, {
            angle: -360
          }).repeatForever().start();
        } else {
          cc.tween(this.spinArrow).by(this.spinArrowRotateDuration_Fast, {
            angle: -360
          }).repeatForever().start();
          cc.tween(this.spinArrowH).by(this.spinArrowRotateDuration_Fast, {
            angle: -360
          }).repeatForever().start();
        }
      };
      G53_GameUI.prototype.showTotalGetScore = function(p_totalGetScore) {
        _super.prototype.showTotalGetScore.call(this, p_totalGetScore);
        this.updateBroadEffect_F.node.active = true;
        this.updateBroadEffect_F.play();
      };
      G53_GameUI.prototype.hideGetScoreTip = function() {
        _super.prototype.hideGetScoreTip.call(this);
        this.updateBroadEffect.node.active = false;
        this.updateBroadEffect_F.stop();
        this.updateBroadEffect_F.node.active = false;
      };
      G53_GameUI.prototype.setAutoSpinCount = function(p_autoSpinCount) {
        _super.prototype.setAutoSpinCount.call(this, p_autoSpinCount);
        if (-1 == p_autoSpinCount) {
          this.autoSpinCount.node.setScale(2);
          this.autoSpinCountH.string = "=";
        } else {
          this.autoSpinCount.node.setScale(1);
          this.autoSpinCountH.string = p_autoSpinCount.toString();
        }
      };
      G53_GameUI.prototype.switchCanvas = function() {
        var _this = this;
        this.setMenu();
        this.setOffset();
        this.updateScoreColor();
        this.scheduleOnce(function() {
          _this.resetMulLbl();
        });
      };
      G53_GameUI.prototype.setMenu = function() {
        cc.Tween.stopAllByTarget(this.menu);
        var pos = OrientationToolManager_1.default.orientationState == OrientationToolManager_1.ORIENTATION_TYPE.HORIZONTAL ? this._menuClosePositionHorizontal : this._menuClosePosition;
        this.isMenuOpen ? this.menu.setPosition(cc.Vec3.ZERO) : this.menu.setPosition(pos);
        this.menu.active = this.isMenuOpen;
      };
      G53_GameUI.prototype.setOffset = function() {
        this.jackpotNode.y = this.backButton.active || OrientationToolManager_1.default.orientationState != OrientationToolManager_1.ORIENTATION_TYPE.HORIZONTAL ? 0 : 100;
        this.offset.y = this.backButton.active || OrientationToolManager_1.default.orientationState != OrientationToolManager_1.ORIENTATION_TYPE.VERTICAL ? 0 : -60;
      };
      G53_GameUI.prototype.refreshJackpot = function(data) {
        var _this = this;
        data.jackpots.forEach(function(item) {
          switch (item.name) {
           case "grand":
            var grandScore = G53_DataManager_1.default.getInstance().getClientMoney(item.bonus);
            _this.jackpotGrand.string = CommonTool_1.CommonTool.getNumText(grandScore, 2, true, false);
            break;

           case "major":
            var majorScore = G53_DataManager_1.default.getInstance().getClientMoney(item.bonus);
            _this.jackpotMajor.string = CommonTool_1.CommonTool.getNumText(majorScore, 2, true, false);
            break;

           case "minor":
            var minorScore = G53_DataManager_1.default.getInstance().getClientMoney(item.bonus);
            _this.jackpotMinor.string = CommonTool_1.CommonTool.getNumText(minorScore, 2, true, false);
            break;

           case "mini":
            var miniScore = G53_DataManager_1.default.getInstance().getClientMoney(item.bonus);
            _this.jackpotMini.string = CommonTool_1.CommonTool.getNumText(miniScore, 2, true, false);
          }
        });
      };
      G53_GameUI.prototype.openJackpotPanel = function(p_freeGameGetScore, level) {
        this.jackpotPanel.open(p_freeGameGetScore, level);
      };
      G53_GameUI.prototype.closeJackpotPanel = function() {
        this.jackpotPanel.close();
      };
      G53_GameUI.prototype.orientationAddandReduceSetParent = function() {
        if (OrientationToolManager_1.default.orientationState == OrientationToolManager_1.ORIENTATION_TYPE.HORIZONTAL) {
          this.spinMask.active && this.scoreCtrl(false);
          this.AddScoreButton.setParent(this.offset);
          this.ReduceScoreButton.setParent(this.offset);
        } else {
          this.scoreCtrl(true);
          this.AddScoreButton.setParent(this.buttonGroup);
          this.ReduceScoreButton.setParent(this.buttonGroup);
          this.AddScoreButton.setSiblingIndex(0);
          this.ReduceScoreButton.setSiblingIndex(0);
        }
      };
      G53_GameUI.prototype.playMainRewardWinEffect = function() {
        var _this = this;
        this.mainRewardWinEffect.node.active = true;
        this.mainRewardWinEffect.play();
        this.mainRewardWinEffect.once("finished", function() {
          _this.mainRewardWinEffect.node.active = false;
        });
      };
      G53_GameUI.prototype.playFreeRewardWinEffect = function() {
        var _this = this;
        this.freeRewardWinEffect.node.active = true;
        this.freeRewardWinEffect.play();
        this.freeRewardWinEffect2.play();
        this.freeRewardWinEffect.once("finished", function() {
          _this.freeRewardWinEffect.node.active = false;
        });
        this.freeRewardWinSpine.setAnimation(0, "fg_win", false);
        this.freeRewardWinSpine.setCompleteListener(function() {
          _this.freeRewardWinSpine.setCompleteListener(null);
          _this.freeRewardWinSpine.setAnimation(0, "fg_normal", true);
        });
      };
      G53_GameUI.prototype.updateScoreColor = function() {
        this.score.node.children[0].color = this.score.node.color;
        this.winScore.node.children[0].color = this.winScore.node.color;
      };
      G53_GameUI.prototype.showWildReward = function(score, mul, hasMul, close) {
        var _this = this;
        void 0 === close && (close = true);
        this.wildRewardMul.node.active = hasMul;
        this.wildRewardMul.string = "*" + mul;
        this.wildRewardScore.string = CommonTool_1.CommonTool.getNumText(score, 2, true, false);
        this.wildReward.active = true;
        close && this.scheduleOnce(function() {
          _this.wildReward.active = false;
        }, .5);
      };
      G53_GameUI.prototype.resetMulLbl = function() {
        var _this = this;
        this.tempNode.forEach(function(item, index) {
          if (!_this.isFly) {
            var oriPos = item.parent.convertToWorldSpaceAR(item.position);
            oriPos = _this.mulGroup.convertToNodeSpaceAR(oriPos);
            oriPos.y += 34;
            _this.mulGroup.children[index].position = oriPos;
          }
        });
      };
      G53_GameUI.prototype.setMulLbl = function(winPosList) {
        var _this = this;
        this.isFly = false;
        winPosList.forEach(function(winPos) {
          var mul = winPos.mul;
          var index = _this.tempNode.indexOf(winPos.node);
          if (-1 != index) {
            var lbl = _this.mulGroup.children[index].getComponentInChildren(cc.Label);
            var num = parseInt(lbl.string.slice(1));
            lbl.string = "*" + (mul + num);
          } else {
            _this.tempNode.push(winPos.node);
            var oriPos = winPos.node.parent.convertToWorldSpaceAR(winPos.node.position);
            oriPos = _this.mulGroup.convertToNodeSpaceAR(oriPos);
            oriPos.y += 34;
            var mulLabel = null;
            mulLabel = _this.mulLabelPool.size() < 1 ? cc.instantiate(_this.mulLabelNode) : _this.mulLabelPool.get();
            mulLabel.position = oriPos;
            mulLabel.getComponentInChildren(cc.Label).string = "*" + mul;
            mulLabel.active = true;
            _this.mulGroup.addChild(mulLabel);
          }
        });
      };
      G53_GameUI.prototype.showMulFly = function() {
        var _this = this;
        AudioManager_1.AudioManager.instance.playAudioEvent("ShowMulFly");
        this.mulEndFX.play();
        this.mulGroup.children.forEach(function(lbl) {
          var targetPos = _this.wildRewardMul.node.parent.convertToWorldSpaceAR(_this.wildRewardMul.node.position);
          targetPos = _this.mulGroup.convertToNodeSpaceAR(targetPos);
          cc.tween(lbl).call(function() {
            lbl.getComponent(FXController_1.default).play();
            _this.isFly = true;
          }).to(.3, {
            position: targetPos
          }).call(function() {
            lbl.getComponentInChildren(cc.ParticleSystem3D).stop();
            _this.mulLabelPool.put(lbl);
            _this.tempNode = [];
          }).start();
        });
      };
      G53_GameUI.prototype.showFreeGameCollctEffect = function(parent, winPosList, remainingFreeGameCount) {
        var _this = this;
        AudioManager_1.AudioManager.instance.playAudioEvent("FreeGameFireBall");
        cc.tween(this.node).call(function() {
          _this.isFreeGameCollctEffecting = true;
        }).delay(.3).call(function() {
          winPosList.forEach(function(winPos) {
            var oriPos = parent.convertToWorldSpaceAR(winPos);
            oriPos = _this.addFlyGroup.convertToNodeSpaceAR(oriPos);
            var targetPos = _this.remainingFreeGameCount.node.parent.convertToWorldSpaceAR(_this.remainingFreeGameCount.node.position);
            targetPos = _this.addFlyGroup.convertToNodeSpaceAR(targetPos);
            var flyNode = null;
            flyNode = _this.addFlyPool.size() < 1 ? cc.instantiate(_this.addFlyNode) : _this.addFlyPool.get();
            flyNode.position = oriPos;
            _this.addFlyGroup.addChild(flyNode);
            cc.tween(flyNode).call(function() {
              flyNode.getComponent(FXController_1.default).play();
            }).to(.5, {
              position: targetPos
            }).call(function() {
              _this.AddFGFX2.play();
            }).delay(.3).call(function() {
              _this.setRemainingFreeGameCount(remainingFreeGameCount + 6);
              _this.addFlyPool.put(flyNode);
            }).start();
          });
        }).delay(1).call(function() {
          _this.isFreeGameCollctEffecting = false;
        }).start();
      };
      __decorate([ property(G53_FreeGamePanel_1.default) ], G53_GameUI.prototype, "freeGamePanel", void 0);
      __decorate([ property(G53_FreeGameGetScorePanel_1.default) ], G53_GameUI.prototype, "freeGameGetScorePanel", void 0);
      __decorate([ property(cc.Node) ], G53_GameUI.prototype, "baseGameBG", void 0);
      __decorate([ property(cc.Node) ], G53_GameUI.prototype, "freeGameBG", void 0);
      __decorate([ property({
        readonly: true,
        displayName: "========================"
      }) ], G53_GameUI.prototype, "tip6String", void 0);
      __decorate([ property(cc.Node) ], G53_GameUI.prototype, "exitGameButton", void 0);
      __decorate([ property(cc.Node) ], G53_GameUI.prototype, "openHistoryButton", void 0);
      __decorate([ property(cc.Node) ], G53_GameUI.prototype, "openRuleButton", void 0);
      __decorate([ property(cc.Node) ], G53_GameUI.prototype, "openMusicOptionsButton", void 0);
      __decorate([ property(cc.Node) ], G53_GameUI.prototype, "closeMenuButton", void 0);
      __decorate([ property(cc.Node) ], G53_GameUI.prototype, "copyVersionButton", void 0);
      __decorate([ property({
        readonly: true,
        displayName: "========================"
      }) ], G53_GameUI.prototype, "tip318String", void 0);
      __decorate([ property(cc.Label) ], G53_GameUI.prototype, "jackpotGrand", void 0);
      __decorate([ property(cc.Label) ], G53_GameUI.prototype, "jackpotMajor", void 0);
      __decorate([ property(cc.Label) ], G53_GameUI.prototype, "jackpotMinor", void 0);
      __decorate([ property(cc.Label) ], G53_GameUI.prototype, "jackpotMini", void 0);
      __decorate([ property(G53_JackpotPanel_1.default) ], G53_GameUI.prototype, "jackpotPanel", void 0);
      __decorate([ property(cc.Node) ], G53_GameUI.prototype, "spinArrowH", void 0);
      __decorate([ property(cc.Animation) ], G53_GameUI.prototype, "spinClickAni", void 0);
      __decorate([ property(cc.Animation) ], G53_GameUI.prototype, "mainRewardWinEffect", void 0);
      __decorate([ property(cc.Animation) ], G53_GameUI.prototype, "freeRewardWinEffect", void 0);
      __decorate([ property(cc.Animation) ], G53_GameUI.prototype, "freeRewardWinEffect2", void 0);
      __decorate([ property(sp.Skeleton) ], G53_GameUI.prototype, "freeRewardWinSpine", void 0);
      __decorate([ property(cc.Animation) ], G53_GameUI.prototype, "updateBroadEffect_F", void 0);
      __decorate([ property(cc.Node) ], G53_GameUI.prototype, "jackpotNode", void 0);
      __decorate([ property(cc.Node) ], G53_GameUI.prototype, "wildReward", void 0);
      __decorate([ property(cc.Label) ], G53_GameUI.prototype, "wildRewardMul", void 0);
      __decorate([ property(cc.Label) ], G53_GameUI.prototype, "wildRewardScore", void 0);
      __decorate([ property(cc.Node) ], G53_GameUI.prototype, "mulGroup", void 0);
      __decorate([ property(cc.Node) ], G53_GameUI.prototype, "mulLabelNode", void 0);
      __decorate([ property(FXController_1.default) ], G53_GameUI.prototype, "mulEndFX", void 0);
      __decorate([ property(cc.Node) ], G53_GameUI.prototype, "addFlyGroup", void 0);
      __decorate([ property(cc.Node) ], G53_GameUI.prototype, "addFlyNode", void 0);
      __decorate([ property(cc.Animation) ], G53_GameUI.prototype, "AddFGFX2", void 0);
      __decorate([ property(cc.Node) ], G53_GameUI.prototype, "offset", void 0);
      __decorate([ property(cc.Label) ], G53_GameUI.prototype, "autoSpinCountH", void 0);
      G53_GameUI = __decorate([ ccclass ], G53_GameUI);
      return G53_GameUI;
    }(Slot_GameUI_1.default);
    exports.default = G53_GameUI;
    cc._RF.pop();
  }, {
    "../../../Common/Optional/Crosis/FXController/FXController": void 0,
    "../../../Common/Tools/AudioManager/AudioManager": void 0,
    "../../../Common/Tools/CommonTool": void 0,
    "../../../Common/Tools/OrientationTool/OrientationToolManager": void 0,
    "../../../SlotFramework/Game/view/Slot_GameUI": void 0,
    "../../Common/G53_DataManager": "G53_DataManager",
    "../../Common/G53_DynamicPopUpPanelManager": "G53_DynamicPopUpPanelManager",
    "../../Common/Socket/G53_SocketManager": "G53_SocketManager",
    "./G53_FreeGameGetScorePanel": "G53_FreeGameGetScorePanel",
    "./G53_FreeGamePanel": "G53_FreeGamePanel",
    "./G53_JackpotPanel": "G53_JackpotPanel"
  } ],
  G53_Game: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "8d16dzuMKJNmpMic9VUHsK/", "G53_Game");
    "use strict";
    var __extends = this && this.__extends || function() {
      var extendStatics = function(d, b) {
        extendStatics = Object.setPrototypeOf || {
          __proto__: []
        } instanceof Array && function(d, b) {
          d.__proto__ = b;
        } || function(d, b) {
          for (var p in b) Object.prototype.hasOwnProperty.call(b, p) && (d[p] = b[p]);
        };
        return extendStatics(d, b);
      };
      return function(d, b) {
        extendStatics(d, b);
        function __() {
          this.constructor = d;
        }
        d.prototype = null === b ? Object.create(b) : (__.prototype = b.prototype, new __());
      };
    }();
    var __decorate = this && this.__decorate || function(decorators, target, key, desc) {
      var c = arguments.length, r = c < 3 ? target : null === desc ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
      if ("object" === typeof Reflect && "function" === typeof Reflect.decorate) r = Reflect.decorate(decorators, target, key, desc); else for (var i = decorators.length - 1; i >= 0; i--) (d = decorators[i]) && (r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r);
      return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __awaiter = this && this.__awaiter || function(thisArg, _arguments, P, generator) {
      function adopt(value) {
        return value instanceof P ? value : new P(function(resolve) {
          resolve(value);
        });
      }
      return new (P || (P = Promise))(function(resolve, reject) {
        function fulfilled(value) {
          try {
            step(generator.next(value));
          } catch (e) {
            reject(e);
          }
        }
        function rejected(value) {
          try {
            step(generator["throw"](value));
          } catch (e) {
            reject(e);
          }
        }
        function step(result) {
          result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected);
        }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
      });
    };
    var __generator = this && this.__generator || function(thisArg, body) {
      var _ = {
        label: 0,
        sent: function() {
          if (1 & t[0]) throw t[1];
          return t[1];
        },
        trys: [],
        ops: []
      }, f, y, t, g;
      return g = {
        next: verb(0),
        throw: verb(1),
        return: verb(2)
      }, "function" === typeof Symbol && (g[Symbol.iterator] = function() {
        return this;
      }), g;
      function verb(n) {
        return function(v) {
          return step([ n, v ]);
        };
      }
      function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
          if (f = 1, y && (t = 2 & op[0] ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 
          0) : y.next) && !(t = t.call(y, op[1])).done) return t;
          (y = 0, t) && (op = [ 2 & op[0], t.value ]);
          switch (op[0]) {
           case 0:
           case 1:
            t = op;
            break;

           case 4:
            _.label++;
            return {
              value: op[1],
              done: false
            };

           case 5:
            _.label++;
            y = op[1];
            op = [ 0 ];
            continue;

           case 7:
            op = _.ops.pop();
            _.trys.pop();
            continue;

           default:
            if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (6 === op[0] || 2 === op[0])) {
              _ = 0;
              continue;
            }
            if (3 === op[0] && (!t || op[1] > t[0] && op[1] < t[3])) {
              _.label = op[1];
              break;
            }
            if (6 === op[0] && _.label < t[1]) {
              _.label = t[1];
              t = op;
              break;
            }
            if (t && _.label < t[2]) {
              _.label = t[2];
              _.ops.push(op);
              break;
            }
            t[2] && _.ops.pop();
            _.trys.pop();
            continue;
          }
          op = body.call(thisArg, _);
        } catch (e) {
          op = [ 6, e ];
          y = 0;
        } finally {
          f = t = 0;
        }
        if (5 & op[0]) throw op[1];
        return {
          value: op[0] ? op[1] : void 0,
          done: true
        };
      }
    };
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    var AudioManager_1 = require("../../Common/Tools/AudioManager/AudioManager");
    var LocalizationManager_1 = require("../../Common/Tools/Localization/LocalizationManager");
    var Slot_GameManager_InitData_1 = require("../../SlotFramework/Game/Data/Slot_GameManager_InitData");
    var Slot_GameUI_InitData_1 = require("../../SlotFramework/Game/Data/Slot_GameUI_InitData");
    var Slot_ReelManager_InitData_1 = require("../../SlotFramework/Game/Data/Slot_ReelManager_InitData");
    var Slot_ReelManager_ScrollData_1 = require("../../SlotFramework/Game/Data/Slot_ReelManager_ScrollData");
    var Slot_GameManager_1 = require("../../SlotFramework/Game/Slot_GameManager");
    var Slot_GameUI_1 = require("../../SlotFramework/Game/view/Slot_GameUI");
    var G53_DataManager_1 = require("../Common/G53_DataManager");
    var G53_DynamicPopUpPanelManager_1 = require("../Common/G53_DynamicPopUpPanelManager");
    var G53_SocketManager_1 = require("../Common/Socket/G53_SocketManager");
    var G53_GameUI_1 = require("./View/G53_GameUI");
    var G53_SlotReelManager_1 = require("./View/G53_SlotReelManager");
    var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
    var GAME_STATUS;
    (function(GAME_STATUS) {
      GAME_STATUS[GAME_STATUS["NONE"] = 0] = "NONE";
      GAME_STATUS[GAME_STATUS["IDLE"] = 1] = "IDLE";
      GAME_STATUS[GAME_STATUS["SERVER_REQUEST"] = 2] = "SERVER_REQUEST";
      GAME_STATUS[GAME_STATUS["SPIN"] = 3] = "SPIN";
      GAME_STATUS[GAME_STATUS["FREE_GAME_TIP"] = 4] = "FREE_GAME_TIP";
      GAME_STATUS[GAME_STATUS["FREE_GAME"] = 5] = "FREE_GAME";
      GAME_STATUS[GAME_STATUS["FREE_GAME_REWARD"] = 6] = "FREE_GAME_REWARD";
      GAME_STATUS[GAME_STATUS["FREE_GAME_WIN_SCORE_EFFECT"] = 7] = "FREE_GAME_WIN_SCORE_EFFECT";
      GAME_STATUS[GAME_STATUS["CHANGE_PANEL"] = 8] = "CHANGE_PANEL";
      GAME_STATUS[GAME_STATUS["THROW_SYMBOL"] = 9] = "THROW_SYMBOL";
    })(GAME_STATUS || (GAME_STATUS = {}));
    var FREEGAME_WIN_SCORE_EFFECT_STATUS;
    (function(FREEGAME_WIN_SCORE_EFFECT_STATUS) {
      FREEGAME_WIN_SCORE_EFFECT_STATUS[FREEGAME_WIN_SCORE_EFFECT_STATUS["NONE"] = 0] = "NONE";
      FREEGAME_WIN_SCORE_EFFECT_STATUS[FREEGAME_WIN_SCORE_EFFECT_STATUS["WIN_SCORE_EFFECT_START"] = 1] = "WIN_SCORE_EFFECT_START";
      FREEGAME_WIN_SCORE_EFFECT_STATUS[FREEGAME_WIN_SCORE_EFFECT_STATUS["WIN_SCORE_EFFECT_ING"] = 2] = "WIN_SCORE_EFFECT_ING";
      FREEGAME_WIN_SCORE_EFFECT_STATUS[FREEGAME_WIN_SCORE_EFFECT_STATUS["WIN_SCORE_EFFECT_END"] = 3] = "WIN_SCORE_EFFECT_END";
    })(FREEGAME_WIN_SCORE_EFFECT_STATUS || (FREEGAME_WIN_SCORE_EFFECT_STATUS = {}));
    var SPIN_STATUS;
    (function(SPIN_STATUS) {
      SPIN_STATUS[SPIN_STATUS["NONE"] = 0] = "NONE";
      SPIN_STATUS[SPIN_STATUS["REEL_SCROLL_START"] = 1] = "REEL_SCROLL_START";
      SPIN_STATUS[SPIN_STATUS["REEL_SCROLL_ING"] = 2] = "REEL_SCROLL_ING";
      SPIN_STATUS[SPIN_STATUS["REEL_SCROLL_END"] = 3] = "REEL_SCROLL_END";
      SPIN_STATUS[SPIN_STATUS["CHECK_CHANGE_SYMBOL"] = 4] = "CHECK_CHANGE_SYMBOL";
      SPIN_STATUS[SPIN_STATUS["CHANGE_SYMBOL_START"] = 5] = "CHANGE_SYMBOL_START";
      SPIN_STATUS[SPIN_STATUS["CHANGE_SYMBOL_ING"] = 6] = "CHANGE_SYMBOL_ING";
      SPIN_STATUS[SPIN_STATUS["CHANGE_SYMBOL_END"] = 7] = "CHANGE_SYMBOL_END";
      SPIN_STATUS[SPIN_STATUS["CHECK_REWARD"] = 8] = "CHECK_REWARD";
      SPIN_STATUS[SPIN_STATUS["JACKPOT_WAIT"] = 9] = "JACKPOT_WAIT";
      SPIN_STATUS[SPIN_STATUS["JACKPOT_START"] = 10] = "JACKPOT_START";
      SPIN_STATUS[SPIN_STATUS["JACKPOT_ING"] = 11] = "JACKPOT_ING";
      SPIN_STATUS[SPIN_STATUS["REWARD_EFFECT_START"] = 12] = "REWARD_EFFECT_START";
      SPIN_STATUS[SPIN_STATUS["REWARD_EFFECT_ING"] = 13] = "REWARD_EFFECT_ING";
      SPIN_STATUS[SPIN_STATUS["REWARD_EFFECT_END"] = 14] = "REWARD_EFFECT_END";
      SPIN_STATUS[SPIN_STATUS["WIN_SCORE_EFFECT_START"] = 15] = "WIN_SCORE_EFFECT_START";
      SPIN_STATUS[SPIN_STATUS["WIN_SCORE_EFFECT_ING"] = 16] = "WIN_SCORE_EFFECT_ING";
      SPIN_STATUS[SPIN_STATUS["WIN_SCORE_EFFECT_END"] = 17] = "WIN_SCORE_EFFECT_END";
      SPIN_STATUS[SPIN_STATUS["FINISHED_WAIT"] = 18] = "FINISHED_WAIT";
      SPIN_STATUS[SPIN_STATUS["FINISHED"] = 19] = "FINISHED";
      SPIN_STATUS[SPIN_STATUS["SYMBOL_DISAPPEAR_START"] = 20] = "SYMBOL_DISAPPEAR_START";
      SPIN_STATUS[SPIN_STATUS["SYMBOL_DISAPPEAR_ING"] = 21] = "SYMBOL_DISAPPEAR_ING";
      SPIN_STATUS[SPIN_STATUS["SYMBOL_DISAPPEAR_END"] = 22] = "SYMBOL_DISAPPEAR_END";
      SPIN_STATUS[SPIN_STATUS["SYMBOL_DISAPPEAR_DROP_START"] = 23] = "SYMBOL_DISAPPEAR_DROP_START";
      SPIN_STATUS[SPIN_STATUS["SYMBOL_DISAPPEAR_DROP_ING"] = 24] = "SYMBOL_DISAPPEAR_DROP_ING";
      SPIN_STATUS[SPIN_STATUS["SYMBOL_DISAPPEAR_DROP_END"] = 25] = "SYMBOL_DISAPPEAR_DROP_END";
      SPIN_STATUS[SPIN_STATUS["SHOW_SCORE"] = 26] = "SHOW_SCORE";
      SPIN_STATUS[SPIN_STATUS["SCATTER_WIN_START"] = 27] = "SCATTER_WIN_START";
      SPIN_STATUS[SPIN_STATUS["SCATTER_WIN_ING"] = 28] = "SCATTER_WIN_ING";
      SPIN_STATUS[SPIN_STATUS["SCATTER_WIN_END"] = 29] = "SCATTER_WIN_END";
      SPIN_STATUS[SPIN_STATUS["FREE_GAME_COLLECT_START"] = 30] = "FREE_GAME_COLLECT_START";
      SPIN_STATUS[SPIN_STATUS["FREE_GAME_COLLECT_ING"] = 31] = "FREE_GAME_COLLECT_ING";
    })(SPIN_STATUS || (SPIN_STATUS = {}));
    var G53_Game = function(_super) {
      __extends(G53_Game, _super);
      function G53_Game() {
        var _this = null !== _super && _super.apply(this, arguments) || this;
        _this.slotReelManager = null;
        _this.firstWinLineChangeTime = .4;
        _this.winLineChangeTime = 2;
        _this.winLineChangeWaitTime = .1;
        _this.ui = null;
        _this.spinCD = .5;
        _this.normalSpinFinishedStopTime = .5;
        _this.fastSpinFinishedStopTime = .2;
        _this.normalUpdateTipBroadEffectTime = .1;
        _this.autoSpinUpdateTipBroadEffectTime = .9;
        _this.normalRewardTime = 1;
        _this.autoRewardTime = 1;
        _this.scatterShowTime = 3;
        _this.data = null;
        _this.socket = null;
        _this.popup = null;
        _this.gameStatus = GAME_STATUS.NONE;
        _this.spinStatus = SPIN_STATUS.NONE;
        _this.freeGameWinScoreEffectStatus = FREEGAME_WIN_SCORE_EFFECT_STATUS.NONE;
        _this.eliminateListIndex = 0;
        _this.waitRewardTime = 0;
        _this.competitionStartFontSize1 = 30;
        _this.competitionStartFontColor1 = "#3A1304";
        _this.competitionStartFontSize2 = 30;
        _this.competitionStartFontColor2 = "#DC5050";
        return _this;
      }
      G53_Game.prototype.onLoad = function() {
        this.ui.openHistoryButton.on("click", this.onOpenHistory, this);
        this.ui.openRuleButton.on("click", this.onOpenRule, this);
        this.ui.openMusicOptionsButton.on("click", this.onOpenMusicOptionPanel, this);
        this.ui.closeMenuButton.on("click", this.onCloseMenu, this);
        this.ui.copyVersionButton.on("click", this.OnCopyVersion, this);
        this.ui.exitGameButton.on("click", this.onExitGame, this);
      };
      G53_Game.prototype.start = function() {
        var _this = this;
        this.socket = G53_SocketManager_1.default.getInstance();
        this.data = G53_DataManager_1.default.getInstance();
        this.popup = G53_DynamicPopUpPanelManager_1.default.getInstance();
        _super.prototype.start.call(this);
        var _data = new Slot_GameManager_InitData_1.default();
        _data.bundleName = "G" + G53_DataManager_1.default.getInstance().gameID;
        _data.bundlePath = "" + G53_DataManager_1.default.getInstance().path;
        this.init(_data);
        this.socket.setOnGetInitialReelInfoEvent().then(function(p_data) {
          _this.showResponseLog("GetInitialReelInfo", p_data);
          _this.setReelInfo(p_data);
          _this.playerLogin();
          _this.gameStatus = GAME_STATUS.IDLE;
        });
        true;
        window["Game"] = this;
      };
      G53_Game.prototype.init = function(p_data) {
        this.data = G53_DataManager_1.default.getInstance();
        this.data.init();
        var _data = new Slot_ReelManager_InitData_1.default();
        _data.bundleName = p_data.bundleName;
        _data.bundlePath = p_data.bundlePath;
        this.slotReelManager.init(_data);
        _super.prototype.init.call(this, p_data);
        var _uiData = new Slot_GameUI_InitData_1.default();
        _uiData.bundleName = p_data.bundleName;
        _uiData.bundlePath = p_data.bundlePath;
        _uiData.dataManager = this.data;
        this.ui.init(_uiData);
        this.ui.setVersion(this.data.gameVersion);
        this.ui.setRemainingFreeGameCount(0);
        this.registerEvents();
        this.data.jpData && this.ui.refreshJackpot(this.data.jpData);
        AudioManager_1.AudioManager.instance.playAudioEvent("GameInit");
      };
      G53_Game.prototype.onDisable = function() {
        _super.prototype.onDisable.call(this);
        AudioManager_1.AudioManager.instance.playAudioEvent("GameExit");
      };
      G53_Game.prototype.update = function(dt) {
        if (this.toFastStopSpin && this.tempSpinCD > this.spinCD) {
          this.stopSpin();
          this.toFastStopSpin = false;
        }
        if (this.gameStatus == GAME_STATUS.SPIN || this.gameStatus == GAME_STATUS.FREE_GAME) {
          this.tempSpinCD += dt;
          switch (this.spinStatus) {
           case SPIN_STATUS.REEL_SCROLL_START:
            this.reelScrollStart();
            break;

           case SPIN_STATUS.REEL_SCROLL_ING:
            this.reelScrolling();
            break;

           case SPIN_STATUS.REEL_SCROLL_END:
            this.reelScrollEnd();
            break;

           case SPIN_STATUS.CHECK_CHANGE_SYMBOL:
            this.checkChangeSymbol();
            break;

           case SPIN_STATUS.CHANGE_SYMBOL_START:
            this.changeSymbolStart();
            break;

           case SPIN_STATUS.CHANGE_SYMBOL_ING:
            this.changeSymbolIng();
            break;

           case SPIN_STATUS.CHANGE_SYMBOL_END:
            this.changeSymbolEnd();
            break;

           case SPIN_STATUS.CHECK_REWARD:
            this.checkReward();
            break;

           case SPIN_STATUS.JACKPOT_START:
            this.getJackpotStart();
            break;

           case SPIN_STATUS.JACKPOT_ING:
            this.getJackpotIng();
            break;

           case SPIN_STATUS.REWARD_EFFECT_START:
            this.rewardEffectStart();
            break;

           case SPIN_STATUS.REWARD_EFFECT_ING:
            this.rewardEffecting();
            break;

           case SPIN_STATUS.REWARD_EFFECT_END:
            this.rewardEffectEnd();
            break;

           case SPIN_STATUS.WIN_SCORE_EFFECT_START:
            this.winScoreEffectStart();
            break;

           case SPIN_STATUS.WIN_SCORE_EFFECT_ING:
            this.winScoreEffectIng();
            break;

           case SPIN_STATUS.WIN_SCORE_EFFECT_END:
            this.winScoreEffectEnd();
            break;

           case SPIN_STATUS.FINISHED_WAIT:
            this.spinFinishedWait(dt);
            break;

           case SPIN_STATUS.FINISHED:
            this.spinFinished();
            break;

           case SPIN_STATUS.SYMBOL_DISAPPEAR_START:
            this.symbolDisappearStart();
            break;

           case SPIN_STATUS.SYMBOL_DISAPPEAR_ING:
            this.symbolDisappearIng();
            break;

           case SPIN_STATUS.SYMBOL_DISAPPEAR_END:
            this.symbolDisappearEnd();
            break;

           case SPIN_STATUS.SYMBOL_DISAPPEAR_DROP_START:
            this.symbolDisappearDropStart();
            break;

           case SPIN_STATUS.SYMBOL_DISAPPEAR_DROP_ING:
            this.symbolDisappearDropIng();
            break;

           case SPIN_STATUS.SYMBOL_DISAPPEAR_DROP_END:
            this.symbolDisappearDropEnd();
            break;

           case SPIN_STATUS.SCATTER_WIN_START:
            this.scatterWinEffectStart();
            break;

           case SPIN_STATUS.SCATTER_WIN_ING:
            this.scatterWinEffecting();
            break;

           case SPIN_STATUS.SCATTER_WIN_END:
            this.scatterWinEffectEnd();
            break;

           case SPIN_STATUS.FREE_GAME_COLLECT_START:
            this.freeGameCollectStart();
            break;

           case SPIN_STATUS.FREE_GAME_COLLECT_ING:
            this.freeGameCollectEffecting();
          }
        } else if (this.gameStatus == GAME_STATUS.FREE_GAME_WIN_SCORE_EFFECT) switch (this.freeGameWinScoreEffectStatus) {
         case FREEGAME_WIN_SCORE_EFFECT_STATUS.WIN_SCORE_EFFECT_START:
          this.freeGameWinScoreEffectStart();
          break;

         case FREEGAME_WIN_SCORE_EFFECT_STATUS.WIN_SCORE_EFFECT_ING:
          this.freeGameWinScoreEffectIng();
          break;

         case FREEGAME_WIN_SCORE_EFFECT_STATUS.WIN_SCORE_EFFECT_END:
          this.freeGameWinScoreEffectEnd();
        } else if (this.gameStatus == GAME_STATUS.IDLE && this.isChangeMoney) {
          this.isChangeMoney = false;
          this.data.coin = this.data.getClientMoney(this.newMoney);
          this.ui.setScore(this.data.coin);
        }
      };
      G53_Game.prototype.freeGameWinScoreEffectStart = function() {
        console.log("\u3010\u514d\u8cbb\u904a\u6232 \u8d0f\u9322\u6548\u679c \u3011\u958b\u59cb");
        this.freeGameWinScoreEffectStatus = FREEGAME_WIN_SCORE_EFFECT_STATUS.WIN_SCORE_EFFECT_ING;
        var _totalWinScore = this.data.totalBonus;
        var _betScore = this.data.playerBetScore;
        var _isFreeGame = false;
        this.ui.showWinScoreEffect(_totalWinScore, _betScore, _isFreeGame);
      };
      G53_Game.prototype.freeGameWinScoreEffectIng = function() {
        this.ui.isWinScoreEffecting() || (this.freeGameWinScoreEffectStatus = FREEGAME_WIN_SCORE_EFFECT_STATUS.WIN_SCORE_EFFECT_END);
      };
      G53_Game.prototype.freeGameWinScoreEffectEnd = function() {
        console.log("\u3010\u514d\u8cbb\u904a\u6232 \u8d0f\u9322\u6548\u679c \u3011\u7d50\u675f");
        this.freeGameWinScoreEffectStatus = FREEGAME_WIN_SCORE_EFFECT_STATUS.NONE;
        this.gameStatus = GAME_STATUS.IDLE;
        this.spinStatus = SPIN_STATUS.FINISHED_WAIT;
        AudioManager_1.AudioManager.instance.playAudioEvent("CloseWinScorePanel");
        this.ui.closeWinScoreEffect();
        this.checkAutoGameContinue();
      };
      G53_Game.prototype.registerEvents = function() {
        var _this = this;
        G53_SocketManager_1.default.getInstance().setBalanceModifyCallback(function(data) {
          _this.onBalanceModifyEvent(data);
        });
        G53_SocketManager_1.default.getInstance().setRefreshJackpotCallback(function(data) {
          _this.ui.refreshJackpot(data);
        });
      };
      G53_Game.prototype.showSymbolTip = function(symbolID, position) {
        this.gameStatus == GAME_STATUS.IDLE && _super.prototype.showSymbolTip.call(this, symbolID, position);
      };
      G53_Game.prototype.baseGameSpin_Response = function(p_data) {
        this.ui.showSpinMask();
        if (this.data.isAutoSpin) {
          this.autoSpinCount++;
          this.ui.openCancelAutoSpinButton();
          var _autoSpinCount = this.data.autoGameSpinCount - this.autoSpinCount;
          -1 == this.data.autoGameSpinCount && (_autoSpinCount = -1);
          this.ui.setAutoSpinCount(_autoSpinCount);
        }
        this.tempSpinCD = 0;
        this.toFastStopSpin = false;
        this.resultListIndex = 0;
        this.data.coin = this.data.getClientMoney(p_data.currentCash);
        this.data.totalBonus = this.data.getClientMoney(p_data.totalBonus);
        this.data.playerBetScore = this.data.getClientMoney(p_data.totalBet);
        this.data.resultList = p_data.resultList;
        this.data.remainingFreeGameCount = 6;
        this.data.getJackpot = p_data.getJackpot;
        this.eliminateListIndex = 0;
        var _money = this.data.getClientMoney(p_data.currentCash - p_data.totalBonus);
        this.ui.setScore(_money);
        this.ui.setWinScore(0);
        this.ui.setSpinArrowSpinningSpeed(Slot_GameUI_1.SPIN_ARROW_SPINNIG_TYPE.FAST);
        this.gameStatus = GAME_STATUS.SPIN;
        this.spinStatus = SPIN_STATUS.REEL_SCROLL_START;
        this.ui.spinButton.getComponent(cc.Button).interactable = true;
      };
      G53_Game.prototype.onSpin = function() {
        var _this = this;
        console.log("Spin");
        if (this.gameStatus == GAME_STATUS.SPIN && this.spinStatus == SPIN_STATUS.REEL_SCROLL_ING) this.toFastStopSpin = true; else if (this.gameStatus == GAME_STATUS.IDLE) {
          var _betScore = this.data.betOdds[this.data.betOddsStartIndex];
          this.slotReelManager.allSymbolPlayIdleEffect();
          if (this.hasEnoughBetScore()) {
            this.data.isAutoSpin || this.ui.showSpinButtonClickEffect();
            AudioManager_1.AudioManager.instance.playAudioEvent("StartSpin");
            _super.prototype.onSpin.call(this);
            this.gameStatus = GAME_STATUS.SERVER_REQUEST;
            this.socket.setOnSpinBaseGameEvent(_betScore).then(function(baseGameSpinResponeData) {
              _this.showResponseLog("SpinBaseGame", baseGameSpinResponeData);
              _this.baseGameSpin_Response(baseGameSpinResponeData);
            }).catch(function(error) {
              error.code == GameClient.errCode.RESPONSE_TIMED_OUT ? _this.socket.showErrorAlert(LocalizationManager_1.default.getInstance().get("ERROR_SOCKET_TIME_OUT"), null, function() {
                G53_SocketManager_1.default.getInstance().login();
              }) : error.code == GameClient.errCode.SLOT_INSUFFICIENT_BALANCE && _this.socket.showErrorAlert(LocalizationManager_1.default.getInstance().get("ERROR_NO_COIN"), null, function() {
                if (_this.data.isAutoSpin) {
                  _this.onCancelAutoSpinButton();
                  _this.ui.hideSpinMask();
                  _this.ui.showSpinButtonIdleEffect();
                  _this.ui.setSpinArrowSpinningSpeed(Slot_GameUI_1.SPIN_ARROW_SPINNIG_TYPE.NORMAL);
                }
              });
              _this.ui.spinButton.getComponent(cc.Button).interactable = true;
              _this.gameStatus = GAME_STATUS.IDLE;
            });
          } else {
            this.showLackMoneyPanel(_betScore * this.data.oneOddsScore);
            this.data.isAutoSpin && this.onCancelAutoSpinButton();
          }
        }
      };
      G53_Game.prototype.stopSpin = function() {
        AudioManager_1.AudioManager.instance.playAudioEvent("StopSpin");
        this.slotReelManager.stopAllReelScroll();
      };
      G53_Game.prototype.onCloseWinEffect = function() {
        this.ui.winScoreIsEndScore() ? this.winScoreEffectEnd() : this.ui.winScoreEffectToEndScore();
      };
      G53_Game.prototype.onStartAutoGame = function() {
        console.log("\u8f38\u5206\u505c\u6b62 " + this.data.autoGameScoreLessStop);
        console.log("\u8d0f\u5206\u505c\u6b62 " + this.data.autoGameWinScoreStop);
        AudioManager_1.AudioManager.instance.playAudioEvent("StartAutoGame");
        this.data.isAutoSpin = true;
        this.ui.autoSpinSwitch(true);
        this.autoSpinCount = 0;
        this.data.autoGameStartScore = this.data.coin;
        this.onSpin();
      };
      G53_Game.prototype.startFreeGame = function() {
        var _this = this;
        if (this.gameStatus == GAME_STATUS.CHANGE_PANEL) return;
        console.log("\u95dc\u9589\u514d\u8cbb\u904a\u6232\u9762\u677f");
        this.resultListIndex++;
        this.eliminateListIndex = 0;
        this.gameStatus = GAME_STATUS.CHANGE_PANEL;
        this.saveBaseGameLastSymbol();
        this.slotReelManager.setNormalMode();
        this.slotReelManager.setReelTable(this.data.reelTable_FG);
        this.slotReelManager.allSymbolPlayIdleEffect();
        this.slotReelManager.hideMaskEffect();
        this.ui.changeToFreeGameBackGround();
        AudioManager_1.AudioManager.instance.playAudioEvent("StarFreeGame");
        this.ui.closeFreeGamePanel().then(function() {
          console.log("\u958b\u59cb\u514d\u8cbb\u904a\u6232");
          _this.gameStatus = GAME_STATUS.FREE_GAME;
          _this.spinStatus = SPIN_STATUS.REEL_SCROLL_START;
        });
        this.ui.menuButtonSwitch(false);
        this.ui.buttonGroupSwitch(false);
        this.ui.showFreeGameOddsTip();
        this.ui.showRemainingFreeGameCount();
        this.ui.setRemainingFreeGameCount(this.data.remainingFreeGameCount);
      };
      G53_Game.prototype.freeGameEnd = function() {
        var _this = this;
        if (this.gameStatus == GAME_STATUS.CHANGE_PANEL) return;
        _super.prototype.freeGameEnd.call(this);
        console.log("\u95dc\u9589\u514d\u8cbb\u904a\u6232\u5f97\u5206\u9762\u677f");
        var _money = this.data.coin;
        var _winMoney = this.data.totalBonus;
        this.gameStatus = GAME_STATUS.CHANGE_PANEL;
        this.ui.closeWinScoreEffect();
        this.ui.closeFreeGameGetScorePanel().then(function() {
          console.log("\u514d\u8cbb\u904a\u6232\u7d50\u675f");
          AudioManager_1.AudioManager.instance.playAudioEvent("FreeGameEnd");
          if (_this.hasWinScoreEffect(_winMoney)) {
            _this.spinStatus = SPIN_STATUS.NONE;
            _this.freeGameWinScoreEffectStatus = FREEGAME_WIN_SCORE_EFFECT_STATUS.WIN_SCORE_EFFECT_START;
            _this.gameStatus = GAME_STATUS.FREE_GAME_WIN_SCORE_EFFECT;
          } else {
            _this.gameStatus = GAME_STATUS.IDLE;
            _this.spinStatus = SPIN_STATUS.NONE;
            _this.checkAutoGameContinue();
          }
        });
        this.ui.hideRemainingFreeGameCount();
        this.ui.changeToBaseGameBackGround();
        this.ui.menuButtonSwitch(true);
        this.ui.buttonGroupSwitch(true);
        this.ui.setScore(_money);
        this.ui.setWinScore(_winMoney);
        this.ui.showMainGameTip();
        this.ui.hideGetScoreTip();
        this.slotReelManager.showAllReelSymbol();
        this.slotReelManager.setReelTable(this.data.reelTable_BG);
        this.slotReelManager.setAllReelSymbol(this.baseGameLastSymbol);
        this.slotReelManager.allSymbolPlayIdleEffect();
        this.slotReelManager.hideMaskEffect();
        this.data.isFaseSpin && this.slotReelManager.setFastMode();
      };
      G53_Game.prototype.reelScrollStart = function() {
        console.log("\u3010\u8f2a\u5b50\u6efe\u52d5\u3011 \u958b\u59cb");
        AudioManager_1.AudioManager.instance.playAudioEvent("ReelScrollStart");
        var _targetIndex = this.data.resultList[this.resultListIndex].randomNumList;
        var _scrollData = new Slot_ReelManager_ScrollData_1.default();
        _scrollData.targetSymbolIndex = _targetIndex;
        _scrollData.hasOmenReels = [ false, false, false, false, false, false ];
        _scrollData.reels = this.data.resultList[this.resultListIndex].reels;
        _scrollData.isFreeGame = this.gameStatus == GAME_STATUS.FREE_GAME;
        this.data.isAutoSpin || this.gameStatus == GAME_STATUS.FREE_GAME ? _scrollData.rewardTime = this.autoRewardTime : _scrollData.rewardTime = this.normalRewardTime;
        this.ui.hideGetScoreTip();
        this.slotReelManager.scrollAllReels(_scrollData);
        this.spinStatus = SPIN_STATUS.REEL_SCROLL_ING;
        if (this.gameStatus == GAME_STATUS.FREE_GAME) {
          AudioManager_1.AudioManager.instance.playAudioEvent("StartSpinFree");
          this.data.remainingFreeGameCount--;
          this.ui.setRemainingFreeGameCount(this.data.remainingFreeGameCount);
          this.ui.showFreeGameTip();
        } else this.ui.showMainGameTip();
      };
      G53_Game.prototype.reelScrolling = function() {
        this.slotReelManager.isReelSrolling() || (this.spinStatus = SPIN_STATUS.REEL_SCROLL_END);
      };
      G53_Game.prototype.reelScrollEnd = function() {
        console.log("\u3010\u8f2a\u5b50\u6efe\u52d5\u3011\u7d50\u675f");
        AudioManager_1.AudioManager.instance.playAudioEvent("AllReelStop");
        this.spinStatus = SPIN_STATUS.CHECK_CHANGE_SYMBOL;
      };
      G53_Game.prototype.checkChangeSymbol = function() {
        console.log("\u3010\u6aa2\u67e5\u6709\u6c92\u6709\u8b8aWILD\u3011\u958b\u59cb");
        if (this.eliminateListIndex > 0) {
          this.spinStatus = SPIN_STATUS.CHANGE_SYMBOL_START;
          return;
        }
        this.spinStatus = SPIN_STATUS.CHECK_REWARD;
      };
      G53_Game.prototype.changeSymbolStart = function() {
        console.log("\u3010\u6539\u8b8a\u5716\u793a\u3011\u958b\u59cb");
        AudioManager_1.AudioManager.instance.playAudioEvent("ChangeSymbolToWild");
        this.slotReelManager.changeSymbol(this.data.resultList[this.resultListIndex].eliminateList[this.eliminateListIndex].wildCoverPos);
        this.spinStatus = SPIN_STATUS.CHANGE_SYMBOL_ING;
      };
      G53_Game.prototype.changeSymbolIng = function() {
        this.slotReelManager.isChangeSymbolIng() || (this.spinStatus = SPIN_STATUS.CHANGE_SYMBOL_END);
      };
      G53_Game.prototype.changeSymbolEnd = function() {
        this.spinStatus = SPIN_STATUS.CHECK_REWARD;
      };
      G53_Game.prototype.checkReward = function() {
        console.log("\u6aa2\u67e5\u4e2d\u734e");
        var _winScore = this.data.getClientMoney(this.data.resultList[this.resultListIndex].totalWinBonus);
        var _isFreeGame = this.gameStatus == GAME_STATUS.FREE_GAME;
        this.hasEliminateData() && this.data.resultList[this.resultListIndex].eliminateList[this.eliminateListIndex].isEliminate || this.data.getJackpot ? this.spinStatus = SPIN_STATUS.REWARD_EFFECT_START : !_isFreeGame && this.hasFreeGame() ? this.spinStatus = SPIN_STATUS.SCATTER_WIN_START : _isFreeGame && this.data.resultList[this.resultListIndex].scatter.count >= 2 ? this.spinStatus = SPIN_STATUS.FREE_GAME_COLLECT_START : this.hasWinScoreEffect(_winScore) ? this.spinStatus = SPIN_STATUS.WIN_SCORE_EFFECT_START : this.spinStatus = SPIN_STATUS.FINISHED_WAIT;
      };
      G53_Game.prototype.scatterWinEffectStart = function() {
        console.log("\u3010Scatter\u4e2d\u734e\u7279\u6548\u3011 \u958b\u59cb");
        var scatter = this.data.resultList[this.resultListIndex].scatter;
        var _winPositstion = scatter.scatterPos;
        var _winScore = this.data.getClientMoney(scatter.winBonus);
        this.slotReelManager.allSymbolPlayIdleEffect();
        this.slotReelManager.showSymbolReWardEffect(_winPositstion, true);
        this.ui.addScore(_winScore);
        this.ui.addWinScore(_winScore);
        this.ui.showGetScoreTip(_winScore);
        this.ui.hideGameTip();
        AudioManager_1.AudioManager.instance.playAudioEvent("GotFreeGame");
        this.spinStatus = SPIN_STATUS.SCATTER_WIN_ING;
      };
      G53_Game.prototype.scatterWinEffecting = function() {
        this.slotReelManager.isRewardEffecting() || (this.spinStatus = SPIN_STATUS.SCATTER_WIN_END);
      };
      G53_Game.prototype.scatterWinEffectEnd = function() {
        console.log("\u3010Scatter\u4e2d\u734e\u7279\u6548\u3011 \u7d50\u675f");
        this.spinStatus = SPIN_STATUS.FINISHED;
      };
      G53_Game.prototype.hasEliminateData = function() {
        if (null == this.data.resultList[this.resultListIndex].eliminateList) return false;
        if (this.eliminateListIndex < this.data.resultList[this.resultListIndex].eliminateList.length) return true;
        return false;
      };
      G53_Game.prototype.getJackpotStart = function() {
        return __awaiter(this, void 0, Promise, function() {
          var _winScore, count;
          var _this = this;
          return __generator(this, function(_a) {
            console.log(" \u3010Get Jackpot\u3011 \u958b\u59cb");
            _winScore = this.data.getClientMoney(this.data.resultList[this.resultListIndex].jackpotBonus.winBonus);
            count = this.data.resultList[this.resultListIndex].jackpotBonus.count;
            switch (count) {
             case 1:
              AudioManager_1.AudioManager.instance.playAudioEvent("JackPotMini");
              break;

             case 2:
              AudioManager_1.AudioManager.instance.playAudioEvent("JackPotMinor");
              break;

             case 3:
              AudioManager_1.AudioManager.instance.playAudioEvent("JackPotMajor");
              break;

             case 4:
              AudioManager_1.AudioManager.instance.playAudioEvent("JackPotGrand");
            }
            this.ui.openJackpotPanel(_winScore, count);
            this.scheduleOnce(function() {
              _this.slotReelManager.hideMaskEffect();
              _this.ui.closeJackpotPanel();
            }, 5);
            this.spinStatus = SPIN_STATUS.JACKPOT_ING;
            return [ 2 ];
          });
        });
      };
      G53_Game.prototype.getJackpotIng = function() {
        if (!this.ui.jackpotPanel.node.active) {
          AudioManager_1.AudioManager.instance.playAudioEvent("FreeGameEnd");
          this.spinStatus = SPIN_STATUS.FINISHED;
        }
      };
      G53_Game.prototype.rewardEffectStart = function() {
        console.log("\u3010\u4e2d\u734e\u7279\u6548\u3011 \u958b\u59cb");
        var _winScore = this.data.getClientMoney(this.data.resultList[this.resultListIndex].eliminateList[this.eliminateListIndex].eliWinBonus);
        var _winPositstion = this.data.resultList[this.resultListIndex].eliminateList[this.eliminateListIndex].eliAllPos;
        if (this.data.getJackpot) {
          _winPositstion = this.data.resultList[this.resultListIndex].jackpotBonus.jackpotPos;
          _winScore = this.data.getClientMoney(this.data.resultList[this.resultListIndex].jackpotBonus.winBonus);
        }
        this.ui.addScore(_winScore);
        this.ui.addWinScore(_winScore);
        this.ui.showGetScoreTip(_winScore);
        this.ui.hideGameTip();
        this.gameStatus !== GAME_STATUS.FREE_GAME ? this.ui.playMainRewardWinEffect() : this.ui.playFreeRewardWinEffect();
        this.slotReelManager.showSymbolReWardEffect(_winPositstion, true);
        this.playRewardSound();
        this.spinStatus = SPIN_STATUS.REWARD_EFFECT_ING;
      };
      G53_Game.prototype.playRewardSound = function() {
        var rewardAudio = this.gameStatus == GAME_STATUS.FREE_GAME ? "PlayRewardFree" : "PlayRewardMain";
        AudioManager_1.AudioManager.instance.playAudioEvent(rewardAudio);
        console.log("\u64ad\u653e\u4e2d\u734e\u97f3\u6548");
      };
      G53_Game.prototype.rewardEffecting = function() {
        this.slotReelManager.isRewardEffecting() || (this.spinStatus = SPIN_STATUS.REWARD_EFFECT_END);
      };
      G53_Game.prototype.rewardEffectEnd = function() {
        var _this = this;
        console.log("\u3010\u4e2d\u734e\u7279\u6548\u3011 \u7d50\u675f");
        if (this.data.getJackpot) {
          this.spinStatus = SPIN_STATUS.JACKPOT_WAIT;
          setTimeout(function() {
            _this.spinStatus = SPIN_STATUS.JACKPOT_START;
          }, 1300);
        } else this.spinStatus = SPIN_STATUS.SYMBOL_DISAPPEAR_START;
      };
      G53_Game.prototype.symbolDisappearStart = function() {
        var _this = this;
        console.log("\u3010\u7b26\u865f\u6d88\u9664\u3011 \u958b\u59cb");
        var _winPositstion = this.data.resultList[this.resultListIndex].eliminateList[this.eliminateListIndex].eliAllPos;
        var _nextReels = this.data.resultList[this.resultListIndex].eliminateList[this.eliminateListIndex].nextReels;
        AudioManager_1.AudioManager.instance.playAudioEvent("BubbleEffect");
        this.slotReelManager.showSymbolDisappearEffect(_winPositstion, _nextReels);
        this.data.resultList[this.resultListIndex].eliminateList[this.eliminateListIndex].winJoinList.forEach(function(item) {
          item.specialMultiple > 1 && _this.scheduleOnce(function() {
            _this.ui.setMulLbl(_this.slotReelManager.getWildMulPos(item.singlePos));
          }, .9);
        });
        this.spinStatus = SPIN_STATUS.SYMBOL_DISAPPEAR_ING;
      };
      G53_Game.prototype.symbolDisappearIng = function() {
        this.slotReelManager.isSymbolDisappearEffecting() || (this.spinStatus = SPIN_STATUS.SYMBOL_DISAPPEAR_END);
      };
      G53_Game.prototype.symbolDisappearEnd = function() {
        console.log("\u3010\u7b26\u865f\u6d88\u9664\u3011 \u7d50\u675f");
        this.showScore();
        this.spinStatus = SPIN_STATUS.SHOW_SCORE;
      };
      G53_Game.prototype.showScore = function() {
        var _this = this;
        var _winScore = this.data.getClientMoney(this.data.resultList[this.resultListIndex].eliminateList[this.eliminateListIndex].eliWinBonus);
        var count = 0;
        var hasMul = false;
        this.data.resultList[this.resultListIndex].eliminateList[this.eliminateListIndex].winJoinList.forEach(function(item) {
          if (item.specialMultiple > 1) {
            !hasMul && _this.ui.showMulFly();
            hasMul = true;
            count += item.specialMultiple;
          }
        });
        count = hasMul ? count : 1;
        if (hasMul) {
          this.ui.showWildReward(_winScore / count, count, false, false);
          this.scheduleOnce(function() {
            _this.ui.showWildReward(_winScore, count, true);
            _this.scheduleOnce(function() {
              _this.spinStatus = SPIN_STATUS.SYMBOL_DISAPPEAR_DROP_START;
            }, .5);
          }, .4);
        } else {
          this.ui.showWildReward(_winScore / count, count, false);
          this.scheduleOnce(function() {
            _this.spinStatus = SPIN_STATUS.SYMBOL_DISAPPEAR_DROP_START;
          }, .5);
        }
      };
      G53_Game.prototype.symbolDisappearDropStart = function() {
        console.log("\u3010\u7b26\u865f\u6d88\u9664\u5f8c\u6389\u843d\u3011 \u958b\u59cb");
        var _winPositstion = this.data.resultList[this.resultListIndex].eliminateList[this.eliminateListIndex].eliAllPos;
        this.slotReelManager.ShowSymbolDisappearDropEffect(_winPositstion);
        this.spinStatus = SPIN_STATUS.SYMBOL_DISAPPEAR_DROP_ING;
        AudioManager_1.AudioManager.instance.playAudioEvent("SymbolDropStart");
      };
      G53_Game.prototype.symbolDisappearDropIng = function() {
        this.slotReelManager.isSymbolDisappearDropEffecting() || (this.spinStatus = SPIN_STATUS.SYMBOL_DISAPPEAR_DROP_END);
      };
      G53_Game.prototype.symbolDisappearDropEnd = function() {
        console.log("\u3010\u7b26\u865f\u6d88\u9664\u5f8c\u6389\u843d\u3011 \u7d50\u675f");
        this.eliminateListIndex++;
        this.spinStatus = SPIN_STATUS.CHECK_CHANGE_SYMBOL;
      };
      G53_Game.prototype.winScoreEffectStart = function() {
        console.log("\u3010 \u8d0f\u9322\u6548\u679c \u3011\u958b\u59cb");
        this.spinStatus = SPIN_STATUS.WIN_SCORE_EFFECT_ING;
        var _totalWinScore = this.data.getClientMoney(this.data.resultList[this.resultListIndex].totalWinBonus);
        var _betScore = this.data.playerBetScore;
        var _isFreeGame = this.gameStatus === GAME_STATUS.FREE_GAME;
        this.ui.showWinScoreEffect(_totalWinScore, _betScore, _isFreeGame);
      };
      G53_Game.prototype.winScoreEffectIng = function() {
        this.ui.isWinScoreEffecting() || (this.spinStatus = SPIN_STATUS.WIN_SCORE_EFFECT_END);
      };
      G53_Game.prototype.winScoreEffectEnd = function() {
        console.log("\u3010 \u8d0f\u9322\u6548\u679c \u3011\u7d50\u675f");
        if (this.gameStatus == GAME_STATUS.FREE_GAME_WIN_SCORE_EFFECT) this.freeGameWinScoreEffectStatus = FREEGAME_WIN_SCORE_EFFECT_STATUS.WIN_SCORE_EFFECT_END; else {
          this.spinStatus = SPIN_STATUS.FINISHED_WAIT;
          this.gameStatus == GAME_STATUS.FREE_GAME ? AudioManager_1.AudioManager.instance.playAudioEvent("CloseWinScorePanelFreeGame") : AudioManager_1.AudioManager.instance.playAudioEvent("CloseWinScorePanel");
          this.ui.closeWinScoreEffect();
        }
      };
      G53_Game.prototype.freeGameCollectStart = function() {
        console.log("\u64a5\u653efree game Collect\u7279\u6548");
        var scatterPos = this.data.resultList[this.resultListIndex].scatter.scatterPos;
        var _winScore = this.data.getClientMoney(this.data.resultList[this.resultListIndex].scatter.winBonus);
        this.slotReelManager.allSymbolPlayIdleEffect();
        this.slotReelManager.showSymbolReWardEffect(scatterPos, true);
        this.ui.showFreeGameCollctEffect(this.slotReelManager.node, this.slotReelManager.getSymbolReWardPos(scatterPos), this.data.remainingFreeGameCount);
        this.ui.addScore(_winScore);
        this.ui.addWinScore(_winScore);
        this.ui.showGetScoreTip(_winScore);
        this.ui.hideGameTip();
        this.data.remainingFreeGameCount += 6;
        this.spinStatus = SPIN_STATUS.FREE_GAME_COLLECT_ING;
      };
      G53_Game.prototype.freeGameCollectEffecting = function() {
        this.ui.isFreeGameCollctEffecting || (this.spinStatus = SPIN_STATUS.FINISHED_WAIT);
      };
      G53_Game.prototype.spinFinishedWait = function(p_dt) {
        if (0 == this.tempWaitTime) {
          var _totalWinScore = this.data.getClientMoney(this.data.resultList[this.resultListIndex].totalWinBonus);
          this.data.isAutoSpin || this.gameStatus == GAME_STATUS.FREE_GAME ? this.tempSpinFinishedStopTime = this.fastSpinFinishedStopTime : this.tempSpinFinishedStopTime = this.normalSpinFinishedStopTime;
          if (_totalWinScore > 0) {
            AudioManager_1.AudioManager.instance.playAudioEvent("WinScore");
            this.ui.showTotalGetScore(_totalWinScore);
            this.data.isAutoSpin ? this.tempSpinFinishedStopTime += this.autoSpinUpdateTipBroadEffectTime : this.tempSpinFinishedStopTime += this.normalUpdateTipBroadEffectTime;
          }
        }
        this.tempWaitTime += p_dt;
        if (this.tempWaitTime > this.tempSpinFinishedStopTime) {
          this.spinStatus = SPIN_STATUS.FINISHED;
          this.tempWaitTime = 0;
        }
      };
      G53_Game.prototype.spinFinished = function() {
        return __awaiter(this, void 0, Promise, function() {
          var _freeGameWinMoney, _betScore;
          var _this = this;
          return __generator(this, function(_a) {
            console.log("\u3010Spin\u3011 \u7d50\u675f");
            if (this.gameStatus == GAME_STATUS.FREE_GAME) if (this.hasNextResultList()) {
              this.resultListIndex++;
              this.eliminateListIndex = 0;
              this.slotReelManager.allSymbolPlayIdleEffect();
              this.spinStatus = SPIN_STATUS.REEL_SCROLL_START;
            } else {
              this.ui.hideFreeGameOddsTip();
              console.log("\u958b\u555f\u514d\u8cbb\u904a\u6232\u7d50\u7b97\u9762\u677f");
              AudioManager_1.AudioManager.instance.playAudioEvent("OpenFreeGameGetScorePanel");
              _freeGameWinMoney = this.data.totalBonus - this.data.getClientMoney(this.data.resultList[0].totalWinBonus);
              _betScore = this.data.playerBetScore;
              this.ui.openFreeGameGetScorePanel(_freeGameWinMoney, _betScore);
              this.ui.setSpinArrowSpinningSpeed(Slot_GameUI_1.SPIN_ARROW_SPINNIG_TYPE.NORMAL);
              this.gameStatus = GAME_STATUS.FREE_GAME_REWARD;
              this.spinStatus = SPIN_STATUS.NONE;
              setTimeout(function() {
                _this.gameStatus == GAME_STATUS.FREE_GAME_REWARD && _this.freeGameEnd();
              }, 5e3);
            } else if (this.hasFreeGame()) cc.tween(this.node).call(function() {
              _this.gameStatus = GAME_STATUS.FREE_GAME_TIP;
            }).delay(this.scatterShowTime).call(function() {
              var _freeGameCount = 6;
              console.log("\u958b\u8d77\u514d\u8cbb\u904a\u6232\u9762\u677f");
              AudioManager_1.AudioManager.instance.playAudioEvent("OpenFreeGamePanel");
              _this.ui.openFreeGamePanel(_freeGameCount);
              setTimeout(function() {
                _this.gameStatus == GAME_STATUS.FREE_GAME_TIP && _this.startFreeGame();
              }, 3e3);
            }).start(); else {
              this.gameStatus = GAME_STATUS.IDLE;
              this.spinStatus = SPIN_STATUS.NONE;
              this.checkAutoGameContinue();
            }
            return [ 2 ];
          });
        });
      };
      G53_Game.prototype.hasNextResultList = function() {
        if (this.resultListIndex < this.data.resultList.length - 1) return true;
        return false;
      };
      G53_Game.prototype.hasFreeGame = function() {
        if (this.data.resultList.length > 1) return true;
        return false;
      };
      G53_Game.prototype.setReelInfo = function(p_data) {
        this.data.reelStartIndex = p_data.position;
        this.data.reelTable_BG = p_data.reels.BG;
        this.data.reelTable_FG = p_data.reels.FG;
        this.slotReelManager.setReelTable(this.data.reelTable_BG);
        this.slotReelManager.setReelStartIndex(this.data.reelStartIndex);
      };
      G53_Game.prototype.onErrorEvent = function(_a) {
        var errCode = _a.errCode;
        console.error("\u6536\u5230\u932f\u8aa4\u8a0a\u606f", errCode);
      };
      G53_Game.prototype.playerLogin = function() {
        this.data.betOddsStartIndex = this.data.getBetIndex();
        var _money = this.data.coin;
        var _betMoney = this.data.getClientMoney(this.data.betOdds[this.data.betOddsStartIndex] * this.data.oneOddsScore);
        this.ui.setScore(_money);
        this.ui.setBetScore(_betMoney);
        this.ui.setWinScore(0);
      };
      __decorate([ property({
        type: G53_SlotReelManager_1.default,
        override: true
      }) ], G53_Game.prototype, "slotReelManager", void 0);
      __decorate([ property({
        type: cc.Float,
        displayName: "\u4e2d\u734e\u7b2c\u4e00\u6b21\u63db\u7dda\u7684\u6642\u9593"
      }) ], G53_Game.prototype, "firstWinLineChangeTime", void 0);
      __decorate([ property({
        type: cc.Float,
        displayName: "\u4e2d\u734e\u7dda\u66f4\u63db\u6642\u9593"
      }) ], G53_Game.prototype, "winLineChangeTime", void 0);
      __decorate([ property({
        type: cc.Float,
        displayName: "\u4e2d\u734e\u7dda\u66f4\u63db\u7b49\u5f85\u6642\u9593"
      }) ], G53_Game.prototype, "winLineChangeWaitTime", void 0);
      __decorate([ property({
        type: G53_GameUI_1.default,
        override: true
      }) ], G53_Game.prototype, "ui", void 0);
      __decorate([ property({
        type: cc.Float,
        displayName: "spin\u51b7\u537b\u6642\u9593",
        tooltip: "\u958b\u59cbSpin\u5f8c\u7684\u51b7\u537b\u6642\u9593\uff0c\u54ea\u4f86\u8b93\u6efe\u8f2a\u6703\u7a0d\u5fae\u6efe\u52d5\u5728\u505c\u6b62"
      }) ], G53_Game.prototype, "spinCD", void 0);
      __decorate([ property({
        type: cc.Float,
        displayName: "\u4e00\u822cspin\u7d50\u675f\u505c\u6b62\u7b49\u5f85\u6642\u9593"
      }) ], G53_Game.prototype, "normalSpinFinishedStopTime", void 0);
      __decorate([ property({
        type: cc.Float,
        displayName: "\u5feb\u901fspin\u7d50\u675f\u505c\u6b62\u7b49\u5f85\u6642\u9593"
      }) ], G53_Game.prototype, "fastSpinFinishedStopTime", void 0);
      __decorate([ property({
        type: cc.Float,
        displayName: "\u624b\u52d5Spin \u4e2d\u734e\u66f4\u65b0\u63d0\u793a\u9762\u677f\u7279\u6548\u6642\u9593",
        tooltip: "\u53ea\u6709\u5728\u4e2d\u734e\u7684\u6642\u5019\u624d\u6703\u5403\u9019\u500b\u53c3\u6578"
      }) ], G53_Game.prototype, "normalUpdateTipBroadEffectTime", void 0);
      __decorate([ property({
        type: cc.Float,
        displayName: "\u81ea\u52d5Spin \u4e2d\u734e\u66f4\u65b0\u63d0\u793a\u9762\u677f\u7279\u6548\u6642\u9593",
        tooltip: "\u53ea\u6709\u5728\u4e2d\u734e\u7684\u6642\u5019\u624d\u6703\u5403\u9019\u500b\u53c3\u6578"
      }) ], G53_Game.prototype, "autoSpinUpdateTipBroadEffectTime", void 0);
      __decorate([ property({
        type: cc.Float,
        displayName: "\u624b\u52d5Spin\u4e2d\u734e\u6642\u9593"
      }) ], G53_Game.prototype, "normalRewardTime", void 0);
      __decorate([ property({
        type: cc.Float,
        displayName: "\u81ea\u52d5Spin\u4e2d\u734e\u6642\u9593"
      }) ], G53_Game.prototype, "autoRewardTime", void 0);
      __decorate([ property({
        type: cc.Float,
        displayName: "scatter\u986f\u793a\u7684\u6642\u9593"
      }) ], G53_Game.prototype, "scatterShowTime", void 0);
      G53_Game = __decorate([ ccclass ], G53_Game);
      return G53_Game;
    }(Slot_GameManager_1.default);
    exports.default = G53_Game;
    cc._RF.pop();
  }, {
    "../../Common/Tools/AudioManager/AudioManager": void 0,
    "../../Common/Tools/Localization/LocalizationManager": void 0,
    "../../SlotFramework/Game/Data/Slot_GameManager_InitData": void 0,
    "../../SlotFramework/Game/Data/Slot_GameUI_InitData": void 0,
    "../../SlotFramework/Game/Data/Slot_ReelManager_InitData": void 0,
    "../../SlotFramework/Game/Data/Slot_ReelManager_ScrollData": void 0,
    "../../SlotFramework/Game/Slot_GameManager": void 0,
    "../../SlotFramework/Game/view/Slot_GameUI": void 0,
    "../Common/G53_DataManager": "G53_DataManager",
    "../Common/G53_DynamicPopUpPanelManager": "G53_DynamicPopUpPanelManager",
    "../Common/Socket/G53_SocketManager": "G53_SocketManager",
    "./View/G53_GameUI": "G53_GameUI",
    "./View/G53_SlotReelManager": "G53_SlotReelManager"
  } ],
  G53_JackpotPanel: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "87e375GfqxPCKettk66Szq8", "G53_JackpotPanel");
    "use strict";
    var __extends = this && this.__extends || function() {
      var extendStatics = function(d, b) {
        extendStatics = Object.setPrototypeOf || {
          __proto__: []
        } instanceof Array && function(d, b) {
          d.__proto__ = b;
        } || function(d, b) {
          for (var p in b) Object.prototype.hasOwnProperty.call(b, p) && (d[p] = b[p]);
        };
        return extendStatics(d, b);
      };
      return function(d, b) {
        extendStatics(d, b);
        function __() {
          this.constructor = d;
        }
        d.prototype = null === b ? Object.create(b) : (__.prototype = b.prototype, new __());
      };
    }();
    var __decorate = this && this.__decorate || function(decorators, target, key, desc) {
      var c = arguments.length, r = c < 3 ? target : null === desc ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
      if ("object" === typeof Reflect && "function" === typeof Reflect.decorate) r = Reflect.decorate(decorators, target, key, desc); else for (var i = decorators.length - 1; i >= 0; i--) (d = decorators[i]) && (r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r);
      return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    var FXController_1 = require("../../../Common/Optional/Crosis/FXController/FXController");
    var CommonTool_1 = require("../../../Common/Tools/CommonTool");
    var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
    var G53_JackpotPanel = function(_super) {
      __extends(G53_JackpotPanel, _super);
      function G53_JackpotPanel() {
        var _this = null !== _super && _super.apply(this, arguments) || this;
        _this.fxJackpotLevel = [];
        _this.spineJackpotLevel = [];
        _this.score = null;
        _this.jpStr = [ "MINI", "MINOR", "MAJOR", "GRAND" ];
        _this.level = 0;
        return _this;
      }
      G53_JackpotPanel.prototype.init = function() {
        this.node.active = false;
      };
      G53_JackpotPanel.prototype.open = function(p_freeGameGetScore, level) {
        var _this = this;
        this.level = level;
        this.fadeIn();
        this.node.setPosition(cc.Vec2.ZERO);
        this.fxJackpotLevel.forEach(function(item) {
          return item.stop();
        });
        this.fxJackpotLevel[level - 1].play();
        this.fxJackpotLevel[level - 1].getComponentsInChildren(cc.ParticleSystem3D).forEach(function(item) {
          item.play();
        });
        this.spineJackpotLevel[level - 1].setAnimation(0, this.jpStr[level - 1] + "_Start", false);
        this.spineJackpotLevel[level - 1].setCompleteListener(function() {
          _this.spineJackpotLevel[level - 1].setCompleteListener(null);
          _this.spineJackpotLevel[level - 1].setAnimation(0, _this.jpStr[level - 1] + "_Loop", true);
        });
        this.score.string = CommonTool_1.CommonTool.getNumText(p_freeGameGetScore, 2, true, false);
        cc.tween(this.score.node).to(.3, {
          scale: 1
        }, {
          easing: cc.easing.cubicInOut
        }).start();
      };
      G53_JackpotPanel.prototype.close = function() {
        var _this = this;
        this.scheduleOnce(function() {
          _this.fxJackpotLevel[_this.level - 1].getComponentsInChildren(cc.ParticleSystem3D).forEach(function(item) {
            item.stop();
          });
        }, .25);
        this.fadeOut();
      };
      G53_JackpotPanel.prototype.fadeIn = function() {
        this.node.opacity = 255;
        this.node.active = true;
      };
      G53_JackpotPanel.prototype.fadeOut = function() {
        var _this = this;
        cc.tween(this.node).to(.5, {
          opacity: 0
        }, {
          easing: "cubicInOut"
        }).call(function() {
          _this.node.active = false;
        }).start();
      };
      __decorate([ property([ FXController_1.default ]) ], G53_JackpotPanel.prototype, "fxJackpotLevel", void 0);
      __decorate([ property([ sp.Skeleton ]) ], G53_JackpotPanel.prototype, "spineJackpotLevel", void 0);
      __decorate([ property(cc.Label) ], G53_JackpotPanel.prototype, "score", void 0);
      G53_JackpotPanel = __decorate([ ccclass ], G53_JackpotPanel);
      return G53_JackpotPanel;
    }(cc.Component);
    exports.default = G53_JackpotPanel;
    cc._RF.pop();
  }, {
    "../../../Common/Optional/Crosis/FXController/FXController": void 0,
    "../../../Common/Tools/CommonTool": void 0
  } ],
  G53_Language: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "74152d92GVBiLN3KcLkXr75", "G53_Language");
    "use strict";
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    exports.G53_Language = void 0;
    exports.G53_Language = {
      "zh-cn": {
        TEXT_SLOT_MONEY_LESS_SETTING: "\u6301\u6709\u91d1\u989d\u4f4e\u4e8e\u4f59\u989d\u51cf\u5c11\u8bbe\u5b9a\u91d1\u989d\uff0c\u8bf7\u8c03\u6574\u8bbe\u5b9a",
        TEXT_G53_LOADING: "\u73a9\u547d\u52a0\u8f7d\u4e2d...",
        TEXT_G53_SYMBOL_TIP_WILD: "WILD",
        TEXT_G53_SYMBOL_TIP_BONUS: "BOUNS",
        RULE_G53_GAME_ERROR: "\u6e38\u620f\u51fa\u73b0\u6545\u969c\u65f6\uff0c\u6240\u6709\u8d54\u4ed8\u548c\u6e38\u620f\u90fd\u89c6\u4e3a\u65e0\u6548",
        RULE_G53_1_1: "\u70b9\u51fb\u52a0\u51cf\u53f7\u6309\u94ae\uff0c\u8c03\u6574\u6295\u6ce8\u7684\u603b\u989d",
        RULE_G53_1_2: "\u70b9\u51fbSPIN\u6309\u94ae\u4ee5\u5f53\u524d\u7684\u6295\u6ce8\u65cb\u8f6c\u8f6c\u8f74",
        RULE_G53_1_3: "\u70b9\u51fb\u81ea\u52a8\u6e38\u620f\u6309\u94ae\u4f1a\u5f00\u542f\u81ea\u52a8\u6e38\u620f\u8bbe\u7f6e\uff0c\u81ea\u52a8\u6e38\u620f\u542f\u7528\u65f6\uff0c\u70b9\u51fb\u65cb\u8f6c\u6309\u94ae\u53ef\u505c\u6b62\u81ea\u52a8\u6a21\u5f0f",
        RULE_G53_1_4: "\u70b9\u51fb\u6781\u901f\u6e38\u620f\u6309\u94ae\uff0c\u6309\u94ae\u4eae\u8d77\u65f6\u53ef\u5f00\u542f\u6781\u901f\u6a21\u5f0f\uff0c\u6781\u901f\u6a21\u5f0f\u53ef\u4e0e\u81ea\u52a8\u6e38\u620f\u540c\u65f6\u751f\u6548",
        RULE_G53_1_5: "A\uff1d\u6295\u6ce8\u603b\u989d\nB\uff1d\u56fe\u6807\u6d88\u9664\u9897\u6570\u8d54\u7387(\u8bf7\u53c2\u8003\u8d54\u7387\u8868)\nC\uff1d\u767e\u642d\u56fe\u6807\u52a0\u6210\u500d\u7387",
        RULE_G53_1_7: "\u4e2d\u5956\u91d1\u989d = (A/50)*B*C",
        RULE_G53_4_1: "\u76d8\u9762\u51fa\u73b01~4\u4e2a\u3000\u3000\u3000\u53ca\u83b7\u5f97\u5927\u5956\u5f69\u91d1\uff0c\u6d3e\u5f69\u540e\u7ed3\u675f\u6b64\u5c40",
        RULE_G53_4_2: "\u82e5\u76d8\u9762\u4ea7\u751f\u5f97\u5206\u7ec4\u5408\uff0c\u5c06\u4f1a\u751f\u62101 ~ 2\u4e2a\u3000\u3000\u3000",
        RULE_G53_4_3: "\u3000\u3000\u3000\u4e0a\u9762\u6709\u673a\u4f1a\u51fa\u73b0\u4e58\u500d:",
        RULE_G53_4_4: "X2\u3001X3\u3001X5\uff08X5\u4ec5\u5728\u514d\u8d39\u6e38\u620f\u51fa\u73b0\uff09\u82e5\u4e2d\u5956\u76d8\u9762\u5305\u542b\u6709\u4e58\u500d\u7684\u3000\u3000\u3000\uff0c\u5219\u8be5\u6b21\u5f97\u5206\u4e58\u4e0a\u8be5\u6570\u5b57",
        RULE_G53_4_5: "\u82e5\u5305\u542b\u4e24\u4e2a\u4ee5\u4e0a\u6709\u4e58\u500d\u7684\u3000\u3000\u3000\uff0c\u5219\u8be5\u7ec4\u5206\u6570\u5c06\u4e58\u4e0a\u53ef\u53d8\u6807\u8bb0\u7684\u4e58\u500d\u52a0\u603b",
        RULE_G53_5_1: "\u76e4\u9762\u4e2d\u4efb\u610f\u4f4d\u7f6e\u51fa\u73b02\u4e2a\u4ee5\u4e0a\u3000\u3000\u3000\u65f6\uff0c\u89e6\u53d16\u6b21\u514d\u8d39\u6e38\u620f",
        RULE_G53_5_2: "\u76e4\u9762\u4e2d\u4efb\u610f\u4f4d\u7f6e\u51fa\u73b02\u4e2a\u4ee5\u4e0a\u3000\u3000\u3000\u65f6\uff0c\u53ef\u83b7\u5f972\u500d\u4e0b\u6ce8\u5206\u6570\u5f97\u5206",
        RULE_G53_5_3: "\u514d\u8d39\u6e38\u620f\u671f\u95f4\u5185\uff0c\u76e4\u9762\u4e2d\u4efb\u610f\u4f4d\u7f6e\u51fa\u73b02\u4e2a\u4ee5\u4e0a\u3000\u3000\u3000\u65f6\uff0c\u514d\u8d39\u6b21\u6570+6",
        RULE_G53_5_4: "\u514d\u8d39\u6e38\u620f\u671f\u95f4\uff0c\u6bcf\u5c40\u4e00\u5f00\u59cb\u5fc5\u4f1a\u843d\u4e0b2\u4e2a",
        RULE_G53_6_1: "\u76d8\u9762\u4e0a\u76f8\u8fde\u6307\u5b9a\u6570\u91cf\u7684\u76f8\u540c\u56fe\u6807\uff0c\u5373\u53ef\u83b7\u5f97\u8be5\u56fe\u6807\u7684\u5f97\u5206\u5956\u52b1\uff0c\u659c\u7ebf\u4e0d\u7b97\u76f8\u8fde"
      },
      "zh-tw": {
        TEXT_SLOT_MONEY_LESS_SETTING: "\u6301\u6709\u91d1\u984d\u4f4e\u65bc\u9918\u984d\u6e1b\u5c11\u8a2d\u5b9a\u91d1\u984d\uff0c\u8acb\u8abf\u6574\u8a2d\u5b9a",
        TEXT_G53_LOADING: "\u73a9\u547d\u52a0\u8f09\u4e2d...",
        TEXT_G53_SYMBOL_TIP_WILD: "WILD",
        TEXT_G53_SYMBOL_TIP_BONUS: "BOUNS",
        RULE_G53_GAME_ERROR: "\u904a\u6232\u51fa\u73fe\u6545\u969c\u6642\uff0c\u6240\u6709\u8ce0\u4ed8\u548c\u904a\u6232\u90fd\u8996\u70ba\u7121\u6548",
        RULE_G53_1_1: "\u9ede\u64ca\u52a0\u6e1b\u865f\u6309\u9215\uff0c\u8abf\u6574\u6295\u6ce8\u7684\u7e3d\u984d",
        RULE_G53_1_2: "\u9ede\u64caSPIN\u6309\u9215\u4ee5\u7576\u524d\u7684\u6295\u6ce8\u65cb\u8f49\u8f49\u8ef8",
        RULE_G53_1_3: "\u9ede\u64ca\u81ea\u52d5\u904a\u6232\u6309\u9215\u6703\u958b\u555f\u81ea\u52d5\u904a\u6232\u8a2d\u7f6e\uff0c\u81ea\u52d5\u904a\u6232\u555f\u7528\u6642\uff0c\u9ede\u64ca\u65cb\u8f49\u6309\u9215\u53ef\u505c\u6b62\u81ea\u52d5\u6a21\u5f0f",
        RULE_G53_1_4: "\u9ede\u64ca\u6975\u901f\u904a\u6232\u6309\u9215\uff0c\u6309\u9215\u4eae\u8d77\u6642\u53ef\u958b\u555f\u6975\u901f\u6a21\u5f0f\uff0c\u6975\u901f\u6a21\u5f0f\u53ef\u8207\u81ea\u52d5\u904a\u6232\u540c\u6642\u751f\u6548",
        RULE_G53_1_5: "A\uff1d\u6295\u6ce8\u7e3d\u984d\nB\uff1d\u5716\u6a19\u6d88\u9664\u9846\u6578\u8ce0\u7387(\u8acb\u53c3\u8003\u8ce0\u7387\u8868)\nC\uff1d\u767e\u642d\u5716\u6a19\u52a0\u6210\u500d\u7387",
        RULE_G53_1_7: "\u4e2d\u734e\u91d1\u984d = (A/50)*B*C",
        RULE_G53_4_1: "\u76e4\u9762\u51fa\u73fe1~4\u500b\u3000\u3000\u3000\u53ca\u7372\u5f97\u5927\u734e\u5f69\u91d1\uff0c\u6d3e\u5f69\u5f8c\u7d50\u675f\u6b64\u5c40",
        RULE_G53_4_2: "\u82e5\u76e4\u9762\u7522\u751f\u5f97\u5206\u7d44\u5408\uff0c\u5c07\u6703\u751f\u62101 ~ 2\u500b\u3000\u3000\u3000",
        RULE_G53_4_3: "\u3000\u3000\u3000\u4e0a\u9762\u6709\u6a5f\u6703\u51fa\u73fe\u4e58\u500d:",
        RULE_G53_4_4: "X2\u3001X3\u3001X5\uff08X5\u50c5\u5728\u514d\u8cbb\u904a\u6232\u51fa\u73fe\uff09\u82e5\u4e2d\u734e\u76e4\u9eb5\u5305\u542b\u6709\u4e58\u500d\u7684\u3000\u3000\u3000\uff0c\u5247\u8a72\u6b21\u5f97\u5206\u4e58\u4e0a\u8a72\u6578\u5b57",
        RULE_G53_4_5: "\u82e5\u5305\u542b\u5169\u500b\u4ee5\u4e0a\u6709\u4e58\u500d\u7684\u3000\u3000\u3000\uff0c\u5247\u8a72\u7d44\u5206\u6578\u5c07\u4e58\u4e0a\u53ef\u8b8a\u6a19\u8a18\u7684\u4e58\u500d\u52a0\u7e3d",
        RULE_G53_5_1: "\u76e4\u9762\u4e2d\u4efb\u610f\u4f4d\u7f6e\u51fa\u73fe2\u500b\u4ee5\u4e0a\u3000\u3000\u3000\u6642\uff0c\u89f8\u767c6\u6b21\u514d\u8cbb\u904a\u6232",
        RULE_G53_5_2: "\u76e4\u9762\u4e2d\u4efb\u610f\u4f4d\u7f6e\u51fa\u73fe2\u500b\u4ee5\u4e0a\u3000\u3000\u3000\u6642\uff0c\u53ef\u7372\u5f972\u500d\u4e0b\u6ce8\u5206\u6578\u5f97\u5206",
        RULE_G53_5_3: "\u514d\u8cbb\u904a\u6232\u671f\u9593\u5167\uff0c\u76e4\u9762\u4e2d\u4efb\u610f\u4f4d\u7f6e\u51fa\u73fe2\u500b\u4ee5\u4e0a\u3000\u3000\u3000\u6642\uff0c\u514d\u8cbb\u6b21\u6578+6",
        RULE_G53_5_4: "\u514d\u8cbb\u904a\u6232\u671f\u9593\uff0c\u6bcf\u5c40\u4e00\u958b\u59cb\u5fc5\u6703\u843d\u4e0b2\u500b",
        RULE_G53_6_1: "\u76e4\u9762\u4e0a\u76f8\u9023\u6307\u5b9a\u6578\u91cf\u7684\u76f8\u540c\u5716\u6a19\uff0c\u5373\u53ef\u7372\u5f97\u8a72\u5716\u6a19\u7684\u5f97\u5206\u734e\u52f5\uff0c\u659c\u7dda\u4e0d\u7b97\u76f8\u9023"
      },
      "en-us": {
        TEXT_SLOT_MONEY_LESS_SETTING: "null",
        TEXT_G53_LOADING: "null",
        TEXT_G53_SYMBOL_TIP_WILD: "null",
        TEXT_G53_SYMBOL_TIP_BONUS: "null",
        RULE_G53_GAME_ERROR: "null",
        RULE_G53_1_1: "null",
        RULE_G53_1_2: "null",
        RULE_G53_1_3: "null",
        RULE_G53_1_4: "null",
        RULE_G53_1_5: "null",
        RULE_G53_1_7: "null",
        RULE_G53_4_1: "null",
        RULE_G53_4_2: "null",
        RULE_G53_4_3: "null",
        RULE_G53_4_4: "null",
        RULE_G53_4_5: "null",
        RULE_G53_5_1: "null",
        RULE_G53_5_2: "null",
        RULE_G53_5_3: "null",
        RULE_G53_5_4: "null",
        RULE_G53_6_1: "null"
      }
    };
    cc._RF.pop();
  }, {} ],
  G53_LoadingItem: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "f2cb78HQvVNk4bzi9uPHIbn", "G53_LoadingItem");
    "use strict";
    var __extends = this && this.__extends || function() {
      var extendStatics = function(d, b) {
        extendStatics = Object.setPrototypeOf || {
          __proto__: []
        } instanceof Array && function(d, b) {
          d.__proto__ = b;
        } || function(d, b) {
          for (var p in b) Object.prototype.hasOwnProperty.call(b, p) && (d[p] = b[p]);
        };
        return extendStatics(d, b);
      };
      return function(d, b) {
        extendStatics(d, b);
        function __() {
          this.constructor = d;
        }
        d.prototype = null === b ? Object.create(b) : (__.prototype = b.prototype, new __());
      };
    }();
    var __decorate = this && this.__decorate || function(decorators, target, key, desc) {
      var c = arguments.length, r = c < 3 ? target : null === desc ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
      if ("object" === typeof Reflect && "function" === typeof Reflect.decorate) r = Reflect.decorate(decorators, target, key, desc); else for (var i = decorators.length - 1; i >= 0; i--) (d = decorators[i]) && (r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r);
      return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    var Slot_LoadingItem_1 = require("../../SlotFramework/Common/Slot_LoadingItem");
    var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
    var G53_LoadingItem = function(_super) {
      __extends(G53_LoadingItem, _super);
      function G53_LoadingItem() {
        var _this = null !== _super && _super.apply(this, arguments) || this;
        _this.loadingBarFX = null;
        return _this;
      }
      G53_LoadingItem.prototype.setLoadingBar = function(p_percent) {
        _super.prototype.setLoadingBar.call(this, p_percent);
        this.loadingBarFX.position = cc.v3(526 * p_percent - 300, this.loadingBarFX.y);
      };
      __decorate([ property(cc.Node) ], G53_LoadingItem.prototype, "loadingBarFX", void 0);
      G53_LoadingItem = __decorate([ ccclass ], G53_LoadingItem);
      return G53_LoadingItem;
    }(Slot_LoadingItem_1.default);
    exports.default = G53_LoadingItem;
    cc._RF.pop();
  }, {
    "../../SlotFramework/Common/Slot_LoadingItem": void 0
  } ],
  G53_LoadingUI: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "e6aaeVKVzJHI4drQFvLx/WQ", "G53_LoadingUI");
    "use strict";
    var __extends = this && this.__extends || function() {
      var extendStatics = function(d, b) {
        extendStatics = Object.setPrototypeOf || {
          __proto__: []
        } instanceof Array && function(d, b) {
          d.__proto__ = b;
        } || function(d, b) {
          for (var p in b) Object.prototype.hasOwnProperty.call(b, p) && (d[p] = b[p]);
        };
        return extendStatics(d, b);
      };
      return function(d, b) {
        extendStatics(d, b);
        function __() {
          this.constructor = d;
        }
        d.prototype = null === b ? Object.create(b) : (__.prototype = b.prototype, new __());
      };
    }();
    var __decorate = this && this.__decorate || function(decorators, target, key, desc) {
      var c = arguments.length, r = c < 3 ? target : null === desc ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
      if ("object" === typeof Reflect && "function" === typeof Reflect.decorate) r = Reflect.decorate(decorators, target, key, desc); else for (var i = decorators.length - 1; i >= 0; i--) (d = decorators[i]) && (r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r);
      return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    var ccclass = cc._decorator.ccclass;
    var G53_LoadingUI = function(_super) {
      __extends(G53_LoadingUI, _super);
      function G53_LoadingUI() {
        return null !== _super && _super.apply(this, arguments) || this;
      }
      G53_LoadingUI = __decorate([ ccclass ], G53_LoadingUI);
      return G53_LoadingUI;
    }(cc.Component);
    exports.default = G53_LoadingUI;
    cc._RF.pop();
  }, {} ],
  G53_Loading_InitData: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "33ddfuZS+dPD7fQKQDiJNdV", "G53_Loading_InitData");
    "use strict";
    var __decorate = this && this.__decorate || function(decorators, target, key, desc) {
      var c = arguments.length, r = c < 3 ? target : null === desc ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
      if ("object" === typeof Reflect && "function" === typeof Reflect.decorate) r = Reflect.decorate(decorators, target, key, desc); else for (var i = decorators.length - 1; i >= 0; i--) (d = decorators[i]) && (r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r);
      return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    var ccclass = cc._decorator.ccclass;
    var G53_Loading_InitData = function() {
      function G53_Loading_InitData() {}
      G53_Loading_InitData = __decorate([ ccclass ], G53_Loading_InitData);
      return G53_Loading_InitData;
    }();
    exports.default = G53_Loading_InitData;
    cc._RF.pop();
  }, {} ],
  G53_Loading: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "28a8ekI19VGsawOGRaW/Sed", "G53_Loading");
    "use strict";
    var __extends = this && this.__extends || function() {
      var extendStatics = function(d, b) {
        extendStatics = Object.setPrototypeOf || {
          __proto__: []
        } instanceof Array && function(d, b) {
          d.__proto__ = b;
        } || function(d, b) {
          for (var p in b) Object.prototype.hasOwnProperty.call(b, p) && (d[p] = b[p]);
        };
        return extendStatics(d, b);
      };
      return function(d, b) {
        extendStatics(d, b);
        function __() {
          this.constructor = d;
        }
        d.prototype = null === b ? Object.create(b) : (__.prototype = b.prototype, new __());
      };
    }();
    var __decorate = this && this.__decorate || function(decorators, target, key, desc) {
      var c = arguments.length, r = c < 3 ? target : null === desc ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
      if ("object" === typeof Reflect && "function" === typeof Reflect.decorate) r = Reflect.decorate(decorators, target, key, desc); else for (var i = decorators.length - 1; i >= 0; i--) (d = decorators[i]) && (r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r);
      return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __awaiter = this && this.__awaiter || function(thisArg, _arguments, P, generator) {
      function adopt(value) {
        return value instanceof P ? value : new P(function(resolve) {
          resolve(value);
        });
      }
      return new (P || (P = Promise))(function(resolve, reject) {
        function fulfilled(value) {
          try {
            step(generator.next(value));
          } catch (e) {
            reject(e);
          }
        }
        function rejected(value) {
          try {
            step(generator["throw"](value));
          } catch (e) {
            reject(e);
          }
        }
        function step(result) {
          result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected);
        }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
      });
    };
    var __generator = this && this.__generator || function(thisArg, body) {
      var _ = {
        label: 0,
        sent: function() {
          if (1 & t[0]) throw t[1];
          return t[1];
        },
        trys: [],
        ops: []
      }, f, y, t, g;
      return g = {
        next: verb(0),
        throw: verb(1),
        return: verb(2)
      }, "function" === typeof Symbol && (g[Symbol.iterator] = function() {
        return this;
      }), g;
      function verb(n) {
        return function(v) {
          return step([ n, v ]);
        };
      }
      function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
          if (f = 1, y && (t = 2 & op[0] ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 
          0) : y.next) && !(t = t.call(y, op[1])).done) return t;
          (y = 0, t) && (op = [ 2 & op[0], t.value ]);
          switch (op[0]) {
           case 0:
           case 1:
            t = op;
            break;

           case 4:
            _.label++;
            return {
              value: op[1],
              done: false
            };

           case 5:
            _.label++;
            y = op[1];
            op = [ 0 ];
            continue;

           case 7:
            op = _.ops.pop();
            _.trys.pop();
            continue;

           default:
            if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (6 === op[0] || 2 === op[0])) {
              _ = 0;
              continue;
            }
            if (3 === op[0] && (!t || op[1] > t[0] && op[1] < t[3])) {
              _.label = op[1];
              break;
            }
            if (6 === op[0] && _.label < t[1]) {
              _.label = t[1];
              t = op;
              break;
            }
            if (t && _.label < t[2]) {
              _.label = t[2];
              _.ops.push(op);
              break;
            }
            t[2] && _.ops.pop();
            _.trys.pop();
            continue;
          }
          op = body.call(thisArg, _);
        } catch (e) {
          op = [ 6, e ];
          y = 0;
        } finally {
          f = t = 0;
        }
        if (5 & op[0]) throw op[1];
        return {
          value: op[0] ? op[1] : void 0,
          done: true
        };
      }
    };
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    var AudioManager_1 = require("../../Common/Tools/AudioManager/AudioManager");
    var DesktopBrowserTransform_1 = require("../../Common/Tools/DesktopBrowserTransform/DesktopBrowserTransform");
    var Slot_LoadingManager_1 = require("../../SlotFramework/Loading/Slot_LoadingManager");
    var G53_DataManager_1 = require("../Common/G53_DataManager");
    var G53_DynamicPopUpPanelManager_1 = require("../Common/G53_DynamicPopUpPanelManager");
    var G53_LoadingItem_1 = require("../Common/G53_LoadingItem");
    var G53_SocketManager_1 = require("../Common/Socket/G53_SocketManager");
    var G53_Language_1 = require("../Localization/G53_Language");
    var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
    var PRELOAD_RESOURCE_LIST = [ "Resources" ];
    var PRELOAD_SCENE_LIST = [ "G53_Lobby", "G53_Game" ];
    var preloadResources = [ {
      bundle: "G53",
      scene: {
        path: PRELOAD_SCENE_LIST
      },
      resource: {
        path: PRELOAD_RESOURCE_LIST
      },
      audio: {
        csvPath: "Resources/Audio - G53",
        path: [ "Resources/music", "Resources/sound" ]
      },
      loadRate: .97
    } ];
    var G53_Loading = function(_super) {
      __extends(G53_Loading, _super);
      function G53_Loading() {
        var _this = null !== _super && _super.apply(this, arguments) || this;
        _this.loadingItem = null;
        _this.languageMap = G53_Language_1.G53_Language;
        return _this;
      }
      G53_Loading.prototype.onLoad = function() {
        DesktopBrowserTransform_1.default.needFixScreen = true;
        _super.prototype.onLoad.call(this);
        cc.game.setFrameRate(60);
        cc.view.enableAntiAlias(true);
        cc.dynamicAtlasManager.enabled = false;
        cc.macro.ENABLE_MULTI_TOUCH = false;
      };
      G53_Loading.prototype.start = function() {
        _super.prototype.start.call(this);
        this.loadingItem.show();
        this.processAsync().catch(function(err) {
          return console.error(err);
        });
      };
      G53_Loading.prototype.processAsync = function() {
        return __awaiter(this, void 0, Promise, function() {
          return __generator(this, function(_a) {
            switch (_a.label) {
             case 0:
              AudioManager_1.AudioManager.delInstance();
              G53_DataManager_1.default.deleteInstance();
              return [ 4, G53_SocketManager_1.default.getInstance().connect() ];

             case 1:
              _a.sent();
              return [ 4, this.initGameConfig(G53_DataManager_1.default.getInstance()) ];

             case 2:
              _a.sent();
              return [ 4, _super.prototype.processAsync.call(this, preloadResources) ];

             case 3:
              _a.sent();
              this.processLogin(G53_SocketManager_1.default.getInstance());
              return [ 2 ];
            }
          });
        });
      };
      G53_Loading.prototype.onAssetLoadComplete = function(prefabs) {
        G53_DynamicPopUpPanelManager_1.default.getInstance().prestore(prefabs);
      };
      __decorate([ property({
        type: G53_LoadingItem_1.default,
        override: true
      }) ], G53_Loading.prototype, "loadingItem", void 0);
      G53_Loading = __decorate([ ccclass ], G53_Loading);
      return G53_Loading;
    }(Slot_LoadingManager_1.default);
    exports.default = G53_Loading;
    cc._RF.pop();
  }, {
    "../../Common/Tools/AudioManager/AudioManager": void 0,
    "../../Common/Tools/DesktopBrowserTransform/DesktopBrowserTransform": void 0,
    "../../SlotFramework/Loading/Slot_LoadingManager": void 0,
    "../Common/G53_DataManager": "G53_DataManager",
    "../Common/G53_DynamicPopUpPanelManager": "G53_DynamicPopUpPanelManager",
    "../Common/G53_LoadingItem": "G53_LoadingItem",
    "../Common/Socket/G53_SocketManager": "G53_SocketManager",
    "../Localization/G53_Language": "G53_Language"
  } ],
  G53_LobbyUI: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "f0ae5EZWAxKTo1LOoWTnDk4", "G53_LobbyUI");
    "use strict";
    var __extends = this && this.__extends || function() {
      var extendStatics = function(d, b) {
        extendStatics = Object.setPrototypeOf || {
          __proto__: []
        } instanceof Array && function(d, b) {
          d.__proto__ = b;
        } || function(d, b) {
          for (var p in b) Object.prototype.hasOwnProperty.call(b, p) && (d[p] = b[p]);
        };
        return extendStatics(d, b);
      };
      return function(d, b) {
        extendStatics(d, b);
        function __() {
          this.constructor = d;
        }
        d.prototype = null === b ? Object.create(b) : (__.prototype = b.prototype, new __());
      };
    }();
    var __decorate = this && this.__decorate || function(decorators, target, key, desc) {
      var c = arguments.length, r = c < 3 ? target : null === desc ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
      if ("object" === typeof Reflect && "function" === typeof Reflect.decorate) r = Reflect.decorate(decorators, target, key, desc); else for (var i = decorators.length - 1; i >= 0; i--) (d = decorators[i]) && (r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r);
      return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    var Slot_LobbyUI_1 = require("../../../SlotFramework/Lobby/Slot_LobbyUI");
    var ccclass = cc._decorator.ccclass;
    var G53_LobbyUI = function(_super) {
      __extends(G53_LobbyUI, _super);
      function G53_LobbyUI() {
        return null !== _super && _super.apply(this, arguments) || this;
      }
      G53_LobbyUI.prototype.init = function(p_data) {
        _super.prototype.init.call(this, p_data);
      };
      G53_LobbyUI.prototype.setPlayerIcon = function(p_iconIndex) {};
      G53_LobbyUI = __decorate([ ccclass ], G53_LobbyUI);
      return G53_LobbyUI;
    }(Slot_LobbyUI_1.default);
    exports.default = G53_LobbyUI;
    cc._RF.pop();
  }, {
    "../../../SlotFramework/Lobby/Slot_LobbyUI": void 0
  } ],
  G53_Lobby: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "233ddHKighMmpxNf6lDydgB", "G53_Lobby");
    "use strict";
    var __extends = this && this.__extends || function() {
      var extendStatics = function(d, b) {
        extendStatics = Object.setPrototypeOf || {
          __proto__: []
        } instanceof Array && function(d, b) {
          d.__proto__ = b;
        } || function(d, b) {
          for (var p in b) Object.prototype.hasOwnProperty.call(b, p) && (d[p] = b[p]);
        };
        return extendStatics(d, b);
      };
      return function(d, b) {
        extendStatics(d, b);
        function __() {
          this.constructor = d;
        }
        d.prototype = null === b ? Object.create(b) : (__.prototype = b.prototype, new __());
      };
    }();
    var __decorate = this && this.__decorate || function(decorators, target, key, desc) {
      var c = arguments.length, r = c < 3 ? target : null === desc ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
      if ("object" === typeof Reflect && "function" === typeof Reflect.decorate) r = Reflect.decorate(decorators, target, key, desc); else for (var i = decorators.length - 1; i >= 0; i--) (d = decorators[i]) && (r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r);
      return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    var LocalizationManager_1 = require("../../Common/Tools/Localization/LocalizationManager");
    var Slot_LobbyManager_InitData_1 = require("../../SlotFramework/Lobby/Data/Slot_LobbyManager_InitData");
    var Slot_LobbyUI_InitData_1 = require("../../SlotFramework/Lobby/Data/Slot_LobbyUI_InitData");
    var Slot_LobbyManager_1 = require("../../SlotFramework/Lobby/Slot_LobbyManager");
    var G53_DataManager_1 = require("../Common/G53_DataManager");
    var G53_SocketManager_1 = require("../Common/Socket/G53_SocketManager");
    var G53_LobbyUI_1 = require("./View/G53_LobbyUI");
    var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
    var G53_Lobby = function(_super) {
      __extends(G53_Lobby, _super);
      function G53_Lobby() {
        var _this = null !== _super && _super.apply(this, arguments) || this;
        _this.ui = null;
        _this.data = null;
        _this.socket = null;
        return _this;
      }
      G53_Lobby.prototype.start = function() {
        _super.prototype.start.call(this);
        this.socket = G53_SocketManager_1.default.getInstance();
        var _data = new Slot_LobbyManager_InitData_1.default();
        _data.bundleName = "G" + G53_DataManager_1.default.getInstance().gameID;
        _data.bundlePath = "" + G53_DataManager_1.default.getInstance().path;
        this.init(_data);
        this.onFastJoinGame();
      };
      G53_Lobby.prototype.onDestroy = function() {
        this.removeEvents();
      };
      G53_Lobby.prototype.init = function(p_data) {
        this.data = G53_DataManager_1.default.getInstance();
        this.data.init();
        this.registerEvents();
        _super.prototype.init.call(this, p_data);
        var _data = new Slot_LobbyUI_InitData_1.default();
        _data.bundleName = p_data.bundleName;
        _data.bundlePath = p_data.bundlePath;
        this.ui.init(_data);
      };
      G53_Lobby.prototype.registerEvents = function() {
        var _this = this;
        G53_SocketManager_1.default.getInstance().setSelfEnterEventCallback(function(data) {
          _this.onSelfEnterEvent(data);
        });
      };
      G53_Lobby.prototype.removeEvents = function() {
        G53_SocketManager_1.default.getInstance().setSelfEnterEventCallback(null);
      };
      G53_Lobby.prototype.onSelfEnterEvent = function(data) {
        G53_DataManager_1.default.getInstance().coin = G53_DataManager_1.default.getInstance().getClientMoney(data.Coin);
      };
      G53_Lobby.prototype.onBackButton = function() {
        console.log("\u96e2\u958b");
      };
      G53_Lobby.prototype.onOpenHistoryButton = function() {
        console.log("\u958b\u555f\u6b77\u53f2\u7d00\u9304");
      };
      G53_Lobby.prototype.onOpenMenuButton = function() {
        this.ui.openMenu();
      };
      G53_Lobby.prototype.onCloseMenuButton = function() {
        this.ui.closeMenu();
      };
      G53_Lobby.prototype.onChangeMachine = function() {
        console.log("\u63db\u4e00\u6279");
      };
      G53_Lobby.prototype.onFastJoinGame = function() {
        var _this = this;
        console.log("\u5feb\u901f\u52a0\u5165");
        this.socket.setOnFastChoseRoomEvent().then(function() {
          cc.director.loadScene("G53_Game");
        }).catch(function(error) {
          error.code == GameClient.errCode.RESPONSE_TIMED_OUT && _this.socket.showErrorAlert(LocalizationManager_1.default.getInstance().get("ERROR_SOCKET_TIME_OUT"), null, function() {
            G53_SocketManager_1.default.getInstance().login();
          });
        });
      };
      G53_Lobby.prototype.onChangePlayerHead = function() {
        console.log("\u63db\u73a9\u5bb6\u982d\u50cf");
      };
      G53_Lobby.prototype.setPlayerInfo = function() {
        var _money = this.data.coin;
        this.ui.setPlayerInfo(this.data.nickname, this.data.avatarID, _money);
      };
      G53_Lobby.prototype.setRoomData = function() {
        console.log("\u8a2d\u5b9a\u623f\u9593\u8cc7\u6599");
      };
      __decorate([ property(G53_LobbyUI_1.default) ], G53_Lobby.prototype, "ui", void 0);
      G53_Lobby = __decorate([ ccclass ], G53_Lobby);
      return G53_Lobby;
    }(Slot_LobbyManager_1.default);
    exports.default = G53_Lobby;
    cc._RF.pop();
  }, {
    "../../Common/Tools/Localization/LocalizationManager": void 0,
    "../../SlotFramework/Lobby/Data/Slot_LobbyManager_InitData": void 0,
    "../../SlotFramework/Lobby/Data/Slot_LobbyUI_InitData": void 0,
    "../../SlotFramework/Lobby/Slot_LobbyManager": void 0,
    "../Common/G53_DataManager": "G53_DataManager",
    "../Common/Socket/G53_SocketManager": "G53_SocketManager",
    "./View/G53_LobbyUI": "G53_LobbyUI"
  } ],
  G53_MusicOptionPanel: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "1d9d415C69EXJP3q21QuNA/", "G53_MusicOptionPanel");
    "use strict";
    var __extends = this && this.__extends || function() {
      var extendStatics = function(d, b) {
        extendStatics = Object.setPrototypeOf || {
          __proto__: []
        } instanceof Array && function(d, b) {
          d.__proto__ = b;
        } || function(d, b) {
          for (var p in b) Object.prototype.hasOwnProperty.call(b, p) && (d[p] = b[p]);
        };
        return extendStatics(d, b);
      };
      return function(d, b) {
        extendStatics(d, b);
        function __() {
          this.constructor = d;
        }
        d.prototype = null === b ? Object.create(b) : (__.prototype = b.prototype, new __());
      };
    }();
    var __decorate = this && this.__decorate || function(decorators, target, key, desc) {
      var c = arguments.length, r = c < 3 ? target : null === desc ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
      if ("object" === typeof Reflect && "function" === typeof Reflect.decorate) r = Reflect.decorate(decorators, target, key, desc); else for (var i = decorators.length - 1; i >= 0; i--) (d = decorators[i]) && (r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r);
      return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    var Slot_MusicOptionPanel_1 = require("../../../SlotFramework/Game/Panel/Slot_MusicOptionPanel");
    var ccclass = cc._decorator.ccclass;
    var G53_MusicOptionPanel = function(_super) {
      __extends(G53_MusicOptionPanel, _super);
      function G53_MusicOptionPanel() {
        return null !== _super && _super.apply(this, arguments) || this;
      }
      G53_MusicOptionPanel = __decorate([ ccclass ], G53_MusicOptionPanel);
      return G53_MusicOptionPanel;
    }(Slot_MusicOptionPanel_1.default);
    exports.default = G53_MusicOptionPanel;
    cc._RF.pop();
  }, {
    "../../../SlotFramework/Game/Panel/Slot_MusicOptionPanel": void 0
  } ],
  G53_Reel: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "d11e0846sdJFrCP4FbZebBy", "G53_Reel");
    "use strict";
    var __extends = this && this.__extends || function() {
      var extendStatics = function(d, b) {
        extendStatics = Object.setPrototypeOf || {
          __proto__: []
        } instanceof Array && function(d, b) {
          d.__proto__ = b;
        } || function(d, b) {
          for (var p in b) Object.prototype.hasOwnProperty.call(b, p) && (d[p] = b[p]);
        };
        return extendStatics(d, b);
      };
      return function(d, b) {
        extendStatics(d, b);
        function __() {
          this.constructor = d;
        }
        d.prototype = null === b ? Object.create(b) : (__.prototype = b.prototype, new __());
      };
    }();
    var __decorate = this && this.__decorate || function(decorators, target, key, desc) {
      var c = arguments.length, r = c < 3 ? target : null === desc ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
      if ("object" === typeof Reflect && "function" === typeof Reflect.decorate) r = Reflect.decorate(decorators, target, key, desc); else for (var i = decorators.length - 1; i >= 0; i--) (d = decorators[i]) && (r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r);
      return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    var Slot_Reel_1 = require("../../../SlotFramework/Game/view/Slot_Reel");
    var AudioManager_1 = require("../../../Common/Tools/AudioManager/AudioManager");
    var Slot_Reel_InitData_1 = require("../../../SlotFramework/Game/Data/Slot_Reel_InitData");
    var ccclass = cc._decorator.ccclass;
    var STATUS;
    (function(STATUS) {
      STATUS[STATUS["IDLE"] = 0] = "IDLE";
      STATUS[STATUS["SCROLLING_UP"] = 1] = "SCROLLING_UP";
      STATUS[STATUS["SCROLLING"] = 2] = "SCROLLING";
      STATUS[STATUS["OMEN_SCROLLING"] = 3] = "OMEN_SCROLLING";
      STATUS[STATUS["REBOUND_ALIGNMENT"] = 4] = "REBOUND_ALIGNMENT";
      STATUS[STATUS["SLOWING"] = 5] = "SLOWING";
      STATUS[STATUS["SLOW_WAIT"] = 6] = "SLOW_WAIT";
      STATUS[STATUS["REBOUND"] = 7] = "REBOUND";
      STATUS[STATUS["REBOUND_WAIT"] = 8] = "REBOUND_WAIT";
      STATUS[STATUS["REWARD_EFFECTING"] = 9] = "REWARD_EFFECTING";
      STATUS[STATUS["DROP_OUT_START"] = 10] = "DROP_OUT_START";
      STATUS[STATUS["DROP_OUT_WAIT"] = 11] = "DROP_OUT_WAIT";
      STATUS[STATUS["DROP_IN_START"] = 12] = "DROP_IN_START";
      STATUS[STATUS["DROP_IN_WAIT"] = 13] = "DROP_IN_WAIT";
      STATUS[STATUS["SYMBOL_DISAPPEAR_EFFECTING"] = 14] = "SYMBOL_DISAPPEAR_EFFECTING";
      STATUS[STATUS["SYMBOL_DISAPPEAR_DROP_EFFECTING"] = 15] = "SYMBOL_DISAPPEAR_DROP_EFFECTING";
      STATUS[STATUS["CHANGE_SYMBOL_EFFECTING"] = 16] = "CHANGE_SYMBOL_EFFECTING";
    })(STATUS || (STATUS = {}));
    var G53_Reel = function(_super) {
      __extends(G53_Reel, _super);
      function G53_Reel() {
        var _this = null !== _super && _super.apply(this, arguments) || this;
        _this.symbolsList = [];
        _this.nextReels = [];
        _this.data = null;
        _this.isPlayDropInAudio = false;
        return _this;
      }
      G53_Reel.prototype.init = function(p_data) {
        this.data = p_data;
        _super.prototype.init.call(this, p_data);
      };
      G53_Reel.prototype.update = function(dt) {
        switch (this.status) {
         case STATUS.SYMBOL_DISAPPEAR_EFFECTING:
          this.symbolDisappearEffecting();
          break;

         case STATUS.SYMBOL_DISAPPEAR_DROP_EFFECTING:
          this.symbolDisappearDropEffecting();
          break;

         case STATUS.CHANGE_SYMBOL_EFFECTING:
          this.changeSymbolEffecting();
          break;

         case STATUS.DROP_IN_WAIT:
          this.dropInWait();
          break;

         default:
          _super.prototype.update.call(this, dt);
        }
      };
      G53_Reel.prototype.showSymbolDisappearEffect = function(p_winPosition, p_nextReels, p_reelTime, p_symbolTime, p_symbolDisappearFinishDelayTime) {
        this.nextReels = p_nextReels;
        this.reel = p_nextReels;
        var _delayCount = 0;
        var isPlayAudio = false;
        for (var i = 0; i < p_winPosition.length; i++) if (1 == p_winPosition[i]) for (var j = 0; j < this.symbolsList.length; j++) if (this.symbolsList[j].getOrder() - 2 == i) {
          this.symbolsList[j].setZOrder(10);
          this.symbolsList[j].playDisappearEffect(p_reelTime + p_symbolTime * _delayCount, p_symbolDisappearFinishDelayTime);
          _delayCount++;
          isPlayAudio = true;
        }
        isPlayAudio && this.scheduleOnce(function() {
          AudioManager_1.AudioManager.instance.playAudioEvent("SymbolDisappearEffect");
        }, p_reelTime);
        this.status = STATUS.SYMBOL_DISAPPEAR_EFFECTING;
      };
      G53_Reel.prototype.symbolDisappearEffecting = function() {
        for (var i = 0; i < this.symbolsList.length; i++) if (this.symbolsList[i].isDisappearEffecting()) return;
        this.status = STATUS.IDLE;
      };
      G53_Reel.prototype.isSymbolDisappearEffecting = function() {
        return this.status == STATUS.SYMBOL_DISAPPEAR_EFFECTING;
      };
      G53_Reel.prototype.ShowSymbolDisappearDropEffect = function(p_winPosition) {
        var _disppearSymbolOrder = [];
        for (var i = 0; i < p_winPosition.length; i++) 1 == p_winPosition[i] && _disppearSymbolOrder.push(i + 2);
        this.undisappearDroping(_disppearSymbolOrder, p_winPosition);
        this.disppearSymbolToTop(_disppearSymbolOrder);
        this.changeDisppear(_disppearSymbolOrder);
        this.disappearDroping(_disppearSymbolOrder);
        this.status = STATUS.SYMBOL_DISAPPEAR_DROP_EFFECTING;
      };
      G53_Reel.prototype.symbolDisappearDropEffecting = function() {
        for (var i = 0; i < this.symbolsList.length; i++) if (this.symbolsList[i].isDropEffecting()) return;
        this.status = STATUS.IDLE;
      };
      G53_Reel.prototype.isSymbolDisappearDropEffecting = function() {
        return this.status == STATUS.SYMBOL_DISAPPEAR_DROP_EFFECTING;
      };
      G53_Reel.prototype.undisappearDroping = function(p_disppearSymbolOrder, p_winPosition) {
        var _symbolHeight = this.symbolsList[0].node.getContentSize().height * this.symbolsList[0].node.scaleY;
        for (var i = 2; i < this.symbolsList.length; i++) if (0 == p_winPosition[i - 2]) {
          var _symbolOrder = this.getSymbolOrder(i);
          var targetOreder = _symbolOrder;
          var _targetPosition = null;
          var _dropCount = 0;
          for (var j = 0; j < p_disppearSymbolOrder.length; j++) if (i < p_disppearSymbolOrder[j]) {
            targetOreder++;
            _targetPosition = this.getSymbolPosition(targetOreder);
            _dropCount++;
          }
          if (_dropCount > 0) {
            _targetPosition.y += _symbolHeight + this.symbolSpacing;
            this.symbolsList[_symbolOrder].playDropEffect(0, _targetPosition);
          }
        }
      };
      G53_Reel.prototype.disppearSymbolToTop = function(p_disppearSymbolOrder) {
        var _symbolHeight = this.symbolsList[0].node.getContentSize().height * this.symbolsList[0].node.scaleY;
        for (var i = 0; i < p_disppearSymbolOrder.length; i++) {
          var _targetPosition = this.getSymbolPosition(0);
          _targetPosition.y += (p_disppearSymbolOrder.length - i) * (_symbolHeight + this.symbolSpacing);
          var _symbolOrder = this.getSymbolOrder(p_disppearSymbolOrder[i]);
          this.symbolsList[_symbolOrder].node.setPosition(_targetPosition);
        }
      };
      G53_Reel.prototype.changeDisppear = function(p_disppearSymbolOrder) {
        for (var i = 0; i < p_disppearSymbolOrder.length; i++) {
          var _newSymbolIndex = this.nextReels[i];
          var _symbolOrder = this.getSymbolOrder(p_disppearSymbolOrder[i]);
          var _symbolValue = _newSymbolIndex;
          this.symbolsList[_symbolOrder].setData(_newSymbolIndex, _symbolValue, this.isFreeGame);
        }
      };
      G53_Reel.prototype.disappearDroping = function(p_disppearSymbolOrder) {
        var _symbolHeight = this.symbolsList[0].node.getContentSize().height * this.symbolsList[0].node.scaleY;
        for (var i = 0; i < p_disppearSymbolOrder.length; i++) {
          var _targetPosition = this.getSymbolPosition(i + 2);
          var _symbolOrder = this.getSymbolOrder(p_disppearSymbolOrder[i]);
          _targetPosition.y += _symbolHeight + this.symbolSpacing;
          this.symbolsList[_symbolOrder].playDropEffect(0, _targetPosition);
        }
      };
      G53_Reel.prototype.correctionSymbolOrder = function() {
        for (var i = 0; i < this.symbolsList.length; i++) for (var j = 0; j < this.symbolsList.length; j++) {
          var _target = this.symbolPrefab.getContentSize().height * this.symbolsList[0].node.scaleY + this.symbolSpacing;
          Math.abs(this.symbolsList[i].node.position.y - (this.getSymbolPosition(j).y + _target)) < 10 && this.symbolsList[i].setOrder(j);
        }
        this.symbolsList.sort(function(a, b) {
          return a.getOrder() - b.getOrder();
        });
      };
      G53_Reel.prototype.changeSymbolEffect = function(p_changePos, p_targetSymbol) {
        var _targetSymbol = 0;
        _targetSymbol = p_changePos + 2;
        for (var i = 0; i < this.symbolsList.length; i++) this.symbolsList[i].getOrder() == _targetSymbol && this.symbolsList[i].changeSymbolEffect(p_targetSymbol);
        this.status = STATUS.CHANGE_SYMBOL_EFFECTING;
      };
      G53_Reel.prototype.changeSymbolEffecting = function() {
        for (var i = 0; i < this.symbolsList.length; i++) if (this.symbolsList[i].isChangeSymbolEffecting()) return;
        this.status = STATUS.IDLE;
      };
      G53_Reel.prototype.isChangeSymbolEffecting = function() {
        return this.status == STATUS.CHANGE_SYMBOL_EFFECTING;
      };
      G53_Reel.prototype.dropInStart = function(p_dt) {
        var _this = this;
        this.isPlayDropInAudio = false;
        this.tempDropInTime += p_dt;
        if (this.tempDropInTime < this.dropInStartDelayTime) return;
        this.status = STATUS.DROP_IN_WAIT;
        var _loop_1 = function(i) {
          var _symbolHeight = this_1.symbolsList[i].node.getContentSize().height * this_1.symbolsList[0].node.scaleY;
          var _targetPosition = this_1.getSymbolPosition(i);
          var _movePositionY = (_symbolHeight + this_1.symbolSpacing) * this_1.symbolsList.length;
          var _shakeAngle = Math.random() > .5 ? this_1.dropInReboundAngle : -this_1.dropInReboundAngle;
          var _reboundTime = .5 * this_1.dropInReboundTime;
          var _reboundByPosOut = cc.v3(0, -this_1.dropInReboundHeight);
          var _reboundByPosIn = cc.v3(0, +this_1.dropInReboundHeight);
          var _reboundAngleTime = .5 * this_1.dropInReboundAngleTime;
          _targetPosition.y += _symbolHeight + this_1.symbolSpacing;
          this_1.symbolsList[i].node.stopAllActions();
          this_1.symbolsList[i].node.setPosition(_targetPosition.x, _targetPosition.y + _movePositionY);
          this_1.symbolsList[i].node.opacity = 255;
          this_1.symbolsList[i].setOrder(i);
          this_1.changeSymbol(i, true, this_1.reel, this_1.isFreeGame);
          this_1.allSymbolPlayIdleEffect(true);
          cc.tween(this_1.symbolsList[i].node).delay((this_1.symbolsList.length - i) * this_1.dropInDelayTime).to(this_1.dropInTime, {
            position: cc.v3(_targetPosition.x, _targetPosition.y)
          }, {
            easing: this_1.dropInEasing
          }).parallel(cc.tween().sequence(cc.tween().by(_reboundTime, {
            position: _reboundByPosOut
          }, {
            easing: this_1.dropInReboundEasingOut
          }), cc.tween().by(_reboundTime, {
            position: _reboundByPosIn
          }, {
            easing: this_1.dropInReboundEasingIn
          })), cc.tween().sequence(cc.tween().by(_reboundAngleTime, {
            angle: _shakeAngle
          }, {
            easing: this_1.dropInReboundEasingOut
          }), cc.tween().by(_reboundAngleTime, {
            angle: -_shakeAngle
          }, {
            easing: this_1.dropInReboundEasingIn
          }))).call(function() {
            if (2 == i) {
              _this.status = STATUS.IDLE;
              _this.hideOutRangeSymbol();
            }
          }).start();
        };
        var this_1 = this;
        for (var i = this.symbolsList.length - 1; i >= 0; i--) _loop_1(i);
      };
      G53_Reel.prototype.dropInWait = function() {
        if (!this.isPlayDropInAudio) {
          this.isPlayDropInAudio = true;
          if (0 != this.data.reelIndex) return;
          AudioManager_1.AudioManager.instance.playAudioEvent("ReelScrollStartAgain");
        }
      };
      G53_Reel.prototype.changeSymbol = function(p_symbolOrder, p_Alignment, p_reel, p_isFreeGame) {
        void 0 === p_reel && (p_reel = []);
        void 0 === p_isFreeGame && (p_isFreeGame = false);
        if (p_reel.length > 0) {
          var _reel = p_reel;
          this.reelMode !== Slot_Reel_InitData_1.ReelMode.DROP && this.reelMode !== Slot_Reel_InitData_1.ReelMode.SCROLL || (_reel = this.reelAbove ? [ 1, this.reelAbove ].concat(p_reel) : [ 1, 1 ].concat(p_reel));
          for (var j = 0; j < this.symbolsList.length; j++) if (this.symbolsList[j].getOrder() == p_symbolOrder) {
            var _symbolIndex = this.symbolsList[j].getNowIndex();
            this.symbolsList[j].setData(_symbolIndex, _reel[p_symbolOrder], p_isFreeGame);
          }
        }
      };
      G53_Reel.prototype.getSymbolReWardPos = function(p_winPosition) {
        var list = [];
        for (var i = 0; i < p_winPosition.length; i++) if (1 == p_winPosition[i]) for (var j = 0; j < this.symbolsList.length; j++) this.symbolsList[j].getOrder() - 2 == i && list.push(this.symbolsList[j].node.position);
        return list;
      };
      G53_Reel.prototype.getWildMulPos = function(p_winPosition) {
        var list = [];
        for (var i = 0; i < p_winPosition.length; i++) if (1 == p_winPosition[i]) for (var j = 0; j < this.symbolsList.length; j++) this.symbolsList[j].getOrder() - 2 == i && this.symbolsList[j].getSymbolMul() > 0 && list.push({
          node: this.symbolsList[j].node,
          mul: this.symbolsList[j].getSymbolMul()
        });
        return list;
      };
      G53_Reel.prototype.reelsSpacingY = function() {
        switch (this.status) {
         case STATUS.SCROLLING_UP:
         case STATUS.SCROLLING:
         case STATUS.OMEN_SCROLLING:
         case STATUS.REBOUND_ALIGNMENT:
          this.orientationSyncY();
          break;

         case STATUS.REBOUND_WAIT:
         case STATUS.REBOUND:
          this.rebound();
          break;

         case STATUS.SLOWING:
         case STATUS.SLOW_WAIT:
          this.slowing();
          break;

         case STATUS.DROP_OUT_START:
         case STATUS.DROP_OUT_WAIT:
          for (var i = 0; i < this.symbolsList.length; i++) {
            cc.Tween.stopAllByTarget(this.symbolsList[i].node);
            var _symbolHeight = this.symbolsList[i].node.getContentSize().height * this.symbolsList[i].node.scaleY;
            var _movePositionY = (_symbolHeight + this.symbolSpacing) * this.symbolsList.length;
            var _targetPosition = this.getSymbolPosition(i);
            _targetPosition.y -= _movePositionY;
            this.symbolsList[i].setSymbolY(_targetPosition.y);
          }
          this.dropInStartDelayTime = this.dropOutStartDelayTime;
          this.status = STATUS.DROP_IN_START;
          break;

         case STATUS.DROP_IN_START:
         case STATUS.DROP_IN_WAIT:
         case STATUS.SYMBOL_DISAPPEAR_DROP_EFFECTING:
          this.symbolsList.forEach(function(symbol) {
            return symbol.skipDropEffect();
          });
          if (this.status === STATUS.DROP_IN_START || this.status === STATUS.DROP_IN_WAIT) for (var i = 2, j = 0; i < this.symbolsList.length; i++, 
          j++) {
            var index = this.getSymbolOrder(i);
            this.symbolsList[index].setData(index, this.reel[j], this.isFreeGame);
          } else for (var i = 2, j = 0; i < this.symbolsList.length; i++, j++) {
            var index = this.getSymbolOrder(i);
            this.symbolsList[index].setData(index, this.nextReels[j], this.isFreeGame);
          }

         default:
          for (var i = 0; i < this.symbolsList.length; i++) {
            var _symbolCount = this.symbolsList.length;
            var _addtion = 0;
            _symbolCount % 2 == 0 && (_addtion = -this.symbolPrefab.getContentSize().height * this.symbolsList[0].node.scaleY / 2);
            var _middleOrder = Math.floor(_symbolCount / 2);
            var _times = -(this.symbolsList[i].getOrder() - _middleOrder);
            var _spacing = this.symbolPrefab.getContentSize().height * this.symbolsList[0].node.scaleY + this.symbolSpacing;
            var offset = _spacing * _times + _addtion;
            this.symbolsList[i].setSymbolY(offset + this.symbolPrefab.getContentSize().height * this.symbolsList[0].node.scaleY + this.symbolSpacing);
          }
          this.status !== STATUS.DROP_IN_START && this.status !== STATUS.DROP_IN_WAIT || (this.status = STATUS.IDLE);
        }
      };
      G53_Reel = __decorate([ ccclass ], G53_Reel);
      return G53_Reel;
    }(Slot_Reel_1.default);
    exports.default = G53_Reel;
    cc._RF.pop();
  }, {
    "../../../Common/Tools/AudioManager/AudioManager": void 0,
    "../../../SlotFramework/Game/Data/Slot_Reel_InitData": void 0,
    "../../../SlotFramework/Game/view/Slot_Reel": void 0
  } ],
  G53_RulePanel: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "62df4A+vbtKCrLgjw/hCkUT", "G53_RulePanel");
    "use strict";
    var __extends = this && this.__extends || function() {
      var extendStatics = function(d, b) {
        extendStatics = Object.setPrototypeOf || {
          __proto__: []
        } instanceof Array && function(d, b) {
          d.__proto__ = b;
        } || function(d, b) {
          for (var p in b) Object.prototype.hasOwnProperty.call(b, p) && (d[p] = b[p]);
        };
        return extendStatics(d, b);
      };
      return function(d, b) {
        extendStatics(d, b);
        function __() {
          this.constructor = d;
        }
        d.prototype = null === b ? Object.create(b) : (__.prototype = b.prototype, new __());
      };
    }();
    var __decorate = this && this.__decorate || function(decorators, target, key, desc) {
      var c = arguments.length, r = c < 3 ? target : null === desc ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
      if ("object" === typeof Reflect && "function" === typeof Reflect.decorate) r = Reflect.decorate(decorators, target, key, desc); else for (var i = decorators.length - 1; i >= 0; i--) (d = decorators[i]) && (r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r);
      return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    var Slot_RulePanel_1 = require("../../../SlotFramework/Game/Panel/Slot_RulePanel");
    var AudioManager_1 = require("../../../Common/Tools/AudioManager/AudioManager");
    var G53_DataManager_1 = require("../../Common/G53_DataManager");
    var ccclass = cc._decorator.ccclass;
    var G53_RulePanel = function(_super) {
      __extends(G53_RulePanel, _super);
      function G53_RulePanel() {
        var _this = null !== _super && _super.apply(this, arguments) || this;
        _this.data = null;
        return _this;
      }
      G53_RulePanel.prototype.init = function() {
        this.data = G53_DataManager_1.default.getInstance();
        _super.prototype.init.call(this);
      };
      G53_RulePanel.prototype.onNextRulePage = function() {
        if (this.status == Slot_RulePanel_1.STATUS.CHANGE_PAGE) return;
        AudioManager_1.AudioManager.instance.playAudioEvent("NextRulePage");
        _super.prototype.onNextRulePage.call(this);
      };
      G53_RulePanel.prototype.onPreRulePage = function() {
        if (this.status == Slot_RulePanel_1.STATUS.CHANGE_PAGE) return;
        AudioManager_1.AudioManager.instance.playAudioEvent("PreRulePage");
        _super.prototype.onPreRulePage.call(this);
      };
      G53_RulePanel.prototype.open = function() {
        _super.prototype.open.call(this);
      };
      G53_RulePanel.prototype.close = function() {
        _super.prototype.close.call(this);
        AudioManager_1.AudioManager.instance.playAudioEvent("CloseRule");
      };
      G53_RulePanel.prototype.initMagnification = function() {
        for (var i = 0; i < this.oddsLabelGroup.length; i++) {
          var oddsLabel = this.oddsLabelGroup[i];
          var symbolID = parseInt(oddsLabel.node.name.split("_")[1]);
          var level = parseInt(oddsLabel.node.name.split("_")[2]);
          this.changeMagnification(symbolID, i, level - 1);
        }
      };
      G53_RulePanel.prototype.changeMagnification = function(symbolID, labelIndex, level) {
        void 0 === level && (level = 0);
        var levelCount = 11;
        var keys = Object.keys(this.data.magnificationTable[symbolID]).sort(function(a, b) {
          return Number(a) - Number(b);
        });
        keys = keys.slice(level * levelCount, level * levelCount + levelCount);
        var combotStr = "";
        var scoreStr = "";
        for (var i = 0; i < keys.length; i++) {
          combotStr = combotStr + "" + keys[i];
          scoreStr = scoreStr + "" + this.data.magnificationTable[symbolID][keys[i]];
          if (i < keys.length - 1) {
            combotStr += "\n";
            scoreStr += "\n";
          }
        }
        null != this.oddsTitleLabelGroup[labelIndex] && (this.oddsTitleLabelGroup[labelIndex].string = combotStr);
        null != this.oddsLabelGroup[labelIndex] && (this.oddsLabelGroup[labelIndex].string = scoreStr);
      };
      G53_RulePanel = __decorate([ ccclass ], G53_RulePanel);
      return G53_RulePanel;
    }(Slot_RulePanel_1.default);
    exports.default = G53_RulePanel;
    cc._RF.pop();
  }, {
    "../../../Common/Tools/AudioManager/AudioManager": void 0,
    "../../../SlotFramework/Game/Panel/Slot_RulePanel": void 0,
    "../../Common/G53_DataManager": "G53_DataManager"
  } ],
  G53_SlotReelManager: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "e9bb5Wo42BC+JkD2IajZf39", "G53_SlotReelManager");
    "use strict";
    var __extends = this && this.__extends || function() {
      var extendStatics = function(d, b) {
        extendStatics = Object.setPrototypeOf || {
          __proto__: []
        } instanceof Array && function(d, b) {
          d.__proto__ = b;
        } || function(d, b) {
          for (var p in b) Object.prototype.hasOwnProperty.call(b, p) && (d[p] = b[p]);
        };
        return extendStatics(d, b);
      };
      return function(d, b) {
        extendStatics(d, b);
        function __() {
          this.constructor = d;
        }
        d.prototype = null === b ? Object.create(b) : (__.prototype = b.prototype, new __());
      };
    }();
    var __decorate = this && this.__decorate || function(decorators, target, key, desc) {
      var c = arguments.length, r = c < 3 ? target : null === desc ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
      if ("object" === typeof Reflect && "function" === typeof Reflect.decorate) r = Reflect.decorate(decorators, target, key, desc); else for (var i = decorators.length - 1; i >= 0; i--) (d = decorators[i]) && (r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r);
      return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    var Slot_ReelManager_1 = require("../../../SlotFramework/Game/view/Slot_ReelManager");
    var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
    var STATUS;
    (function(STATUS) {
      STATUS[STATUS["IDLE"] = 0] = "IDLE";
      STATUS[STATUS["SCROLL_ING"] = 1] = "SCROLL_ING";
      STATUS[STATUS["SCROLL_END"] = 2] = "SCROLL_END";
      STATUS[STATUS["RWARD_EFFECTING_ING"] = 3] = "RWARD_EFFECTING_ING";
      STATUS[STATUS["RWARD_EFFECTING_END"] = 4] = "RWARD_EFFECTING_END";
      STATUS[STATUS["SYMBOL_DISAPPEAR_EFFECTING"] = 5] = "SYMBOL_DISAPPEAR_EFFECTING";
      STATUS[STATUS["SYMBOL_DISAPPEAR_DROP_EFFECTING"] = 6] = "SYMBOL_DISAPPEAR_DROP_EFFECTING";
      STATUS[STATUS["CHANGE_SYMBOL_EFFECTING"] = 7] = "CHANGE_SYMBOL_EFFECTING";
    })(STATUS || (STATUS = {}));
    var G53_SlotReelManager = function(_super) {
      __extends(G53_SlotReelManager, _super);
      function G53_SlotReelManager() {
        var _this = null !== _super && _super.apply(this, arguments) || this;
        _this.tipDisappearString = "\u4ee5\u4e0b\u70baG53\u6d88\u9664\u8a2d\u5b9a";
        _this.reelDisappearDelayTime = .05;
        _this.symbolDisappearDelayTime = .05;
        _this.symbolDisappearFinishDelayTime = -.5;
        _this.reelsList = [];
        return _this;
      }
      G53_SlotReelManager.prototype.update = function(_dt) {
        switch (this.status) {
         case STATUS.SYMBOL_DISAPPEAR_EFFECTING:
          this.symbolDisappearEffecting();
          break;

         case STATUS.SYMBOL_DISAPPEAR_DROP_EFFECTING:
          this.symbolDisappearDropEffecting();
          break;

         case STATUS.CHANGE_SYMBOL_EFFECTING:
          this.changeSymbolEffecting();
          break;

         default:
          _super.prototype.update.call(this, _dt);
        }
      };
      G53_SlotReelManager.prototype.showSymbolDisappearEffect = function(p_winPosition, p_nextReels) {
        for (var i = 0; i < p_winPosition.length; i++) {
          this.reelsList[i].SetAllSymbolZOrder(0);
          this.reelsList[i].showSymbolDisappearEffect(p_winPosition[i], p_nextReels[i], this.reelDisappearDelayTime * i, this.symbolDisappearDelayTime, this.symbolDisappearFinishDelayTime);
        }
        this.status = STATUS.SYMBOL_DISAPPEAR_EFFECTING;
      };
      G53_SlotReelManager.prototype.symbolDisappearEffecting = function() {
        for (var i = 0; i < this.reelsList.length; i++) if (this.reelsList[i].isSymbolDisappearEffecting()) return;
        this.status = STATUS.IDLE;
      };
      G53_SlotReelManager.prototype.isSymbolDisappearEffecting = function() {
        return this.status == STATUS.SYMBOL_DISAPPEAR_EFFECTING;
      };
      G53_SlotReelManager.prototype.ShowSymbolDisappearDropEffect = function(p_winPosition) {
        this.hideMaskEffect();
        for (var i = 0; i < p_winPosition.length; i++) this.reelsList[i].ShowSymbolDisappearDropEffect(p_winPosition[i]);
        this.status = STATUS.SYMBOL_DISAPPEAR_DROP_EFFECTING;
      };
      G53_SlotReelManager.prototype.symbolDisappearDropEffecting = function() {
        for (var i = 0; i < this.reelsList.length; i++) if (this.reelsList[i].isSymbolDisappearDropEffecting()) return;
        this.correctionSymbolOrder();
        this.allSymbolPlayIdleEffect();
        this.status = STATUS.IDLE;
      };
      G53_SlotReelManager.prototype.isSymbolDisappearDropEffecting = function() {
        return this.status == STATUS.SYMBOL_DISAPPEAR_DROP_EFFECTING;
      };
      G53_SlotReelManager.prototype.correctionSymbolOrder = function() {
        for (var i = 0; i < this.reelsList.length; i++) this.reelsList[i].correctionSymbolOrder();
      };
      G53_SlotReelManager.prototype.changeSymbol = function(p_changePos) {
        for (var i = 0; i < p_changePos.length; i++) for (var j = 0; j < p_changePos[i].length; j++) p_changePos[i][j] > 0 && this.reelsList[i].changeSymbolEffect(j, p_changePos[i][j]);
        this.status = STATUS.CHANGE_SYMBOL_EFFECTING;
      };
      G53_SlotReelManager.prototype.changeSymbolEffecting = function() {
        for (var i = 0; i < this.reelsList.length; i++) if (this.reelsList[i].isChangeSymbolEffecting()) return;
        this.status = STATUS.IDLE;
      };
      G53_SlotReelManager.prototype.isChangeSymbolIng = function() {
        return this.status == STATUS.CHANGE_SYMBOL_EFFECTING;
      };
      G53_SlotReelManager.prototype.getSymbolReWardPos = function(p_winPosition) {
        var list = [];
        for (var i = 0; i < p_winPosition.length; i++) this.reelsList[i].getSymbolReWardPos(p_winPosition[i]).forEach(function(pos) {
          return list.push(pos);
        });
        return list;
      };
      G53_SlotReelManager.prototype.getWildMulPos = function(p_winPosition) {
        var list = [];
        for (var i = 0; i < p_winPosition.length; i++) this.reelsList[i].getWildMulPos(p_winPosition[i]).forEach(function(pos) {
          return list.push(pos);
        });
        return list;
      };
      __decorate([ property({
        readonly: true,
        displayName: "============ \u3010 \u4ee5\u4e0b\u70baG53\u6d88\u9664\u8a2d\u5b9a \u3011 ============"
      }) ], G53_SlotReelManager.prototype, "tipDisappearString", void 0);
      __decorate([ property({
        displayName: "\u6d88\u9664\u9593\u9694(Reel)",
        type: cc.Float
      }) ], G53_SlotReelManager.prototype, "reelDisappearDelayTime", void 0);
      __decorate([ property({
        displayName: "\u6d88\u9664\u9593\u9694(Symbol)",
        type: cc.Float
      }) ], G53_SlotReelManager.prototype, "symbolDisappearDelayTime", void 0);
      __decorate([ property({
        displayName: "\u6d88\u9664\u5f8c\u518d\u6389\u843d\u9593\u9694\u5fae\u8abf(Symbol)",
        type: cc.Float
      }) ], G53_SlotReelManager.prototype, "symbolDisappearFinishDelayTime", void 0);
      G53_SlotReelManager = __decorate([ ccclass ], G53_SlotReelManager);
      return G53_SlotReelManager;
    }(Slot_ReelManager_1.default);
    exports.default = G53_SlotReelManager;
    cc._RF.pop();
  }, {
    "../../../SlotFramework/Game/view/Slot_ReelManager": void 0
  } ],
  G53_SocketConnect: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "b6cd4sX4LZON73VrLXSAq8v", "G53_SocketConnect");
    "use strict";
    var __extends = this && this.__extends || function() {
      var extendStatics = function(d, b) {
        extendStatics = Object.setPrototypeOf || {
          __proto__: []
        } instanceof Array && function(d, b) {
          d.__proto__ = b;
        } || function(d, b) {
          for (var p in b) Object.prototype.hasOwnProperty.call(b, p) && (d[p] = b[p]);
        };
        return extendStatics(d, b);
      };
      return function(d, b) {
        extendStatics(d, b);
        function __() {
          this.constructor = d;
        }
        d.prototype = null === b ? Object.create(b) : (__.prototype = b.prototype, new __());
      };
    }();
    var __decorate = this && this.__decorate || function(decorators, target, key, desc) {
      var c = arguments.length, r = c < 3 ? target : null === desc ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
      if ("object" === typeof Reflect && "function" === typeof Reflect.decorate) r = Reflect.decorate(decorators, target, key, desc); else for (var i = decorators.length - 1; i >= 0; i--) (d = decorators[i]) && (r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r);
      return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    exports.G53_SocketConnect = void 0;
    var Slot_SocketConnect_1 = require("../../../SlotFramework/Common/Socket/Slot_SocketConnect");
    var G53_DataManager_1 = require("../G53_DataManager");
    var G53_DynamicPopUpPanelManager_1 = require("../G53_DynamicPopUpPanelManager");
    var G53_SocketManager_1 = require("./G53_SocketManager");
    var ccclass = cc._decorator.ccclass;
    var G53_SocketConnect = function(_super) {
      __extends(G53_SocketConnect, _super);
      function G53_SocketConnect() {
        return null !== _super && _super.apply(this, arguments) || this;
      }
      Object.defineProperty(G53_SocketConnect.prototype, "socketManager", {
        get: function() {
          return G53_SocketManager_1.default.getInstance();
        },
        enumerable: false,
        configurable: true
      });
      Object.defineProperty(G53_SocketConnect.prototype, "popupManager", {
        get: function() {
          return G53_DynamicPopUpPanelManager_1.default.getInstance();
        },
        enumerable: false,
        configurable: true
      });
      Object.defineProperty(G53_SocketConnect.prototype, "dataManager", {
        get: function() {
          return G53_DataManager_1.default.getInstance();
        },
        enumerable: false,
        configurable: true
      });
      G53_SocketConnect.prototype.init = function() {
        _super.prototype.init.call(this);
      };
      G53_SocketConnect = __decorate([ ccclass ], G53_SocketConnect);
      return G53_SocketConnect;
    }(Slot_SocketConnect_1.Slot_SocketConnect);
    exports.G53_SocketConnect = G53_SocketConnect;
    cc._RF.pop();
  }, {
    "../../../SlotFramework/Common/Socket/Slot_SocketConnect": void 0,
    "../G53_DataManager": "G53_DataManager",
    "../G53_DynamicPopUpPanelManager": "G53_DynamicPopUpPanelManager",
    "./G53_SocketManager": "G53_SocketManager"
  } ],
  G53_SocketManager: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "cefb2LGusBByJ+23QUDj3X0", "G53_SocketManager");
    "use strict";
    var __extends = this && this.__extends || function() {
      var extendStatics = function(d, b) {
        extendStatics = Object.setPrototypeOf || {
          __proto__: []
        } instanceof Array && function(d, b) {
          d.__proto__ = b;
        } || function(d, b) {
          for (var p in b) Object.prototype.hasOwnProperty.call(b, p) && (d[p] = b[p]);
        };
        return extendStatics(d, b);
      };
      return function(d, b) {
        extendStatics(d, b);
        function __() {
          this.constructor = d;
        }
        d.prototype = null === b ? Object.create(b) : (__.prototype = b.prototype, new __());
      };
    }();
    var __decorate = this && this.__decorate || function(decorators, target, key, desc) {
      var c = arguments.length, r = c < 3 ? target : null === desc ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
      if ("object" === typeof Reflect && "function" === typeof Reflect.decorate) r = Reflect.decorate(decorators, target, key, desc); else for (var i = decorators.length - 1; i >= 0; i--) (d = decorators[i]) && (r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r);
      return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    var G53_TestSocket_1 = require("./G53_TestSocket");
    var G53_DataManager_1 = require("../G53_DataManager");
    var G53_DynamicPopUpPanelManager_1 = require("../G53_DynamicPopUpPanelManager");
    var G53_SocketConnect_1 = require("./G53_SocketConnect");
    var Slot_SocketManager_1 = require("../../../SlotFramework/Common/Socket/Slot_SocketManager");
    var ccclass = cc._decorator.ccclass;
    var G53_SocketManager = function(_super) {
      __extends(G53_SocketManager, _super);
      function G53_SocketManager() {
        var _this = null !== _super && _super.apply(this, arguments) || this;
        _this.testSocket = null;
        _this.isOffline = false;
        _this.selfEnterEventCallback = null;
        _this.refreshJackpotCallback = null;
        _this.socketConnect = null;
        return _this;
      }
      G53_SocketManager.prototype.init = function() {
        this.socketConnect = new G53_SocketConnect_1.G53_SocketConnect();
        this.socketConnect.init();
        if (G53_DataManager_1.default.offLineMode) {
          this.testSocket = new G53_TestSocket_1.default();
          this.isOffline = true;
        } else {
          console.log("Socket Init");
          this.setErrorAlertCallback(this.showErrorAlert.bind(this));
          this.initGameClient();
          this.client && this.registerEvents();
          this.registerOnSelfEnterEvent();
          this.registerRefreshJackpotNotice();
        }
      };
      G53_SocketManager.prototype.setSelfEnterEventCallback = function(selfEnterCallback) {
        this.selfEnterEventCallback = selfEnterCallback;
      };
      G53_SocketManager.prototype.setRefreshJackpotCallback = function(jackpotCallback) {
        this.refreshJackpotCallback = jackpotCallback;
      };
      G53_SocketManager.prototype.login = function() {
        var _this = this;
        if (G53_DataManager_1.default.offLineMode) {
          console.log("\u55ae\u6a5f\u6a21\u5f0f\u767b\u5165");
          this.testSocket = new G53_TestSocket_1.default();
          this.isOffline = true;
          this.init();
          this.testSocket.login().then(function(loginRes) {
            _this.handleOnOpen(loginRes);
          });
        } else {
          console.log("\u9023\u7dda\u6a21\u5f0f\u767b\u5165");
          _super.prototype.login.call(this);
        }
      };
      G53_SocketManager.prototype.showErrorAlert = function(message, title, confirmCallback) {
        G53_DynamicPopUpPanelManager_1.default.getInstance().confirmSystemMsgDialogBox.show(message, function() {
          confirmCallback && confirmCallback();
        });
      };
      G53_SocketManager.prototype.showErrorYesNoAlert = function(message, title, yesCallback, noCallback) {
        G53_DynamicPopUpPanelManager_1.default.getInstance().ynDialogBox.show(message, function() {
          yesCallback && yesCallback();
        }, function() {
          noCallback && noCallback();
        });
      };
      G53_SocketManager.prototype.setOnGetInitialReelInfoEvent = function() {
        if (this.isOffline) return this.sendTestSocket("GetInitialReelInfo", this.testSocket.setOnGetInitialReelInfoEvent.bind(this.testSocket));
        return this.request("GetInitialReelInfo", this.client.setOnGetInitialReelInfoEvent.bind(this.client));
      };
      G53_SocketManager.prototype.setOnGetRoomListEvent = function() {
        return this.request("GetRoomList", this.client.setOnGetInitialReelInfoEvent.bind(this.client), null);
      };
      G53_SocketManager.prototype.setOnChoseRoomEvent = function(roomId) {
        return this.request("ChoseRoom", this.client.setOnChoseRoomEvent.bind(this.client), roomId);
      };
      G53_SocketManager.prototype.setOnFastChoseRoomEvent = function() {
        if (this.isOffline) return this.sendTestSocket("FastChoseRoom", this.testSocket.setOnFastChoseRoomEvent.bind(this.testSocket));
        return this.request("FastChoseRoom", this.client.setOnFastChoseRoomEvent.bind(this.client), null);
      };
      G53_SocketManager.prototype.setOnSpinBaseGameEvent = function(multiple) {
        if (this.isOffline) return this.sendTestSocket("SpinBaseGame", this.testSocket.setOnSpinBaseGameEvent.bind(this.testSocket), multiple);
        return this.request("SpinBaseGame", this.client.setOnSpinBaseGameEvent.bind(this.client), multiple);
      };
      G53_SocketManager.prototype.setOnLeaveEvent = function() {
        if (this.isOffline) return this.sendTestSocket("Leave", this.testSocket.setOnLeaveEvent.bind(this.testSocket));
        return this.request("Leave", this.client.setOnLeaveEvent.bind(this.client), null);
      };
      G53_SocketManager.prototype.setOnHistoryEvent = function() {
        if (this.isOffline) return this.sendTestSocket("Leave", this.testSocket.setOnHistoryEvent.bind(this.testSocket));
        return this.request("History", this.client.setOnHistoryEvent.bind(this.client), null);
      };
      G53_SocketManager.prototype.setOnStashEvent = function() {
        if (this.isOffline) return this.sendTestSocket("Leave", this.testSocket.setOnLeaveEvent.bind(this.testSocket));
        return this.request("Stash", this.client.setOnStashEvent.bind(this.client), null);
      };
      G53_SocketManager.prototype.registerOnSelfEnterEvent = function() {
        var _this = this;
        this.client.setSelfEnterEvent(function(data) {
          _this.selfEnterEventCallback(data);
        });
      };
      G53_SocketManager.prototype.registerRefreshJackpotNotice = function() {
        var _this = this;
        this.client.setOnRefreshJackpotNotice(function(data) {
          G53_DataManager_1.default.getInstance().jpData = data;
          _this.refreshJackpotCallback && _this.refreshJackpotCallback(data);
        });
      };
      G53_SocketManager.prototype.sendTestSocket = function(eventName, event, content) {
        var _this = this;
        this.showSendLog(eventName, null);
        return new Promise(function(resolve) {
          event.bind(_this)(content).then(function(data) {
            _this.showResponseLog(eventName, data);
            resolve(data);
          });
        });
      };
      G53_SocketManager = __decorate([ ccclass ], G53_SocketManager);
      return G53_SocketManager;
    }(Slot_SocketManager_1.default);
    exports.default = G53_SocketManager;
    cc._RF.pop();
  }, {
    "../../../SlotFramework/Common/Socket/Slot_SocketManager": void 0,
    "../G53_DataManager": "G53_DataManager",
    "../G53_DynamicPopUpPanelManager": "G53_DynamicPopUpPanelManager",
    "./G53_SocketConnect": "G53_SocketConnect",
    "./G53_TestSocket": "G53_TestSocket"
  } ],
  G53_SymbolTipPanel: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "27cd2e5fDdDMqagSaGRxvPp", "G53_SymbolTipPanel");
    "use strict";
    var __extends = this && this.__extends || function() {
      var extendStatics = function(d, b) {
        extendStatics = Object.setPrototypeOf || {
          __proto__: []
        } instanceof Array && function(d, b) {
          d.__proto__ = b;
        } || function(d, b) {
          for (var p in b) Object.prototype.hasOwnProperty.call(b, p) && (d[p] = b[p]);
        };
        return extendStatics(d, b);
      };
      return function(d, b) {
        extendStatics(d, b);
        function __() {
          this.constructor = d;
        }
        d.prototype = null === b ? Object.create(b) : (__.prototype = b.prototype, new __());
      };
    }();
    var __decorate = this && this.__decorate || function(decorators, target, key, desc) {
      var c = arguments.length, r = c < 3 ? target : null === desc ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
      if ("object" === typeof Reflect && "function" === typeof Reflect.decorate) r = Reflect.decorate(decorators, target, key, desc); else for (var i = decorators.length - 1; i >= 0; i--) (d = decorators[i]) && (r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r);
      return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    var Slot_SymbolTipPanel_1 = require("../../../SlotFramework/Game/Panel/Slot_SymbolTipPanel");
    var ccclass = cc._decorator.ccclass;
    var G53_SymbolTipPanel = function(_super) {
      __extends(G53_SymbolTipPanel, _super);
      function G53_SymbolTipPanel() {
        var _this = null !== _super && _super.apply(this, arguments) || this;
        _this.data = null;
        return _this;
      }
      G53_SymbolTipPanel.prototype.init = function() {};
      G53_SymbolTipPanel = __decorate([ ccclass ], G53_SymbolTipPanel);
      return G53_SymbolTipPanel;
    }(Slot_SymbolTipPanel_1.default);
    exports.default = G53_SymbolTipPanel;
    cc._RF.pop();
  }, {
    "../../../SlotFramework/Game/Panel/Slot_SymbolTipPanel": void 0
  } ],
  G53_Symbol: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "e51dbWgnZBNT47fT4j70sbi", "G53_Symbol");
    "use strict";
    var __extends = this && this.__extends || function() {
      var extendStatics = function(d, b) {
        extendStatics = Object.setPrototypeOf || {
          __proto__: []
        } instanceof Array && function(d, b) {
          d.__proto__ = b;
        } || function(d, b) {
          for (var p in b) Object.prototype.hasOwnProperty.call(b, p) && (d[p] = b[p]);
        };
        return extendStatics(d, b);
      };
      return function(d, b) {
        extendStatics(d, b);
        function __() {
          this.constructor = d;
        }
        d.prototype = null === b ? Object.create(b) : (__.prototype = b.prototype, new __());
      };
    }();
    var __decorate = this && this.__decorate || function(decorators, target, key, desc) {
      var c = arguments.length, r = c < 3 ? target : null === desc ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
      if ("object" === typeof Reflect && "function" === typeof Reflect.decorate) r = Reflect.decorate(decorators, target, key, desc); else for (var i = decorators.length - 1; i >= 0; i--) (d = decorators[i]) && (r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r);
      return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    var Slot_Symbol_1 = require("../../../SlotFramework/Game/view/Slot_Symbol");
    var Slot_DataManager_1 = require("../../../SlotFramework/Slot_DataManager");
    var FXController_1 = require("../../../Common/Optional/Crosis/FXController/FXController");
    var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
    var EXTRA_STATUS;
    (function(EXTRA_STATUS) {
      EXTRA_STATUS[EXTRA_STATUS["IDLE"] = 0] = "IDLE";
      EXTRA_STATUS[EXTRA_STATUS["DISAPPEAR_EFFECTING"] = 1] = "DISAPPEAR_EFFECTING";
      EXTRA_STATUS[EXTRA_STATUS["DROP_EFFECTING"] = 2] = "DROP_EFFECTING";
      EXTRA_STATUS[EXTRA_STATUS["CHANGE_EFFECTING"] = 3] = "CHANGE_EFFECTING";
    })(EXTRA_STATUS || (EXTRA_STATUS = {}));
    var G53_Symbol = function(_super) {
      __extends(G53_Symbol, _super);
      function G53_Symbol() {
        var _this = null !== _super && _super.apply(this, arguments) || this;
        _this.FX1Prompt = null;
        _this.FXdisappear = null;
        _this.symbolData = null;
        _this.wildMulData = null;
        _this.addFreeGameFX = null;
        _this.playChangeWild = null;
        _this.extraStatus = EXTRA_STATUS.IDLE;
        _this.symbolName = "";
        _this.symbolMul = 0;
        _this.isFreeGame = false;
        return _this;
      }
      G53_Symbol.prototype.setData = function(p_nowIndex, p_symbolValue, p_isFreeGame) {
        void 0 === p_isFreeGame && (p_isFreeGame = false);
        if (p_symbolValue > 50 && p_symbolValue < 60) {
          this.symbolName = "X" + p_symbolValue % 50;
          this.symbolMul = p_symbolValue % 50;
          p_symbolValue = 50;
          this.symbolSpine.skeletonData = this.wildMulData;
        } else {
          this.symbolSpine.skeletonData = this.symbolData;
          this.symbolName = Slot_DataManager_1.SYMBOL_NAME[p_symbolValue];
          this.symbolMul = 0;
        }
        this.isFreeGame = p_isFreeGame;
        this.nowIndex = p_nowIndex;
        this.symbolID = p_symbolValue;
        this.setSymbolSize();
        this.closeAllEffect();
        this.symbolSpine.setAnimation(0, this.symbolName + "_play_normal", true);
      };
      G53_Symbol.prototype.playRewardEffect = function(p_effectLoop) {
        if (this.symbolID === Slot_DataManager_1.SYMBOL_NAME.Jackpot) this.symbolSpine.setAnimation(0, this.symbolName + "_play_win", false); else if (this.symbolID === Slot_DataManager_1.SYMBOL_NAME.Scatter) {
          this.symbolSpine.setAnimation(0, this.symbolName + "_play_win", true);
          this.isFreeGame && this.addFreeGameFX.play();
        }
      };
      G53_Symbol.prototype.setScale = function(p_Size) {
        this.node.setScale(p_Size);
      };
      G53_Symbol.prototype.playScrollingEffect = function() {
        this.closeAllEffect();
      };
      G53_Symbol.prototype.playIdleEffect = function() {
        this.closeAllEffect();
        this.symbolSpine.setAnimation(0, this.symbolName + "_play_normal", true);
      };
      G53_Symbol.prototype.closeAllEffect = function() {
        cc.Tween.stopAllByTarget(this.symbolSpine.node);
      };
      G53_Symbol.prototype.playDisappearEffect = function(p_startDelay, p_symbolDisappearFinishDelayTime) {
        var _this = this;
        this.extraStatus = EXTRA_STATUS.DISAPPEAR_EFFECTING;
        var aniName = this.symbolName + "_play_win";
        cc.Tween.stopAllByTarget(this.symbolSpine.node);
        cc.tween(this.symbolSpine.node).delay(p_startDelay).call(function() {
          _this.symbolSpine.setAnimation(0, aniName, false);
          _this.FX1Prompt.node.active = true;
          _this.FX1Prompt.play();
          _this.FXdisappear.node.active = true;
          _this.FXdisappear.play();
        }).delay(1.2 + p_symbolDisappearFinishDelayTime).call(function() {
          _this.FX1Prompt.node.active = false;
          _this.FXdisappear.node.active = false;
          _this.extraStatus = EXTRA_STATUS.IDLE;
          _this.closeAllEffect();
        }).start();
      };
      G53_Symbol.prototype.isDisappearEffecting = function() {
        return this.extraStatus == EXTRA_STATUS.DISAPPEAR_EFFECTING;
      };
      G53_Symbol.prototype.playDropEffect = function(p_startDelay, p_targetPosition) {
        var _this = this;
        this.setZOrder(0);
        this.extraStatus = EXTRA_STATUS.DROP_EFFECTING;
        this.node.opacity = 255;
        this.symbolSpine.node.opacity = 255;
        cc.tween(this.node).delay(p_startDelay).to(.3, {
          position: cc.v3(p_targetPosition)
        }, {
          easing: "sineIn"
        }).by(.05, {
          position: cc.v3(0, 15)
        }, {
          easing: "cubicOut"
        }).delay(.1).by(.05, {
          position: cc.v3(0, -15)
        }, {
          easing: "cubicIn"
        }).call(function() {
          _this.extraStatus = EXTRA_STATUS.IDLE;
          _this.playIdleEffect();
        }).start();
      };
      G53_Symbol.prototype.isDropEffecting = function() {
        return this.extraStatus == EXTRA_STATUS.DROP_EFFECTING;
      };
      G53_Symbol.prototype.skipDropEffect = function() {
        cc.Tween.stopAllByTarget(this.node);
        this.extraStatus = EXTRA_STATUS.IDLE;
        this.playIdleEffect();
      };
      G53_Symbol.prototype.changeSymbolEffect = function(p_targetSymbol) {
        var _this = this;
        this.extraStatus = EXTRA_STATUS.CHANGE_EFFECTING;
        cc.tween(this.node).call(function() {
          _this.playChangeWild.play();
        }).delay(.3).call(function() {
          _this.setData(_this.nowIndex, p_targetSymbol);
        }).delay(.2).call(function() {
          _this.extraStatus = EXTRA_STATUS.IDLE;
        }).start();
      };
      G53_Symbol.prototype.isChangeSymbolEffecting = function() {
        return this.extraStatus == EXTRA_STATUS.CHANGE_EFFECTING;
      };
      G53_Symbol.prototype.getSymbolMul = function() {
        return this.symbolMul;
      };
      G53_Symbol.prototype.setSymbolSpineMix = function() {};
      __decorate([ property(cc.Animation) ], G53_Symbol.prototype, "FX1Prompt", void 0);
      __decorate([ property(cc.Animation) ], G53_Symbol.prototype, "FXdisappear", void 0);
      __decorate([ property(sp.SkeletonData) ], G53_Symbol.prototype, "symbolData", void 0);
      __decorate([ property(sp.SkeletonData) ], G53_Symbol.prototype, "wildMulData", void 0);
      __decorate([ property(FXController_1.default) ], G53_Symbol.prototype, "addFreeGameFX", void 0);
      __decorate([ property(FXController_1.default) ], G53_Symbol.prototype, "playChangeWild", void 0);
      G53_Symbol = __decorate([ ccclass ], G53_Symbol);
      return G53_Symbol;
    }(Slot_Symbol_1.default);
    exports.default = G53_Symbol;
    cc._RF.pop();
  }, {
    "../../../Common/Optional/Crosis/FXController/FXController": void 0,
    "../../../SlotFramework/Game/view/Slot_Symbol": void 0,
    "../../../SlotFramework/Slot_DataManager": void 0
  } ],
  G53_TestSocket: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "42941iJ+LZJ37Tbi4cTL/Bl", "G53_TestSocket");
    "use strict";
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    var G53_DataManager_1 = require("../G53_DataManager");
    var G53_CommonData_1 = require("../../Game/G53_CommonData");
    var G53_TestSocket = function() {
      function G53_TestSocket() {
        this.tempSpinCount = 1;
      }
      G53_TestSocket.prototype.login = function() {
        var _this = this;
        return new Promise(function(resolve) {
          var _data = new G53_CommonData_1.LoginData();
          _data.nickname = "\u533f\u540dA";
          _data.language = "zh-cn";
          _data.balance = 1e7;
          _data.timestamp = 1e9;
          _data.icon = 1;
          _data.gameId = 53;
          _this.data = G53_DataManager_1.default.getInstance();
          resolve(_data);
        });
      };
      G53_TestSocket.prototype.setOnFastChoseRoomEvent = function() {
        return new Promise(function(resolve) {
          var _data = new G53_CommonData_1.FastChoseRoomData();
          _data.roomId = "\u6e2c\u8a66Room";
          resolve(_data);
        });
      };
      G53_TestSocket.prototype.setOnGetInitialReelInfoEvent = function() {
        return new Promise(function(resolve) {
          var _data = new G53_CommonData_1.GetInitialReelInfoData();
          _data.position = [ 3, 3, 3, 3, 3, 3 ];
          _data.reels.BG = [ [ 20, 1, 2, 3, 11, 50 ], [ 12, 13, 12, 1, 2, 3 ], [ 11, 12, 13, 13, 1, 2 ], [ 3, 11, 12, 13, 11, 1 ], [ 2, 3, 11, 12, 13, 12 ], [ 20, 11, 12, 13, 11, 50 ] ];
          _data.reels.FG = _data.reels.BG;
          resolve(_data);
        });
      };
      G53_TestSocket.prototype.setOnSpinBaseGameEvent = function(p_multiple) {
        var _this = this;
        return new Promise(function(resolve) {
          var _data = null;
          p_multiple *= _this.data.oneOddsScore;
          -1 != G53_TestSocket.assignData && (_this.tempSpinCount = G53_TestSocket.assignData);
          switch (_this.tempSpinCount) {
           case 1:
            _data = _this.testSpinData_1(p_multiple);
            break;

           case 2:
            _data = _this.testSpinData_2(p_multiple);
            break;

           case 3:
            _data = _this.testSpinData_3(p_multiple);
            break;

           case 4:
            _data = _this.testSpinData_4(p_multiple);
            break;

           case 5:
            _data = _this.testSpinData_5(p_multiple);
            break;

           case 6:
            _data = _this.testSpinData_6(p_multiple);
            break;

           case 7:
            _data = _this.testSpinData_7(p_multiple);
            break;

           case 8:
            _data = _this.testSpinData_8(p_multiple);
            break;

           case 9:
            _data = _this.testSpinData_9(p_multiple);
            break;

           case 10:
            _data = _this.testSpinData_10(p_multiple);
          }
          _this.tempSpinCount++;
          _this.tempSpinCount > 10 && (_this.tempSpinCount = 1);
          resolve(_data);
        });
      };
      G53_TestSocket.prototype.testSpinData_1 = function(p_betScore) {
        var _data = new G53_CommonData_1.BaseGameSpinResponeData();
        _data.resultList[0] = new G53_CommonData_1.ResultData();
        _data.resultList[0].reels = [ [ 20, 1, 2, 3, 11, 50 ], [ 12, 13, 12, 1, 2, 3 ], [ 11, 12, 13, 13, 1, 2 ], [ 3, 11, 12, 13, 11, 1 ], [ 2, 3, 11, 12, 13, 12 ], [ 20, 11, 12, 13, 11, 50 ] ];
        _data.resultList[0].eliminateList[0] = new G53_CommonData_1.EliminateData();
        _data.resultList[0].eliminateList[0].isEliminate = false;
        return this.processResultData(_data, p_betScore);
      };
      G53_TestSocket.prototype.testSpinData_2 = function(p_betScore) {
        var _data = new G53_CommonData_1.BaseGameSpinResponeData();
        _data.resultList[0] = new G53_CommonData_1.ResultData();
        _data.resultList[0].reels = [ [ 1, 3, 3, 3, 11, 50 ], [ 1, 1, 1, 1, 2, 3 ], [ 11, 12, 52, 13, 1, 2 ], [ 50, 53, 12, 53, 11, 1 ], [ 2, 52, 11, 12, 13, 12 ], [ 11, 11, 12, 12, 13, 13 ] ];
        _data.resultList[0].eliminateList[0] = new G53_CommonData_1.EliminateData();
        _data.resultList[0].eliminateList[0].isEliminate = true;
        _data.resultList[0].eliminateList[0].eliAllPos = [ [ 1, 0, 0, 0, 0, 0 ], [ 1, 1, 1, 0, 0, 0 ], [ 0, 0, 1, 0, 0, 0 ], [ 0, 0, 1, 1, 0, 0 ], [ 0, 0, 0, 1, 0, 0 ], [ 0, 0, 0, 1, 0, 0 ] ];
        _data.resultList[0].eliminateList[0].nextReels = [ [ 3, 1, 2, 3, 11, 1 ], [ 1, 12, 13, 1, 2, 3 ], [ 13, 3, 13, 11, 12, 2 ], [ 1, 3, 50, 52, 12, 1 ], [ 11, 13, 52, 11, 12, 13 ], [ 3, 11, 11, 12, 1, 1 ] ];
        _data.resultList[0].eliminateList[0].eliWinBonus = 13e5;
        _data.resultList[0].eliminateList[0].winJoinList[0] = new G53_CommonData_1.WinJoinData();
        _data.resultList[0].eliminateList[0].winJoinList[0].singlePos = [ [ 1, 0, 0, 0, 0, 0 ], [ 1, 1, 1, 0, 0, 0 ], [ 0, 0, 1, 0, 0, 0 ], [ 0, 0, 0, 0, 0, 0 ], [ 0, 0, 0, 0, 0, 0 ], [ 0, 0, 0, 0, 0, 0 ] ];
        _data.resultList[0].eliminateList[0].winJoinList[0].specialMultiple = 2;
        _data.resultList[0].eliminateList[0].winJoinList[0].winBonus = 2e5;
        _data.resultList[0].eliminateList[0].winJoinList[1] = new G53_CommonData_1.WinJoinData();
        _data.resultList[0].eliminateList[0].winJoinList[1].singlePos = [ [ 0, 0, 0, 0, 0, 0 ], [ 0, 0, 0, 0, 0, 0 ], [ 0, 0, 1, 0, 0, 0 ], [ 0, 0, 1, 1, 0, 0 ], [ 0, 0, 0, 1, 0, 0 ], [ 0, 0, 0, 1, 0, 0 ] ];
        _data.resultList[0].eliminateList[0].winJoinList[1].specialMultiple = 5;
        _data.resultList[0].eliminateList[0].winJoinList[1].winBonus = 5e5;
        _data.resultList[0].eliminateList[1] = new G53_CommonData_1.EliminateData();
        _data.resultList[0].eliminateList[1].isEliminate = true;
        _data.resultList[0].eliminateList[1].eliAllPos = [ [ 0, 0, 0, 0, 0, 0 ], [ 0, 0, 0, 0, 0, 0 ], [ 0, 1, 1, 1, 1, 0 ], [ 0, 1, 1, 1, 1, 0 ], [ 0, 1, 1, 1, 1, 0 ], [ 0, 1, 1, 0, 0, 0 ] ];
        _data.resultList[0].eliminateList[1].nextReels = [ [ 50, 1, 2, 3, 11, 1 ], [ 20, 12, 13, 1, 2, 3 ], [ 13, 13, 13, 11, 1, 2 ], [ 20, 3, 13, 13, 13, 1 ], [ 11, 3, 11, 12, 13, 13 ], [ 50, 11, 12, 12, 11, 20 ] ];
        _data.resultList[0].eliminateList[1].eliWinBonus = 13e5;
        _data.resultList[0].eliminateList[1].winJoinList[0] = new G53_CommonData_1.WinJoinData();
        _data.resultList[0].eliminateList[1].winJoinList[0].singlePos = [ [ 0, 0, 0, 0, 0, 0 ], [ 0, 0, 0, 0, 0, 0 ], [ 0, 1, 0, 0, 0, 0 ], [ 0, 1, 1, 1, 0, 0 ], [ 0, 0, 1, 0, 0, 0 ], [ 0, 0, 0, 0, 0, 0 ] ];
        _data.resultList[0].eliminateList[1].winJoinList[0].specialMultiple = 5;
        _data.resultList[0].eliminateList[1].winJoinList[0].winBonus = 2e5;
        _data.resultList[0].eliminateList[1].winJoinList[1] = new G53_CommonData_1.WinJoinData();
        _data.resultList[0].eliminateList[1].winJoinList[1].singlePos = [ [ 0, 0, 0, 0, 0, 0 ], [ 0, 0, 0, 0, 0, 0 ], [ 0, 0, 1, 0, 0, 0 ], [ 0, 0, 1, 1, 0, 0 ], [ 0, 1, 1, 0, 0, 0 ], [ 0, 0, 0, 0, 0, 0 ] ];
        _data.resultList[0].eliminateList[1].winJoinList[1].specialMultiple = 5;
        _data.resultList[0].eliminateList[1].winJoinList[1].winBonus = 2e5;
        _data.resultList[0].eliminateList[1].winJoinList[2] = new G53_CommonData_1.WinJoinData();
        _data.resultList[0].eliminateList[1].winJoinList[2].singlePos = [ [ 0, 0, 0, 0, 0, 0 ], [ 0, 0, 0, 0, 0, 0 ], [ 0, 0, 0, 1, 0, 0 ], [ 0, 0, 1, 1, 0, 0 ], [ 0, 0, 1, 1, 0, 0 ], [ 0, 1, 1, 0, 0, 0 ] ];
        _data.resultList[0].eliminateList[1].winJoinList[2].specialMultiple = 5;
        _data.resultList[0].eliminateList[1].winJoinList[2].winBonus = 2e5;
        _data.resultList[0].eliminateList[2] = new G53_CommonData_1.EliminateData();
        _data.resultList[0].eliminateList[2].isEliminate = false;
        return this.processResultData(_data, p_betScore);
      };
      G53_TestSocket.prototype.testSpinData_3 = function(p_betScore) {
        var _data = new G53_CommonData_1.BaseGameSpinResponeData();
        _data.resultList[0] = new G53_CommonData_1.ResultData();
        _data.resultList[0].reels = [ [ 20, 1, 2, 3, 11, 50 ], [ 12, 13, 12, 1, 2, 3 ], [ 11, 12, 13, 13, 1, 2 ], [ 3, 11, 12, 13, 11, 1 ], [ 2, 3, 11, 12, 13, 12 ], [ 20, 11, 12, 13, 11, 50 ] ];
        _data.resultList[0].eliminateList[0] = new G53_CommonData_1.EliminateData();
        _data.resultList[0].eliminateList[0].isEliminate = false;
        _data.resultList[0].scatter = new G53_CommonData_1.ScatterData();
        _data.resultList[0].scatter.count = 2;
        _data.resultList[0].scatter.scatterPos = [ [ 1, 0, 0, 0, 0, 0 ], [ 0, 0, 0, 0, 0, 0 ], [ 0, 0, 0, 0, 0, 0 ], [ 0, 0, 0, 0, 0, 0 ], [ 0, 0, 0, 0, 0, 0 ], [ 1, 0, 0, 0, 0, 0 ] ];
        _data.resultList[1] = new G53_CommonData_1.ResultData();
        _data.resultList[1].reels = [ [ 20, 1, 2, 3, 11, 50 ], [ 12, 13, 12, 1, 2, 3 ], [ 11, 12, 13, 13, 1, 2 ], [ 3, 11, 12, 13, 11, 1 ], [ 2, 3, 11, 12, 13, 12 ], [ 20, 11, 12, 13, 11, 50 ] ];
        _data.resultList[1].eliminateList[0] = new G53_CommonData_1.EliminateData();
        _data.resultList[1].eliminateList[0].isEliminate = false;
        _data.resultList[1].scatter = new G53_CommonData_1.ScatterData();
        _data.resultList[1].scatter.count = 2;
        _data.resultList[1].scatter.scatterPos = [ [ 1, 0, 0, 0, 0, 0 ], [ 0, 0, 0, 0, 0, 0 ], [ 0, 0, 0, 0, 0, 0 ], [ 0, 0, 0, 0, 0, 0 ], [ 0, 0, 0, 0, 0, 0 ], [ 1, 0, 0, 0, 0, 0 ] ];
        for (var i = 2; i < 13; i++) {
          _data.resultList[i] = new G53_CommonData_1.ResultData();
          _data.resultList[i].reels = [ [ 11, 1, 2, 3, 11, 50 ], [ 12, 13, 12, 1, 2, 3 ], [ 11, 12, 13, 13, 1, 2 ], [ 3, 11, 12, 13, 11, 1 ], [ 2, 3, 11, 12, 13, 12 ], [ 13, 11, 12, 13, 11, 50 ] ];
          _data.resultList[i].eliminateList[0] = new G53_CommonData_1.EliminateData();
          _data.resultList[i].eliminateList[0].isEliminate = false;
          _data.resultList[i].scatter = new G53_CommonData_1.ScatterData();
          _data.resultList[i].scatter.count = 0;
        }
        return this.processResultData(_data, p_betScore);
      };
      G53_TestSocket.prototype.testSpinData_4 = function(p_betScore) {
        var _data = new G53_CommonData_1.BaseGameSpinResponeData();
        _data.resultList[0] = new G53_CommonData_1.ResultData();
        _data.getJackpot = true;
        _data.resultList[0].reels = [ [ 1, 1, 1, 1, 1, 1 ], [ 1, 1, 999, 1, 12, 1 ], [ 1, 1, 1, 1, 1, 1 ], [ 1, 1, 1, 1, 1, 1 ], [ 1, 1, 1, 1, 2, 2 ], [ 1, 1, 1, 1, 3, 3 ] ];
        _data.resultList[0].eliminateList[0] = new G53_CommonData_1.EliminateData();
        _data.resultList[0].eliminateList[0].isEliminate = false;
        _data.resultList[0].jackpotBonus = new G53_CommonData_1.jackpotBonusData();
        _data.resultList[0].jackpotBonus.winBonus = 200 * p_betScore;
        _data.resultList[0].jackpotBonus.count = 1;
        _data.resultList[0].jackpotBonus.jackpotPos = [ [ 0, 0, 0, 0, 0, 0 ], [ 0, 0, 1, 0, 0, 0 ], [ 0, 0, 0, 0, 0, 0 ], [ 0, 0, 0, 0, 0, 0 ], [ 0, 0, 0, 0, 0, 0 ], [ 0, 0, 0, 0, 0, 0 ] ];
        return this.processResultData(_data, p_betScore);
      };
      G53_TestSocket.prototype.testSpinData_5 = function(p_betScore) {
        var _data = new G53_CommonData_1.BaseGameSpinResponeData();
        _data.resultList[0] = new G53_CommonData_1.ResultData();
        _data.getJackpot = true;
        _data.resultList[0].reels = [ [ 1, 1, 1, 1, 1, 1 ], [ 1, 1, 999, 1, 12, 1 ], [ 1, 1, 1, 1, 1, 1 ], [ 999, 1, 1, 1, 1, 1 ], [ 1, 1, 1, 1, 2, 2 ], [ 1, 1, 1, 1, 3, 3 ] ];
        _data.resultList[0].eliminateList[0] = new G53_CommonData_1.EliminateData();
        _data.resultList[0].eliminateList[0].isEliminate = false;
        _data.resultList[0].jackpotBonus = new G53_CommonData_1.jackpotBonusData();
        _data.resultList[0].jackpotBonus.winBonus = 300 * p_betScore;
        _data.resultList[0].jackpotBonus.count = 2;
        _data.resultList[0].jackpotBonus.jackpotPos = [ [ 0, 0, 0, 0, 0, 0 ], [ 0, 0, 1, 0, 0, 0 ], [ 0, 0, 0, 0, 0, 0 ], [ 1, 0, 0, 0, 0, 0 ], [ 0, 0, 0, 0, 0, 0 ], [ 0, 0, 0, 0, 0, 0 ] ];
        return this.processResultData(_data, p_betScore);
      };
      G53_TestSocket.prototype.testSpinData_6 = function(p_betScore) {
        var _data = new G53_CommonData_1.BaseGameSpinResponeData();
        _data.resultList[0] = new G53_CommonData_1.ResultData();
        _data.getJackpot = true;
        _data.resultList[0].reels = [ [ 1, 1, 1, 1, 1, 1 ], [ 1, 1, 999, 1, 12, 1 ], [ 1, 1, 1, 1, 1, 1 ], [ 1, 1, 1, 1, 1, 1 ], [ 1, 999, 1, 1, 2, 2 ], [ 1, 1, 1, 999, 3, 3 ] ];
        _data.resultList[0].eliminateList[0] = new G53_CommonData_1.EliminateData();
        _data.resultList[0].eliminateList[0].isEliminate = false;
        _data.resultList[0].jackpotBonus = new G53_CommonData_1.jackpotBonusData();
        _data.resultList[0].jackpotBonus.winBonus = 500 * p_betScore;
        _data.resultList[0].jackpotBonus.count = 3;
        _data.resultList[0].jackpotBonus.jackpotPos = [ [ 0, 0, 0, 0, 0, 0 ], [ 0, 0, 1, 0, 0, 0 ], [ 0, 0, 0, 0, 0, 0 ], [ 0, 0, 0, 0, 0, 0 ], [ 0, 1, 0, 0, 0, 0 ], [ 0, 0, 0, 1, 0, 0 ] ];
        return this.processResultData(_data, p_betScore);
      };
      G53_TestSocket.prototype.testSpinData_7 = function(p_betScore) {
        var _data = new G53_CommonData_1.BaseGameSpinResponeData();
        _data.resultList[0] = new G53_CommonData_1.ResultData();
        _data.getJackpot = true;
        _data.resultList[0].reels = [ [ 1, 1, 1, 1, 1, 999 ], [ 1, 1, 999, 1, 12, 1 ], [ 1, 1, 1, 1, 1, 1 ], [ 1, 1, 1, 1, 1, 1 ], [ 1, 1, 1, 1, 999, 2 ], [ 999, 1, 1, 1, 3, 3 ] ];
        _data.resultList[0].eliminateList[0] = new G53_CommonData_1.EliminateData();
        _data.resultList[0].eliminateList[0].isEliminate = false;
        _data.resultList[0].jackpotBonus = new G53_CommonData_1.jackpotBonusData();
        _data.resultList[0].jackpotBonus.winBonus = 1e3 * p_betScore;
        _data.resultList[0].jackpotBonus.count = 4;
        _data.resultList[0].jackpotBonus.jackpotPos = [ [ 0, 0, 0, 0, 0, 1 ], [ 0, 0, 1, 0, 0, 0 ], [ 0, 0, 0, 0, 0, 0 ], [ 0, 0, 0, 0, 0, 0 ], [ 0, 0, 0, 0, 1, 0 ], [ 1, 0, 0, 0, 0, 0 ] ];
        return this.processResultData(_data, p_betScore);
      };
      G53_TestSocket.prototype.testSpinData_8 = function(p_betScore) {
        var _data = new G53_CommonData_1.BaseGameSpinResponeData();
        _data.resultList[0] = new G53_CommonData_1.ResultData();
        _data.resultList[0].reels = [ [ 1, 1, 1, 1, 1, 11 ], [ 1, 1, 1, 1, 12, 12 ], [ 1, 1, 1, 1, 13, 13 ], [ 1, 1, 1, 1, 13, 13 ], [ 1, 1, 1, 1, 2, 2 ], [ 1, 1, 1, 1, 3, 3 ] ];
        _data.resultList[0].eliminateList[0] = new G53_CommonData_1.EliminateData();
        _data.resultList[0].eliminateList[0].isEliminate = true;
        _data.resultList[0].eliminateList[0].eliAllPos = [ [ 1, 1, 1, 1, 1, 0 ], [ 1, 1, 1, 1, 0, 0 ], [ 1, 1, 1, 1, 0, 0 ], [ 1, 1, 1, 1, 0, 0 ], [ 1, 1, 1, 1, 0, 0 ], [ 1, 1, 1, 1, 0, 0 ] ];
        _data.resultList[0].eliminateList[0].nextReels = [ [ 1, 2, 3, 11, 12, 11 ], [ 2, 3, 11, 12, 12, 12 ], [ 3, 11, 13, 13, 13, 13 ], [ 11, 12, 13, 13, 13, 13 ], [ 12, 13, 13, 2, 2, 2 ], [ 13, 13, 1, 3, 3, 3 ] ];
        _data.resultList[0].eliminateList[0].eliWinBonus = 30 * p_betScore;
        _data.resultList[0].eliminateList[1] = new G53_CommonData_1.EliminateData();
        _data.resultList[0].eliminateList[1].isEliminate = false;
        return this.processResultData(_data, p_betScore);
      };
      G53_TestSocket.prototype.testSpinData_9 = function(p_betScore) {
        var _data = new G53_CommonData_1.BaseGameSpinResponeData();
        _data.resultList[0] = new G53_CommonData_1.ResultData();
        _data.resultList[0].reels = [ [ 1, 1, 1, 1, 1, 11 ], [ 1, 1, 1, 1, 12, 12 ], [ 1, 1, 1, 1, 13, 13 ], [ 1, 1, 1, 1, 13, 13 ], [ 1, 1, 1, 1, 2, 2 ], [ 1, 1, 1, 1, 3, 3 ] ];
        _data.resultList[0].eliminateList[0] = new G53_CommonData_1.EliminateData();
        _data.resultList[0].eliminateList[0].isEliminate = true;
        _data.resultList[0].eliminateList[0].eliAllPos = [ [ 1, 1, 1, 1, 1, 0 ], [ 1, 1, 1, 1, 0, 0 ], [ 1, 1, 1, 1, 0, 0 ], [ 1, 1, 1, 1, 0, 0 ], [ 1, 1, 1, 1, 0, 0 ], [ 1, 1, 1, 1, 0, 0 ] ];
        _data.resultList[0].eliminateList[0].nextReels = [ [ 1, 2, 3, 11, 12, 11 ], [ 2, 3, 11, 12, 12, 12 ], [ 3, 11, 13, 13, 13, 13 ], [ 11, 12, 13, 13, 13, 13 ], [ 12, 13, 13, 2, 2, 2 ], [ 13, 13, 1, 3, 3, 3 ] ];
        _data.resultList[0].eliminateList[0].eliWinBonus = 60 * p_betScore;
        _data.resultList[0].eliminateList[1] = new G53_CommonData_1.EliminateData();
        _data.resultList[0].eliminateList[1].isEliminate = false;
        return this.processResultData(_data, p_betScore);
      };
      G53_TestSocket.prototype.testSpinData_10 = function(p_betScore) {
        var _data = new G53_CommonData_1.BaseGameSpinResponeData();
        _data.resultList[0] = new G53_CommonData_1.ResultData();
        _data.resultList[0].reels = [ [ 1, 1, 1, 1, 1, 11 ], [ 1, 1, 1, 1, 12, 12 ], [ 1, 1, 1, 1, 13, 13 ], [ 1, 1, 1, 1, 13, 13 ], [ 1, 1, 1, 1, 2, 2 ], [ 1, 1, 1, 1, 3, 3 ] ];
        _data.resultList[0].eliminateList[0] = new G53_CommonData_1.EliminateData();
        _data.resultList[0].eliminateList[0].isEliminate = true;
        _data.resultList[0].eliminateList[0].eliAllPos = [ [ 1, 1, 1, 1, 1, 0 ], [ 1, 1, 1, 1, 0, 0 ], [ 1, 1, 1, 1, 0, 0 ], [ 1, 1, 1, 1, 0, 0 ], [ 1, 1, 1, 1, 0, 0 ], [ 1, 1, 1, 1, 0, 0 ] ];
        _data.resultList[0].eliminateList[0].nextReels = [ [ 1, 2, 3, 11, 12, 11 ], [ 2, 3, 11, 12, 12, 12 ], [ 3, 11, 13, 13, 13, 13 ], [ 11, 12, 13, 13, 13, 13 ], [ 12, 13, 13, 2, 2, 2 ], [ 13, 13, 1, 3, 3, 3 ] ];
        _data.resultList[0].eliminateList[0].eliWinBonus = 500 * p_betScore;
        _data.resultList[0].eliminateList[1] = new G53_CommonData_1.EliminateData();
        _data.resultList[0].eliminateList[1].isEliminate = false;
        return this.processResultData(_data, p_betScore);
      };
      G53_TestSocket.prototype.processResultData = function(_data, p_betScore) {
        _data.totalBet = p_betScore;
        _data.resultList.forEach(function(result) {
          result.eliminateList.forEach(function(eliminate) {
            result.totalWinBonus += eliminate.eliWinBonus;
          });
          _data.totalBonus += result.totalWinBonus;
        });
        _data.currentCash = 1e4 * this.data.coin - p_betScore + _data.totalBonus;
        return _data;
      };
      G53_TestSocket.prototype.setOnLeaveEvent = function() {
        return new Promise(function(resolve) {
          resolve();
        });
      };
      G53_TestSocket.prototype.setOnHistoryEvent = function() {
        return new Promise(function(resolve) {
          resolve();
        });
      };
      G53_TestSocket.assignData = -1;
      return G53_TestSocket;
    }();
    exports.default = G53_TestSocket;
    cc._RF.pop();
  }, {
    "../../Game/G53_CommonData": "G53_CommonData",
    "../G53_DataManager": "G53_DataManager"
  } ],
  G53_TestUI: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "fd0baOGSl5KGpFX9ORCmdBU", "G53_TestUI");
    "use strict";
    var __extends = this && this.__extends || function() {
      var extendStatics = function(d, b) {
        extendStatics = Object.setPrototypeOf || {
          __proto__: []
        } instanceof Array && function(d, b) {
          d.__proto__ = b;
        } || function(d, b) {
          for (var p in b) Object.prototype.hasOwnProperty.call(b, p) && (d[p] = b[p]);
        };
        return extendStatics(d, b);
      };
      return function(d, b) {
        extendStatics(d, b);
        function __() {
          this.constructor = d;
        }
        d.prototype = null === b ? Object.create(b) : (__.prototype = b.prototype, new __());
      };
    }();
    var __decorate = this && this.__decorate || function(decorators, target, key, desc) {
      var c = arguments.length, r = c < 3 ? target : null === desc ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
      if ("object" === typeof Reflect && "function" === typeof Reflect.decorate) r = Reflect.decorate(decorators, target, key, desc); else for (var i = decorators.length - 1; i >= 0; i--) (d = decorators[i]) && (r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r);
      return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    var DesktopBrowserTransform_1 = require("../../Common/Tools/DesktopBrowserTransform/DesktopBrowserTransform");
    var G53_DataManager_1 = require("../Common/G53_DataManager");
    var G53_TestSocket_1 = require("../Common/Socket/G53_TestSocket");
    var G53_SlotReelManager_1 = require("./View/G53_SlotReelManager");
    var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
    var G53_TestUI = function(_super) {
      __extends(G53_TestUI, _super);
      function G53_TestUI() {
        var _this = null !== _super && _super.apply(this, arguments) || this;
        _this.menuNode = null;
        _this.gameSpeedEditBox = null;
        _this.pageList = null;
        _this.reelMgrValueList = [ "dropOutTime", "dropOutEasing", "dropOutDelayTime", "dropOutStartIntervalTime", "dropInTime", "dropInEasing", "dropInDelayTime", "dropInStartIntervalTime", "dropInReboundHeight", "dropInReboundTime", "dropInReboundEasingOut", "dropInReboundEasingIn", "dropInReboundAngle", "dropInReboundAngleTime" ];
        _this.valueUpdateHis = [ "===== Update History =====" ];
        _this.updateIdx = 0;
        return _this;
      }
      G53_TestUI.prototype.start = function() {
        if (G53_DataManager_1.default.offLineMode) {
          this.node.setPosition(cc.Vec2.ZERO);
          this.menuNode.active = false;
        } else this.node.active = false;
      };
      G53_TestUI.prototype.setTestData = function(event, customEventData) {
        console.log("\u4f7f\u7528\u6e2c\u8a66\u8cc7\u6599 " + customEventData);
        G53_TestSocket_1.default.assignData = Number(customEventData);
        this.closeMenu();
      };
      G53_TestUI.prototype.setGameSpeed = function() {
        DesktopBrowserTransform_1.default.getInstance().setGameSpeed(Number(this.gameSpeedEditBox.string));
      };
      G53_TestUI.prototype.showMenu = function() {
        this.menuNode.active = true;
      };
      G53_TestUI.prototype.closeMenu = function() {
        console.log("\u95dc\u9589\u6e2c\u8a66UI");
        this.menuNode.active = false;
      };
      G53_TestUI.prototype.onToggleClick = function(p_toggle) {
        var _index = Number(p_toggle.node.name) - 1;
        this.pageList.children.forEach(function(node, index) {
          node.active = _index == index;
          node.position = cc.Vec3.ZERO;
        });
        1 == _index && this.syncSlotReelManagerValue();
      };
      G53_TestUI.prototype.syncSlotReelManagerValue = function() {
        var _this = this;
        var _reelMgr = cc.Canvas.instance.node.getComponentInChildren(G53_SlotReelManager_1.default);
        this.reelMgrValueList.forEach(function(value) {
          var editBoxList = _this.node.getComponentsInChildren(cc.EditBox);
          editBoxList.forEach(function(editBox) {
            editBox.node.name === value && 0 == editBox.string.length && (null != _reelMgr[value] ? editBox.string = "" + _reelMgr[value] : console.warn("updateReelPage " + value + " \u66f4\u65b0\u5931\u6557"));
          });
        });
        cc.sys.isMobile || (this.pageList.children[1].getComponentInChildren(cc.WebView).node.active = true);
      };
      G53_TestUI.prototype.updateSlotReelManagerValue = function() {
        var _this = this;
        var _reelMgr = cc.Canvas.instance.node.getComponentInChildren(G53_SlotReelManager_1.default);
        this.valueUpdateHis.push("----- \u66f4\u65b0 " + ++this.updateIdx + " -----");
        var _nowUpdate = [ "----- \u66f4\u65b0 " + this.updateIdx + " -----" ];
        this.reelMgrValueList.forEach(function(valueName) {
          var editBoxList = _this.node.getComponentsInChildren(cc.EditBox);
          editBoxList.forEach(function(editBox) {
            if (editBox.node.name === valueName && editBox.string.length > 0) {
              var valueLabelZh = editBox.node.parent.getComponent(cc.Label).string;
              if (_reelMgr[valueName] && "number" == typeof _reelMgr[valueName]) {
                var _updateValue = Number(editBox.string);
                if (!Number.isNaN(_updateValue) && _updateValue != _reelMgr[valueName]) {
                  _this.valueUpdateHis.push("    " + valueLabelZh + " from " + _reelMgr[valueName] + " to " + _updateValue);
                  _nowUpdate.push("    " + valueLabelZh + " from " + _reelMgr[valueName] + " to " + _updateValue);
                  _reelMgr[valueName] = _updateValue;
                }
              } else {
                var _updateValue = editBox.string;
                if (_updateValue != _reelMgr[valueName]) {
                  _this.valueUpdateHis.push("    " + valueLabelZh + " from " + _reelMgr[valueName] + " to " + _updateValue);
                  _nowUpdate.push("    " + valueLabelZh + " from " + _reelMgr[valueName] + " to " + _updateValue);
                  _reelMgr[valueName] = _updateValue;
                }
              }
            }
          });
        });
        console.warn(JSON.stringify(_nowUpdate, null, 2));
        this.closeMenu();
      };
      G53_TestUI.prototype.showUpdateHisroty = function() {
        var _this = this;
        var _reelMgr = cc.Canvas.instance.node.getComponentInChildren(G53_SlotReelManager_1.default);
        var _nowValue = [ "", "===== Now Value =====" ];
        this.reelMgrValueList.forEach(function(valueName) {
          var editBoxList = _this.node.getComponentsInChildren(cc.EditBox);
          editBoxList.forEach(function(editBox) {
            if (editBox.node.name === valueName && editBox.string.length > 0) {
              var valueLabelZh = editBox.node.parent.getComponent(cc.Label).string;
              _nowValue.push("    " + valueLabelZh + " = " + _reelMgr[valueName]);
            }
          });
        });
        console.warn(JSON.stringify(this.valueUpdateHis.concat(_nowValue), null, 2));
      };
      G53_TestUI.prototype.resetTip = function(_editBox) {
        _editBox.string = _editBox.placeholder;
      };
      __decorate([ property(cc.Node) ], G53_TestUI.prototype, "menuNode", void 0);
      __decorate([ property(cc.EditBox) ], G53_TestUI.prototype, "gameSpeedEditBox", void 0);
      __decorate([ property(cc.Node) ], G53_TestUI.prototype, "pageList", void 0);
      G53_TestUI = __decorate([ ccclass ], G53_TestUI);
      return G53_TestUI;
    }(cc.Component);
    exports.default = G53_TestUI;
    cc._RF.pop();
  }, {
    "../../Common/Tools/DesktopBrowserTransform/DesktopBrowserTransform": void 0,
    "../Common/G53_DataManager": "G53_DataManager",
    "../Common/Socket/G53_TestSocket": "G53_TestSocket",
    "./View/G53_SlotReelManager": "G53_SlotReelManager"
  } ],
  G53_WinScorePanel: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "2ceedKcm4BGw5OaIt2+6dW5", "G53_WinScorePanel");
    "use strict";
    var __extends = this && this.__extends || function() {
      var extendStatics = function(d, b) {
        extendStatics = Object.setPrototypeOf || {
          __proto__: []
        } instanceof Array && function(d, b) {
          d.__proto__ = b;
        } || function(d, b) {
          for (var p in b) Object.prototype.hasOwnProperty.call(b, p) && (d[p] = b[p]);
        };
        return extendStatics(d, b);
      };
      return function(d, b) {
        extendStatics(d, b);
        function __() {
          this.constructor = d;
        }
        d.prototype = null === b ? Object.create(b) : (__.prototype = b.prototype, new __());
      };
    }();
    var __decorate = this && this.__decorate || function(decorators, target, key, desc) {
      var c = arguments.length, r = c < 3 ? target : null === desc ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
      if ("object" === typeof Reflect && "function" === typeof Reflect.decorate) r = Reflect.decorate(decorators, target, key, desc); else for (var i = decorators.length - 1; i >= 0; i--) (d = decorators[i]) && (r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r);
      return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    var Slot_WinScorePanel_1 = require("../../../SlotFramework/Game/Panel/Slot_WinScorePanel");
    var AudioManager_1 = require("../../../Common/Tools/AudioManager/AudioManager");
    var FXController_1 = require("../../../Common/Optional/Crosis/FXController/FXController");
    var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
    var G53_WinScorePanel = function(_super) {
      __extends(G53_WinScorePanel, _super);
      function G53_WinScorePanel() {
        var _this = null !== _super && _super.apply(this, arguments) || this;
        _this.fxBigWin = null;
        _this.fxSuperWin = null;
        _this.fxMegaWin = null;
        _this.bigWinSpine = null;
        _this.superWinSpine = null;
        _this.megaWinSpine = null;
        return _this;
      }
      G53_WinScorePanel.prototype.open = function(p_totalWinScore, p_betScore, p_isFreeGame) {
        void 0 === p_isFreeGame && (p_isFreeGame = false);
        _super.prototype.open.call(this, p_totalWinScore, p_betScore, p_isFreeGame);
      };
      G53_WinScorePanel.prototype.closeAllWinEffect = function() {
        this.fxBigWin.stop();
        this.fxSuperWin.stop();
        this.fxMegaWin.stop();
      };
      G53_WinScorePanel.prototype.showBigWinEffect = function() {
        var _this = this;
        console.log("\u3010Show Big Win\u3011 ");
        AudioManager_1.AudioManager.instance.playAudioEvent("WinScorePanelBigWinEffect");
        _super.prototype.showBigWinEffect.call(this);
        this.closeAllWinEffect();
        this.fxBigWin.play();
        this.bigWinSpine.setAnimation(0, "BigWin_Start", false);
        this.bigWinSpine.setCompleteListener(function() {
          _this.bigWinSpine.setCompleteListener(null);
          _this.bigWinSpine.setAnimation(0, "BigWin_Loop", true);
        });
      };
      G53_WinScorePanel.prototype.showSuperWinEffect = function() {
        var _this = this;
        console.log("\u3010Show Super Win\u3011 ");
        AudioManager_1.AudioManager.instance.playAudioEvent("WinScorePanelSuperWinEffect");
        _super.prototype.showSuperWinEffect.call(this);
        this.closeAllWinEffect();
        this.fxSuperWin.play();
        this.superWinSpine.setAnimation(0, "SuperWin_Start", false);
        this.superWinSpine.setCompleteListener(function() {
          _this.superWinSpine.setCompleteListener(null);
          _this.superWinSpine.setAnimation(0, "SuperWin_Loop", true);
        });
      };
      G53_WinScorePanel.prototype.showMegaWinEffect = function() {
        var _this = this;
        console.log("\u3010Mega Win\u3011");
        AudioManager_1.AudioManager.instance.playAudioEvent("WinScorePanelMegaWinEffect");
        _super.prototype.showMegaWinEffect.call(this);
        this.closeAllWinEffect();
        this.fxMegaWin.play();
        this.megaWinSpine.setAnimation(0, "MegaWin_Start", false);
        this.megaWinSpine.setCompleteListener(function() {
          _this.megaWinSpine.setCompleteListener(null);
          _this.megaWinSpine.setAnimation(0, "MegaWin_Loop", true);
        });
      };
      __decorate([ property(FXController_1.default) ], G53_WinScorePanel.prototype, "fxBigWin", void 0);
      __decorate([ property(FXController_1.default) ], G53_WinScorePanel.prototype, "fxSuperWin", void 0);
      __decorate([ property(FXController_1.default) ], G53_WinScorePanel.prototype, "fxMegaWin", void 0);
      __decorate([ property(sp.Skeleton) ], G53_WinScorePanel.prototype, "bigWinSpine", void 0);
      __decorate([ property(sp.Skeleton) ], G53_WinScorePanel.prototype, "superWinSpine", void 0);
      __decorate([ property(sp.Skeleton) ], G53_WinScorePanel.prototype, "megaWinSpine", void 0);
      G53_WinScorePanel = __decorate([ ccclass ], G53_WinScorePanel);
      return G53_WinScorePanel;
    }(Slot_WinScorePanel_1.default);
    exports.default = G53_WinScorePanel;
    cc._RF.pop();
  }, {
    "../../../Common/Optional/Crosis/FXController/FXController": void 0,
    "../../../Common/Tools/AudioManager/AudioManager": void 0,
    "../../../SlotFramework/Game/Panel/Slot_WinScorePanel": void 0
  } ]
}, {}, [ "G53_DataManager", "G53_DynamicPopUpPanelManager", "G53_LoadingItem", "G53_SocketConnect", "G53_SocketManager", "G53_TestSocket", "G53_CommonData", "G53_Game", "G53_TestUI", "G53_AutoGamePanel", "G53_BonusSymbolPanel", "G53_FreeGameGetScorePanel", "G53_FreeGamePanel", "G53_GameUI", "G53_JackpotPanel", "G53_MusicOptionPanel", "G53_Reel", "G53_RulePanel", "G53_SlotReelManager", "G53_Symbol", "G53_SymbolTipPanel", "G53_WinScorePanel", "G53_Loading_InitData", "G53_Loading", "G53_LoadingUI", "G53_Lobby", "G53_LobbyUI", "G53_Language" ]);