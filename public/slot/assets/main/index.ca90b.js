window.__require = function e(t, n, r) {
  function s(o, u) {
    if (!n[o]) {
      if (!t[o]) {
        var b = o.split("/");
        b = b[b.length - 1];
        if (!t[b]) {
          var a = "function" == typeof __require && __require;
          if (!u && a) return a(b, !0);
          if (i) return i(b, !0);
          throw new Error("Cannot find module '" + o + "'");
        }
        o = b;
      }
      var f = n[o] = {
        exports: {}
      };
      t[o][0].call(f.exports, function(e) {
        var n = t[o][1][e];
        return s(n || e);
      }, f, f.exports, e, t, n, r);
    }
    return n[o].exports;
  }
  var i = "function" == typeof __require && __require;
  for (var o = 0; o < r.length; o++) s(r[o]);
  return s;
}({
  BundleManager: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "6f755it5lZLxZBPXDZpWYKg", "BundleManager");
    "use strict";
    var __awaiter = this && this.__awaiter || function(thisArg, _arguments, P, generator) {
      function adopt(value) {
        return value instanceof P ? value : new P(function(resolve) {
          resolve(value);
        });
      }
      return new (P || (P = Promise))(function(resolve, reject) {
        function fulfilled(value) {
          try {
            step(generator.next(value));
          } catch (e) {
            reject(e);
          }
        }
        function rejected(value) {
          try {
            step(generator["throw"](value));
          } catch (e) {
            reject(e);
          }
        }
        function step(result) {
          result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected);
        }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
      });
    };
    var __generator = this && this.__generator || function(thisArg, body) {
      var _ = {
        label: 0,
        sent: function() {
          if (1 & t[0]) throw t[1];
          return t[1];
        },
        trys: [],
        ops: []
      }, f, y, t, g;
      return g = {
        next: verb(0),
        throw: verb(1),
        return: verb(2)
      }, "function" === typeof Symbol && (g[Symbol.iterator] = function() {
        return this;
      }), g;
      function verb(n) {
        return function(v) {
          return step([ n, v ]);
        };
      }
      function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
          if (f = 1, y && (t = 2 & op[0] ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 
          0) : y.next) && !(t = t.call(y, op[1])).done) return t;
          (y = 0, t) && (op = [ 2 & op[0], t.value ]);
          switch (op[0]) {
           case 0:
           case 1:
            t = op;
            break;

           case 4:
            _.label++;
            return {
              value: op[1],
              done: false
            };

           case 5:
            _.label++;
            y = op[1];
            op = [ 0 ];
            continue;

           case 7:
            op = _.ops.pop();
            _.trys.pop();
            continue;

           default:
            if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (6 === op[0] || 2 === op[0])) {
              _ = 0;
              continue;
            }
            if (3 === op[0] && (!t || op[1] > t[0] && op[1] < t[3])) {
              _.label = op[1];
              break;
            }
            if (6 === op[0] && _.label < t[1]) {
              _.label = t[1];
              t = op;
              break;
            }
            if (t && _.label < t[2]) {
              _.label = t[2];
              _.ops.push(op);
              break;
            }
            t[2] && _.ops.pop();
            _.trys.pop();
            continue;
          }
          op = body.call(thisArg, _);
        } catch (e) {
          op = [ 6, e ];
          y = 0;
        } finally {
          f = t = 0;
        }
        if (5 & op[0]) throw op[1];
        return {
          value: op[0] ? op[1] : void 0,
          done: true
        };
      }
    };
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    exports.BundleManager = void 0;
    var BundleManager = function() {
      function BundleManager() {}
      BundleManager.loadScript = function(gameName) {
        var _this = this;
        return new Promise(function(resolve) {
          false;
          var hashData = _this.getBundleHash("sdk_polyfill", gameName);
          var url = "./src/sdk_polyfill." + hashData + ".js";
          var script = document.createElement("script");
          script.type = "text/javascript";
          script.src = url;
          script.onload = function() {
            resolve();
          };
          document.body.appendChild(script);
        });
      };
      BundleManager.get = function(bundleName, url, type) {
        var bundle = cc.assetManager.getBundle(bundleName);
        return bundle.get(url, type);
      };
      BundleManager.load = function(bundleName, url, type) {
        var bundle = cc.assetManager.getBundle(bundleName);
        return new Promise(function(resolve, reject) {
          bundle.load(url, type, function(error, data) {
            if (error) {
              console.error("[BundleManager] Error loading resource", error);
              reject(null);
            }
            resolve(data);
          });
        });
      };
      BundleManager.loadBundles = function(bundleNames, gameName) {
        return __awaiter(this, void 0, Promise, function() {
          var loadingBundles, bundles;
          var _this = this;
          return __generator(this, function(_a) {
            switch (_a.label) {
             case 0:
              loadingBundles = bundleNames.map(function(name) {
                return _this.loadBundle(name, gameName);
              });
              return [ 4, Promise.all(loadingBundles) ];

             case 1:
              bundles = _a.sent();
              return [ 2, bundles ];
            }
          });
        });
      };
      BundleManager.loadBundle = function(bundleName, gameName) {
        return __awaiter(this, void 0, Promise, function() {
          var hashCode;
          return __generator(this, function(_a) {
            hashCode = this.getBundleHash(bundleName, gameName);
            return [ 2, new Promise(function(resolve, reject) {
              cc.assetManager.loadBundle(bundleName, {
                version: hashCode
              }, function(error, bundle) {
                if (error) return reject(error);
                resolve(bundle);
              });
            }) ];
          });
        });
      };
      BundleManager.getBundleHash = function(bundleName, gameName) {
        false;
        var hashJson = cc.assetManager.assets.get(window.hashPath);
        var map = window.hashData || hashJson.json;
        if (null == map) return null;
        return window.hashData[gameName][bundleName];
      };
      return BundleManager;
    }();
    exports.BundleManager = BundleManager;
    cc._RF.pop();
  }, {} ],
  KKGameOrientationCanvas: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "b71dc4QBqBIMLYbXrmOhXVF", "KKGameOrientationCanvas");
    "use strict";
    var __extends = this && this.__extends || function() {
      var extendStatics = function(d, b) {
        extendStatics = Object.setPrototypeOf || {
          __proto__: []
        } instanceof Array && function(d, b) {
          d.__proto__ = b;
        } || function(d, b) {
          for (var p in b) Object.prototype.hasOwnProperty.call(b, p) && (d[p] = b[p]);
        };
        return extendStatics(d, b);
      };
      return function(d, b) {
        extendStatics(d, b);
        function __() {
          this.constructor = d;
        }
        d.prototype = null === b ? Object.create(b) : (__.prototype = b.prototype, new __());
      };
    }();
    var __decorate = this && this.__decorate || function(decorators, target, key, desc) {
      var c = arguments.length, r = c < 3 ? target : null === desc ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
      if ("object" === typeof Reflect && "function" === typeof Reflect.decorate) r = Reflect.decorate(decorators, target, key, desc); else for (var i = decorators.length - 1; i >= 0; i--) (d = decorators[i]) && (r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r);
      return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __read = this && this.__read || function(o, n) {
      var m = "function" === typeof Symbol && o[Symbol.iterator];
      if (!m) return o;
      var i = m.call(o), r, ar = [], e;
      try {
        while ((void 0 === n || n-- > 0) && !(r = i.next()).done) ar.push(r.value);
      } catch (error) {
        e = {
          error: error
        };
      } finally {
        try {
          r && !r.done && (m = i["return"]) && m.call(i);
        } finally {
          if (e) throw e.error;
        }
      }
      return ar;
    };
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    exports.KKGameOrientationCanvas = void 0;
    var KKGameOrientationToolManager_1 = require("./KKGameOrientationToolManager");
    var ccclass = cc._decorator.ccclass;
    var ORIENTATION_EVENT = {
      RESIZE: "ORIENTATION_EVENT_RESIZE",
      WIDGET_RESIZE: "ORIENTATION_EVENT_WIDGET_RESIZE"
    };
    var KKGameOrientationCanvas = function(_super) {
      __extends(KKGameOrientationCanvas, _super);
      function KKGameOrientationCanvas() {
        var _this = null !== _super && _super.apply(this, arguments) || this;
        _this.orientationToolManager = null;
        _this.isChanging = false;
        _this.onResize = _this.canvasResize.bind(_this);
        return _this;
      }
      Object.defineProperty(KKGameOrientationCanvas.prototype, "orientationAngle", {
        get: function() {
          var _a;
          return (null === (_a = null === screen || void 0 === screen ? void 0 : screen.orientation) || void 0 === _a ? void 0 : _a.angle) || window.orientation;
        },
        enumerable: false,
        configurable: true
      });
      KKGameOrientationCanvas.prototype.onLoad = function() {
        this.orientationToolManager = KKGameOrientationToolManager_1.KKGameOrientationToolManager.getInstance();
      };
      KKGameOrientationCanvas.prototype.start = function() {
        this.orientationToolManager && this.initOrientation();
        window.addEventListener("resize", this.onResize);
      };
      KKGameOrientationCanvas.prototype.onDestroy = function() {
        KKGameOrientationToolManager_1.KKGameOrientationToolManager.orientationState = KKGameOrientationToolManager_1.KKGAME_ORIENTATION_TYPE.NONE;
        window.removeEventListener("resize", this.onResize);
      };
      KKGameOrientationCanvas.prototype.initOrientation = function() {
        cc.sys.isMobile ? 180 === this.orientationAngle || 0 === this.orientationAngle ? KKGameOrientationToolManager_1.KKGameOrientationToolManager.orientationState = KKGameOrientationToolManager_1.KKGAME_ORIENTATION_TYPE.VERTICAL : KKGameOrientationToolManager_1.KKGameOrientationToolManager.orientationState = KKGameOrientationToolManager_1.KKGAME_ORIENTATION_TYPE.HORIZONTAL : window.innerWidth >= window.innerHeight ? KKGameOrientationToolManager_1.KKGameOrientationToolManager.orientationState = KKGameOrientationToolManager_1.KKGAME_ORIENTATION_TYPE.HORIZONTAL : KKGameOrientationToolManager_1.KKGameOrientationToolManager.orientationState = KKGameOrientationToolManager_1.KKGAME_ORIENTATION_TYPE.VERTICAL;
        this.setCanvasSize();
        for (var key in this.orientationToolManager.orientationToolList) KKGameOrientationToolManager_1.KKGameOrientationToolManager.orientationState === KKGameOrientationToolManager_1.KKGAME_ORIENTATION_TYPE.HORIZONTAL ? this.orientationToolManager.orientationToolList[key].toHorizontal() : this.orientationToolManager.orientationToolList[key].toVerticall();
      };
      KKGameOrientationCanvas.prototype.setCanvasSize = function() {
        var _a, _b, _c, _d;
        var canvasSize = cc.Canvas.instance.node.getContentSize();
        var width = canvasSize.width;
        var height = canvasSize.height;
        var x = cc.Canvas.instance.node.x;
        var y = cc.Canvas.instance.node.y;
        if (KKGameOrientationToolManager_1.KKGameOrientationToolManager.orientationState === KKGameOrientationToolManager_1.KKGAME_ORIENTATION_TYPE.HORIZONTAL) {
          cc.view._orientation = cc.macro.ORIENTATION_LANDSCAPE;
          if (width < height) {
            _a = __read([ height, width ], 2), width = _a[0], height = _a[1];
            _b = __read([ y, x ], 2), x = _b[0], y = _b[1];
          }
        } else {
          cc.view._orientation = cc.macro.ORIENTATION_PORTRAIT;
          if (height < width) {
            _c = __read([ height, width ], 2), width = _c[0], height = _c[1];
            _d = __read([ y, x ], 2), x = _d[0], y = _d[1];
          }
        }
        cc.Canvas.instance.node.setContentSize(width, height);
        cc.Canvas.instance.node.setPosition(x, y);
        cc.Canvas.instance.designResolution = KKGameOrientationToolManager_1.KKGameOrientationToolManager.orientationState === KKGameOrientationToolManager_1.KKGAME_ORIENTATION_TYPE.HORIZONTAL ? new cc.Size(1280, 720) : new cc.Size(720, 1280);
        cc.Canvas.instance.fitHeight = cc.Canvas.instance.designResolution.width > cc.Canvas.instance.designResolution.height;
        cc.Canvas.instance.fitWidth = cc.Canvas.instance.designResolution.height > cc.Canvas.instance.designResolution.width;
        this.isChanging = false;
      };
      KKGameOrientationCanvas.prototype.canvasResize = function() {
        var _this = this;
        this.scheduleOnce(function() {
          if (cc.sys.isMobile) {
            if (_this.isChanging || KKGameOrientationToolManager_1.KKGameOrientationToolManager.lockingOrientation) return;
            var gameDiv;
            var width;
            var height;
            var currentOrientaionOfGameDiv;
            false;
            _this.isChanging = true;
            _this.switchCanvas();
            for (var key in _this.orientationToolManager.orientationToolList) _this.orientationToolManager.orientationToolList[key].canvasResize();
            cc.systemEvent.emit(ORIENTATION_EVENT.RESIZE);
          }
          cc.systemEvent.emit(ORIENTATION_EVENT.WIDGET_RESIZE);
          _this.node.getComponentsInChildren(cc.Widget).forEach(function(widget) {
            widget.enabled && widget.updateAlignment();
          });
        });
      };
      KKGameOrientationCanvas.prototype.switchCanvas = function() {
        window.innerWidth >= window.innerHeight ? KKGameOrientationToolManager_1.KKGameOrientationToolManager.orientationState = KKGameOrientationToolManager_1.KKGAME_ORIENTATION_TYPE.HORIZONTAL : KKGameOrientationToolManager_1.KKGameOrientationToolManager.orientationState = KKGameOrientationToolManager_1.KKGAME_ORIENTATION_TYPE.VERTICAL;
        this.setCanvasSize();
      };
      KKGameOrientationCanvas = __decorate([ ccclass ], KKGameOrientationCanvas);
      return KKGameOrientationCanvas;
    }(cc.Component);
    exports.KKGameOrientationCanvas = KKGameOrientationCanvas;
    cc._RF.pop();
  }, {
    "./KKGameOrientationToolManager": "KKGameOrientationToolManager"
  } ],
  KKGameOrientationToolManager: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "bb3e48+RARCOqHYr3blRo7K", "KKGameOrientationToolManager");
    "use strict";
    var __extends = this && this.__extends || function() {
      var extendStatics = function(d, b) {
        extendStatics = Object.setPrototypeOf || {
          __proto__: []
        } instanceof Array && function(d, b) {
          d.__proto__ = b;
        } || function(d, b) {
          for (var p in b) Object.prototype.hasOwnProperty.call(b, p) && (d[p] = b[p]);
        };
        return extendStatics(d, b);
      };
      return function(d, b) {
        extendStatics(d, b);
        function __() {
          this.constructor = d;
        }
        d.prototype = null === b ? Object.create(b) : (__.prototype = b.prototype, new __());
      };
    }();
    var __decorate = this && this.__decorate || function(decorators, target, key, desc) {
      var c = arguments.length, r = c < 3 ? target : null === desc ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
      if ("object" === typeof Reflect && "function" === typeof Reflect.decorate) r = Reflect.decorate(decorators, target, key, desc); else for (var i = decorators.length - 1; i >= 0; i--) (d = decorators[i]) && (r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r);
      return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    exports.KKGameOrientationToolManager = exports.KKGAME_ORIENTATION_TYPE = void 0;
    var KKGameOrientationTool_1 = require("./KKGameOrientationTool");
    var _a = cc._decorator, ccclass = _a.ccclass, executeInEditMode = _a.executeInEditMode;
    var KKGAME_ORIENTATION_TYPE;
    (function(KKGAME_ORIENTATION_TYPE) {
      KKGAME_ORIENTATION_TYPE[KKGAME_ORIENTATION_TYPE["NONE"] = 0] = "NONE";
      KKGAME_ORIENTATION_TYPE[KKGAME_ORIENTATION_TYPE["HORIZONTAL"] = 1] = "HORIZONTAL";
      KKGAME_ORIENTATION_TYPE[KKGAME_ORIENTATION_TYPE["VERTICAL"] = 2] = "VERTICAL";
    })(KKGAME_ORIENTATION_TYPE = exports.KKGAME_ORIENTATION_TYPE || (exports.KKGAME_ORIENTATION_TYPE = {}));
    var KKGameOrientationToolManager = function(_super) {
      __extends(KKGameOrientationToolManager, _super);
      function KKGameOrientationToolManager() {
        var _this = null !== _super && _super.apply(this, arguments) || this;
        _this.orientationToolList = {};
        return _this;
      }
      KKGameOrientationToolManager_1 = KKGameOrientationToolManager;
      Object.defineProperty(KKGameOrientationToolManager, "lockingOrientation", {
        get: function() {
          return KKGameOrientationToolManager_1._lockingOrientation;
        },
        set: function(param) {
          KKGameOrientationToolManager_1._lockingOrientation = param;
          param || (KKGameOrientationToolManager_1.orientationState === KKGAME_ORIENTATION_TYPE.HORIZONTAL ? KKGameOrientationToolManager_1.getInstance().toHorizontal() : KKGameOrientationToolManager_1.orientationState === KKGAME_ORIENTATION_TYPE.VERTICAL && KKGameOrientationToolManager_1.getInstance().toVerticall());
        },
        enumerable: false,
        configurable: true
      });
      KKGameOrientationToolManager.prototype.onLoad = function() {
        true;
        this.getAllTools();
      };
      KKGameOrientationToolManager.prototype.toVerticall = function() {
        for (var key in this.orientationToolList) this.orientationToolList.hasOwnProperty(key) && cc.isValid(this.orientationToolList[key]) && this.orientationToolList[key].toVerticall();
      };
      KKGameOrientationToolManager.prototype.toHorizontal = function() {
        for (var key in this.orientationToolList) this.orientationToolList.hasOwnProperty(key) && cc.isValid(this.orientationToolList[key]) && this.orientationToolList[key].toHorizontal();
      };
      KKGameOrientationToolManager.prototype.addToList = function(item) {
        this.orientationToolList[item.uuid] = item;
      };
      KKGameOrientationToolManager.getInstance = function() {
        var _a;
        if (!cc.director.getScene()) return;
        var obj = cc.find("OrientationToolManager");
        if (null == obj) {
          var canvas = null === (_a = cc.Canvas.instance) || void 0 === _a ? void 0 : _a.node;
          if (null == canvas) return;
          obj = new cc.Node("OrientationToolManager");
          obj.setParent(canvas.parent);
          obj.addComponent(KKGameOrientationToolManager_1);
        }
        return obj.getComponent(KKGameOrientationToolManager_1);
      };
      KKGameOrientationToolManager.prototype.getAllTools = function() {
        var node = cc.Canvas.instance.node.parent;
        var tools = node.getComponentsInChildren(KKGameOrientationTool_1.KKGameOrientationTool);
        for (var i = 0; i < tools.length; i++) {
          tools[i].isStaticNode = true;
          this.orientationToolList[tools[i].uuid] = tools[i];
        }
      };
      var KKGameOrientationToolManager_1;
      KKGameOrientationToolManager.orientationState = KKGAME_ORIENTATION_TYPE.NONE;
      KKGameOrientationToolManager._lockingOrientation = false;
      KKGameOrientationToolManager = KKGameOrientationToolManager_1 = __decorate([ ccclass, executeInEditMode ], KKGameOrientationToolManager);
      return KKGameOrientationToolManager;
    }(cc.Component);
    exports.KKGameOrientationToolManager = KKGameOrientationToolManager;
    cc._RF.pop();
  }, {
    "./KKGameOrientationTool": "KKGameOrientationTool"
  } ],
  KKGameOrientationToolModule: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "0df80lwEMdOS5l9zbIOU5A9", "KKGameOrientationToolModule");
    "use strict";
    var __decorate = this && this.__decorate || function(decorators, target, key, desc) {
      var c = arguments.length, r = c < 3 ? target : null === desc ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
      if ("object" === typeof Reflect && "function" === typeof Reflect.decorate) r = Reflect.decorate(decorators, target, key, desc); else for (var i = decorators.length - 1; i >= 0; i--) (d = decorators[i]) && (r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r);
      return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    exports.KKGamePolygonModule = exports.KKGameLabelModule = exports.KKGameI18nLabelModuleItem = exports.KKGameMaskModule = exports.KKGameSpriteModule = exports.KKGameSpineModule = exports.KKGameI18nSpineModuleItem = exports.KKGameAnimationModule = exports.KKGameNodeModule = void 0;
    var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
    var KKGameI18nNodeModuleItem = function() {
      function KKGameI18nNodeModuleItem() {
        this.language = "";
        this.position = null;
        this.size = null;
      }
      __decorate([ property() ], KKGameI18nNodeModuleItem.prototype, "language", void 0);
      __decorate([ property(cc.Vec3) ], KKGameI18nNodeModuleItem.prototype, "position", void 0);
      __decorate([ property(cc.Size) ], KKGameI18nNodeModuleItem.prototype, "size", void 0);
      KKGameI18nNodeModuleItem = __decorate([ ccclass("KKGameI18nNodeModuleItem") ], KKGameI18nNodeModuleItem);
      return KKGameI18nNodeModuleItem;
    }();
    var KKGameNodeModule = function() {
      function KKGameNodeModule() {
        this.useI18n = false;
        this.i18nReferenceList = [];
        this.nodeActive = false;
        this.position = new cc.Vec3(0, 0);
        this.rotation = 0;
        this.rotation3D = new cc.Vec3(0, 0, 0);
        this.scale = new cc.Vec3(0, 0, 0);
        this.size = new cc.Size(0, 0);
        this.richTextSyncMaxWidth = false;
        this.opacity = 0;
        this.color = cc.Color.WHITE;
        this.isInit = false;
      }
      __decorate([ property() ], KKGameNodeModule.prototype, "useI18n", void 0);
      __decorate([ property({
        type: [ KKGameI18nNodeModuleItem ],
        visible: function() {
          return this.useI18n;
        }
      }) ], KKGameNodeModule.prototype, "i18nReferenceList", void 0);
      __decorate([ property(cc.Boolean) ], KKGameNodeModule.prototype, "nodeActive", void 0);
      __decorate([ property(cc.Vec3) ], KKGameNodeModule.prototype, "position", void 0);
      __decorate([ property(cc.Float) ], KKGameNodeModule.prototype, "rotation", void 0);
      __decorate([ property(cc.Vec3) ], KKGameNodeModule.prototype, "rotation3D", void 0);
      __decorate([ property(cc.Vec3) ], KKGameNodeModule.prototype, "scale", void 0);
      __decorate([ property(cc.Size) ], KKGameNodeModule.prototype, "size", void 0);
      __decorate([ property(cc.Boolean) ], KKGameNodeModule.prototype, "richTextSyncMaxWidth", void 0);
      __decorate([ property({
        type: cc.Float,
        range: [ 0, 255, 255 ]
      }) ], KKGameNodeModule.prototype, "opacity", void 0);
      __decorate([ property(cc.Color) ], KKGameNodeModule.prototype, "color", void 0);
      __decorate([ property({
        readonly: true,
        displayName: "\u662f\u5426\u5df2\u8a18\u61b6\u8a2d\u5b9a"
      }) ], KKGameNodeModule.prototype, "isInit", void 0);
      KKGameNodeModule = __decorate([ ccclass("KKGameNodeModule") ], KKGameNodeModule);
      return KKGameNodeModule;
    }();
    exports.KKGameNodeModule = KKGameNodeModule;
    var KKGameAnimationModule = function() {
      function KKGameAnimationModule() {
        this.defalutClip = null;
        this.isInit = false;
      }
      __decorate([ property(cc.AnimationClip) ], KKGameAnimationModule.prototype, "defalutClip", void 0);
      __decorate([ property(cc.Boolean) ], KKGameAnimationModule.prototype, "isInit", void 0);
      KKGameAnimationModule = __decorate([ ccclass("KKGameAnimationModule") ], KKGameAnimationModule);
      return KKGameAnimationModule;
    }();
    exports.KKGameAnimationModule = KKGameAnimationModule;
    var KKGameI18nSpineModuleItem = function() {
      function KKGameI18nSpineModuleItem() {
        this.language = "";
        this.skeletonData = null;
      }
      __decorate([ property() ], KKGameI18nSpineModuleItem.prototype, "language", void 0);
      __decorate([ property(sp.SkeletonData) ], KKGameI18nSpineModuleItem.prototype, "skeletonData", void 0);
      KKGameI18nSpineModuleItem = __decorate([ ccclass("KKGameI18nSpineModuleItem") ], KKGameI18nSpineModuleItem);
      return KKGameI18nSpineModuleItem;
    }();
    exports.KKGameI18nSpineModuleItem = KKGameI18nSpineModuleItem;
    var KKGameSpineModule = function() {
      function KKGameSpineModule() {
        this.useI18n = false;
        this.i18nReferenceList = [];
        this.skeletonData = null;
        this.animationName = "";
        this.isInit = false;
      }
      __decorate([ property() ], KKGameSpineModule.prototype, "useI18n", void 0);
      __decorate([ property({
        type: [ KKGameI18nSpineModuleItem ],
        visible: function() {
          return this.useI18n;
        }
      }) ], KKGameSpineModule.prototype, "i18nReferenceList", void 0);
      __decorate([ property({
        type: sp.SkeletonData,
        displayName: "\u9810\u8a2d\u7684 SkeletonData"
      }) ], KKGameSpineModule.prototype, "skeletonData", void 0);
      __decorate([ property({
        displayName: "\u9810\u8a2d Animation name"
      }) ], KKGameSpineModule.prototype, "animationName", void 0);
      __decorate([ property(cc.Boolean) ], KKGameSpineModule.prototype, "isInit", void 0);
      KKGameSpineModule = __decorate([ ccclass("KKGameSpineModule") ], KKGameSpineModule);
      return KKGameSpineModule;
    }();
    exports.KKGameSpineModule = KKGameSpineModule;
    var KKGameI18nSpriteModuleItem = function() {
      function KKGameI18nSpriteModuleItem() {
        this.language = "";
        this.spriteFrame = null;
        this.material = null;
      }
      __decorate([ property() ], KKGameI18nSpriteModuleItem.prototype, "language", void 0);
      __decorate([ property(cc.SpriteFrame) ], KKGameI18nSpriteModuleItem.prototype, "spriteFrame", void 0);
      __decorate([ property(cc.Material) ], KKGameI18nSpriteModuleItem.prototype, "material", void 0);
      KKGameI18nSpriteModuleItem = __decorate([ ccclass("KKGameI18nSpriteModuleItem") ], KKGameI18nSpriteModuleItem);
      return KKGameI18nSpriteModuleItem;
    }();
    var KKGameSpriteModule = function() {
      function KKGameSpriteModule() {
        this.useI18n = false;
        this.i18nReferenceList = [];
        this.spriteFrame = null;
        this.material = null;
        this.isInit = false;
      }
      __decorate([ property() ], KKGameSpriteModule.prototype, "useI18n", void 0);
      __decorate([ property({
        type: [ KKGameI18nSpriteModuleItem ],
        visible: function() {
          return this.useI18n;
        }
      }) ], KKGameSpriteModule.prototype, "i18nReferenceList", void 0);
      __decorate([ property({
        type: cc.SpriteFrame,
        displayName: "\u9810\u8a2d SpriteFrame"
      }) ], KKGameSpriteModule.prototype, "spriteFrame", void 0);
      __decorate([ property({
        type: cc.Material,
        displayName: "\u9810\u8a2d Material"
      }) ], KKGameSpriteModule.prototype, "material", void 0);
      __decorate([ property(cc.Boolean) ], KKGameSpriteModule.prototype, "isInit", void 0);
      KKGameSpriteModule = __decorate([ ccclass("KKGameSpriteModule") ], KKGameSpriteModule);
      return KKGameSpriteModule;
    }();
    exports.KKGameSpriteModule = KKGameSpriteModule;
    var KKGameMaskModule = function() {
      function KKGameMaskModule() {
        this.alphaThreshold = 0;
        this.spriteFrame = null;
        this.isInit = false;
      }
      __decorate([ property({
        type: cc.Float,
        range: [ 0, 1, 0 ],
        step: .1,
        slide: true
      }) ], KKGameMaskModule.prototype, "alphaThreshold", void 0);
      __decorate([ property(cc.SpriteFrame) ], KKGameMaskModule.prototype, "spriteFrame", void 0);
      __decorate([ property(cc.Boolean) ], KKGameMaskModule.prototype, "isInit", void 0);
      KKGameMaskModule = __decorate([ ccclass("KKGameMaskModule") ], KKGameMaskModule);
      return KKGameMaskModule;
    }();
    exports.KKGameMaskModule = KKGameMaskModule;
    var KKGameI18nLabelModuleItem = function() {
      function KKGameI18nLabelModuleItem() {
        this.language = "";
        this.fontSize = 0;
        this.lineHeight = 0;
        this.spacingX = 0;
        this.font = null;
        this.enableBold = false;
        this.key = "";
      }
      __decorate([ property() ], KKGameI18nLabelModuleItem.prototype, "language", void 0);
      __decorate([ property(cc.Float) ], KKGameI18nLabelModuleItem.prototype, "fontSize", void 0);
      __decorate([ property(cc.Float) ], KKGameI18nLabelModuleItem.prototype, "lineHeight", void 0);
      __decorate([ property(cc.Float) ], KKGameI18nLabelModuleItem.prototype, "spacingX", void 0);
      __decorate([ property(cc.Font) ], KKGameI18nLabelModuleItem.prototype, "font", void 0);
      __decorate([ property() ], KKGameI18nLabelModuleItem.prototype, "enableBold", void 0);
      __decorate([ property() ], KKGameI18nLabelModuleItem.prototype, "key", void 0);
      KKGameI18nLabelModuleItem = __decorate([ ccclass("KKGameI18nLabelModuleItem") ], KKGameI18nLabelModuleItem);
      return KKGameI18nLabelModuleItem;
    }();
    exports.KKGameI18nLabelModuleItem = KKGameI18nLabelModuleItem;
    var KKGameLabelModule = function() {
      function KKGameLabelModule() {
        this.useI18n = false;
        this.i18nReferenceList = [];
        this.fontSize = 0;
        this.lineHeight = 0;
        this.spacingX = 0;
        this.font = null;
        this.enableBold = false;
        this.isInit = false;
      }
      __decorate([ property() ], KKGameLabelModule.prototype, "useI18n", void 0);
      __decorate([ property({
        type: [ KKGameI18nLabelModuleItem ],
        visible: function() {
          return this.useI18n;
        }
      }) ], KKGameLabelModule.prototype, "i18nReferenceList", void 0);
      __decorate([ property({
        type: cc.Float,
        displayName: "\u9810\u8a2d\u5b57\u9ad4\u5927\u5c0f"
      }) ], KKGameLabelModule.prototype, "fontSize", void 0);
      __decorate([ property({
        type: cc.Float,
        displayName: "\u9810\u8a2d\u884c\u9ad8"
      }) ], KKGameLabelModule.prototype, "lineHeight", void 0);
      __decorate([ property({
        type: cc.Float,
        displayName: "\u9810\u8a2d Spacing X"
      }) ], KKGameLabelModule.prototype, "spacingX", void 0);
      __decorate([ property({
        type: cc.Font,
        displayName: "\u9810\u8a2d\u5b57\u9ad4"
      }) ], KKGameLabelModule.prototype, "font", void 0);
      __decorate([ property({
        displayName: "\u9810\u8a2d\u662f\u5426\u4f7f\u7528\u7c97\u9ad4"
      }) ], KKGameLabelModule.prototype, "enableBold", void 0);
      __decorate([ property(cc.Boolean) ], KKGameLabelModule.prototype, "isInit", void 0);
      KKGameLabelModule = __decorate([ ccclass("KKGameLabelModule") ], KKGameLabelModule);
      return KKGameLabelModule;
    }();
    exports.KKGameLabelModule = KKGameLabelModule;
    var KKGamePolygonModule = function() {
      function KKGamePolygonModule() {
        this.points = [];
        this.isInit = false;
      }
      __decorate([ property({
        type: [ cc.Vec2 ]
      }) ], KKGamePolygonModule.prototype, "points", void 0);
      __decorate([ property(cc.Boolean) ], KKGamePolygonModule.prototype, "isInit", void 0);
      KKGamePolygonModule = __decorate([ ccclass("KKGamePolygonModule") ], KKGamePolygonModule);
      return KKGamePolygonModule;
    }();
    exports.KKGamePolygonModule = KKGamePolygonModule;
    cc._RF.pop();
  }, {} ],
  KKGameOrientationTool: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "960aftKOrpEFITJMCfiG09y", "KKGameOrientationTool");
    "use strict";
    var __extends = this && this.__extends || function() {
      var extendStatics = function(d, b) {
        extendStatics = Object.setPrototypeOf || {
          __proto__: []
        } instanceof Array && function(d, b) {
          d.__proto__ = b;
        } || function(d, b) {
          for (var p in b) Object.prototype.hasOwnProperty.call(b, p) && (d[p] = b[p]);
        };
        return extendStatics(d, b);
      };
      return function(d, b) {
        extendStatics(d, b);
        function __() {
          this.constructor = d;
        }
        d.prototype = null === b ? Object.create(b) : (__.prototype = b.prototype, new __());
      };
    }();
    var __decorate = this && this.__decorate || function(decorators, target, key, desc) {
      var c = arguments.length, r = c < 3 ? target : null === desc ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
      if ("object" === typeof Reflect && "function" === typeof Reflect.decorate) r = Reflect.decorate(decorators, target, key, desc); else for (var i = decorators.length - 1; i >= 0; i--) (d = decorators[i]) && (r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r);
      return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    exports.KKGameOrientationTool = void 0;
    var KKGameOrientationToolManager_1 = require("./KKGameOrientationToolManager");
    var KKGameOrientationToolModule_1 = require("./KKGameOrientationToolModule");
    var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property, executeInEditMode = _a.executeInEditMode, menu = _a.menu;
    var OrientationModeStatus;
    (function(OrientationModeStatus) {
      OrientationModeStatus[OrientationModeStatus["None"] = 0] = "None";
      OrientationModeStatus[OrientationModeStatus["Verticall"] = 1] = "Verticall";
      OrientationModeStatus[OrientationModeStatus["Horizontal"] = 2] = "Horizontal";
    })(OrientationModeStatus || (OrientationModeStatus = {}));
    var KKGameOrientationTool = function(_super) {
      __extends(KKGameOrientationTool, _super);
      function KKGameOrientationTool() {
        var _this = null !== _super && _super.apply(this, arguments) || this;
        _this.statusDisplay = "";
        _this.showParameter = false;
        _this.nodeModule = false;
        _this.verticalNodeModule = new KKGameOrientationToolModule_1.KKGameNodeModule();
        _this.horizontalNodeModule = new KKGameOrientationToolModule_1.KKGameNodeModule();
        _this.animationModule = false;
        _this.verticalAnimationModule = new KKGameOrientationToolModule_1.KKGameAnimationModule();
        _this.horizontalAnimationModule = new KKGameOrientationToolModule_1.KKGameAnimationModule();
        _this.spineModule = false;
        _this.verticalSpineModule = new KKGameOrientationToolModule_1.KKGameSpineModule();
        _this.horizontalSpineModule = new KKGameOrientationToolModule_1.KKGameSpineModule();
        _this.spriteModule = false;
        _this.verticalSpriteModule = new KKGameOrientationToolModule_1.KKGameSpriteModule();
        _this.horizontalSpriteModule = new KKGameOrientationToolModule_1.KKGameSpriteModule();
        _this.maskModule = false;
        _this.verticalMaskModule = new KKGameOrientationToolModule_1.KKGameMaskModule();
        _this.horizontalMaskModule = new KKGameOrientationToolModule_1.KKGameMaskModule();
        _this.labelModule = false;
        _this.verticalLabelModule = new KKGameOrientationToolModule_1.KKGameLabelModule();
        _this.horizontalLabelModule = new KKGameOrientationToolModule_1.KKGameLabelModule();
        _this.polygonModule = false;
        _this.verticalPolygonModule = new KKGameOrientationToolModule_1.KKGamePolygonModule();
        _this.horizontalPolygonModule = new KKGameOrientationToolModule_1.KKGamePolygonModule();
        _this.particleReset = false;
        _this.particleList = [];
        _this.canvas = null;
        _this.mask = null;
        _this.animation = null;
        _this.sprite = null;
        _this.spine = null;
        _this.label = null;
        _this.polygon = null;
        _this.missSpine = false;
        _this.orientationMode = OrientationModeStatus.None;
        _this.isStaticNode = false;
        return _this;
      }
      KKGameOrientationTool.prototype.start = function() {
        var _a, _b, _c;
        null === (_a = KKGameOrientationToolManager_1.KKGameOrientationToolManager.getInstance()) || void 0 === _a ? void 0 : _a.addToList(this);
        if (true, this.spriteModule) {
          null === (_b = this.verticalSpriteModule.spriteFrame) || void 0 === _b ? void 0 : _b.addRef();
          null === (_c = this.horizontalSpriteModule.spriteFrame) || void 0 === _c ? void 0 : _c.addRef();
        }
      };
      KKGameOrientationTool.prototype.onEnable = function() {
        var useNodeModule;
        false;
        KKGameOrientationToolManager_1.KKGameOrientationToolManager.orientationState === KKGameOrientationToolManager_1.KKGAME_ORIENTATION_TYPE.NONE || this.isStaticNode || this.canvasResize();
      };
      KKGameOrientationTool.prototype.canvasResize = function() {
        KKGameOrientationToolManager_1.KKGameOrientationToolManager.orientationState === KKGameOrientationToolManager_1.KKGAME_ORIENTATION_TYPE.HORIZONTAL ? this.toHorizontal() : this.toVerticall();
        this.resetParticle();
      };
      KKGameOrientationTool.prototype.resetParticle = function() {
        this.particleList.forEach(function(particleNode) {
          if (!particleNode.active) return;
          var particle2D = particleNode.getComponent(cc.ParticleSystem);
          var particle3D = particleNode.getComponent(cc.ParticleSystem3D);
          if (particle2D) particle2D.resetSystem(); else if (particle3D) {
            particle3D.clear();
            particle3D.loop && particle3D.play();
          }
        });
      };
      KKGameOrientationTool.prototype.toVerticall = function() {
        this.orientationMode = OrientationModeStatus.Verticall;
        this.setModules();
        this.statusDisplay = "\u76f4\u7248";
      };
      KKGameOrientationTool.prototype.toHorizontal = function() {
        this.orientationMode = OrientationModeStatus.Horizontal;
        this.setModules();
        this.statusDisplay = "\u6a6b\u7248";
      };
      KKGameOrientationTool.prototype.updateNodeModuleParameter = function(mode) {
        var useModule = mode == OrientationModeStatus.Verticall ? this.verticalNodeModule : this.horizontalNodeModule;
        useModule.isInit = true;
        useModule.nodeActive = this.node.active;
        useModule.color = this.node.color;
        useModule.position = this.node.position;
        if (this.node.is3DNode) {
          useModule.rotation3D.x = this.node.eulerAngles.x;
          useModule.rotation3D.y = this.node.eulerAngles.y;
          useModule.rotation3D.z = this.node.eulerAngles.z;
        } else useModule.rotation = this.node.angle;
        this.node.getScale(useModule.scale);
        useModule.size = this.node.getContentSize();
        useModule.opacity = this.node.opacity;
      };
      KKGameOrientationTool.prototype.updateAnimationModule = function(mode) {
        this.animation = this.node.getComponent(cc.Animation);
        if (false == cc.isValid(this.animation)) return;
        var useModule = mode == OrientationModeStatus.Verticall ? this.verticalAnimationModule : this.horizontalAnimationModule;
        useModule.isInit = true;
        useModule.defalutClip = this.animation.defaultClip;
      };
      KKGameOrientationTool.prototype.updateSpineModule = function(mode) {
        this.spine = this.node.getComponent(sp.Skeleton);
        if (false == cc.isValid(this.spine)) return;
        var useModule = mode == OrientationModeStatus.Verticall ? this.verticalSpineModule : this.horizontalSpineModule;
        useModule.isInit = true;
        useModule.animationName = this.spine.animation;
        useModule.skeletonData = this.spine.skeletonData;
      };
      KKGameOrientationTool.prototype.updateSpriteModule = function(mode) {
        this.sprite = this.node.getComponent(cc.Sprite);
        if (false == cc.isValid(this.sprite)) return;
        var useModule = mode == OrientationModeStatus.Verticall ? this.verticalSpriteModule : this.horizontalSpriteModule;
        useModule.isInit = true;
        useModule.spriteFrame = this.sprite.spriteFrame;
        useModule.material = this.sprite.getMaterial(0);
      };
      KKGameOrientationTool.prototype.updateMaskModule = function(mode) {
        this.mask = this.node.getComponent(cc.Mask);
        if (false == cc.isValid(this.mask)) return;
        var useModule = mode == OrientationModeStatus.Verticall ? this.verticalMaskModule : this.horizontalMaskModule;
        useModule.isInit = true;
        useModule.spriteFrame = this.mask.spriteFrame;
        useModule.alphaThreshold = this.mask.alphaThreshold;
      };
      KKGameOrientationTool.prototype.updateLableModule = function(mode) {
        this.label = this.node.getComponent(cc.Label);
        if (false == cc.isValid(this.label)) return;
        var useModule = mode == OrientationModeStatus.Verticall ? this.verticalLabelModule : this.horizontalLabelModule;
        useModule.isInit = true;
        useModule.fontSize = this.label.fontSize;
        useModule.lineHeight = this.label.lineHeight;
        useModule.spacingX = this.label.spacingX;
        useModule.font = this.label.font;
        useModule.enableBold = this.label.enableBold;
      };
      KKGameOrientationTool.prototype.updatePolygonModule = function(mode) {
        this.polygon = this.node.getComponent(cc.PolygonCollider);
        if (false == cc.isValid(this.polygon)) return;
        var useModule = mode == OrientationModeStatus.Verticall ? this.verticalPolygonModule : this.horizontalPolygonModule;
        useModule.isInit = true;
        useModule.points = this.polygon.points;
      };
      KKGameOrientationTool.prototype.setModules = function() {
        this.orientationMode === OrientationModeStatus.None && (this.orientationMode = cc.Canvas.instance.designResolution.height > cc.Canvas.instance.designResolution.width ? OrientationModeStatus.Verticall : OrientationModeStatus.Horizontal);
        this.nodeModule && this.setNodeModule();
        this.animationModule && this.setAnimationModule();
      };
      KKGameOrientationTool.prototype.setNodeModule = function() {
        var useModule = this.orientationMode == OrientationModeStatus.Verticall ? this.verticalNodeModule : this.horizontalNodeModule;
        if (false == useModule.isInit) return;
        this.node.active = useModule.nodeActive;
        this.node.color = useModule.color;
        this.node.setPosition(useModule.position);
        this.node.setContentSize(useModule.size);
        useModule.richTextSyncMaxWidth && this.node.getComponent(cc.RichText) && (this.node.getComponent(cc.RichText).maxWidth = this.node.width);
        this.node.angle = useModule.rotation;
        if (this.node.is3DNode) {
          var temp = new cc.Quat();
          this.node.setRotation(temp.fromEuler(useModule.rotation3D));
        } else this.node.angle = useModule.rotation;
        this.node.setScale(useModule.scale);
        this.node.opacity = useModule.opacity;
      };
      KKGameOrientationTool.prototype.setAnimationModule = function() {
        this.animation = this.node.getComponent(cc.Animation);
        if (false == cc.isValid(this.animation)) return;
        var useModule = this.orientationMode == OrientationModeStatus.Verticall ? this.verticalAnimationModule : this.horizontalAnimationModule;
        if (false == useModule.isInit) return;
        if (null == useModule.defalutClip) return;
        var nowClipName = this.getNowAnimationPlayClipName(this.animation);
        var clips = this.animation.getClips();
        var hasClip = false;
        for (var i = 0; i < clips.length; i++) if (clips[i] == useModule.defalutClip) {
          hasClip = true;
          break;
        }
        hasClip && this.animation.addClip(useModule.defalutClip, useModule.defalutClip.name);
        this.animation.defaultClip = useModule.defalutClip;
        true;
        if (!nowClipName) return;
        var animState = this.animation.getAnimationState(nowClipName);
        var time = 0;
        cc.isValid(animState) && (time = animState.time);
        this.animation.play(useModule.defalutClip.name, time);
      };
      KKGameOrientationTool.prototype.getNowAnimationPlayClipName = function(animation) {
        var clips = animation.getClips();
        for (var i = 0; i < clips.length; i++) {
          var animState = animation.getAnimationState(clips[i].name);
          if (animState.isPlaying) return clips[i].name;
        }
        return null;
      };
      KKGameOrientationTool.prototype.changeName = function(name, mode) {
        var newName = "";
        var array = name.split("_");
        for (var i = 0; i < array.length - 1; i++) newName += array[i] + "_";
        newName += mode == OrientationModeStatus.Verticall ? "V" : "H";
        return newName;
      };
      KKGameOrientationTool.prototype.getCanvas = function() {
        if (!cc.Canvas.instance) return;
        if (false == cc.isValid(this.canvas)) {
          var canvasNode = cc.Canvas.instance.node;
          if (false == cc.isValid(canvasNode)) {
            console.log("\u627e\u4e0d\u5230Canvas");
            return null;
          }
          this.canvas = canvasNode.getComponent(cc.Canvas);
        }
        return this.canvas;
      };
      KKGameOrientationTool.prototype.getNowOrientationMode = function() {
        var canvas = this.getCanvas();
        if (false == cc.isValid(canvas)) return null;
        if (canvas.designResolution.width > canvas.designResolution.height) return OrientationModeStatus.Horizontal;
        return OrientationModeStatus.Verticall;
      };
      __decorate([ property({
        readonly: true,
        displayName: "\u76f4\u6a6b\u7248\u72c0\u614b"
      }) ], KKGameOrientationTool.prototype, "statusDisplay", void 0);
      __decorate([ property(cc.Boolean) ], KKGameOrientationTool.prototype, "showParameter", void 0);
      __decorate([ property(cc.Boolean) ], KKGameOrientationTool.prototype, "nodeModule", void 0);
      __decorate([ property({
        visible: function() {
          return this.nodeModule && this.showParameter;
        },
        type: KKGameOrientationToolModule_1.KKGameNodeModule
      }) ], KKGameOrientationTool.prototype, "verticalNodeModule", void 0);
      __decorate([ property({
        visible: function() {
          return this.nodeModule && this.showParameter;
        },
        type: KKGameOrientationToolModule_1.KKGameNodeModule
      }) ], KKGameOrientationTool.prototype, "horizontalNodeModule", void 0);
      __decorate([ property(cc.Boolean) ], KKGameOrientationTool.prototype, "animationModule", void 0);
      __decorate([ property({
        visible: function() {
          return this.animationModule && this.showParameter;
        },
        type: KKGameOrientationToolModule_1.KKGameAnimationModule
      }) ], KKGameOrientationTool.prototype, "verticalAnimationModule", void 0);
      __decorate([ property({
        visible: function() {
          return this.animationModule && this.showParameter;
        },
        type: KKGameOrientationToolModule_1.KKGameAnimationModule
      }) ], KKGameOrientationTool.prototype, "horizontalAnimationModule", void 0);
      __decorate([ property(cc.Boolean) ], KKGameOrientationTool.prototype, "spineModule", void 0);
      __decorate([ property({
        visible: function() {
          return this.spineModule && this.showParameter;
        },
        type: KKGameOrientationToolModule_1.KKGameSpineModule
      }) ], KKGameOrientationTool.prototype, "verticalSpineModule", void 0);
      __decorate([ property({
        visible: function() {
          return this.spineModule && this.showParameter;
        },
        type: KKGameOrientationToolModule_1.KKGameSpineModule
      }) ], KKGameOrientationTool.prototype, "horizontalSpineModule", void 0);
      __decorate([ property(cc.Boolean) ], KKGameOrientationTool.prototype, "spriteModule", void 0);
      __decorate([ property({
        visible: function() {
          return this.spriteModule && this.showParameter;
        },
        type: KKGameOrientationToolModule_1.KKGameSpriteModule
      }) ], KKGameOrientationTool.prototype, "verticalSpriteModule", void 0);
      __decorate([ property({
        visible: function() {
          return this.spriteModule && this.showParameter;
        },
        type: KKGameOrientationToolModule_1.KKGameSpriteModule
      }) ], KKGameOrientationTool.prototype, "horizontalSpriteModule", void 0);
      __decorate([ property(cc.Boolean) ], KKGameOrientationTool.prototype, "maskModule", void 0);
      __decorate([ property({
        visible: function() {
          return this.maskModule && this.showParameter;
        },
        type: KKGameOrientationToolModule_1.KKGameMaskModule
      }) ], KKGameOrientationTool.prototype, "verticalMaskModule", void 0);
      __decorate([ property({
        visible: function() {
          return this.maskModule && this.showParameter;
        },
        type: KKGameOrientationToolModule_1.KKGameMaskModule
      }) ], KKGameOrientationTool.prototype, "horizontalMaskModule", void 0);
      __decorate([ property(cc.Boolean) ], KKGameOrientationTool.prototype, "labelModule", void 0);
      __decorate([ property({
        visible: function() {
          return this.labelModule && this.showParameter;
        },
        type: KKGameOrientationToolModule_1.KKGameLabelModule
      }) ], KKGameOrientationTool.prototype, "verticalLabelModule", void 0);
      __decorate([ property({
        visible: function() {
          return this.labelModule && this.showParameter;
        },
        type: KKGameOrientationToolModule_1.KKGameLabelModule
      }) ], KKGameOrientationTool.prototype, "horizontalLabelModule", void 0);
      __decorate([ property(cc.Boolean) ], KKGameOrientationTool.prototype, "polygonModule", void 0);
      __decorate([ property({
        visible: function() {
          return this.polygonModule && this.showParameter;
        },
        type: KKGameOrientationToolModule_1.KKGamePolygonModule
      }) ], KKGameOrientationTool.prototype, "verticalPolygonModule", void 0);
      __decorate([ property({
        visible: function() {
          return this.polygonModule && this.showParameter;
        },
        type: KKGameOrientationToolModule_1.KKGamePolygonModule
      }) ], KKGameOrientationTool.prototype, "horizontalPolygonModule", void 0);
      __decorate([ property(cc.Boolean) ], KKGameOrientationTool.prototype, "particleReset", void 0);
      __decorate([ property({
        type: [ cc.Node ],
        visible: function() {
          return this.particleReset;
        }
      }) ], KKGameOrientationTool.prototype, "particleList", void 0);
      KKGameOrientationTool = __decorate([ ccclass, executeInEditMode ], KKGameOrientationTool);
      return KKGameOrientationTool;
    }(cc.Component);
    exports.KKGameOrientationTool = KKGameOrientationTool;
    cc._RF.pop();
  }, {
    "./KKGameOrientationToolManager": "KKGameOrientationToolManager",
    "./KKGameOrientationToolModule": "KKGameOrientationToolModule"
  } ],
  KKGame: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "f3eeftegoZKbIXTdIBqfgA9", "KKGame");
    "use strict";
    var __extends = this && this.__extends || function() {
      var extendStatics = function(d, b) {
        extendStatics = Object.setPrototypeOf || {
          __proto__: []
        } instanceof Array && function(d, b) {
          d.__proto__ = b;
        } || function(d, b) {
          for (var p in b) Object.prototype.hasOwnProperty.call(b, p) && (d[p] = b[p]);
        };
        return extendStatics(d, b);
      };
      return function(d, b) {
        extendStatics(d, b);
        function __() {
          this.constructor = d;
        }
        d.prototype = null === b ? Object.create(b) : (__.prototype = b.prototype, new __());
      };
    }();
    var __decorate = this && this.__decorate || function(decorators, target, key, desc) {
      var c = arguments.length, r = c < 3 ? target : null === desc ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
      if ("object" === typeof Reflect && "function" === typeof Reflect.decorate) r = Reflect.decorate(decorators, target, key, desc); else for (var i = decorators.length - 1; i >= 0; i--) (d = decorators[i]) && (r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r);
      return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    exports.KKGame = void 0;
    var SceneManager_1 = require("../scripts/SceneManager");
    var UrlManager_1 = require("../scripts/UrlManager");
    var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
    var KKGame = function(_super) {
      __extends(KKGame, _super);
      function KKGame() {
        var _this = null !== _super && _super.apply(this, arguments) || this;
        _this.audioSource = null;
        _this.jingleList = [];
        return _this;
      }
      KKGame.prototype.onLoad = function() {
        UrlManager_1.UrlManager.init(window.location.href);
        var gameName = UrlManager_1.UrlManager.getGameId();
        var materialId = Number(UrlManager_1.UrlManager.getMaterialId());
        this.jingleList[materialId - 1].active = true;
        SceneManager_1.SceneManager.init();
        cc.sys.isMobile && SceneManager_1.SceneManager.setOrientation(gameName);
        this.changeScene(gameName);
        this.registerBackgroundEvent();
      };
      KKGame.prototype.start = function() {
        var musicVolume = window.localStorage.getItem("demeter_setting_music_volume");
        this.audioSource.volume = musicVolume ? Number(musicVolume) : 1;
        this.audioSource.play();
      };
      KKGame.prototype.onDestroy = function() {
        this.unRegisterBackgroundEvent();
      };
      KKGame.prototype.changeScene = function(gameName) {
        setTimeout(function() {
          SceneManager_1.SceneManager.changeScene(gameName);
        }, 2500);
      };
      KKGame.prototype.registerBackgroundEvent = function() {
        cc.game.on(cc.game.EVENT_HIDE, this.muteBG, this);
        cc.game.on(cc.game.EVENT_SHOW, this.unMuteBG, this);
      };
      KKGame.prototype.unRegisterBackgroundEvent = function() {
        cc.game.off(cc.game.EVENT_HIDE, this.muteBG, this);
        cc.game.off(cc.game.EVENT_SHOW, this.unMuteBG, this);
      };
      KKGame.prototype.muteBG = function() {
        this.audioSource.mute = true;
      };
      KKGame.prototype.unMuteBG = function() {
        this.audioSource.mute = false;
      };
      __decorate([ property(cc.AudioSource) ], KKGame.prototype, "audioSource", void 0);
      __decorate([ property([ cc.Node ]) ], KKGame.prototype, "jingleList", void 0);
      KKGame = __decorate([ ccclass ], KKGame);
      return KKGame;
    }(cc.Component);
    exports.KKGame = KKGame;
    cc._RF.pop();
  }, {
    "../scripts/SceneManager": "SceneManager",
    "../scripts/UrlManager": "UrlManager"
  } ],
  SceneManager: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "16bcaHAGmFIpbCscRzxs5jR", "SceneManager");
    "use strict";
    var __awaiter = this && this.__awaiter || function(thisArg, _arguments, P, generator) {
      function adopt(value) {
        return value instanceof P ? value : new P(function(resolve) {
          resolve(value);
        });
      }
      return new (P || (P = Promise))(function(resolve, reject) {
        function fulfilled(value) {
          try {
            step(generator.next(value));
          } catch (e) {
            reject(e);
          }
        }
        function rejected(value) {
          try {
            step(generator["throw"](value));
          } catch (e) {
            reject(e);
          }
        }
        function step(result) {
          result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected);
        }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
      });
    };
    var __generator = this && this.__generator || function(thisArg, body) {
      var _ = {
        label: 0,
        sent: function() {
          if (1 & t[0]) throw t[1];
          return t[1];
        },
        trys: [],
        ops: []
      }, f, y, t, g;
      return g = {
        next: verb(0),
        throw: verb(1),
        return: verb(2)
      }, "function" === typeof Symbol && (g[Symbol.iterator] = function() {
        return this;
      }), g;
      function verb(n) {
        return function(v) {
          return step([ n, v ]);
        };
      }
      function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
          if (f = 1, y && (t = 2 & op[0] ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 
          0) : y.next) && !(t = t.call(y, op[1])).done) return t;
          (y = 0, t) && (op = [ 2 & op[0], t.value ]);
          switch (op[0]) {
           case 0:
           case 1:
            t = op;
            break;

           case 4:
            _.label++;
            return {
              value: op[1],
              done: false
            };

           case 5:
            _.label++;
            y = op[1];
            op = [ 0 ];
            continue;

           case 7:
            op = _.ops.pop();
            _.trys.pop();
            continue;

           default:
            if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (6 === op[0] || 2 === op[0])) {
              _ = 0;
              continue;
            }
            if (3 === op[0] && (!t || op[1] > t[0] && op[1] < t[3])) {
              _.label = op[1];
              break;
            }
            if (6 === op[0] && _.label < t[1]) {
              _.label = t[1];
              t = op;
              break;
            }
            if (t && _.label < t[2]) {
              _.label = t[2];
              _.ops.push(op);
              break;
            }
            t[2] && _.ops.pop();
            _.trys.pop();
            continue;
          }
          op = body.call(thisArg, _);
        } catch (e) {
          op = [ 6, e ];
          y = 0;
        } finally {
          f = t = 0;
        }
        if (5 & op[0]) throw op[1];
        return {
          value: op[0] ? op[1] : void 0,
          done: true
        };
      }
    };
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    exports.SceneManager = void 0;
    var BundleManager_1 = require("./BundleManager");
    var UrlManager_1 = require("./UrlManager");
    var SceneManager = function() {
      function SceneManager() {}
      SceneManager.init = function() {
        var bundleConfig = cc.assetManager.assets.get(window.gameBundlesPath);
        var orientationConfig = cc.assetManager.assets.get(window.gameOrientationPath);
        this.gameMapping = window.gameBundlesData || bundleConfig.json;
        this.gameOrientation = window.gameOrientationData || orientationConfig.json;
      };
      SceneManager.changeScene = function(game) {
        return __awaiter(this, void 0, Promise, function() {
          var GAME_BUNDLES, gameType, bundleNames;
          var _this = this;
          return __generator(this, function(_a) {
            switch (_a.label) {
             case 0:
              if (!this.gameMapping[game]) {
                console.log("\u8acb\u8f38\u5165\u6b63\u78ba\u904a\u6232\u540d\u7a31");
                return [ 2 ];
              }
              GAME_BUNDLES = this.gameMapping[game];
              gameType = GAME_BUNDLES.filter(function(bundle) {
                return /Framework/.test(bundle);
              })[0];
              "SlotFramework" === gameType && (SceneManager.sdkType = "Slot");
              console.warn("Change Scene to " + game);
              SceneManager.currentGame = game;
              bundleNames = Object.assign([], this.gameMapping[game]);
              bundleNames.splice(bundleNames.indexOf(game), 1);
              bundleNames.splice(bundleNames.indexOf("Common"), 1);
              return [ 4, BundleManager_1.BundleManager.loadBundles([ "Common" ], game) ];

             case 1:
              _a.sent();
              return [ 4, BundleManager_1.BundleManager.loadBundles(bundleNames, game) ];

             case 2:
              _a.sent();
              return [ 4, Promise.all([ BundleManager_1.BundleManager.loadBundles([ game ], game), BundleManager_1.BundleManager.loadScript(game) ]) ];

             case 3:
              _a.sent();
              window.CrossGameManager.digestCrossItems();
              cc.director.loadScene(game + "_Loading", function() {
                _this.setOrientation(game);
                SceneManager.persistentRootNodeList.forEach(function(node) {
                  return node.isValid && node.destroy();
                });
                SceneManager.persistentRootNodeList = [];
              });
              return [ 2 ];
            }
          });
        });
      };
      SceneManager.setOrientation = function(game) {
        var orientation = this.gameOrientation[game];
        var orientationCode = "LANDSCAPE" == orientation ? cc.macro.ORIENTATION_LANDSCAPE : cc.macro.ORIENTATION_PORTRAIT;
        cc.view.setOrientation(orientationCode);
      };
      SceneManager.changeURLsToken = function(token) {
        history.replaceState(null, null, token);
        null == UrlManager_1.UrlManager.getUrlParams(UrlManager_1.UrlParamsKeys.FAST_MODE) && UrlManager_1.UrlManager.setUrlParams(UrlManager_1.UrlParamsKeys.FAST_MODE, "true");
        window.CrossGameManager.storeCrossItems();
      };
      SceneManager.registerPersistentRootNode = function(node) {
        SceneManager.persistentRootNodeList.push(node);
      };
      SceneManager.currentGame = null;
      SceneManager.sdkType = null;
      SceneManager.persistentRootNodeList = [];
      SceneManager.gameMapping = null;
      SceneManager.gameOrientation = null;
      return SceneManager;
    }();
    exports.SceneManager = SceneManager;
    window.SceneManager = SceneManager;
    cc._RF.pop();
  }, {
    "./BundleManager": "BundleManager",
    "./UrlManager": "UrlManager"
  } ],
  SentryManager: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "02ae9N98mhPx6hzXTwUQoOY", "SentryManager");
    "use strict";
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    exports.SentryManager = exports.ENV = exports.COCOS_EVENT_CATEGORIES = exports.SENTRY_TAG_NAME = exports.SENTRY_CONTEXT_SUB_TITLE = exports.SENTRY_CONTEXT_TITLE = exports.SENTRY_LEVEL = void 0;
    var SENTRY_LEVEL;
    (function(SENTRY_LEVEL) {
      SENTRY_LEVEL["FATAL"] = "fatal";
      SENTRY_LEVEL["CRITICAL"] = "critical";
      SENTRY_LEVEL["ERROR"] = "error";
      SENTRY_LEVEL["WARNING"] = "warning";
      SENTRY_LEVEL["LOG"] = "log";
      SENTRY_LEVEL["INFO"] = "info";
      SENTRY_LEVEL["DEBUG"] = "debug";
    })(SENTRY_LEVEL = exports.SENTRY_LEVEL || (exports.SENTRY_LEVEL = {}));
    var SENTRY_CONTEXT_TITLE;
    (function(SENTRY_CONTEXT_TITLE) {
      SENTRY_CONTEXT_TITLE["CALL_STACK"] = "CALL STACK";
    })(SENTRY_CONTEXT_TITLE = exports.SENTRY_CONTEXT_TITLE || (exports.SENTRY_CONTEXT_TITLE = {}));
    var SENTRY_CONTEXT_SUB_TITLE;
    (function(SENTRY_CONTEXT_SUB_TITLE) {
      SENTRY_CONTEXT_SUB_TITLE["STACK"] = "Stack";
    })(SENTRY_CONTEXT_SUB_TITLE = exports.SENTRY_CONTEXT_SUB_TITLE || (exports.SENTRY_CONTEXT_SUB_TITLE = {}));
    var SENTRY_TAG_NAME;
    (function(SENTRY_TAG_NAME) {
      SENTRY_TAG_NAME["COCOS_EVENT"] = "cocosEvent";
      SENTRY_TAG_NAME["GAME_ID"] = "gameId";
      SENTRY_TAG_NAME["PLAYER_ID"] = "playerId";
      SENTRY_TAG_NAME["SCENE"] = "scene";
      SENTRY_TAG_NAME["ERROR_CODE"] = "errorCode";
    })(SENTRY_TAG_NAME = exports.SENTRY_TAG_NAME || (exports.SENTRY_TAG_NAME = {}));
    var COCOS_EVENT_CATEGORIES;
    (function(COCOS_EVENT_CATEGORIES) {
      COCOS_EVENT_CATEGORIES["LOGIN"] = "login";
      COCOS_EVENT_CATEGORIES["BET"] = "bet";
      COCOS_EVENT_CATEGORIES["LOGIN_RECORD"] = "loginRecord";
      COCOS_EVENT_CATEGORIES["COMPUTING_FAILURE"] = "computingFailure";
    })(COCOS_EVENT_CATEGORIES = exports.COCOS_EVENT_CATEGORIES || (exports.COCOS_EVENT_CATEGORIES = {}));
    var ENV;
    (function(ENV) {
      ENV["DEV"] = "DEV";
      ENV["SIT"] = "SIT";
      ENV["UAT"] = "UAT";
      ENV["PROD"] = "PROD";
    })(ENV = exports.ENV || (exports.ENV = {}));
    var SentryManager = function() {
      function SentryManager() {}
      SentryManager.init = function(env, gameId) {
        if (false, env === ENV.DEV) return;
        var version = [ ENV[env], gameId, window.versionData[gameId] ].join("_");
        window.Sentry.init({
          dsn: "https://41c28e263dc94c0d86d91de81a6b5ffe@sentry-egame.pstdsf.com/3",
          release: "kkgame@" + version,
          integrations: [ new Sentry.BrowserTracing() ],
          environment: ENV[env],
          tracesSampleRate: 1,
          ignoreErrors: []
        });
        Sentry.configureScope(function(scope) {
          scope.setLevel(SENTRY_LEVEL.ERROR);
        });
      };
      SentryManager.sendMessage = function(titleMsg, sentryLevel, tagName) {
        var _a;
        false;
        var obj = {
          level: sentryLevel,
          tags: (_a = {}, _a[SENTRY_TAG_NAME.COCOS_EVENT] = tagName, _a)
        };
        Sentry.captureMessage(titleMsg, obj);
      };
      SentryManager.sendErrorCode = function(error, errorCode, stack) {
        false;
        Sentry.withScope(function(scope) {
          var _a;
          scope.setLevel(SENTRY_LEVEL.ERROR);
          scope.setTag([ SENTRY_TAG_NAME.ERROR_CODE ], errorCode);
          scope.setContext(SENTRY_CONTEXT_TITLE.CALL_STACK, (_a = {}, _a[SENTRY_CONTEXT_SUB_TITLE.STACK] = stack, 
          _a));
          Sentry.captureException(error);
        });
      };
      SentryManager.setTag = function(tagName, value) {
        false;
        Sentry.configureScope(function(scope) {
          scope.setTag(tagName, value);
        });
      };
      return SentryManager;
    }();
    exports.SentryManager = SentryManager;
    cc._RF.pop();
  }, {} ],
  UrlManager: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "78729S9hP1DFJqW6r2qKQ/e", "UrlManager");
    "use strict";
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    exports.UrlManager = exports.DecodeParamsKeys = exports.UrlParamsKeys = void 0;
    var UrlParamsKeys;
    (function(UrlParamsKeys) {
      UrlParamsKeys["TOKEN"] = "token";
      UrlParamsKeys["TEST_MODE"] = "testMode";
      UrlParamsKeys["GAME_ID"] = "gameId";
      UrlParamsKeys["LOCAL_BUILD"] = "localBuild";
      UrlParamsKeys["AUDIO"] = "audio";
      UrlParamsKeys["GAME_RECORD_ID"] = "gameRecordId";
      UrlParamsKeys["LANG"] = "lang";
      UrlParamsKeys["FAST_MODE"] = "fastMode";
      UrlParamsKeys["CROSS_GAME_DATA"] = "crossGameData";
      UrlParamsKeys["SKIN"] = "lobbyId";
      UrlParamsKeys["CURRENCY"] = "currency";
      UrlParamsKeys["MATERIALID"] = "materialId";
    })(UrlParamsKeys = exports.UrlParamsKeys || (exports.UrlParamsKeys = {}));
    var DecodeParamsKeys;
    (function(DecodeParamsKeys) {
      DecodeParamsKeys["TOKEN"] = "token";
      DecodeParamsKeys["DOMAIN"] = "domain";
      DecodeParamsKeys["LANG"] = "lang";
      DecodeParamsKeys["CURRENCY"] = "currency";
      DecodeParamsKeys["REQ_GAME_ID"] = "reqGameId";
      DecodeParamsKeys["TARGET_GAME_ID"] = "targetGameId";
      DecodeParamsKeys["CATEGORY"] = "category";
      DecodeParamsKeys["SKIN"] = "lobbyId";
      DecodeParamsKeys["MATERIALID"] = "materialId";
    })(DecodeParamsKeys = exports.DecodeParamsKeys || (exports.DecodeParamsKeys = {}));
    var UrlManager = function() {
      function UrlManager() {}
      UrlManager.init = function(url) {
        if (this.url == url) return;
        var urlObject = new URL(url);
        var urlParams = new URLSearchParams(urlObject.search.replace("?", "&"));
        var decodeData = atob(decodeURIComponent(urlParams.get(UrlParamsKeys.TOKEN)));
        var decodeParams = new URLSearchParams(decodeData);
        this.url = url;
        this.urlParams = urlParams;
        this.decodeParams = decodeParams;
      };
      UrlManager.getDecodeParams = function(key) {
        this.init(window.location.href);
        return this.decodeParams.get(key);
      };
      UrlManager.getUrlParams = function(key) {
        this.init(window.location.href);
        return this.urlParams.get(key);
      };
      UrlManager.setUrlParams = function(key, value) {
        var url = new URL(window.location.href);
        url.searchParams.set(key, value);
        history.replaceState(null, null, url.toString());
      };
      UrlManager.delUrlParams = function(key) {
        var url = new URL(window.location.href);
        url.searchParams.delete(key);
        history.replaceState(null, null, url.toString());
      };
      UrlManager.getGameId = function() {
        this.init(window.location.href);
        var testGameId = this.urlParams.get(UrlParamsKeys.GAME_ID);
        if (testGameId) return this.convertGameName(testGameId);
        var decodeId = this.decodeParams.get(DecodeParamsKeys.TARGET_GAME_ID);
        var decodeGameId = this.convertGameName(decodeId);
        return decodeGameId;
      };
      UrlManager.getLang = function() {
        this.init(window.location.href);
        var testLanguage = this.urlParams.get(UrlParamsKeys.LANG);
        if (testLanguage) return testLanguage;
        var decodeLanguage = this.decodeParams.get(DecodeParamsKeys.LANG);
        return decodeLanguage || "zh-cn";
      };
      UrlManager.getSkin = function() {
        this.init(window.location.href);
        var tempSkin = this.urlParams.get(UrlParamsKeys.SKIN);
        if (tempSkin) return tempSkin;
        var decodeSkin = this.decodeParams.get(DecodeParamsKeys.SKIN);
        return decodeSkin || "10001";
      };
      UrlManager.getCurrency = function() {
        this.init(window.location.href);
        var tempCurrency = this.urlParams.get(UrlParamsKeys.CURRENCY);
        if (tempCurrency) return tempCurrency;
        var decodeCurrency = this.decodeParams.get(DecodeParamsKeys.CURRENCY);
        return decodeCurrency || "CNY";
      };
      UrlManager.getMaterialId = function() {
        this.init(window.location.href);
        var tempMaterialId = this.urlParams.get(UrlParamsKeys.MATERIALID);
        if (tempMaterialId) return tempMaterialId;
        var decodeMaterialId = this.decodeParams.get(DecodeParamsKeys.MATERIALID);
        return decodeMaterialId || "1";
      };
      UrlManager.convertGameName = function(gameId) {
        var id = Number(gameId);
        if (Number.isNaN(id)) throw new Error("game id is not legal");
        return id > 1e4 ? "L" + id : "G" + id;
      };
      UrlManager.url = null;
      UrlManager.urlParams = null;
      UrlManager.decodeParams = null;
      return UrlManager;
    }();
    exports.UrlManager = UrlManager;
    cc._RF.pop();
  }, {} ]
}, {}, [ "KKGame", "KKGameOrientationCanvas", "KKGameOrientationTool", "KKGameOrientationToolManager", "KKGameOrientationToolModule", "BundleManager", "SceneManager", "SentryManager", "UrlManager" ]);