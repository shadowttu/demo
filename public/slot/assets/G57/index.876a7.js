window.__require = function e(t, n, r) {
  function s(o, u) {
    if (!n[o]) {
      if (!t[o]) {
        var b = o.split("/");
        b = b[b.length - 1];
        if (!t[b]) {
          var a = "function" == typeof __require && __require;
          if (!u && a) return a(b, !0);
          if (i) return i(b, !0);
          throw new Error("Cannot find module '" + o + "'");
        }
        o = b;
      }
      var f = n[o] = {
        exports: {}
      };
      t[o][0].call(f.exports, function(e) {
        var n = t[o][1][e];
        return s(n || e);
      }, f, f.exports, e, t, n, r);
    }
    return n[o].exports;
  }
  var i = "function" == typeof __require && __require;
  for (var o = 0; o < r.length; o++) s(r[o]);
  return s;
}({
  G57_AutoGamePanel: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "7ccc5Bagp1NQYlLYLjrkFFz", "G57_AutoGamePanel");
    "use strict";
    var __extends = this && this.__extends || function() {
      var extendStatics = function(d, b) {
        extendStatics = Object.setPrototypeOf || {
          __proto__: []
        } instanceof Array && function(d, b) {
          d.__proto__ = b;
        } || function(d, b) {
          for (var p in b) Object.prototype.hasOwnProperty.call(b, p) && (d[p] = b[p]);
        };
        return extendStatics(d, b);
      };
      return function(d, b) {
        extendStatics(d, b);
        function __() {
          this.constructor = d;
        }
        d.prototype = null === b ? Object.create(b) : (__.prototype = b.prototype, new __());
      };
    }();
    var __decorate = this && this.__decorate || function(decorators, target, key, desc) {
      var c = arguments.length, r = c < 3 ? target : null === desc ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
      if ("object" === typeof Reflect && "function" === typeof Reflect.decorate) r = Reflect.decorate(decorators, target, key, desc); else for (var i = decorators.length - 1; i >= 0; i--) (d = decorators[i]) && (r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r);
      return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    var Slot_AutoGamePanel_1 = require("../../../../SlotFramework/Game/Panel/Slot_AutoGamePanel");
    var G57_DataManager_1 = require("../../../Common/script/G57_DataManager");
    var ccclass = cc._decorator.ccclass;
    var G57_AutoGamePanel = function(_super) {
      __extends(G57_AutoGamePanel, _super);
      function G57_AutoGamePanel() {
        var _this = null !== _super && _super.apply(this, arguments) || this;
        _this.data = null;
        return _this;
      }
      G57_AutoGamePanel.prototype.init = function() {
        this.data = G57_DataManager_1.default.getInstance();
        _super.prototype.init.call(this);
      };
      G57_AutoGamePanel = __decorate([ ccclass ], G57_AutoGamePanel);
      return G57_AutoGamePanel;
    }(Slot_AutoGamePanel_1.default);
    exports.default = G57_AutoGamePanel;
    cc._RF.pop();
  }, {
    "../../../../SlotFramework/Game/Panel/Slot_AutoGamePanel": void 0,
    "../../../Common/script/G57_DataManager": "G57_DataManager"
  } ],
  G57_CollectFXPool: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "f8fb3iH3oZMIporZvnKxw2L", "G57_CollectFXPool");
    "use strict";
    var __extends = this && this.__extends || function() {
      var extendStatics = function(d, b) {
        extendStatics = Object.setPrototypeOf || {
          __proto__: []
        } instanceof Array && function(d, b) {
          d.__proto__ = b;
        } || function(d, b) {
          for (var p in b) Object.prototype.hasOwnProperty.call(b, p) && (d[p] = b[p]);
        };
        return extendStatics(d, b);
      };
      return function(d, b) {
        extendStatics(d, b);
        function __() {
          this.constructor = d;
        }
        d.prototype = null === b ? Object.create(b) : (__.prototype = b.prototype, new __());
      };
    }();
    var __decorate = this && this.__decorate || function(decorators, target, key, desc) {
      var c = arguments.length, r = c < 3 ? target : null === desc ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
      if ("object" === typeof Reflect && "function" === typeof Reflect.decorate) r = Reflect.decorate(decorators, target, key, desc); else for (var i = decorators.length - 1; i >= 0; i--) (d = decorators[i]) && (r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r);
      return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __awaiter = this && this.__awaiter || function(thisArg, _arguments, P, generator) {
      function adopt(value) {
        return value instanceof P ? value : new P(function(resolve) {
          resolve(value);
        });
      }
      return new (P || (P = Promise))(function(resolve, reject) {
        function fulfilled(value) {
          try {
            step(generator.next(value));
          } catch (e) {
            reject(e);
          }
        }
        function rejected(value) {
          try {
            step(generator["throw"](value));
          } catch (e) {
            reject(e);
          }
        }
        function step(result) {
          result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected);
        }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
      });
    };
    var __generator = this && this.__generator || function(thisArg, body) {
      var _ = {
        label: 0,
        sent: function() {
          if (1 & t[0]) throw t[1];
          return t[1];
        },
        trys: [],
        ops: []
      }, f, y, t, g;
      return g = {
        next: verb(0),
        throw: verb(1),
        return: verb(2)
      }, "function" === typeof Symbol && (g[Symbol.iterator] = function() {
        return this;
      }), g;
      function verb(n) {
        return function(v) {
          return step([ n, v ]);
        };
      }
      function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
          if (f = 1, y && (t = 2 & op[0] ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 
          0) : y.next) && !(t = t.call(y, op[1])).done) return t;
          (y = 0, t) && (op = [ 2 & op[0], t.value ]);
          switch (op[0]) {
           case 0:
           case 1:
            t = op;
            break;

           case 4:
            _.label++;
            return {
              value: op[1],
              done: false
            };

           case 5:
            _.label++;
            y = op[1];
            op = [ 0 ];
            continue;

           case 7:
            op = _.ops.pop();
            _.trys.pop();
            continue;

           default:
            if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (6 === op[0] || 2 === op[0])) {
              _ = 0;
              continue;
            }
            if (3 === op[0] && (!t || op[1] > t[0] && op[1] < t[3])) {
              _.label = op[1];
              break;
            }
            if (6 === op[0] && _.label < t[1]) {
              _.label = t[1];
              t = op;
              break;
            }
            if (t && _.label < t[2]) {
              _.label = t[2];
              _.ops.push(op);
              break;
            }
            t[2] && _.ops.pop();
            _.trys.pop();
            continue;
          }
          op = body.call(thisArg, _);
        } catch (e) {
          op = [ 6, e ];
          y = 0;
        } finally {
          f = t = 0;
        }
        if (5 & op[0]) throw op[1];
        return {
          value: op[0] ? op[1] : void 0,
          done: true
        };
      }
    };
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    var FXController_1 = require("../../../Common/Optional/Crosis/FXController/FXController");
    var CommonTool_1 = require("../../../Common/Tools/CommonTool");
    var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
    var G57_CollectFXPool = function(_super) {
      __extends(G57_CollectFXPool, _super);
      function G57_CollectFXPool() {
        var _this = null !== _super && _super.apply(this, arguments) || this;
        _this.prefab = null;
        _this.collectFXPoolSize = 0;
        _this.collectFXPool = null;
        return _this;
      }
      G57_CollectFXPool.prototype.initCollectFXPool = function() {
        return __awaiter(this, void 0, Promise, function() {
          var i, collectFX;
          return __generator(this, function(_a) {
            switch (_a.label) {
             case 0:
              this.collectFXPool = new cc.NodePool();
              i = 0;
              _a.label = 1;

             case 1:
              if (!(i < this.collectFXPoolSize)) return [ 3, 4 ];
              return [ 4, CommonTool_1.CommonTool.toNextTick() ];

             case 2:
              _a.sent();
              collectFX = cc.instantiate(this.prefab);
              this.collectFXPool.put(collectFX);
              _a.label = 3;

             case 3:
              ++i;
              return [ 3, 1 ];

             case 4:
              this.prefab.active = false;
              return [ 2 ];
            }
          });
        });
      };
      G57_CollectFXPool.prototype.getCollectFX = function() {
        var collectFX;
        if (this.collectFXPool.size() <= 0) {
          collectFX = cc.instantiate(this.prefab).getComponent(FXController_1.default);
          this.collectFXPool.put(collectFX.node);
        }
        collectFX = this.collectFXPool.get().getComponent(FXController_1.default);
        return collectFX;
      };
      G57_CollectFXPool.prototype.recycleCollectFX = function(collectFX) {
        collectFX.stop();
        this.collectFXPool.put(collectFX.node);
      };
      __decorate([ property(cc.Node) ], G57_CollectFXPool.prototype, "prefab", void 0);
      __decorate([ property({
        type: cc.Integer
      }) ], G57_CollectFXPool.prototype, "collectFXPoolSize", void 0);
      G57_CollectFXPool = __decorate([ ccclass ], G57_CollectFXPool);
      return G57_CollectFXPool;
    }(cc.Component);
    exports.default = G57_CollectFXPool;
    cc._RF.pop();
  }, {
    "../../../Common/Optional/Crosis/FXController/FXController": void 0,
    "../../../Common/Tools/CommonTool": void 0
  } ],
  G57_CommonData: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "1f9ffFcljNGhLxA2LT8DGzp", "G57_CommonData");
    "use strict";
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    exports.FastChoseRoomData = exports.ScatterResult = exports.WinWay = exports.ResultData = exports.BaseGameSpinResponeData = exports.RoomData = exports.GetRoomListData = exports.ReelTable = exports.GetInitialReelInfoData = exports.LoginData = void 0;
    var LoginData = function() {
      function LoginData() {
        this.nickname = "";
        this.language = "";
        this.balance = 0;
        this.timestamp = 0;
        this.themeType = 0;
        this.icon = 0;
        this.playerId = "";
        this.gameId = 0;
      }
      return LoginData;
    }();
    exports.LoginData = LoginData;
    var GetInitialReelInfoData = function() {
      function GetInitialReelInfoData() {
        this.reels = null;
        this.position = [];
      }
      return GetInitialReelInfoData;
    }();
    exports.GetInitialReelInfoData = GetInitialReelInfoData;
    var ReelTable = function() {
      function ReelTable() {
        this.BG = [];
        this.FG = [];
      }
      return ReelTable;
    }();
    exports.ReelTable = ReelTable;
    var GetRoomListData = function() {
      function GetRoomListData() {
        this.rooms = [];
      }
      return GetRoomListData;
    }();
    exports.GetRoomListData = GetRoomListData;
    var RoomData = function() {
      function RoomData() {
        this.roomId = "";
        this.status = 0;
      }
      return RoomData;
    }();
    exports.RoomData = RoomData;
    var BaseGameSpinResponeData = function() {
      function BaseGameSpinResponeData() {
        this.currentCash = 0;
        this.totalBonus = 0;
        this.totalBet = 0;
        this.resultList = [];
        this.Jackpot = null;
      }
      return BaseGameSpinResponeData;
    }();
    exports.BaseGameSpinResponeData = BaseGameSpinResponeData;
    var ResultData = function() {
      function ResultData() {
        this.randomNumList = [];
        this.reels = [];
        this.totalWinBonus = 0;
        this.totalHitPos = [ [] ];
        this.scatter = null;
        this.winWayList = [];
        this.specialMultiple = 0;
        this.isGetFreeSpins = false;
      }
      return ResultData;
    }();
    exports.ResultData = ResultData;
    var WinWay = function() {
      function WinWay() {
        this.x = 0;
        this.winNum = 0;
        this.lineNum = 0;
        this.winBonus = 0;
        this.singlePos = [ [] ];
        this.specialMultiple = 0;
      }
      return WinWay;
    }();
    exports.WinWay = WinWay;
    var ScatterResult = function() {
      function ScatterResult() {
        this.winNum = 0;
        this.count = 0;
        this.winBonus = 0;
        this.scatterPos = [];
        this.getSpins = 0;
      }
      return ScatterResult;
    }();
    exports.ScatterResult = ScatterResult;
    var FastChoseRoomData = function() {
      function FastChoseRoomData() {}
      return FastChoseRoomData;
    }();
    exports.FastChoseRoomData = FastChoseRoomData;
    cc._RF.pop();
  }, {} ],
  G57_DataManager: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "f41a0fSKMhGDoi7KQOhNWzo", "G57_DataManager");
    "use strict";
    var __extends = this && this.__extends || function() {
      var extendStatics = function(d, b) {
        extendStatics = Object.setPrototypeOf || {
          __proto__: []
        } instanceof Array && function(d, b) {
          d.__proto__ = b;
        } || function(d, b) {
          for (var p in b) Object.prototype.hasOwnProperty.call(b, p) && (d[p] = b[p]);
        };
        return extendStatics(d, b);
      };
      return function(d, b) {
        extendStatics(d, b);
        function __() {
          this.constructor = d;
        }
        d.prototype = null === b ? Object.create(b) : (__.prototype = b.prototype, new __());
      };
    }();
    var __decorate = this && this.__decorate || function(decorators, target, key, desc) {
      var c = arguments.length, r = c < 3 ? target : null === desc ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
      if ("object" === typeof Reflect && "function" === typeof Reflect.decorate) r = Reflect.decorate(decorators, target, key, desc); else for (var i = decorators.length - 1; i >= 0; i--) (d = decorators[i]) && (r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r);
      return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    var Slot_DataManager_1 = require("../../../SlotFramework/Slot_DataManager");
    var ccclass = cc._decorator.ccclass;
    var G57_DataManager = function(_super) {
      __extends(G57_DataManager, _super);
      function G57_DataManager() {
        var _this = null !== _super && _super.apply(this, arguments) || this;
        _this.gameVersion = "Dev 1.0.9";
        _this.SDKVersion = "20200812";
        _this.isShowDrawcall = false;
        _this.resultList = [];
        _this.reelTable_ALLFG = [];
        return _this;
      }
      G57_DataManager.prototype.init = function() {
        _super.prototype.init.call(this);
        this.gameID = 57;
        this.path = "Resources";
        this.skin = "Normal";
        this.setMagnificationTable();
      };
      G57_DataManager.prototype.setMagnificationTable = function() {
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H1] = {};
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H1][3] = 95;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H1][4] = 250;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H1][5] = 500;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H2] = {};
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H2][3] = 60;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H2][4] = 180;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H2][5] = 350;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H3] = {};
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H3][3] = 50;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H3][4] = 120;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H3][5] = 300;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H4] = {};
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H4][3] = 35;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H4][4] = 120;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H4][5] = 240;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.N1] = {};
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.N1][3] = 20;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.N1][4] = 35;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.N1][5] = 150;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.N2] = {};
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.N2][3] = 20;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.N2][4] = 30;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.N2][5] = 130;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.N3] = {};
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.N3][3] = 15;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.N3][4] = 25;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.N3][5] = 120;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.N4] = {};
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.N4][3] = 15;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.N4][4] = 25;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.N4][5] = 120;
      };
      G57_DataManager = __decorate([ ccclass ], G57_DataManager);
      return G57_DataManager;
    }(Slot_DataManager_1.default);
    exports.default = G57_DataManager;
    cc._RF.pop();
  }, {
    "../../../SlotFramework/Slot_DataManager": void 0
  } ],
  G57_DynamicPopUpPanelManager: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "55d22eGwg1NmYls8nSY8TAp", "G57_DynamicPopUpPanelManager");
    "use strict";
    var __extends = this && this.__extends || function() {
      var extendStatics = function(d, b) {
        extendStatics = Object.setPrototypeOf || {
          __proto__: []
        } instanceof Array && function(d, b) {
          d.__proto__ = b;
        } || function(d, b) {
          for (var p in b) Object.prototype.hasOwnProperty.call(b, p) && (d[p] = b[p]);
        };
        return extendStatics(d, b);
      };
      return function(d, b) {
        extendStatics(d, b);
        function __() {
          this.constructor = d;
        }
        d.prototype = null === b ? Object.create(b) : (__.prototype = b.prototype, new __());
      };
    }();
    var __values = this && this.__values || function(o) {
      var s = "function" === typeof Symbol && Symbol.iterator, m = s && o[s], i = 0;
      if (m) return m.call(o);
      if (o && "number" === typeof o.length) return {
        next: function() {
          o && i >= o.length && (o = void 0);
          return {
            value: o && o[i++],
            done: !o
          };
        }
      };
      throw new TypeError(s ? "Object is not iterable." : "Symbol.iterator is not defined.");
    };
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    var Slot_DynamicPopUpManager_1 = require("../../../SlotFramework/Common/DynamicPopUp/Slot_DynamicPopUpManager");
    var G57_DynamicPopUpPanelManager = function(_super) {
      __extends(G57_DynamicPopUpPanelManager, _super);
      function G57_DynamicPopUpPanelManager() {
        return null !== _super && _super.apply(this, arguments) || this;
      }
      G57_DynamicPopUpPanelManager.prototype.prestore = function(prefabs) {
        var e_1, _a;
        try {
          for (var prefabs_1 = __values(prefabs), prefabs_1_1 = prefabs_1.next(); !prefabs_1_1.done; prefabs_1_1 = prefabs_1.next()) {
            var prefab = prefabs_1_1.value;
            prefab.name;
            _super.prototype.prestore.call(this, [ prefab ]);
          }
        } catch (e_1_1) {
          e_1 = {
            error: e_1_1
          };
        } finally {
          try {
            prefabs_1_1 && !prefabs_1_1.done && (_a = prefabs_1.return) && _a.call(prefabs_1);
          } finally {
            if (e_1) throw e_1.error;
          }
        }
      };
      return G57_DynamicPopUpPanelManager;
    }(Slot_DynamicPopUpManager_1.default);
    exports.default = G57_DynamicPopUpPanelManager;
    cc._RF.pop();
  }, {
    "../../../SlotFramework/Common/DynamicPopUp/Slot_DynamicPopUpManager": void 0
  } ],
  G57_FreeGameGetScorePanel: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "a2a18BEhN5O/4Nhor+e989b", "G57_FreeGameGetScorePanel");
    "use strict";
    var __extends = this && this.__extends || function() {
      var extendStatics = function(d, b) {
        extendStatics = Object.setPrototypeOf || {
          __proto__: []
        } instanceof Array && function(d, b) {
          d.__proto__ = b;
        } || function(d, b) {
          for (var p in b) Object.prototype.hasOwnProperty.call(b, p) && (d[p] = b[p]);
        };
        return extendStatics(d, b);
      };
      return function(d, b) {
        extendStatics(d, b);
        function __() {
          this.constructor = d;
        }
        d.prototype = null === b ? Object.create(b) : (__.prototype = b.prototype, new __());
      };
    }();
    var __decorate = this && this.__decorate || function(decorators, target, key, desc) {
      var c = arguments.length, r = c < 3 ? target : null === desc ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
      if ("object" === typeof Reflect && "function" === typeof Reflect.decorate) r = Reflect.decorate(decorators, target, key, desc); else for (var i = decorators.length - 1; i >= 0; i--) (d = decorators[i]) && (r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r);
      return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    var FXController_1 = require("../../../../Common/Optional/Crosis/FXController/FXController");
    var AudioManager_1 = require("../../../../Common/Tools/AudioManager/AudioManager");
    var CommonTool_1 = require("../../../../Common/Tools/CommonTool");
    var OrientationToolManager_1 = require("../../../../Common/Tools/OrientationTool/OrientationToolManager");
    var Slot_FreeGameGetScorePanel_1 = require("../../../../SlotFramework/Game/Panel/Slot_FreeGameGetScorePanel");
    var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
    var G57_FreeGameGetScorePanel = function(_super) {
      __extends(G57_FreeGameGetScorePanel, _super);
      function G57_FreeGameGetScorePanel() {
        var _this = null !== _super && _super.apply(this, arguments) || this;
        _this.FXEnd = null;
        _this.winSpine = null;
        return _this;
      }
      G57_FreeGameGetScorePanel.prototype.init = function() {
        _super.prototype.init.call(this);
      };
      G57_FreeGameGetScorePanel.prototype.open = function(p_freeGameGetScore, p_betScore) {
        _super.prototype.open.call(this, p_freeGameGetScore, p_betScore);
        this.showWinEffect();
        this.freeGameGetScore.getComponentInChildren(cc.Label).string = CommonTool_1.CommonTool.getNumText(p_freeGameGetScore, 2, true, false);
        this.FXEnd.play();
      };
      G57_FreeGameGetScorePanel.prototype.close = function() {
        AudioManager_1.AudioManager.instance.playAudioEvent("CloseFreeGameGetScorePanel");
        return _super.prototype.close.call(this);
      };
      G57_FreeGameGetScorePanel.prototype.showWinEffect = function() {
        var _this = this;
        console.log("\u3010Show Win\u3011 ");
        var spineName = this.isHorizontal() ? "H" : "V";
        this.winSpine.setAnimation(0, "End_start_" + spineName, false);
        this.winSpine.setCompleteListener(function() {
          var spineName = _this.isHorizontal() ? "H" : "V";
          _this.winSpine.setAnimation(0, "End_loop_" + spineName, true);
        });
        AudioManager_1.AudioManager.instance.playAudioEvent("FreeGameGetScoreShowWinEffect");
      };
      G57_FreeGameGetScorePanel.prototype.isHorizontal = function() {
        return OrientationToolManager_1.default.orientationState === OrientationToolManager_1.ORIENTATION_TYPE.HORIZONTAL;
      };
      __decorate([ property(FXController_1.default) ], G57_FreeGameGetScorePanel.prototype, "FXEnd", void 0);
      __decorate([ property(sp.Skeleton) ], G57_FreeGameGetScorePanel.prototype, "winSpine", void 0);
      G57_FreeGameGetScorePanel = __decorate([ ccclass ], G57_FreeGameGetScorePanel);
      return G57_FreeGameGetScorePanel;
    }(Slot_FreeGameGetScorePanel_1.default);
    exports.default = G57_FreeGameGetScorePanel;
    cc._RF.pop();
  }, {
    "../../../../Common/Optional/Crosis/FXController/FXController": void 0,
    "../../../../Common/Tools/AudioManager/AudioManager": void 0,
    "../../../../Common/Tools/CommonTool": void 0,
    "../../../../Common/Tools/OrientationTool/OrientationToolManager": void 0,
    "../../../../SlotFramework/Game/Panel/Slot_FreeGameGetScorePanel": void 0
  } ],
  G57_FreeGamePanel: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "c0da42bchRGyJOAGy9L1fIF", "G57_FreeGamePanel");
    "use strict";
    var __extends = this && this.__extends || function() {
      var extendStatics = function(d, b) {
        extendStatics = Object.setPrototypeOf || {
          __proto__: []
        } instanceof Array && function(d, b) {
          d.__proto__ = b;
        } || function(d, b) {
          for (var p in b) Object.prototype.hasOwnProperty.call(b, p) && (d[p] = b[p]);
        };
        return extendStatics(d, b);
      };
      return function(d, b) {
        extendStatics(d, b);
        function __() {
          this.constructor = d;
        }
        d.prototype = null === b ? Object.create(b) : (__.prototype = b.prototype, new __());
      };
    }();
    var __decorate = this && this.__decorate || function(decorators, target, key, desc) {
      var c = arguments.length, r = c < 3 ? target : null === desc ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
      if ("object" === typeof Reflect && "function" === typeof Reflect.decorate) r = Reflect.decorate(decorators, target, key, desc); else for (var i = decorators.length - 1; i >= 0; i--) (d = decorators[i]) && (r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r);
      return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    var FXController_1 = require("../../../../Common/Optional/Crosis/FXController/FXController");
    var AudioManager_1 = require("../../../../Common/Tools/AudioManager/AudioManager");
    var CommonTool_1 = require("../../../../Common/Tools/CommonTool");
    var OrientationToolManager_1 = require("../../../../Common/Tools/OrientationTool/OrientationToolManager");
    var Slot_FreeGamePanel_1 = require("../../../../SlotFramework/Game/Panel/Slot_FreeGamePanel");
    var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
    var MILLESIMAL = .001;
    var TOTALTIME = 15e3;
    var G57_FreeGamePanel = function(_super) {
      __extends(G57_FreeGamePanel, _super);
      function G57_FreeGamePanel() {
        var _this = null !== _super && _super.apply(this, arguments) || this;
        _this.fxFGIn = null;
        _this.fxSelect = null;
        _this.buttonPos = [];
        _this.count = null;
        _this.countSpr = null;
        _this.countNode = null;
        _this.bgSpine = null;
        _this.remainTime = 0;
        _this.countdownTime = 0;
        _this.endTime = 0;
        _this.onCountdown = false;
        return _this;
      }
      G57_FreeGamePanel.prototype.init = function() {
        _super.prototype.init.call(this);
        this.buttonPos.forEach(function(item) {
          item.getComponent(cc.Button).interactable = false;
        });
        this.countNode.opacity = 0;
      };
      G57_FreeGamePanel.prototype.open = function(endTime) {
        var _this = this;
        _super.prototype.open.call(this, endTime);
        this.fxFGIn.play();
        this.setCountdownTime(endTime);
        var spineName = this.isHorizontal() ? "H" : "V";
        this.bgSpine.setAnimation(0, "trans_start_" + spineName, false);
        this.bgSpine.setCompleteListener(function() {
          spineName = _this.isHorizontal() ? "H" : "V";
          _this.bgSpine.setAnimation(0, "trans_loop_" + spineName, true);
        });
        this.scheduleOnce(function() {
          _this.buttonPos.forEach(function(item) {
            item.getComponent(cc.Button).interactable = true;
          });
          _this.countNode.opacity = 255;
        }, 2.8);
        AudioManager_1.AudioManager.instance.playAudioEvent("OpenFreeGamePanel");
      };
      G57_FreeGamePanel.prototype.close = function() {
        console.log("\u95dc\u9589\u514d\u8cbb\u904a\u6232\u9762\u677f");
        AudioManager_1.AudioManager.instance.playAudioEvent("CloseFreeGamePanel");
        return _super.prototype.close.call(this);
      };
      G57_FreeGamePanel.prototype.setFXSelect = function(mode) {
        this.buttonPos.forEach(function(item) {
          item.getComponent(cc.Button).interactable = false;
        });
        this.countdownTime = 0;
        this.countNode.opacity = 0;
        this.fxSelect.node.setPosition(this.buttonPos[mode].position);
        this.fxSelect.play();
      };
      G57_FreeGamePanel.prototype.update = function() {
        this.refreshCountdownTime();
      };
      G57_FreeGamePanel.prototype.closeAnimation = function() {
        var _this = this;
        return new Promise(function(resolve) {
          cc.tween(_this.node).to(.5, {
            opacity: 0
          }, {
            easing: "cubicInOut"
          }).call(function() {
            _this.node.active = false;
            _this.fxFGIn.stop();
            _this.fxSelect.stop();
            resolve();
          }).start();
        });
      };
      G57_FreeGamePanel.prototype.isHorizontal = function() {
        return OrientationToolManager_1.default.orientationState === OrientationToolManager_1.ORIENTATION_TYPE.HORIZONTAL;
      };
      G57_FreeGamePanel.prototype.buttonSelect = function(touch, mode) {
        this.setFXSelect(mode);
      };
      G57_FreeGamePanel.prototype.setCountdownTime = function(choosingTime) {
        this.onCountdown = true;
        this.endTime = CommonTool_1.CommonTool.getServerTime() + choosingTime;
        this.countdownTime = -1;
      };
      G57_FreeGamePanel.prototype.refreshCountdownTime = function() {
        if (0 === this.countdownTime) {
          if (this.onCountdown) {
            this.onCountdown = false;
            this.count.string = "";
          }
          return;
        }
        this.remainTime = Math.max(0, this.endTime - CommonTool_1.CommonTool.getServerTime()) * MILLESIMAL;
        this.countSpr.fillRange = Math.max(0, Math.min(1, (this.endTime - CommonTool_1.CommonTool.getServerTime()) / TOTALTIME));
        var remainTimeInt = Math.ceil(this.remainTime);
        if (this.countdownTime !== remainTimeInt) {
          this.countdownTime = remainTimeInt;
          this.count.string = this.countdownTime.toString();
          this.countdownTime <= 5 && this.countdownTime > 0 && AudioManager_1.AudioManager.instance.playAudioEvent("FreeGamePageCountdown");
        }
      };
      __decorate([ property(FXController_1.default) ], G57_FreeGamePanel.prototype, "fxFGIn", void 0);
      __decorate([ property(FXController_1.default) ], G57_FreeGamePanel.prototype, "fxSelect", void 0);
      __decorate([ property([ cc.Node ]) ], G57_FreeGamePanel.prototype, "buttonPos", void 0);
      __decorate([ property(cc.Label) ], G57_FreeGamePanel.prototype, "count", void 0);
      __decorate([ property(cc.Sprite) ], G57_FreeGamePanel.prototype, "countSpr", void 0);
      __decorate([ property(cc.Node) ], G57_FreeGamePanel.prototype, "countNode", void 0);
      __decorate([ property(sp.Skeleton) ], G57_FreeGamePanel.prototype, "bgSpine", void 0);
      G57_FreeGamePanel = __decorate([ ccclass ], G57_FreeGamePanel);
      return G57_FreeGamePanel;
    }(Slot_FreeGamePanel_1.default);
    exports.default = G57_FreeGamePanel;
    cc._RF.pop();
  }, {
    "../../../../Common/Optional/Crosis/FXController/FXController": void 0,
    "../../../../Common/Tools/AudioManager/AudioManager": void 0,
    "../../../../Common/Tools/CommonTool": void 0,
    "../../../../Common/Tools/OrientationTool/OrientationToolManager": void 0,
    "../../../../SlotFramework/Game/Panel/Slot_FreeGamePanel": void 0
  } ],
  G57_GameUI: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "3c6d8fxtadO4aSMiHNxD+HW", "G57_GameUI");
    "use strict";
    var __extends = this && this.__extends || function() {
      var extendStatics = function(d, b) {
        extendStatics = Object.setPrototypeOf || {
          __proto__: []
        } instanceof Array && function(d, b) {
          d.__proto__ = b;
        } || function(d, b) {
          for (var p in b) Object.prototype.hasOwnProperty.call(b, p) && (d[p] = b[p]);
        };
        return extendStatics(d, b);
      };
      return function(d, b) {
        extendStatics(d, b);
        function __() {
          this.constructor = d;
        }
        d.prototype = null === b ? Object.create(b) : (__.prototype = b.prototype, new __());
      };
    }();
    var __decorate = this && this.__decorate || function(decorators, target, key, desc) {
      var c = arguments.length, r = c < 3 ? target : null === desc ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
      if ("object" === typeof Reflect && "function" === typeof Reflect.decorate) r = Reflect.decorate(decorators, target, key, desc); else for (var i = decorators.length - 1; i >= 0; i--) (d = decorators[i]) && (r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r);
      return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __awaiter = this && this.__awaiter || function(thisArg, _arguments, P, generator) {
      function adopt(value) {
        return value instanceof P ? value : new P(function(resolve) {
          resolve(value);
        });
      }
      return new (P || (P = Promise))(function(resolve, reject) {
        function fulfilled(value) {
          try {
            step(generator.next(value));
          } catch (e) {
            reject(e);
          }
        }
        function rejected(value) {
          try {
            step(generator["throw"](value));
          } catch (e) {
            reject(e);
          }
        }
        function step(result) {
          result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected);
        }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
      });
    };
    var __generator = this && this.__generator || function(thisArg, body) {
      var _ = {
        label: 0,
        sent: function() {
          if (1 & t[0]) throw t[1];
          return t[1];
        },
        trys: [],
        ops: []
      }, f, y, t, g;
      return g = {
        next: verb(0),
        throw: verb(1),
        return: verb(2)
      }, "function" === typeof Symbol && (g[Symbol.iterator] = function() {
        return this;
      }), g;
      function verb(n) {
        return function(v) {
          return step([ n, v ]);
        };
      }
      function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
          if (f = 1, y && (t = 2 & op[0] ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 
          0) : y.next) && !(t = t.call(y, op[1])).done) return t;
          (y = 0, t) && (op = [ 2 & op[0], t.value ]);
          switch (op[0]) {
           case 0:
           case 1:
            t = op;
            break;

           case 4:
            _.label++;
            return {
              value: op[1],
              done: false
            };

           case 5:
            _.label++;
            y = op[1];
            op = [ 0 ];
            continue;

           case 7:
            op = _.ops.pop();
            _.trys.pop();
            continue;

           default:
            if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (6 === op[0] || 2 === op[0])) {
              _ = 0;
              continue;
            }
            if (3 === op[0] && (!t || op[1] > t[0] && op[1] < t[3])) {
              _.label = op[1];
              break;
            }
            if (6 === op[0] && _.label < t[1]) {
              _.label = t[1];
              t = op;
              break;
            }
            if (t && _.label < t[2]) {
              _.label = t[2];
              _.ops.push(op);
              break;
            }
            t[2] && _.ops.pop();
            _.trys.pop();
            continue;
          }
          op = body.call(thisArg, _);
        } catch (e) {
          op = [ 6, e ];
          y = 0;
        } finally {
          f = t = 0;
        }
        if (5 & op[0]) throw op[1];
        return {
          value: op[0] ? op[1] : void 0,
          done: true
        };
      }
    };
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    var FXController_1 = require("../../../../Common/Optional/Crosis/FXController/FXController");
    var AudioManager_1 = require("../../../../Common/Tools/AudioManager/AudioManager");
    var CommonTool_1 = require("../../../../Common/Tools/CommonTool");
    var OrientationToolManager_1 = require("../../../../Common/Tools/OrientationTool/OrientationToolManager");
    var Slot_GameUI_1 = require("../../../../SlotFramework/Game/view/Slot_GameUI");
    var G57_CollectFXPool_1 = require("../../../Common/script/G57_CollectFXPool");
    var G57_DataManager_1 = require("../../../Common/script/G57_DataManager");
    var G57_DynamicPopUpPanelManager_1 = require("../../../Common/script/G57_DynamicPopUpPanelManager");
    var G57_SocketManager_1 = require("../../../Common/script/socket/G57_SocketManager");
    var G57_AutoGamePanel_1 = require("./G57_AutoGamePanel");
    var G57_FreeGameGetScorePanel_1 = require("./G57_FreeGameGetScorePanel");
    var G57_FreeGamePanel_1 = require("./G57_FreeGamePanel");
    var G57_MusicOptionPanel_1 = require("./G57_MusicOptionPanel");
    var G57_SymbolTipPanel_1 = require("./G57_SymbolTipPanel");
    var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
    var G57_GameUI = function(_super) {
      __extends(G57_GameUI, _super);
      function G57_GameUI() {
        var _this = null !== _super && _super.apply(this, arguments) || this;
        _this.baseGameBG = null;
        _this.baseGameFX = null;
        _this.freeGameBG = null;
        _this.freeGameFX = null;
        _this.tip5String = "\u4ee5\u4e0b\u70ba\u9762\u677f";
        _this.freeGamePanel = null;
        _this.freeGameGetScorePanel = null;
        _this.autoGamePanel = null;
        _this.musicOptionPanel = null;
        _this.symbolTipPanel = null;
        _this.tip6String = "\u4ee5\u4e0b\u70baMG\u7279\u6548";
        _this.mgRewardWin_FX = null;
        _this.mgSpeakerJumpn_FX = null;
        _this.spinButtonEffect_2 = null;
        _this.spinButtonEffect_1 = null;
        _this.mgNode = [];
        _this.tip7String = "\u4ee5\u4e0b\u70baFG\u7279\u6548";
        _this.freeGameSpeciamultiple = null;
        _this.freeGameSpeciamultiple_FX = null;
        _this.speciamultipleRetrigger_FX = null;
        _this.fgRewardWin_FX = null;
        _this.fgRewardWinHFX = null;
        _this.fgRewardWinVFX = null;
        _this.fgSpeakerJump_FX = null;
        _this.spinBroadOnce_FX = null;
        _this.fgNode = [];
        _this.collectFXPools = [];
        _this.collectFlyFX = null;
        _this.fxFreegameAddTime = null;
        _this.preloadSpines = null;
        _this.isFreeGameCollctEffecting = false;
        _this.isMenuOpen = false;
        _this._menuClosePosition = cc.v3(0, -1e3, 0);
        _this._menuClosePosition_hori = cc.v3(1e3, 0, 0);
        return _this;
      }
      Object.defineProperty(G57_GameUI.prototype, "socket", {
        get: function() {
          return G57_SocketManager_1.default.getInstance();
        },
        enumerable: false,
        configurable: true
      });
      Object.defineProperty(G57_GameUI.prototype, "popup", {
        get: function() {
          return G57_DynamicPopUpPanelManager_1.default.getInstance();
        },
        enumerable: false,
        configurable: true
      });
      Object.defineProperty(G57_GameUI.prototype, "data", {
        get: function() {
          return G57_DataManager_1.default.getInstance();
        },
        enumerable: false,
        configurable: true
      });
      G57_GameUI.prototype.init = function(p_data) {
        return __awaiter(this, void 0, Promise, function() {
          var i;
          return __generator(this, function(_a) {
            switch (_a.label) {
             case 0:
              _super.prototype.init.call(this, p_data);
              this.freeGamePanel.init();
              this.freeGameGetScorePanel.init();
              this.historyPanel.init(this.data, this.popup, this.socket);
              this.webRulePanel.init(this.data, this.popup);
              this.autoGamePanel.init();
              this.hideAllLongWilfEffect();
              this.setSpinArrowSpinningSpeed(Slot_GameUI_1.SPIN_ARROW_SPINNIG_TYPE.NORMAL);
              cc.systemEvent.on(OrientationToolManager_1.ORIENTATION_EVENT.RESIZE, this.switchCanvas, this);
              i = 0;
              _a.label = 1;

             case 1:
              if (!(i < this.collectFXPools.length)) return [ 3, 4 ];
              return [ 4, this.collectFXPools[i].initCollectFXPool() ];

             case 2:
              _a.sent();
              _a.label = 3;

             case 3:
              i++;
              return [ 3, 1 ];

             case 4:
              return [ 2 ];
            }
          });
        });
      };
      G57_GameUI.prototype.onDestroy = function() {
        cc.systemEvent.off(OrientationToolManager_1.ORIENTATION_EVENT.RESIZE, this.switchCanvas, this);
      };
      G57_GameUI.prototype.changeToBaseGameBackGround = function() {
        this.baseGameBG.opacity = 255;
        this.freeGameBG.opacity = 0;
        this.mgNode.forEach(function(item) {
          return item.opacity = 255;
        });
        this.fgNode.forEach(function(item) {
          return item.opacity = 0;
        });
        this.setBackBtnState(true);
      };
      G57_GameUI.prototype.changeToFreeGameBackGround = function() {
        this.freeGameBG.opacity = 255;
        this.baseGameBG.opacity = 0;
        this.setBackBtnState(false);
        this.mgNode.forEach(function(item) {
          return item.opacity = 0;
        });
        this.fgNode.forEach(function(item) {
          return item.opacity = 255;
        });
      };
      G57_GameUI.prototype.openFreeGamePanel = function(endTime) {
        this.freeGamePanel.open(endTime);
      };
      G57_GameUI.prototype.autoSelectFreeGameMode = function(mode) {
        this.freeGamePanel.setFXSelect(mode);
      };
      G57_GameUI.prototype.closeFreeGamePanel = function() {
        return this.freeGamePanel.close();
      };
      G57_GameUI.prototype.openFreeGameGetScorePanel = function(p_freeGameGetScore, p_betScore) {
        this.freeGameGetScorePanel.open(p_freeGameGetScore, p_betScore);
      };
      G57_GameUI.prototype.closeFreeGameGetScorePanel = function() {
        return this.freeGameGetScorePanel.close();
      };
      G57_GameUI.prototype.showSpinButtonClickEffect = function() {
        var _this = this;
        cc.tween(this.spinButtonEffect_2).call(function() {
          _this.spinButtonEffect_1.play();
          _this.spinButtonEffect_2.play();
        }).delay(1).call(function() {
          _this.spinButtonEffect_1.stop();
          _this.spinButtonEffect_2.stop();
        }).start();
      };
      G57_GameUI.prototype.showTipBroadEffect = function() {
        this.spinBroadOnce_FX.play();
      };
      G57_GameUI.prototype.openAutoGamePanel = function() {
        this.autoGamePanel.open();
      };
      G57_GameUI.prototype.isHorizontal = function() {
        return OrientationToolManager_1.default.orientationState === OrientationToolManager_1.ORIENTATION_TYPE.HORIZONTAL;
      };
      G57_GameUI.prototype.closeMenu = function(p_isFast) {
        var _this = this;
        this.isMenuOpen = false;
        if (p_isFast) {
          this.isHorizontal() ? this.menu.setPosition(this._menuClosePosition_hori) : this.menu.setPosition(this._menuClosePosition);
          this.menu.active = false;
        } else {
          this.menu.setPosition(cc.Vec3.ZERO);
          var pos = this.isHorizontal() ? this._menuClosePosition_hori : this._menuClosePosition;
          cc.tween(this.menu).to(this.menuCloseTime, {
            position: pos
          }).call(function() {
            _this.menu.active = false;
          }).start();
        }
      };
      G57_GameUI.prototype.openMenu = function() {
        this.isHorizontal() ? this.menu.setPosition(this._menuClosePosition_hori) : this.menu.setPosition(this._menuClosePosition);
        this.isMenuOpen = true;
        this.menu.active = true;
        cc.tween(this.menu).to(this.menuOpenTime, {
          position: cc.Vec3.ZERO
        }).start();
      };
      G57_GameUI.prototype.showTotalGetScore = function(p_totalGetScore) {
        _super.prototype.showTotalGetScore.call(this, p_totalGetScore);
        this.totalGetScoreTip.node.parent.scale = 0;
        cc.tween(this.totalGetScoreTip.node.parent).to(.2, {
          scale: 1.5
        }, {
          easing: "sineOut"
        }).to(.2, {
          scale: 1
        }, {
          easing: "sineIn"
        }).start();
      };
      G57_GameUI.prototype.setSpeciamultiple = function(specialmultuple) {
        this.freeGameSpeciamultiple.string = specialmultuple.toString();
        this.freeGameSpeciamultiple_FX.string = specialmultuple.toString();
      };
      G57_GameUI.prototype.specianultipleFX = function() {
        this.speciamultipleRetrigger_FX.play();
      };
      G57_GameUI.prototype.mgRewardFX = function() {
        this.mgRewardWin_FX.play();
        this.mgSpeakerJumpn_FX.play();
      };
      G57_GameUI.prototype.fgRewardFX = function() {
        this.fgRewardWin_FX.play();
        this.fgRewardWinHFX.node.active && this.fgRewardWinHFX.play();
        this.fgRewardWinVFX.node.active && this.fgRewardWinVFX.play();
        this.fgSpeakerJump_FX.play();
      };
      G57_GameUI.prototype.setSpinArrowSpinningSpeed = function(p_type) {
        this.spinArrow.stopAllActions();
        p_type == Slot_GameUI_1.SPIN_ARROW_SPINNIG_TYPE.STOP || (p_type == Slot_GameUI_1.SPIN_ARROW_SPINNIG_TYPE.NORMAL ? cc.tween(this.spinArrow).by(this.spinArrowRotateDuration_Normal, {
          angle: -360
        }).repeatForever().start() : cc.tween(this.spinArrow).by(this.spinArrowRotateDuration_Fast, {
          angle: -360
        }).repeatForever().start());
      };
      G57_GameUI.prototype.showFreeGameCollctEffect = function(parent, winPosList, remainingFreeGameCount) {
        var _this = this;
        cc.tween(this.node).call(function() {
          _this.isFreeGameCollctEffecting = true;
        }).delay(.2).call(function() {
          return __awaiter(_this, void 0, void 0, function() {
            var _loop_1, this_1, i;
            var _this = this;
            return __generator(this, function(_a) {
              switch (_a.label) {
               case 0:
                AudioManager_1.AudioManager.instance.playAudioEvent("FreeGameFireBall");
                _loop_1 = function(i) {
                  var oriPos, targetPos, fxCollectFly, fxScatterCollectStart, targetTo, centerTo, offset;
                  return __generator(this, function(_a) {
                    switch (_a.label) {
                     case 0:
                      return [ 4, CommonTool_1.CommonTool.toNextTick(20) ];

                     case 1:
                      _a.sent();
                      oriPos = parent.convertToWorldSpaceAR(winPosList[i]);
                      oriPos = this_1.collectFlyFX.convertToNodeSpaceAR(oriPos);
                      targetPos = this_1.remainingFreeGameCount.node.parent.convertToWorldSpaceAR(this_1.remainingFreeGameCount.node.position);
                      targetPos = this_1.collectFlyFX.convertToNodeSpaceAR(targetPos);
                      fxCollectFly = this_1.collectFXPools[0].getCollectFX();
                      fxScatterCollectStart = this_1.collectFXPools[1].getCollectFX();
                      fxCollectFly.node.position = oriPos;
                      fxScatterCollectStart.node.position = oriPos;
                      this_1.collectFlyFX.addChild(fxCollectFly.node);
                      this_1.collectFlyFX.addChild(fxScatterCollectStart.node);
                      targetTo = cc.v2(winPosList[i].x, winPosList[i].y);
                      centerTo = cc.v2(.5 * (targetPos.x + targetTo.x), .5 * (targetPos.y + targetTo.y));
                      offset = 0;
                      targetTo.x - targetPos.x > 50 ? offset = 50 : targetTo.x - targetPos.x < -50 && (offset = -50);
                      cc.tween(fxCollectFly.node).call(function() {
                        fxScatterCollectStart.play();
                        fxCollectFly.play();
                      }).bezierTo(.5, cc.v2(centerTo.x + offset, centerTo.y), cc.v2(centerTo.x + offset, centerTo.y), cc.v2(targetPos.x, targetPos.y)).call(function() {
                        if (0 === i) {
                          AudioManager_1.AudioManager.instance.playAudioEvent("FreeGameFireballLand");
                          _this.fxFreegameAddTime.play();
                        }
                        _this.collectFXPools[1].recycleCollectFX(fxScatterCollectStart);
                      }).delay(.3).call(function() {
                        0 === i && _this.setRemainingFreeGameCount(remainingFreeGameCount);
                        _this.collectFXPools[0].recycleCollectFX(fxCollectFly);
                      }).start();
                      return [ 2 ];
                    }
                  });
                };
                this_1 = this;
                i = 0;
                _a.label = 1;

               case 1:
                if (!(i < winPosList.length)) return [ 3, 4 ];
                return [ 5, _loop_1(i) ];

               case 2:
                _a.sent();
                _a.label = 3;

               case 3:
                i++;
                return [ 3, 1 ];

               case 4:
                return [ 2 ];
              }
            });
          });
        }).delay(1).call(function() {
          _this.isFreeGameCollctEffecting = false;
        }).start();
      };
      G57_GameUI.prototype.preloadSpineAnimation = function() {
        return __awaiter(this, void 0, void 0, function() {
          var i, spine, animations, j;
          return __generator(this, function(_a) {
            switch (_a.label) {
             case 0:
              i = 0;
              _a.label = 1;

             case 1:
              if (!(i < this.preloadSpines.children.length)) return [ 3, 7 ];
              spine = this.preloadSpines.children[i].getComponent(sp.Skeleton);
              animations = spine.skeletonData.getRuntimeData().animations;
              spine.node.active = true;
              j = 0;
              _a.label = 2;

             case 2:
              if (!(j < animations.length)) return [ 3, 5 ];
              return [ 4, CommonTool_1.CommonTool.toNextTick() ];

             case 3:
              _a.sent();
              spine.updateAnimationCache(animations[j].name);
              _a.label = 4;

             case 4:
              j++;
              return [ 3, 2 ];

             case 5:
              spine.node.active = false;
              _a.label = 6;

             case 6:
              i++;
              return [ 3, 1 ];

             case 7:
              return [ 2 ];
            }
          });
        });
      };
      G57_GameUI.prototype.playLongWildSound = function() {
        AudioManager_1.AudioManager.instance.playAudioEvent("LongWildSound");
      };
      G57_GameUI.prototype.switchCanvas = function() {
        cc.Tween.stopAllByTarget(this.menu);
        var pos = OrientationToolManager_1.default.orientationState == OrientationToolManager_1.ORIENTATION_TYPE.HORIZONTAL ? this._menuClosePosition_hori : this._menuClosePosition;
        this.isMenuOpen ? this.menu.setPosition(cc.Vec3.ZERO) : this.menu.setPosition(pos);
        this.menu.active = this.isMenuOpen;
      };
      __decorate([ property(cc.Node) ], G57_GameUI.prototype, "baseGameBG", void 0);
      __decorate([ property(cc.Node) ], G57_GameUI.prototype, "baseGameFX", void 0);
      __decorate([ property(cc.Node) ], G57_GameUI.prototype, "freeGameBG", void 0);
      __decorate([ property(cc.Node) ], G57_GameUI.prototype, "freeGameFX", void 0);
      __decorate([ property({
        readonly: true,
        displayName: "============ \u3010 \u9762\u677f \u3011 ============"
      }) ], G57_GameUI.prototype, "tip5String", void 0);
      __decorate([ property(G57_FreeGamePanel_1.default) ], G57_GameUI.prototype, "freeGamePanel", void 0);
      __decorate([ property(G57_FreeGameGetScorePanel_1.default) ], G57_GameUI.prototype, "freeGameGetScorePanel", void 0);
      __decorate([ property({
        type: G57_AutoGamePanel_1.default,
        override: true
      }) ], G57_GameUI.prototype, "autoGamePanel", void 0);
      __decorate([ property({
        type: G57_MusicOptionPanel_1.default,
        override: true
      }) ], G57_GameUI.prototype, "musicOptionPanel", void 0);
      __decorate([ property({
        type: G57_SymbolTipPanel_1.default,
        override: true
      }) ], G57_GameUI.prototype, "symbolTipPanel", void 0);
      __decorate([ property({
        readonly: true,
        displayName: "============ \u3010 MG\u7279\u6548 \u3011 ============"
      }) ], G57_GameUI.prototype, "tip6String", void 0);
      __decorate([ property(FXController_1.default) ], G57_GameUI.prototype, "mgRewardWin_FX", void 0);
      __decorate([ property(FXController_1.default) ], G57_GameUI.prototype, "mgSpeakerJumpn_FX", void 0);
      __decorate([ property(FXController_1.default) ], G57_GameUI.prototype, "spinButtonEffect_2", void 0);
      __decorate([ property(FXController_1.default) ], G57_GameUI.prototype, "spinButtonEffect_1", void 0);
      __decorate([ property([ cc.Node ]) ], G57_GameUI.prototype, "mgNode", void 0);
      __decorate([ property({
        readonly: true,
        displayName: "============ \u3010 FG\u7279\u6548 \u3011 ============"
      }) ], G57_GameUI.prototype, "tip7String", void 0);
      __decorate([ property(cc.Label) ], G57_GameUI.prototype, "freeGameSpeciamultiple", void 0);
      __decorate([ property(cc.Label) ], G57_GameUI.prototype, "freeGameSpeciamultiple_FX", void 0);
      __decorate([ property(FXController_1.default) ], G57_GameUI.prototype, "speciamultipleRetrigger_FX", void 0);
      __decorate([ property(FXController_1.default) ], G57_GameUI.prototype, "fgRewardWin_FX", void 0);
      __decorate([ property(FXController_1.default) ], G57_GameUI.prototype, "fgRewardWinHFX", void 0);
      __decorate([ property(FXController_1.default) ], G57_GameUI.prototype, "fgRewardWinVFX", void 0);
      __decorate([ property(FXController_1.default) ], G57_GameUI.prototype, "fgSpeakerJump_FX", void 0);
      __decorate([ property(FXController_1.default) ], G57_GameUI.prototype, "spinBroadOnce_FX", void 0);
      __decorate([ property([ cc.Node ]) ], G57_GameUI.prototype, "fgNode", void 0);
      __decorate([ property([ G57_CollectFXPool_1.default ]) ], G57_GameUI.prototype, "collectFXPools", void 0);
      __decorate([ property(cc.Node) ], G57_GameUI.prototype, "collectFlyFX", void 0);
      __decorate([ property(FXController_1.default) ], G57_GameUI.prototype, "fxFreegameAddTime", void 0);
      __decorate([ property(cc.Node) ], G57_GameUI.prototype, "preloadSpines", void 0);
      G57_GameUI = __decorate([ ccclass ], G57_GameUI);
      return G57_GameUI;
    }(Slot_GameUI_1.default);
    exports.default = G57_GameUI;
    cc._RF.pop();
  }, {
    "../../../../Common/Optional/Crosis/FXController/FXController": void 0,
    "../../../../Common/Tools/AudioManager/AudioManager": void 0,
    "../../../../Common/Tools/CommonTool": void 0,
    "../../../../Common/Tools/OrientationTool/OrientationToolManager": void 0,
    "../../../../SlotFramework/Game/view/Slot_GameUI": void 0,
    "../../../Common/script/G57_CollectFXPool": "G57_CollectFXPool",
    "../../../Common/script/G57_DataManager": "G57_DataManager",
    "../../../Common/script/G57_DynamicPopUpPanelManager": "G57_DynamicPopUpPanelManager",
    "../../../Common/script/socket/G57_SocketManager": "G57_SocketManager",
    "./G57_AutoGamePanel": "G57_AutoGamePanel",
    "./G57_FreeGameGetScorePanel": "G57_FreeGameGetScorePanel",
    "./G57_FreeGamePanel": "G57_FreeGamePanel",
    "./G57_MusicOptionPanel": "G57_MusicOptionPanel",
    "./G57_SymbolTipPanel": "G57_SymbolTipPanel"
  } ],
  G57_Game: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "55ab2wkE2dHH4krJnrJyRuZ", "G57_Game");
    "use strict";
    var __extends = this && this.__extends || function() {
      var extendStatics = function(d, b) {
        extendStatics = Object.setPrototypeOf || {
          __proto__: []
        } instanceof Array && function(d, b) {
          d.__proto__ = b;
        } || function(d, b) {
          for (var p in b) Object.prototype.hasOwnProperty.call(b, p) && (d[p] = b[p]);
        };
        return extendStatics(d, b);
      };
      return function(d, b) {
        extendStatics(d, b);
        function __() {
          this.constructor = d;
        }
        d.prototype = null === b ? Object.create(b) : (__.prototype = b.prototype, new __());
      };
    }();
    var __decorate = this && this.__decorate || function(decorators, target, key, desc) {
      var c = arguments.length, r = c < 3 ? target : null === desc ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
      if ("object" === typeof Reflect && "function" === typeof Reflect.decorate) r = Reflect.decorate(decorators, target, key, desc); else for (var i = decorators.length - 1; i >= 0; i--) (d = decorators[i]) && (r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r);
      return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __awaiter = this && this.__awaiter || function(thisArg, _arguments, P, generator) {
      function adopt(value) {
        return value instanceof P ? value : new P(function(resolve) {
          resolve(value);
        });
      }
      return new (P || (P = Promise))(function(resolve, reject) {
        function fulfilled(value) {
          try {
            step(generator.next(value));
          } catch (e) {
            reject(e);
          }
        }
        function rejected(value) {
          try {
            step(generator["throw"](value));
          } catch (e) {
            reject(e);
          }
        }
        function step(result) {
          result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected);
        }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
      });
    };
    var __generator = this && this.__generator || function(thisArg, body) {
      var _ = {
        label: 0,
        sent: function() {
          if (1 & t[0]) throw t[1];
          return t[1];
        },
        trys: [],
        ops: []
      }, f, y, t, g;
      return g = {
        next: verb(0),
        throw: verb(1),
        return: verb(2)
      }, "function" === typeof Symbol && (g[Symbol.iterator] = function() {
        return this;
      }), g;
      function verb(n) {
        return function(v) {
          return step([ n, v ]);
        };
      }
      function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
          if (f = 1, y && (t = 2 & op[0] ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 
          0) : y.next) && !(t = t.call(y, op[1])).done) return t;
          (y = 0, t) && (op = [ 2 & op[0], t.value ]);
          switch (op[0]) {
           case 0:
           case 1:
            t = op;
            break;

           case 4:
            _.label++;
            return {
              value: op[1],
              done: false
            };

           case 5:
            _.label++;
            y = op[1];
            op = [ 0 ];
            continue;

           case 7:
            op = _.ops.pop();
            _.trys.pop();
            continue;

           default:
            if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (6 === op[0] || 2 === op[0])) {
              _ = 0;
              continue;
            }
            if (3 === op[0] && (!t || op[1] > t[0] && op[1] < t[3])) {
              _.label = op[1];
              break;
            }
            if (6 === op[0] && _.label < t[1]) {
              _.label = t[1];
              t = op;
              break;
            }
            if (t && _.label < t[2]) {
              _.label = t[2];
              _.ops.push(op);
              break;
            }
            t[2] && _.ops.pop();
            _.trys.pop();
            continue;
          }
          op = body.call(thisArg, _);
        } catch (e) {
          op = [ 6, e ];
          y = 0;
        } finally {
          f = t = 0;
        }
        if (5 & op[0]) throw op[1];
        return {
          value: op[0] ? op[1] : void 0,
          done: true
        };
      }
    };
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    var AudioManager_1 = require("../../../Common/Tools/AudioManager/AudioManager");
    var CommonTool_1 = require("../../../Common/Tools/CommonTool");
    var LocalizationManager_1 = require("../../../Common/Tools/Localization/LocalizationManager");
    var Slot_GameManager_InitData_1 = require("../../../SlotFramework/Game/Data/Slot_GameManager_InitData");
    var Slot_GameUI_InitData_1 = require("../../../SlotFramework/Game/Data/Slot_GameUI_InitData");
    var Slot_ReelManager_InitData_1 = require("../../../SlotFramework/Game/Data/Slot_ReelManager_InitData");
    var Slot_ReelManager_ScrollData_1 = require("../../../SlotFramework/Game/Data/Slot_ReelManager_ScrollData");
    var Slot_GameManager_1 = require("../../../SlotFramework/Game/Slot_GameManager");
    var Slot_GameUI_1 = require("../../../SlotFramework/Game/view/Slot_GameUI");
    var Slot_DataManager_1 = require("../../../SlotFramework/Slot_DataManager");
    var G57_DataManager_1 = require("../../Common/script/G57_DataManager");
    var G57_DynamicPopUpPanelManager_1 = require("../../Common/script/G57_DynamicPopUpPanelManager");
    var G57_SocketManager_1 = require("../../Common/script/socket/G57_SocketManager");
    var G57_GameUI_1 = require("./view/G57_GameUI");
    var G57_SlotReelManager_1 = require("./view/G57_SlotReelManager");
    var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
    var GAME_STATUS;
    (function(GAME_STATUS) {
      GAME_STATUS[GAME_STATUS["NONE"] = 0] = "NONE";
      GAME_STATUS[GAME_STATUS["IDLE"] = 1] = "IDLE";
      GAME_STATUS[GAME_STATUS["SERVER_REQUEST"] = 2] = "SERVER_REQUEST";
      GAME_STATUS[GAME_STATUS["SPIN"] = 3] = "SPIN";
      GAME_STATUS[GAME_STATUS["FREE_GAME_TIP"] = 4] = "FREE_GAME_TIP";
      GAME_STATUS[GAME_STATUS["FREE_GAME"] = 5] = "FREE_GAME";
      GAME_STATUS[GAME_STATUS["FREE_GAME_REWARD"] = 6] = "FREE_GAME_REWARD";
      GAME_STATUS[GAME_STATUS["BOUNS_GAME"] = 7] = "BOUNS_GAME";
      GAME_STATUS[GAME_STATUS["BOUNS_GAME_REWARD"] = 8] = "BOUNS_GAME_REWARD";
      GAME_STATUS[GAME_STATUS["FREE_GAME_WIN_SCORE_EFFECT"] = 9] = "FREE_GAME_WIN_SCORE_EFFECT";
      GAME_STATUS[GAME_STATUS["CHANGE_PANEL"] = 10] = "CHANGE_PANEL";
      GAME_STATUS[GAME_STATUS["JACKPOT_WIN_SCORE"] = 11] = "JACKPOT_WIN_SCORE";
    })(GAME_STATUS || (GAME_STATUS = {}));
    var FREEGAME_WIN_SCORE_EFFECT_STATUS;
    (function(FREEGAME_WIN_SCORE_EFFECT_STATUS) {
      FREEGAME_WIN_SCORE_EFFECT_STATUS[FREEGAME_WIN_SCORE_EFFECT_STATUS["NONE"] = 0] = "NONE";
      FREEGAME_WIN_SCORE_EFFECT_STATUS[FREEGAME_WIN_SCORE_EFFECT_STATUS["WIN_SCORE_EFFECT_START"] = 1] = "WIN_SCORE_EFFECT_START";
      FREEGAME_WIN_SCORE_EFFECT_STATUS[FREEGAME_WIN_SCORE_EFFECT_STATUS["WIN_SCORE_EFFECT_ING"] = 2] = "WIN_SCORE_EFFECT_ING";
      FREEGAME_WIN_SCORE_EFFECT_STATUS[FREEGAME_WIN_SCORE_EFFECT_STATUS["WIN_SCORE_EFFECT_END"] = 3] = "WIN_SCORE_EFFECT_END";
    })(FREEGAME_WIN_SCORE_EFFECT_STATUS || (FREEGAME_WIN_SCORE_EFFECT_STATUS = {}));
    var SPIN_STATUS;
    (function(SPIN_STATUS) {
      SPIN_STATUS[SPIN_STATUS["NONE"] = 0] = "NONE";
      SPIN_STATUS[SPIN_STATUS["REEL_SCROLL_START"] = 1] = "REEL_SCROLL_START";
      SPIN_STATUS[SPIN_STATUS["REEL_SCROLL_ING"] = 2] = "REEL_SCROLL_ING";
      SPIN_STATUS[SPIN_STATUS["REEL_SCROLL_END"] = 3] = "REEL_SCROLL_END";
      SPIN_STATUS[SPIN_STATUS["CHECK_REWARD"] = 4] = "CHECK_REWARD";
      SPIN_STATUS[SPIN_STATUS["REWARD_EFFECT_START"] = 5] = "REWARD_EFFECT_START";
      SPIN_STATUS[SPIN_STATUS["REWARD_EFFECT_ING"] = 6] = "REWARD_EFFECT_ING";
      SPIN_STATUS[SPIN_STATUS["REWARD_EFFECT_END"] = 7] = "REWARD_EFFECT_END";
      SPIN_STATUS[SPIN_STATUS["CHECK_SCATTER_WIN"] = 8] = "CHECK_SCATTER_WIN";
      SPIN_STATUS[SPIN_STATUS["SCATTER_WIN_WAIT"] = 9] = "SCATTER_WIN_WAIT";
      SPIN_STATUS[SPIN_STATUS["SCATTER_WIN_START"] = 10] = "SCATTER_WIN_START";
      SPIN_STATUS[SPIN_STATUS["SCATTER_WIN_ING"] = 11] = "SCATTER_WIN_ING";
      SPIN_STATUS[SPIN_STATUS["WIN_SCORE_EFFECT_START"] = 12] = "WIN_SCORE_EFFECT_START";
      SPIN_STATUS[SPIN_STATUS["WIN_SCORE_EFFECT_ING"] = 13] = "WIN_SCORE_EFFECT_ING";
      SPIN_STATUS[SPIN_STATUS["WIN_SCORE_EFFECT_END"] = 14] = "WIN_SCORE_EFFECT_END";
      SPIN_STATUS[SPIN_STATUS["CHECK_FREE_GAME_COLLECT"] = 15] = "CHECK_FREE_GAME_COLLECT";
      SPIN_STATUS[SPIN_STATUS["FREE_GAME_COLLECT_START"] = 16] = "FREE_GAME_COLLECT_START";
      SPIN_STATUS[SPIN_STATUS["FREE_GAME_COLLECT_ING"] = 17] = "FREE_GAME_COLLECT_ING";
      SPIN_STATUS[SPIN_STATUS["JACKPOT_WIN_START"] = 18] = "JACKPOT_WIN_START";
      SPIN_STATUS[SPIN_STATUS["JACKPOT_WIN_ING"] = 19] = "JACKPOT_WIN_ING";
      SPIN_STATUS[SPIN_STATUS["FINISHED_WAIT"] = 20] = "FINISHED_WAIT";
      SPIN_STATUS[SPIN_STATUS["FINISHED"] = 21] = "FINISHED";
    })(SPIN_STATUS || (SPIN_STATUS = {}));
    var G57_Game = function(_super) {
      __extends(G57_Game, _super);
      function G57_Game() {
        var _this = null !== _super && _super.apply(this, arguments) || this;
        _this.slotReelManager = null;
        _this.ui = null;
        _this.firstWinLineChangeTime = .4;
        _this.winLineChangeTime = 2;
        _this.winLineChangeWaitTime = .1;
        _this.spinCD = .5;
        _this.normalSpinFinishedStopTime = .5;
        _this.fastSpinFinishedStopTime = .2;
        _this.normalUpdateTipBroadEffectTime = .1;
        _this.autoSpinUpdateTipBroadEffectTime = .9;
        _this.normalRewardTime = 1;
        _this.autoRewardTime = 1;
        _this.scatterShowTime = 3;
        _this.showOmenScatterCount = 3;
        _this.data = null;
        _this.socket = null;
        _this.popup = null;
        _this.competitionStartFontSize1 = 30;
        _this.competitionStartFontColor1 = "#FFFFFF";
        _this.competitionStartFontSize2 = 30;
        _this.competitionStartFontColor2 = "#DC5050";
        _this.gameStatus = GAME_STATUS.NONE;
        _this.spinStatus = SPIN_STATUS.NONE;
        _this.freeGameWinScoreEffectStatus = FREEGAME_WIN_SCORE_EFFECT_STATUS.NONE;
        _this.hasWinLine = false;
        _this.tempScatterWaitTime = 0;
        _this.tempWinLineChangeTime = 0;
        _this.nowShowWinLine = 0;
        _this.selectMode = 0;
        _this.selectFreeGameTimeout = 0;
        return _this;
      }
      G57_Game.prototype.start = function() {
        return __awaiter(this, void 0, void 0, function() {
          var _data;
          var _this = this;
          return __generator(this, function(_a) {
            switch (_a.label) {
             case 0:
              this.socket = G57_SocketManager_1.default.getInstance();
              this.data = G57_DataManager_1.default.getInstance();
              this.popup = G57_DynamicPopUpPanelManager_1.default.getInstance();
              _data = new Slot_GameManager_InitData_1.default();
              _data.bundleName = "G" + G57_DataManager_1.default.getInstance().gameID;
              _data.bundlePath = "" + G57_DataManager_1.default.getInstance().path;
              return [ 4, this.init(_data) ];

             case 1:
              _a.sent();
              this.socket.setOnGetInitialReelInfoEvent().then(function(p_data) {
                _this.setReelInfo(p_data);
                _this.playerLogin();
                _this.gameStatus = GAME_STATUS.IDLE;
                _super.prototype.start.call(_this);
              });
              return [ 2 ];
            }
          });
        });
      };
      G57_Game.prototype.onDisable = function() {
        _super.prototype.onDisable.call(this);
        AudioManager_1.AudioManager.instance.playAudioEvent("GameExit");
      };
      G57_Game.prototype.onDestroy = function() {
        CommonTool_1.CommonTool.clearNextTickTask();
      };
      G57_Game.prototype.init = function(p_data) {
        return __awaiter(this, void 0, Promise, function() {
          var _uiData, _data;
          return __generator(this, function(_a) {
            switch (_a.label) {
             case 0:
              _super.prototype.init.call(this, p_data);
              this.data = G57_DataManager_1.default.getInstance();
              this.data.init();
              _uiData = new Slot_GameUI_InitData_1.default();
              _uiData.bundleName = p_data.bundleName;
              _uiData.bundlePath = p_data.bundlePath;
              _uiData.dataManager = this.data;
              return [ 4, this.ui.init(_uiData) ];

             case 1:
              _a.sent();
              this.ui.setVersion(this.data.gameVersion);
              _data = new Slot_ReelManager_InitData_1.default();
              _data.bundleName = p_data.bundleName;
              _data.bundlePath = p_data.bundlePath;
              return [ 4, this.slotReelManager.init(_data) ];

             case 2:
              _a.sent();
              return [ 4, this.ui.preloadSpineAnimation() ];

             case 3:
              _a.sent();
              this.registerEvents();
              this.ui.changeToBaseGameBackGround();
              AudioManager_1.AudioManager.instance.playAudioEvent("GameInit");
              return [ 2 ];
            }
          });
        });
      };
      G57_Game.prototype.update = function(dt) {
        if (this.toFastStopSpin && this.tempSpinCD > this.spinCD) {
          this.stopSpin();
          this.toFastStopSpin = false;
        }
        if (this.gameStatus == GAME_STATUS.SPIN || this.gameStatus == GAME_STATUS.FREE_GAME) {
          this.tempSpinCD += dt;
          switch (this.spinStatus) {
           case SPIN_STATUS.REEL_SCROLL_START:
            this.reelScrollStart();
            break;

           case SPIN_STATUS.REEL_SCROLL_ING:
            this.reelScrolling();
            break;

           case SPIN_STATUS.REEL_SCROLL_END:
            this.reelScrollEnd();
            break;

           case SPIN_STATUS.CHECK_REWARD:
            this.checkReward();
            break;

           case SPIN_STATUS.REWARD_EFFECT_START:
            this.rewardEffectStart();
            break;

           case SPIN_STATUS.REWARD_EFFECT_ING:
            this.rewardEffecting();
            break;

           case SPIN_STATUS.REWARD_EFFECT_END:
            this.rewardEffectEnd();
            break;

           case SPIN_STATUS.CHECK_SCATTER_WIN:
            this.chackScatterWin();
            break;

           case SPIN_STATUS.SCATTER_WIN_WAIT:
            this.scatterWinWait(dt);
            break;

           case SPIN_STATUS.SCATTER_WIN_START:
            this.scatterWinEffect();
            break;

           case SPIN_STATUS.SCATTER_WIN_ING:
            this.scatterWinEffecting();
            break;

           case SPIN_STATUS.WIN_SCORE_EFFECT_START:
            this.winScoreEffectStart();
            break;

           case SPIN_STATUS.WIN_SCORE_EFFECT_ING:
            this.winScoreEffectIng();
            break;

           case SPIN_STATUS.WIN_SCORE_EFFECT_END:
            this.winScoreEffectEnd();
            break;

           case SPIN_STATUS.CHECK_FREE_GAME_COLLECT:
            this.checkFreeGameCollect();
            break;

           case SPIN_STATUS.FREE_GAME_COLLECT_START:
            this.freeGameCollectStart();
            break;

           case SPIN_STATUS.FREE_GAME_COLLECT_ING:
            this.freeGameCollectEffecting();
            break;

           case SPIN_STATUS.FINISHED_WAIT:
            this.spinFinishedWait(dt);
            break;

           case SPIN_STATUS.FINISHED:
            this.spinFinished();
          }
        } else if (this.gameStatus == GAME_STATUS.FREE_GAME_WIN_SCORE_EFFECT) switch (this.freeGameWinScoreEffectStatus) {
         case FREEGAME_WIN_SCORE_EFFECT_STATUS.WIN_SCORE_EFFECT_START:
          this.freeGameWinScoreEffectStart();
          break;

         case FREEGAME_WIN_SCORE_EFFECT_STATUS.WIN_SCORE_EFFECT_ING:
          this.freeGameWinScoreEffectIng();
          break;

         case FREEGAME_WIN_SCORE_EFFECT_STATUS.WIN_SCORE_EFFECT_END:
          this.freeGameWinScoreEffectEnd();
        } else if (this.gameStatus == GAME_STATUS.JACKPOT_WIN_SCORE) switch (this.spinStatus) {
         case SPIN_STATUS.JACKPOT_WIN_START:
          this.jackpotWinStart();
          break;

         case SPIN_STATUS.JACKPOT_WIN_ING:
        } else if (this.gameStatus == GAME_STATUS.IDLE) {
          if (this.isChangeMoney) {
            this.isChangeMoney = false;
            this.data.coin = this.data.getClientMoney(this.newMoney);
            this.ui.setScore(this.data.coin);
          }
          this.hasWinLine && this.TakeTurnsAllWinLine(dt);
        }
      };
      G57_Game.prototype.jackpotWinStart = function() {
        var _this = this;
        console.log("\u3010 jp \u8d0f\u9322\u6548\u679c \u3011\u958b\u59cb");
        this.spinStatus = SPIN_STATUS.JACKPOT_WIN_ING;
        var done = function() {
          _this.gameStatus = GAME_STATUS.IDLE;
          _this.spinStatus = SPIN_STATUS.NONE;
          _this.ui.addScore(_this.data.getClientMoney(_this.data.jackpotData.amount));
          _this.data.jackpotData = null;
          _this.checkAutoGameContinue();
        };
        cc.systemEvent.emit("JACKPOT_WIN_START", this.data.jackpotData, done);
      };
      G57_Game.prototype.registerEvents = function() {
        var _this = this;
        G57_SocketManager_1.default.getInstance().setBalanceModifyCallback(function(data) {
          _this.onBalanceModifyEvent(data);
        });
      };
      G57_Game.prototype.showSymbolTip = function(symbolID, position) {
        this.gameStatus == GAME_STATUS.IDLE && _super.prototype.showSymbolTip.call(this, symbolID, position);
      };
      G57_Game.prototype.baseGameSpin_Response = function(p_data) {
        var _a;
        this.ui.showSpinMask();
        if (this.data.isAutoSpin) {
          this.autoSpinCount++;
          this.ui.openCancelAutoSpinButton();
          var _autoSpinCount = this.data.autoGameSpinCount - this.autoSpinCount;
          -1 == this.data.autoGameSpinCount && (_autoSpinCount = -1);
          this.ui.setAutoSpinCount(_autoSpinCount);
        }
        this.tempWinLineChangeTime = this.winLineChangeTime - this.firstWinLineChangeTime;
        this.tempSpinCD = 0;
        this.toFastStopSpin = false;
        this.resultListIndex = 0;
        this.data.coin = this.data.getClientMoney(p_data.currentCash);
        this.data.totalBonus = this.data.getClientMoney(p_data.totalBonus);
        this.data.playerBetScore = this.data.getClientMoney(p_data.totalBet);
        this.data.resultList = p_data.resultList;
        this.data.jackpotData = p_data.Jackpot;
        var jpAmount = (null === (_a = p_data.Jackpot) || void 0 === _a ? void 0 : _a.amount) || 0;
        var _money = this.data.getClientMoney(p_data.currentCash - p_data.totalBonus - jpAmount);
        this.ui.setScore(_money);
        this.ui.setWinScore(0);
        this.ui.setSpinArrowSpinningSpeed(Slot_GameUI_1.SPIN_ARROW_SPINNIG_TYPE.FAST);
        this.gameStatus = GAME_STATUS.SPIN;
        this.spinStatus = SPIN_STATUS.REEL_SCROLL_START;
        this.ui.spinButton.getComponent(cc.Button).interactable = true;
      };
      G57_Game.prototype.onSpin = function() {
        var _this = this;
        console.log("Spin");
        if (this.gameStatus == GAME_STATUS.SPIN && this.spinStatus == SPIN_STATUS.REEL_SCROLL_ING) this.toFastStopSpin = true; else if (this.gameStatus == GAME_STATUS.IDLE) {
          var _betScore = this.data.betOdds[this.data.betOddsStartIndex];
          if (this.hasEnoughBetScore()) {
            this.data.isAutoSpin || this.ui.showSpinButtonClickEffect();
            AudioManager_1.AudioManager.instance.playAudioEvent("StartSpin");
            _super.prototype.onSpin.call(this);
            this.gameStatus = GAME_STATUS.SERVER_REQUEST, this.socket.setOnSpinBaseGameEvent(_betScore).then(function(response) {
              _this.baseGameSpin_Response(response);
            }).catch(function(error) {
              error.code == GameClient.errCode.RESPONSE_TIMED_OUT ? _this.socket.showErrorAlert(LocalizationManager_1.default.getInstance().get("ERROR_SOCKET_TIME_OUT"), null, function() {
                G57_SocketManager_1.default.getInstance().login();
              }) : error.code == GameClient.errCode.SLOT_INSUFFICIENT_BALANCE && _this.socket.showErrorAlert(LocalizationManager_1.default.getInstance().get("ERROR_NO_COIN"), null, function() {
                if (_this.data.isAutoSpin) {
                  _this.cancelAutoSpin();
                  _this.ui.hideSpinMask();
                  _this.ui.showSpinButtonIdleEffect();
                  _this.ui.setSpinArrowSpinningSpeed(Slot_GameUI_1.SPIN_ARROW_SPINNIG_TYPE.NORMAL);
                }
              });
              _this.ui.spinButton.getComponent(cc.Button).interactable = true;
              _this.gameStatus = GAME_STATUS.IDLE;
            });
          } else {
            this.showLackMoneyPanel(_betScore * this.data.oneOddsScore);
            this.data.isAutoSpin && this.cancelAutoSpin();
          }
        }
      };
      G57_Game.prototype.stopSpin = function() {
        AudioManager_1.AudioManager.instance.playAudioEvent("StopSpin");
        this.slotReelManager.stopAllReelScroll();
      };
      G57_Game.prototype.onCloseWinEffect = function() {
        this.ui.winScoreIsEndScore() ? this.winScoreEffectEnd() : this.ui.winScoreEffectToEndScore();
      };
      G57_Game.prototype.onStartAutoGame = function() {
        console.error("\u958b\u59cb\u5206\u6578 " + this.data.coin + "\n \u65cb\u8f49\u6b21\u6578 " + this.data.autoGameSpinCount + "\n\u8f38\u5206\u505c\u6b62 " + this.data.autoGameScoreLessStop + "\n\u8d0f\u5206\u505c\u6b62 " + this.data.autoGameWinScoreStop);
        console.log("\u8f38\u5206\u505c\u6b62 " + this.data.autoGameScoreLessStop);
        console.log("\u8d0f\u5206\u505c\u6b62 " + this.data.autoGameWinScoreStop);
        AudioManager_1.AudioManager.instance.playAudioEvent("StartAutoGame");
        this.data.isAutoSpin = true;
        this.ui.autoSpinSwitch(true);
        this.autoSpinCount = 0;
        this.data.autoGameStartScore = this.data.coin;
        this.onSpin();
      };
      G57_Game.prototype.startFreeGame = function() {
        return __awaiter(this, void 0, void 0, function() {
          var specialmultiple;
          var _this = this;
          return __generator(this, function(_a) {
            switch (_a.label) {
             case 0:
              if (this.gameStatus == GAME_STATUS.CHANGE_PANEL) return [ 2 ];
              console.log("\u958b\u59cb\u514d\u8cbb\u904a\u6232");
              this.resultListIndex++;
              this.freeGameOddsIndex = 0;
              this.gameStatus = GAME_STATUS.CHANGE_PANEL;
              cc.systemEvent.emit("JACKPOT_OPEN", false);
              this.saveBaseGameLastSymbol();
              this.slotReelManager.setNormalMode();
              this.slotReelManager.setReelTable(this.data.reelTable_ALLFG[this.selectMode]);
              this.slotReelManager.hideMaskEffect();
              specialmultiple = this.data.resultList[this.resultListIndex].specialMultiple;
              this.ui.setSpeciamultiple(specialmultiple);
              return [ 4, CommonTool_1.CommonTool.toNextTick() ];

             case 1:
              _a.sent();
              this.ui.closeFreeGamePanel().then(function() {
                AudioManager_1.AudioManager.instance.playAudioEvent("StarFreeGame");
                _this.gameStatus = GAME_STATUS.FREE_GAME;
                _this.spinStatus = SPIN_STATUS.REEL_SCROLL_START;
              });
              this.ui.changeFreeGameOddsLabel(this.freeGameOddsIndex);
              this.ui.menuButtonSwitch(false);
              this.ui.buttonGroupSwitch(false);
              this.ui.showFreeGameOddsTip();
              this.ui.showRemainingFreeGameCount();
              this.ui.setRemainingFreeGameCount(this.data.remainingFreeGameCount);
              return [ 2 ];
            }
          });
        });
      };
      G57_Game.prototype.freeGameEnd = function() {
        var _this = this;
        var _a;
        if (this.gameStatus == GAME_STATUS.CHANGE_PANEL) return;
        _super.prototype.freeGameEnd.call(this);
        console.log("\u514d\u8cbb\u904a\u6232\u7d50\u675f");
        var jpAmount = this.data.getClientMoney((null === (_a = this.data.jackpotData) || void 0 === _a ? void 0 : _a.amount) || 0);
        var _money = this.data.coin - jpAmount;
        var _winMoney = this.data.totalBonus;
        cc.systemEvent.emit("JACKPOT_OPEN", true);
        this.ui.closeWinScoreEffect();
        this.ui.closeFreeGameGetScorePanel().then(function() {
          AudioManager_1.AudioManager.instance.playAudioEvent("FreeGameEnd");
          if (_this.hasWinScoreEffect(_winMoney)) {
            _this.spinStatus = SPIN_STATUS.NONE;
            _this.freeGameWinScoreEffectStatus = FREEGAME_WIN_SCORE_EFFECT_STATUS.WIN_SCORE_EFFECT_START;
            _this.gameStatus = GAME_STATUS.FREE_GAME_WIN_SCORE_EFFECT;
          } else if (_this.data.jackpotData) {
            _this.gameStatus = GAME_STATUS.JACKPOT_WIN_SCORE;
            _this.spinStatus = SPIN_STATUS.JACKPOT_WIN_START;
          } else {
            _this.gameStatus = GAME_STATUS.IDLE;
            _this.spinStatus = SPIN_STATUS.NONE;
            _this.checkAutoGameContinue();
          }
        });
        this.ui.hideRemainingFreeGameCount();
        this.ui.changeToBaseGameBackGround();
        this.ui.menuButtonSwitch(true);
        this.ui.buttonGroupSwitch(true);
        this.ui.setScore(_money);
        this.ui.setWinScore(_winMoney);
        this.ui.showMainGameTip();
        this.ui.hideGetScoreTip();
        this.ui.setSpeciamultiple(0);
        this.slotReelManager.setReelTable(this.data.reelTable_BG);
        this.slotReelManager.setAllReelSymbol(this.baseGameLastSymbol);
        this.slotReelManager.allSymbolPlayIdleEffect();
        this.slotReelManager.hideMaskEffect();
        this.hasWinLine = false;
        this.data.isFaseSpin && this.slotReelManager.setFastMode();
      };
      G57_Game.prototype.reelScrollStart = function() {
        return __awaiter(this, void 0, void 0, function() {
          var _targetIndex, _hasOmenReels, _scrollData;
          return __generator(this, function(_a) {
            switch (_a.label) {
             case 0:
              console.log("\u3010\u8f2a\u5b50\u6efe\u52d5\u3011 \u958b\u59cb");
              _targetIndex = this.data.resultList[this.resultListIndex].randomNumList;
              _hasOmenReels = this.getHasOmenReels();
              this.slotReelManager.hideScoreReWardEffect();
              this.hasWinLine = false;
              this.nowShowWinLine = 0;
              this.data.isFaseSpin || AudioManager_1.AudioManager.instance.playAudioEvent("ReelScrollStart");
              _scrollData = new Slot_ReelManager_ScrollData_1.default();
              _scrollData.targetSymbolIndex = _targetIndex;
              _scrollData.hasOmenReels = _hasOmenReels;
              this.data.isAutoSpin || this.gameStatus == GAME_STATUS.FREE_GAME ? _scrollData.rewardTime = this.autoRewardTime : _scrollData.rewardTime = this.normalRewardTime;
              this.ui.hideGetScoreTip();
              return [ 4, CommonTool_1.CommonTool.toNextTick() ];

             case 1:
              _a.sent();
              this.slotReelManager.scrollAllReels(_scrollData);
              this.socket.resetRoomTimeout();
              this.spinStatus = SPIN_STATUS.REEL_SCROLL_ING;
              if (this.gameStatus == GAME_STATUS.FREE_GAME) {
                this.resultListIndex > 1 && this.setSpeciamultiple();
                this.data.remainingFreeGameCount--;
                this.ui.setRemainingFreeGameCount(this.data.remainingFreeGameCount);
                this.ui.showFreeGameTip();
              } else this.ui.showMainGameTip();
              return [ 2 ];
            }
          });
        });
      };
      G57_Game.prototype.getHasOmenReels = function() {
        var _reelhasOmen = [];
        var _targetIndex = this.data.resultList[this.resultListIndex].randomNumList;
        var _reels = this.data.resultList[this.resultListIndex].reels;
        var _scatterCount = 0;
        for (var i = 0; i < _targetIndex.length; i++) {
          _scatterCount >= this.showOmenScatterCount - 1 && 3 == i ? _reelhasOmen[i] = true : _scatterCount > this.showOmenScatterCount - 1 && 4 == i ? _reelhasOmen[i] = true : _reelhasOmen[i] = false;
          for (var j = 0; j < _reels[i].length; j++) if (_reels[i][j] == Slot_DataManager_1.SYMBOL_NAME.Scatter) {
            _scatterCount++;
            continue;
          }
        }
        return _reelhasOmen;
      };
      G57_Game.prototype.reelScrolling = function() {
        this.slotReelManager.isReelSrolling() || (this.spinStatus = SPIN_STATUS.REEL_SCROLL_END);
      };
      G57_Game.prototype.reelScrollEnd = function() {
        console.log("\u3010\u8f2a\u5b50\u6efe\u52d5\u3011\u7d50\u675f");
        AudioManager_1.AudioManager.instance.playAudioEvent("AllReelStop");
        this.spinStatus = SPIN_STATUS.CHECK_REWARD;
      };
      G57_Game.prototype.checkReward = function() {
        console.log("\u6aa2\u67e5\u4e2d\u734e");
        var _winBouns = this.data.getClientMoney(this.data.resultList[this.resultListIndex].totalWinBonus);
        if (this.hasWinLineList() && _winBouns > 0) {
          this.hasWinLine = true;
          this.spinStatus = SPIN_STATUS.REWARD_EFFECT_START;
        } else this.gameStatus != GAME_STATUS.FREE_GAME ? this.spinStatus = SPIN_STATUS.CHECK_SCATTER_WIN : this.spinStatus = SPIN_STATUS.CHECK_FREE_GAME_COLLECT;
      };
      G57_Game.prototype.hasWinLineList = function() {
        if (this.data.resultList[this.resultListIndex].winWayList.length > 0) return true;
        return false;
      };
      G57_Game.prototype.setSpeciamultiple = function() {
        var specialmultiple = this.data.resultList[this.resultListIndex].specialMultiple;
        this.ui.setSpeciamultiple(specialmultiple);
        this.ui.specianultipleFX();
        AudioManager_1.AudioManager.instance.playAudioEvent("RemainFreeGameCount");
      };
      G57_Game.prototype.rewardEffectStart = function() {
        console.log("\u3010\u4e2d\u734e\u7279\u6548\u3011 \u958b\u59cb");
        var _winScore = this.data.getClientMoney(this.data.resultList[this.resultListIndex].totalWinBonus);
        var _winPositstion = this.data.resultList[this.resultListIndex].totalHitPos;
        this.ui.addScore(_winScore);
        this.ui.addWinScore(_winScore);
        this.ui.showTotalGetScore(_winScore);
        this.ui.showTipBroadEffect();
        this.ui.hideGameTip();
        this.slotReelManager.showSymbolReWardEffect(_winPositstion, true);
        this.playRewardSound();
        _winScore > 0 && (this.gameStatus == GAME_STATUS.FREE_GAME ? this.ui.fgRewardFX() : this.ui.mgRewardFX());
        this.spinStatus = SPIN_STATUS.REWARD_EFFECT_ING;
      };
      G57_Game.prototype.playRewardSound = function() {
        var reels = this.data.resultList[this.resultListIndex].reels;
        var winPositstion = this.data.resultList[this.resultListIndex].totalHitPos;
        var rewardAudio = this.gameStatus === GAME_STATUS.FREE_GAME ? "PlayRewardFree" : "PlayRewardMain";
        AudioManager_1.AudioManager.instance.playAudioEvent(rewardAudio);
        console.log("\u64ad\u653e\u4e2d\u734e\u97f3\u6548");
        for (var i = 0; i < reels.length; i++) for (var j = 0; j < reels[i].length; j++) if (1 === winPositstion[i][j] && reels[i][j] === Slot_DataManager_1.SYMBOL_NAME.Wild) {
          this.gameStatus === GAME_STATUS.FREE_GAME ? AudioManager_1.AudioManager.instance.playAudioEvent("PlayRewardFreeWild") : AudioManager_1.AudioManager.instance.playAudioEvent("PlayRewardMainWild");
          return;
        }
      };
      G57_Game.prototype.rewardEffecting = function() {
        this.slotReelManager.isRewardEffecting() || (this.spinStatus = SPIN_STATUS.REWARD_EFFECT_END);
      };
      G57_Game.prototype.rewardEffectEnd = function() {
        console.log("\u3010\u4e2d\u734e\u7279\u6548\u3011 \u7d50\u675f");
        var _winScore = this.data.getClientMoney(this.data.resultList[this.resultListIndex].totalWinBonus);
        this.gameStatus != GAME_STATUS.FREE_GAME ? this.spinStatus = SPIN_STATUS.CHECK_SCATTER_WIN : this.hasWinScoreEffect(_winScore) ? this.spinStatus = SPIN_STATUS.WIN_SCORE_EFFECT_START : this.spinStatus = SPIN_STATUS.CHECK_FREE_GAME_COLLECT;
      };
      G57_Game.prototype.chackScatterWin = function() {
        console.log("\u6aa2\u67e5scatter\u4e2d\u734e");
        var _scatterCount = this.data.resultList[this.resultListIndex].scatter.count;
        var _winScore = this.data.getClientMoney(this.data.resultList[this.resultListIndex].totalWinBonus);
        _scatterCount >= 5 ? this.spinStatus = this.hasWinLineList() ? SPIN_STATUS.SCATTER_WIN_WAIT : SPIN_STATUS.SCATTER_WIN_START : this.hasWinScoreEffect(_winScore) ? this.spinStatus = SPIN_STATUS.WIN_SCORE_EFFECT_START : this.spinStatus = SPIN_STATUS.FINISHED_WAIT;
      };
      G57_Game.prototype.scatterWinWait = function(p_dt) {
        this.tempScatterWaitTime += p_dt;
        var rewardTime;
        rewardTime = this.data.isAutoSpin || this.gameStatus == GAME_STATUS.FREE_GAME ? this.autoRewardTime : this.firstWinLineChangeTime;
        if (this.tempScatterWaitTime > rewardTime) {
          this.tempScatterWaitTime = 0;
          this.spinStatus = SPIN_STATUS.SCATTER_WIN_START;
        }
      };
      G57_Game.prototype.scatterWinEffect = function() {
        console.log("\u3010\u4e2d\u734e\u7279\u6548\u3011 Scatter");
        var scatterData = this.data.resultList[this.resultListIndex].scatter;
        var _winPositstion = scatterData.scatterPos;
        this.slotReelManager.allSymbolPlayIdleEffect();
        this.slotReelManager.showSymbolReWardEffect(_winPositstion, true);
        AudioManager_1.AudioManager.instance.playAudioEvent("GotFreeGame");
        this.spinStatus = SPIN_STATUS.SCATTER_WIN_ING;
      };
      G57_Game.prototype.scatterWinEffecting = function() {
        this.slotReelManager.isRewardEffecting() || (this.spinStatus = SPIN_STATUS.FINISHED_WAIT);
      };
      G57_Game.prototype.winScoreEffectStart = function() {
        console.log("\u3010 \u8d0f\u9322\u6548\u679c \u3011\u958b\u59cb");
        this.spinStatus = SPIN_STATUS.WIN_SCORE_EFFECT_ING;
        var _totalWinScore = this.data.getClientMoney(this.data.resultList[this.resultListIndex].totalWinBonus);
        var _betScore = this.data.playerBetScore;
        var _isFreeGame = this.gameStatus === GAME_STATUS.FREE_GAME;
        this.ui.showWinScoreEffect(_totalWinScore, _betScore, _isFreeGame);
      };
      G57_Game.prototype.winScoreEffectIng = function() {
        this.ui.isWinScoreEffecting() || (this.spinStatus = SPIN_STATUS.WIN_SCORE_EFFECT_END);
      };
      G57_Game.prototype.winScoreEffectEnd = function() {
        console.log("\u3010 \u8d0f\u9322\u6548\u679c \u3011\u7d50\u675f");
        if (this.gameStatus == GAME_STATUS.FREE_GAME_WIN_SCORE_EFFECT) this.freeGameWinScoreEffectStatus = FREEGAME_WIN_SCORE_EFFECT_STATUS.WIN_SCORE_EFFECT_END; else {
          if (this.gameStatus == GAME_STATUS.FREE_GAME) {
            this.spinStatus = SPIN_STATUS.CHECK_FREE_GAME_COLLECT;
            AudioManager_1.AudioManager.instance.playAudioEvent("CloseWinScorePanelFreeGame");
          } else {
            this.spinStatus = SPIN_STATUS.FINISHED_WAIT;
            AudioManager_1.AudioManager.instance.playAudioEvent("CloseWinScorePanel");
          }
          this.ui.closeWinScoreEffect();
        }
      };
      G57_Game.prototype.checkFreeGameCollect = function() {
        console.log("\u6aa2\u67e5Collect\u7279\u6548");
        var isGetFreeSpins = this.data.resultList[this.resultListIndex].isGetFreeSpins;
        this.spinStatus = isGetFreeSpins ? SPIN_STATUS.FREE_GAME_COLLECT_START : SPIN_STATUS.FINISHED_WAIT;
      };
      G57_Game.prototype.freeGameCollectStart = function() {
        console.log("\u64a5\u653efree game Collect\u7279\u6548");
        AudioManager_1.AudioManager.instance.playAudioEvent("FreeGameScatter");
        var GET_SPINS = this.data.resultList[this.resultListIndex].scatter.getSpins;
        var scatterPos = this.data.resultList[this.resultListIndex].scatter.scatterPos;
        this.slotReelManager.allSymbolPlayIdleEffect();
        this.slotReelManager.showSymbolReWardEffect(scatterPos, true);
        this.data.remainingFreeGameCount += GET_SPINS;
        this.ui.showFreeGameCollctEffect(this.slotReelManager.node, this.slotReelManager.getSymbolReWardPos(scatterPos), this.data.remainingFreeGameCount);
        this.spinStatus = SPIN_STATUS.FREE_GAME_COLLECT_ING;
      };
      G57_Game.prototype.freeGameCollectEffecting = function() {
        this.ui.isFreeGameCollctEffecting || (this.spinStatus = SPIN_STATUS.FINISHED_WAIT);
      };
      G57_Game.prototype.spinFinishedWait = function(p_dt) {
        if (0 == this.tempWaitTime) {
          var _totalWinScore = this.data.getClientMoney(this.data.resultList[this.resultListIndex].totalWinBonus);
          this.data.isFaseSpin ? this.tempSpinFinishedStopTime = this.fastSpinFinishedStopTime : this.tempSpinFinishedStopTime = this.normalSpinFinishedStopTime;
          if (_totalWinScore > 0) {
            AudioManager_1.AudioManager.instance.playAudioEvent("WinScore");
            this.data.isAutoSpin || this.gameStatus == GAME_STATUS.FREE_GAME ? this.tempSpinFinishedStopTime += this.autoSpinUpdateTipBroadEffectTime : this.tempSpinFinishedStopTime += this.normalUpdateTipBroadEffectTime;
          }
        }
        this.tempWaitTime += p_dt;
        if (this.tempWaitTime > this.tempSpinFinishedStopTime) {
          this.spinStatus = SPIN_STATUS.FINISHED;
          this.tempWaitTime = 0;
        }
      };
      G57_Game.prototype.spinFinished = function() {
        var _this = this;
        console.log("\u3010Spin\u3011 \u7d50\u675f");
        if (this.gameStatus == GAME_STATUS.FREE_GAME) if (this.hasNextResultList()) {
          this.resultListIndex++;
          this.freeGameOddsIndex = 0;
          this.ui.changeFreeGameOddsLabel(this.freeGameOddsIndex);
          this.slotReelManager.allSymbolPlayIdleEffect();
          this.spinStatus = SPIN_STATUS.REEL_SCROLL_START;
        } else {
          this.ui.hideFreeGameOddsTip();
          console.log("\u958b\u555f\u514d\u8cbb\u904a\u6232\u7d50\u7b97\u9762\u677f");
          AudioManager_1.AudioManager.instance.playAudioEvent("OpenFreeGameGetScorePanel");
          var _freeGameWinMoney = this.data.totalBonus - this.data.getClientMoney(this.data.resultList[0].totalWinBonus);
          var _betScore = this.data.playerBetScore;
          this.ui.openFreeGameGetScorePanel(_freeGameWinMoney, _betScore);
          this.ui.setSpinArrowSpinningSpeed(Slot_GameUI_1.SPIN_ARROW_SPINNIG_TYPE.NORMAL);
          this.gameStatus = GAME_STATUS.FREE_GAME_REWARD;
          this.spinStatus = SPIN_STATUS.NONE;
          setTimeout(function() {
            _this.gameStatus == GAME_STATUS.FREE_GAME_REWARD && _this.freeGameEnd();
          }, 5e3);
        } else if (this.hasFreeGame()) cc.tween(this.node).call(function() {
          _this.gameStatus = GAME_STATUS.FREE_GAME_TIP;
        }).delay(this.scatterShowTime).call(function() {
          var END_TIME = 17800;
          var DEFAULT_MODE = 4;
          console.log("\u958b\u8d77\u514d\u8cbb\u904a\u6232\u9762\u677f");
          _this.ui.openFreeGamePanel(END_TIME);
          _this.ui.changeToFreeGameBackGround();
          _this.slotReelManager.allSymbolPlayIdleEffect();
          _this.selectFreeGameTimeout = window.setTimeout(function() {
            if (_this.gameStatus == GAME_STATUS.FREE_GAME_TIP) {
              AudioManager_1.AudioManager.instance.playAudioEvent("StartFreeGameClickAuto");
              _this.ui.autoSelectFreeGameMode(DEFAULT_MODE);
              _this.sendOnChoiceFreeGameEvent(DEFAULT_MODE);
            }
          }, END_TIME);
        }).start(); else if (this.data.jackpotData) {
          this.gameStatus = GAME_STATUS.JACKPOT_WIN_SCORE;
          this.spinStatus = SPIN_STATUS.JACKPOT_WIN_START;
        } else {
          this.gameStatus = GAME_STATUS.IDLE;
          this.spinStatus = SPIN_STATUS.NONE;
          this.checkAutoGameContinue();
        }
      };
      G57_Game.prototype.freeGameWinScoreEffectStart = function() {
        console.log("\u3010\u514d\u8cbb\u904a\u6232 \u8d0f\u9322\u6548\u679c \u3011\u958b\u59cb");
        this.freeGameWinScoreEffectStatus = FREEGAME_WIN_SCORE_EFFECT_STATUS.WIN_SCORE_EFFECT_ING;
        var _totalWinScore = this.data.totalBonus;
        var _betScore = this.data.playerBetScore;
        var _isFreeGame = false;
        this.ui.showWinScoreEffect(_totalWinScore, _betScore, _isFreeGame);
      };
      G57_Game.prototype.freeGameWinScoreEffectIng = function() {
        this.ui.isWinScoreEffecting() || (this.freeGameWinScoreEffectStatus = FREEGAME_WIN_SCORE_EFFECT_STATUS.WIN_SCORE_EFFECT_END);
      };
      G57_Game.prototype.freeGameWinScoreEffectEnd = function() {
        console.log("\u3010\u514d\u8cbb\u904a\u6232 \u8d0f\u9322\u6548\u679c \u3011\u7d50\u675f");
        this.freeGameWinScoreEffectStatus = FREEGAME_WIN_SCORE_EFFECT_STATUS.NONE;
        if (this.data.jackpotData) {
          this.gameStatus = GAME_STATUS.JACKPOT_WIN_SCORE;
          this.spinStatus = SPIN_STATUS.JACKPOT_WIN_START;
        } else {
          this.gameStatus = GAME_STATUS.IDLE;
          this.spinStatus = SPIN_STATUS.FINISHED_WAIT;
          this.checkAutoGameContinue();
        }
        AudioManager_1.AudioManager.instance.playAudioEvent("CloseWinScorePanel");
        this.ui.closeWinScoreEffect();
      };
      G57_Game.prototype.getWinPos = function(p_winLineIndex) {
        return this.data.resultList[this.resultListIndex].winWayList[p_winLineIndex].singlePos;
      };
      G57_Game.prototype.getTotalWinPos = function() {
        var _winPos = this.data.resultList[this.resultListIndex].totalHitPos;
        var _scatterWinPos = this.data.resultList[this.resultListIndex].scatter.scatterPos;
        if (this.data.resultList[this.resultListIndex].scatter.count >= 5) for (var i = 0; i < 5; i++) for (var j = 0; j < 3; j++) 1 == _scatterWinPos[i][j] && (_winPos[i][j] = 0);
        return _winPos;
      };
      G57_Game.prototype.TakeTurnsAllWinLine = function(p_dt) {
        this.tempWinLineChangeTime += p_dt;
        this.data.resultList[this.resultListIndex].winWayList.sort(function(a, b) {
          return a.winNum - b.winNum;
        });
        var _winLineCount = this.data.resultList[this.resultListIndex].winWayList.length;
        if (1 == _winLineCount && 1 == this.nowShowWinLine) return;
        if (this.tempWinLineChangeTime > this.winLineChangeTime + this.winLineChangeWaitTime) {
          console.log("\u8f2a\u6d41\u64ad\u653e\u6240\u6709\u7684\u4e2d\u734e\u9023\u7dda");
          var _winPositstion = null;
          this.tempWinLineChangeTime = 0;
          this.nowShowWinLine > _winLineCount && (this.nowShowWinLine = 0);
          if (this.nowShowWinLine == _winLineCount) _winPositstion = this.getTotalWinPos(); else {
            _winPositstion = this.getWinPos(this.nowShowWinLine);
            var _wayScore = this.data.resultList[this.resultListIndex].winWayList[this.nowShowWinLine].winBonus.toString();
            this.slotReelManager.showScoreReWardEffect(_winPositstion, this.data.getClientMoney(_wayScore).toString());
          }
          this.slotReelManager.showSymbolReWardEffect(_winPositstion, true);
          AudioManager_1.AudioManager.instance.playAudioEvent("ShowOneWinLine");
          this.nowShowWinLine++;
        } else if (this.tempWinLineChangeTime > this.winLineChangeTime) {
          this.slotReelManager.allSymbolPlayIdleEffect();
          this.slotReelManager.SetAllSymbolZOrder(0);
          this.slotReelManager.hideScoreReWardEffect();
        }
      };
      G57_Game.prototype.hasNextResultList = function() {
        if (this.resultListIndex < this.data.resultList.length - 1) return true;
        return false;
      };
      G57_Game.prototype.hasFreeGame = function() {
        if (this.data.resultList[0].isGetFreeSpins) return true;
        return false;
      };
      G57_Game.prototype.setReelInfo = function(p_data) {
        this.data.reelStartIndex = p_data.position;
        this.data.reelTable_BG = p_data.reels.BG;
        this.data.reelTable_ALLFG = p_data.reels.FG;
        this.slotReelManager.setReelTable(this.data.reelTable_BG);
        this.slotReelManager.setReelStartIndex(this.data.reelStartIndex);
      };
      G57_Game.prototype.playerLogin = function() {
        this.data.betOddsStartIndex = this.data.getBetIndex();
        var _money = this.data.coin;
        var _betMoney = this.data.getClientMoney(this.data.betOdds[this.data.betOddsStartIndex] * this.data.oneOddsScore);
        this.ui.setScore(_money);
        this.ui.setBetScore(_betMoney);
        this.ui.setWinScore(0);
      };
      G57_Game.prototype.onFreeGameModeSelect = function(event, mode) {
        AudioManager_1.AudioManager.instance.playAudioEvent("StartFreeGameClick");
        this.sendOnChoiceFreeGameEvent(Number(mode));
      };
      G57_Game.prototype.sendOnChoiceFreeGameEvent = function(mode) {
        var _this = this;
        var ARR_FREE_GAME_TIMES = [ 1, 3, 5, 8, 12 ];
        clearTimeout(this.selectFreeGameTimeout);
        this.socket.setOnChoiceFreeGameEvent(mode).then(function(response) {
          _this.selectMode = mode;
          _this.data.resultList = response.resultList;
          _this.data.remainingFreeGameCount = ARR_FREE_GAME_TIMES[mode];
          _this.data.coin = _this.data.getClientMoney(response.currentCash);
          _this.data.totalBonus = _this.data.getClientMoney(response.totalBonus);
          _this.scheduleOnce(function() {
            _this.startFreeGame();
          }, 1);
        }).catch(function(error) {
          error.code == GameClient.errCode.RESPONSE_TIMED_OUT && _this.socket.showErrorAlert(LocalizationManager_1.default.getInstance().get("ERROR_SOCKET_TIME_OUT"), null, function() {
            G57_SocketManager_1.default.getInstance().login();
          });
        });
      };
      __decorate([ property({
        type: G57_SlotReelManager_1.default,
        override: true
      }) ], G57_Game.prototype, "slotReelManager", void 0);
      __decorate([ property({
        type: G57_GameUI_1.default,
        override: true
      }) ], G57_Game.prototype, "ui", void 0);
      __decorate([ property({
        type: cc.Float,
        displayName: "\u4e2d\u734e\u7b2c\u4e00\u6b21\u63db\u7dda\u7684\u6642\u9593"
      }) ], G57_Game.prototype, "firstWinLineChangeTime", void 0);
      __decorate([ property({
        type: cc.Float,
        displayName: "\u4e2d\u734e\u7dda\u66f4\u63db\u6642\u9593"
      }) ], G57_Game.prototype, "winLineChangeTime", void 0);
      __decorate([ property({
        type: cc.Float,
        displayName: "\u4e2d\u734e\u7dda\u66f4\u63db\u7b49\u5f85\u6642\u9593"
      }) ], G57_Game.prototype, "winLineChangeWaitTime", void 0);
      __decorate([ property({
        type: cc.Float,
        displayName: "spin\u51b7\u537b\u6642\u9593",
        tooltip: "\u958b\u59cbSpin\u5f8c\u7684\u51b7\u537b\u6642\u9593\uff0c\u54ea\u4f86\u8b93\u6efe\u8f2a\u6703\u7a0d\u5fae\u6efe\u52d5\u5728\u505c\u6b62"
      }) ], G57_Game.prototype, "spinCD", void 0);
      __decorate([ property({
        type: cc.Float,
        displayName: "\u4e00\u822cspin\u7d50\u675f\u505c\u6b62\u6642\u9593"
      }) ], G57_Game.prototype, "normalSpinFinishedStopTime", void 0);
      __decorate([ property({
        type: cc.Float,
        displayName: "\u5feb\u901fspin\u7d50\u675f\u505c\u6b62\u6642\u9593"
      }) ], G57_Game.prototype, "fastSpinFinishedStopTime", void 0);
      __decorate([ property({
        type: cc.Float,
        displayName: "\u624b\u52d5Spin \u4e2d\u734e\u66f4\u65b0\u63d0\u793a\u9762\u677f\u7279\u6548\u6642\u9593",
        tooltip: "\u53ea\u6709\u5728\u4e2d\u734e\u7684\u6642\u5019\u624d\u6703\u5403\u9019\u500b\u53c3\u6578"
      }) ], G57_Game.prototype, "normalUpdateTipBroadEffectTime", void 0);
      __decorate([ property({
        type: cc.Float,
        displayName: "\u81ea\u52d5Spin \u4e2d\u734e\u66f4\u65b0\u63d0\u793a\u9762\u677f\u7279\u6548\u6642\u9593",
        tooltip: "\u53ea\u6709\u5728\u4e2d\u734e\u7684\u6642\u5019\u624d\u6703\u5403\u9019\u500b\u53c3\u6578"
      }) ], G57_Game.prototype, "autoSpinUpdateTipBroadEffectTime", void 0);
      __decorate([ property({
        type: cc.Float,
        displayName: "\u624b\u52d5Spin\u4e2d\u734e\u6642\u9593"
      }) ], G57_Game.prototype, "normalRewardTime", void 0);
      __decorate([ property({
        type: cc.Float,
        displayName: "\u81ea\u52d5Spin\u4e2d\u734e\u6642\u9593"
      }) ], G57_Game.prototype, "autoRewardTime", void 0);
      __decorate([ property({
        type: cc.Float,
        displayName: "scatter\u986f\u793a\u7684\u6642\u9593"
      }) ], G57_Game.prototype, "scatterShowTime", void 0);
      __decorate([ property({
        type: cc.Float,
        displayName: "\u51fa\u73fe\u9810\u5146\u7684Scatter\u6578\u91cf"
      }) ], G57_Game.prototype, "showOmenScatterCount", void 0);
      G57_Game = __decorate([ ccclass ], G57_Game);
      return G57_Game;
    }(Slot_GameManager_1.default);
    exports.default = G57_Game;
    cc._RF.pop();
  }, {
    "../../../Common/Tools/AudioManager/AudioManager": void 0,
    "../../../Common/Tools/CommonTool": void 0,
    "../../../Common/Tools/Localization/LocalizationManager": void 0,
    "../../../SlotFramework/Game/Data/Slot_GameManager_InitData": void 0,
    "../../../SlotFramework/Game/Data/Slot_GameUI_InitData": void 0,
    "../../../SlotFramework/Game/Data/Slot_ReelManager_InitData": void 0,
    "../../../SlotFramework/Game/Data/Slot_ReelManager_ScrollData": void 0,
    "../../../SlotFramework/Game/Slot_GameManager": void 0,
    "../../../SlotFramework/Game/view/Slot_GameUI": void 0,
    "../../../SlotFramework/Slot_DataManager": void 0,
    "../../Common/script/G57_DataManager": "G57_DataManager",
    "../../Common/script/G57_DynamicPopUpPanelManager": "G57_DynamicPopUpPanelManager",
    "../../Common/script/socket/G57_SocketManager": "G57_SocketManager",
    "./view/G57_GameUI": "G57_GameUI",
    "./view/G57_SlotReelManager": "G57_SlotReelManager"
  } ],
  G57_Language: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "42b97plldZFarSChsqPe1W4", "G57_Language");
    "use strict";
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    exports.G57_Language = void 0;
    exports.G57_Language = {
      "zh-cn": {
        Slot_Score: "\u53ef\u7528\u5206\u6570",
        Slot_WinScore: "\u8d62\u5206",
        Slot_BetScore: "\u62bc\u6ce8\u5206\u6570",
        Fail_Bet_Odds_Message: "\u4e0b\u6ce8\u64cd\u4f5c\u5931\u8d25\n\u670d\u52a1\u5668\u56de\u5e94\u903e\u65f6",
        RulePage_1_1: "\u70b9\u51fb\u52a0\u51cf\u53f7\u6309\u94ae\uff0c\u8c03\u6574\u6295\u6ce8\u7684\u603b\u989d",
        RulePage_1_2: "\u70b9\u51fb\u65cb\u8f6c\u6309\u94ae\u4ee5\u5f53\u524d\u7684\u6295\u6ce8\u65cb\u8f6c\u8f6c\u8f74",
        RulePage_1_3: "\u70b9\u51fb\u81ea\u52a8\u6e38\u620f\u6309\u94ae\u4f1a\u5f00\u542f\u81ea\u52a8\u6e38\u620f\u8bbe\u7f6e\uff0c\u81ea\u52a8\u6e38\u620f\u542f\u7528\u65f6\uff0c\u70b9\u51fb\u65cb\u8f6c\u6309\u94ae\u53ef\u505c\u6b62\u81ea\u52a8\u6a21\u5f0f",
        RulePage_1_4: "\u70b9\u51fb\u6781\u901f\u6e38\u620f\u6309\u94ae\uff0c\u6309\u94ae\u4eae\u8d77\u65f6\u53ef\u5f00\u542f\u6781\u901f\u6a21\u5f0f\uff0c\u6781\u901f\u6a21\u5f0f\u53ef\u4e0e\u81ea\u52a8\u6e38\u620f\u540c\u65f6\u751f\u6548",
        RulePage_1_6: "A\uff1d\u6295\u6ce8\u603b\u989d\nB\uff1d\u6295\u6ce8\u7ebf\u6570\uff08\u672c\u6e38\u620f\u4ee550\u7ebf\u8ba1\u7b97\uff09\nC\uff1d\u56fe\u6807\u8fde\u7ebf\u8d54\u7387\u500d\u6570\uff08\u8bf7\u53c2\u8003\u8d54\u7387\u8868\uff09\nD\uff1d\u514d\u8d39\u6e38\u620f\u500d\u7387\uff08\u514d\u8d39\u6e38\u620f\u9636\u6bb5\u624d\u8ba1\u7b97\uff09",
        RulePage_1_7: "1\u7ebf\u4e2d\u5956\u91d1\u989d\uff1d(A/B)*C*D",
        RulePage_1_8: "\u6e38\u620f\u51fa\u73b0\u6545\u969c\u65f6\uff0c\u6240\u6709\u8d54\u4ed8\u548c\u6e38\u620f\u90fd\u89c6\u4e3a\u65e0\u6548",
        RulePage_2_1: "\u3000\u3000\u3000\u56fe\u6807\u53ef\u66ff\u4ee3\u9664\u4e86\u3000\u3000\u3000\u4ee5\u5916\u7684\u6240\u6709\u56fe\u6807",
        RulePage_2_2: "\u767e\u642d",
        RulePage_2_3: "\u5206\u6563\n\u5956\u56fe",
        RulePage_3_1: "5\u4e2a\u6eda\u8f6e\u5185\uff0c\u90fd\u51fa\u73b0\u5206\u6563\u5956\u56fe\u5373\u53ef\u8fdb\u5165\u514d\u8d39\u6e38\u620f",
        RulePage_3_2: "\u514d\u8d39\u6e38\u620f\u6240\u6709\u5f97\u5206\u4f1a\u989d\u5916\u4e58\u4e0a\u4e00\u4e2a\u5956\u52b1\u500d\u6570",
        RulePage_3_3: "\u89e6\u53d1\u514d\u8d39\u6e38\u620f\u7684\u3000\u3000\u3000\u8d8a\u591a\uff0c\u5956\u52b1\u500d\u6570\u8d8a\u9ad8",
        RulePage_3_4: "5\u3000\u3000\u3000\uff1a\u5956\u52b1\u500d\u6570\u989d\u5916X1",
        RulePage_3_5: "6\u3000\u3000\u3000\uff1a\u5956\u52b1\u500d\u6570\u989d\u5916X2",
        RulePage_3_6: "7\u3000\u3000\u3000\uff1a\u5956\u52b1\u500d\u6570\u989d\u5916X3",
        RulePage_4_1: "5\u4e2a\u6a21\u5f0f\u7684\u514d\u8d39\u6e38\u620f\uff0c\u514d\u8d39\u65cb\u8f6c\u6b21\u6570\u8d8a\u5c11\uff0c\u8d77\u59cb\u5956\u52b1\u500d\u6570\u8d8a\u9ad8\uff08\u82e5\u73a9\u5bb6\u65ad\u7ebf\u5c06\u9ed8\u8ba4\u9009\u62e9\u6a21\u5f0f5\uff09",
        RulePage_4_2: "<b><color=#fff600>\u6a21\u5f0f1\uff1a</c>1\u6b21\u65cb\u8f6c\uff0c\u5956\u52b1\u500d\u657050\u500d\u5f00\u59cb\uff0c\u6700\u591a3\u6b21\u514d\u8d39\u65cb\u8f6c</b>",
        RulePage_4_3: "<b><color=#fff600>\u6a21\u5f0f2\uff1a</c>3\u6b21\u65cb\u8f6c\uff0c\u5956\u52b1\u500d\u6570\u4ece12\u500d\u5f00\u59cb\uff0c\u6bcf\u4e00\u6b21\u65cb\u8f6c\u5956\u52b1\u500d\u6570+12\uff0c\u6700\u591a9\u6b21\u514d\u8d39\u65cb\u8f6c</b>",
        RulePage_4_4: "<b><color=#fff600>\u6a21\u5f0f3\uff1a</c>5\u6b21\u65cb\u8f6c\uff0c\u5956\u52b1\u500d\u6570\u4ece5\u500d\u5f00\u59cb\uff0c\u6bcf\u4e00\u6b21\u65cb\u8f6c\u5956\u52b1\u500d\u6570+5\uff0c\u6700\u591a15\u6b21\u514d\u8d39\u65cb\u8f6c</b>",
        RulePage_4_5: "<b><color=#fff600>\u6a21\u5f0f4\uff1a</c>8\u6b21\u65cb\u8f6c\uff0c\u5956\u52b1\u500d\u6570\u4ece2\u500d\u5f00\u59cb\uff0c\u6bcf\u4e00\u6b21\u65cb\u8f6c\u5956\u52b1\u500d\u6570+2\uff0c\u6700\u591a24\u6b21\u514d\u8d39\u65cb\u8f6c</b>",
        RulePage_4_6: "<b><color=#fff600>\u6a21\u5f0f5\uff1a</c>12\u6b21\u65cb\u8f6c\uff0c\u5956\u52b1\u500d\u6570\u4ece1\u500d\u5f00\u59cb\uff0c\u6bcf\u4e00\u6b21\u65cb\u8f6c\u5956\u52b1\u500d\u6570+1\uff0c\u6700\u591a50\u6b21\u514d\u8d39\u65cb\u8f6c</b>",
        RulePage_4_7: "\u514d\u8d39\u6e38\u620f\u4e2d5\u4e2a\u6eda\u8f6e\u90fd\u51fa\u73b0\u3000\u3000\u3000\u5373\u53ef\u4ee5\u83b7\u5f97\u989d\u5916\u7684\u514d\u8d39\u65cb\u8f6c",
        RulePage_4_8: "\u518d\u6b21\u89e6\u53d1\u514d\u8d39\u6e38\u620f\u65f6\uff0c\u83b7\u5f97\u573a\u6b21\u548c\u5956\u52b1\u500d\u6570\u4e0e\u89e6\u53d1\u514d\u8d39\u6e38\u620f\u65f6\u76f8\u540c",
        RulePage_5_1: "\u6eda\u8f6e\u4e0a\u7531\u6700\u5de6\u81f3\u53f3\u8fde\u7eed\u51fa\u73b0\u4e09\u4e2a\u4ee5\u4e0a\u7684\u76f8\u540c\u56fe\u6807\uff0c\u5373\u53ef\u83b7\u5f97\u8be5\u56fe\u793a\u7684\u5f97\u5206\u5956\u52b1",
        SymbolTip_Scatter: "5\u4e2a\u6eda\u8f6e\u5185\uff0c\u90fd\u51fa\u73b0\u5206\u6563\u56fe\u6807\u5373\u53ef\u8fdb\u5165\u514d\u8d39\u6e38\u620f",
        SymbolTip_Wild: "\u767e\u642d\u56fe\u6807\u53ef\n\u4ee3\u66ff\u9664\u5206\u6563\n\u5956\u56fe\u5916\u7684\u6240\n\u6709\u56fe\u6807",
        TEXT_FREEGAME_GET_REWARD: "\u9886\u5956",
        TEXT_AUTOGAMEPANEL_START: "\u5f00\u59cb\u81ea\u52a8\u65cb\u8f6c"
      },
      "zh-tw": {
        Slot_Score: "\u53ef\u7528\u5206\u6578",
        Slot_WinScore: "\u8d0f\u5206",
        Slot_BetScore: "\u62bc\u6ce8\u5206\u6578",
        Fail_Bet_Odds_Message: "\u4e0b\u6ce8\u64cd\u4f5c\u5931\u6557\n\u670d\u52d9\u5668\u56de\u61c9\u903e\u6642",
        RulePage_1_1: "\u9ede\u64ca\u52a0\u6e1b\u865f\u6309\u9215\uff0c\u8abf\u6574\u6295\u6ce8\u7684\u7e3d\u984d",
        RulePage_1_2: "\u9ede\u64ca\u65cb\u8f49\u6309\u9215\u4ee5\u7576\u524d\u7684\u6295\u6ce8\u65cb\u8f49\u8f49\u8ef8",
        RulePage_1_3: "\u9ede\u64ca\u81ea\u52d5\u904a\u6232\u6309\u9215\u6703\u958b\u555f\u81ea\u52d5\u904a\u6232\u8a2d\u7f6e\uff0c\u81ea\u52d5\u904a\u6232\u555f\u7528\u6642\uff0c\u9ede\u64ca\u65cb\u8f49\u6309\u9215\u53ef\u505c\u6b62\u81ea\u52d5\u6a21\u5f0f",
        RulePage_1_4: "\u9ede\u64ca\u6975\u901f\u904a\u6232\u6309\u9215\uff0c\u6309\u9215\u4eae\u8d77\u6642\u53ef\u958b\u555f\u6975\u901f\u6a21\u5f0f\uff0c\u6975\u901f\u6a21\u5f0f\u53ef\u8207\u81ea\u52d5\u904a\u6232\u540c\u6642\u751f\u6548",
        RulePage_1_6: "A\uff1d\u6295\u6ce8\u7e3d\u984d\nB\uff1d\u6295\u6ce8\u7dda\u6578\uff08\u672c\u904a\u6232\u4ee550\u7dda\u8a08\u7b97\uff09\nC\uff1d\u5716\u6a19\u9023\u7dda\u8ce0\u7387\u500d\u6578\uff08\u8acb\u53c3\u8003\u8ce0\u7387\u8868\uff09\nD\uff1d\u514d\u8cbb\u904a\u6232\u500d\u7387\uff08\u514d\u8cbb\u904a\u6232\u968e\u6bb5\u624d\u8a08\u7b97\uff09",
        RulePage_1_7: "1\u7dda\u4e2d\u734e\u91d1\u984d\uff1d(A/B)*C*D",
        RulePage_1_8: "\u904a\u6232\u51fa\u73fe\u6545\u969c\u6642\uff0c\u6240\u6709\u8ce0\u4ed8\u548c\u904a\u6232\u90fd\u8996\u70ba\u7121\u6548",
        RulePage_2_1: "\u3000\u3000\u3000\u5716\u6a19\u53ef\u66ff\u4ee3\u9664\u4e86\u3000\u3000\u3000\u4ee5\u5916\u7684\u6240\u6709\u5716\u6a19",
        RulePage_2_2: "\u767e\u642d",
        RulePage_2_3: "\u5206\u6563\n\u734e\u5716",
        RulePage_3_1: "5\u500b\u6efe\u8f2a\u5167\uff0c\u90fd\u51fa\u73fe\u5206\u6563\u734e\u5716\u5373\u53ef\u9032\u5165\u514d\u8cbb\u904a\u6232",
        RulePage_3_2: "\u514d\u8cbb\u904a\u6232\u6240\u6709\u5f97\u5206\u6703\u984d\u5916\u4e58\u4e0a\u4e00\u500b\u734e\u52f5\u500d\u6578",
        RulePage_3_3: "\u89f8\u767c\u514d\u8cbb\u904a\u6232\u7684\u3000\u3000\u3000\u8d8a\u591a\uff0c\u734e\u52f5\u500d\u6578\u8d8a\u9ad8",
        RulePage_3_4: "5\u3000\u3000\u3000\uff1a\u734e\u52f5\u500d\u6578\u984d\u5916X1",
        RulePage_3_5: "6\u3000\u3000\u3000\uff1a\u734e\u52f5\u500d\u6578\u984d\u5916X2",
        RulePage_3_6: "7\u3000\u3000\u3000\uff1a\u734e\u52f5\u500d\u6578\u984d\u5916X3",
        RulePage_4_1: "5\u500b\u6a21\u5f0f\u7684\u514d\u8cbb\u904a\u6232\uff0c\u514d\u8cbb\u65cb\u8f49\u6b21\u6578\u8d8a\u5c11\uff0c\u8d77\u59cb\u734e\u52f5\u500d\u6578\u8d8a\u9ad8\uff08\u82e5\u73a9\u5bb6\u65b7\u7dda\u5c07\u9ed8\u8a8d\u9078\u64c7\u6a21\u5f0f5\uff09",
        RulePage_4_2: "<b><color=#fff600>\u6a21\u5f0f1\uff1a</c>1\u6b21\u65cb\u8f49\uff0c\u734e\u52f5\u500d\u657850\u500d\u958b\u59cb\uff0c\u6700\u591a3\u6b21\u514d\u8cbb\u65cb\u8f49</b>",
        RulePage_4_3: "<b><color=#fff600>\u6a21\u5f0f2\uff1a</c>3\u6b21\u65cb\u8f49\uff0c\u734e\u52f5\u500d\u6578\u5f9e12\u500d\u958b\u59cb\uff0c\u6bcf\u4e00\u6b21\u65cb\u8f49\u734e\u52f5\u500d\u6578+12\uff0c\u6700\u591a9\u6b21\u514d\u8cbb\u65cb\u8f49</b>",
        RulePage_4_4: "<b><color=#fff600>\u6a21\u5f0f3\uff1a</c>5\u6b21\u65cb\u8f49\uff0c\u734e\u52f5\u500d\u6578\u5f9e5\u500d\u958b\u59cb\uff0c\u6bcf\u4e00\u6b21\u65cb\u8f49\u734e\u52f5\u500d\u6578+5\uff0c\u6700\u591a15\u6b21\u514d\u8cbb\u65cb\u8f49</b>",
        RulePage_4_5: "<b><color=#fff600>\u6a21\u5f0f4\uff1a</c>8\u6b21\u65cb\u8f49\uff0c\u734e\u52f5\u500d\u6578\u5f9e2\u500d\u958b\u59cb\uff0c\u6bcf\u4e00\u6b21\u65cb\u8f49\u734e\u52f5\u500d\u6578+2\uff0c\u6700\u591a24\u6b21\u514d\u8cbb\u65cb\u8f49</b>",
        RulePage_4_6: "<b><color=#fff600>\u6a21\u5f0f5\uff1a</c>12\u6b21\u65cb\u8f49\uff0c\u734e\u52f5\u500d\u6578\u5f9e1\u500d\u958b\u59cb\uff0c\u6bcf\u4e00\u6b21\u65cb\u8f49\u734e\u52f5\u500d\u6578+1\uff0c\u6700\u591a50\u6b21\u514d\u8cbb\u65cb\u8f49</b>",
        RulePage_4_7: "\u514d\u8cbb\u904a\u6232\u4e2d5\u500b\u6efe\u8f2a\u90fd\u51fa\u73fe\u3000\u3000\u3000\u5373\u53ef\u4ee5\u7372\u5f97\u984d\u5916\u7684\u514d\u8cbb\u65cb\u8f49",
        RulePage_4_8: "\u518d\u6b21\u89f8\u767c\u514d\u8cbb\u904a\u6232\u6642\uff0c\u7372\u5f97\u5834\u6b21\u548c\u734e\u52f5\u500d\u6578\u8207\u89f8\u767c\u514d\u8cbb\u904a\u6232\u6642\u76f8\u540c",
        RulePage_5_1: "\u6efe\u8f2a\u4e0a\u7531\u6700\u5de6\u81f3\u53f3\u9023\u7e8c\u51fa\u73fe\u4e09\u500b\u4ee5\u4e0a\u7684\u76f8\u540c\u5716\u6a19\uff0c\u5373\u53ef\u7372\u5f97\u8a72\u5716\u793a\u7684\u5f97\u5206\u734e\u52f5",
        SymbolTip_Scatter: "5\u500b\u6efe\u8f2a\u5167\uff0c\u90fd\u51fa\u73fe\u5206\u6563\u5716\u6a19\u5373\u53ef\u9032\u5165\u514d\u8cbb\u904a\u6232",
        SymbolTip_Wild: "\u767e\u642d\u5716\u6a19\u53ef\n\u4ee3\u66ff\u9664\u5206\u6563\n\u734e\u5716\u5916\u7684\u6240\n\u6709\u5716\u6a19",
        TEXT_FREEGAME_GET_REWARD: "\u9818\u734e",
        TEXT_AUTOGAMEPANEL_START: "\u958b\u59cb\u81ea\u52d5\u65cb\u8f49"
      },
      "en-us": {
        Slot_Score: "Game coins",
        Slot_WinScore: "Win",
        Slot_BetScore: "Bet",
        Fail_Bet_Odds_Message: "Bet failed\nRequest Timeout",
        RulePage_1_1: "Press the button (+)(-) to change bet.",
        RulePage_1_2: "Press the circular arrow to submit the bet and spin the reels.",
        RulePage_1_3: "Press the AutoSpin button will open the AutoSpin settings.When AutoSpin active, press the SPIN button to stop.",
        RulePage_1_4: "Press the Speed button will turn on the Speed Mode.Speed Mode can be used with AutoSpin.",
        RulePage_1_6: "A\uff1dTotal Bet\nB\uff1dNumber of lines\uff08The game is 50 \uff09\nC\uff1dSymbol odds\nD\uff1dMultiple\uff08Free Game only\uff09",
        RulePage_1_7: "One line win = ( A / B ) x C x D",
        RulePage_1_8: "Malfunction Voids All Pays and Play",
        RulePage_2_1: "\u3000\u3000\u3000\u3000substitutes for all symbols except\u3000\u3000\u3000\u3000.",
        RulePage_2_2: "null",
        RulePage_2_3: "null",
        RulePage_3_1: "null",
        RulePage_3_2: "null",
        RulePage_3_3: "null",
        RulePage_3_4: "null",
        RulePage_3_5: "null",
        RulePage_3_6: "null",
        RulePage_4_1: "null",
        RulePage_4_2: "null",
        RulePage_4_3: "null",
        RulePage_4_4: "null",
        RulePage_4_5: "null",
        RulePage_4_6: "null",
        RulePage_4_7: "null",
        RulePage_4_8: "null",
        RulePage_5_1: "null",
        SymbolTip_Scatter: "Three, four, five SCATTER will trigger Free Spins.",
        SymbolTip_Wild: "WILD Substitutes for All Symbols Except SCATTER",
        TEXT_FREEGAME_GET_REWARD: "null",
        TEXT_AUTOGAMEPANEL_START: "null"
      }
    };
    cc._RF.pop();
  }, {} ],
  G57_LoadingItem: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "65c87lt/pxMXK8IS8I9pwUU", "G57_LoadingItem");
    "use strict";
    var __extends = this && this.__extends || function() {
      var extendStatics = function(d, b) {
        extendStatics = Object.setPrototypeOf || {
          __proto__: []
        } instanceof Array && function(d, b) {
          d.__proto__ = b;
        } || function(d, b) {
          for (var p in b) Object.prototype.hasOwnProperty.call(b, p) && (d[p] = b[p]);
        };
        return extendStatics(d, b);
      };
      return function(d, b) {
        extendStatics(d, b);
        function __() {
          this.constructor = d;
        }
        d.prototype = null === b ? Object.create(b) : (__.prototype = b.prototype, new __());
      };
    }();
    var __decorate = this && this.__decorate || function(decorators, target, key, desc) {
      var c = arguments.length, r = c < 3 ? target : null === desc ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
      if ("object" === typeof Reflect && "function" === typeof Reflect.decorate) r = Reflect.decorate(decorators, target, key, desc); else for (var i = decorators.length - 1; i >= 0; i--) (d = decorators[i]) && (r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r);
      return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    var Slot_LoadingItem_1 = require("../../SlotFramework/Common/Slot_LoadingItem");
    var ccclass = cc._decorator.ccclass;
    var G57_LoadingItem = function(_super) {
      __extends(G57_LoadingItem, _super);
      function G57_LoadingItem() {
        return null !== _super && _super.apply(this, arguments) || this;
      }
      G57_LoadingItem = __decorate([ ccclass ], G57_LoadingItem);
      return G57_LoadingItem;
    }(Slot_LoadingItem_1.default);
    exports.default = G57_LoadingItem;
    cc._RF.pop();
  }, {
    "../../SlotFramework/Common/Slot_LoadingItem": void 0
  } ],
  G57_LoadingUI: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "dae27iFicZEArP1mHFrWtvF", "G57_LoadingUI");
    "use strict";
    var __extends = this && this.__extends || function() {
      var extendStatics = function(d, b) {
        extendStatics = Object.setPrototypeOf || {
          __proto__: []
        } instanceof Array && function(d, b) {
          d.__proto__ = b;
        } || function(d, b) {
          for (var p in b) Object.prototype.hasOwnProperty.call(b, p) && (d[p] = b[p]);
        };
        return extendStatics(d, b);
      };
      return function(d, b) {
        extendStatics(d, b);
        function __() {
          this.constructor = d;
        }
        d.prototype = null === b ? Object.create(b) : (__.prototype = b.prototype, new __());
      };
    }();
    var __decorate = this && this.__decorate || function(decorators, target, key, desc) {
      var c = arguments.length, r = c < 3 ? target : null === desc ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
      if ("object" === typeof Reflect && "function" === typeof Reflect.decorate) r = Reflect.decorate(decorators, target, key, desc); else for (var i = decorators.length - 1; i >= 0; i--) (d = decorators[i]) && (r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r);
      return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    var ccclass = cc._decorator.ccclass;
    var G57_LoadingUI = function(_super) {
      __extends(G57_LoadingUI, _super);
      function G57_LoadingUI() {
        return null !== _super && _super.apply(this, arguments) || this;
      }
      G57_LoadingUI = __decorate([ ccclass ], G57_LoadingUI);
      return G57_LoadingUI;
    }(cc.Component);
    exports.default = G57_LoadingUI;
    cc._RF.pop();
  }, {} ],
  G57_Loading_InitData: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "dbf42fbrZNEzaeT8jTKrtzc", "G57_Loading_InitData");
    "use strict";
    var __decorate = this && this.__decorate || function(decorators, target, key, desc) {
      var c = arguments.length, r = c < 3 ? target : null === desc ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
      if ("object" === typeof Reflect && "function" === typeof Reflect.decorate) r = Reflect.decorate(decorators, target, key, desc); else for (var i = decorators.length - 1; i >= 0; i--) (d = decorators[i]) && (r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r);
      return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    var ccclass = cc._decorator.ccclass;
    var G57_Loading_InitData = function() {
      function G57_Loading_InitData() {}
      G57_Loading_InitData = __decorate([ ccclass ], G57_Loading_InitData);
      return G57_Loading_InitData;
    }();
    exports.default = G57_Loading_InitData;
    cc._RF.pop();
  }, {} ],
  G57_Loading: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "7218doNHBRHcq781ebWczqi", "G57_Loading");
    "use strict";
    var __extends = this && this.__extends || function() {
      var extendStatics = function(d, b) {
        extendStatics = Object.setPrototypeOf || {
          __proto__: []
        } instanceof Array && function(d, b) {
          d.__proto__ = b;
        } || function(d, b) {
          for (var p in b) Object.prototype.hasOwnProperty.call(b, p) && (d[p] = b[p]);
        };
        return extendStatics(d, b);
      };
      return function(d, b) {
        extendStatics(d, b);
        function __() {
          this.constructor = d;
        }
        d.prototype = null === b ? Object.create(b) : (__.prototype = b.prototype, new __());
      };
    }();
    var __decorate = this && this.__decorate || function(decorators, target, key, desc) {
      var c = arguments.length, r = c < 3 ? target : null === desc ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
      if ("object" === typeof Reflect && "function" === typeof Reflect.decorate) r = Reflect.decorate(decorators, target, key, desc); else for (var i = decorators.length - 1; i >= 0; i--) (d = decorators[i]) && (r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r);
      return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __awaiter = this && this.__awaiter || function(thisArg, _arguments, P, generator) {
      function adopt(value) {
        return value instanceof P ? value : new P(function(resolve) {
          resolve(value);
        });
      }
      return new (P || (P = Promise))(function(resolve, reject) {
        function fulfilled(value) {
          try {
            step(generator.next(value));
          } catch (e) {
            reject(e);
          }
        }
        function rejected(value) {
          try {
            step(generator["throw"](value));
          } catch (e) {
            reject(e);
          }
        }
        function step(result) {
          result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected);
        }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
      });
    };
    var __generator = this && this.__generator || function(thisArg, body) {
      var _ = {
        label: 0,
        sent: function() {
          if (1 & t[0]) throw t[1];
          return t[1];
        },
        trys: [],
        ops: []
      }, f, y, t, g;
      return g = {
        next: verb(0),
        throw: verb(1),
        return: verb(2)
      }, "function" === typeof Symbol && (g[Symbol.iterator] = function() {
        return this;
      }), g;
      function verb(n) {
        return function(v) {
          return step([ n, v ]);
        };
      }
      function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
          if (f = 1, y && (t = 2 & op[0] ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 
          0) : y.next) && !(t = t.call(y, op[1])).done) return t;
          (y = 0, t) && (op = [ 2 & op[0], t.value ]);
          switch (op[0]) {
           case 0:
           case 1:
            t = op;
            break;

           case 4:
            _.label++;
            return {
              value: op[1],
              done: false
            };

           case 5:
            _.label++;
            y = op[1];
            op = [ 0 ];
            continue;

           case 7:
            op = _.ops.pop();
            _.trys.pop();
            continue;

           default:
            if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (6 === op[0] || 2 === op[0])) {
              _ = 0;
              continue;
            }
            if (3 === op[0] && (!t || op[1] > t[0] && op[1] < t[3])) {
              _.label = op[1];
              break;
            }
            if (6 === op[0] && _.label < t[1]) {
              _.label = t[1];
              t = op;
              break;
            }
            if (t && _.label < t[2]) {
              _.label = t[2];
              _.ops.push(op);
              break;
            }
            t[2] && _.ops.pop();
            _.trys.pop();
            continue;
          }
          op = body.call(thisArg, _);
        } catch (e) {
          op = [ 6, e ];
          y = 0;
        } finally {
          f = t = 0;
        }
        if (5 & op[0]) throw op[1];
        return {
          value: op[0] ? op[1] : void 0,
          done: true
        };
      }
    };
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    var AudioManager_1 = require("../../../Common/Tools/AudioManager/AudioManager");
    var DesktopBrowserTransform_1 = require("../../../Common/Tools/DesktopBrowserTransform/DesktopBrowserTransform");
    var Slot_LoadingManager_1 = require("../../../SlotFramework/Loading/Slot_LoadingManager");
    var G57_DataManager_1 = require("../../Common/script/G57_DataManager");
    var G57_DynamicPopUpPanelManager_1 = require("../../Common/script/G57_DynamicPopUpPanelManager");
    var G57_SocketManager_1 = require("../../Common/script/socket/G57_SocketManager");
    var G57_Language_1 = require("../../Localization/G57_Language");
    var ccclass = cc._decorator.ccclass;
    var PRELOAD_RESOURCE_LIST = [ "Resources" ];
    var PRELOAD_SCENE_LIST = [ "G57_Lobby", "G57_Game" ];
    var preloadResources = [ {
      bundle: "G57",
      scene: {
        path: PRELOAD_SCENE_LIST
      },
      resource: {
        path: PRELOAD_RESOURCE_LIST
      },
      audio: {
        csvPath: "Resources/Audio - G57",
        path: [ "Resources/music", "Resources/sound" ]
      },
      loadRate: .97
    } ];
    var G57_Loading = function(_super) {
      __extends(G57_Loading, _super);
      function G57_Loading() {
        var _this = null !== _super && _super.apply(this, arguments) || this;
        _this.languageMap = G57_Language_1.G57_Language;
        return _this;
      }
      G57_Loading.prototype.onLoad = function() {
        DesktopBrowserTransform_1.default.needFixScreen = true;
        _super.prototype.onLoad.call(this);
        cc.game.setFrameRate(60);
        cc.view.enableAntiAlias(true);
        cc.dynamicAtlasManager.enabled = false;
        cc.macro.ENABLE_MULTI_TOUCH = false;
      };
      G57_Loading.prototype.start = function() {
        _super.prototype.start.call(this);
        this.processAsync().catch(function(err) {
          return console.error(err);
        });
      };
      G57_Loading.prototype.processAsync = function() {
        return __awaiter(this, void 0, Promise, function() {
          return __generator(this, function(_a) {
            switch (_a.label) {
             case 0:
              AudioManager_1.AudioManager.delInstance();
              G57_DataManager_1.default.deleteInstance();
              return [ 4, G57_SocketManager_1.default.getInstance().connect() ];

             case 1:
              _a.sent();
              return [ 4, this.initGameConfig(G57_DataManager_1.default.getInstance()) ];

             case 2:
              _a.sent();
              return [ 4, _super.prototype.processAsync.call(this, preloadResources) ];

             case 3:
              _a.sent();
              this.processLogin(G57_SocketManager_1.default.getInstance());
              return [ 2 ];
            }
          });
        });
      };
      G57_Loading.prototype.onAssetLoadComplete = function(prefabs) {
        G57_DynamicPopUpPanelManager_1.default.getInstance().prestore(prefabs);
      };
      G57_Loading = __decorate([ ccclass ], G57_Loading);
      return G57_Loading;
    }(Slot_LoadingManager_1.default);
    exports.default = G57_Loading;
    cc._RF.pop();
  }, {
    "../../../Common/Tools/AudioManager/AudioManager": void 0,
    "../../../Common/Tools/DesktopBrowserTransform/DesktopBrowserTransform": void 0,
    "../../../SlotFramework/Loading/Slot_LoadingManager": void 0,
    "../../Common/script/G57_DataManager": "G57_DataManager",
    "../../Common/script/G57_DynamicPopUpPanelManager": "G57_DynamicPopUpPanelManager",
    "../../Common/script/socket/G57_SocketManager": "G57_SocketManager",
    "../../Localization/G57_Language": "G57_Language"
  } ],
  G57_LobbyUI: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "36a74cMP91G+pbovsU4C4KE", "G57_LobbyUI");
    "use strict";
    var __extends = this && this.__extends || function() {
      var extendStatics = function(d, b) {
        extendStatics = Object.setPrototypeOf || {
          __proto__: []
        } instanceof Array && function(d, b) {
          d.__proto__ = b;
        } || function(d, b) {
          for (var p in b) Object.prototype.hasOwnProperty.call(b, p) && (d[p] = b[p]);
        };
        return extendStatics(d, b);
      };
      return function(d, b) {
        extendStatics(d, b);
        function __() {
          this.constructor = d;
        }
        d.prototype = null === b ? Object.create(b) : (__.prototype = b.prototype, new __());
      };
    }();
    var __decorate = this && this.__decorate || function(decorators, target, key, desc) {
      var c = arguments.length, r = c < 3 ? target : null === desc ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
      if ("object" === typeof Reflect && "function" === typeof Reflect.decorate) r = Reflect.decorate(decorators, target, key, desc); else for (var i = decorators.length - 1; i >= 0; i--) (d = decorators[i]) && (r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r);
      return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    var Slot_LobbyUI_1 = require("../../../../SlotFramework/Lobby/Slot_LobbyUI");
    var ccclass = cc._decorator.ccclass;
    var G57_LobbyUI = function(_super) {
      __extends(G57_LobbyUI, _super);
      function G57_LobbyUI() {
        return null !== _super && _super.apply(this, arguments) || this;
      }
      G57_LobbyUI.prototype.init = function(p_data) {
        _super.prototype.init.call(this, p_data);
      };
      G57_LobbyUI = __decorate([ ccclass ], G57_LobbyUI);
      return G57_LobbyUI;
    }(Slot_LobbyUI_1.default);
    exports.default = G57_LobbyUI;
    cc._RF.pop();
  }, {
    "../../../../SlotFramework/Lobby/Slot_LobbyUI": void 0
  } ],
  G57_Lobby: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "448caDltY9Fx6IoBjAvFxTl", "G57_Lobby");
    "use strict";
    var __extends = this && this.__extends || function() {
      var extendStatics = function(d, b) {
        extendStatics = Object.setPrototypeOf || {
          __proto__: []
        } instanceof Array && function(d, b) {
          d.__proto__ = b;
        } || function(d, b) {
          for (var p in b) Object.prototype.hasOwnProperty.call(b, p) && (d[p] = b[p]);
        };
        return extendStatics(d, b);
      };
      return function(d, b) {
        extendStatics(d, b);
        function __() {
          this.constructor = d;
        }
        d.prototype = null === b ? Object.create(b) : (__.prototype = b.prototype, new __());
      };
    }();
    var __decorate = this && this.__decorate || function(decorators, target, key, desc) {
      var c = arguments.length, r = c < 3 ? target : null === desc ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
      if ("object" === typeof Reflect && "function" === typeof Reflect.decorate) r = Reflect.decorate(decorators, target, key, desc); else for (var i = decorators.length - 1; i >= 0; i--) (d = decorators[i]) && (r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r);
      return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    var LocalizationManager_1 = require("../../../Common/Tools/Localization/LocalizationManager");
    var Slot_LobbyManager_InitData_1 = require("../../../SlotFramework/Lobby/Data/Slot_LobbyManager_InitData");
    var Slot_LobbyUI_InitData_1 = require("../../../SlotFramework/Lobby/Data/Slot_LobbyUI_InitData");
    var Slot_LobbyManager_1 = require("../../../SlotFramework/Lobby/Slot_LobbyManager");
    var G57_DataManager_1 = require("../../Common/script/G57_DataManager");
    var G57_SocketManager_1 = require("../../Common/script/socket/G57_SocketManager");
    var G57_LobbyUI_1 = require("./view/G57_LobbyUI");
    var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
    var G57_Lobby = function(_super) {
      __extends(G57_Lobby, _super);
      function G57_Lobby() {
        var _this = null !== _super && _super.apply(this, arguments) || this;
        _this.ui = null;
        _this.data = null;
        _this.socket = null;
        return _this;
      }
      G57_Lobby.prototype.start = function() {
        _super.prototype.start.call(this);
        this.socket = G57_SocketManager_1.default.getInstance();
        var _data = new Slot_LobbyManager_InitData_1.default();
        _data.bundleName = "G" + G57_DataManager_1.default.getInstance().gameID;
        _data.bundlePath = "" + G57_DataManager_1.default.getInstance().path;
        this.init(_data);
        this.onFastJoinGame();
      };
      G57_Lobby.prototype.onDestroy = function() {
        this.removeEvents();
      };
      G57_Lobby.prototype.init = function(p_data) {
        this.data = G57_DataManager_1.default.getInstance();
        this.data.init();
        this.registerEvents();
        _super.prototype.init.call(this, p_data);
        var _data = new Slot_LobbyUI_InitData_1.default();
        _data.bundleName = p_data.bundleName;
        _data.bundlePath = p_data.bundlePath;
        this.ui.init(_data);
      };
      G57_Lobby.prototype.registerEvents = function() {
        var _this = this;
        G57_SocketManager_1.default.getInstance().setSelfEnterEventCallback(function(data) {
          _this.onSelfEnterEvent(data);
        });
      };
      G57_Lobby.prototype.removeEvents = function() {
        G57_SocketManager_1.default.getInstance().setSelfEnterEventCallback(null);
      };
      G57_Lobby.prototype.onSelfEnterEvent = function(data) {
        G57_DataManager_1.default.getInstance().coin = G57_DataManager_1.default.getInstance().getClientMoney(data.Coin);
      };
      G57_Lobby.prototype.onBackButton = function() {
        console.log("\u96e2\u958b");
      };
      G57_Lobby.prototype.onOpenHistoryButton = function() {
        console.log("\u958b\u555f\u6b77\u53f2\u7d00\u9304");
      };
      G57_Lobby.prototype.onOpenMenuButton = function() {
        this.ui.openMenu();
      };
      G57_Lobby.prototype.onCloseMenuButton = function() {
        this.ui.closeMenu();
      };
      G57_Lobby.prototype.onChangeMachine = function() {
        console.log("\u63db\u4e00\u6279");
      };
      G57_Lobby.prototype.onFastJoinGame = function() {
        var _this = this;
        console.log("\u5feb\u901f\u52a0\u5165");
        this.socket.setOnFastChoseRoomEvent().then(function() {
          cc.director.loadScene("G57_Game");
        }).catch(function(error) {
          error.code == GameClient.errCode.RESPONSE_TIMED_OUT && _this.socket.showErrorAlert(LocalizationManager_1.default.getInstance().get("ERROR_SOCKET_TIME_OUT"), null, function() {
            G57_SocketManager_1.default.getInstance().login();
          });
        });
      };
      G57_Lobby.prototype.onChangePlayerHead = function() {
        console.log("\u63db\u73a9\u5bb6\u982d\u50cf");
      };
      G57_Lobby.prototype.setPlayerInfo = function() {
        var _money = this.data.coin;
        this.ui.setPlayerInfo(this.data.nickname, this.data.avatarID, _money);
      };
      G57_Lobby.prototype.setRoomData = function() {
        console.log("\u8a2d\u5b9a\u623f\u9593\u8cc7\u6599");
      };
      __decorate([ property(G57_LobbyUI_1.default) ], G57_Lobby.prototype, "ui", void 0);
      G57_Lobby = __decorate([ ccclass ], G57_Lobby);
      return G57_Lobby;
    }(Slot_LobbyManager_1.default);
    exports.default = G57_Lobby;
    cc._RF.pop();
  }, {
    "../../../Common/Tools/Localization/LocalizationManager": void 0,
    "../../../SlotFramework/Lobby/Data/Slot_LobbyManager_InitData": void 0,
    "../../../SlotFramework/Lobby/Data/Slot_LobbyUI_InitData": void 0,
    "../../../SlotFramework/Lobby/Slot_LobbyManager": void 0,
    "../../Common/script/G57_DataManager": "G57_DataManager",
    "../../Common/script/socket/G57_SocketManager": "G57_SocketManager",
    "./view/G57_LobbyUI": "G57_LobbyUI"
  } ],
  G57_MusicOptionPanel: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "f12b9V9Ef1NZp1+RDu9Xfz+", "G57_MusicOptionPanel");
    "use strict";
    var __extends = this && this.__extends || function() {
      var extendStatics = function(d, b) {
        extendStatics = Object.setPrototypeOf || {
          __proto__: []
        } instanceof Array && function(d, b) {
          d.__proto__ = b;
        } || function(d, b) {
          for (var p in b) Object.prototype.hasOwnProperty.call(b, p) && (d[p] = b[p]);
        };
        return extendStatics(d, b);
      };
      return function(d, b) {
        extendStatics(d, b);
        function __() {
          this.constructor = d;
        }
        d.prototype = null === b ? Object.create(b) : (__.prototype = b.prototype, new __());
      };
    }();
    var __decorate = this && this.__decorate || function(decorators, target, key, desc) {
      var c = arguments.length, r = c < 3 ? target : null === desc ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
      if ("object" === typeof Reflect && "function" === typeof Reflect.decorate) r = Reflect.decorate(decorators, target, key, desc); else for (var i = decorators.length - 1; i >= 0; i--) (d = decorators[i]) && (r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r);
      return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    var Slot_MusicOptionPanel_1 = require("../../../../SlotFramework/Game/Panel/Slot_MusicOptionPanel");
    var ccclass = cc._decorator.ccclass;
    var G57_MusicOptionPanel = function(_super) {
      __extends(G57_MusicOptionPanel, _super);
      function G57_MusicOptionPanel() {
        return null !== _super && _super.apply(this, arguments) || this;
      }
      G57_MusicOptionPanel = __decorate([ ccclass ], G57_MusicOptionPanel);
      return G57_MusicOptionPanel;
    }(Slot_MusicOptionPanel_1.default);
    exports.default = G57_MusicOptionPanel;
    cc._RF.pop();
  }, {
    "../../../../SlotFramework/Game/Panel/Slot_MusicOptionPanel": void 0
  } ],
  G57_Reel: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "13954pZDNtFopVb/wPIObQR", "G57_Reel");
    "use strict";
    var __extends = this && this.__extends || function() {
      var extendStatics = function(d, b) {
        extendStatics = Object.setPrototypeOf || {
          __proto__: []
        } instanceof Array && function(d, b) {
          d.__proto__ = b;
        } || function(d, b) {
          for (var p in b) Object.prototype.hasOwnProperty.call(b, p) && (d[p] = b[p]);
        };
        return extendStatics(d, b);
      };
      return function(d, b) {
        extendStatics(d, b);
        function __() {
          this.constructor = d;
        }
        d.prototype = null === b ? Object.create(b) : (__.prototype = b.prototype, new __());
      };
    }();
    var __decorate = this && this.__decorate || function(decorators, target, key, desc) {
      var c = arguments.length, r = c < 3 ? target : null === desc ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
      if ("object" === typeof Reflect && "function" === typeof Reflect.decorate) r = Reflect.decorate(decorators, target, key, desc); else for (var i = decorators.length - 1; i >= 0; i--) (d = decorators[i]) && (r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r);
      return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __awaiter = this && this.__awaiter || function(thisArg, _arguments, P, generator) {
      function adopt(value) {
        return value instanceof P ? value : new P(function(resolve) {
          resolve(value);
        });
      }
      return new (P || (P = Promise))(function(resolve, reject) {
        function fulfilled(value) {
          try {
            step(generator.next(value));
          } catch (e) {
            reject(e);
          }
        }
        function rejected(value) {
          try {
            step(generator["throw"](value));
          } catch (e) {
            reject(e);
          }
        }
        function step(result) {
          result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected);
        }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
      });
    };
    var __generator = this && this.__generator || function(thisArg, body) {
      var _ = {
        label: 0,
        sent: function() {
          if (1 & t[0]) throw t[1];
          return t[1];
        },
        trys: [],
        ops: []
      }, f, y, t, g;
      return g = {
        next: verb(0),
        throw: verb(1),
        return: verb(2)
      }, "function" === typeof Symbol && (g[Symbol.iterator] = function() {
        return this;
      }), g;
      function verb(n) {
        return function(v) {
          return step([ n, v ]);
        };
      }
      function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
          if (f = 1, y && (t = 2 & op[0] ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 
          0) : y.next) && !(t = t.call(y, op[1])).done) return t;
          (y = 0, t) && (op = [ 2 & op[0], t.value ]);
          switch (op[0]) {
           case 0:
           case 1:
            t = op;
            break;

           case 4:
            _.label++;
            return {
              value: op[1],
              done: false
            };

           case 5:
            _.label++;
            y = op[1];
            op = [ 0 ];
            continue;

           case 7:
            op = _.ops.pop();
            _.trys.pop();
            continue;

           default:
            if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (6 === op[0] || 2 === op[0])) {
              _ = 0;
              continue;
            }
            if (3 === op[0] && (!t || op[1] > t[0] && op[1] < t[3])) {
              _.label = op[1];
              break;
            }
            if (6 === op[0] && _.label < t[1]) {
              _.label = t[1];
              t = op;
              break;
            }
            if (t && _.label < t[2]) {
              _.label = t[2];
              _.ops.push(op);
              break;
            }
            t[2] && _.ops.pop();
            _.trys.pop();
            continue;
          }
          op = body.call(thisArg, _);
        } catch (e) {
          op = [ 6, e ];
          y = 0;
        } finally {
          f = t = 0;
        }
        if (5 & op[0]) throw op[1];
        return {
          value: op[0] ? op[1] : void 0,
          done: true
        };
      }
    };
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    var FXController_1 = require("../../../../Common/Optional/Crosis/FXController/FXController");
    var CommonTool_1 = require("../../../../Common/Tools/CommonTool");
    var Slot_Symbol_InitData_1 = require("../../../../SlotFramework/Game/Data/Slot_Symbol_InitData");
    var Slot_Reel_1 = require("../../../../SlotFramework/Game/view/Slot_Reel");
    var G57_Symbol_1 = require("./G57_Symbol");
    var ccclass = cc._decorator.ccclass;
    var G57_Reel = function(_super) {
      __extends(G57_Reel, _super);
      function G57_Reel() {
        var _this = null !== _super && _super.apply(this, arguments) || this;
        _this.symbolsList = [];
        return _this;
      }
      G57_Reel.prototype.getSymbolReWardPos = function(p_winPosition) {
        var list = [];
        for (var i = 0; i < p_winPosition.length; i++) if (1 == p_winPosition[i]) for (var j = 0; j < this.symbolsList.length; j++) this.symbolsList[j].getOrder() - 2 == i && list.push(this.symbolsList[j].node.position);
        return list;
      };
      G57_Reel.prototype.showOmenEffect = function() {
        _super.prototype.showOmenEffect.call(this);
        for (var i = 0; i < this.symbolsList.length; i++) this.symbolsList[i].showOmenEffect();
        this.omenEffect.opacity = 255;
        this.omenEffect.getComponent(FXController_1.default).play();
      };
      G57_Reel.prototype.closeOmenEffect = function() {
        var _this = this;
        cc.tween(this.omenEffect).to(.2, {
          opacity: 0
        }, {
          easing: "sineIn"
        }).call(function() {
          _super.prototype.closeOmenEffect.call(_this);
        }).start();
      };
      G57_Reel.prototype.init = function(p_data) {
        return __awaiter(this, void 0, Promise, function() {
          var _symbolCount, i, _symbolNode, _symbol, _data, i, _symbolPosition;
          return __generator(this, function(_a) {
            switch (_a.label) {
             case 0:
              _symbolCount = p_data.symbolCount + 2;
              this.reelSlowTime = p_data.reelSlowTime;
              this.moveSpeed = p_data.moveSpeed;
              this.omenMoveSpeed = p_data.omenMoveSpeed;
              this.omenSpeedMutly = p_data.omenSpeedMutly;
              this.omenSlowTime = p_data.omenSlowTime;
              this.reboundTime = p_data.reboundTime;
              this.reelReboundRange = p_data.reelReboundRange;
              this.symbolSpacing = p_data.symbolSpacing;
              this.alignmentDataCount = _symbolCount - 1;
              this.scrollingUpTime = p_data.scrollingUpTime;
              this.scrollingUpRange = p_data.scrollingUpRange;
              this.reelOffset = p_data.reelOffset;
              this.displaySymbolCount = p_data.symbolCount;
              this.reelMode = p_data.reelMode;
              this.reelIndex = p_data.reelIndex;
              i = 0;
              _a.label = 1;

             case 1:
              if (!(i < _symbolCount)) return [ 3, 4 ];
              return [ 4, CommonTool_1.CommonTool.toNextTick() ];

             case 2:
              _a.sent();
              _symbolNode = cc.instantiate(this.symbolPrefab);
              _symbol = _symbolNode.getComponent(G57_Symbol_1.default);
              _data = new Slot_Symbol_InitData_1.default();
              _data.bundleName = p_data.bundleName;
              _data.bundlePath = p_data.bundlePath;
              _data.reelIndex = p_data.reelIndex;
              _symbol.init(_data);
              this.symbolsList.push(_symbol);
              _a.label = 3;

             case 3:
              i++;
              return [ 3, 1 ];

             case 4:
              return [ 4, CommonTool_1.CommonTool.toNextTick() ];

             case 5:
              _a.sent();
              for (i = 0; i < this.symbolsList.length; i++) {
                this.symbolsList[i].node.setScale(p_data.symbolScale);
                _symbolPosition = this.getSymbolPosition(i);
                this.symbolsList[i].node.setParent(this.node.parent);
                this.symbolsList[i].node.setSiblingIndex(0);
                this.symbolsList[i].node.setPosition(_symbolPosition);
              }
              return [ 4, CommonTool_1.CommonTool.toNextTick() ];

             case 6:
              _a.sent();
              this.setOmenEffectParent();
              this.closeOmenEffect();
              this.initOmenBackground();
              this.omenScale(p_data.omenScale);
              return [ 2 ];
            }
          });
        });
      };
      G57_Reel.disappearEffectDelayCount = 0;
      G57_Reel = __decorate([ ccclass ], G57_Reel);
      return G57_Reel;
    }(Slot_Reel_1.default);
    exports.default = G57_Reel;
    cc._RF.pop();
  }, {
    "../../../../Common/Optional/Crosis/FXController/FXController": void 0,
    "../../../../Common/Tools/CommonTool": void 0,
    "../../../../SlotFramework/Game/Data/Slot_Symbol_InitData": void 0,
    "../../../../SlotFramework/Game/view/Slot_Reel": void 0,
    "./G57_Symbol": "G57_Symbol"
  } ],
  G57_RulePanel: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "f0d0fag70tMy4DDHqXapjWI", "G57_RulePanel");
    "use strict";
    var __extends = this && this.__extends || function() {
      var extendStatics = function(d, b) {
        extendStatics = Object.setPrototypeOf || {
          __proto__: []
        } instanceof Array && function(d, b) {
          d.__proto__ = b;
        } || function(d, b) {
          for (var p in b) Object.prototype.hasOwnProperty.call(b, p) && (d[p] = b[p]);
        };
        return extendStatics(d, b);
      };
      return function(d, b) {
        extendStatics(d, b);
        function __() {
          this.constructor = d;
        }
        d.prototype = null === b ? Object.create(b) : (__.prototype = b.prototype, new __());
      };
    }();
    var __decorate = this && this.__decorate || function(decorators, target, key, desc) {
      var c = arguments.length, r = c < 3 ? target : null === desc ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
      if ("object" === typeof Reflect && "function" === typeof Reflect.decorate) r = Reflect.decorate(decorators, target, key, desc); else for (var i = decorators.length - 1; i >= 0; i--) (d = decorators[i]) && (r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r);
      return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    var AudioManager_1 = require("../../../../Common/Tools/AudioManager/AudioManager");
    var Slot_RulePanel_1 = require("../../../../SlotFramework/Game/Panel/Slot_RulePanel");
    var G57_DataManager_1 = require("../../../Common/script/G57_DataManager");
    var ccclass = cc._decorator.ccclass;
    var G57_RulePanel = function(_super) {
      __extends(G57_RulePanel, _super);
      function G57_RulePanel() {
        var _this = null !== _super && _super.apply(this, arguments) || this;
        _this.data = null;
        return _this;
      }
      G57_RulePanel.prototype.init = function() {
        this.data = G57_DataManager_1.default.getInstance();
        _super.prototype.init.call(this);
      };
      G57_RulePanel.prototype.onNextRulePage = function() {
        if (this.status == Slot_RulePanel_1.STATUS.CHANGE_PAGE) return;
        AudioManager_1.AudioManager.instance.playAudioEvent("NextRulePage");
        _super.prototype.onNextRulePage.call(this);
      };
      G57_RulePanel.prototype.onPreRulePage = function() {
        if (this.status == Slot_RulePanel_1.STATUS.CHANGE_PAGE) return;
        AudioManager_1.AudioManager.instance.playAudioEvent("PreRulePage");
        _super.prototype.onPreRulePage.call(this);
      };
      G57_RulePanel.prototype.open = function() {
        _super.prototype.open.call(this);
      };
      G57_RulePanel.prototype.close = function() {
        _super.prototype.close.call(this);
        AudioManager_1.AudioManager.instance.playAudioEvent("CloseRule");
      };
      G57_RulePanel = __decorate([ ccclass ], G57_RulePanel);
      return G57_RulePanel;
    }(Slot_RulePanel_1.default);
    exports.default = G57_RulePanel;
    cc._RF.pop();
  }, {
    "../../../../Common/Tools/AudioManager/AudioManager": void 0,
    "../../../../SlotFramework/Game/Panel/Slot_RulePanel": void 0,
    "../../../Common/script/G57_DataManager": "G57_DataManager"
  } ],
  G57_SlotReelManager: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "bf12bl6F8NPcYegW5vzFk3a", "G57_SlotReelManager");
    "use strict";
    var __extends = this && this.__extends || function() {
      var extendStatics = function(d, b) {
        extendStatics = Object.setPrototypeOf || {
          __proto__: []
        } instanceof Array && function(d, b) {
          d.__proto__ = b;
        } || function(d, b) {
          for (var p in b) Object.prototype.hasOwnProperty.call(b, p) && (d[p] = b[p]);
        };
        return extendStatics(d, b);
      };
      return function(d, b) {
        extendStatics(d, b);
        function __() {
          this.constructor = d;
        }
        d.prototype = null === b ? Object.create(b) : (__.prototype = b.prototype, new __());
      };
    }();
    var __decorate = this && this.__decorate || function(decorators, target, key, desc) {
      var c = arguments.length, r = c < 3 ? target : null === desc ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
      if ("object" === typeof Reflect && "function" === typeof Reflect.decorate) r = Reflect.decorate(decorators, target, key, desc); else for (var i = decorators.length - 1; i >= 0; i--) (d = decorators[i]) && (r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r);
      return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __awaiter = this && this.__awaiter || function(thisArg, _arguments, P, generator) {
      function adopt(value) {
        return value instanceof P ? value : new P(function(resolve) {
          resolve(value);
        });
      }
      return new (P || (P = Promise))(function(resolve, reject) {
        function fulfilled(value) {
          try {
            step(generator.next(value));
          } catch (e) {
            reject(e);
          }
        }
        function rejected(value) {
          try {
            step(generator["throw"](value));
          } catch (e) {
            reject(e);
          }
        }
        function step(result) {
          result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected);
        }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
      });
    };
    var __generator = this && this.__generator || function(thisArg, body) {
      var _ = {
        label: 0,
        sent: function() {
          if (1 & t[0]) throw t[1];
          return t[1];
        },
        trys: [],
        ops: []
      }, f, y, t, g;
      return g = {
        next: verb(0),
        throw: verb(1),
        return: verb(2)
      }, "function" === typeof Symbol && (g[Symbol.iterator] = function() {
        return this;
      }), g;
      function verb(n) {
        return function(v) {
          return step([ n, v ]);
        };
      }
      function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
          if (f = 1, y && (t = 2 & op[0] ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 
          0) : y.next) && !(t = t.call(y, op[1])).done) return t;
          (y = 0, t) && (op = [ 2 & op[0], t.value ]);
          switch (op[0]) {
           case 0:
           case 1:
            t = op;
            break;

           case 4:
            _.label++;
            return {
              value: op[1],
              done: false
            };

           case 5:
            _.label++;
            y = op[1];
            op = [ 0 ];
            continue;

           case 7:
            op = _.ops.pop();
            _.trys.pop();
            continue;

           default:
            if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (6 === op[0] || 2 === op[0])) {
              _ = 0;
              continue;
            }
            if (3 === op[0] && (!t || op[1] > t[0] && op[1] < t[3])) {
              _.label = op[1];
              break;
            }
            if (6 === op[0] && _.label < t[1]) {
              _.label = t[1];
              t = op;
              break;
            }
            if (t && _.label < t[2]) {
              _.label = t[2];
              _.ops.push(op);
              break;
            }
            t[2] && _.ops.pop();
            _.trys.pop();
            continue;
          }
          op = body.call(thisArg, _);
        } catch (e) {
          op = [ 6, e ];
          y = 0;
        } finally {
          f = t = 0;
        }
        if (5 & op[0]) throw op[1];
        return {
          value: op[0] ? op[1] : void 0,
          done: true
        };
      }
    };
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    var Slot_ReelManager_1 = require("../../../../SlotFramework/Game/view/Slot_ReelManager");
    var G57_Reel_1 = require("./G57_Reel");
    var CommonTool_1 = require("../../../../Common/Tools/CommonTool");
    var OrientationToolManager_1 = require("../../../../Common/Tools/OrientationTool/OrientationToolManager");
    var Slot_Reel_InitData_1 = require("../../../../SlotFramework/Game/Data/Slot_Reel_InitData");
    var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
    var G57_SlotReelManager = function(_super) {
      __extends(G57_SlotReelManager, _super);
      function G57_SlotReelManager() {
        var _this = null !== _super && _super.apply(this, arguments) || this;
        _this.rewardScoreLableList = [];
        _this.reelsList = [];
        return _this;
      }
      G57_SlotReelManager.prototype.init = function(p_data) {
        return __awaiter(this, void 0, void 0, function() {
          return __generator(this, function(_a) {
            switch (_a.label) {
             case 0:
              this.bundleName = p_data.bundleName;
              this.bundlePath = p_data.bundlePath;
              this.omenMask.active = false;
              this.omenMask.zIndex = 1;
              this.omenMaskOpacity = this.omenMask.opacity;
              return [ 4, this.createReels() ];

             case 1:
              _a.sent();
              this.initSymbolPos();
              this.hideScoreReWardEffect();
              return [ 2 ];
            }
          });
        });
      };
      G57_SlotReelManager.prototype.showScoreReWardEffect = function(p_winPosition, score) {
        var pos = 0;
        for (var i = 0; i < p_winPosition[2].length; i++) 1 == p_winPosition[2][i] && (pos = i);
        this.rewardScoreLableList[pos].node.active = true;
        this.rewardScoreLableList[pos].string = CommonTool_1.CommonTool.getNumText(Number(score), 2, true, false);
        this.rewardScoreLableList[pos].node.scale = 0;
        cc.tween(this.rewardScoreLableList[pos].node).to(.38, {
          scale: 1
        }, {
          easing: "elasticOut"
        }).start();
      };
      G57_SlotReelManager.prototype.hideScoreReWardEffect = function() {
        for (var i = 0; i < this.rewardScoreLableList.length; i++) this.rewardScoreLableList[i].node.active = false;
      };
      G57_SlotReelManager.prototype.getSymbolReWardPos = function(p_winPosition) {
        var list = [];
        for (var i = 0; i < p_winPosition.length; i++) this.reelsList[i].getSymbolReWardPos(p_winPosition[i]).forEach(function(pos) {
          return list.push(pos);
        });
        return list;
      };
      G57_SlotReelManager.prototype.createReels = function() {
        return __awaiter(this, void 0, Promise, function() {
          var i, _reelNode, _reel, spacing, _reelPosition, _data;
          return __generator(this, function(_a) {
            switch (_a.label) {
             case 0:
              this.reelsList = [];
              i = 0;
              _a.label = 1;

             case 1:
              if (!(i < this.reelCount)) return [ 3, 7 ];
              return [ 4, CommonTool_1.CommonTool.toNextTick() ];

             case 2:
              _a.sent();
              _reelNode = cc.instantiate(this.reelPrefab);
              _reel = _reelNode.getComponent(G57_Reel_1.default);
              return [ 4, CommonTool_1.CommonTool.toNextTick() ];

             case 3:
              _a.sent();
              this.setReelParent(i, _reelNode);
              spacing = this.reelSpacing;
              OrientationToolManager_1.default.orientationState !== OrientationToolManager_1.ORIENTATION_TYPE.NONE && (spacing = this.isHorizontal() ? this.reelSpacingH : this.reelSpacingV);
              _reelPosition = this.getReelPosition(i, this.reelCount, spacing);
              _reelNode.setPosition(_reelPosition);
              _data = new Slot_Reel_InitData_1.default();
              _data.symbolCount = this.symbolCount;
              if (OrientationToolManager_1.default.orientationState === OrientationToolManager_1.ORIENTATION_TYPE.NONE) {
                _data.symbolSpacing = this.symbolSpacing;
                _data.symbolScale = new cc.Vec2(1, 1);
                _data.omenScale = this.omenScaleV;
              } else {
                _data.symbolSpacing = this.isHorizontal() ? this.symbolSpacingH : this.symbolSpacingV;
                _data.symbolScale = this.isHorizontal() ? this.symbolScaleH : this.symbolScaleV;
                _data.omenScale = this.isHorizontal() ? this.omenScaleH : this.omenScaleV;
              }
              _data.moveSpeed = this.reelMoveSpeed;
              _data.omenMoveSpeed = this.reelOmenSpeed;
              _data.omenSpeedMutly = this.omenSpeedMutly;
              _data.omenSlowTime = this.omenSlowTime;
              _data.reelSlowTime = this.reelSlowTime;
              _data.reboundTime = this.reelReboundTime;
              _data.reelReboundRange = this.reelReboundRange;
              _data.scrollingUpTime = this.reelScrollUpTime;
              _data.scrollingUpRange = this.reelScrollUpRange;
              _data.bundleName = this.bundleName;
              _data.bundlePath = this.bundlePath;
              _data.reelIndex = i;
              cc.isValid(this.reelsOffset[i]) || (this.reelsOffset[i] = cc.Vec2.ZERO);
              _data.reelOffset = this.reelsOffset[i];
              _data.reelMode = this.reelMode;
              return [ 4, CommonTool_1.CommonTool.toNextTick() ];

             case 4:
              _a.sent();
              return [ 4, _reel.init(_data) ];

             case 5:
              _a.sent();
              this.reelsList.push(_reel);
              _a.label = 6;

             case 6:
              i++;
              return [ 3, 1 ];

             case 7:
              return [ 2 ];
            }
          });
        });
      };
      __decorate([ property([ cc.Label ]) ], G57_SlotReelManager.prototype, "rewardScoreLableList", void 0);
      G57_SlotReelManager = __decorate([ ccclass ], G57_SlotReelManager);
      return G57_SlotReelManager;
    }(Slot_ReelManager_1.default);
    exports.default = G57_SlotReelManager;
    cc._RF.pop();
  }, {
    "../../../../Common/Tools/CommonTool": void 0,
    "../../../../Common/Tools/OrientationTool/OrientationToolManager": void 0,
    "../../../../SlotFramework/Game/Data/Slot_Reel_InitData": void 0,
    "../../../../SlotFramework/Game/view/Slot_ReelManager": void 0,
    "./G57_Reel": "G57_Reel"
  } ],
  G57_SocketConnect: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "57d5253gVhNI6Q7vudUQBfM", "G57_SocketConnect");
    "use strict";
    var __extends = this && this.__extends || function() {
      var extendStatics = function(d, b) {
        extendStatics = Object.setPrototypeOf || {
          __proto__: []
        } instanceof Array && function(d, b) {
          d.__proto__ = b;
        } || function(d, b) {
          for (var p in b) Object.prototype.hasOwnProperty.call(b, p) && (d[p] = b[p]);
        };
        return extendStatics(d, b);
      };
      return function(d, b) {
        extendStatics(d, b);
        function __() {
          this.constructor = d;
        }
        d.prototype = null === b ? Object.create(b) : (__.prototype = b.prototype, new __());
      };
    }();
    var __decorate = this && this.__decorate || function(decorators, target, key, desc) {
      var c = arguments.length, r = c < 3 ? target : null === desc ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
      if ("object" === typeof Reflect && "function" === typeof Reflect.decorate) r = Reflect.decorate(decorators, target, key, desc); else for (var i = decorators.length - 1; i >= 0; i--) (d = decorators[i]) && (r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r);
      return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    exports.G57_SocketConnect = void 0;
    var Slot_SocketConnect_1 = require("../../../../SlotFramework/Common/Socket/Slot_SocketConnect");
    var G57_DataManager_1 = require("../G57_DataManager");
    var G57_DynamicPopUpPanelManager_1 = require("../G57_DynamicPopUpPanelManager");
    var G57_SocketManager_1 = require("./G57_SocketManager");
    var ccclass = cc._decorator.ccclass;
    var G57_SocketConnect = function(_super) {
      __extends(G57_SocketConnect, _super);
      function G57_SocketConnect() {
        return null !== _super && _super.apply(this, arguments) || this;
      }
      Object.defineProperty(G57_SocketConnect.prototype, "socketManager", {
        get: function() {
          return G57_SocketManager_1.default.getInstance();
        },
        enumerable: false,
        configurable: true
      });
      Object.defineProperty(G57_SocketConnect.prototype, "popupManager", {
        get: function() {
          return G57_DynamicPopUpPanelManager_1.default.getInstance();
        },
        enumerable: false,
        configurable: true
      });
      Object.defineProperty(G57_SocketConnect.prototype, "dataManager", {
        get: function() {
          return G57_DataManager_1.default.getInstance();
        },
        enumerable: false,
        configurable: true
      });
      G57_SocketConnect.prototype.init = function() {
        _super.prototype.init.call(this);
      };
      G57_SocketConnect = __decorate([ ccclass ], G57_SocketConnect);
      return G57_SocketConnect;
    }(Slot_SocketConnect_1.Slot_SocketConnect);
    exports.G57_SocketConnect = G57_SocketConnect;
    cc._RF.pop();
  }, {
    "../../../../SlotFramework/Common/Socket/Slot_SocketConnect": void 0,
    "../G57_DataManager": "G57_DataManager",
    "../G57_DynamicPopUpPanelManager": "G57_DynamicPopUpPanelManager",
    "./G57_SocketManager": "G57_SocketManager"
  } ],
  G57_SocketManager: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "68892kw8bNJlrD4AllKVNP/", "G57_SocketManager");
    "use strict";
    var __extends = this && this.__extends || function() {
      var extendStatics = function(d, b) {
        extendStatics = Object.setPrototypeOf || {
          __proto__: []
        } instanceof Array && function(d, b) {
          d.__proto__ = b;
        } || function(d, b) {
          for (var p in b) Object.prototype.hasOwnProperty.call(b, p) && (d[p] = b[p]);
        };
        return extendStatics(d, b);
      };
      return function(d, b) {
        extendStatics(d, b);
        function __() {
          this.constructor = d;
        }
        d.prototype = null === b ? Object.create(b) : (__.prototype = b.prototype, new __());
      };
    }();
    var __decorate = this && this.__decorate || function(decorators, target, key, desc) {
      var c = arguments.length, r = c < 3 ? target : null === desc ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
      if ("object" === typeof Reflect && "function" === typeof Reflect.decorate) r = Reflect.decorate(decorators, target, key, desc); else for (var i = decorators.length - 1; i >= 0; i--) (d = decorators[i]) && (r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r);
      return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    var G57_DataManager_1 = require("../G57_DataManager");
    var G57_TestSocket_1 = require("./G57_TestSocket");
    var G57_DynamicPopUpPanelManager_1 = require("../G57_DynamicPopUpPanelManager");
    var G57_SocketConnect_1 = require("./G57_SocketConnect");
    var Slot_SocketManager_1 = require("../../../../SlotFramework/Common/Socket/Slot_SocketManager");
    var ccclass = cc._decorator.ccclass;
    var G57_SocketManager = function(_super) {
      __extends(G57_SocketManager, _super);
      function G57_SocketManager() {
        var _this = null !== _super && _super.apply(this, arguments) || this;
        _this.testSocket = null;
        _this.isOffline = false;
        _this.selfEnterEventCallback = null;
        _this.socketConnect = null;
        return _this;
      }
      G57_SocketManager.prototype.init = function() {
        this.socketConnect = new G57_SocketConnect_1.G57_SocketConnect();
        this.socketConnect.init();
        if (G57_DataManager_1.default.offLineMode) {
          this.testSocket = new G57_TestSocket_1.default();
          this.isOffline = true;
        } else {
          console.log("Socket Init");
          this.setErrorAlertCallback(this.showErrorAlert.bind(this));
          this.initGameClient();
          this.client && this.registerEvents();
          this.registerOnSelfEnterEvent();
        }
      };
      G57_SocketManager.prototype.setSelfEnterEventCallback = function(selfEnterCallback) {
        this.selfEnterEventCallback = selfEnterCallback;
      };
      G57_SocketManager.prototype.login = function() {
        var _this = this;
        if (G57_DataManager_1.default.offLineMode) {
          console.log("\u55ae\u6a5f\u6a21\u5f0f\u767b\u5165");
          this.testSocket = new G57_TestSocket_1.default();
          this.isOffline = true;
          this.init();
          this.testSocket.login().then(function(loginRes) {
            _this.handleOnOpen(loginRes);
          });
        } else {
          console.log("\u9023\u7dda\u6a21\u5f0f\u767b\u5165");
          _super.prototype.login.call(this);
        }
      };
      G57_SocketManager.prototype.showErrorAlert = function(message, title, confirmCallback) {
        G57_DynamicPopUpPanelManager_1.default.getInstance().confirmSystemMsgDialogBox.show(message, function() {
          "function" === typeof confirmCallback && confirmCallback();
        });
      };
      G57_SocketManager.prototype.setOnGetInitialReelInfoEvent = function() {
        if (this.isOffline) return this.sendTestSocket("GetInitialReelInfo", this.testSocket.setOnGetInitialReelInfoEvent.bind(this.testSocket));
        return this.request("GetInitialReelInfo", this.client.setOnGetInitialReelInfoEvent.bind(this.client), null);
      };
      G57_SocketManager.prototype.setOnGetRoomListEvent = function() {
        return this.request("GetRoomList", this.client.setOnGetInitialReelInfoEvent.bind(this.client), null);
      };
      G57_SocketManager.prototype.setOnChoseRoomEvent = function(roomId) {
        return this.request("ChoseRoom", this.client.setOnChoseRoomEvent.bind(this.client), roomId);
      };
      G57_SocketManager.prototype.setOnFastChoseRoomEvent = function() {
        if (this.isOffline) return this.sendTestSocket("FastChoseRoom", this.testSocket.setOnFastChoseRoomEvent.bind(this.testSocket));
        return this.request("FastChoseRoom", this.client.setOnFastChoseRoomEvent.bind(this.client), null);
      };
      G57_SocketManager.prototype.setOnSpinBaseGameEvent = function(multiple) {
        if (this.isOffline) return this.sendTestSocket("SpinBaseGame", this.testSocket.setOnSpinBaseGameEvent.bind(this.testSocket), multiple);
        return this.request("SpinBaseGame", this.client.setOnSpinBaseGameEvent.bind(this.client), multiple);
      };
      G57_SocketManager.prototype.setOnChoiceFreeGameEvent = function(freeModule) {
        if (this.isOffline) return this.sendTestSocket("ChoiceFreeGame", this.testSocket.setOnChoiceFreeGameEvent.bind(this.testSocket), freeModule);
        return this.request("ChoiceFreeGame", this.client.setOnChoiceFreeGameEvent.bind(this.client), freeModule);
      };
      G57_SocketManager.prototype.setOnLeaveEvent = function() {
        if (this.isOffline) return this.sendTestSocket("Leave", this.testSocket.setOnLeaveEvent.bind(this.testSocket));
        return this.request("Leave", this.client.setOnLeaveEvent.bind(this.client), null);
      };
      G57_SocketManager.prototype.resetRoomTimeout = function() {
        if (this.isOffline) return;
        return this.request("ResetRoomTimeout", this.client.resetRoomTimeout.bind(this.client), null);
      };
      G57_SocketManager.prototype.registerOnSelfEnterEvent = function() {
        var _this = this;
        this.client.setSelfEnterEvent(function(data) {
          _this.selfEnterEventCallback(data);
        });
      };
      G57_SocketManager.prototype.sendTestSocket = function(eventName, event, content) {
        var _this = this;
        this.showSendLog(eventName, null);
        return new Promise(function(resolve) {
          event.bind(_this)(content).then(function(data) {
            _this.showResponseLog(eventName, data);
            resolve(data);
          });
        });
      };
      G57_SocketManager = __decorate([ ccclass ], G57_SocketManager);
      return G57_SocketManager;
    }(Slot_SocketManager_1.default);
    exports.default = G57_SocketManager;
    cc._RF.pop();
  }, {
    "../../../../SlotFramework/Common/Socket/Slot_SocketManager": void 0,
    "../G57_DataManager": "G57_DataManager",
    "../G57_DynamicPopUpPanelManager": "G57_DynamicPopUpPanelManager",
    "./G57_SocketConnect": "G57_SocketConnect",
    "./G57_TestSocket": "G57_TestSocket"
  } ],
  G57_SymbolTipPanel: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "c814dhN0dBPRbwEXN58MBBk", "G57_SymbolTipPanel");
    "use strict";
    var __extends = this && this.__extends || function() {
      var extendStatics = function(d, b) {
        extendStatics = Object.setPrototypeOf || {
          __proto__: []
        } instanceof Array && function(d, b) {
          d.__proto__ = b;
        } || function(d, b) {
          for (var p in b) Object.prototype.hasOwnProperty.call(b, p) && (d[p] = b[p]);
        };
        return extendStatics(d, b);
      };
      return function(d, b) {
        extendStatics(d, b);
        function __() {
          this.constructor = d;
        }
        d.prototype = null === b ? Object.create(b) : (__.prototype = b.prototype, new __());
      };
    }();
    var __decorate = this && this.__decorate || function(decorators, target, key, desc) {
      var c = arguments.length, r = c < 3 ? target : null === desc ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
      if ("object" === typeof Reflect && "function" === typeof Reflect.decorate) r = Reflect.decorate(decorators, target, key, desc); else for (var i = decorators.length - 1; i >= 0; i--) (d = decorators[i]) && (r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r);
      return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    var MathUtils_1 = require("../../../../Common/Tools/MathUtils");
    var Slot_SymbolTipPanel_1 = require("../../../../SlotFramework/Game/Panel/Slot_SymbolTipPanel");
    var Slot_DataManager_1 = require("../../../../SlotFramework/Slot_DataManager");
    var G57_DataManager_1 = require("../../../Common/script/G57_DataManager");
    var G57_Symbol_1 = require("./G57_Symbol");
    var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
    var G57_SymbolTipPanel = function(_super) {
      __extends(G57_SymbolTipPanel, _super);
      function G57_SymbolTipPanel() {
        var _this = null !== _super && _super.apply(this, arguments) || this;
        _this.defaulTipSymbol = null;
        _this.wildSymbol = null;
        _this.scatterSymbol = null;
        _this.data = null;
        return _this;
      }
      G57_SymbolTipPanel.prototype.init = function() {
        this.data = G57_DataManager_1.default.getInstance();
        _super.prototype.init.call(this);
      };
      G57_SymbolTipPanel.prototype.getTipScore = function(id, combo) {
        var A = MathUtils_1.MathUtils.div10000(this.data.betOdds[this.data.betOddsStartIndex] * this.data.oneOddsScore);
        var B = 50;
        var C = this.data.magnificationTable[id][combo];
        var D = 1;
        console.warn("\nsymbol: " + Slot_DataManager_1.SYMBOL_NAME[id] + "\ncombo: " + combo + "\nA = " + A + ", B = " + B + ", C = " + C + ", D = " + D);
        return A / B * C * D;
      };
      __decorate([ property({
        type: G57_Symbol_1.default,
        override: true
      }) ], G57_SymbolTipPanel.prototype, "defaulTipSymbol", void 0);
      __decorate([ property({
        type: G57_Symbol_1.default,
        override: true
      }) ], G57_SymbolTipPanel.prototype, "wildSymbol", void 0);
      __decorate([ property({
        type: G57_Symbol_1.default,
        override: true
      }) ], G57_SymbolTipPanel.prototype, "scatterSymbol", void 0);
      G57_SymbolTipPanel = __decorate([ ccclass ], G57_SymbolTipPanel);
      return G57_SymbolTipPanel;
    }(Slot_SymbolTipPanel_1.default);
    exports.default = G57_SymbolTipPanel;
    cc._RF.pop();
  }, {
    "../../../../Common/Tools/MathUtils": void 0,
    "../../../../SlotFramework/Game/Panel/Slot_SymbolTipPanel": void 0,
    "../../../../SlotFramework/Slot_DataManager": void 0,
    "../../../Common/script/G57_DataManager": "G57_DataManager",
    "./G57_Symbol": "G57_Symbol"
  } ],
  G57_Symbol: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "95dfeLyVDhNJ6hkeWrSjeMD", "G57_Symbol");
    "use strict";
    var __extends = this && this.__extends || function() {
      var extendStatics = function(d, b) {
        extendStatics = Object.setPrototypeOf || {
          __proto__: []
        } instanceof Array && function(d, b) {
          d.__proto__ = b;
        } || function(d, b) {
          for (var p in b) Object.prototype.hasOwnProperty.call(b, p) && (d[p] = b[p]);
        };
        return extendStatics(d, b);
      };
      return function(d, b) {
        extendStatics(d, b);
        function __() {
          this.constructor = d;
        }
        d.prototype = null === b ? Object.create(b) : (__.prototype = b.prototype, new __());
      };
    }();
    var __decorate = this && this.__decorate || function(decorators, target, key, desc) {
      var c = arguments.length, r = c < 3 ? target : null === desc ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
      if ("object" === typeof Reflect && "function" === typeof Reflect.decorate) r = Reflect.decorate(decorators, target, key, desc); else for (var i = decorators.length - 1; i >= 0; i--) (d = decorators[i]) && (r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r);
      return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    var Slot_Symbol_1 = require("../../../../SlotFramework/Game/view/Slot_Symbol");
    var Slot_DataManager_1 = require("../../../../SlotFramework/Slot_DataManager");
    var FXController_1 = require("../../../../Common/Optional/Crosis/FXController/FXController");
    var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
    var G57_Symbol = function(_super) {
      __extends(G57_Symbol, _super);
      function G57_Symbol() {
        var _this = null !== _super && _super.apply(this, arguments) || this;
        _this.symbolFX = null;
        _this.winFX = null;
        _this.omenFX = null;
        _this.symbolFXPartical = null;
        _this.symbolName = "";
        return _this;
      }
      G57_Symbol.prototype.setData = function(p_nowIndex, p_symbolValue) {
        this.symbolName = Slot_DataManager_1.SYMBOL_NAME[p_symbolValue];
        this.nowIndex = p_nowIndex;
        this.symbolID = p_symbolValue;
        this.symbolSpine.setAnimation(0, this.symbolName + "_stop", true);
      };
      G57_Symbol.prototype.playRewardOnce = function() {
        this.symbolSpine.setAnimation(0, this.symbolName + "_play_win", false);
      };
      G57_Symbol.prototype.playRewardEffect = function() {
        this.symbolSpine.setAnimation(0, this.symbolName + "_play_win", true);
        this.symbolFXPartical.active = true;
        this.winFX.play();
        this.symbolFX.play();
      };
      G57_Symbol.prototype.playScrollingEffect = function() {
        this.symbolSpine.setAnimation(0, this.symbolName + "_stop", true);
        this.closeAllEffect();
      };
      G57_Symbol.prototype.playIdleEffect = function() {
        this.symbolSpine.setAnimation(0, this.symbolName + "_play_normal", true);
        this.closeAllEffect();
      };
      G57_Symbol.prototype.closeAllEffect = function() {
        this.winFX.stop();
        this.winFX.node.scale = 1;
        this.symbolFX.stop();
        this.symbolFXPartical.active = false;
      };
      G57_Symbol.prototype.showOmenEffect = function() {
        this.omenFX.play();
      };
      G57_Symbol.prototype.showSymbolTip = function() {
        if (0 !== this.node.opacity) {
          var position = this.node.parent.parent.parent.convertToWorldSpaceAR(this.node.position);
          cc.systemEvent.emit("ShowSymbolTipEvent", this.symbolID, position);
        }
      };
      G57_Symbol.prototype.setSymbolSpineMix = function() {};
      __decorate([ property(FXController_1.default) ], G57_Symbol.prototype, "symbolFX", void 0);
      __decorate([ property(cc.Animation) ], G57_Symbol.prototype, "winFX", void 0);
      __decorate([ property(cc.Animation) ], G57_Symbol.prototype, "omenFX", void 0);
      __decorate([ property(cc.Node) ], G57_Symbol.prototype, "symbolFXPartical", void 0);
      G57_Symbol = __decorate([ ccclass ], G57_Symbol);
      return G57_Symbol;
    }(Slot_Symbol_1.default);
    exports.default = G57_Symbol;
    cc._RF.pop();
  }, {
    "../../../../Common/Optional/Crosis/FXController/FXController": void 0,
    "../../../../SlotFramework/Game/view/Slot_Symbol": void 0,
    "../../../../SlotFramework/Slot_DataManager": void 0
  } ],
  G57_TestSocket: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "9ff35eit+NPK4cOqeU6MUtw", "G57_TestSocket");
    "use strict";
    var __decorate = this && this.__decorate || function(decorators, target, key, desc) {
      var c = arguments.length, r = c < 3 ? target : null === desc ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
      if ("object" === typeof Reflect && "function" === typeof Reflect.decorate) r = Reflect.decorate(decorators, target, key, desc); else for (var i = decorators.length - 1; i >= 0; i--) (d = decorators[i]) && (r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r);
      return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    var G57_DataManager_1 = require("../G57_DataManager");
    var G57_CommonData_1 = require("../../../Game/script/G57_CommonData");
    var ccclass = cc._decorator.ccclass;
    var G57_TestSocket = function() {
      function G57_TestSocket() {
        this.receiveCallback = null;
        this.tempSpinCount = 1;
      }
      G57_TestSocket_1 = G57_TestSocket;
      G57_TestSocket.prototype.login = function() {
        return new Promise(function(resolve) {
          var _data = new G57_CommonData_1.LoginData();
          _data.nickname = "\u533f\u540dA";
          _data.language = "zh-cn";
          _data.balance = 5e5;
          _data.timestamp = 1e9;
          _data.themeType = 0;
          _data.icon = 1;
          _data.gameId = 57;
          resolve(_data);
        });
      };
      G57_TestSocket.prototype.setOnFastChoseRoomEvent = function() {
        return new Promise(function(resolve) {
          var _data = new G57_CommonData_1.FastChoseRoomData();
          _data.roomId = "\u6e2c\u8a66Room";
          resolve(_data);
        });
      };
      G57_TestSocket.prototype.setOnGetInitialReelInfoEvent = function() {
        return new Promise(function(resolve) {
          var _data = new G57_CommonData_1.GetInitialReelInfoData();
          _data.position = [ 4, 2, 22, 4, 4 ];
          _data.reels = new G57_CommonData_1.ReelTable();
          var _BGTable = [];
          _BGTable[0] = [ 50, 50, 14, 12, 11, 20, 14, 2, 11, 3, 2, 11, 14, 11, 2, 14, 11, 3, 13, 11, 14, 2, 13, 20, 2, 3, 12, 14, 4, 11, 4, 11, 14, 1, 1, 1, 13, 3, 4, 14, 11, 13, 3, 14, 13, 4, 20, 14, 11, 14, 13, 14, 2, 11, 14, 3, 11, 20, 14, 12, 11, 14, 14, 2, 20, 3, 5 ];
          _BGTable[1] = [ 12, 13, 12, 4, 20, 13, 4, 12, 13, 12, 13, 1, 12, 1, 3, 1, 12, 13, 4, 13, 1, 12, 13, 4, 13, 12, 11, 20, 1, 13, 12, 1, 11, 1, 12, 2, 12, 3, 11, 4, 14, 3, 13, 12, 4, 20, 11, 13, 2, 14, 4, 12, 14, 12, 13, 1, 1, 3, 14, 11, 12, 13, 11, 14, 4, 1, 13, 4, 14, 13, 12, 4, 13, 12, 4, 13 ];
          _BGTable[2] = [ 12, 20, 2, 20, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 11, 2, 4, 12, 2, 11, 3, 1, 14, 14, 14, 4, 2, 13, 1, 4, 20, 14, 13, 4, 12, 2, 1, 3, 12, 2, 11, 4, 3, 14, 14, 14, 1, 13, 11, 20, 14, 4, 12, 3, 14, 13, 2, 14, 3, 13, 4, 14, 2, 11, 1, 14, 3, 11, 2, 13, 11, 3, 14, 14, 14, 4, 11, 12, 14, 2, 11, 4, 12, 3, 12, 14, 14, 14, 3, 13, 2, 14, 3 ];
          _BGTable[3] = [ 12, 2, 20, 12, 50, 50, 50, 50, 50, 13, 4, 14, 13, 11, 4, 12, 2, 13, 1, 14, 2, 12, 14, 12, 3, 14, 12, 2, 20, 12, 13, 4, 12, 11, 3, 13, 4, 12, 20, 13, 2, 1, 12, 1, 50, 50, 50, 50, 50, 13, 12, 3, 11, 14, 4, 13, 3, 12, 14, 2, 11, 1, 12, 3, 11, 12, 1, 12, 3, 14, 1, 4, 13, 2, 11, 3, 1, 14, 2, 20, 11, 4, 2, 1, 14, 2, 11, 14, 4, 3, 3, 2 ];
          _BGTable[4] = [ 11, 2, 20, 50, 50, 50, 50, 50, 13, 4, 14, 13, 11, 4, 12, 2, 13, 1, 14, 2, 12, 14, 12, 3, 14, 12, 2, 20, 12, 13, 4, 12, 11, 3, 13, 4, 12, 20, 13, 2, 1, 12, 1, 50, 50, 50, 50, 50, 13, 12, 3, 11, 14, 4, 13, 3, 12, 14, 2, 11, 1, 12, 3, 11, 12, 1, 12, 3, 14, 1, 4, 13, 2, 11, 3 ];
          var _FGTable = [];
          for (var i = 0; i < 5; i++) {
            _FGTable[i] = [];
            _FGTable[i][0] = [ 2, 13, 14, 12, 11, 4, 14, 2, 11, 3, 2, 11, 14, 11, 2, 14, 11, 3, 13, 11, 14, 2, 13, 20, 2, 3, 12, 14, 4, 11, 4, 11, 14, 1, 1, 1, 13, 3, 4, 5, 14, 11, 13, 5, 3, 14, 5, 13, 4, 20, 14, 11, 14, 13, 14, 2, 11, 14, 3, 11, 20, 14, 12, 11, 14, 14, 2, 20, 3, 5 ];
            _FGTable[i][1] = [ 12, 13, 12, 4, 3, 13, 4, 12, 13, 12, 13, 1, 12, 1, 3, 1, 12, 13, 4, 13, 1, 12, 13, 4, 13, 12, 11, 20, 1, 13, 12, 1, 11, 1, 12, 2, 12, 3, 11, 4, 14, 3, 13, 12, 4, 20, 11, 13, 2, 14, 4, 5, 12, 14, 12, 13, 1, 1, 3, 14, 11, 12, 13, 11, 14, 4, 1, 13, 4, 14, 13, 12, 4, 13, 12, 4, 13 ];
            _FGTable[i][2] = [ 12, 3, 14, 1, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 11, 2, 4, 12, 2, 11, 3, 1, 14, 14, 14, 4, 2, 13, 1, 4, 20, 14, 13, 4, 12, 2, 1, 3, 12, 2, 11, 4, 3, 14, 14, 14, 1, 13, 11, 20, 14, 4, 12, 3, 14, 13, 2, 14, 3, 13, 4, 14, 2, 11, 1, 14, 3, 11, 2, 13, 11, 3, 14, 1, 1, 4, 11, 1, 1, 2, 11, 4, 12, 3, 12, 14, 14, 14, 3, 13, 2, 14, 3 ];
            _FGTable[i][3] = [ 12, 14, 1, 12, 50, 50, 50, 50, 50, 13, 4, 14, 13, 11, 4, 12, 2, 13, 1, 14, 2, 12, 14, 12, 3, 14, 12, 2, 20, 12, 13, 4, 12, 11, 3, 13, 4, 12, 20, 13, 2, 1, 12, 1, 50, 50, 50, 50, 50, 13, 12, 3, 11, 14, 4, 13, 3, 12, 14, 2, 11, 1, 12, 3, 11, 12, 1, 12, 3, 14, 1, 4, 13, 2, 11, 3, 1, 14, 2, 20, 11, 4, 2, 1, 14, 2, 11, 14, 4, 3, 3, 2 ];
            _FGTable[i][4] = [ 11, 13, 50, 50, 50, 50, 50, 50, 13, 4, 14, 13, 11, 4, 12, 2, 13, 1, 14, 2, 12, 14, 12, 3, 14, 12, 2, 20, 12, 13, 4, 12, 11, 3, 13, 4, 12, 20, 13, 2, 1, 12, 1, 50, 50, 50, 50, 50, 13, 12, 3, 11, 14, 4, 13, 3, 12, 14, 2, 11, 1, 12, 3, 11, 12, 1, 12, 3, 14, 1, 4, 13, 2, 11, 3 ];
          }
          _data.reels.BG = _BGTable;
          _data.reels.FG = _FGTable;
          resolve(_data);
        });
      };
      G57_TestSocket.prototype.setOnSpinBaseGameEvent = function(p_multiple) {
        var _this = this;
        return new Promise(function(resolve) {
          p_multiple *= 5e3;
          var _data = null;
          -1 != G57_TestSocket_1.assignData && (_this.tempSpinCount = G57_TestSocket_1.assignData);
          switch (_this.tempSpinCount) {
           case 1:
            _data = _this.testSpinData_1(p_multiple);
            break;

           case 2:
            _data = _this.testSpinData_2(p_multiple);
            break;

           case 3:
            _data = _this.testSpinData_4(p_multiple);
            break;

           case 4:
            _data = _this.testSpinData_6(p_multiple);
            break;

           case 5:
            _data = _this.testSpinData_9(p_multiple);
            break;

           case 6:
            _data = _this.testSpinData_10(p_multiple);
            break;

           case 7:
            _data = _this.testSpinData_11(p_multiple);
          }
          _this.tempSpinCount++;
          _this.tempSpinCount > 6 && (_this.tempSpinCount = 1);
          resolve(_data);
        });
      };
      G57_TestSocket.prototype.setOnChoiceFreeGameEvent = function(p_multiple) {
        var _this = this;
        return new Promise(function(resolve) {
          p_multiple *= 5e3;
          var _data = null;
          _data = _this.testSpinData_4(p_multiple);
          resolve(_data);
        });
      };
      G57_TestSocket.prototype.testSpinData_1 = function(p_betScore) {
        var _data = new G57_CommonData_1.BaseGameSpinResponeData();
        _data.currentCash = 1e4 * G57_DataManager_1.default.getInstance().coin - p_betScore;
        _data.totalBet = p_betScore;
        _data.totalBonus = 0;
        _data.resultList[0] = new G57_CommonData_1.ResultData();
        _data.resultList[0].randomNumList = [ 5, 4, 3, 20, 20 ];
        _data.resultList[0].totalWinBonus = 0;
        _data.resultList[0].reels = [ [ 3, 2, 4 ], [ 3, 1, 4 ], [ 2, 3, 3 ], [ 20, 3, 4 ], [ 1, 3, 2 ] ];
        _data.resultList[0].scatter = new G57_CommonData_1.ScatterResult();
        _data.resultList[0].scatter.count = 0;
        _data.resultList[0].winWayList[0] = new G57_CommonData_1.WinWay();
        _data.resultList[0].winWayList[0].lineNum = 4;
        return _data;
      };
      G57_TestSocket.prototype.testSpinData_2 = function(p_betScore) {
        var _data = new G57_CommonData_1.BaseGameSpinResponeData();
        var _winScore = p_betScore / 50 * 20 * 1 * 1;
        _data.currentCash = 1e4 * G57_DataManager_1.default.getInstance().coin - p_betScore + _winScore;
        _data.totalBet = p_betScore;
        _data.totalBonus = _winScore;
        _data.resultList[0] = new G57_CommonData_1.ResultData();
        _data.resultList[0].randomNumList = [ 1, 4, 2, 20, 21 ];
        _data.resultList[0].totalWinBonus = _winScore;
        _data.resultList[0].totalHitPos = [ [ 1, 1, 1 ], [ 1, 1, 1 ], [ 1, 1, 1 ], [ 1, 1, 1 ], [ 1, 1, 1 ] ];
        _data.resultList[0].reels = [ [ 3, 2, 4 ], [ 3, 1, 4 ], [ 2, 3, 3 ], [ 20, 3, 4 ], [ 1, 3, 2 ] ];
        _data.resultList[0].scatter = new G57_CommonData_1.ScatterResult();
        _data.resultList[0].scatter.count = 0;
        _data.resultList[0].winWayList[0] = new G57_CommonData_1.WinWay();
        _data.resultList[0].winWayList[0].winBonus = 2e3;
        _data.resultList[0].winWayList[0].lineNum = 4;
        _data.resultList[0].winWayList[0].singlePos = [ [ 1, 1, 1 ], [ 1, 1, 1 ], [ 1, 1, 1 ], [ 1, 1, 1 ], [ 1, 1, 1 ] ];
        return _data;
      };
      G57_TestSocket.prototype.testSpinData_4 = function(p_betScore) {
        var _data = new G57_CommonData_1.BaseGameSpinResponeData();
        var _winScore = p_betScore / 50 * 25e4 * 1 * 1 + p_betScore / 50 * 20 * 1 * 1 + p_betScore / 50 * 15 * 1 * 1 + p_betScore / 50 * 20 * 1 * 1;
        _data.currentCash = 1e4 * G57_DataManager_1.default.getInstance().coin - p_betScore + _winScore;
        _data.totalBet = p_betScore;
        _data.totalBonus = _winScore;
        _data.resultList[0] = new G57_CommonData_1.ResultData();
        _data.resultList[0].randomNumList = [ 5, 4, 3, 2, 2 ];
        _data.resultList[0].specialMultiple = 1;
        _data.resultList[0].reels = [ [ 3, 20, 4 ], [ 3, 20, 4 ], [ 2, 20, 3 ], [ 3, 20, 4 ], [ 1, 20, 2 ] ];
        _data.resultList[0].scatter = new G57_CommonData_1.ScatterResult();
        _data.resultList[0].scatter.count = 6;
        _data.resultList[0].scatter.scatterPos = [ [ 0, 1, 0 ], [ 0, 1, 0 ], [ 0, 1, 0 ], [ 0, 1, 0 ], [ 0, 1, 0 ] ];
        _data.resultList[0].totalHitPos = [ [ 1, 1, 1 ], [ 1, 1, 1 ], [ 1, 1, 1 ], [ 1, 1, 1 ], [ 1, 1, 1 ] ];
        _data.resultList[0].isGetFreeSpins = true;
        _data.resultList[1] = new G57_CommonData_1.ResultData();
        _data.resultList[1].randomNumList = [ 25, 29, 33, 1, 1 ];
        _data.resultList[1].totalWinBonus = 0;
        _data.resultList[1].specialMultiple = 2;
        _data.resultList[1].reels = [ [ 3, 20, 4 ], [ 3, 1, 4 ], [ 2, 20, 3 ], [ 20, 3, 4 ], [ 1, 3, 2 ] ];
        _data.resultList[1].totalHitPos = [ [ 1, 1, 1 ], [ 1, 1, 1 ], [ 1, 1, 1 ], [ 1, 1, 1 ], [ 1, 1, 1 ] ];
        _data.resultList[1].isGetFreeSpins = true;
        _data.resultList[1].scatter = new G57_CommonData_1.ScatterResult();
        _data.resultList[1].scatter.count = 5;
        _data.resultList[1].scatter.scatterPos = [ [ 1, 0, 0 ], [ 0, 1, 0 ], [ 0, 0, 1 ], [ 0, 1, 0 ], [ 0, 1, 0 ] ];
        _data.resultList[1].winWayList[0] = new G57_CommonData_1.WinWay();
        _data.resultList[1].winWayList[0].lineNum = 4;
        _data.resultList[1].winWayList[0].singlePos = [ [ 0, 1, 0 ], [ 0, 1, 0 ], [ 0, 1, 0 ], [ 0, 1, 0 ], [ 0, 1, 0 ] ];
        _data.resultList[2] = new G57_CommonData_1.ResultData();
        _data.resultList[2].randomNumList = [ 25, 29, 33, 1, 1 ];
        _data.resultList[2].totalWinBonus = p_betScore / 50 * 20 * 1 * 1;
        _data.resultList[2].specialMultiple = 3;
        _data.resultList[2].reels = [ [ 3, 2, 4 ], [ 3, 1, 4 ], [ 2, 3, 3 ], [ 20, 3, 4 ], [ 1, 3, 2 ] ];
        _data.resultList[2].totalHitPos = [ [ 1, 1, 1 ], [ 1, 1, 1 ], [ 1, 1, 1 ], [ 1, 1, 1 ], [ 1, 1, 1 ] ];
        _data.resultList[2].scatter = new G57_CommonData_1.ScatterResult();
        _data.resultList[2].scatter.count = 4;
        _data.resultList[2].winWayList[0] = new G57_CommonData_1.WinWay();
        _data.resultList[2].winWayList[0].lineNum = 4;
        _data.resultList[2].winWayList[0].singlePos = [ [ 0, 1, 0 ], [ 0, 1, 0 ], [ 0, 1, 0 ], [ 0, 1, 0 ], [ 0, 1, 0 ] ];
        _data.resultList[3] = new G57_CommonData_1.ResultData();
        _data.resultList[3].randomNumList = [ 25, 29, 33, 1, 1 ];
        _data.resultList[3].totalWinBonus = p_betScore / 50 * 15 * 1 * 1 + p_betScore / 50 * 20 * 1 * 1;
        _data.resultList[3].specialMultiple = 4;
        _data.resultList[3].reels = [ [ 3, 2, 4 ], [ 3, 1, 4 ], [ 2, 3, 3 ], [ 20, 3, 4 ], [ 1, 3, 2 ] ];
        _data.resultList[3].scatter = new G57_CommonData_1.ScatterResult();
        _data.resultList[3].scatter.count = 4;
        _data.resultList[3].totalHitPos = [ [ 1, 1, 1 ], [ 1, 1, 1 ], [ 1, 1, 1 ], [ 1, 1, 1 ], [ 1, 1, 1 ] ];
        _data.resultList[3].winWayList[0] = new G57_CommonData_1.WinWay();
        _data.resultList[3].winWayList[0].lineNum = 4;
        _data.resultList[3].winWayList[0].singlePos = [ [ 0, 1, 0 ], [ 0, 1, 0 ], [ 0, 1, 0 ], [ 0, 1, 0 ], [ 0, 1, 0 ] ];
        _data.resultList[4] = new G57_CommonData_1.ResultData();
        _data.resultList[4].randomNumList = [ 25, 29, 33, 1, 1 ];
        _data.resultList[4].totalWinBonus = 0;
        _data.resultList[4].specialMultiple = 5;
        _data.resultList[4].reels = [ [ 3, 2, 4 ], [ 3, 1, 4 ], [ 2, 3, 3 ], [ 20, 3, 4 ], [ 1, 3, 2 ] ];
        _data.resultList[4].totalHitPos = [ [ 1, 1, 1 ], [ 1, 1, 1 ], [ 1, 1, 1 ], [ 1, 1, 1 ], [ 1, 1, 1 ] ];
        _data.resultList[4].scatter = new G57_CommonData_1.ScatterResult();
        _data.resultList[4].scatter.count = 4;
        _data.resultList[4].winWayList[0] = new G57_CommonData_1.WinWay();
        _data.resultList[4].winWayList[0].lineNum = 4;
        _data.resultList[4].winWayList[0].singlePos = [ [ 0, 1, 0 ], [ 0, 1, 0 ], [ 0, 1, 0 ], [ 0, 1, 0 ], [ 0, 1, 0 ] ];
        _data.resultList[5] = new G57_CommonData_1.ResultData();
        _data.resultList[5].randomNumList = [ 5, 4, 3, 2, 2 ];
        _data.resultList[5].totalWinBonus = 0;
        _data.resultList[5].specialMultiple = 6;
        _data.resultList[5].reels = [ [ 3, 2, 4 ], [ 3, 1, 4 ], [ 2, 3, 3 ], [ 20, 3, 4 ], [ 1, 3, 2 ] ];
        _data.resultList[5].totalHitPos = [ [ 1, 1, 1 ], [ 1, 1, 1 ], [ 1, 1, 1 ], [ 1, 1, 1 ], [ 1, 1, 1 ] ];
        _data.resultList[5].scatter = new G57_CommonData_1.ScatterResult();
        _data.resultList[5].scatter.count = 4;
        _data.resultList[5].winWayList[0] = new G57_CommonData_1.WinWay();
        _data.resultList[5].winWayList[0].lineNum = 4;
        _data.resultList[5].winWayList[0].singlePos = [ [ 0, 1, 0 ], [ 0, 1, 0 ], [ 0, 1, 0 ], [ 0, 1, 0 ], [ 0, 1, 0 ] ];
        return _data;
      };
      G57_TestSocket.prototype.testSpinData_6 = function(p_betScore) {
        var _data = new G57_CommonData_1.BaseGameSpinResponeData();
        var _winScore = 0;
        _data.currentCash = 1e4 * G57_DataManager_1.default.getInstance().coin - p_betScore + _winScore;
        _data.totalBet = p_betScore;
        _data.totalBonus = _winScore;
        _data.resultList[0] = new G57_CommonData_1.ResultData();
        _data.resultList[0].randomNumList = [ 5, 4, 3, 2, 2 ];
        _data.resultList[0].totalWinBonus = p_betScore / 50 * 250 * 1 * 1;
        _data.resultList[0].specialMultiple = 1;
        _data.resultList[0].reels = [ [ 3, 20, 4 ], [ 3, 20, 4 ], [ 2, 20, 3 ], [ 3, 20, 4 ], [ 1, 20, 2 ] ];
        _data.resultList[0].scatter = new G57_CommonData_1.ScatterResult();
        _data.resultList[0].scatter.count = 6;
        _data.resultList[0].scatter.scatterPos = [ [ 0, 1, 0 ], [ 0, 1, 0 ], [ 0, 1, 0 ], [ 0, 1, 0 ], [ 0, 1, 0 ] ];
        return _data;
      };
      G57_TestSocket.prototype.testSpinData_9 = function(p_betScore) {
        var _data = new G57_CommonData_1.BaseGameSpinResponeData();
        var _winScore = p_betScore / 100 * 5900 * 1 * 1;
        _data.currentCash = 1e4 * G57_DataManager_1.default.getInstance().coin - p_betScore + _winScore;
        _data.totalBet = p_betScore;
        _data.totalBonus = _winScore;
        _data.resultList[0] = new G57_CommonData_1.ResultData();
        _data.resultList[0].randomNumList = [ 1, 5, 3, 20, 21 ];
        _data.resultList[0].totalWinBonus = _winScore;
        _data.resultList[0].reels = [ [ 3, 2, 4 ], [ 3, 1, 4 ], [ 2, 3, 3 ], [ 20, 3, 4 ], [ 1, 3, 2 ] ];
        _data.resultList[0].totalHitPos = [ [ 1, 1, 1 ], [ 1, 1, 1 ], [ 1, 1, 1 ], [ 1, 1, 1 ], [ 1, 1, 1 ] ];
        _data.resultList[0].scatter = new G57_CommonData_1.ScatterResult();
        _data.resultList[0].scatter.count = 0;
        _data.resultList[0].winWayList[0] = new G57_CommonData_1.WinWay();
        _data.resultList[0].winWayList[0].winBonus = 29;
        _data.resultList[0].winWayList[0].lineNum = 4;
        _data.resultList[0].winWayList[0].singlePos = [ [ 0, 1, 0 ], [ 0, 1, 0 ], [ 0, 1, 0 ], [ 0, 1, 0 ], [ 0, 1, 0 ] ];
        return _data;
      };
      G57_TestSocket.prototype.testSpinData_10 = function(p_betScore) {
        var _data = new G57_CommonData_1.BaseGameSpinResponeData();
        var _winScore = p_betScore / 100 * 11900 * 1 * 1;
        _data.currentCash = 1e4 * G57_DataManager_1.default.getInstance().coin - p_betScore + _winScore;
        _data.totalBet = p_betScore;
        _data.totalBonus = _winScore;
        _data.resultList[0] = new G57_CommonData_1.ResultData();
        _data.resultList[0].randomNumList = [ 1, 5, 3, 20, 21 ];
        _data.resultList[0].totalWinBonus = _winScore;
        _data.resultList[0].reels = [ [ 3, 2, 4 ], [ 3, 1, 4 ], [ 2, 3, 3 ], [ 20, 3, 4 ], [ 1, 3, 2 ] ];
        _data.resultList[0].totalHitPos = [ [ 1, 1, 1 ], [ 1, 1, 1 ], [ 1, 1, 1 ], [ 1, 1, 1 ], [ 1, 1, 1 ] ];
        _data.resultList[0].scatter = new G57_CommonData_1.ScatterResult();
        _data.resultList[0].scatter.count = 0;
        _data.resultList[0].winWayList[0] = new G57_CommonData_1.WinWay();
        _data.resultList[0].winWayList[0].winBonus = 59;
        _data.resultList[0].winWayList[0].lineNum = 4;
        _data.resultList[0].winWayList[0].singlePos = [ [ 0, 1, 0 ], [ 0, 1, 0 ], [ 0, 1, 0 ], [ 0, 1, 0 ], [ 0, 1, 0 ] ];
        return _data;
      };
      G57_TestSocket.prototype.testSpinData_11 = function(p_betScore) {
        var _data = new G57_CommonData_1.BaseGameSpinResponeData();
        var _winScore = p_betScore / 100 * 2e4 * 1 * 1;
        _data.currentCash = 1e4 * G57_DataManager_1.default.getInstance().coin - p_betScore + _winScore;
        _data.totalBet = p_betScore;
        _data.totalBonus = _winScore;
        _data.resultList[0] = new G57_CommonData_1.ResultData();
        _data.resultList[0].randomNumList = [ 1, 5, 3, 20, 21 ];
        _data.resultList[0].totalWinBonus = _winScore;
        _data.resultList[0].reels = [ [ 3, 2, 4 ], [ 3, 1, 4 ], [ 2, 3, 3 ], [ 20, 3, 4 ], [ 1, 3, 2 ] ];
        _data.resultList[0].totalHitPos = [ [ 1, 1, 1 ], [ 1, 1, 1 ], [ 1, 1, 1 ], [ 1, 1, 1 ], [ 1, 1, 1 ] ];
        _data.resultList[0].scatter = new G57_CommonData_1.ScatterResult();
        _data.resultList[0].scatter.count = 0;
        _data.resultList[0].winWayList[0] = new G57_CommonData_1.WinWay();
        _data.resultList[0].winWayList[0].winBonus = 89;
        _data.resultList[0].winWayList[0].lineNum = 4;
        _data.resultList[0].winWayList[0].singlePos = [ [ 0, 1, 0 ], [ 0, 1, 0 ], [ 0, 1, 0 ], [ 0, 1, 0 ], [ 0, 1, 0 ] ];
        return _data;
      };
      G57_TestSocket.prototype.setOnLeaveEvent = function() {
        return new Promise(function(resolve) {
          resolve();
        });
      };
      var G57_TestSocket_1;
      G57_TestSocket.assignData = -1;
      G57_TestSocket = G57_TestSocket_1 = __decorate([ ccclass ], G57_TestSocket);
      return G57_TestSocket;
    }();
    exports.default = G57_TestSocket;
    cc._RF.pop();
  }, {
    "../../../Game/script/G57_CommonData": "G57_CommonData",
    "../G57_DataManager": "G57_DataManager"
  } ],
  G57_TestUI: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "ab99c0NzV1F+6BmYrsKT4UY", "G57_TestUI");
    "use strict";
    var __extends = this && this.__extends || function() {
      var extendStatics = function(d, b) {
        extendStatics = Object.setPrototypeOf || {
          __proto__: []
        } instanceof Array && function(d, b) {
          d.__proto__ = b;
        } || function(d, b) {
          for (var p in b) Object.prototype.hasOwnProperty.call(b, p) && (d[p] = b[p]);
        };
        return extendStatics(d, b);
      };
      return function(d, b) {
        extendStatics(d, b);
        function __() {
          this.constructor = d;
        }
        d.prototype = null === b ? Object.create(b) : (__.prototype = b.prototype, new __());
      };
    }();
    var __decorate = this && this.__decorate || function(decorators, target, key, desc) {
      var c = arguments.length, r = c < 3 ? target : null === desc ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
      if ("object" === typeof Reflect && "function" === typeof Reflect.decorate) r = Reflect.decorate(decorators, target, key, desc); else for (var i = decorators.length - 1; i >= 0; i--) (d = decorators[i]) && (r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r);
      return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    var DesktopBrowserTransform_1 = require("../../../Common/Tools/DesktopBrowserTransform/DesktopBrowserTransform");
    var G57_DataManager_1 = require("../../Common/script/G57_DataManager");
    var G57_TestSocket_1 = require("../../Common/script/socket/G57_TestSocket");
    var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
    var G57_TestUI = function(_super) {
      __extends(G57_TestUI, _super);
      function G57_TestUI() {
        var _this = null !== _super && _super.apply(this, arguments) || this;
        _this.menuNode = null;
        _this.gameSpeedEditBox = null;
        return _this;
      }
      G57_TestUI.prototype.start = function() {
        if (G57_DataManager_1.default.offLineMode) {
          this.node.setPosition(cc.Vec2.ZERO);
          this.menuNode.active = false;
        } else this.node.active = false;
      };
      G57_TestUI.prototype.setTestData = function(event, customEventData) {
        console.log("\u4f7f\u7528\u6e2c\u8a66\u8cc7\u6599 " + customEventData);
        G57_TestSocket_1.default.assignData = Number(customEventData);
        this.closeMenu();
      };
      G57_TestUI.prototype.setGameSpeed = function() {
        DesktopBrowserTransform_1.default.getInstance().setGameSpeed(Number(this.gameSpeedEditBox.string));
      };
      G57_TestUI.prototype.showMenu = function() {
        this.menuNode.active = true;
      };
      G57_TestUI.prototype.closeMenu = function() {
        console.log("\u95dc\u9589\u6e2c\u8a66UI");
        this.menuNode.active = false;
      };
      __decorate([ property(cc.Node) ], G57_TestUI.prototype, "menuNode", void 0);
      __decorate([ property(cc.EditBox) ], G57_TestUI.prototype, "gameSpeedEditBox", void 0);
      G57_TestUI = __decorate([ ccclass ], G57_TestUI);
      return G57_TestUI;
    }(cc.Component);
    exports.default = G57_TestUI;
    cc._RF.pop();
  }, {
    "../../../Common/Tools/DesktopBrowserTransform/DesktopBrowserTransform": void 0,
    "../../Common/script/G57_DataManager": "G57_DataManager",
    "../../Common/script/socket/G57_TestSocket": "G57_TestSocket"
  } ],
  G57_WinScorePanel: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "c5c28TjQ1tAwL4fYS6wFux8", "G57_WinScorePanel");
    "use strict";
    var __extends = this && this.__extends || function() {
      var extendStatics = function(d, b) {
        extendStatics = Object.setPrototypeOf || {
          __proto__: []
        } instanceof Array && function(d, b) {
          d.__proto__ = b;
        } || function(d, b) {
          for (var p in b) Object.prototype.hasOwnProperty.call(b, p) && (d[p] = b[p]);
        };
        return extendStatics(d, b);
      };
      return function(d, b) {
        extendStatics(d, b);
        function __() {
          this.constructor = d;
        }
        d.prototype = null === b ? Object.create(b) : (__.prototype = b.prototype, new __());
      };
    }();
    var __decorate = this && this.__decorate || function(decorators, target, key, desc) {
      var c = arguments.length, r = c < 3 ? target : null === desc ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
      if ("object" === typeof Reflect && "function" === typeof Reflect.decorate) r = Reflect.decorate(decorators, target, key, desc); else for (var i = decorators.length - 1; i >= 0; i--) (d = decorators[i]) && (r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r);
      return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    var FXController_1 = require("../../../../Common/Optional/Crosis/FXController/FXController");
    var AudioManager_1 = require("../../../../Common/Tools/AudioManager/AudioManager");
    var Slot_WinScorePanel_1 = require("../../../../SlotFramework/Game/Panel/Slot_WinScorePanel");
    var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
    var G57_WinScorePanel = function(_super) {
      __extends(G57_WinScorePanel, _super);
      function G57_WinScorePanel() {
        var _this = null !== _super && _super.apply(this, arguments) || this;
        _this.winGroup = [];
        _this.background = null;
        _this.animationGroup = [];
        _this.particalGroup = [];
        _this.closeWinEffectButton = null;
        _this.curLevel = 0;
        _this.isShowEndScore = false;
        return _this;
      }
      G57_WinScorePanel.prototype.init = function() {
        _super.prototype.init.call(this);
        this.node.opacity = 0;
      };
      G57_WinScorePanel.prototype.open = function(p_totalWinScore, p_betScore, p_isFreeGame) {
        void 0 === p_isFreeGame && (p_isFreeGame = false);
        this.curLevel = 0;
        this.background.opacity = 220;
        this.closeWinEffectButton.interactable = true;
        this.isShowEndScore = false;
        this.particalGroup.forEach(function(item) {
          return item.opacity = 73;
        });
        _super.prototype.open.call(this, p_totalWinScore, p_betScore, p_isFreeGame);
        cc.tween(this.node).to(.5, {
          opacity: 255
        }).start();
      };
      G57_WinScorePanel.prototype.close = function() {
        var _this = this;
        this.node.stopAllActions();
        this.closeWinEffectButton.interactable = false;
        this.particalGroup.forEach(function(item) {
          return item.stopAllActions();
        });
        cc.tween(this.node).to(.5, {
          opacity: 0
        }).call(function() {
          _super.prototype.close.call(_this);
        }).start();
      };
      G57_WinScorePanel.prototype.showEndScore = function() {
        this.isShowEndScore = true;
        _super.prototype.showEndScore.call(this);
      };
      G57_WinScorePanel.prototype.showBigWinEffect = function() {
        console.log("\u3010Big Win\u3011");
        _super.prototype.showBigWinEffect.call(this);
        this.closeAllWinEffect();
        this.winGroup[0].play();
        AudioManager_1.AudioManager.instance.playAudioEvent("WinScorePanelBigWinEffect");
      };
      G57_WinScorePanel.prototype.showSuperWinEffect = function() {
        var _this = this;
        console.log("\u3010Super Win\u3011 ");
        this.particalGroup[this.curLevel].stopAllActions();
        _super.prototype.showSuperWinEffect.call(this);
        this.animationGroup[this.curLevel].playClips(1);
        if (this.isShowEndScore) {
          this.animationGroup[this.curLevel].setCurrentTime(1);
          this.particalGroup[this.curLevel].opacity = 0;
          this.curLevel = 1;
          this.winGroup[this.curLevel].play();
          AudioManager_1.AudioManager.instance.playAudioEvent("WinScorePanelSuperWinEffect");
        } else {
          cc.tween(this.particalGroup[this.curLevel]).to(1, {
            opacity: 0
          }).start();
          this.animationGroup[this.curLevel].once("finished", function() {
            _this.curLevel = 1;
            _this.winGroup[_this.curLevel].play();
            AudioManager_1.AudioManager.instance.playAudioEvent("WinScorePanelSuperWinEffect");
          });
        }
      };
      G57_WinScorePanel.prototype.showMegaWinEffect = function() {
        var _this = this;
        if (this.winScoreType === Slot_WinScorePanel_1.WIN_SCORE_TYPE.MEGA_WIN) return;
        console.log("\u3010Mega Win\u3011");
        this.particalGroup[this.curLevel].stopAllActions();
        _super.prototype.showMegaWinEffect.call(this);
        this.animationGroup[this.curLevel].playClips(1);
        if (this.isShowEndScore) {
          this.animationGroup[this.curLevel].setCurrentTime(1);
          this.particalGroup[this.curLevel].opacity = 0;
          this.curLevel = 2;
          this.winGroup[this.curLevel].play();
          AudioManager_1.AudioManager.instance.playAudioEvent("WinScorePanelMegaWinEffect");
        } else {
          cc.tween(this.particalGroup[this.curLevel]).to(1, {
            opacity: 0
          }).start();
          this.animationGroup[this.curLevel].once("finished", function() {
            _this.curLevel = 2;
            _this.winGroup[_this.curLevel].play();
            AudioManager_1.AudioManager.instance.playAudioEvent("WinScorePanelMegaWinEffect");
          });
        }
      };
      G57_WinScorePanel.prototype.runScoreEnd = function() {
        _super.prototype.runScoreEnd.call(this);
      };
      G57_WinScorePanel.prototype.closeAllWinEffect = function() {
        for (var i = 0; i < this.winGroup.length; i++) this.winGroup[i].stop();
      };
      __decorate([ property([ FXController_1.default ]) ], G57_WinScorePanel.prototype, "winGroup", void 0);
      __decorate([ property(cc.Node) ], G57_WinScorePanel.prototype, "background", void 0);
      __decorate([ property([ cc.Animation ]) ], G57_WinScorePanel.prototype, "animationGroup", void 0);
      __decorate([ property([ cc.Node ]) ], G57_WinScorePanel.prototype, "particalGroup", void 0);
      __decorate([ property(cc.Button) ], G57_WinScorePanel.prototype, "closeWinEffectButton", void 0);
      G57_WinScorePanel = __decorate([ ccclass ], G57_WinScorePanel);
      return G57_WinScorePanel;
    }(Slot_WinScorePanel_1.default);
    exports.default = G57_WinScorePanel;
    cc._RF.pop();
  }, {
    "../../../../Common/Optional/Crosis/FXController/FXController": void 0,
    "../../../../Common/Tools/AudioManager/AudioManager": void 0,
    "../../../../SlotFramework/Game/Panel/Slot_WinScorePanel": void 0
  } ]
}, {}, [ "G57_LoadingItem", "G57_CollectFXPool", "G57_DataManager", "G57_DynamicPopUpPanelManager", "G57_SocketConnect", "G57_SocketManager", "G57_TestSocket", "G57_CommonData", "G57_Game", "G57_TestUI", "G57_AutoGamePanel", "G57_FreeGameGetScorePanel", "G57_FreeGamePanel", "G57_GameUI", "G57_MusicOptionPanel", "G57_Reel", "G57_RulePanel", "G57_SlotReelManager", "G57_Symbol", "G57_SymbolTipPanel", "G57_WinScorePanel", "G57_Loading_InitData", "G57_Loading", "G57_LoadingUI", "G57_Lobby", "G57_LobbyUI", "G57_Language" ]);