window.__require = function e(t, n, r) {
  function s(o, u) {
    if (!n[o]) {
      if (!t[o]) {
        var b = o.split("/");
        b = b[b.length - 1];
        if (!t[b]) {
          var a = "function" == typeof __require && __require;
          if (!u && a) return a(b, !0);
          if (i) return i(b, !0);
          throw new Error("Cannot find module '" + o + "'");
        }
        o = b;
      }
      var f = n[o] = {
        exports: {}
      };
      t[o][0].call(f.exports, function(e) {
        var n = t[o][1][e];
        return s(n || e);
      }, f, f.exports, e, t, n, r);
    }
    return n[o].exports;
  }
  var i = "function" == typeof __require && __require;
  for (var o = 0; o < r.length; o++) s(r[o]);
  return s;
}({
  G27_AutoGamePanel: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "b5792nLYwxDBqjTtq8+PKHk", "G27_AutoGamePanel");
    "use strict";
    var __extends = this && this.__extends || function() {
      var extendStatics = function(d, b) {
        extendStatics = Object.setPrototypeOf || {
          __proto__: []
        } instanceof Array && function(d, b) {
          d.__proto__ = b;
        } || function(d, b) {
          for (var p in b) Object.prototype.hasOwnProperty.call(b, p) && (d[p] = b[p]);
        };
        return extendStatics(d, b);
      };
      return function(d, b) {
        extendStatics(d, b);
        function __() {
          this.constructor = d;
        }
        d.prototype = null === b ? Object.create(b) : (__.prototype = b.prototype, new __());
      };
    }();
    var __decorate = this && this.__decorate || function(decorators, target, key, desc) {
      var c = arguments.length, r = c < 3 ? target : null === desc ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
      if ("object" === typeof Reflect && "function" === typeof Reflect.decorate) r = Reflect.decorate(decorators, target, key, desc); else for (var i = decorators.length - 1; i >= 0; i--) (d = decorators[i]) && (r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r);
      return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    var Slot_AutoGamePanel_1 = require("../../../SlotFramework/Game/Panel/Slot_AutoGamePanel");
    var G27_DataManager_1 = require("../../Common/G27_DataManager");
    var ccclass = cc._decorator.ccclass;
    var G27_AutoGamePanel = function(_super) {
      __extends(G27_AutoGamePanel, _super);
      function G27_AutoGamePanel() {
        var _this = null !== _super && _super.apply(this, arguments) || this;
        _this.data = null;
        return _this;
      }
      G27_AutoGamePanel.prototype.init = function() {
        this.data = G27_DataManager_1.default.getInstance();
        _super.prototype.init.call(this);
      };
      G27_AutoGamePanel = __decorate([ ccclass ], G27_AutoGamePanel);
      return G27_AutoGamePanel;
    }(Slot_AutoGamePanel_1.default);
    exports.default = G27_AutoGamePanel;
    cc._RF.pop();
  }, {
    "../../../SlotFramework/Game/Panel/Slot_AutoGamePanel": void 0,
    "../../Common/G27_DataManager": "G27_DataManager"
  } ],
  G27_CommonData: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "f5bb64WCrlK6JEc6QtnzCCZ", "G27_CommonData");
    "use strict";
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    exports.FastChoseRoomData = exports.ScatterResult = exports.WinWayData = exports.ResultData = exports.BaseGameSpinResponeData = exports.RoomData = exports.GetRoomListData = exports.ReelTable = exports.GetInitialReelInfoData = exports.LoginData = void 0;
    var LoginData = function() {
      function LoginData() {
        this.nickname = "";
        this.language = "";
        this.balance = 0;
        this.timestamp = 0;
        this.themeType = 0;
        this.icon = 0;
        this.playerId = "";
        this.gameId = 0;
      }
      return LoginData;
    }();
    exports.LoginData = LoginData;
    var GetInitialReelInfoData = function() {
      function GetInitialReelInfoData() {
        this.reels = null;
        this.position = [];
        this.wheelRatioList = [];
      }
      return GetInitialReelInfoData;
    }();
    exports.GetInitialReelInfoData = GetInitialReelInfoData;
    var ReelTable = function() {
      function ReelTable() {
        this.BG = [];
        this.FG = [];
      }
      return ReelTable;
    }();
    exports.ReelTable = ReelTable;
    var GetRoomListData = function() {
      function GetRoomListData() {
        this.rooms = [];
      }
      return GetRoomListData;
    }();
    exports.GetRoomListData = GetRoomListData;
    var RoomData = function() {
      function RoomData() {
        this.roomId = "";
        this.status = 0;
      }
      return RoomData;
    }();
    exports.RoomData = RoomData;
    var BaseGameSpinResponeData = function() {
      function BaseGameSpinResponeData() {
        this.currentCash = 0;
        this.totalBonus = 0;
        this.totalBet = 0;
        this.allReels = [];
        this.resultList = [];
        this.Jackpot = null;
      }
      return BaseGameSpinResponeData;
    }();
    exports.BaseGameSpinResponeData = BaseGameSpinResponeData;
    var ResultData = function() {
      function ResultData() {
        this.changeSymbol = 0;
        this.changePos = [ [ 0, 0, 0, 0 ], [ 0, 0, 0, 0 ], [ 0, 0, 0, 0 ], [ 0, 0, 0, 0 ], [ 0, 0, 0, 0 ] ];
        this.jump = [];
        this.randomNumList = [];
        this.totalWinBonus = 0;
        this.wheelIdx = 0;
        this.wheelBonus = 0;
        this.winWayList = [];
        this.totalHitPos = [ [ 0, 0, 0, 0 ], [ 0, 0, 0, 0 ], [ 0, 0, 0, 0 ], [ 0, 0, 0, 0 ], [ 0, 0, 0, 0 ] ];
        this.scatter = new ScatterResult();
      }
      return ResultData;
    }();
    exports.ResultData = ResultData;
    var WinWayData = function() {
      function WinWayData() {
        this.x = 0;
        this.winNum = 0;
        this.winBonus = 0;
        this.lineNum = 0;
        this.singlePos = [ [ 0, 0, 0, 0 ], [ 0, 0, 0, 0 ], [ 0, 0, 0, 0 ], [ 0, 0, 0, 0 ], [ 0, 0, 0, 0 ] ];
      }
      return WinWayData;
    }();
    exports.WinWayData = WinWayData;
    var ScatterResult = function() {
      function ScatterResult() {
        this.winNum = 0;
        this.count = 0;
        this.winBonus = 0;
        this.scatterPos = [ [ 0, 0, 0, 0 ], [ 0, 0, 0, 0 ], [ 0, 0, 0, 0 ], [ 0, 0, 0, 0 ], [ 0, 0, 0, 0 ] ];
      }
      return ScatterResult;
    }();
    exports.ScatterResult = ScatterResult;
    var FastChoseRoomData = function() {
      function FastChoseRoomData() {}
      return FastChoseRoomData;
    }();
    exports.FastChoseRoomData = FastChoseRoomData;
    cc._RF.pop();
  }, {} ],
  G27_DataManager: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "66a65likcxE7Irhgyos5Hus", "G27_DataManager");
    "use strict";
    var __extends = this && this.__extends || function() {
      var extendStatics = function(d, b) {
        extendStatics = Object.setPrototypeOf || {
          __proto__: []
        } instanceof Array && function(d, b) {
          d.__proto__ = b;
        } || function(d, b) {
          for (var p in b) Object.prototype.hasOwnProperty.call(b, p) && (d[p] = b[p]);
        };
        return extendStatics(d, b);
      };
      return function(d, b) {
        extendStatics(d, b);
        function __() {
          this.constructor = d;
        }
        d.prototype = null === b ? Object.create(b) : (__.prototype = b.prototype, new __());
      };
    }();
    var __decorate = this && this.__decorate || function(decorators, target, key, desc) {
      var c = arguments.length, r = c < 3 ? target : null === desc ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
      if ("object" === typeof Reflect && "function" === typeof Reflect.decorate) r = Reflect.decorate(decorators, target, key, desc); else for (var i = decorators.length - 1; i >= 0; i--) (d = decorators[i]) && (r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r);
      return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    var Slot_DataManager_1 = require("../../SlotFramework/Slot_DataManager");
    var ccclass = cc._decorator.ccclass;
    var G27_DataManager = function(_super) {
      __extends(G27_DataManager, _super);
      function G27_DataManager() {
        var _this = null !== _super && _super.apply(this, arguments) || this;
        _this.gameVersion = "Dev 0.1.1";
        _this.SDKVersion = "Dev 0.1.1";
        _this.isShowDrawcall = false;
        _this.resultList = [];
        _this.lineTable = [ [ 1, 1, 1, 1, 1 ], [ 0, 0, 0, 0, 0 ], [ 2, 2, 2, 2, 2 ], [ 0, 1, 2, 1, 0 ], [ 2, 1, 0, 1, 2 ], [ 0, 0, 1, 0, 0 ], [ 2, 2, 1, 2, 2 ], [ 1, 2, 2, 2, 1 ], [ 1, 0, 0, 0, 1 ], [ 0, 1, 1, 1, 0 ], [ 2, 1, 1, 1, 2 ], [ 1, 1, 0, 1, 1 ], [ 1, 1, 2, 1, 1 ], [ 0, 2, 2, 2, 0 ], [ 2, 0, 0, 0, 2 ], [ 1, 0, 1, 0, 1 ], [ 1, 2, 1, 2, 1 ], [ 0, 2, 0, 2, 0 ], [ 2, 0, 2, 0, 2 ], [ 0, 1, 0, 1, 0 ], [ 2, 1, 2, 1, 2 ], [ 0, 2, 1, 2, 0 ], [ 2, 0, 1, 0, 2 ], [ 1, 0, 2, 0, 1 ], [ 1, 2, 0, 2, 1 ], [ 0, 0, 2, 0, 0 ], [ 2, 2, 0, 2, 2 ], [ 0, 0, 0, 0, 1 ], [ 2, 2, 2, 2, 1 ], [ 1, 1, 1, 1, 0 ] ];
        _this.linePositionJson = '{"line":[{"pos":[{"x":-360,"y":60},{"x":-280,"y":60},{"x":-146.66666666666666,"y":60},{"x":0,"y":60},{"x":146.66666666666666,"y":60},{"x":280,"y":60},{"x":360,"y":60}],"tip":"L"},{"pos":[{"x":-360,"y":233.33333333333334},{"x":-280,"y":233.33333333333334},{"x":-146.66666666666666,"y":233.33333333333334},{"x":0,"y":233.33333333333334},{"x":146.66666666666666,"y":233.33333333333334},{"x":280,"y":233.33333333333334},{"x":360,"y":233.33333333333334}],"tip":"L"},{"pos":[{"x":-360,"y":-233.33333333333334},{"x":-280,"y":-233.33333333333334},{"x":-146.66666666666666,"y":-233.33333333333334},{"x":0,"y":-233.33333333333334},{"x":146.66666666666666,"y":-233.33333333333334},{"x":280,"y":-233.33333333333334},{"x":360,"y":-233.33333333333334}],"tip":"R"},{"pos":[{"x":-360,"y":220},{"x":-280,"y":220},{"x":-146.66666666666666,"y":6.666666666666667},{"x":0,"y":-206.66666666666666},{"x":146.66666666666666,"y":6.666666666666667},{"x":280,"y":220},{"x":360,"y":220}],"tip":"R"},{"pos":[{"x":-360,"y":-220},{"x":-280,"y":-220},{"x":-146.66666666666666,"y":-20},{"x":0,"y":220},{"x":146.66666666666666,"y":-20},{"x":280,"y":-220},{"x":360,"y":-220}],"tip":"L"},{"pos":[{"x":-360,"y":206.66666666666666},{"x":-280,"y":206.66666666666666},{"x":-146.66666666666666,"y":206.66666666666666},{"x":0,"y":20},{"x":146.66666666666666,"y":206.66666666666666},{"x":280,"y":206.66666666666666},{"x":360,"y":206.66666666666666}],"tip":"L"},{"pos":[{"x":-360,"y":-206.66666666666666},{"x":-280,"y":-206.66666666666666},{"x":-146.66666666666666,"y":-206.66666666666666},{"x":0,"y":-46.666666666666664},{"x":146.66666666666666,"y":-206.66666666666666},{"x":280,"y":-206.66666666666666},{"x":360,"y":-206.66666666666666}],"tip":"R"},{"pos":[{"x":-360,"y":46.666666666666664},{"x":-280,"y":46.666666666666664},{"x":-146.66666666666666,"y":-193.33333333333334},{"x":0,"y":-193.33333333333334},{"x":146.66666666666666,"y":-193.33333333333334},{"x":280,"y":46.666666666666664},{"x":360,"y":46.666666666666664}],"tip":"R"},{"pos":[{"x":-360,"y":33.333333333333336},{"x":-280,"y":33.333333333333336},{"x":-146.66666666666666,"y":153.33333333333334},{"x":0,"y":153.33333333333334},{"x":146.66666666666666,"y":153.33333333333334},{"x":280,"y":33.333333333333336},{"x":360,"y":33.333333333333336}],"tip":"L"},{"pos":[{"x":-360,"y":153.33333333333334},{"x":-280,"y":153.33333333333334},{"x":-146.66666666666666,"y":33.333333333333336},{"x":0,"y":33.333333333333336},{"x":146.66666666666666,"y":33.333333333333336},{"x":280,"y":153.33333333333334},{"x":360,"y":153.33333333333334}],"tip":"L"},{"pos":[{"x":-360,"y":-193.33333333333334},{"x":-280,"y":-193.33333333333334},{"x":-146.66666666666666,"y":46.666666666666664},{"x":0,"y":46.666666666666664},{"x":146.66666666666666,"y":46.666666666666664},{"x":280,"y":-193.33333333333334},{"x":360,"y":-193.33333333333334}],"tip":"L"},{"pos":[{"x":-360,"y":20},{"x":-280,"y":20},{"x":-146.66666666666666,"y":20},{"x":0,"y":140},{"x":146.66666666666666,"y":20},{"x":280,"y":20},{"x":360,"y":20}],"tip":"R"},{"pos":[{"x":-360,"y":-6.666666666666667},{"x":-280,"y":-6.666666666666667},{"x":-146.66666666666666,"y":-6.666666666666667},{"x":0,"y":-220},{"x":146.66666666666666,"y":-6.666666666666667},{"x":280,"y":-6.666666666666667},{"x":360,"y":-6.666666666666667}],"tip":"R"},{"pos":[{"x":-360,"y":113.33333333333333},{"x":-280,"y":113.33333333333333},{"x":-146.66666666666666,"y":-113.33333333333333},{"x":0,"y":-113.33333333333333},{"x":146.66666666666666,"y":-113.33333333333333},{"x":280,"y":113.33333333333333},{"x":360,"y":113.33333333333333}],"tip":"R"},{"pos":[{"x":-360,"y":-113.33333333333333},{"x":-280,"y":-113.33333333333333},{"x":-146.66666666666666,"y":113.33333333333333},{"x":0,"y":113.33333333333333},{"x":146.66666666666666,"y":113.33333333333333},{"x":280,"y":-113.33333333333333},{"x":360,"y":-113.33333333333333}],"tip":"L"},{"pos":[{"x":-360,"y":-20},{"x":-280,"y":-20},{"x":-146.66666666666666,"y":180},{"x":0,"y":-20},{"x":146.66666666666666,"y":180},{"x":280,"y":-20},{"x":360,"y":-20}],"tip":"L"},{"pos":[{"x":-360,"y":6.666666666666667},{"x":-280,"y":6.666666666666667},{"x":-146.66666666666666,"y":-220},{"x":0,"y":6.666666666666667},{"x":146.66666666666666,"y":-220},{"x":280,"y":6.666666666666667},{"x":360,"y":6.666666666666667}],"tip":"L"},{"pos":[{"x":-360,"y":166.66666666666666},{"x":-280,"y":166.66666666666666},{"x":-146.66666666666666,"y":-126.66666666666667},{"x":0,"y":166.66666666666666},{"x":146.66666666666666,"y":-126.66666666666667},{"x":280,"y":166.66666666666666},{"x":360,"y":166.66666666666666}],"tip":"R"},{"pos":[{"x":-360,"y":-126.66666666666667},{"x":-280,"y":-126.66666666666667},{"x":-146.66666666666666,"y":166.66666666666666},{"x":0,"y":-126.66666666666667},{"x":146.66666666666666,"y":166.66666666666666},{"x":280,"y":-126.66666666666667},{"x":360,"y":-126.66666666666667}],"tip":"R"},{"pos":[{"x":-360,"y":180},{"x":-280,"y":180},{"x":-146.66666666666666,"y":-46.666666666666664},{"x":0,"y":180},{"x":146.66666666666666,"y":-46.666666666666664},{"x":280,"y":180},{"x":360,"y":180}],"tip":"L"},{"pos":[{"x":-360,"y":-180},{"x":-280,"y":-180},{"x":-146.66666666666666,"y":-33.333333333333336},{"x":0,"y":-180},{"x":146.66666666666666,"y":-33.333333333333336},{"x":280,"y":-180},{"x":360,"y":-180}],"tip":"R"},{"pos":[{"x":-360,"y":140},{"x":-280,"y":140},{"x":-146.66666666666666,"y":-140},{"x":0,"y":-33.333333333333336},{"x":146.66666666666666,"y":-140},{"x":280,"y":140},{"x":360,"y":140}],"tip":"R"},{"pos":[{"x":-360,"y":-140},{"x":-280,"y":-140},{"x":-146.66666666666666,"y":140},{"x":0,"y":-6.666666666666667},{"x":146.66666666666666,"y":140},{"x":280,"y":-140},{"x":360,"y":-140}],"tip":"L"},{"pos":[{"x":-360,"y":-33.333333333333336},{"x":-280,"y":-33.333333333333336},{"x":-146.66666666666666,"y":220},{"x":0,"y":-140},{"x":146.66666666666666,"y":220},{"x":280,"y":-33.333333333333336},{"x":360,"y":-33.333333333333336}],"tip":"R"},{"pos":[{"x":-360,"y":-46.666666666666664},{"x":-280,"y":-46.666666666666664},{"x":-146.66666666666666,"y":-180},{"x":0,"y":206.66666666666666},{"x":146.66666666666666,"y":-180},{"x":280,"y":-46.666666666666664},{"x":360,"y":-46.666666666666664}],"tip":"L"},{"pos":[{"x":-360,"y":126.66666666666667},{"x":-280,"y":126.66666666666667},{"x":-146.66666666666666,"y":126.66666666666667},{"x":0,"y":-153.33333333333334},{"x":146.66666666666666,"y":126.66666666666667},{"x":280,"y":126.66666666666667},{"x":360,"y":126.66666666666667}],"tip":"L"},{"pos":[{"x":-360,"y":-153.33333333333334},{"x":-280,"y":-153.33333333333334},{"x":-146.66666666666666,"y":-153.33333333333334},{"x":0,"y":126.66666666666667},{"x":146.66666666666666,"y":-153.33333333333334},{"x":280,"y":-153.33333333333334},{"x":360,"y":-153.33333333333334}],"tip":"R"},{"pos":[{"x":-360,"y":193.33333333333334},{"x":-280,"y":193.33333333333334},{"x":-146.66666666666666,"y":193.33333333333334},{"x":0,"y":193.33333333333334},{"x":146.66666666666666,"y":193.33333333333334},{"x":280,"y":-60},{"x":360,"y":-60}],"tip":"R"},{"pos":[{"x":-360,"y":-166.66666666666666},{"x":-280,"y":-166.66666666666666},{"x":-146.66666666666666,"y":-166.66666666666666},{"x":0,"y":-166.66666666666666},{"x":146.66666666666666,"y":-166.66666666666666},{"x":280,"y":-60},{"x":360,"y":-60}],"tip":"L"},{"pos":[{"x":-360,"y":-60},{"x":-280,"y":-60},{"x":-146.66666666666666,"y":-60},{"x":0,"y":-60},{"x":146.66666666666666,"y":-60},{"x":280,"y":193.33333333333334},{"x":360,"y":193.33333333333334}],"tip":"R"}]}';
        return _this;
      }
      G27_DataManager.prototype.init = function() {
        _super.prototype.init.call(this);
        this.gameID = 27;
        this.path = "Resources";
        this.skin = "Normal";
        this.setMagnificationTable();
      };
      G27_DataManager.prototype.setMagnificationTable = function() {
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H1] = {};
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H1][3] = 25;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H1][4] = 100;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H1][5] = 200;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H2] = {};
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H2][3] = 20;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H2][4] = 75;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H2][5] = 150;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H3] = {};
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H3][3] = 15;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H3][4] = 60;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H3][5] = 100;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H4] = {};
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H4][3] = 8;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H4][4] = 30;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H4][5] = 75;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H5] = {};
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H5][3] = 8;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H5][4] = 30;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H5][5] = 75;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.N1] = {};
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.N1][3] = 5;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.N1][4] = 25;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.N1][5] = 60;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.N2] = {};
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.N2][3] = 5;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.N2][4] = 25;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.N2][5] = 60;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.N3] = {};
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.N3][3] = 2;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.N3][4] = 15;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.N3][5] = 50;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.N4] = {};
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.N4][3] = 2;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.N4][4] = 15;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.N4][5] = 50;
      };
      G27_DataManager = __decorate([ ccclass ], G27_DataManager);
      return G27_DataManager;
    }(Slot_DataManager_1.default);
    exports.default = G27_DataManager;
    cc._RF.pop();
  }, {
    "../../SlotFramework/Slot_DataManager": void 0
  } ],
  G27_DynamicPopUpPanelManager: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "e1857XrQQ1AGpNP7uZcnOTY", "G27_DynamicPopUpPanelManager");
    "use strict";
    var __extends = this && this.__extends || function() {
      var extendStatics = function(d, b) {
        extendStatics = Object.setPrototypeOf || {
          __proto__: []
        } instanceof Array && function(d, b) {
          d.__proto__ = b;
        } || function(d, b) {
          for (var p in b) Object.prototype.hasOwnProperty.call(b, p) && (d[p] = b[p]);
        };
        return extendStatics(d, b);
      };
      return function(d, b) {
        extendStatics(d, b);
        function __() {
          this.constructor = d;
        }
        d.prototype = null === b ? Object.create(b) : (__.prototype = b.prototype, new __());
      };
    }();
    var __values = this && this.__values || function(o) {
      var s = "function" === typeof Symbol && Symbol.iterator, m = s && o[s], i = 0;
      if (m) return m.call(o);
      if (o && "number" === typeof o.length) return {
        next: function() {
          o && i >= o.length && (o = void 0);
          return {
            value: o && o[i++],
            done: !o
          };
        }
      };
      throw new TypeError(s ? "Object is not iterable." : "Symbol.iterator is not defined.");
    };
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    var Slot_DynamicPopUpManager_1 = require("../../SlotFramework/Common/DynamicPopUp/Slot_DynamicPopUpManager");
    var G27_DynamicPopUpPanelManager = function(_super) {
      __extends(G27_DynamicPopUpPanelManager, _super);
      function G27_DynamicPopUpPanelManager() {
        return null !== _super && _super.apply(this, arguments) || this;
      }
      G27_DynamicPopUpPanelManager.prototype.prestore = function(prefabs) {
        var e_1, _a;
        try {
          for (var prefabs_1 = __values(prefabs), prefabs_1_1 = prefabs_1.next(); !prefabs_1_1.done; prefabs_1_1 = prefabs_1.next()) {
            var prefab = prefabs_1_1.value;
            prefab.name;
            _super.prototype.prestore.call(this, [ prefab ]);
          }
        } catch (e_1_1) {
          e_1 = {
            error: e_1_1
          };
        } finally {
          try {
            prefabs_1_1 && !prefabs_1_1.done && (_a = prefabs_1.return) && _a.call(prefabs_1);
          } finally {
            if (e_1) throw e_1.error;
          }
        }
      };
      return G27_DynamicPopUpPanelManager;
    }(Slot_DynamicPopUpManager_1.default);
    exports.default = G27_DynamicPopUpPanelManager;
    cc._RF.pop();
  }, {
    "../../SlotFramework/Common/DynamicPopUp/Slot_DynamicPopUpManager": void 0
  } ],
  G27_FXWarning: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "c15b1NHHUhAWJcxMlw6knJq", "G27_FXWarning");
    "use strict";
    var __extends = this && this.__extends || function() {
      var extendStatics = function(d, b) {
        extendStatics = Object.setPrototypeOf || {
          __proto__: []
        } instanceof Array && function(d, b) {
          d.__proto__ = b;
        } || function(d, b) {
          for (var p in b) Object.prototype.hasOwnProperty.call(b, p) && (d[p] = b[p]);
        };
        return extendStatics(d, b);
      };
      return function(d, b) {
        extendStatics(d, b);
        function __() {
          this.constructor = d;
        }
        d.prototype = null === b ? Object.create(b) : (__.prototype = b.prototype, new __());
      };
    }();
    var __decorate = this && this.__decorate || function(decorators, target, key, desc) {
      var c = arguments.length, r = c < 3 ? target : null === desc ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
      if ("object" === typeof Reflect && "function" === typeof Reflect.decorate) r = Reflect.decorate(decorators, target, key, desc); else for (var i = decorators.length - 1; i >= 0; i--) (d = decorators[i]) && (r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r);
      return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    var ccclass = cc._decorator.ccclass;
    var G27_FXWarning = function(_super) {
      __extends(G27_FXWarning, _super);
      function G27_FXWarning() {
        return null !== _super && _super.apply(this, arguments) || this;
      }
      G27_FXWarning = __decorate([ ccclass ], G27_FXWarning);
      return G27_FXWarning;
    }(cc.Component);
    exports.default = G27_FXWarning;
    cc._RF.pop();
  }, {} ],
  G27_FreeGameGetScorePanel: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "1eb30xrb31G8qHd1YUhV6jd", "G27_FreeGameGetScorePanel");
    "use strict";
    var __extends = this && this.__extends || function() {
      var extendStatics = function(d, b) {
        extendStatics = Object.setPrototypeOf || {
          __proto__: []
        } instanceof Array && function(d, b) {
          d.__proto__ = b;
        } || function(d, b) {
          for (var p in b) Object.prototype.hasOwnProperty.call(b, p) && (d[p] = b[p]);
        };
        return extendStatics(d, b);
      };
      return function(d, b) {
        extendStatics(d, b);
        function __() {
          this.constructor = d;
        }
        d.prototype = null === b ? Object.create(b) : (__.prototype = b.prototype, new __());
      };
    }();
    var __decorate = this && this.__decorate || function(decorators, target, key, desc) {
      var c = arguments.length, r = c < 3 ? target : null === desc ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
      if ("object" === typeof Reflect && "function" === typeof Reflect.decorate) r = Reflect.decorate(decorators, target, key, desc); else for (var i = decorators.length - 1; i >= 0; i--) (d = decorators[i]) && (r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r);
      return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    var Slot_FreeGameGetScorePanel_1 = require("../../../SlotFramework/Game/Panel/Slot_FreeGameGetScorePanel");
    var AudioManager_1 = require("../../../Common/Tools/AudioManager/AudioManager");
    var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
    var G27_FreeGameGetScorePanel = function(_super) {
      __extends(G27_FreeGameGetScorePanel, _super);
      function G27_FreeGameGetScorePanel() {
        var _this = null !== _super && _super.apply(this, arguments) || this;
        _this.view = null;
        _this.scoreNode = null;
        _this.winNode = null;
        return _this;
      }
      G27_FreeGameGetScorePanel.prototype.init = function() {
        _super.prototype.init.call(this);
      };
      G27_FreeGameGetScorePanel.prototype.open = function(p_freeGameGetScore, p_betScore) {
        var _this = this;
        this.view.opacity = 0;
        this.winNode.y = 730.987;
        this.scoreNode.active = false;
        _super.prototype.open.call(this, p_freeGameGetScore, p_betScore);
        cc.tween(this.view).delay(.5).call(function() {
          AudioManager_1.AudioManager.instance.playAudioEvent("OpenFreeGameGetScorePanel");
        }).to(.5, {
          opacity: 255
        }, {
          easing: "cubicInOut"
        }).call(function() {
          _this.winNode.y = 73.987;
          _this.scoreNode.active = true;
          _this.showWinEffect();
        }).start();
      };
      G27_FreeGameGetScorePanel.prototype.close = function() {
        AudioManager_1.AudioManager.instance.playAudioEvent("CloseFreeGameGetScorePanel");
        return _super.prototype.close.call(this);
      };
      G27_FreeGameGetScorePanel.prototype.showWinEffect = function() {
        console.log("\u3010Show Win\u3011 ");
        this.node.getComponentsInChildren(cc.ParticleSystem3D).forEach(function(item) {
          item.stop();
          item.play();
        });
        this.node.getComponentInChildren(cc.Animation).play();
        AudioManager_1.AudioManager.instance.playAudioEvent("FreeGameGetScoreShowWinEffect");
      };
      __decorate([ property(cc.Node) ], G27_FreeGameGetScorePanel.prototype, "view", void 0);
      __decorate([ property(cc.Node) ], G27_FreeGameGetScorePanel.prototype, "scoreNode", void 0);
      __decorate([ property(cc.Node) ], G27_FreeGameGetScorePanel.prototype, "winNode", void 0);
      G27_FreeGameGetScorePanel = __decorate([ ccclass ], G27_FreeGameGetScorePanel);
      return G27_FreeGameGetScorePanel;
    }(Slot_FreeGameGetScorePanel_1.default);
    exports.default = G27_FreeGameGetScorePanel;
    cc._RF.pop();
  }, {
    "../../../Common/Tools/AudioManager/AudioManager": void 0,
    "../../../SlotFramework/Game/Panel/Slot_FreeGameGetScorePanel": void 0
  } ],
  G27_FreeGamePanel: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "1342eU7hetN8Y/omqsffZUt", "G27_FreeGamePanel");
    "use strict";
    var __extends = this && this.__extends || function() {
      var extendStatics = function(d, b) {
        extendStatics = Object.setPrototypeOf || {
          __proto__: []
        } instanceof Array && function(d, b) {
          d.__proto__ = b;
        } || function(d, b) {
          for (var p in b) Object.prototype.hasOwnProperty.call(b, p) && (d[p] = b[p]);
        };
        return extendStatics(d, b);
      };
      return function(d, b) {
        extendStatics(d, b);
        function __() {
          this.constructor = d;
        }
        d.prototype = null === b ? Object.create(b) : (__.prototype = b.prototype, new __());
      };
    }();
    var __decorate = this && this.__decorate || function(decorators, target, key, desc) {
      var c = arguments.length, r = c < 3 ? target : null === desc ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
      if ("object" === typeof Reflect && "function" === typeof Reflect.decorate) r = Reflect.decorate(decorators, target, key, desc); else for (var i = decorators.length - 1; i >= 0; i--) (d = decorators[i]) && (r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r);
      return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __awaiter = this && this.__awaiter || function(thisArg, _arguments, P, generator) {
      function adopt(value) {
        return value instanceof P ? value : new P(function(resolve) {
          resolve(value);
        });
      }
      return new (P || (P = Promise))(function(resolve, reject) {
        function fulfilled(value) {
          try {
            step(generator.next(value));
          } catch (e) {
            reject(e);
          }
        }
        function rejected(value) {
          try {
            step(generator["throw"](value));
          } catch (e) {
            reject(e);
          }
        }
        function step(result) {
          result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected);
        }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
      });
    };
    var __generator = this && this.__generator || function(thisArg, body) {
      var _ = {
        label: 0,
        sent: function() {
          if (1 & t[0]) throw t[1];
          return t[1];
        },
        trys: [],
        ops: []
      }, f, y, t, g;
      return g = {
        next: verb(0),
        throw: verb(1),
        return: verb(2)
      }, "function" === typeof Symbol && (g[Symbol.iterator] = function() {
        return this;
      }), g;
      function verb(n) {
        return function(v) {
          return step([ n, v ]);
        };
      }
      function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
          if (f = 1, y && (t = 2 & op[0] ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 
          0) : y.next) && !(t = t.call(y, op[1])).done) return t;
          (y = 0, t) && (op = [ 2 & op[0], t.value ]);
          switch (op[0]) {
           case 0:
           case 1:
            t = op;
            break;

           case 4:
            _.label++;
            return {
              value: op[1],
              done: false
            };

           case 5:
            _.label++;
            y = op[1];
            op = [ 0 ];
            continue;

           case 7:
            op = _.ops.pop();
            _.trys.pop();
            continue;

           default:
            if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (6 === op[0] || 2 === op[0])) {
              _ = 0;
              continue;
            }
            if (3 === op[0] && (!t || op[1] > t[0] && op[1] < t[3])) {
              _.label = op[1];
              break;
            }
            if (6 === op[0] && _.label < t[1]) {
              _.label = t[1];
              t = op;
              break;
            }
            if (t && _.label < t[2]) {
              _.label = t[2];
              _.ops.push(op);
              break;
            }
            t[2] && _.ops.pop();
            _.trys.pop();
            continue;
          }
          op = body.call(thisArg, _);
        } catch (e) {
          op = [ 6, e ];
          y = 0;
        } finally {
          f = t = 0;
        }
        if (5 & op[0]) throw op[1];
        return {
          value: op[0] ? op[1] : void 0,
          done: true
        };
      }
    };
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    var Slot_FreeGamePanel_1 = require("../../../SlotFramework/Game/Panel/Slot_FreeGamePanel");
    var AudioManager_1 = require("../../../Common/Tools/AudioManager/AudioManager");
    var G27_FXWarning_1 = require("./G27_FXWarning");
    var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
    var G27_FreeGamePanel = function(_super) {
      __extends(G27_FreeGamePanel, _super);
      function G27_FreeGamePanel() {
        var _this = null !== _super && _super.apply(this, arguments) || this;
        _this.animation = null;
        _this.warningTailSpine = null;
        _this.fxWarning = null;
        return _this;
      }
      G27_FreeGamePanel.prototype.init = function() {
        _super.prototype.init.call(this);
      };
      G27_FreeGamePanel.prototype.open = function(p_freeGameCount) {
        var _this = this;
        _super.prototype.open.call(this, p_freeGameCount);
        AudioManager_1.AudioManager.instance.playAudioEvent("OpenFreeGamePanel");
        cc.tween(this.animation).call(function() {
          _this.animation.play();
        }).delay(1.83).call(function() {
          var _a;
          null === (_a = _this.warningTailSpine) || void 0 === _a ? void 0 : _a.setAnimation(0, "play_hit", false);
        }).start();
      };
      G27_FreeGamePanel.prototype.close = function() {
        return __awaiter(this, void 0, Promise, function() {
          return __generator(this, function(_a) {
            console.log("\u95dc\u9589\u514d\u8cbb\u904a\u6232\u9762\u677f");
            AudioManager_1.AudioManager.instance.playAudioEvent("CloseFreeGamePanel");
            return [ 2, _super.prototype.close.call(this) ];
          });
        });
      };
      G27_FreeGamePanel.prototype.closeAnimation = function() {
        var _this = this;
        return new Promise(function(resolve) {
          cc.tween(_this.node).delay(.5).call(function() {
            _this.node.active = false;
            resolve();
          }).start();
        });
      };
      __decorate([ property(cc.Animation) ], G27_FreeGamePanel.prototype, "animation", void 0);
      __decorate([ property(sp.Skeleton) ], G27_FreeGamePanel.prototype, "warningTailSpine", void 0);
      __decorate([ property(G27_FXWarning_1.default) ], G27_FreeGamePanel.prototype, "fxWarning", void 0);
      G27_FreeGamePanel = __decorate([ ccclass ], G27_FreeGamePanel);
      return G27_FreeGamePanel;
    }(Slot_FreeGamePanel_1.default);
    exports.default = G27_FreeGamePanel;
    cc._RF.pop();
  }, {
    "../../../Common/Tools/AudioManager/AudioManager": void 0,
    "../../../SlotFramework/Game/Panel/Slot_FreeGamePanel": void 0,
    "./G27_FXWarning": "G27_FXWarning"
  } ],
  G27_GameUI: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "f7beb6VfvZF2oHgMa1c9hOS", "G27_GameUI");
    "use strict";
    var __extends = this && this.__extends || function() {
      var extendStatics = function(d, b) {
        extendStatics = Object.setPrototypeOf || {
          __proto__: []
        } instanceof Array && function(d, b) {
          d.__proto__ = b;
        } || function(d, b) {
          for (var p in b) Object.prototype.hasOwnProperty.call(b, p) && (d[p] = b[p]);
        };
        return extendStatics(d, b);
      };
      return function(d, b) {
        extendStatics(d, b);
        function __() {
          this.constructor = d;
        }
        d.prototype = null === b ? Object.create(b) : (__.prototype = b.prototype, new __());
      };
    }();
    var __decorate = this && this.__decorate || function(decorators, target, key, desc) {
      var c = arguments.length, r = c < 3 ? target : null === desc ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
      if ("object" === typeof Reflect && "function" === typeof Reflect.decorate) r = Reflect.decorate(decorators, target, key, desc); else for (var i = decorators.length - 1; i >= 0; i--) (d = decorators[i]) && (r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r);
      return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    var AudioManager_1 = require("../../../Common/Tools/AudioManager/AudioManager");
    var BaseObjectPool_1 = require("../../../Common/Tools/BaseObjectPool/BaseObjectPool");
    var Slot_GameUI_1 = require("../../../SlotFramework/Game/view/Slot_GameUI");
    var G27_DataManager_1 = require("../../Common/G27_DataManager");
    var G27_DynamicPopUpPanelManager_1 = require("../../Common/G27_DynamicPopUpPanelManager");
    var G27_SocketManager_1 = require("../../Common/Socket/G27_SocketManager");
    var G27_AutoGamePanel_1 = require("./G27_AutoGamePanel");
    var G27_FreeGameGetScorePanel_1 = require("./G27_FreeGameGetScorePanel");
    var G27_FreeGamePanel_1 = require("./G27_FreeGamePanel");
    var G27_MusicOptionPanel_1 = require("./G27_MusicOptionPanel");
    var G27_SymbolTipPanel_1 = require("./G27_SymbolTipPanel");
    var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
    var G27_GameUI = function(_super) {
      __extends(G27_GameUI, _super);
      function G27_GameUI() {
        var _this = null !== _super && _super.apply(this, arguments) || this;
        _this.freeGamePanel = null;
        _this.freeGameGetScorePanel = null;
        _this.autoGamePanel = null;
        _this.musicOptionPanel = null;
        _this.symbolTipPanel = null;
        _this.baseGameBG = null;
        _this.baseGameFG = null;
        _this.baseGameBGSpine = null;
        _this.freeGameBG = null;
        _this.freeGameFG = null;
        _this.freeGameBGSpine = null;
        _this.freeGameBGSpine2 = null;
        _this.updateBroadParticle = null;
        _this.getFreeGameTip = null;
        _this.tip6String = "\u4ee5\u4e0b\u70ba Menu Panel Buttons ";
        _this.exitGameButton = null;
        _this.openHistoryButton = null;
        _this.openRuleButton = null;
        _this.openMusicOptionsButton = null;
        _this.closeMenuButton = null;
        _this.copyVersionButton = null;
        _this.spinArrowOnSpin = null;
        _this.autoSpinCountNode = null;
        _this.autoSpine = null;
        _this.minusSpine = null;
        _this.plusSpine = null;
        _this.turboSpine = null;
        _this.mainSpine = null;
        _this.MGNormal1Spine = null;
        _this.MGNormal2Spine = null;
        _this.FGSnakeSpine = null;
        _this.MGPlayWinEffect = null;
        _this.MGPlayBigWinEffect = null;
        _this.FGPlayWinEffect = null;
        _this.moveNode = null;
        _this.FXScene = null;
        _this.ButtonBlock = null;
        _this.freeGameButtonMask = null;
        _this.freeGameTooltip = null;
        _this.mainGameLeaf = null;
        _this.animalHand = null;
        _this.openMenuButton = null;
        _this.gameIcon = null;
        _this.changeSymbolFX = null;
        _this.collectFlyFX = null;
        _this.fxCollectFly = null;
        _this.fxScatterCollectStart = null;
        _this.fxFreegameAddTime = null;
        _this.preloadSpine = [];
        _this.jpNode = null;
        _this.collectFXPools = [];
        _this.isFreeGameCollctEffecting = false;
        _this._menuClosePosition = cc.v3(0, -501, 0);
        return _this;
      }
      Object.defineProperty(G27_GameUI.prototype, "socket", {
        get: function() {
          return G27_SocketManager_1.default.getInstance();
        },
        enumerable: false,
        configurable: true
      });
      Object.defineProperty(G27_GameUI.prototype, "popup", {
        get: function() {
          return G27_DynamicPopUpPanelManager_1.default.getInstance();
        },
        enumerable: false,
        configurable: true
      });
      Object.defineProperty(G27_GameUI.prototype, "data", {
        get: function() {
          return G27_DataManager_1.default.getInstance();
        },
        enumerable: false,
        configurable: true
      });
      G27_GameUI.prototype.init = function(p_data) {
        _super.prototype.init.call(this, p_data);
        this.freeGamePanel.init();
        this.freeGameGetScorePanel.init();
        this.historyPanel.init(this.data, this.popup, this.socket);
        this.webRulePanel.init(this.data, this.popup);
        this.autoGamePanel.init();
        this.jpNode.opacity = 0;
        this.FXScene.active = false;
        this.animalHand.node.opacity = 0;
        this.changeSymbolFX.active = false;
        this.hideGetFreeGameTip();
        this.changeToBaseGameBackGround();
        this.setSpinArrowSpinningSpeed(Slot_GameUI_1.SPIN_ARROW_SPINNIG_TYPE.NORMAL);
        this.GAMETIP_DELAYTIME = 2;
        this.GAMETIP_PLAYTIME = 5;
        for (var i = 0; i < this.collectFXPools.length; i++) this.collectFXPools[i].init();
      };
      G27_GameUI.prototype.closeMenu = function(p_isFast) {
        var _this = this;
        if (p_isFast) {
          this.menu.setPosition(this._menuClosePosition);
          this.menu.active = false;
        } else {
          cc.Tween.stopAllByTarget(this.menu);
          this.menu.setPosition(cc.Vec3.ZERO);
          cc.tween(this.menu).to(this.menuCloseTime, {
            position: this._menuClosePosition
          }).call(function() {
            _this.menu.active = false;
          }).start();
        }
      };
      G27_GameUI.prototype.openMenu = function() {
        cc.Tween.stopAllByTarget(this.menu);
        this.menu.setPosition(this._menuClosePosition);
        this.menu.active = true;
        cc.tween(this.menu).to(this.menuOpenTime, {
          position: cc.Vec3.ZERO
        }).start();
      };
      G27_GameUI.prototype.changeToBaseGameBackGround = function() {
        this.baseGameBG.active = true;
        this.mainGameLeaf.active = true;
        this.freeGameBG.active = false;
        cc.tween(this.freeGameFG).to(.5, {
          opacity: 0
        }).start();
        this.ButtonBlock.active = false;
        this.freeGameButtonMask.active = false;
        this.freeGameTooltip.active = false;
        this.setBackBtnState(true);
      };
      G27_GameUI.prototype.changeToFreeGameBackGround = function() {
        var _this = this;
        this.baseGameBG.active = false;
        this.mainGameLeaf.active = false;
        this.freeGameBG.active = true;
        this.freeGameTooltip.active = true;
        cc.tween(this.freeGameFG).delay(.5).to(.5, {
          opacity: 255
        }).start();
        this.ButtonBlock.active = true;
        this.freeGameBGSpine2.setCompleteListener(function() {
          _this.freeGameBGSpine.node.active = false;
        });
        this.freeGameBGSpine.node.active = true;
        this.freeGameBGSpine.setAnimation(0, "TurnFG", false);
        this.freeGameBGSpine2.setAnimation(0, "TurnFG", false);
        this.setBackBtnState(false);
      };
      G27_GameUI.prototype.openFreeGamePanel = function(p_freeGameCount) {
        this.freeGamePanel.open(p_freeGameCount);
      };
      G27_GameUI.prototype.closeFreeGamePanel = function() {
        return this.freeGamePanel.close();
      };
      G27_GameUI.prototype.openFreeGameGetScorePanel = function(p_freeGameGetScore, p_betScore) {
        this.freeGameGetScorePanel.open(p_freeGameGetScore, p_betScore);
      };
      G27_GameUI.prototype.closeFreeGameGetScorePanel = function() {
        return this.freeGameGetScorePanel.close();
      };
      G27_GameUI.prototype.showSpinButtonClickEffect = function() {
        this.spinButtonClickEffect.active = true;
        this.spinButtonClickEffect.getComponentsInChildren(cc.Animation).forEach(function(item) {
          item.play();
        });
        this.spinButtonClickEffect.getComponentsInChildren(cc.ParticleSystem3D).forEach(function(item) {
          item.stop();
          item.play();
        });
      };
      G27_GameUI.prototype.showBGScrollingEffect = function() {
        this.freeGameBG.active && AudioManager_1.AudioManager.instance.playAudioEvent("FreeGameScrolling");
      };
      G27_GameUI.prototype.showMainGamePlayWinEffect = function() {
        var _this = this;
        this.MGPlayWinEffect.active = true;
        this.MGPlayWinEffect.getComponentInChildren(cc.ParticleSystem3D).stop();
        this.MGPlayWinEffect.getComponentInChildren(cc.ParticleSystem3D).play();
        this.MGPlayWinEffect.getComponentInChildren(cc.Animation).play();
        this.MGNormal1Spine.setAnimation(0, "MG_Win", false);
        this.MGNormal1Spine.setCompleteListener(function() {
          _this.MGNormal1Spine.setCompleteListener(null);
          _this.MGNormal1Spine.setAnimation(0, "MG_Normal1", true);
        });
      };
      G27_GameUI.prototype.showMainGamePlayBigWinEffect = function() {
        var _this = this;
        this.MGPlayBigWinEffect.active = true;
        this.MGPlayBigWinEffect.getComponentInChildren(cc.ParticleSystem3D).stop();
        this.MGPlayBigWinEffect.getComponentInChildren(cc.ParticleSystem3D).play();
        this.MGNormal1Spine.node.active = false;
        this.MGNormal2Spine.setAnimation(0, "MG_BigWin", false);
        this.MGNormal2Spine.setCompleteListener(function() {
          _this.MGNormal2Spine.setCompleteListener(null);
          _this.MGNormal1Spine.setAnimation(0, "MG_Normal1", true);
          _this.MGNormal2Spine.setAnimation(0, "MG_Normal2", true);
          _this.MGNormal1Spine.node.active = true;
        });
      };
      G27_GameUI.prototype.showFreeGamePlayWinEffect = function() {
        var _this = this;
        this.FGPlayWinEffect.active = true;
        this.FGPlayWinEffect.getComponentInChildren(cc.Animation).play();
        this.FGSnakeSpine.setAnimation(0, "play_win_Snake", false);
        this.FGSnakeSpine.setCompleteListener(function() {
          _this.FGSnakeSpine.setCompleteListener(null);
          _this.FGSnakeSpine.setAnimation(0, "play_normal_Snake", true);
        });
      };
      G27_GameUI.prototype.showGetFreeGameTip = function() {
        this.getFreeGameTip.active = true;
        this.getFreeGameTip.scale = 0;
        cc.tween(this.getFreeGameTip).to(.2, {
          scale: 1.3
        }, {
          easing: "sineOut"
        }).to(.2, {
          scale: 1
        }, {
          easing: "sineIn"
        }).start();
      };
      G27_GameUI.prototype.hideGetFreeGameTip = function() {
        this.getFreeGameTip.active = false;
      };
      G27_GameUI.prototype.showTotalGetScore = function(p_totalGetScore) {
        _super.prototype.showTotalGetScore.call(this, p_totalGetScore);
        this.totalGetScoreTip.node.parent.scale = 0;
        this.totalGetScoreTip.node.parent.stopAllActions();
        cc.tween(this.totalGetScoreTip.node.parent).to(.2, {
          scale: 1.5
        }, {
          easing: "sineOut"
        }).to(.2, {
          scale: 1
        }, {
          easing: "sineIn"
        }).start();
        this.updateBroadParticle.getComponentsInChildren(cc.ParticleSystem3D).forEach(function(item) {
          item.stop();
          item.play();
        });
      };
      G27_GameUI.prototype.setSpinArrowSpinningSpeed = function(p_type) {
        this.spinArrow.stopAllActions();
        this.spinArrowOnSpin.stopAllActions();
        if (p_type == Slot_GameUI_1.SPIN_ARROW_SPINNIG_TYPE.STOP) ; else if (p_type == Slot_GameUI_1.SPIN_ARROW_SPINNIG_TYPE.NORMAL) {
          this.spinArrow.opacity = 255;
          this.spinArrowOnSpin.opacity = 0;
          cc.tween(this.spinArrow).by(this.spinArrowRotateDuration_Normal, {
            angle: -360
          }).repeatForever().start();
          cc.tween(this.spinArrowOnSpin).by(this.spinArrowRotateDuration_Normal, {
            angle: -360
          }).repeatForever().start();
        } else {
          this.spinArrow.opacity = 0;
          this.spinArrowOnSpin.opacity = 255;
          cc.tween(this.spinArrow).by(this.spinArrowRotateDuration_Fast, {
            angle: -360
          }).repeatForever().start();
          cc.tween(this.spinArrowOnSpin).by(this.spinArrowRotateDuration_Fast, {
            angle: -360
          }).repeatForever().start();
        }
      };
      G27_GameUI.prototype.openCancelAutoSpinButton = function() {
        _super.prototype.openCancelAutoSpinButton.call(this);
        this.spinArrowOnSpin.active = false;
        this.autoSpinCountNode.active = true;
      };
      G27_GameUI.prototype.closeCancelAutoSpinButton = function() {
        _super.prototype.closeCancelAutoSpinButton.call(this);
        this.spinArrowOnSpin.active = true;
        this.autoSpinCountNode.active = false;
      };
      G27_GameUI.prototype.showWheelBlock = function() {
        this.freeGameButtonMask.active = true;
        cc.tween(this.freeGameButtonMask).to(.5, {
          opacity: 255
        }).start();
      };
      G27_GameUI.prototype.showAnimalHand = function(type, aniType) {
        var _this = this;
        var typeString = [ "Tiger", "Rhino", "Croco" ];
        var modeString = [ "normal", "hit", "bighit" ];
        var index = 1;
        var delayArr = [];
        AudioManager_1.AudioManager.instance.playAudioEvent("ChangeSymbolFX");
        this.changeSymbolFX.active = true;
        this.changeSymbolFX.getComponentsInChildren(cc.Animation).forEach(function(item) {
          item.play();
        });
        this.changeSymbolFX.getComponentInChildren(cc.ParticleSystem3D).stop();
        this.changeSymbolFX.getComponentInChildren(cc.ParticleSystem3D).play();
        aniType.forEach(function(item, index) {
          delayArr[index] = 0 === item ? 0 : .5;
        });
        cc.tween(this.animalHand.node).delay(2).call(function() {
          _this.animalHand.node.position = cc.v3(-300, 0);
          _this.animalHand.node.opacity = 255;
          _this.animalHand.setAnimation(0, typeString[type - 1] + "_play_normal", true);
        }).to(.6, {
          position: cc.v3(-150, 0)
        }).call(function() {
          _this.animalHandChange(aniType[index], typeString[type - 1] + "_play_" + modeString[aniType[index++]], typeString[type - 1] + "_play_normal");
        }).delay(delayArr[1]).to(.6, {
          position: cc.v3(0, 0)
        }).call(function() {
          _this.animalHandChange(aniType[index], typeString[type - 1] + "_play_" + modeString[aniType[index++]], typeString[type - 1] + "_play_normal");
        }).delay(delayArr[2]).to(.6, {
          position: cc.v3(150, 0)
        }).call(function() {
          _this.animalHandChange(aniType[index], typeString[type - 1] + "_play_" + modeString[aniType[index++]], typeString[type - 1] + "_play_normal");
        }).delay(delayArr[3]).to(.6, {
          position: cc.v3(300, 0)
        }).call(function() {
          _this.animalHandChange(aniType[index], typeString[type - 1] + "_play_" + modeString[aniType[index++]], typeString[type - 1] + "_play_normal");
        }).delay(delayArr[4]).to(.6, {
          position: cc.v3(450, 0)
        }).call(function() {
          _this.animalHand.node.opacity = 0;
          _this.changeSymbolFX.active = false;
        }).start();
      };
      G27_GameUI.prototype.animalHandChange = function(aniType, beforeStr, afterString) {
        var _this = this;
        1 === aniType ? AudioManager_1.AudioManager.instance.playAudioEvent("AnimalHandHit") : 2 === aniType && AudioManager_1.AudioManager.instance.playAudioEvent("AnimalHandBigHit");
        this.animalHand.setAnimation(0, beforeStr, false);
        this.animalHand.setCompleteListener(function() {
          _this.animalHand.setCompleteListener(null);
          _this.animalHand.setAnimation(0, afterString, true);
        });
      };
      G27_GameUI.prototype.stopAnimalHand = function() {
        this.animalHand.node.stopAllActions();
        this.animalHand.node.opacity = 0;
        this.changeSymbolFX.active = false;
      };
      G27_GameUI.prototype.showSpinMask = function() {
        _super.prototype.showSpinMask.call(this);
        this.openMenuButton.interactable = false;
      };
      G27_GameUI.prototype.hideSpinMask = function() {
        _super.prototype.hideSpinMask.call(this);
        this.openMenuButton.interactable = true;
      };
      G27_GameUI.prototype.playChangeGameTipEff = function(newGameTip, nowGameTip, main) {
        var _this = this;
        this.resetGameTipPos(newGameTip, 0);
        cc.tween(nowGameTip).to(1, {
          opacity: 0
        }).call(function() {
          _this.playGameTip(newGameTip, main);
        }).start();
        cc.tween(newGameTip).delay(1).to(1, {
          opacity: 255
        }).start();
      };
      G27_GameUI.prototype.pushGameIcon = function() {
        var _this = this;
        cc.tween(this.gameIcon).delay(2.45).call(function() {
          _this.gameIcon.opacity = 255;
        }).to(.15, {
          scale: 1.03
        }).to(.1, {
          scale: 1
        }).start();
      };
      G27_GameUI.prototype.fadeInCanvas = function() {
        this.moveNode.setPosition(0, -1680);
        cc.tween(this.moveNode).to(1.2, {
          position: cc.v3(0, 0)
        }).start();
      };
      G27_GameUI.prototype.showFreeGameCollctEffect = function(winPosList, remainingFreeGameCount) {
        var _this = this;
        var freegameEffectTargetPos = cc.v2(0, 520);
        cc.tween(this.node).call(function() {
          _this.isFreeGameCollctEffecting = true;
        }).delay(.2).call(function() {
          AudioManager_1.AudioManager.instance.playAudioEvent("FreeGameFireBall");
          winPosList.forEach(function(winPos) {
            var fxCollectFly = _this.collectFXPools[0].get();
            var fxScatterCollectStart = _this.collectFXPools[1].get();
            winPos.y -= 52;
            fxCollectFly.position = winPos;
            fxCollectFly.opacity = 255;
            fxScatterCollectStart.position = winPos;
            _this.collectFlyFX.addChild(fxCollectFly);
            _this.collectFlyFX.addChild(fxScatterCollectStart);
            var targetTo = cc.v2(winPos.x, winPos.y);
            var centerTo = cc.v2(.5 * (freegameEffectTargetPos.x + targetTo.x), .5 * (freegameEffectTargetPos.y + targetTo.y));
            var offset = 0;
            targetTo.x - freegameEffectTargetPos.x > 50 ? offset = 50 : targetTo.x - freegameEffectTargetPos.x < -50 && (offset = -50);
            cc.tween(fxCollectFly).call(function() {
              fxScatterCollectStart.getComponentInChildren(cc.Animation).play();
              fxCollectFly.getComponentInChildren(cc.Animation).play();
              fxCollectFly.getComponentInChildren(cc.ParticleSystem).stopSystem();
              fxCollectFly.getComponentInChildren(cc.ParticleSystem).resetSystem();
            }).bezierTo(.5, cc.v2(centerTo.x + offset, centerTo.y), cc.v2(centerTo.x + offset, centerTo.y), freegameEffectTargetPos).call(function() {
              _this.collectFXPools[1].put(fxScatterCollectStart);
              fxCollectFly.opacity = 0;
              _this.fxFreegameAddTime.active = true;
              _this.fxFreegameAddTime.getComponentInChildren(cc.Animation).play();
              _this.fxFreegameAddTime.getComponentsInChildren(cc.ParticleSystem3D).forEach(function(item) {
                item.stop();
                item.play();
              });
            }).delay(.3).call(function() {
              _this.setRemainingFreeGameCount(remainingFreeGameCount + 2);
              _this.collectFXPools[0].put(fxCollectFly);
            }).start();
          });
        }).delay(1).call(function() {
          _this.isFreeGameCollctEffecting = false;
        }).start();
      };
      G27_GameUI.prototype.showWinScoreEffect = function(p_totalWinScore, p_betScore, p_isFreeGame) {
        void 0 === p_isFreeGame && (p_isFreeGame = false);
        this.winScore.node.opacity = 255;
        _super.prototype.showWinScoreEffect.call(this, p_totalWinScore, p_betScore, p_isFreeGame);
      };
      G27_GameUI.prototype.closeWinScoreEffect = function() {
        var _this = this;
        cc.tween(this.winScorePanel.node).to(.2, {
          opacity: 0
        }).call(function() {
          return _this.winScorePanel.close();
        }).start();
      };
      G27_GameUI.prototype.initJpPosition = function() {
        this.jpNode.opacity = 255;
        this.jpNode.getComponentInChildren(cc.Widget).updateAlignment();
      };
      G27_GameUI.prototype.playLongWildSound = function() {
        AudioManager_1.AudioManager.instance.playAudioEvent("LongWildSound");
      };
      __decorate([ property(G27_FreeGamePanel_1.default) ], G27_GameUI.prototype, "freeGamePanel", void 0);
      __decorate([ property(G27_FreeGameGetScorePanel_1.default) ], G27_GameUI.prototype, "freeGameGetScorePanel", void 0);
      __decorate([ property({
        type: G27_AutoGamePanel_1.default,
        override: true
      }) ], G27_GameUI.prototype, "autoGamePanel", void 0);
      __decorate([ property({
        type: G27_MusicOptionPanel_1.default,
        override: true
      }) ], G27_GameUI.prototype, "musicOptionPanel", void 0);
      __decorate([ property({
        type: G27_SymbolTipPanel_1.default,
        override: true
      }) ], G27_GameUI.prototype, "symbolTipPanel", void 0);
      __decorate([ property(cc.Node) ], G27_GameUI.prototype, "baseGameBG", void 0);
      __decorate([ property(cc.Node) ], G27_GameUI.prototype, "baseGameFG", void 0);
      __decorate([ property(sp.Skeleton) ], G27_GameUI.prototype, "baseGameBGSpine", void 0);
      __decorate([ property(cc.Node) ], G27_GameUI.prototype, "freeGameBG", void 0);
      __decorate([ property(cc.Node) ], G27_GameUI.prototype, "freeGameFG", void 0);
      __decorate([ property(sp.Skeleton) ], G27_GameUI.prototype, "freeGameBGSpine", void 0);
      __decorate([ property(sp.Skeleton) ], G27_GameUI.prototype, "freeGameBGSpine2", void 0);
      __decorate([ property(cc.Node) ], G27_GameUI.prototype, "updateBroadParticle", void 0);
      __decorate([ property(cc.Node) ], G27_GameUI.prototype, "getFreeGameTip", void 0);
      __decorate([ property({
        readonly: true,
        displayName: "========================"
      }) ], G27_GameUI.prototype, "tip6String", void 0);
      __decorate([ property(cc.Node) ], G27_GameUI.prototype, "exitGameButton", void 0);
      __decorate([ property(cc.Node) ], G27_GameUI.prototype, "openHistoryButton", void 0);
      __decorate([ property(cc.Node) ], G27_GameUI.prototype, "openRuleButton", void 0);
      __decorate([ property(cc.Node) ], G27_GameUI.prototype, "openMusicOptionsButton", void 0);
      __decorate([ property(cc.Node) ], G27_GameUI.prototype, "closeMenuButton", void 0);
      __decorate([ property(cc.Node) ], G27_GameUI.prototype, "copyVersionButton", void 0);
      __decorate([ property(cc.Node) ], G27_GameUI.prototype, "spinArrowOnSpin", void 0);
      __decorate([ property(cc.Node) ], G27_GameUI.prototype, "autoSpinCountNode", void 0);
      __decorate([ property(sp.Skeleton) ], G27_GameUI.prototype, "autoSpine", void 0);
      __decorate([ property(sp.Skeleton) ], G27_GameUI.prototype, "minusSpine", void 0);
      __decorate([ property(sp.Skeleton) ], G27_GameUI.prototype, "plusSpine", void 0);
      __decorate([ property(sp.Skeleton) ], G27_GameUI.prototype, "turboSpine", void 0);
      __decorate([ property(sp.Skeleton) ], G27_GameUI.prototype, "mainSpine", void 0);
      __decorate([ property(sp.Skeleton) ], G27_GameUI.prototype, "MGNormal1Spine", void 0);
      __decorate([ property(sp.Skeleton) ], G27_GameUI.prototype, "MGNormal2Spine", void 0);
      __decorate([ property(sp.Skeleton) ], G27_GameUI.prototype, "FGSnakeSpine", void 0);
      __decorate([ property(cc.Node) ], G27_GameUI.prototype, "MGPlayWinEffect", void 0);
      __decorate([ property(cc.Node) ], G27_GameUI.prototype, "MGPlayBigWinEffect", void 0);
      __decorate([ property(cc.Node) ], G27_GameUI.prototype, "FGPlayWinEffect", void 0);
      __decorate([ property(cc.Node) ], G27_GameUI.prototype, "moveNode", void 0);
      __decorate([ property(cc.Node) ], G27_GameUI.prototype, "FXScene", void 0);
      __decorate([ property(cc.Node) ], G27_GameUI.prototype, "ButtonBlock", void 0);
      __decorate([ property(cc.Node) ], G27_GameUI.prototype, "freeGameButtonMask", void 0);
      __decorate([ property(cc.Node) ], G27_GameUI.prototype, "freeGameTooltip", void 0);
      __decorate([ property(cc.Node) ], G27_GameUI.prototype, "mainGameLeaf", void 0);
      __decorate([ property(sp.Skeleton) ], G27_GameUI.prototype, "animalHand", void 0);
      __decorate([ property(cc.Button) ], G27_GameUI.prototype, "openMenuButton", void 0);
      __decorate([ property(cc.Node) ], G27_GameUI.prototype, "gameIcon", void 0);
      __decorate([ property(cc.Node) ], G27_GameUI.prototype, "changeSymbolFX", void 0);
      __decorate([ property(cc.Node) ], G27_GameUI.prototype, "collectFlyFX", void 0);
      __decorate([ property(cc.Node) ], G27_GameUI.prototype, "fxCollectFly", void 0);
      __decorate([ property(cc.Node) ], G27_GameUI.prototype, "fxScatterCollectStart", void 0);
      __decorate([ property(cc.Node) ], G27_GameUI.prototype, "fxFreegameAddTime", void 0);
      __decorate([ property(sp.Skeleton) ], G27_GameUI.prototype, "preloadSpine", void 0);
      __decorate([ property(cc.Node) ], G27_GameUI.prototype, "jpNode", void 0);
      __decorate([ property([ BaseObjectPool_1.default ]) ], G27_GameUI.prototype, "collectFXPools", void 0);
      G27_GameUI = __decorate([ ccclass ], G27_GameUI);
      return G27_GameUI;
    }(Slot_GameUI_1.default);
    exports.default = G27_GameUI;
    cc._RF.pop();
  }, {
    "../../../Common/Tools/AudioManager/AudioManager": void 0,
    "../../../Common/Tools/BaseObjectPool/BaseObjectPool": void 0,
    "../../../SlotFramework/Game/view/Slot_GameUI": void 0,
    "../../Common/G27_DataManager": "G27_DataManager",
    "../../Common/G27_DynamicPopUpPanelManager": "G27_DynamicPopUpPanelManager",
    "../../Common/Socket/G27_SocketManager": "G27_SocketManager",
    "./G27_AutoGamePanel": "G27_AutoGamePanel",
    "./G27_FreeGameGetScorePanel": "G27_FreeGameGetScorePanel",
    "./G27_FreeGamePanel": "G27_FreeGamePanel",
    "./G27_MusicOptionPanel": "G27_MusicOptionPanel",
    "./G27_SymbolTipPanel": "G27_SymbolTipPanel"
  } ],
  G27_Game: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "34428DawBJB9Ix9sN1q3NFW", "G27_Game");
    "use strict";
    var __extends = this && this.__extends || function() {
      var extendStatics = function(d, b) {
        extendStatics = Object.setPrototypeOf || {
          __proto__: []
        } instanceof Array && function(d, b) {
          d.__proto__ = b;
        } || function(d, b) {
          for (var p in b) Object.prototype.hasOwnProperty.call(b, p) && (d[p] = b[p]);
        };
        return extendStatics(d, b);
      };
      return function(d, b) {
        extendStatics(d, b);
        function __() {
          this.constructor = d;
        }
        d.prototype = null === b ? Object.create(b) : (__.prototype = b.prototype, new __());
      };
    }();
    var __decorate = this && this.__decorate || function(decorators, target, key, desc) {
      var c = arguments.length, r = c < 3 ? target : null === desc ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
      if ("object" === typeof Reflect && "function" === typeof Reflect.decorate) r = Reflect.decorate(decorators, target, key, desc); else for (var i = decorators.length - 1; i >= 0; i--) (d = decorators[i]) && (r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r);
      return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __awaiter = this && this.__awaiter || function(thisArg, _arguments, P, generator) {
      function adopt(value) {
        return value instanceof P ? value : new P(function(resolve) {
          resolve(value);
        });
      }
      return new (P || (P = Promise))(function(resolve, reject) {
        function fulfilled(value) {
          try {
            step(generator.next(value));
          } catch (e) {
            reject(e);
          }
        }
        function rejected(value) {
          try {
            step(generator["throw"](value));
          } catch (e) {
            reject(e);
          }
        }
        function step(result) {
          result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected);
        }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
      });
    };
    var __generator = this && this.__generator || function(thisArg, body) {
      var _ = {
        label: 0,
        sent: function() {
          if (1 & t[0]) throw t[1];
          return t[1];
        },
        trys: [],
        ops: []
      }, f, y, t, g;
      return g = {
        next: verb(0),
        throw: verb(1),
        return: verb(2)
      }, "function" === typeof Symbol && (g[Symbol.iterator] = function() {
        return this;
      }), g;
      function verb(n) {
        return function(v) {
          return step([ n, v ]);
        };
      }
      function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
          if (f = 1, y && (t = 2 & op[0] ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 
          0) : y.next) && !(t = t.call(y, op[1])).done) return t;
          (y = 0, t) && (op = [ 2 & op[0], t.value ]);
          switch (op[0]) {
           case 0:
           case 1:
            t = op;
            break;

           case 4:
            _.label++;
            return {
              value: op[1],
              done: false
            };

           case 5:
            _.label++;
            y = op[1];
            op = [ 0 ];
            continue;

           case 7:
            op = _.ops.pop();
            _.trys.pop();
            continue;

           default:
            if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (6 === op[0] || 2 === op[0])) {
              _ = 0;
              continue;
            }
            if (3 === op[0] && (!t || op[1] > t[0] && op[1] < t[3])) {
              _.label = op[1];
              break;
            }
            if (6 === op[0] && _.label < t[1]) {
              _.label = t[1];
              t = op;
              break;
            }
            if (t && _.label < t[2]) {
              _.label = t[2];
              _.ops.push(op);
              break;
            }
            t[2] && _.ops.pop();
            _.trys.pop();
            continue;
          }
          op = body.call(thisArg, _);
        } catch (e) {
          op = [ 6, e ];
          y = 0;
        } finally {
          f = t = 0;
        }
        if (5 & op[0]) throw op[1];
        return {
          value: op[0] ? op[1] : void 0,
          done: true
        };
      }
    };
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    var AudioManager_1 = require("../../Common/Tools/AudioManager/AudioManager");
    var CommonTool_1 = require("../../Common/Tools/CommonTool");
    var LocalizationManager_1 = require("../../Common/Tools/Localization/LocalizationManager");
    var MarqueeSystemManager_1 = require("../../Common/Tools/MarqueeSystem/MarqueeSystemManager");
    var Slot_GameManager_InitData_1 = require("../../SlotFramework/Game/Data/Slot_GameManager_InitData");
    var Slot_GameUI_InitData_1 = require("../../SlotFramework/Game/Data/Slot_GameUI_InitData");
    var Slot_ReelManager_InitData_1 = require("../../SlotFramework/Game/Data/Slot_ReelManager_InitData");
    var Slot_ReelManager_ScrollData_1 = require("../../SlotFramework/Game/Data/Slot_ReelManager_ScrollData");
    var Slot_GameManager_1 = require("../../SlotFramework/Game/Slot_GameManager");
    var Slot_GameUI_1 = require("../../SlotFramework/Game/view/Slot_GameUI");
    var Slot_DataManager_1 = require("../../SlotFramework/Slot_DataManager");
    var G27_DataManager_1 = require("../Common/G27_DataManager");
    var G27_DynamicPopUpPanelManager_1 = require("../Common/G27_DynamicPopUpPanelManager");
    var G27_SocketManager_1 = require("../Common/Socket/G27_SocketManager");
    var G27_GameUI_1 = require("./View/G27_GameUI");
    var G27_SlotReelManager_1 = require("./View/G27_SlotReelManager");
    var G27_Wheel_1 = require("./View/G27_Wheel");
    var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
    var GAME_STATUS;
    (function(GAME_STATUS) {
      GAME_STATUS[GAME_STATUS["NONE"] = 0] = "NONE";
      GAME_STATUS[GAME_STATUS["IDLE"] = 1] = "IDLE";
      GAME_STATUS[GAME_STATUS["SERVER_REQUEST"] = 2] = "SERVER_REQUEST";
      GAME_STATUS[GAME_STATUS["SPIN"] = 3] = "SPIN";
      GAME_STATUS[GAME_STATUS["FREE_GAME_TIP"] = 4] = "FREE_GAME_TIP";
      GAME_STATUS[GAME_STATUS["FREE_GAME"] = 5] = "FREE_GAME";
      GAME_STATUS[GAME_STATUS["FREE_GAME_REWARD"] = 6] = "FREE_GAME_REWARD";
      GAME_STATUS[GAME_STATUS["FREE_GAME_WIN_SCORE_EFFECT"] = 7] = "FREE_GAME_WIN_SCORE_EFFECT";
      GAME_STATUS[GAME_STATUS["CHANGE_PANEL"] = 8] = "CHANGE_PANEL";
      GAME_STATUS[GAME_STATUS["JACKPOT_WIN_SCORE"] = 9] = "JACKPOT_WIN_SCORE";
    })(GAME_STATUS || (GAME_STATUS = {}));
    var FREEGAME_WIN_SCORE_EFFECT_STATUS;
    (function(FREEGAME_WIN_SCORE_EFFECT_STATUS) {
      FREEGAME_WIN_SCORE_EFFECT_STATUS[FREEGAME_WIN_SCORE_EFFECT_STATUS["NONE"] = 0] = "NONE";
      FREEGAME_WIN_SCORE_EFFECT_STATUS[FREEGAME_WIN_SCORE_EFFECT_STATUS["WIN_SCORE_EFFECT_START"] = 1] = "WIN_SCORE_EFFECT_START";
      FREEGAME_WIN_SCORE_EFFECT_STATUS[FREEGAME_WIN_SCORE_EFFECT_STATUS["WIN_SCORE_EFFECT_ING"] = 2] = "WIN_SCORE_EFFECT_ING";
      FREEGAME_WIN_SCORE_EFFECT_STATUS[FREEGAME_WIN_SCORE_EFFECT_STATUS["WIN_SCORE_EFFECT_END"] = 3] = "WIN_SCORE_EFFECT_END";
    })(FREEGAME_WIN_SCORE_EFFECT_STATUS || (FREEGAME_WIN_SCORE_EFFECT_STATUS = {}));
    var SPIN_STATUS;
    (function(SPIN_STATUS) {
      SPIN_STATUS[SPIN_STATUS["NONE"] = 0] = "NONE";
      SPIN_STATUS[SPIN_STATUS["WHEEL_SCROLL_START"] = 1] = "WHEEL_SCROLL_START";
      SPIN_STATUS[SPIN_STATUS["WHEEL_SCROLL_ING"] = 2] = "WHEEL_SCROLL_ING";
      SPIN_STATUS[SPIN_STATUS["WHEEL_CHECK_REWARD"] = 3] = "WHEEL_CHECK_REWARD";
      SPIN_STATUS[SPIN_STATUS["REEL_SCROLL_START"] = 4] = "REEL_SCROLL_START";
      SPIN_STATUS[SPIN_STATUS["REEL_SCROLL_ING"] = 5] = "REEL_SCROLL_ING";
      SPIN_STATUS[SPIN_STATUS["REEL_SCROLL_END"] = 6] = "REEL_SCROLL_END";
      SPIN_STATUS[SPIN_STATUS["CHECK_CHANGE_SYMBOL"] = 7] = "CHECK_CHANGE_SYMBOL";
      SPIN_STATUS[SPIN_STATUS["CHANGE_SYMBOL_START"] = 8] = "CHANGE_SYMBOL_START";
      SPIN_STATUS[SPIN_STATUS["CHANGE_SYMBOL_ING"] = 9] = "CHANGE_SYMBOL_ING";
      SPIN_STATUS[SPIN_STATUS["CHANGE_SYMBOL_END"] = 10] = "CHANGE_SYMBOL_END";
      SPIN_STATUS[SPIN_STATUS["CHECK_REWARD"] = 11] = "CHECK_REWARD";
      SPIN_STATUS[SPIN_STATUS["REWARD_EFFECT_START"] = 12] = "REWARD_EFFECT_START";
      SPIN_STATUS[SPIN_STATUS["REWARD_EFFECT_ING"] = 13] = "REWARD_EFFECT_ING";
      SPIN_STATUS[SPIN_STATUS["REWARD_EFFECT_END"] = 14] = "REWARD_EFFECT_END";
      SPIN_STATUS[SPIN_STATUS["CHECK_SCATTER_WIN"] = 15] = "CHECK_SCATTER_WIN";
      SPIN_STATUS[SPIN_STATUS["SCATTER_WIN_WAIT"] = 16] = "SCATTER_WIN_WAIT";
      SPIN_STATUS[SPIN_STATUS["SCATTER_WIN_START"] = 17] = "SCATTER_WIN_START";
      SPIN_STATUS[SPIN_STATUS["SCATTER_WIN_ING"] = 18] = "SCATTER_WIN_ING";
      SPIN_STATUS[SPIN_STATUS["WIN_SCORE_EFFECT_START"] = 19] = "WIN_SCORE_EFFECT_START";
      SPIN_STATUS[SPIN_STATUS["WIN_SCORE_EFFECT_ING"] = 20] = "WIN_SCORE_EFFECT_ING";
      SPIN_STATUS[SPIN_STATUS["WIN_SCORE_EFFECT_END"] = 21] = "WIN_SCORE_EFFECT_END";
      SPIN_STATUS[SPIN_STATUS["CHECK_FREE_GAME_COLLECT"] = 22] = "CHECK_FREE_GAME_COLLECT";
      SPIN_STATUS[SPIN_STATUS["FREE_GAME_COLLECT_START"] = 23] = "FREE_GAME_COLLECT_START";
      SPIN_STATUS[SPIN_STATUS["FREE_GAME_COLLECT_ING"] = 24] = "FREE_GAME_COLLECT_ING";
      SPIN_STATUS[SPIN_STATUS["JACKPOT_WIN_START"] = 25] = "JACKPOT_WIN_START";
      SPIN_STATUS[SPIN_STATUS["JACKPOT_WIN_ING"] = 26] = "JACKPOT_WIN_ING";
      SPIN_STATUS[SPIN_STATUS["FINISHED_WAIT"] = 27] = "FINISHED_WAIT";
      SPIN_STATUS[SPIN_STATUS["FINISHED"] = 28] = "FINISHED";
    })(SPIN_STATUS || (SPIN_STATUS = {}));
    var G27_Game = function(_super) {
      __extends(G27_Game, _super);
      function G27_Game() {
        var _this = null !== _super && _super.apply(this, arguments) || this;
        _this.slotReelManager = null;
        _this.wheel = null;
        _this.ui = null;
        _this.firstWinLineChangeTime = .4;
        _this.winLineChangeTime = 2;
        _this.winLineChangeWaitTime = .1;
        _this.spinCD = .5;
        _this.normalSpinFinishedStopTime = .5;
        _this.fastSpinFinishedStopTime = .2;
        _this.normalUpdateTipBroadEffectTime = .1;
        _this.autoSpinUpdateTipBroadEffectTime = .9;
        _this.normalRewardTime = 1;
        _this.autoRewardTime = 1;
        _this.scatterShowTime = 3;
        _this.showOmenScatterCount = 3;
        _this.data = null;
        _this.socket = null;
        _this.popup = null;
        _this.gameStatus = GAME_STATUS.NONE;
        _this.spinStatus = SPIN_STATUS.NONE;
        _this.freeGameWinScoreEffectStatus = FREEGAME_WIN_SCORE_EFFECT_STATUS.NONE;
        _this.isJumpChangeSymbol = false;
        _this.hasWinLine = false;
        _this.tempWinLineChangeTime = 0;
        _this.nowShowWinLine = 0;
        _this.tempScatterWaitTime = 0;
        _this.leopardEffectRate = 15;
        _this.isLineChange = false;
        _this.competitionStartFontSize1 = 30;
        _this.competitionStartFontColor1 = "#E4C48B";
        _this.competitionStartFontSize2 = 30;
        _this.competitionStartFontColor2 = "#DC5050";
        return _this;
      }
      G27_Game.prototype.onLoad = function() {
        this.ui.exitGameButton.on("click", this.onExitGame, this);
        this.ui.openHistoryButton.on("click", this.onOpenHistory, this);
        this.ui.openRuleButton.on("click", this.onOpenRule, this);
        this.ui.openMusicOptionsButton.on("click", this.onOpenMusicOptionPanel, this);
        this.ui.closeMenuButton.on("click", this.onCloseMenu, this);
        this.ui.copyVersionButton.on("click", this.OnCopyVersion, this);
      };
      G27_Game.prototype.start = function() {
        return __awaiter(this, void 0, void 0, function() {
          var _data;
          var _this = this;
          return __generator(this, function(_a) {
            switch (_a.label) {
             case 0:
              this.socket = G27_SocketManager_1.default.getInstance();
              this.data = G27_DataManager_1.default.getInstance();
              this.popup = G27_DynamicPopUpPanelManager_1.default.getInstance();
              MarqueeSystemManager_1.default.setMarqueeShow(false);
              _data = new Slot_GameManager_InitData_1.default();
              _data.bundleName = "G" + G27_DataManager_1.default.getInstance().gameID;
              _data.bundlePath = "" + G27_DataManager_1.default.getInstance().path;
              return [ 4, this.init(_data) ];

             case 1:
              _a.sent();
              this.socket.setOnGetInitialReelInfoEvent().then(function(p_data) {
                _this.showResponseLog("GetInitialReelInfo", p_data);
                _this.setReelInfo(p_data);
                _this.playerLogin();
                _this.gameStatus = GAME_STATUS.IDLE;
                _super.prototype.start.call(_this);
                _this.ui.setBackBtnState(false);
              });
              return [ 2 ];
            }
          });
        });
      };
      G27_Game.prototype.init = function(p_data) {
        return __awaiter(this, void 0, void 0, function() {
          var _data, _uiData;
          return __generator(this, function(_a) {
            switch (_a.label) {
             case 0:
              this.data = G27_DataManager_1.default.getInstance();
              this.data.init();
              _data = new Slot_ReelManager_InitData_1.default();
              _data.bundleName = p_data.bundleName;
              _data.bundlePath = p_data.bundlePath;
              this.slotReelManager.init(_data);
              this.wheel.init();
              this.wheel.node.active = false;
              _super.prototype.init.call(this, p_data);
              _uiData = new Slot_GameUI_InitData_1.default();
              _uiData.bundleName = p_data.bundleName;
              _uiData.bundlePath = p_data.bundlePath;
              _uiData.dataManager = this.data;
              this.ui.init(_uiData);
              this.ui.setVersion(this.data.gameVersion);
              this.ui.setBackBtnState(false);
              this.registerEvents();
              AudioManager_1.AudioManager.instance.playAudioEvent("GameInit");
              cc.systemEvent.on("LoadingAniEnd", this.showFXScene, this);
              cc.systemEvent.on("pushGameIcon", this.pushGameIcon, this);
              cc.systemEvent.on("fadeInCanvas", this.fadeInCanvas, this);
              return [ 4, CommonTool_1.CommonTool.updateCacheForSpine(this.ui.preloadSpine, false) ];

             case 1:
              _a.sent();
              return [ 2 ];
            }
          });
        });
      };
      G27_Game.prototype.onDisable = function() {
        _super.prototype.onDisable.call(this);
        AudioManager_1.AudioManager.instance.playAudioEvent("GameExit");
        cc.systemEvent.off("LoadingAniEnd", this.showFXScene, this);
        cc.systemEvent.off("pushGameIcon", this.pushGameIcon, this);
        cc.systemEvent.off("fadeInCanvas", this.fadeInCanvas, this);
      };
      G27_Game.prototype.update = function(dt) {
        if (this.toFastStopSpin && this.tempSpinCD > this.spinCD) {
          this.isJumpChangeSymbol = true;
          this.stopSpin();
          this.toFastStopSpin = false;
        }
        if (this.gameStatus == GAME_STATUS.SPIN || this.gameStatus == GAME_STATUS.FREE_GAME) {
          this.tempSpinCD += dt;
          switch (this.spinStatus) {
           case SPIN_STATUS.WHEEL_SCROLL_ING:
            this.wheelScrolling();
            break;

           case SPIN_STATUS.WHEEL_CHECK_REWARD:
            this.wheelCheckReward();
            break;

           case SPIN_STATUS.REEL_SCROLL_START:
            this.reelScrollStart();
            break;

           case SPIN_STATUS.REEL_SCROLL_ING:
            this.reelScrolling();
            break;

           case SPIN_STATUS.REEL_SCROLL_END:
            this.reelScrollEnd();
            break;

           case SPIN_STATUS.CHECK_CHANGE_SYMBOL:
            this.checkChangeSymbol();
            break;

           case SPIN_STATUS.CHANGE_SYMBOL_START:
            this.changeSymbolStart();
            break;

           case SPIN_STATUS.CHANGE_SYMBOL_ING:
            this.changeSymbolIng();
            break;

           case SPIN_STATUS.CHANGE_SYMBOL_END:
            this.changeSymbolEnd();
            break;

           case SPIN_STATUS.CHECK_REWARD:
            this.checkReward();
            break;

           case SPIN_STATUS.REWARD_EFFECT_START:
            this.rewardEffectStart();
            break;

           case SPIN_STATUS.REWARD_EFFECT_ING:
            this.rewardEffecting();
            break;

           case SPIN_STATUS.REWARD_EFFECT_END:
            this.rewardEffectEnd();
            break;

           case SPIN_STATUS.CHECK_SCATTER_WIN:
            this.chackScatterWin();
            break;

           case SPIN_STATUS.SCATTER_WIN_WAIT:
            this.scatterWinWait(dt);
            break;

           case SPIN_STATUS.SCATTER_WIN_START:
            this.scatterWinEffect();
            break;

           case SPIN_STATUS.SCATTER_WIN_ING:
            this.scatterWinEffecting();
            break;

           case SPIN_STATUS.WIN_SCORE_EFFECT_START:
            this.winScoreEffectStart();
            break;

           case SPIN_STATUS.WIN_SCORE_EFFECT_ING:
            this.winScoreEffectIng();
            break;

           case SPIN_STATUS.WIN_SCORE_EFFECT_END:
            this.winScoreEffectEnd();
            break;

           case SPIN_STATUS.CHECK_FREE_GAME_COLLECT:
            this.checkFreeGameCollect();
            break;

           case SPIN_STATUS.FREE_GAME_COLLECT_START:
            this.freeGameCollectStart();
            break;

           case SPIN_STATUS.FREE_GAME_COLLECT_ING:
            this.freeGameCollectEffecting();
            break;

           case SPIN_STATUS.FINISHED_WAIT:
            this.spinFinishedWait(dt);
            break;

           case SPIN_STATUS.FINISHED:
            this.spinFinished();
          }
        } else if (this.gameStatus == GAME_STATUS.FREE_GAME_WIN_SCORE_EFFECT) switch (this.freeGameWinScoreEffectStatus) {
         case FREEGAME_WIN_SCORE_EFFECT_STATUS.WIN_SCORE_EFFECT_START:
          this.freeGameWinScoreEffectStart();
          break;

         case FREEGAME_WIN_SCORE_EFFECT_STATUS.WIN_SCORE_EFFECT_ING:
          this.freeGameWinScoreEffectIng();
          break;

         case FREEGAME_WIN_SCORE_EFFECT_STATUS.WIN_SCORE_EFFECT_END:
          this.freeGameWinScoreEffectEnd();
        } else if (this.gameStatus == GAME_STATUS.JACKPOT_WIN_SCORE) switch (this.spinStatus) {
         case SPIN_STATUS.JACKPOT_WIN_START:
          this.jackpotWinStart();
          break;

         case SPIN_STATUS.JACKPOT_WIN_ING:
        } else if (this.gameStatus == GAME_STATUS.IDLE) {
          if (this.isChangeMoney) {
            this.isChangeMoney = false;
            this.data.coin = this.data.getClientMoney(this.newMoney);
            this.ui.setScore(this.data.coin);
          }
          this.hasWinLine && this.TakeTurnsAllWinLine(dt);
        }
      };
      G27_Game.prototype.freeGameWinScoreEffectStart = function() {
        console.log("\u3010\u514d\u8cbb\u904a\u6232 \u8d0f\u9322\u6548\u679c \u3011\u958b\u59cb");
        var _totalWinScore = this.data.totalBonus;
        var _betScore = this.data.playerBetScore;
        this.freeGameWinScoreEffectStatus = FREEGAME_WIN_SCORE_EFFECT_STATUS.WIN_SCORE_EFFECT_ING;
        var _isFreeGame = false;
        this.ui.showWinScoreEffect(_totalWinScore, _betScore, _isFreeGame);
      };
      G27_Game.prototype.freeGameWinScoreEffectIng = function() {
        this.ui.isWinScoreEffecting() || (this.freeGameWinScoreEffectStatus = FREEGAME_WIN_SCORE_EFFECT_STATUS.WIN_SCORE_EFFECT_END);
      };
      G27_Game.prototype.freeGameWinScoreEffectEnd = function() {
        console.log("\u3010\u514d\u8cbb\u904a\u6232 \u8d0f\u9322\u6548\u679c \u3011\u7d50\u675f");
        this.freeGameWinScoreEffectStatus = FREEGAME_WIN_SCORE_EFFECT_STATUS.NONE;
        if (this.data.jackpotData) {
          this.gameStatus = GAME_STATUS.JACKPOT_WIN_SCORE;
          this.spinStatus = SPIN_STATUS.JACKPOT_WIN_START;
        } else {
          this.gameStatus = GAME_STATUS.IDLE;
          this.spinStatus = SPIN_STATUS.FINISHED_WAIT;
          this.checkAutoGameContinue();
        }
        AudioManager_1.AudioManager.instance.playAudioEvent("CloseWinScorePanel");
        this.ui.closeWinScoreEffect();
      };
      G27_Game.prototype.registerEvents = function() {
        var _this = this;
        G27_SocketManager_1.default.getInstance().setBalanceModifyCallback(function(data) {
          _this.onBalanceModifyEvent(data);
        });
      };
      G27_Game.prototype.showSymbolTip = function(symbolID, position) {
        this.gameStatus == GAME_STATUS.IDLE && _super.prototype.showSymbolTip.call(this, symbolID, position);
      };
      G27_Game.prototype.showFXScene = function() {
        return __awaiter(this, void 0, void 0, function() {
          return __generator(this, function(_a) {
            this.ui.FXScene.active = true;
            MarqueeSystemManager_1.default.setMarqueeShow(true);
            this.ui.setBackBtnState(this.backBtnActive);
            this.ui.initJpPosition();
            return [ 2 ];
          });
        });
      };
      G27_Game.prototype.pushGameIcon = function() {
        this.ui.pushGameIcon();
      };
      G27_Game.prototype.fadeInCanvas = function() {
        this.ui.fadeInCanvas();
      };
      G27_Game.prototype.baseGameSpin_Response = function(p_data) {
        var _a;
        this.ui.showSpinMask();
        AudioManager_1.AudioManager.instance.playAudioEvent("StartSpin");
        if (this.data.isAutoSpin) {
          this.autoSpinCount++;
          this.ui.openCancelAutoSpinButton();
          var _autoSpinCount = this.data.autoGameSpinCount - this.autoSpinCount;
          -1 == this.data.autoGameSpinCount && (_autoSpinCount = -1);
          this.ui.setAutoSpinCount(_autoSpinCount);
        }
        this.tempWinLineChangeTime = this.winLineChangeTime - this.firstWinLineChangeTime;
        this.tempSpinCD = 0;
        this.toFastStopSpin = false;
        this.isLineChange = false;
        this.resultListIndex = 0;
        this.data.coin = this.data.getClientMoney(p_data.currentCash);
        this.data.totalBonus = this.data.getClientMoney(p_data.totalBonus);
        this.data.playerBetScore = this.data.getClientMoney(p_data.totalBet);
        this.data.resultList = p_data.resultList;
        this.data.remainingFreeGameCount = 10;
        this.data.jackpotData = p_data.Jackpot;
        var jpAmount = (null === (_a = p_data.Jackpot) || void 0 === _a ? void 0 : _a.amount) || 0;
        var _money = this.data.getClientMoney(p_data.currentCash - p_data.totalBonus - jpAmount);
        this.ui.setScore(_money);
        this.ui.setWinScore(0);
        this.ui.setSpinArrowSpinningSpeed(Slot_GameUI_1.SPIN_ARROW_SPINNIG_TYPE.FAST);
        this.gameStatus = GAME_STATUS.SPIN;
        this.spinStatus = SPIN_STATUS.REEL_SCROLL_START;
        this.ui.spinButton.getComponent(cc.Button).interactable = true;
      };
      G27_Game.prototype.onSpin = function() {
        var _this = this;
        console.log("Spin");
        if (this.gameStatus != GAME_STATUS.SPIN || this.spinStatus != SPIN_STATUS.REEL_SCROLL_ING && this.spinStatus != SPIN_STATUS.CHANGE_SYMBOL_ING) {
          if (this.gameStatus == GAME_STATUS.IDLE) {
            var _betScore = this.data.betOdds[this.data.betOddsStartIndex];
            this.slotReelManager.hideScoreReWardEffect();
            if (this.hasEnoughBetScore()) {
              if (!this.data.isAutoSpin) {
                this.scheduleOnce(function() {
                  return _this.ui.showSpinButtonClickEffect();
                });
                this.spinButtonChange(true);
              }
              _super.prototype.onSpin.call(this);
              this.gameStatus = GAME_STATUS.SERVER_REQUEST;
              this.socket.setOnSpinBaseGameEvent(_betScore).then(function(baseGameSpinResponeData) {
                _this.showResponseLog("SpinBaseGame", baseGameSpinResponeData);
                _this.baseGameSpin_Response(baseGameSpinResponeData);
              }).catch(function(error) {
                error.code == GameClient.errCode.RESPONSE_TIMED_OUT ? _this.socket.showErrorAlert(LocalizationManager_1.default.getInstance().get("ERROR_SOCKET_TIME_OUT"), null, function() {
                  G27_SocketManager_1.default.getInstance().login();
                }) : error.code == GameClient.errCode.SLOT_INSUFFICIENT_BALANCE && _this.socket.showErrorAlert(LocalizationManager_1.default.getInstance().get("ERROR_NO_COIN"), null, function() {
                  if (_this.data.isAutoSpin) {
                    _this.cancelAutoSpin();
                    _this.ui.hideSpinMask();
                    _this.ui.showSpinButtonIdleEffect();
                    _this.ui.setSpinArrowSpinningSpeed(Slot_GameUI_1.SPIN_ARROW_SPINNIG_TYPE.NORMAL);
                  }
                });
                _this.ui.spinButton.getComponent(cc.Button).interactable = true;
                _this.gameStatus = GAME_STATUS.IDLE;
              });
            } else {
              this.showLackMoneyPanel(_betScore * this.data.oneOddsScore);
              this.spinButtonChange(true, false);
              this.data.isAutoSpin && this.cancelAutoSpin();
            }
          }
        } else {
          this.toFastStopSpin = true;
          this.spinButtonChange(false);
        }
      };
      G27_Game.prototype.stopSpin = function() {
        AudioManager_1.AudioManager.instance.playAudioEvent("StopSpin");
        this.slotReelManager.stopAllReelScroll();
        if (this.isJumpChangeSymbol) {
          this.slotReelManager.changeSymbol(this.data.resultList[this.resultListIndex].changePos, this.data.resultList[this.resultListIndex].changeSymbol);
          this.slotReelManager.stopChangeSymbol();
          this.ui.stopAnimalHand();
        }
      };
      G27_Game.prototype.onCloseWinEffect = function() {
        this.ui.winScoreIsEndScore() ? this.winScoreEffectEnd() : this.ui.winScoreEffectToEndScore();
      };
      G27_Game.prototype.jackpotWinStart = function() {
        var _this = this;
        console.log("\u3010 jp \u8d0f\u9322\u6548\u679c \u3011\u958b\u59cb");
        this.spinStatus = SPIN_STATUS.JACKPOT_WIN_ING;
        var done = function() {
          _this.gameStatus = GAME_STATUS.IDLE;
          _this.spinStatus = SPIN_STATUS.NONE;
          _this.ui.addScore(_this.data.getClientMoney(_this.data.jackpotData.amount));
          _this.data.jackpotData = null;
          _this.checkAutoGameContinue();
        };
        cc.systemEvent.emit("JACKPOT_WIN_START", this.data.jackpotData, done);
      };
      G27_Game.prototype.onStartAutoGame = function() {
        console.log("\u8f38\u5206\u505c\u6b62 " + this.data.autoGameScoreLessStop);
        console.log("\u8d0f\u5206\u505c\u6b62 " + this.data.autoGameWinScoreStop);
        AudioManager_1.AudioManager.instance.playAudioEvent("StartAutoGame");
        this.ui.autoSpine.setAnimation(0, "auto_on_stop", false);
        this.data.isAutoSpin = true;
        this.ui.autoSpinSwitch(true);
        this.autoSpinCount = 0;
        this.data.autoGameStartScore = this.data.coin;
        this.onSpin();
      };
      G27_Game.prototype.startLuckyWheel = function() {
        var _this = this;
        console.log("\u958b\u59cb\u5e78\u904b\u8f2a");
        this.saveBaseGameLastSymbol();
        this.slotReelManager.setNormalMode();
        this.slotReelManager.setReelTable(this.data.reelTable_FG);
        this.slotReelManager.allSymbolPlayIdleEffect();
        this.slotReelManager.hideMaskEffect();
        this.ui.changeToFreeGameBackGround();
        this.ui.showWheelBlock();
        this.wheel.setData(this.data.resultList[0].wheelIdx);
        setTimeout(function() {
          _this.wheel.node.active = true;
          _this.wheel.scroll(_this.data.getClientMoney(_this.data.betOdds[_this.data.betOddsStartIndex] * _this.data.oneOddsScore));
          _this.spinStatus = SPIN_STATUS.WHEEL_SCROLL_ING;
        }, 2400);
        this.spinStatus = SPIN_STATUS.WHEEL_SCROLL_START;
        this.gameStatus = GAME_STATUS.FREE_GAME;
      };
      G27_Game.prototype.startFreeGame = function() {
        if (this.gameStatus == GAME_STATUS.CHANGE_PANEL) return;
        console.log("\u958b\u59cb\u514d\u8cbb\u904a\u6232");
        this.resultListIndex++;
        this.freeGameOddsIndex = this.resultListIndex;
        cc.systemEvent.emit("JACKPOT_OPEN", false);
        AudioManager_1.AudioManager.instance.playAudioEvent("StarFreeGame");
        this.gameStatus = GAME_STATUS.FREE_GAME;
        this.spinStatus = SPIN_STATUS.REEL_SCROLL_START;
        this.ui.showFreeGameOddsTip();
        this.ui.showRemainingFreeGameCount();
        this.ui.setRemainingFreeGameCount(this.data.remainingFreeGameCount);
      };
      G27_Game.prototype.freeGameEnd = function() {
        var _this = this;
        var _a;
        if (this.gameStatus == GAME_STATUS.CHANGE_PANEL) return;
        _super.prototype.freeGameEnd.call(this);
        console.log("\u514d\u8cbb\u904a\u6232\u7d50\u675f");
        var jpAmount = this.data.getClientMoney((null === (_a = this.data.jackpotData) || void 0 === _a ? void 0 : _a.amount) || 0);
        var _money = this.data.coin - jpAmount;
        var _winMoney = this.data.totalBonus;
        this.gameStatus = GAME_STATUS.CHANGE_PANEL;
        cc.systemEvent.emit("JACKPOT_OPEN", true);
        this.ui.closeWinScoreEffect();
        this.ui.closeFreeGameGetScorePanel().then(function() {
          AudioManager_1.AudioManager.instance.playAudioEvent("FreeGameEnd");
          if (_this.hasWinScoreEffect(_winMoney)) {
            _this.spinStatus = SPIN_STATUS.NONE;
            _this.freeGameWinScoreEffectStatus = FREEGAME_WIN_SCORE_EFFECT_STATUS.WIN_SCORE_EFFECT_START;
            _this.gameStatus = GAME_STATUS.FREE_GAME_WIN_SCORE_EFFECT;
          } else if (_this.data.jackpotData) {
            _this.gameStatus = GAME_STATUS.JACKPOT_WIN_SCORE;
            _this.spinStatus = SPIN_STATUS.JACKPOT_WIN_START;
          } else {
            _this.gameStatus = GAME_STATUS.IDLE;
            _this.spinStatus = SPIN_STATUS.NONE;
            _this.checkAutoGameContinue();
          }
        });
        this.ui.hideRemainingFreeGameCount();
        this.ui.changeToBaseGameBackGround();
        this.ui.menuButtonSwitch(true);
        this.ui.buttonGroupSwitch(true);
        this.ui.setScore(_money);
        this.ui.setWinScore(_winMoney);
        this.ui.showMainGameTip();
        this.ui.hideGetScoreTip();
        this.slotReelManager.setReelTable(this.data.reelTable_BG);
        this.slotReelManager.setAllReelSymbol(this.baseGameLastSymbol);
        this.slotReelManager.allSymbolPlayIdleEffect();
        this.slotReelManager.hideScoreReWardEffect();
        this.slotReelManager.hideMaskEffect();
        this.hasWinLine = false;
        this.data.isFaseSpin && this.slotReelManager.setFastMode();
      };
      G27_Game.prototype.reelScrollStart = function() {
        console.log("\u3010\u8f2a\u5b50\u6efe\u52d5\u3011 \u958b\u59cb");
        var _targetIndex = this.data.resultList[this.resultListIndex].randomNumList;
        var _hasOmenReels = this.getHasOmenReels();
        this.hasWinLine = false;
        this.nowShowWinLine = 0;
        this.data.isFaseSpin || AudioManager_1.AudioManager.instance.playAudioEvent("ReelScrollStart");
        var _scrollData = new Slot_ReelManager_ScrollData_1.default();
        _scrollData.targetSymbolIndex = _targetIndex;
        _scrollData.hasLongWild = false;
        _scrollData.hasOmenReels = _hasOmenReels;
        this.data.isAutoSpin || this.gameStatus == GAME_STATUS.FREE_GAME ? _scrollData.rewardTime = this.autoRewardTime : _scrollData.rewardTime = this.normalRewardTime;
        this.ui.showBGScrollingEffect();
        this.ui.hideGetScoreTip();
        this.ui.hideGetFreeGameTip();
        this.slotReelManager.scrollAllReels(_scrollData);
        this.socket.resetRoomTimeout();
        this.spinStatus = SPIN_STATUS.REEL_SCROLL_ING;
        if (this.gameStatus == GAME_STATUS.FREE_GAME) {
          this.data.remainingFreeGameCount--;
          this.ui.setRemainingFreeGameCount(this.data.remainingFreeGameCount);
          this.ui.showFreeGameTip();
        } else this.ui.showMainGameTip();
      };
      G27_Game.prototype.getHasOmenReels = function() {
        var _reelhasOmen = [];
        if (this.gameStatus == GAME_STATUS.FREE_GAME) return _reelhasOmen;
        var _symbolCount = [ 4, 3, 3, 3, 4 ];
        var _middleSymbol = null;
        var _targetIndex = this.data.resultList[this.resultListIndex].randomNumList;
        var _scatterCount = 0;
        for (var i = 0; i < _targetIndex.length; i++) {
          _scatterCount >= this.showOmenScatterCount - 1 && 4 === i ? _reelhasOmen[i] = true : _reelhasOmen[i] = false;
          _middleSymbol = Math.floor(_symbolCount[i] / 2);
          for (var j = 0; j < _symbolCount[i]; j++) {
            var _offset = j - _middleSymbol;
            this.data.reelTable_BG[i][_targetIndex[i] + _offset] == Slot_DataManager_1.SYMBOL_NAME.Scatter && _scatterCount++;
          }
        }
        return _reelhasOmen;
      };
      G27_Game.prototype.reelScrolling = function() {
        this.slotReelManager.isReelSrolling() || (this.spinStatus = SPIN_STATUS.REEL_SCROLL_END);
      };
      G27_Game.prototype.reelScrollEnd = function() {
        console.log("\u3010\u8f2a\u5b50\u6efe\u52d5\u3011\u7d50\u675f");
        AudioManager_1.AudioManager.instance.playAudioEvent("AllReelStop");
        this.spinStatus = SPIN_STATUS.CHECK_CHANGE_SYMBOL;
      };
      G27_Game.prototype.checkChangeSymbol = function() {
        console.log("\u3010\u6aa2\u67e5\u6709\u6c92\u6709\u7b2c\u4e00\u8f2a\u6eff\u8ef8\u3011\u958b\u59cb");
        if (0 !== this.data.resultList[this.resultListIndex].changeSymbol && !this.isJumpChangeSymbol) {
          this.spinStatus = SPIN_STATUS.CHANGE_SYMBOL_START;
          return;
        }
        this.spinStatus = SPIN_STATUS.CHECK_REWARD;
      };
      G27_Game.prototype.changeSymbolStart = function() {
        console.log("\u3010\u6539\u8b8a\u5716\u793a\u3011\u958b\u59cb");
        AudioManager_1.AudioManager.instance.playAudioEvent("H1ToWild");
        var aniType = [];
        var curCount = 0;
        for (var i = 0; i < 5; i++) {
          curCount = 0;
          for (var j = 0 === i || 4 === i ? 0 : 1; j < 4; j++) 1 === this.data.resultList[this.resultListIndex].changePos[i][j] && curCount++;
          aniType[i] = 0 === curCount ? 0 : curCount === (0 === i || 4 === i ? 4 : 3) ? 2 : 1;
        }
        this.ui.showAnimalHand(this.data.resultList[this.resultListIndex].changeSymbol, aniType);
        this.slotReelManager.changeSymbolEffect(this.data.resultList[this.resultListIndex].changePos, this.data.resultList[this.resultListIndex].changeSymbol, aniType, this.gameStatus === GAME_STATUS.FREE_GAME);
        this.spinStatus = SPIN_STATUS.CHANGE_SYMBOL_ING;
      };
      G27_Game.prototype.changeSymbolIng = function() {
        this.slotReelManager.isChangeSymbolIng() || (this.spinStatus = SPIN_STATUS.CHANGE_SYMBOL_END);
      };
      G27_Game.prototype.changeSymbolEnd = function() {
        this.spinStatus = SPIN_STATUS.CHECK_REWARD;
      };
      G27_Game.prototype.checkReward = function() {
        console.log("\u6aa2\u67e5\u4e2d\u734e");
        this.isJumpChangeSymbol = false;
        var _winScore = this.data.getClientMoney(this.data.resultList[this.resultListIndex].totalWinBonus);
        if (this.hasWinLineList()) {
          this.hasWinLine = true;
          this.spinStatus = SPIN_STATUS.REWARD_EFFECT_START;
        } else this.gameStatus != GAME_STATUS.FREE_GAME ? this.spinStatus = SPIN_STATUS.CHECK_SCATTER_WIN : this.hasWinScoreEffect(_winScore) ? this.spinStatus = SPIN_STATUS.WIN_SCORE_EFFECT_START : this.spinStatus = SPIN_STATUS.CHECK_FREE_GAME_COLLECT;
      };
      G27_Game.prototype.hasWinLineList = function() {
        var _winWayList = this.data.resultList[this.resultListIndex].winWayList;
        if (_winWayList.length > 0) {
          if (1 == _winWayList.length && _winWayList[0].winNum == Slot_DataManager_1.SYMBOL_NAME.Scatter) return false;
          return true;
        }
        return false;
      };
      G27_Game.prototype.rewardEffectStart = function() {
        return __awaiter(this, void 0, void 0, function() {
          var _wheelScore, _winScore, _getNowScore, _winPositstion;
          return __generator(this, function(_a) {
            switch (_a.label) {
             case 0:
              console.log("\u3010\u4e2d\u734e\u7279\u6548\u3011 \u958b\u59cb");
              _wheelScore = this.data.resultList[this.resultListIndex].wheelBonus;
              _winScore = this.data.getClientMoney(this.data.resultList[this.resultListIndex].totalWinBonus - _wheelScore);
              _getNowScore = this.data.getClientMoney(this.data.resultList[this.resultListIndex].totalWinBonus - _wheelScore);
              _winPositstion = this.getTotalWinPos();
              this.slotReelManager.showSymbolReWardEffect(_winPositstion, true);
              this.spinStatus = SPIN_STATUS.REWARD_EFFECT_ING;
              return [ 4, CommonTool_1.CommonTool.toNextTick() ];

             case 1:
              _a.sent();
              if (this.gameStatus === GAME_STATUS.FREE_GAME) {
                this.ui.showFreeGamePlayWinEffect();
                AudioManager_1.AudioManager.instance.playAudioEvent("PlayRewardFree");
              } else if (this.hasLeopardEffect(_winScore)) {
                this.ui.showMainGamePlayBigWinEffect();
                AudioManager_1.AudioManager.instance.playAudioEvent("PlayRewardMainLeopard");
              } else {
                this.ui.showMainGamePlayWinEffect();
                AudioManager_1.AudioManager.instance.playAudioEvent("PlayRewardMain");
              }
              this.ui.addScore(_winScore);
              this.ui.addWinScore(_winScore);
              this.ui.hideGameTip();
              this.ui.showGetScoreTip(_getNowScore);
              this.ui.showTotalGetScore(_getNowScore);
              return [ 2 ];
            }
          });
        });
      };
      G27_Game.prototype.getWinPos = function(p_winLineIndex) {
        return this.data.resultList[this.resultListIndex].winWayList[p_winLineIndex].singlePos;
      };
      G27_Game.prototype.getTotalWinPos = function() {
        var _winPos = this.data.resultList[this.resultListIndex].totalHitPos;
        var _scatterWinPos = this.data.resultList[this.resultListIndex].scatter.scatterPos;
        if (this.data.resultList[this.resultListIndex].scatter.count >= 3) for (var i = 0; i < 5; i++) for (var j = 0; j < 4; j++) 1 == _scatterWinPos[i][j] && (_winPos[i][j] = 0);
        return _winPos;
      };
      G27_Game.prototype.rewardEffecting = function() {
        this.slotReelManager.isRewardEffecting() || (this.spinStatus = SPIN_STATUS.REWARD_EFFECT_END);
      };
      G27_Game.prototype.rewardEffectEnd = function() {
        console.log("\u3010\u4e2d\u734e\u7279\u6548\u3011 \u7d50\u675f");
        var _winScore = this.data.getClientMoney(this.data.resultList[this.resultListIndex].totalWinBonus);
        this.gameStatus != GAME_STATUS.FREE_GAME ? this.spinStatus = SPIN_STATUS.CHECK_SCATTER_WIN : this.hasWinScoreEffect(_winScore) ? this.spinStatus = SPIN_STATUS.WIN_SCORE_EFFECT_START : this.spinStatus = SPIN_STATUS.CHECK_FREE_GAME_COLLECT;
      };
      G27_Game.prototype.chackScatterWin = function() {
        console.log("\u6aa2\u67e5scatter\u4e2d\u734e");
        var _scatterCount = this.data.resultList[this.resultListIndex].scatter.count;
        var _winScore = this.data.getClientMoney(this.data.resultList[this.resultListIndex].totalWinBonus);
        _scatterCount >= 3 ? this.spinStatus = this.hasWinLineList() ? SPIN_STATUS.SCATTER_WIN_WAIT : SPIN_STATUS.SCATTER_WIN_START : this.hasWinScoreEffect(_winScore) ? this.spinStatus = SPIN_STATUS.WIN_SCORE_EFFECT_START : this.spinStatus = SPIN_STATUS.FINISHED_WAIT;
      };
      G27_Game.prototype.scatterWinWait = function(p_dt) {
        this.tempScatterWaitTime += p_dt;
        var rewardTime;
        rewardTime = this.data.isAutoSpin || this.gameStatus == GAME_STATUS.FREE_GAME ? this.autoRewardTime : this.firstWinLineChangeTime;
        if (this.tempScatterWaitTime > rewardTime) {
          this.tempScatterWaitTime = 0;
          this.spinStatus = SPIN_STATUS.SCATTER_WIN_START;
        }
      };
      G27_Game.prototype.scatterWinEffect = function() {
        console.log("\u3010\u4e2d\u734e\u7279\u6548\u3011 Scatter");
        var scatterData = this.data.resultList[this.resultListIndex].scatter;
        var _winPositstion = scatterData.scatterPos;
        this.ui.showMainGamePlayWinEffect();
        this.gameStatus == GAME_STATUS.SPIN && this.hasFreeGame() && AudioManager_1.AudioManager.instance.playAudioEvent("GotFreeGame");
        this.slotReelManager.allSymbolPlayIdleEffect();
        this.slotReelManager.showSymbolReWardEffect(_winPositstion, true);
        this.spinStatus = SPIN_STATUS.SCATTER_WIN_ING;
      };
      G27_Game.prototype.scatterWinEffecting = function() {
        this.slotReelManager.isRewardEffecting() || (this.spinStatus = SPIN_STATUS.FINISHED_WAIT);
      };
      G27_Game.prototype.winScoreEffectStart = function() {
        console.log("\u3010 \u8d0f\u9322\u6548\u679c \u3011\u958b\u59cb");
        this.spinStatus = SPIN_STATUS.WIN_SCORE_EFFECT_ING;
        var _totalWinScore = this.data.getClientMoney(this.data.resultList[this.resultListIndex].totalWinBonus);
        var _betScore = this.data.playerBetScore;
        var _isFreeGame = this.gameStatus === GAME_STATUS.FREE_GAME;
        this.ui.showWinScoreEffect(_totalWinScore, _betScore, _isFreeGame);
      };
      G27_Game.prototype.winScoreEffectIng = function() {
        this.ui.isWinScoreEffecting() || (this.spinStatus = SPIN_STATUS.WIN_SCORE_EFFECT_END);
      };
      G27_Game.prototype.winScoreEffectEnd = function() {
        console.log("\u3010 \u8d0f\u9322\u6548\u679c \u3011\u7d50\u675f");
        if (this.gameStatus == GAME_STATUS.FREE_GAME_WIN_SCORE_EFFECT) this.freeGameWinScoreEffectStatus = FREEGAME_WIN_SCORE_EFFECT_STATUS.WIN_SCORE_EFFECT_END; else {
          if (this.gameStatus == GAME_STATUS.FREE_GAME) {
            this.spinStatus = SPIN_STATUS.CHECK_FREE_GAME_COLLECT;
            AudioManager_1.AudioManager.instance.playAudioEvent("CloseWinScorePanelFreeGame");
          } else {
            this.spinStatus = SPIN_STATUS.FINISHED_WAIT;
            AudioManager_1.AudioManager.instance.playAudioEvent("CloseWinScorePanel");
          }
          this.ui.closeWinScoreEffect();
        }
      };
      G27_Game.prototype.checkFreeGameCollect = function() {
        console.log("\u6aa2\u67e5Collect\u7279\u6548");
        var _scatterCount = this.data.resultList[this.resultListIndex].scatter.count;
        this.spinStatus = _scatterCount >= 3 ? SPIN_STATUS.FREE_GAME_COLLECT_START : SPIN_STATUS.FINISHED_WAIT;
      };
      G27_Game.prototype.freeGameCollectStart = function() {
        console.log("\u64a5\u653efree game Collect\u7279\u6548");
        var scatterPos = this.data.resultList[this.resultListIndex].scatter.scatterPos;
        this.slotReelManager.allSymbolPlayIdleEffect();
        this.slotReelManager.showSymbolReWardEffect(scatterPos, true);
        this.ui.showFreeGameCollctEffect(this.slotReelManager.getSymbolReWardPos(scatterPos), this.data.remainingFreeGameCount);
        this.data.remainingFreeGameCount += 2;
        this.spinStatus = SPIN_STATUS.FREE_GAME_COLLECT_ING;
      };
      G27_Game.prototype.freeGameCollectEffecting = function() {
        this.ui.isFreeGameCollctEffecting || (this.spinStatus = SPIN_STATUS.FINISHED_WAIT);
      };
      G27_Game.prototype.spinFinishedWait = function(p_dt) {
        if (0 == this.tempWaitTime) {
          var _totalWinScore = this.data.getClientMoney(this.data.resultList[this.resultListIndex].totalWinBonus);
          this.data.isAutoSpin || this.gameStatus == GAME_STATUS.FREE_GAME ? this.tempSpinFinishedStopTime = this.fastSpinFinishedStopTime : this.tempSpinFinishedStopTime = this.normalSpinFinishedStopTime;
          if (_totalWinScore > 0) {
            AudioManager_1.AudioManager.instance.playAudioEvent("WinScore");
            this.data.isAutoSpin ? this.tempSpinFinishedStopTime += this.autoSpinUpdateTipBroadEffectTime : this.tempSpinFinishedStopTime += this.normalUpdateTipBroadEffectTime;
          }
        }
        this.tempWaitTime += p_dt;
        if (this.tempWaitTime > this.tempSpinFinishedStopTime) {
          this.spinStatus = SPIN_STATUS.FINISHED;
          this.tempWaitTime = 0;
        }
      };
      G27_Game.prototype.spinFinished = function() {
        var _this = this;
        console.log("\u3010Spin\u3011 \u7d50\u675f");
        if (this.gameStatus == GAME_STATUS.FREE_GAME) if (this.hasNextResultList()) {
          this.resultListIndex++;
          this.slotReelManager.allSymbolPlayIdleEffect();
          this.slotReelManager.hideScoreReWardEffect();
          this.spinStatus = SPIN_STATUS.REEL_SCROLL_START;
        } else {
          this.ui.hideFreeGameOddsTip();
          console.log("\u958b\u555f\u514d\u8cbb\u904a\u6232\u7d50\u7b97\u9762\u677f");
          var _wheelScore = this.data.getClientMoney(this.data.resultList[0].wheelBonus);
          var _freeGameWinMoney = this.data.totalBonus + _wheelScore - this.data.getClientMoney(this.data.resultList[0].totalWinBonus);
          var _betScore = this.data.playerBetScore;
          this.ui.openFreeGameGetScorePanel(_freeGameWinMoney, _betScore);
          this.data.isAutoSpin || this.ui.setSpinArrowSpinningSpeed(Slot_GameUI_1.SPIN_ARROW_SPINNIG_TYPE.NORMAL);
          this.gameStatus = GAME_STATUS.FREE_GAME_REWARD;
          this.spinStatus = SPIN_STATUS.NONE;
          setTimeout(function() {
            _this.gameStatus == GAME_STATUS.FREE_GAME_REWARD && _this.freeGameEnd();
          }, 6e3);
        } else if (this.hasFreeGame()) cc.tween(this.node).call(function() {
          _this.gameStatus = GAME_STATUS.FREE_GAME_TIP;
        }).delay(this.scatterShowTime).call(function() {
          _this.startLuckyWheel();
        }).start(); else if (this.data.jackpotData) {
          this.gameStatus = GAME_STATUS.JACKPOT_WIN_SCORE;
          this.spinStatus = SPIN_STATUS.JACKPOT_WIN_START;
        } else {
          this.gameStatus = GAME_STATUS.IDLE;
          this.spinStatus = SPIN_STATUS.NONE;
          this.checkAutoGameContinue();
        }
      };
      G27_Game.prototype.TakeTurnsAllWinLine = function(p_dt) {
        this.tempWinLineChangeTime += p_dt;
        this.data.resultList[this.resultListIndex].winWayList.sort(function(a, b) {
          return a.winNum - b.winNum;
        });
        var _winLineCount = this.data.resultList[this.resultListIndex].winWayList.length;
        if (1 == _winLineCount && 1 == this.nowShowWinLine) return;
        if (this.tempWinLineChangeTime > this.winLineChangeTime + this.winLineChangeWaitTime) {
          console.log("\u8f2a\u6d41\u64ad\u653e\u6240\u6709\u7684\u4e2d\u734e\u9023\u7dda");
          this.isLineChange = false;
          var _winPositstion = null;
          this.tempWinLineChangeTime = 0;
          this.nowShowWinLine > _winLineCount && (this.nowShowWinLine = 0);
          if (this.nowShowWinLine == _winLineCount) _winPositstion = this.getTotalWinPos(); else {
            _winPositstion = this.getWinPos(this.nowShowWinLine);
            var _wayScore = this.data.resultList[this.resultListIndex].winWayList[this.nowShowWinLine].winBonus.toString();
            this.slotReelManager.showScoreReWardEffect(_winPositstion, this.data.getClientMoney(_wayScore).toString());
          }
          this.slotReelManager.showSymbolReWardEffect(_winPositstion, true);
          AudioManager_1.AudioManager.instance.playAudioEvent("ShowOneWinLine");
          this.nowShowWinLine++;
        } else if (this.tempWinLineChangeTime > this.winLineChangeTime && !this.isLineChange) {
          this.isLineChange = true;
          this.slotReelManager.allSymbolPlayIdleEffect();
          this.slotReelManager.hideScoreReWardEffect();
          this.slotReelManager.SetAllSymbolZOrder(0);
        }
      };
      G27_Game.prototype.hasNextResultList = function() {
        if (this.resultListIndex < this.data.resultList.length - 1) return true;
        return false;
      };
      G27_Game.prototype.hasFreeGame = function() {
        if (this.data.resultList.length > 1 || this.data.resultList[0].wheelIdx > 0) return true;
        return false;
      };
      G27_Game.prototype.setReelInfo = function(p_data) {
        this.data.reelStartIndex = p_data.position;
        this.data.reelTable_BG = p_data.reels.BG;
        this.data.reelTable_FG = p_data.reels.FG;
        this.slotReelManager.setDisplaySymbolCount([ 4, 3, 3, 3, 4 ]);
        this.slotReelManager.setReelTable(this.data.reelTable_BG);
        this.slotReelManager.setReelStartIndex(this.data.reelStartIndex);
        this.wheel.setWheelRatio(p_data.wheelRatioList);
      };
      G27_Game.prototype.onErrorEvent = function(_a) {
        var errCode = _a.errCode;
        console.error("\u6536\u5230\u932f\u8aa4\u8a0a\u606f", errCode);
      };
      G27_Game.prototype.playerLogin = function() {
        this.data.betOddsStartIndex = this.data.getBetIndex();
        var _money = this.data.coin;
        var _betMoney = this.data.getClientMoney(this.data.betOdds[this.data.betOddsStartIndex] * this.data.oneOddsScore);
        this.ui.setScore(_money);
        this.ui.setBetScore(_betMoney);
        this.ui.setWinScore(0);
      };
      G27_Game.prototype.onOpenAutoGamePanel = function() {
        _super.prototype.onOpenAutoGamePanel.call(this);
        this.ui.autoSpine.setAnimation(0, "auto_off_play", false);
      };
      G27_Game.prototype.onAddScroe = function() {
        var _this = this;
        _super.prototype.onAddScroe.call(this);
        this.ui.plusSpine.setAnimation(0, "plus_off-on", false);
        this.ui.plusSpine.setCompleteListener(function() {
          _this.ui.plusSpine.setCompleteListener(null);
          _this.ui.plusSpine.setAnimation(0, "plus_off_stop", false);
        });
      };
      G27_Game.prototype.onReduceScore = function() {
        var _this = this;
        _super.prototype.onReduceScore.call(this);
        this.ui.minusSpine.setAnimation(0, "minus_off-on", false);
        this.ui.minusSpine.setCompleteListener(function() {
          _this.ui.minusSpine.setCompleteListener(null);
          _this.ui.minusSpine.setAnimation(0, "minus_off_stop", false);
        });
      };
      G27_Game.prototype.onFastSpinOn = function() {
        var _this = this;
        _super.prototype.onFastSpinOn.call(this);
        this.ui.turboSpine.setAnimation(0, "turbo_on_play", false);
        this.ui.turboSpine.setCompleteListener(function() {
          _this.ui.turboSpine.setCompleteListener(null);
          _this.ui.turboSpine.setAnimation(0, "turbo_on_stop", false);
        });
      };
      G27_Game.prototype.onFastSpinOff = function() {
        var _this = this;
        _super.prototype.onFastSpinOff.call(this);
        this.ui.turboSpine.setAnimation(0, "turbo_off_play", false);
        this.ui.turboSpine.setCompleteListener(function() {
          _this.ui.turboSpine.setCompleteListener(null);
          _this.ui.turboSpine.setAnimation(0, "turbo_off_stop", false);
        });
      };
      G27_Game.prototype.onCancelAutoSpinButton = function() {
        _super.prototype.onCancelAutoSpinButton.call(this);
        this.ui.mainSpine.setAnimation(0, "main_on_play", false);
      };
      G27_Game.prototype.onOpenMenu = function() {
        console.log("\u9078\u55ae\u958b\u555f");
        AudioManager_1.AudioManager.instance.playAudioEvent("OpenMenu");
        this.ui.menuButtonSwitch(false);
        this.ui.openMenu();
      };
      G27_Game.prototype.spinButtonChange = function(isStart, isOn) {
        var _this = this;
        void 0 === isOn && (isOn = true);
        if (isStart) {
          this.ui.mainSpine.setAnimation(0, "main_off_play", false);
          this.ui.mainSpine.setCompleteListener(function() {
            _this.ui.mainSpine.setCompleteListener(null);
            isOn ? _this.ui.mainSpine.setAnimation(0, "main_on_stop", false) : _this.ui.mainSpine.setAnimation(0, "main_off_stop", false);
          });
        } else {
          this.ui.mainSpine.setAnimation(0, "main_on_play", false);
          this.ui.mainSpine.setCompleteListener(function() {
            _this.ui.mainSpine.setCompleteListener(null);
            _this.ui.mainSpine.setAnimation(0, "main_off_stop", false);
          });
        }
      };
      G27_Game.prototype.wheelScrolling = function() {
        this.wheel.checkRewardEnd() && (this.spinStatus = SPIN_STATUS.WHEEL_CHECK_REWARD);
      };
      G27_Game.prototype.wheelCheckReward = function() {
        this.spinStatus = SPIN_STATUS.NONE;
        this.wheel.node.active = false;
        10 === this.data.resultList[0].wheelIdx ? this.startFreeGame() : this.spinFinished();
      };
      G27_Game.prototype.hasLeopardEffect = function(p_score) {
        if (p_score >= this.data.playerBetScore * this.leopardEffectRate && p_score < this.data.playerBetScore * this.winEffectOdds) return true;
        return false;
      };
      G27_Game.prototype.cancelAutoSpin = function() {
        _super.prototype.cancelAutoSpin.call(this);
        this.ui.autoSpine.setAnimation(0, "auto_off_stop", false);
      };
      __decorate([ property({
        type: G27_SlotReelManager_1.default,
        override: true
      }) ], G27_Game.prototype, "slotReelManager", void 0);
      __decorate([ property({
        type: G27_Wheel_1.default
      }) ], G27_Game.prototype, "wheel", void 0);
      __decorate([ property({
        type: G27_GameUI_1.default,
        override: true
      }) ], G27_Game.prototype, "ui", void 0);
      __decorate([ property({
        type: cc.Float,
        displayName: "\u4e2d\u734e\u7b2c\u4e00\u6b21\u63db\u7dda\u7684\u6642\u9593"
      }) ], G27_Game.prototype, "firstWinLineChangeTime", void 0);
      __decorate([ property({
        type: cc.Float,
        displayName: "\u4e2d\u734e\u7dda\u66f4\u63db\u6642\u9593"
      }) ], G27_Game.prototype, "winLineChangeTime", void 0);
      __decorate([ property({
        type: cc.Float,
        displayName: "\u4e2d\u734e\u7dda\u66f4\u63db\u7b49\u5f85\u6642\u9593"
      }) ], G27_Game.prototype, "winLineChangeWaitTime", void 0);
      __decorate([ property({
        type: cc.Float,
        displayName: "spin\u51b7\u537b\u6642\u9593",
        tooltip: "\u958b\u59cbSpin\u5f8c\u7684\u51b7\u537b\u6642\u9593\uff0c\u54ea\u4f86\u8b93\u6efe\u8f2a\u6703\u7a0d\u5fae\u6efe\u52d5\u5728\u505c\u6b62"
      }) ], G27_Game.prototype, "spinCD", void 0);
      __decorate([ property({
        type: cc.Float,
        displayName: "\u4e00\u822cspin\u7d50\u675f\u505c\u6b62\u7b49\u5f85\u6642\u9593"
      }) ], G27_Game.prototype, "normalSpinFinishedStopTime", void 0);
      __decorate([ property({
        type: cc.Float,
        displayName: "\u5feb\u901fspin\u7d50\u675f\u505c\u6b62\u7b49\u5f85\u6642\u9593"
      }) ], G27_Game.prototype, "fastSpinFinishedStopTime", void 0);
      __decorate([ property({
        type: cc.Float,
        displayName: "\u624b\u52d5Spin \u4e2d\u734e\u66f4\u65b0\u63d0\u793a\u9762\u677f\u7279\u6548\u6642\u9593",
        tooltip: "\u53ea\u6709\u5728\u4e2d\u734e\u7684\u6642\u5019\u624d\u6703\u5403\u9019\u500b\u53c3\u6578"
      }) ], G27_Game.prototype, "normalUpdateTipBroadEffectTime", void 0);
      __decorate([ property({
        type: cc.Float,
        displayName: "\u81ea\u52d5Spin \u4e2d\u734e\u66f4\u65b0\u63d0\u793a\u9762\u677f\u7279\u6548\u6642\u9593",
        tooltip: "\u53ea\u6709\u5728\u4e2d\u734e\u7684\u6642\u5019\u624d\u6703\u5403\u9019\u500b\u53c3\u6578"
      }) ], G27_Game.prototype, "autoSpinUpdateTipBroadEffectTime", void 0);
      __decorate([ property({
        type: cc.Float,
        displayName: "\u624b\u52d5Spin\u4e2d\u734e\u6642\u9593"
      }) ], G27_Game.prototype, "normalRewardTime", void 0);
      __decorate([ property({
        type: cc.Float,
        displayName: "\u81ea\u52d5Spin\u4e2d\u734e\u6642\u9593"
      }) ], G27_Game.prototype, "autoRewardTime", void 0);
      __decorate([ property({
        type: cc.Float,
        displayName: "scatter\u986f\u793a\u7684\u6642\u9593"
      }) ], G27_Game.prototype, "scatterShowTime", void 0);
      __decorate([ property({
        type: cc.Float,
        displayName: "\u51fa\u73fe\u9810\u5146\u7684Scatter\u6578\u91cf"
      }) ], G27_Game.prototype, "showOmenScatterCount", void 0);
      G27_Game = __decorate([ ccclass ], G27_Game);
      return G27_Game;
    }(Slot_GameManager_1.default);
    exports.default = G27_Game;
    cc._RF.pop();
  }, {
    "../../Common/Tools/AudioManager/AudioManager": void 0,
    "../../Common/Tools/CommonTool": void 0,
    "../../Common/Tools/Localization/LocalizationManager": void 0,
    "../../Common/Tools/MarqueeSystem/MarqueeSystemManager": void 0,
    "../../SlotFramework/Game/Data/Slot_GameManager_InitData": void 0,
    "../../SlotFramework/Game/Data/Slot_GameUI_InitData": void 0,
    "../../SlotFramework/Game/Data/Slot_ReelManager_InitData": void 0,
    "../../SlotFramework/Game/Data/Slot_ReelManager_ScrollData": void 0,
    "../../SlotFramework/Game/Slot_GameManager": void 0,
    "../../SlotFramework/Game/view/Slot_GameUI": void 0,
    "../../SlotFramework/Slot_DataManager": void 0,
    "../Common/G27_DataManager": "G27_DataManager",
    "../Common/G27_DynamicPopUpPanelManager": "G27_DynamicPopUpPanelManager",
    "../Common/Socket/G27_SocketManager": "G27_SocketManager",
    "./View/G27_GameUI": "G27_GameUI",
    "./View/G27_SlotReelManager": "G27_SlotReelManager",
    "./View/G27_Wheel": "G27_Wheel"
  } ],
  G27_Language: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "8c334Gfo8xFJI3pS/bfYJic", "G27_Language");
    "use strict";
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    exports.G27_Language = void 0;
    exports.G27_Language = {
      "zh-cn": {
        TEXT_SLOT_MONEY_LESS_SETTING: "\u6301\u6709\u91d1\u989d\u4f4e\u4e8e\u4f59\u989d\u51cf\u5c11\u8bbe\u5b9a\u91d1\u989d\uff0c\u8bf7\u8c03\u6574\u8bbe\u5b9a",
        TEXT_SLOT_RULE_PAGE_1_1: "\u70b9\u51fb\u52a0\u51cf\u53f7\u6309\u94ae\uff0c\u8c03\u6574\u6295\u6ce8\u7684\u603b\u989d",
        TEXT_SLOT_RULE_PAGE_1_2: "\u70b9\u51fb\u65cb\u8f6c\u6309\u94ae\u4ee5\u5f53\u524d\u7684\u6295\u6ce8\u65cb\u8f6c\u8f6c\u8f74",
        TEXT_SLOT_RULE_PAGE_1_3: "\u70b9\u51fb\u81ea\u52a8\u6e38\u620f\u6309\u94ae\u4f1a\u5f00\u542f\u81ea\u52a8\u6e38\u620f\u8bbe\u7f6e\uff0c\u81ea\u52a8\u6e38\u620f\u542f\u7528\u65f6\uff0c\u70b9\u51fb\u65cb\u8f6c\u6309\u94ae\u53ef\u505c\u6b62\u81ea\u52a8\u6a21\u5f0f",
        TEXT_SLOT_RULE_PAGE_1_4: "\u70b9\u51fb\u6781\u901f\u6e38\u620f\u6309\u94ae\uff0c\u6309\u94ae\u4eae\u8d77\u65f6\u53ef\u5f00\u542f\u6781\u901f\u6a21\u5f0f\uff0c\u6781\u901f\u6a21\u5f0f\u53ef\u4e0e\u81ea\u52a8\u6e38\u620f\u540c\u65f6\u751f\u6548",
        TEXT_SLOT_RULE_PAGE_1_5: "A\uff1d\u6295\u6ce8\u603b\u989d\nB\uff1d\u6295\u6ce8\u7ebf\u6570\uff08\u672c\u6e38\u620f\u4ee550\u7ebf\u8ba1\u7b97\uff09\nC\uff1d\u56fe\u6807\u8fde\u7ebf\u8d54\u7387\u500d\u6570\uff08\u8bf7\u53c2\u8003\u8d54\u7387\u8868\uff09\nD\uff1d\u514d\u8d39\u6e38\u620f\u500d\u7387\uff08\u514d\u8d39\u6e38\u620f\u9636\u6bb5\u624d\u8ba1\u7b97\uff09",
        TEXT_SLOT_RULE_PAGE_1_6: "1\u7ebf\u4e2d\u5956\u91d1\u989d\uff1d\uff08A / B\uff09x C x D",
        TEXT_SLOT_RULE_PAGE_2_1: "\u3000\u3000\u3000\u56fe\u6807\u53ef\u66ff\u4ee3\u9664\u4e86\u3000\u3000\u3000\u4ee5\u5916\u7684\u6240\u6709\u56fe\u6807",
        TEXT_SLOT_RULE_PAGE_2_2: "\u53ea\u4f1a\u51fa\u73b0\u5728\u7b2c2\u3001\u7b2c3\u3001\u7b2c4\u8f74\u4e0a",
        TEXT_SLOT_RULE_PAGE_3_1: "\u3000\u3000\u3000\u4ec5\u51fa\u73b0\u57282\u30013\u30014\u6eda\u8f6e\u53ef\u66ff\u4ee3\u9664\u4e86\n\u3000\u3000\u3000\u4ee5\u5916\u7684\u4efb\u610f\u56fe\u6807\u6765\u8fbe\u5230\u8fde\u7ebf\u5206\u6570\uff01",
        TEXT_SLOT_RULE_PAGE_3_2: "\u6eda\u8f6e1\u30013\u30015\u51fa\u73b0\u3000\u3000\u3000\u53ef\u8fdb\u5165\u5e78\u8fd0\u8f6e\uff0c\u53ef\u83b7\u5f97\u9ad8\u989d\u5956\u91d1\u6216\u662f\u514d\u8d39\u6e38\u620f10\u6b21",
        TEXT_SLOT_RULE_PAGE_3_3: "\u514d\u8d39\u6e38\u620f\u4e2d\uff0c\u6bcf\u8f6e\u65cb\u8f6c\u5c06\u4f1a\u968f\u673a\u5956\u91d1\u500d\u6570\n\uff08\uff11\uff5e\uff18\u500d\uff09",
        TEXT_SLOT_RULE_PAGE_4_1: "\u5f53\u4e3b\u76d8\u9762\u7b2c\u4e00\u8f6e\u5e03\u6ee1\u3000\u3000\u3000\u3000\u3000\u3000\u3000\u3000\u65f6\uff0c2~5\u8f6e\u4e2d\u5982\u51fa\u73b0\u524d\u8ff0\u76843\u79cd\u4efb\u4e00\u56fe\u793a\uff0c\u90fd\u4f1a\u53d8\u6362\u4e3a\u4e0e\u7b2c\u4e00\u8f6e\u76f8\u540c\u7684\u56fe\u6807",
        TEXT_SLOT_RULE_PAGE_4_2: "\u53d8\u6362\u524d\u76d8\u9762",
        TEXT_SLOT_RULE_PAGE_4_3: "\u53d8\u6362\u540e\u76d8\u9762",
        TEXT_SLOT_RULE_PAGE_4_4: "\u514d\u8d39\u6e38\u620f\u671f\u95f4\u4e5f\u4f1a\u89e6\u53d1\u4e3b\u6e38\u620f\u7279\u8272\uff0c\u5e76\u4e14\u518d\u89e6\u53d1\u65f6\uff0c2~5\u8f6e\u6709\u673a\u7387\u4f1a\u6574\u8f6e\u53d8\u6362\u4e3a\u4e0e\u7b2c\u4e00\u8f6e\u76f8\u540c\u56fe\u793a\u3002 (\u5982\u4e0a\u56fe)",
        TEXT_SLOT_RULE_PAGE_4_5: "\u514d\u8d39\u6e38\u620f\u671f\u95f4\u83b7\u5f973\u4e2a\u3000\u3000\u3000\uff0c\u4f1a\u518d\u589e\u52a0\u514d\u8d39\u65cb\u8f6c\u6b21\u65702\u6b21",
        TEXT_SLOT_RULE_PAGE_5_1: "\u6eda\u8f6e\u4e0a\u7531\u6700\u5de6\u81f3\u53f3\u8fde\u7eed\u51fa\u73b03\u4e2a\u4ee5\u4e0a\u7684\u76f8\u540c\u56fe\u6807\uff0c\u5373\u53ef\u83b7\u5f97\u8be5\u56fe\u6807\u7684\u5f97\u5206\u5956\u52b1\uff01",
        TEXT_SLOT_RULE_PAGE_ERROR: "\u6e38\u620f\u51fa\u73b0\u6545\u969c\u65f6\uff0c\u6240\u6709\u8d54\u4ed8\u548c\u6e38\u620f\u90fd\u89c6\u4e3a\u65e0\u6548",
        SymbolTip_Scatter: "3 \u4e2a\u5206\u6563\u56fe\u6807\u53ef\u89e6\u53d1\u5e78\u8fd0\u8f6e",
        SymbolTip_Wild: "\u767e\u642d\u56fe\u6807\u53ef\u4ee3\u66ff\u9664\u5206\u6563\u56fe\u6807\u5916\u7684\u6240\u6709\u56fe\u6807",
        TEXT_SLOT_FREEGAME_WORD_1: "\u606d\u559c\u83b7\u5f97",
        TEXT_SLOT_FREEGAME_WORD_2: "\u514d\u8d39\u6e38\u620f",
        TEXT_SLOT_FREEGAME_WORD_3: "<b>\u65cb\u8f6c\u6b21\u6570<color=8be4a9>{0}</color>\u6b21</b>"
      },
      "zh-tw": {
        TEXT_SLOT_MONEY_LESS_SETTING: "\u6301\u6709\u91d1\u984d\u4f4e\u65bc\u9918\u984d\u6e1b\u5c11\u8a2d\u5b9a\u91d1\u984d\uff0c\u8acb\u8abf\u6574\u8a2d\u5b9a",
        TEXT_SLOT_RULE_PAGE_1_1: "\u9ede\u64ca\u52a0\u6e1b\u865f\u6309\u9215\uff0c\u8abf\u6574\u6295\u6ce8\u7684\u7e3d\u984d",
        TEXT_SLOT_RULE_PAGE_1_2: "\u9ede\u64ca\u65cb\u8f49\u6309\u9215\u4ee5\u7576\u524d\u7684\u6295\u6ce8\u65cb\u8f49\u8f49\u8ef8",
        TEXT_SLOT_RULE_PAGE_1_3: "\u9ede\u64ca\u81ea\u52d5\u904a\u6232\u6309\u9215\u6703\u958b\u555f\u81ea\u52d5\u904a\u6232\u8a2d\u7f6e\uff0c\u81ea\u52d5\u904a\u6232\u555f\u7528\u6642\uff0c\u9ede\u64ca\u65cb\u8f49\u6309\u9215\u53ef\u505c\u6b62\u81ea\u52d5\u6a21\u5f0f",
        TEXT_SLOT_RULE_PAGE_1_4: "\u9ede\u64ca\u6975\u901f\u904a\u6232\u6309\u9215\uff0c\u6309\u9215\u4eae\u8d77\u6642\u53ef\u958b\u555f\u6975\u901f\u6a21\u5f0f\uff0c\u6975\u901f\u6a21\u5f0f\u53ef\u8207\u81ea\u52d5\u904a\u6232\u540c\u6642\u751f\u6548",
        TEXT_SLOT_RULE_PAGE_1_5: "A\uff1d\u6295\u6ce8\u7e3d\u984d\nB\uff1d\u6295\u6ce8\u7dda\u6578\uff08\u672c\u904a\u6232\u4ee550\u7dda\u8a08\u7b97\uff09\nC\uff1d\u5716\u6a19\u9023\u7dda\u8ce0\u7387\u500d\u6578\uff08\u8acb\u53c3\u8003\u8ce0\u7387\u8868\uff09\nD\uff1d\u514d\u8cbb\u904a\u6232\u500d\u7387\uff08\u514d\u8cbb\u904a\u6232\u968e\u6bb5\u624d\u8a08\u7b97\uff09",
        TEXT_SLOT_RULE_PAGE_1_6: "1\u7dda\u4e2d\u734e\u91d1\u984d\uff1d\uff08A / B\uff09x C x D",
        TEXT_SLOT_RULE_PAGE_2_1: "\u3000\u3000\u3000\u5716\u6a19\u53ef\u66ff\u4ee3\u9664\u4e86\u3000\u3000\u3000\u4ee5\u5916\u7684\u6240\u6709\u5716\u6a19",
        TEXT_SLOT_RULE_PAGE_2_2: "\u53ea\u6703\u51fa\u73fe\u5728\u7b2c2\u3001\u7b2c3\u3001\u7b2c4\u8ef8\u4e0a",
        TEXT_SLOT_RULE_PAGE_3_1: "\u3000\u3000\u3000\u50c5\u51fa\u73fe\u57282\u30013\u30014\u6efe\u8f2a\u53ef\u66ff\u4ee3\u9664\u4e86\n\u3000\u3000\u3000\u4ee5\u5916\u7684\u4efb\u610f\u5716\u6a19\u4f86\u9054\u5230\u9023\u7dda\u5206\u6578\uff01",
        TEXT_SLOT_RULE_PAGE_3_2: "\u6efe\u8f2a1\u30013\u30015\u51fa\u73fe\u3000\u3000\u3000\u53ef\u9032\u5165\u5e78\u904b\u8f2a\uff0c\u53ef\u7372\u5f97\u9ad8\u984d\u734e\u91d1\u6216\u662f\u514d\u8cbb\u904a\u623210\u6b21",
        TEXT_SLOT_RULE_PAGE_3_3: "\u514d\u8cbb\u904a\u6232\u4e2d\uff0c\u6bcf\u8f2a\u65cb\u8f49\u5c07\u6703\u96a8\u6a5f\u734e\u91d1\u500d\u6578\n\uff08\uff11\uff5e\uff18\u500d\uff09",
        TEXT_SLOT_RULE_PAGE_4_1: "\u7576\u4e3b\u76e4\u9762\u7b2c\u4e00\u8f2a\u4f48\u6eff\u3000\u3000\u3000\u3000\u3000\u3000\u3000\u3000\u6642\uff0c2~5\u8f2a\u4e2d\u5982\u51fa\u73fe\u524d\u8ff0\u76843\u7a2e\u4efb\u4e00\u5716\u793a\uff0c\u90fd\u6703\u8b8a\u63db\u70ba\u8207\u7b2c\u4e00\u8f2a\u76f8\u540c\u7684\u5716\u6a19",
        TEXT_SLOT_RULE_PAGE_4_2: "\u8b8a\u63db\u524d\u76e4\u9762",
        TEXT_SLOT_RULE_PAGE_4_3: "\u8b8a\u63db\u5f8c\u76e4\u9762",
        TEXT_SLOT_RULE_PAGE_4_4: "\u514d\u8cbb\u904a\u6232\u671f\u9593\u4e5f\u6703\u89f8\u767c\u4e3b\u904a\u6232\u7279\u8272\uff0c\u4e26\u4e14\u518d\u89f8\u767c\u6642\uff0c2~5\u8f2a\u6709\u6a5f\u7387\u6703\u6574\u8f2a\u8b8a\u63db\u70ba\u8207\u7b2c\u4e00\u8f2a\u76f8\u540c\u5716\u793a\u3002 (\u5982\u4e0a\u5716)",
        TEXT_SLOT_RULE_PAGE_4_5: "\u514d\u8cbb\u904a\u6232\u671f\u9593\u7372\u5f973\u500b\u3000\u3000\u3000\uff0c\u6703\u518d\u589e\u52a0\u514d\u8cbb\u65cb\u8f49\u6b21\u65782\u6b21",
        TEXT_SLOT_RULE_PAGE_5_1: "\u6efe\u8f2a\u4e0a\u7531\u6700\u5de6\u81f3\u53f3\u9023\u7e8c\u51fa\u73fe3\u500b\u4ee5\u4e0a\u7684\u76f8\u540c\u5716\u6a19\uff0c\u5373\u53ef\u7372\u5f97\u8a72\u5716\u6a19\u7684\u5f97\u5206\u734e\u52f5\uff01",
        TEXT_SLOT_RULE_PAGE_ERROR: "\u904a\u6232\u51fa\u73fe\u6545\u969c\u6642\uff0c\u6240\u6709\u8ce0\u4ed8\u548c\u904a\u6232\u90fd\u8996\u70ba\u7121\u6548",
        SymbolTip_Scatter: "3 \u500b\u5206\u6563\u5716\u6a19\u53ef\u89f8\u767c\u5e78\u904b\u8f2a",
        SymbolTip_Wild: "\u767e\u642d\u5716\u6a19\u53ef\u4ee3\u66ff\u9664\u5206\u6563\u5716\u6a19\u5916\u7684\u6240\u6709\u5716\u6a19",
        TEXT_SLOT_FREEGAME_WORD_1: "\u606d\u559c\u7372\u5f97",
        TEXT_SLOT_FREEGAME_WORD_2: "\u514d\u8cbb\u904a\u6232",
        TEXT_SLOT_FREEGAME_WORD_3: "<b>\u65cb\u8f49\u6b21\u6578<color=8be4a9>{0}</color>\u6b21</b>"
      },
      "en-us": {
        TEXT_SLOT_MONEY_LESS_SETTING: "The coin balance is lower than the set number. Please adjust the settings.",
        TEXT_SLOT_RULE_PAGE_1_1: "Press the button (+)(-) to change bet.",
        TEXT_SLOT_RULE_PAGE_1_2: "Press the circular arrow to submit the bet and spin the reels.",
        TEXT_SLOT_RULE_PAGE_1_3: "Press the AutoSpin button will open the AutoSpin settings. When AutoSpin is active, press the SPIN button to stop.",
        TEXT_SLOT_RULE_PAGE_1_4: "Press the Speed button will turn on the Speed Mode.Speed Mode can be used with AutoSpin.",
        TEXT_SLOT_RULE_PAGE_1_5: "A\uff1dTotal Bet\nB\uff1dNumber of lines\uff08The game is 50 \uff09\nC\uff1dSymbol odds\nD\uff1dMultiple\uff08Free Game only\uff09",
        TEXT_SLOT_RULE_PAGE_1_6: "One line win = ( A / B ) x C x D",
        TEXT_SLOT_RULE_PAGE_2_1: "\u3000\u3000\u3000substitutes for all symbols except \u3000\u3000\u3000.",
        TEXT_SLOT_RULE_PAGE_2_2: "\u3000\u3000\u3000 appears on reels 2, 3, or 4 only.",
        TEXT_SLOT_RULE_PAGE_3_1: "\u3000\u3000\u3000appears on reels 2, 3 or 4 only, substitutes for all symbols except \u3000\u3000\u3000.",
        TEXT_SLOT_RULE_PAGE_3_2: "\u3000\u3000\u3000 anywhere on 1, 3, or 5 reels trigger Lucky Wheel, chance to get the high point or 10 Free Spins.",
        TEXT_SLOT_RULE_PAGE_3_3: "At the start of each Free Spin, a multiplier is randomly selected from 1X up to 8X.",
        TEXT_SLOT_RULE_PAGE_4_1: "When reel one is all \u3000\u3000\u3000\u3000\u3000\u3000\u3000\u3000, Any of the three symbols above appear on reels 2 to 5 will become the same symbol as the first reel.",
        TEXT_SLOT_RULE_PAGE_4_2: "Before",
        TEXT_SLOT_RULE_PAGE_4_3: "After",
        TEXT_SLOT_RULE_PAGE_4_4: "The Main Game Feature is also triggered during Free Games, and 2 to 5 reels have a chance to become full reels of the same symbol.(pictured above)",
        TEXT_SLOT_RULE_PAGE_4_5: "The player can retrigger 2 free spins with 3 \u3000\u3000\u3000 anywhere on the reels during the free game.",
        TEXT_SLOT_RULE_PAGE_5_1: "The player will win the prize with any 3 or more of the same symbols appearing on the continuous reels from leftmost to the right.",
        TEXT_SLOT_RULE_PAGE_ERROR: "Malfunction Voids All Pays and Play",
        SymbolTip_Scatter: "3 SCATTER will trigger Lucky Wheel",
        SymbolTip_Wild: "WILD Substitutes for All Symbols Except SCATTER",
        TEXT_SLOT_FREEGAME_WORD_1: "Congratulations",
        TEXT_SLOT_FREEGAME_WORD_2: "You won Free Spins",
        TEXT_SLOT_FREEGAME_WORD_3: "<b><color=8be4a9>{0}</color></b>"
      },
      "en-uk": {
        TEXT_SLOT_MONEY_LESS_SETTING: "The coin balance is lower than the set number. Please adjust the settings.",
        TEXT_SLOT_RULE_PAGE_1_1: "Press the button (+)(-) to change bet.",
        TEXT_SLOT_RULE_PAGE_1_2: "Press the circular arrow to submit the bet and spin the reels.",
        TEXT_SLOT_RULE_PAGE_1_3: "Press the AutoSpin button will open the AutoSpin settings. When AutoSpin is active, press the SPIN button to stop.",
        TEXT_SLOT_RULE_PAGE_1_4: "Press the Speed button will turn on the Speed Mode.Speed Mode can be used with AutoSpin.",
        TEXT_SLOT_RULE_PAGE_1_5: "A\uff1dTotal Bet\nB\uff1dNumber of lines\uff08The game is 50 \uff09\nC\uff1dSymbol odds\nD\uff1dMultiple\uff08Free Game only\uff09",
        TEXT_SLOT_RULE_PAGE_1_6: "One line win = ( A / B ) x C x D",
        TEXT_SLOT_RULE_PAGE_2_1: "substitutes for all symbols except \u3000\u3000\u3000.",
        TEXT_SLOT_RULE_PAGE_2_2: "appears on reels 2, 3, or 4 only.",
        TEXT_SLOT_RULE_PAGE_3_1: "appears on reels 2, 3 or 4 only, substitutes for all symbols except \u3000\u3000\u3000.",
        TEXT_SLOT_RULE_PAGE_3_2: "anywhere on 1, 3, or 5 reels trigger Lucky Wheel, chance to get the high point or 10 Free Spins.",
        TEXT_SLOT_RULE_PAGE_3_3: "At the start of each Free Spin, a multiplier is randomly selected from 1X up to 8X.",
        TEXT_SLOT_RULE_PAGE_4_1: "When reel one is all \u3000\u3000\u3000\u3000\u3000\u3000\u3000\u3000, Any of the three symbols above appear on reels 2 to 5 will become the same symbol as the first reel.",
        TEXT_SLOT_RULE_PAGE_4_2: "Before",
        TEXT_SLOT_RULE_PAGE_4_3: "After",
        TEXT_SLOT_RULE_PAGE_4_4: "The Main Game Feature is also triggered during Free Games, and 2 to 5 reels have a chance to become full reels of the same symbol.(pictured above)",
        TEXT_SLOT_RULE_PAGE_4_5: "The player can retrigger 2 free spins with 3 \u3000\u3000\u3000 anywhere on the reels during the free game.",
        TEXT_SLOT_RULE_PAGE_5_1: "The player will win the prize with any 3 or more of the same symbols appearing on the continuous reels from leftmost to the right.",
        TEXT_SLOT_RULE_PAGE_ERROR: "Malfunction Voids All Pays and Play",
        SymbolTip_Scatter: "3 SCATTER will trigger Lucky Wheel",
        SymbolTip_Wild: "WILD Substitutes for All Symbols Except SCATTER",
        TEXT_SLOT_FREEGAME_WORD_1: "Congratulations",
        TEXT_SLOT_FREEGAME_WORD_2: "You won Free Spins",
        TEXT_SLOT_FREEGAME_WORD_3: "<b><color=8be4a9>{0}</color></b>"
      },
      "hi-in": {
        TEXT_SLOT_MONEY_LESS_SETTING: "\u0938\u093f\u0915\u094d\u0915\u0947 \u0915\u093e \u0938\u0902\u0924\u0941\u0932\u0928 \u0928\u093f\u0930\u094d\u0927\u093e\u0930\u093f\u0924 \u0938\u0902\u0916\u094d\u092f\u093e \u0938\u0947 \u0915\u092e \u0939\u0948\u0964 \u0915\u0943\u092a\u092f\u093e \u0938\u0947\u091f\u093f\u0902\u0917\u094d\u0938 \u0938\u092e\u093e\u092f\u094b\u091c\u093f\u0924 \u0915\u0930\u0947\u0902\u0964",
        TEXT_SLOT_RULE_PAGE_1_1: "\u0926\u093e\u0902\u0935 \u092c\u0926\u0932\u0928\u0947 \u0915\u0947 \u0932\u093f\u090f (+) (-) \u092c\u091f\u0928 \u0926\u092c\u093e\u090f\u0902",
        TEXT_SLOT_RULE_PAGE_1_2: "\u0926\u093e\u0902\u0935 \u0938\u092c\u092e\u093f\u091f \u0915\u0930\u0928\u0947 \u0915\u0947 \u0932\u093f\u090f \u0917\u094b\u0932\u093e\u0915\u093e\u0930 \u0924\u0940\u0930 \u0926\u092c\u093e\u090f\u0902 \u0914\u0930 \u0930\u0940\u0932\u094b\u0902 \u0915\u094b \u0938\u094d\u092a\u093f\u0928 \u0915\u0930\u0947\u0902\u0964",
        TEXT_SLOT_RULE_PAGE_1_3: "\u0911\u091f\u094b\u0938\u094d\u092a\u093f\u0928 \u092c\u091f\u0928 \u0926\u092c\u093e\u090f\u0902 \u0911\u091f\u094b\u0938\u094d\u092a\u093f\u0928 \u0938\u0947\u091f\u093f\u0902\u0917\u094d\u0938 \u0916\u0941\u0932 \u091c\u093e\u090f\u0917\u0940\u0964 \u091c\u092c \u0911\u091f\u094b\u0938\u094d\u092a\u093f\u0928 \u0938\u0915\u094d\u0930\u093f\u092f \u0939\u0948 \u0924\u092c \u0930\u094b\u0915\u0928\u0947 \u0915\u0947 \u0932\u093f\u090f \u0938\u094d\u092a\u093f\u0928 \u092c\u091f\u0928 \u0926\u092c\u093e\u090f\u0902\u0964",
        TEXT_SLOT_RULE_PAGE_1_4: "\u0938\u094d\u092a\u0940\u0921 \u092c\u091f\u0928 \u0926\u092c\u093e\u090f\u0902 \u0938\u094d\u092a\u0940\u0921 \u092e\u094b\u0921 \u091a\u093e\u0932\u0942 \u0939\u094b \u091c\u093e\u090f\u0917\u093e\u0964 \u0938\u094d\u092a\u0940\u0921 \u092e\u094b\u0921 \u0915\u093e \u0909\u092a\u092f\u094b\u0917 \u0911\u091f\u094b\u0938\u094d\u092a\u093f\u0928 \u0915\u0947 \u0938\u093e\u0925 \u0915\u093f\u092f\u093e \u091c\u093e \u0938\u0915\u0924\u093e \u0939\u0948\u0964",
        TEXT_SLOT_RULE_PAGE_1_5: "A\uff1d\u0915\u0941\u0932 \u092c\u0947\u091f\nB\uff1d\u0932\u093e\u0907\u0928\u094b\u0902 \u0915\u0940 \u0938\u0902\u0916\u094d\u092f\u093e\uff08\u0916\u0947\u0932 50 \u0939\u0948 \uff09\nC\uff1d\u0938\u093f\u0902\u092c\u0932 \u0911\u0921\u094d\u0938\nD\uff1d\u090f\u0915\u093e\u0927\u093f\u0915\uff08\u0915\u0947\u0935\u0932 \u0928\u093f\u0903\u0936\u0941\u0932\u094d\u0915 \u0916\u0947\u0932\uff09",
        TEXT_SLOT_RULE_PAGE_1_6: "\u090f\u0915 \u092a\u0902\u0915\u094d\u0924\u093f \u091c\u0940\u0924 = (A / B) x C x D",
        TEXT_SLOT_RULE_PAGE_2_1: "\u3000\u3000\u3000\u0915\u094b \u091b\u094b\u0921\u093c\u0915\u0930 \u0938\u092d\u0940 \u092a\u094d\u0930\u0924\u0940\u0915\u094b\u0902 \u0915\u0947 \u0932\u093f\u090f \u0935\u093f\u0915\u0932\u094d\u092a\u3000\u3000\u3000\u0964",
        TEXT_SLOT_RULE_PAGE_2_2: "\u0930\u0940\u0932\u094b\u0902 2, 3, \u092f\u093e 4 \u092a\u0930 \u0939\u0940 \u0926\u093f\u0916\u093e\u0908 \u0926\u0947\u0924\u093e \u0939\u0948\u0964",
        TEXT_SLOT_RULE_PAGE_3_1: "\u0915\u0947\u0935\u0932 2, 3 \u092f\u093e 4\u3000\u3000\u3000\u0930\u0940\u0932\u094b\u0902 \u092a\u0930 \u0926\u093f\u0916\u093e\u0908 \u0926\u0947\u0924\u093e \u0939\u0948, \u0938\u092d\u0940 \u092a\u094d\u0930\u0924\u0940\u0915\u094b\u0902 \u0915\u0947 \u0932\u093f\u090f \u0935\u093f\u0915\u0932\u094d\u092a \u091b\u094b\u0921\u093c\u0915\u0930\u3000\u3000\u3000\u0964",
        TEXT_SLOT_RULE_PAGE_3_2: "1, 3, \u092f\u093e 5 \u0930\u0940\u0932\u094b\u0902 \u092a\u0930 \u0915\u0939\u0940\u0902 \u092d\u0940\u3000\u3000\u3000\u0932\u0915\u0940 \u0935\u094d\u0939\u0940\u0932 \u0915\u094b \u091f\u094d\u0930\u093f\u0917\u0930 \u0915\u0930\u0947\u0902, \u0939\u093e\u0908 \u092a\u0949\u0907\u0902\u091f \u092f\u093e 10 \u092b\u094d\u0930\u0940 \u0938\u094d\u092a\u093f\u0928 \u092a\u094d\u0930\u093e\u092a\u094d\u0924 \u0915\u0930\u0928\u0947 \u0915\u093e \u092e\u094c\u0915\u093e\u0964",
        TEXT_SLOT_RULE_PAGE_3_3: "\u092a\u094d\u0930\u0924\u094d\u092f\u0947\u0915 \u092b\u094d\u0930\u0940 \u0938\u094d\u092a\u093f\u0928 \u0915\u0940 \u0936\u0941\u0930\u0941\u0906\u0924 \u092e\u0947\u0902, \u090f\u0915 \u092e\u0932\u094d\u091f\u0940\u092a\u0932 1X \u0938\u0947 8X \u0924\u0915 \u092c\u0947\u0924\u0930\u0924\u0940\u092c \u0930\u0942\u092a \u0938\u0947 \u091a\u0941\u0928\u093e \u091c\u093e\u0924\u093e \u0939\u0948\u0964",
        TEXT_SLOT_RULE_PAGE_4_1: "\u091c\u092c \u0930\u0940\u0932 \u0935\u0928 \u0939\u0940 \u0911\u0932 ,\n\u090a\u092a\u0930 \u0915\u0947 \u0924\u0940\u0928 \u092a\u094d\u0930\u0924\u0940\u0915\u094b\u0902 \u092e\u0947\u0902 \u0938\u0947 \u0915\u094b\u0908 \u092d\u0940  2 \u0938\u0947 5 \u0930\u0940\u0932\u094b\u0902 \u092a\u0930 \u0926\u093f\u0916\u093e\u0908 \u0926\u0947\u0924\u093e \u0939\u0948, \u0924\u094b \u0935\u0939 \u092a\u0939\u0932\u0940 \u0930\u0940\u0932 \u0915\u0947 \u0938\u092e\u093e\u0928 \u092a\u094d\u0930\u0924\u0940\u0915 \u092c\u0928 \u091c\u093e\u090f\u0917\u093e\u0964",
        TEXT_SLOT_RULE_PAGE_4_2: "\u092a\u0939\u0932\u0947",
        TEXT_SLOT_RULE_PAGE_4_3: "\u092c\u093e\u0926 \u092e\u0947\u0902",
        TEXT_SLOT_RULE_PAGE_4_4: "\u0928\u093f:\u0936\u0941\u0932\u094d\u0915 \u0917\u0947\u092e \u0915\u0947 \u0926\u094c\u0930\u093e\u0928 \u092e\u0941\u0916\u094d\u092f \u0917\u0947\u092e \u092b\u093c\u0940\u091a\u0930 \u092d\u0940 \u091f\u094d\u0930\u093f\u0917\u0930 \u0939\u094b\u0924\u093e \u0939\u0948, \u0914\u0930 2 \u0938\u0947 5 \u0930\u0940\u0932\u094b\u0902 \u0915\u0947 \u092a\u093e\u0938 \u090f\u0915 \u0939\u0940 \u092a\u094d\u0930\u0924\u0940\u0915 \u092a\u0930 \u092a\u0942\u0930\u094d\u0923 \u0930\u0940\u0932 \u092c\u0928\u0928\u0947 \u0915\u093e \u092e\u094c\u0915\u093e \u0939\u094b\u0924\u093e \u0939\u0948\u0964(\u090a\u092a\u0930 \u091a\u093f\u0924\u094d\u0930\u093f\u0924)",
        TEXT_SLOT_RULE_PAGE_4_5: "\u092b\u094d\u0930\u0940 \u0917\u0947\u092e \u0915\u0947 \u0926\u094c\u0930\u093e\u0928 \u0916\u093f\u0932\u093e\u0921\u093c\u0940 \u0930\u0940\u0932\u094b\u0902 \u092a\u0930 \u0915\u0939\u0940\u0902 \u092d\u0940 3 \u0915\u0947 \u0938\u093e\u0925 2 \u092b\u094d\u0930\u0940 \u0938\u094d\u092a\u093f\u0928 \u0915\u094b \u092b\u093f\u0930 \u0938\u0947 \u091f\u094d\u0930\u093f\u0917\u0930 \u0915\u0930 \u0938\u0915\u0924\u093e \u0939\u0948\u0964",
        TEXT_SLOT_RULE_PAGE_5_1: "\u0916\u093f\u0932\u093e\u0921\u093c\u0940 \u0915\u093f\u0938\u0940 \u092d\u0940 3 \u092f\u093e \u0905\u0927\u093f\u0915 \u0938\u092e\u093e\u0928 \u092a\u094d\u0930\u0924\u0940\u0915\u094b\u0902 \u0915\u0947 \u0938\u093e\u0925 \u092a\u0941\u0930\u0938\u094d\u0915\u093e\u0930 \u091c\u0940\u0924\u0947\u0917\u093e \u091c\u094b \u0932\u0917\u093e\u0924\u093e\u0930 \u0930\u0940\u0932\u094b\u0902 \u092a\u0930 \u092c\u093e\u0908\u0902 \u0913\u0930 \u0938\u0947 \u0926\u093e\u0908\u0902 \u0913\u0930 \u0926\u093f\u0916\u093e\u0908 \u0926\u0947\u0917\u093e\u0964",
        TEXT_SLOT_RULE_PAGE_ERROR: "\u0916\u0930\u093e\u092c\u0940 \u0938\u092d\u0940 \u092d\u0941\u0917\u0924\u093e\u0928 \u0915\u0930\u0924\u093e \u0939\u0948 \u0914\u0930 \u0916\u0947\u0932\u0924\u093e \u0939\u0948",
        SymbolTip_Scatter: "3 \u0938\u094d\u0915\u0948\u091f\u0930 \u0932\u0915\u0940 \u0935\u094d\u0939\u0940\u0932 \u0915\u094b \u091f\u094d\u0930\u093f\u0917\u0930 \u0915\u0930\u0947\u0917\u093e",
        SymbolTip_Wild: "\u0938\u094d\u0915\u0948\u091f\u0930 \u0915\u094b \u091b\u094b\u0921\u093c\u0915\u0930 \u0938\u092d\u0940 \u091a\u093f\u0939\u094d\u0928\u094b\u0902 \u0915\u0947 \u0932\u093f\u090f \u091c\u0902\u0917\u0932\u0940 \u0938\u094d\u0925\u093e\u0928\u093e\u092a\u0928\u094d\u0928",
        TEXT_SLOT_FREEGAME_WORD_1: "\u092c\u0927\u093e\u0908 \u0939\u094b",
        TEXT_SLOT_FREEGAME_WORD_2: "\u0906\u092a\u0928\u0947 \u0928\u093f\u0903\u0936\u0941\u0932\u094d\u0915 \u0938\u094d\u092a\u093f\u0928 \u091c\u0940\u0924\u093e",
        TEXT_SLOT_FREEGAME_WORD_3: "<b><color=8be4a9>{0}</color></b>"
      }
    };
    cc._RF.pop();
  }, {} ],
  G27_Line: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "c9289ETlEhMj4P0QektP3Sj", "G27_Line");
    "use strict";
    var __extends = this && this.__extends || function() {
      var extendStatics = function(d, b) {
        extendStatics = Object.setPrototypeOf || {
          __proto__: []
        } instanceof Array && function(d, b) {
          d.__proto__ = b;
        } || function(d, b) {
          for (var p in b) Object.prototype.hasOwnProperty.call(b, p) && (d[p] = b[p]);
        };
        return extendStatics(d, b);
      };
      return function(d, b) {
        extendStatics(d, b);
        function __() {
          this.constructor = d;
        }
        d.prototype = null === b ? Object.create(b) : (__.prototype = b.prototype, new __());
      };
    }();
    var __decorate = this && this.__decorate || function(decorators, target, key, desc) {
      var c = arguments.length, r = c < 3 ? target : null === desc ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
      if ("object" === typeof Reflect && "function" === typeof Reflect.decorate) r = Reflect.decorate(decorators, target, key, desc); else for (var i = decorators.length - 1; i >= 0; i--) (d = decorators[i]) && (r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r);
      return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    var Slot_Line_1 = require("../../../SlotFramework/Game/view/Slot_Line");
    var ccclass = cc._decorator.ccclass;
    var G27_Line = function(_super) {
      __extends(G27_Line, _super);
      function G27_Line() {
        return null !== _super && _super.apply(this, arguments) || this;
      }
      G27_Line.prototype.setScore = function(p_score) {
        var _bone = this.lineSpine.findBone("controlBone4");
        var _y = 0;
        this.scoreNode.active = "" != p_score;
        _y = _bone.y > 100 ? 100 : _bone.y > -83 ? -83 : -230;
        this.scoreNode.setPosition(new cc.Vec2(_bone.x, _y));
        this.scoreLabel.string = Number(p_score).toFixed(2);
        this.scoreLabel.node.scale = 0;
        cc.tween(this.scoreLabel.node).to(.38, {
          scale: 1
        }, {
          easing: "elasticOut"
        }).start();
      };
      G27_Line = __decorate([ ccclass ], G27_Line);
      return G27_Line;
    }(Slot_Line_1.default);
    exports.default = G27_Line;
    cc._RF.pop();
  }, {
    "../../../SlotFramework/Game/view/Slot_Line": void 0
  } ],
  G27_LoadingItem: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "f4b69VTX0pBWJwqPWUiQKvP", "G27_LoadingItem");
    "use strict";
    var __extends = this && this.__extends || function() {
      var extendStatics = function(d, b) {
        extendStatics = Object.setPrototypeOf || {
          __proto__: []
        } instanceof Array && function(d, b) {
          d.__proto__ = b;
        } || function(d, b) {
          for (var p in b) Object.prototype.hasOwnProperty.call(b, p) && (d[p] = b[p]);
        };
        return extendStatics(d, b);
      };
      return function(d, b) {
        extendStatics(d, b);
        function __() {
          this.constructor = d;
        }
        d.prototype = null === b ? Object.create(b) : (__.prototype = b.prototype, new __());
      };
    }();
    var __decorate = this && this.__decorate || function(decorators, target, key, desc) {
      var c = arguments.length, r = c < 3 ? target : null === desc ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
      if ("object" === typeof Reflect && "function" === typeof Reflect.decorate) r = Reflect.decorate(decorators, target, key, desc); else for (var i = decorators.length - 1; i >= 0; i--) (d = decorators[i]) && (r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r);
      return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    var AudioManager_1 = require("../../Common/Tools/AudioManager/AudioManager");
    var Localize_1 = require("../../Common/Tools/Localization/Localize");
    var Slot_LoadingItem_1 = require("../../SlotFramework/Common/Slot_LoadingItem");
    var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
    var G27_LoadingItem = function(_super) {
      __extends(G27_LoadingItem, _super);
      function G27_LoadingItem() {
        var _this = null !== _super && _super.apply(this, arguments) || this;
        _this.count = 3;
        _this.isFirst = true;
        _this.canvasPos = null;
        _this.canvasSize = null;
        _this.loadingBG = null;
        _this.loading = null;
        _this.loadingWord = null;
        _this.particle = null;
        _this.gameNode = null;
        return _this;
      }
      G27_LoadingItem.prototype.onLoad = function() {
        this.particle.active = false;
      };
      G27_LoadingItem.prototype.update = function() {
        if ("G27_Lobby" !== cc.director.getScene().name || this.isFirst) {
          if ("G27_Game" === cc.director.getScene().name && this.isFirst) {
            this.isFirst = false;
            this.canvasPos = cc.director.getScene().getChildByName("Canvas").position;
            this.canvasSize = cc.director.getScene().getChildByName("Canvas").getContentSize();
            cc.director.getScene().getChildByName("Canvas").parent = this.gameNode;
          }
        } else {
          this.isFirst = true;
          this.count = 3;
        }
      };
      G27_LoadingItem.prototype.hide = function() {
        var _this = this;
        AudioManager_1.AudioManager.instance.playAudioEvent("LoadingComplete");
        this.loadingBar.node.active = false;
        this.loadingBG.setCompleteListener(function() {
          _this.checkEnd();
        });
        this.loading.setCompleteListener(function() {
          _this.checkEnd();
        });
        this.loadingWord.setCompleteListener(function() {
          _this.checkEnd();
        });
        this.particle.active = true;
        this.loading.setAnimation(0, "TurnMG_BG", false);
        this.loadingWord.getComponent(Localize_1.default).playI18NSpine(0, "TurnMG_Word", false);
        this.loadingBG.setAnimation(0, "TurnMG_BG", false);
        cc.systemEvent.emit("pushGameIcon");
        cc.systemEvent.emit("fadeInCanvas");
      };
      G27_LoadingItem.prototype.checkEnd = function() {
        var _this = this;
        if (0 === --this.count) {
          this.gameNode.children[0].parent = cc.director.getScene();
          cc.director.getScene().getChildByName("Canvas").position = this.canvasPos;
          cc.director.getScene().getChildByName("Canvas").setContentSize(this.canvasSize);
          this.particle.active = false;
          this.scheduleOnce(function() {
            cc.systemEvent.emit("LoadingAniEnd");
            _this.node.destroy();
          });
        }
      };
      __decorate([ property(sp.Skeleton) ], G27_LoadingItem.prototype, "loadingBG", void 0);
      __decorate([ property(sp.Skeleton) ], G27_LoadingItem.prototype, "loading", void 0);
      __decorate([ property(sp.Skeleton) ], G27_LoadingItem.prototype, "loadingWord", void 0);
      __decorate([ property(cc.Node) ], G27_LoadingItem.prototype, "particle", void 0);
      __decorate([ property(cc.Node) ], G27_LoadingItem.prototype, "gameNode", void 0);
      G27_LoadingItem = __decorate([ ccclass ], G27_LoadingItem);
      return G27_LoadingItem;
    }(Slot_LoadingItem_1.default);
    exports.default = G27_LoadingItem;
    cc._RF.pop();
  }, {
    "../../Common/Tools/AudioManager/AudioManager": void 0,
    "../../Common/Tools/Localization/Localize": void 0,
    "../../SlotFramework/Common/Slot_LoadingItem": void 0
  } ],
  G27_LoadingUI: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "33fd8XkJoBDMKJOzqCnMxz2", "G27_LoadingUI");
    "use strict";
    var __extends = this && this.__extends || function() {
      var extendStatics = function(d, b) {
        extendStatics = Object.setPrototypeOf || {
          __proto__: []
        } instanceof Array && function(d, b) {
          d.__proto__ = b;
        } || function(d, b) {
          for (var p in b) Object.prototype.hasOwnProperty.call(b, p) && (d[p] = b[p]);
        };
        return extendStatics(d, b);
      };
      return function(d, b) {
        extendStatics(d, b);
        function __() {
          this.constructor = d;
        }
        d.prototype = null === b ? Object.create(b) : (__.prototype = b.prototype, new __());
      };
    }();
    var __decorate = this && this.__decorate || function(decorators, target, key, desc) {
      var c = arguments.length, r = c < 3 ? target : null === desc ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
      if ("object" === typeof Reflect && "function" === typeof Reflect.decorate) r = Reflect.decorate(decorators, target, key, desc); else for (var i = decorators.length - 1; i >= 0; i--) (d = decorators[i]) && (r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r);
      return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    var ccclass = cc._decorator.ccclass;
    var G27_LoadingUI = function(_super) {
      __extends(G27_LoadingUI, _super);
      function G27_LoadingUI() {
        return null !== _super && _super.apply(this, arguments) || this;
      }
      G27_LoadingUI = __decorate([ ccclass ], G27_LoadingUI);
      return G27_LoadingUI;
    }(cc.Component);
    exports.default = G27_LoadingUI;
    cc._RF.pop();
  }, {} ],
  G27_Loading_InitData: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "36eadA1Jz5OwJJWlbWxfuPZ", "G27_Loading_InitData");
    "use strict";
    var __decorate = this && this.__decorate || function(decorators, target, key, desc) {
      var c = arguments.length, r = c < 3 ? target : null === desc ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
      if ("object" === typeof Reflect && "function" === typeof Reflect.decorate) r = Reflect.decorate(decorators, target, key, desc); else for (var i = decorators.length - 1; i >= 0; i--) (d = decorators[i]) && (r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r);
      return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    var ccclass = cc._decorator.ccclass;
    var G27_Loading_InitData = function() {
      function G27_Loading_InitData() {}
      G27_Loading_InitData = __decorate([ ccclass ], G27_Loading_InitData);
      return G27_Loading_InitData;
    }();
    exports.default = G27_Loading_InitData;
    cc._RF.pop();
  }, {} ],
  G27_Loading: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "f11dad7H6tAFLbEybelHPa5", "G27_Loading");
    "use strict";
    var __extends = this && this.__extends || function() {
      var extendStatics = function(d, b) {
        extendStatics = Object.setPrototypeOf || {
          __proto__: []
        } instanceof Array && function(d, b) {
          d.__proto__ = b;
        } || function(d, b) {
          for (var p in b) Object.prototype.hasOwnProperty.call(b, p) && (d[p] = b[p]);
        };
        return extendStatics(d, b);
      };
      return function(d, b) {
        extendStatics(d, b);
        function __() {
          this.constructor = d;
        }
        d.prototype = null === b ? Object.create(b) : (__.prototype = b.prototype, new __());
      };
    }();
    var __decorate = this && this.__decorate || function(decorators, target, key, desc) {
      var c = arguments.length, r = c < 3 ? target : null === desc ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
      if ("object" === typeof Reflect && "function" === typeof Reflect.decorate) r = Reflect.decorate(decorators, target, key, desc); else for (var i = decorators.length - 1; i >= 0; i--) (d = decorators[i]) && (r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r);
      return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __awaiter = this && this.__awaiter || function(thisArg, _arguments, P, generator) {
      function adopt(value) {
        return value instanceof P ? value : new P(function(resolve) {
          resolve(value);
        });
      }
      return new (P || (P = Promise))(function(resolve, reject) {
        function fulfilled(value) {
          try {
            step(generator.next(value));
          } catch (e) {
            reject(e);
          }
        }
        function rejected(value) {
          try {
            step(generator["throw"](value));
          } catch (e) {
            reject(e);
          }
        }
        function step(result) {
          result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected);
        }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
      });
    };
    var __generator = this && this.__generator || function(thisArg, body) {
      var _ = {
        label: 0,
        sent: function() {
          if (1 & t[0]) throw t[1];
          return t[1];
        },
        trys: [],
        ops: []
      }, f, y, t, g;
      return g = {
        next: verb(0),
        throw: verb(1),
        return: verb(2)
      }, "function" === typeof Symbol && (g[Symbol.iterator] = function() {
        return this;
      }), g;
      function verb(n) {
        return function(v) {
          return step([ n, v ]);
        };
      }
      function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
          if (f = 1, y && (t = 2 & op[0] ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 
          0) : y.next) && !(t = t.call(y, op[1])).done) return t;
          (y = 0, t) && (op = [ 2 & op[0], t.value ]);
          switch (op[0]) {
           case 0:
           case 1:
            t = op;
            break;

           case 4:
            _.label++;
            return {
              value: op[1],
              done: false
            };

           case 5:
            _.label++;
            y = op[1];
            op = [ 0 ];
            continue;

           case 7:
            op = _.ops.pop();
            _.trys.pop();
            continue;

           default:
            if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (6 === op[0] || 2 === op[0])) {
              _ = 0;
              continue;
            }
            if (3 === op[0] && (!t || op[1] > t[0] && op[1] < t[3])) {
              _.label = op[1];
              break;
            }
            if (6 === op[0] && _.label < t[1]) {
              _.label = t[1];
              t = op;
              break;
            }
            if (t && _.label < t[2]) {
              _.label = t[2];
              _.ops.push(op);
              break;
            }
            t[2] && _.ops.pop();
            _.trys.pop();
            continue;
          }
          op = body.call(thisArg, _);
        } catch (e) {
          op = [ 6, e ];
          y = 0;
        } finally {
          f = t = 0;
        }
        if (5 & op[0]) throw op[1];
        return {
          value: op[0] ? op[1] : void 0,
          done: true
        };
      }
    };
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    var AudioManager_1 = require("../../Common/Tools/AudioManager/AudioManager");
    var DesktopBrowserTransform_1 = require("../../Common/Tools/DesktopBrowserTransform/DesktopBrowserTransform");
    var Slot_LoadingManager_1 = require("../../SlotFramework/Loading/Slot_LoadingManager");
    var G27_DataManager_1 = require("../Common/G27_DataManager");
    var G27_DynamicPopUpPanelManager_1 = require("../Common/G27_DynamicPopUpPanelManager");
    var G27_SocketManager_1 = require("../Common/Socket/G27_SocketManager");
    var G27_Language_1 = require("../Localization/G27_Language");
    var ccclass = cc._decorator.ccclass;
    var PRELOAD_RESOURCE_LIST = [ "Resources" ];
    var PRELOAD_SCENE_LIST = [ "G27_Lobby", "G27_Game" ];
    var preloadResources = [ {
      bundle: "G27",
      scene: {
        path: PRELOAD_SCENE_LIST
      },
      resource: {
        path: PRELOAD_RESOURCE_LIST
      },
      audio: {
        csvPath: "Resources/Audio - G27",
        path: [ "Resources/music", "Resources/sound" ]
      },
      loadRate: .97
    } ];
    var G27_Loading = function(_super) {
      __extends(G27_Loading, _super);
      function G27_Loading() {
        var _this = null !== _super && _super.apply(this, arguments) || this;
        _this.languageMap = G27_Language_1.G27_Language;
        return _this;
      }
      G27_Loading.prototype.onLoad = function() {
        DesktopBrowserTransform_1.default.needFixScreen = true;
        _super.prototype.onLoad.call(this);
        cc.game.setFrameRate(60);
      };
      G27_Loading.prototype.start = function() {
        _super.prototype.start.call(this);
        this.processAsync().catch(function(err) {
          return console.error(err);
        });
      };
      G27_Loading.prototype.processAsync = function() {
        return __awaiter(this, void 0, Promise, function() {
          return __generator(this, function(_a) {
            switch (_a.label) {
             case 0:
              AudioManager_1.AudioManager.delInstance();
              G27_DataManager_1.default.deleteInstance();
              return [ 4, G27_SocketManager_1.default.getInstance().connect() ];

             case 1:
              _a.sent();
              return [ 4, this.initGameConfig(G27_DataManager_1.default.getInstance()) ];

             case 2:
              _a.sent();
              return [ 4, _super.prototype.processAsync.call(this, preloadResources) ];

             case 3:
              _a.sent();
              this.processLogin(G27_SocketManager_1.default.getInstance());
              return [ 2 ];
            }
          });
        });
      };
      G27_Loading.prototype.onAssetLoadComplete = function(prefabs) {
        G27_DynamicPopUpPanelManager_1.default.getInstance().prestore(prefabs);
      };
      G27_Loading = __decorate([ ccclass ], G27_Loading);
      return G27_Loading;
    }(Slot_LoadingManager_1.default);
    exports.default = G27_Loading;
    cc._RF.pop();
  }, {
    "../../Common/Tools/AudioManager/AudioManager": void 0,
    "../../Common/Tools/DesktopBrowserTransform/DesktopBrowserTransform": void 0,
    "../../SlotFramework/Loading/Slot_LoadingManager": void 0,
    "../Common/G27_DataManager": "G27_DataManager",
    "../Common/G27_DynamicPopUpPanelManager": "G27_DynamicPopUpPanelManager",
    "../Common/Socket/G27_SocketManager": "G27_SocketManager",
    "../Localization/G27_Language": "G27_Language"
  } ],
  G27_LobbyUI: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "07c7dkS32BIP7krT0yO+trY", "G27_LobbyUI");
    "use strict";
    var __extends = this && this.__extends || function() {
      var extendStatics = function(d, b) {
        extendStatics = Object.setPrototypeOf || {
          __proto__: []
        } instanceof Array && function(d, b) {
          d.__proto__ = b;
        } || function(d, b) {
          for (var p in b) Object.prototype.hasOwnProperty.call(b, p) && (d[p] = b[p]);
        };
        return extendStatics(d, b);
      };
      return function(d, b) {
        extendStatics(d, b);
        function __() {
          this.constructor = d;
        }
        d.prototype = null === b ? Object.create(b) : (__.prototype = b.prototype, new __());
      };
    }();
    var __decorate = this && this.__decorate || function(decorators, target, key, desc) {
      var c = arguments.length, r = c < 3 ? target : null === desc ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
      if ("object" === typeof Reflect && "function" === typeof Reflect.decorate) r = Reflect.decorate(decorators, target, key, desc); else for (var i = decorators.length - 1; i >= 0; i--) (d = decorators[i]) && (r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r);
      return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    var Slot_LobbyUI_1 = require("../../../SlotFramework/Lobby/Slot_LobbyUI");
    var ccclass = cc._decorator.ccclass;
    var G27_LobbyUI = function(_super) {
      __extends(G27_LobbyUI, _super);
      function G27_LobbyUI() {
        return null !== _super && _super.apply(this, arguments) || this;
      }
      G27_LobbyUI.prototype.init = function(p_data) {
        _super.prototype.init.call(this, p_data);
      };
      G27_LobbyUI.prototype.setPlayerIcon = function() {};
      G27_LobbyUI = __decorate([ ccclass ], G27_LobbyUI);
      return G27_LobbyUI;
    }(Slot_LobbyUI_1.default);
    exports.default = G27_LobbyUI;
    cc._RF.pop();
  }, {
    "../../../SlotFramework/Lobby/Slot_LobbyUI": void 0
  } ],
  G27_Lobby: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "90146J63p9DP7l9nWtSIhJS", "G27_Lobby");
    "use strict";
    var __extends = this && this.__extends || function() {
      var extendStatics = function(d, b) {
        extendStatics = Object.setPrototypeOf || {
          __proto__: []
        } instanceof Array && function(d, b) {
          d.__proto__ = b;
        } || function(d, b) {
          for (var p in b) Object.prototype.hasOwnProperty.call(b, p) && (d[p] = b[p]);
        };
        return extendStatics(d, b);
      };
      return function(d, b) {
        extendStatics(d, b);
        function __() {
          this.constructor = d;
        }
        d.prototype = null === b ? Object.create(b) : (__.prototype = b.prototype, new __());
      };
    }();
    var __decorate = this && this.__decorate || function(decorators, target, key, desc) {
      var c = arguments.length, r = c < 3 ? target : null === desc ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
      if ("object" === typeof Reflect && "function" === typeof Reflect.decorate) r = Reflect.decorate(decorators, target, key, desc); else for (var i = decorators.length - 1; i >= 0; i--) (d = decorators[i]) && (r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r);
      return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    var LocalizationManager_1 = require("../../Common/Tools/Localization/LocalizationManager");
    var Slot_LobbyManager_InitData_1 = require("../../SlotFramework/Lobby/Data/Slot_LobbyManager_InitData");
    var Slot_LobbyUI_InitData_1 = require("../../SlotFramework/Lobby/Data/Slot_LobbyUI_InitData");
    var Slot_LobbyManager_1 = require("../../SlotFramework/Lobby/Slot_LobbyManager");
    var G27_DataManager_1 = require("../Common/G27_DataManager");
    var G27_SocketManager_1 = require("../Common/Socket/G27_SocketManager");
    var G27_LobbyUI_1 = require("./View/G27_LobbyUI");
    var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
    var G27_Lobby = function(_super) {
      __extends(G27_Lobby, _super);
      function G27_Lobby() {
        var _this = null !== _super && _super.apply(this, arguments) || this;
        _this.ui = null;
        _this.data = null;
        _this.socket = null;
        return _this;
      }
      G27_Lobby.prototype.start = function() {
        _super.prototype.start.call(this);
        this.socket = G27_SocketManager_1.default.getInstance();
        var _data = new Slot_LobbyManager_InitData_1.default();
        _data.bundleName = "G" + G27_DataManager_1.default.getInstance().gameID;
        _data.bundlePath = "" + G27_DataManager_1.default.getInstance().path;
        this.init(_data);
        this.onFastJoinGame();
      };
      G27_Lobby.prototype.onDestroy = function() {
        this.removeEvents();
      };
      G27_Lobby.prototype.init = function(p_data) {
        this.data = G27_DataManager_1.default.getInstance();
        this.data.init();
        this.registerEvents();
        _super.prototype.init.call(this, p_data);
        var _data = new Slot_LobbyUI_InitData_1.default();
        _data.bundleName = p_data.bundleName;
        _data.bundlePath = p_data.bundlePath;
        this.ui.init(_data);
      };
      G27_Lobby.prototype.registerEvents = function() {
        var _this = this;
        G27_SocketManager_1.default.getInstance().setSelfEnterEventCallback(function(data) {
          _this.onSelfEnterEvent(data);
        });
      };
      G27_Lobby.prototype.removeEvents = function() {
        G27_SocketManager_1.default.getInstance().setSelfEnterEventCallback(null);
      };
      G27_Lobby.prototype.onSelfEnterEvent = function(data) {
        G27_DataManager_1.default.getInstance().coin = G27_DataManager_1.default.getInstance().getClientMoney(data.Coin);
      };
      G27_Lobby.prototype.onBackButton = function() {
        console.log("\u96e2\u958b");
      };
      G27_Lobby.prototype.onOpenHistoryButton = function() {
        console.log("\u958b\u555f\u6b77\u53f2\u7d00\u9304");
      };
      G27_Lobby.prototype.onOpenMenuButton = function() {
        this.ui.openMenu();
      };
      G27_Lobby.prototype.onCloseMenuButton = function() {
        this.ui.closeMenu();
      };
      G27_Lobby.prototype.onChangeMachine = function() {
        console.log("\u63db\u4e00\u6279");
      };
      G27_Lobby.prototype.onFastJoinGame = function() {
        var _this = this;
        console.log("\u5feb\u901f\u52a0\u5165");
        this.socket.setOnFastChoseRoomEvent().then(function() {
          cc.director.loadScene("G27_Game");
        }).catch(function(error) {
          error.code == GameClient.errCode.RESPONSE_TIMED_OUT && _this.socket.showErrorAlert(LocalizationManager_1.default.getInstance().get("ERROR_SOCKET_TIME_OUT"), null, function() {
            G27_SocketManager_1.default.getInstance().login();
          });
        });
      };
      G27_Lobby.prototype.onChangePlayerHead = function() {
        console.log("\u63db\u73a9\u5bb6\u982d\u50cf");
      };
      G27_Lobby.prototype.setPlayerInfo = function() {
        var _money = this.data.coin;
        this.ui.setPlayerInfo(this.data.nickname, this.data.avatarID, _money);
      };
      G27_Lobby.prototype.setRoomData = function() {
        console.log("\u8a2d\u5b9a\u623f\u9593\u8cc7\u6599");
      };
      __decorate([ property(G27_LobbyUI_1.default) ], G27_Lobby.prototype, "ui", void 0);
      G27_Lobby = __decorate([ ccclass ], G27_Lobby);
      return G27_Lobby;
    }(Slot_LobbyManager_1.default);
    exports.default = G27_Lobby;
    cc._RF.pop();
  }, {
    "../../Common/Tools/Localization/LocalizationManager": void 0,
    "../../SlotFramework/Lobby/Data/Slot_LobbyManager_InitData": void 0,
    "../../SlotFramework/Lobby/Data/Slot_LobbyUI_InitData": void 0,
    "../../SlotFramework/Lobby/Slot_LobbyManager": void 0,
    "../Common/G27_DataManager": "G27_DataManager",
    "../Common/Socket/G27_SocketManager": "G27_SocketManager",
    "./View/G27_LobbyUI": "G27_LobbyUI"
  } ],
  G27_MusicOptionPanel: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "71969jt3WVAMpxgy7tYQPtR", "G27_MusicOptionPanel");
    "use strict";
    var __extends = this && this.__extends || function() {
      var extendStatics = function(d, b) {
        extendStatics = Object.setPrototypeOf || {
          __proto__: []
        } instanceof Array && function(d, b) {
          d.__proto__ = b;
        } || function(d, b) {
          for (var p in b) Object.prototype.hasOwnProperty.call(b, p) && (d[p] = b[p]);
        };
        return extendStatics(d, b);
      };
      return function(d, b) {
        extendStatics(d, b);
        function __() {
          this.constructor = d;
        }
        d.prototype = null === b ? Object.create(b) : (__.prototype = b.prototype, new __());
      };
    }();
    var __decorate = this && this.__decorate || function(decorators, target, key, desc) {
      var c = arguments.length, r = c < 3 ? target : null === desc ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
      if ("object" === typeof Reflect && "function" === typeof Reflect.decorate) r = Reflect.decorate(decorators, target, key, desc); else for (var i = decorators.length - 1; i >= 0; i--) (d = decorators[i]) && (r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r);
      return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    var Slot_MusicOptionPanel_1 = require("../../../SlotFramework/Game/Panel/Slot_MusicOptionPanel");
    var ccclass = cc._decorator.ccclass;
    var G27_MusicOptionPanel = function(_super) {
      __extends(G27_MusicOptionPanel, _super);
      function G27_MusicOptionPanel() {
        return null !== _super && _super.apply(this, arguments) || this;
      }
      G27_MusicOptionPanel = __decorate([ ccclass ], G27_MusicOptionPanel);
      return G27_MusicOptionPanel;
    }(Slot_MusicOptionPanel_1.default);
    exports.default = G27_MusicOptionPanel;
    cc._RF.pop();
  }, {
    "../../../SlotFramework/Game/Panel/Slot_MusicOptionPanel": void 0
  } ],
  G27_Reel: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "a4a85nu7MdKmJhIYAjs+0Vu", "G27_Reel");
    "use strict";
    var __extends = this && this.__extends || function() {
      var extendStatics = function(d, b) {
        extendStatics = Object.setPrototypeOf || {
          __proto__: []
        } instanceof Array && function(d, b) {
          d.__proto__ = b;
        } || function(d, b) {
          for (var p in b) Object.prototype.hasOwnProperty.call(b, p) && (d[p] = b[p]);
        };
        return extendStatics(d, b);
      };
      return function(d, b) {
        extendStatics(d, b);
        function __() {
          this.constructor = d;
        }
        d.prototype = null === b ? Object.create(b) : (__.prototype = b.prototype, new __());
      };
    }();
    var __decorate = this && this.__decorate || function(decorators, target, key, desc) {
      var c = arguments.length, r = c < 3 ? target : null === desc ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
      if ("object" === typeof Reflect && "function" === typeof Reflect.decorate) r = Reflect.decorate(decorators, target, key, desc); else for (var i = decorators.length - 1; i >= 0; i--) (d = decorators[i]) && (r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r);
      return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    var Slot_Reel_1 = require("../../../SlotFramework/Game/view/Slot_Reel");
    var Slot_DataManager_1 = require("../../../SlotFramework/Slot_DataManager");
    var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
    var G27_Reel = function(_super) {
      __extends(G27_Reel, _super);
      function G27_Reel() {
        var _this = null !== _super && _super.apply(this, arguments) || this;
        _this.boardSmoke3 = null;
        _this.boardSmoke4 = null;
        _this.symbolsList = [];
        return _this;
      }
      G27_Reel.prototype.preChangeSymbolEffect = function(p_changePos) {
        var _targetSymbol = 0;
        0 == p_changePos && (_targetSymbol = 2);
        1 == p_changePos && (_targetSymbol = 3);
        2 == p_changePos && (_targetSymbol = 4);
        3 == p_changePos && (_targetSymbol = 5);
        for (var i = 0; i < this.symbolsList.length; i++) this.symbolsList[i].getOrder() == _targetSymbol && this.symbolsList[i].preChangeSymbolEffect();
      };
      G27_Reel.prototype.changeSymbolEffect = function(p_changePos, p_targetSymbol, isAni) {
        void 0 === isAni && (isAni = true);
        var _targetSymbol = 0;
        0 == p_changePos && (_targetSymbol = 2);
        1 == p_changePos && (_targetSymbol = 3);
        2 == p_changePos && (_targetSymbol = 4);
        3 == p_changePos && (_targetSymbol = 5);
        for (var i = 0; i < this.symbolsList.length; i++) this.symbolsList[i].getOrder() == _targetSymbol && (isAni ? this.symbolsList[i].changeSymbolEffect(p_targetSymbol) : this.symbolsList[i].changeSymbol(p_targetSymbol));
        isAni || this.allSymbolPlayIdleEffect(true);
      };
      G27_Reel.prototype.showBoardSmoke = function(p_isFour) {
        if (p_isFour) {
          this.boardSmoke4.position = this.node.position;
          this.boardSmoke4.active = true;
          this.boardSmoke4.getComponentInChildren(cc.ParticleSystem3D).stop();
          this.boardSmoke4.getComponentInChildren(cc.ParticleSystem3D).play();
        } else {
          this.boardSmoke3.position = this.node.position;
          this.boardSmoke3.active = true;
          this.boardSmoke3.getComponentInChildren(cc.ParticleSystem3D).stop();
          this.boardSmoke3.getComponentInChildren(cc.ParticleSystem3D).play();
        }
      };
      G27_Reel.prototype.stopChangeSymbol = function() {
        this.boardSmoke3.active = false;
        this.boardSmoke4.active = false;
      };
      G27_Reel.prototype.hasWild = function() {
        var _top = this.getSymbolPosition(this.symbolsList.length - this.displaySymbolCount - 1).y;
        for (var i = 0; i < this.symbolsList.length; i++) if (this.symbolsList[i].node.y < _top && this.symbolsList[i].getSymbolID() == Slot_DataManager_1.SYMBOL_NAME.Wild) return true;
        return false;
      };
      G27_Reel.prototype.hasScatter = function() {
        var _top = this.getSymbolPosition(this.symbolsList.length - this.displaySymbolCount - 1).y;
        for (var i = 0; i < this.symbolsList.length; i++) if (this.symbolsList[i].node.y < _top && this.symbolsList[i].getSymbolID() == Slot_DataManager_1.SYMBOL_NAME.Scatter) return true;
        return false;
      };
      G27_Reel.prototype.getSymbolReWardPos = function(p_winPosition) {
        var list = [];
        for (var i = 0; i < p_winPosition.length; i++) if (1 == p_winPosition[i]) for (var j = 0; j < this.symbolsList.length; j++) this.symbolsList[j].getOrder() - 2 == i && list.push(this.symbolsList[j].node.position);
        return list;
      };
      G27_Reel.disappearEffectDelayCount = 0;
      __decorate([ property(cc.Node) ], G27_Reel.prototype, "boardSmoke3", void 0);
      __decorate([ property(cc.Node) ], G27_Reel.prototype, "boardSmoke4", void 0);
      G27_Reel = __decorate([ ccclass ], G27_Reel);
      return G27_Reel;
    }(Slot_Reel_1.default);
    exports.default = G27_Reel;
    cc._RF.pop();
  }, {
    "../../../SlotFramework/Game/view/Slot_Reel": void 0,
    "../../../SlotFramework/Slot_DataManager": void 0
  } ],
  G27_RulePanel: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "22e81oW6ZVPa4WO730hKLGX", "G27_RulePanel");
    "use strict";
    var __extends = this && this.__extends || function() {
      var extendStatics = function(d, b) {
        extendStatics = Object.setPrototypeOf || {
          __proto__: []
        } instanceof Array && function(d, b) {
          d.__proto__ = b;
        } || function(d, b) {
          for (var p in b) Object.prototype.hasOwnProperty.call(b, p) && (d[p] = b[p]);
        };
        return extendStatics(d, b);
      };
      return function(d, b) {
        extendStatics(d, b);
        function __() {
          this.constructor = d;
        }
        d.prototype = null === b ? Object.create(b) : (__.prototype = b.prototype, new __());
      };
    }();
    var __decorate = this && this.__decorate || function(decorators, target, key, desc) {
      var c = arguments.length, r = c < 3 ? target : null === desc ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
      if ("object" === typeof Reflect && "function" === typeof Reflect.decorate) r = Reflect.decorate(decorators, target, key, desc); else for (var i = decorators.length - 1; i >= 0; i--) (d = decorators[i]) && (r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r);
      return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    var Slot_RulePanel_1 = require("../../../SlotFramework/Game/Panel/Slot_RulePanel");
    var AudioManager_1 = require("../../../Common/Tools/AudioManager/AudioManager");
    var G27_DataManager_1 = require("../../Common/G27_DataManager");
    var ccclass = cc._decorator.ccclass;
    var G27_RulePanel = function(_super) {
      __extends(G27_RulePanel, _super);
      function G27_RulePanel() {
        var _this = null !== _super && _super.apply(this, arguments) || this;
        _this.data = null;
        return _this;
      }
      G27_RulePanel.prototype.init = function() {
        this.data = G27_DataManager_1.default.getInstance();
        _super.prototype.init.call(this);
      };
      G27_RulePanel.prototype.onNextRulePage = function() {
        if (this.status == Slot_RulePanel_1.STATUS.CHANGE_PAGE) return;
        AudioManager_1.AudioManager.instance.playAudioEvent("NextRulePage");
        _super.prototype.onNextRulePage.call(this);
      };
      G27_RulePanel.prototype.onPreRulePage = function() {
        if (this.status == Slot_RulePanel_1.STATUS.CHANGE_PAGE) return;
        AudioManager_1.AudioManager.instance.playAudioEvent("PreRulePage");
        _super.prototype.onPreRulePage.call(this);
      };
      G27_RulePanel.prototype.open = function() {
        _super.prototype.open.call(this);
      };
      G27_RulePanel.prototype.close = function() {
        _super.prototype.close.call(this);
        AudioManager_1.AudioManager.instance.playAudioEvent("CloseRule");
      };
      G27_RulePanel = __decorate([ ccclass ], G27_RulePanel);
      return G27_RulePanel;
    }(Slot_RulePanel_1.default);
    exports.default = G27_RulePanel;
    cc._RF.pop();
  }, {
    "../../../Common/Tools/AudioManager/AudioManager": void 0,
    "../../../SlotFramework/Game/Panel/Slot_RulePanel": void 0,
    "../../Common/G27_DataManager": "G27_DataManager"
  } ],
  G27_SlotLineManager: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "77987orRDhEPJ98//Yz8a+S", "G27_SlotLineManager");
    "use strict";
    var __extends = this && this.__extends || function() {
      var extendStatics = function(d, b) {
        extendStatics = Object.setPrototypeOf || {
          __proto__: []
        } instanceof Array && function(d, b) {
          d.__proto__ = b;
        } || function(d, b) {
          for (var p in b) Object.prototype.hasOwnProperty.call(b, p) && (d[p] = b[p]);
        };
        return extendStatics(d, b);
      };
      return function(d, b) {
        extendStatics(d, b);
        function __() {
          this.constructor = d;
        }
        d.prototype = null === b ? Object.create(b) : (__.prototype = b.prototype, new __());
      };
    }();
    var __decorate = this && this.__decorate || function(decorators, target, key, desc) {
      var c = arguments.length, r = c < 3 ? target : null === desc ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
      if ("object" === typeof Reflect && "function" === typeof Reflect.decorate) r = Reflect.decorate(decorators, target, key, desc); else for (var i = decorators.length - 1; i >= 0; i--) (d = decorators[i]) && (r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r);
      return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    var Slot_LineManager_1 = require("../../../SlotFramework/Game/view/Slot_LineManager");
    var ccclass = cc._decorator.ccclass;
    var G27_SlotLineManager = function(_super) {
      __extends(G27_SlotLineManager, _super);
      function G27_SlotLineManager() {
        return null !== _super && _super.apply(this, arguments) || this;
      }
      G27_SlotLineManager.prototype.setLinePosition = function(p_lineTable, p_linePositionjson) {
        for (var i = 0; i < p_lineTable.length; i++) {
          this.linePosition[i] = [];
          var _json = JSON.parse(p_linePositionjson);
          for (var j = 0; j < _json["line"][i]["pos"].length; j++) {
            var _x = _json["line"][i]["pos"][j]["x"];
            var _y = _json["line"][i]["pos"][j]["y"];
            this.linePosition[i][j] = new cc.Vec2(_x, _y);
          }
          "R" == _json["line"][i]["tip"] && (this.lineTipSide[i] = Slot_LineManager_1.LineTipSide.RIGHT);
          "L" == _json["line"][i]["tip"] && (this.lineTipSide[i] = Slot_LineManager_1.LineTipSide.LEFT);
        }
      };
      G27_SlotLineManager = __decorate([ ccclass ], G27_SlotLineManager);
      return G27_SlotLineManager;
    }(Slot_LineManager_1.default);
    exports.default = G27_SlotLineManager;
    cc._RF.pop();
  }, {
    "../../../SlotFramework/Game/view/Slot_LineManager": void 0
  } ],
  G27_SlotReelManager: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "20f7bbMILBGzZV/vuw/+w9M", "G27_SlotReelManager");
    "use strict";
    var __extends = this && this.__extends || function() {
      var extendStatics = function(d, b) {
        extendStatics = Object.setPrototypeOf || {
          __proto__: []
        } instanceof Array && function(d, b) {
          d.__proto__ = b;
        } || function(d, b) {
          for (var p in b) Object.prototype.hasOwnProperty.call(b, p) && (d[p] = b[p]);
        };
        return extendStatics(d, b);
      };
      return function(d, b) {
        extendStatics(d, b);
        function __() {
          this.constructor = d;
        }
        d.prototype = null === b ? Object.create(b) : (__.prototype = b.prototype, new __());
      };
    }();
    var __decorate = this && this.__decorate || function(decorators, target, key, desc) {
      var c = arguments.length, r = c < 3 ? target : null === desc ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
      if ("object" === typeof Reflect && "function" === typeof Reflect.decorate) r = Reflect.decorate(decorators, target, key, desc); else for (var i = decorators.length - 1; i >= 0; i--) (d = decorators[i]) && (r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r);
      return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    var AudioManager_1 = require("../../../Common/Tools/AudioManager/AudioManager");
    var CommonTool_1 = require("../../../Common/Tools/CommonTool");
    var Slot_ReelManager_1 = require("../../../SlotFramework/Game/view/Slot_ReelManager");
    var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
    var G27_SlotReelManager = function(_super) {
      __extends(G27_SlotReelManager, _super);
      function G27_SlotReelManager() {
        var _this = null !== _super && _super.apply(this, arguments) || this;
        _this.omenEffectParent = null;
        _this.rewardScoreLableList = [];
        _this.reelsList = [];
        _this.isChangeSymbolEffecting = false;
        _this.arrTimeout = [];
        return _this;
      }
      G27_SlotReelManager.prototype.init = function(p_data) {
        _super.prototype.init.call(this, p_data);
        this.omenEffectParent.zIndex = 1;
        this.hideScoreReWardEffect();
      };
      G27_SlotReelManager.prototype.changeSymbolEffect = function(p_changePos, p_targetSymbol, aniType, isFreegame) {
        var _this = this;
        var isPlayEffectAudio = true;
        var count = 0;
        this.isChangeSymbolEffecting = true;
        this.arrTimeout = [];
        this.arrTimeout.push(window.setTimeout(function() {
          var _loop_2 = function(i) {
            count = 0;
            p_changePos[i].forEach(function(item) {
              1 === item && count++;
            });
            if (isFreegame && ((0 === i || 4 === i) && 4 === count || 0 !== i && 4 !== i && 3 === count)) return "continue";
            _this.arrTimeout.push(window.setTimeout(function() {
              for (var j = 0; j < p_changePos[i].length; j++) if (1 === p_changePos[i][j]) {
                if (isPlayEffectAudio) {
                  isPlayEffectAudio = false;
                  AudioManager_1.AudioManager.instance.playAudioEvent("ChangeSymbolEffect");
                }
                _this.reelsList[i].preChangeSymbolEffect(j);
              }
            }, 400 * i));
          };
          for (var i = 0; i < p_changePos.length; i++) _loop_2(i);
        }, 400));
        var delay = 0;
        count = 1;
        var isChange = false;
        var _loop_1 = function() {
          1 === count ? delay = 2.65 : delay += 0 === aniType[count - 1] ? .6 : 1.1;
          var subCount = count++;
          this_1.scheduleOnce(function() {
            for (var j = 0; j < p_changePos[subCount].length; j++) if (1 === p_changePos[subCount][j]) {
              isChange = true;
              _this.reelsList[subCount].changeSymbolEffect(j, p_targetSymbol);
            }
            isChange && _this.reelsList[subCount].showBoardSmoke(4 === subCount);
            isChange = false;
            4 === subCount && _this.arrTimeout.push(window.setTimeout(function() {
              _this.isChangeSymbolEffecting = false;
            }, 1e3));
          }, delay);
        };
        var this_1 = this;
        while (count < p_changePos.length) _loop_1();
      };
      G27_SlotReelManager.prototype.changeSymbol = function(p_changePos, p_targetSymbol) {
        for (var i = 0; i < p_changePos.length; i++) for (var j = 0; j < p_changePos[i].length; j++) 1 === p_changePos[i][j] && this.reelsList[i].changeSymbolEffect(j, p_targetSymbol, false);
      };
      G27_SlotReelManager.prototype.stopChangeSymbol = function() {
        this.unscheduleAllCallbacks();
        for (var i = 0; i < this.arrTimeout.length; i++) clearTimeout(this.arrTimeout[i]);
        for (var i = 0; i < this.reelsList.length; i++) this.reelsList[i].stopChangeSymbol();
        this.isChangeSymbolEffecting = false;
      };
      G27_SlotReelManager.prototype.isChangeSymbolIng = function() {
        return this.isChangeSymbolEffecting;
      };
      G27_SlotReelManager.prototype.showMaskEffect = function() {
        this.omenEffectParent.active = true;
        _super.prototype.showMaskEffect.call(this);
      };
      G27_SlotReelManager.prototype.hideMaskEffect = function() {
        var _this = this;
        this.omenMask.stopAllActions();
        cc.tween(this.omenMask).to(.1, {
          opacity: 0
        }, {
          easing: "cubicIn"
        }).call(function() {
          _this.omenMask.active = false;
          _this.omenEffectParent.active = false;
        }).start();
      };
      G27_SlotReelManager.prototype.showScoreReWardEffect = function(p_winPosition, score) {
        var pos = 2;
        1 == p_winPosition[2][1] ? pos = 0 : 1 == p_winPosition[2][2] && (pos = 1);
        this.rewardScoreLableList[pos].node.active = true;
        this.rewardScoreLableList[pos].string = CommonTool_1.CommonTool.getNumText(Number(score), 2, true, false);
        this.rewardScoreLableList[pos].node.scale = 0;
        cc.tween(this.rewardScoreLableList[pos].node).to(.38, {
          scale: 1
        }, {
          easing: "elasticOut"
        }).start();
      };
      G27_SlotReelManager.prototype.hideScoreReWardEffect = function() {
        for (var i = 0; i < this.rewardScoreLableList.length; i++) this.rewardScoreLableList[i].node.active = false;
      };
      G27_SlotReelManager.prototype.getSymbolReWardPos = function(p_winPosition) {
        var list = [];
        for (var i = 0; i < p_winPosition.length; i++) this.reelsList[i].getSymbolReWardPos(p_winPosition[i]).forEach(function(pos) {
          return list.push(pos);
        });
        return list;
      };
      __decorate([ property(cc.Node) ], G27_SlotReelManager.prototype, "omenEffectParent", void 0);
      __decorate([ property([ cc.Label ]) ], G27_SlotReelManager.prototype, "rewardScoreLableList", void 0);
      G27_SlotReelManager = __decorate([ ccclass ], G27_SlotReelManager);
      return G27_SlotReelManager;
    }(Slot_ReelManager_1.default);
    exports.default = G27_SlotReelManager;
    cc._RF.pop();
  }, {
    "../../../Common/Tools/AudioManager/AudioManager": void 0,
    "../../../Common/Tools/CommonTool": void 0,
    "../../../SlotFramework/Game/view/Slot_ReelManager": void 0
  } ],
  G27_SocketConnect: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "f5493pzIU1Pg42GQ5in//dB", "G27_SocketConnect");
    "use strict";
    var __extends = this && this.__extends || function() {
      var extendStatics = function(d, b) {
        extendStatics = Object.setPrototypeOf || {
          __proto__: []
        } instanceof Array && function(d, b) {
          d.__proto__ = b;
        } || function(d, b) {
          for (var p in b) Object.prototype.hasOwnProperty.call(b, p) && (d[p] = b[p]);
        };
        return extendStatics(d, b);
      };
      return function(d, b) {
        extendStatics(d, b);
        function __() {
          this.constructor = d;
        }
        d.prototype = null === b ? Object.create(b) : (__.prototype = b.prototype, new __());
      };
    }();
    var __decorate = this && this.__decorate || function(decorators, target, key, desc) {
      var c = arguments.length, r = c < 3 ? target : null === desc ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
      if ("object" === typeof Reflect && "function" === typeof Reflect.decorate) r = Reflect.decorate(decorators, target, key, desc); else for (var i = decorators.length - 1; i >= 0; i--) (d = decorators[i]) && (r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r);
      return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    exports.G27_SocketConnect = void 0;
    var Slot_SocketConnect_1 = require("../../../SlotFramework/Common/Socket/Slot_SocketConnect");
    var G27_DataManager_1 = require("../G27_DataManager");
    var G27_DynamicPopUpPanelManager_1 = require("../G27_DynamicPopUpPanelManager");
    var G27_SocketManager_1 = require("./G27_SocketManager");
    var ccclass = cc._decorator.ccclass;
    var G27_SocketConnect = function(_super) {
      __extends(G27_SocketConnect, _super);
      function G27_SocketConnect() {
        return null !== _super && _super.apply(this, arguments) || this;
      }
      Object.defineProperty(G27_SocketConnect.prototype, "socketManager", {
        get: function() {
          return G27_SocketManager_1.default.getInstance();
        },
        enumerable: false,
        configurable: true
      });
      Object.defineProperty(G27_SocketConnect.prototype, "popupManager", {
        get: function() {
          return G27_DynamicPopUpPanelManager_1.default.getInstance();
        },
        enumerable: false,
        configurable: true
      });
      Object.defineProperty(G27_SocketConnect.prototype, "dataManager", {
        get: function() {
          return G27_DataManager_1.default.getInstance();
        },
        enumerable: false,
        configurable: true
      });
      G27_SocketConnect.prototype.init = function() {
        _super.prototype.init.call(this);
      };
      G27_SocketConnect = __decorate([ ccclass ], G27_SocketConnect);
      return G27_SocketConnect;
    }(Slot_SocketConnect_1.Slot_SocketConnect);
    exports.G27_SocketConnect = G27_SocketConnect;
    cc._RF.pop();
  }, {
    "../../../SlotFramework/Common/Socket/Slot_SocketConnect": void 0,
    "../G27_DataManager": "G27_DataManager",
    "../G27_DynamicPopUpPanelManager": "G27_DynamicPopUpPanelManager",
    "./G27_SocketManager": "G27_SocketManager"
  } ],
  G27_SocketManager: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "8447bMTbw9P5LyeevUAe4zC", "G27_SocketManager");
    "use strict";
    var __extends = this && this.__extends || function() {
      var extendStatics = function(d, b) {
        extendStatics = Object.setPrototypeOf || {
          __proto__: []
        } instanceof Array && function(d, b) {
          d.__proto__ = b;
        } || function(d, b) {
          for (var p in b) Object.prototype.hasOwnProperty.call(b, p) && (d[p] = b[p]);
        };
        return extendStatics(d, b);
      };
      return function(d, b) {
        extendStatics(d, b);
        function __() {
          this.constructor = d;
        }
        d.prototype = null === b ? Object.create(b) : (__.prototype = b.prototype, new __());
      };
    }();
    var __decorate = this && this.__decorate || function(decorators, target, key, desc) {
      var c = arguments.length, r = c < 3 ? target : null === desc ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
      if ("object" === typeof Reflect && "function" === typeof Reflect.decorate) r = Reflect.decorate(decorators, target, key, desc); else for (var i = decorators.length - 1; i >= 0; i--) (d = decorators[i]) && (r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r);
      return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    var G27_TestSocket_1 = require("./G27_TestSocket");
    var G27_DataManager_1 = require("../G27_DataManager");
    var G27_DynamicPopUpPanelManager_1 = require("../G27_DynamicPopUpPanelManager");
    var G27_SocketConnect_1 = require("./G27_SocketConnect");
    var Slot_SocketManager_1 = require("../../../SlotFramework/Common/Socket/Slot_SocketManager");
    var ccclass = cc._decorator.ccclass;
    var G27_SocketManager = function(_super) {
      __extends(G27_SocketManager, _super);
      function G27_SocketManager() {
        var _this = null !== _super && _super.apply(this, arguments) || this;
        _this.testSocket = null;
        _this.isOffline = false;
        _this.selfEnterEventCallback = null;
        _this.socketConnect = null;
        return _this;
      }
      G27_SocketManager.prototype.init = function() {
        this.socketConnect = new G27_SocketConnect_1.G27_SocketConnect();
        this.socketConnect.init();
        if (G27_DataManager_1.default.offLineMode) {
          this.testSocket = new G27_TestSocket_1.default();
          this.isOffline = true;
        } else {
          console.log("Socket Init");
          this.setErrorAlertCallback(this.showErrorAlert.bind(this));
          this.initGameClient();
          this.client && this.registerEvents();
          this.registerOnSelfEnterEvent();
        }
      };
      G27_SocketManager.prototype.setSelfEnterEventCallback = function(selfEnterCallback) {
        this.selfEnterEventCallback = selfEnterCallback;
      };
      G27_SocketManager.prototype.login = function() {
        var _this = this;
        if (G27_DataManager_1.default.offLineMode) {
          console.log("\u55ae\u6a5f\u6a21\u5f0f\u767b\u5165");
          this.testSocket = new G27_TestSocket_1.default();
          this.isOffline = true;
          this.init();
          this.testSocket.login().then(function(loginRes) {
            _this.handleOnOpen(loginRes);
          });
        } else {
          console.log("\u9023\u7dda\u6a21\u5f0f\u767b\u5165");
          _super.prototype.login.call(this);
        }
      };
      G27_SocketManager.prototype.showErrorAlert = function(message, title, confirmCallback) {
        G27_DynamicPopUpPanelManager_1.default.getInstance().confirmSystemMsgDialogBox.show(message, function() {
          confirmCallback && confirmCallback();
        });
      };
      G27_SocketManager.prototype.showErrorYesNoAlert = function(message, title, yesCallback, noCallback) {
        G27_DynamicPopUpPanelManager_1.default.getInstance().ynDialogBox.show(message, function() {
          yesCallback && yesCallback();
        }, function() {
          noCallback && noCallback();
        });
      };
      G27_SocketManager.prototype.setOnGetInitialReelInfoEvent = function() {
        if (this.isOffline) return this.sendTestSocket("GetInitialReelInfo", this.testSocket.setOnGetInitialReelInfoEvent.bind(this.testSocket));
        return this.request("GetInitialReelInfo", this.client.setOnGetInitialReelInfoEvent.bind(this.client), null);
      };
      G27_SocketManager.prototype.setOnGetRoomListEvent = function() {
        return this.request("GetRoomList", this.client.setOnGetInitialReelInfoEvent.bind(this.client), null);
      };
      G27_SocketManager.prototype.setOnChoseRoomEvent = function(roomId) {
        return this.request("ChoseRoom", this.client.setOnChoseRoomEvent.bind(this.client), roomId);
      };
      G27_SocketManager.prototype.setOnFastChoseRoomEvent = function() {
        if (this.isOffline) return this.sendTestSocket("FastChoseRoom", this.testSocket.setOnFastChoseRoomEvent.bind(this.testSocket));
        return this.request("FastChoseRoom", this.client.setOnFastChoseRoomEvent.bind(this.client), null);
      };
      G27_SocketManager.prototype.setOnSpinBaseGameEvent = function(multiple) {
        if (this.isOffline) return this.sendTestSocket("SpinBaseGame", this.testSocket.setOnSpinBaseGameEvent.bind(this.testSocket), multiple);
        return this.request("SpinBaseGame", this.client.setOnSpinBaseGameEvent.bind(this.client), multiple);
      };
      G27_SocketManager.prototype.setOnLeaveEvent = function() {
        if (this.isOffline) return this.sendTestSocket("Leave", this.testSocket.setOnLeaveEvent.bind(this.testSocket));
        return this.request("Leave", this.client.setOnLeaveEvent.bind(this.client), null);
      };
      G27_SocketManager.prototype.resetRoomTimeout = function() {
        if (this.isOffline) return;
        return this.request("ResetRoomTimeout", this.client.resetRoomTimeout.bind(this.client), null);
      };
      G27_SocketManager.prototype.registerOnSelfEnterEvent = function() {
        var _this = this;
        this.client.setSelfEnterEvent(function(data) {
          _this.selfEnterEventCallback(data);
        });
      };
      G27_SocketManager.prototype.sendTestSocket = function(eventName, event, content) {
        var _this = this;
        this.showSendLog(eventName, null);
        return new Promise(function(resolve) {
          event.bind(_this)(content).then(function(data) {
            _this.showResponseLog(eventName, data);
            resolve(data);
          });
        });
      };
      G27_SocketManager = __decorate([ ccclass ], G27_SocketManager);
      return G27_SocketManager;
    }(Slot_SocketManager_1.default);
    exports.default = G27_SocketManager;
    cc._RF.pop();
  }, {
    "../../../SlotFramework/Common/Socket/Slot_SocketManager": void 0,
    "../G27_DataManager": "G27_DataManager",
    "../G27_DynamicPopUpPanelManager": "G27_DynamicPopUpPanelManager",
    "./G27_SocketConnect": "G27_SocketConnect",
    "./G27_TestSocket": "G27_TestSocket"
  } ],
  G27_SymbolTipPanel: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "d2ffc0QVodPH5A544hZ1NS1", "G27_SymbolTipPanel");
    "use strict";
    var __extends = this && this.__extends || function() {
      var extendStatics = function(d, b) {
        extendStatics = Object.setPrototypeOf || {
          __proto__: []
        } instanceof Array && function(d, b) {
          d.__proto__ = b;
        } || function(d, b) {
          for (var p in b) Object.prototype.hasOwnProperty.call(b, p) && (d[p] = b[p]);
        };
        return extendStatics(d, b);
      };
      return function(d, b) {
        extendStatics(d, b);
        function __() {
          this.constructor = d;
        }
        d.prototype = null === b ? Object.create(b) : (__.prototype = b.prototype, new __());
      };
    }();
    var __decorate = this && this.__decorate || function(decorators, target, key, desc) {
      var c = arguments.length, r = c < 3 ? target : null === desc ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
      if ("object" === typeof Reflect && "function" === typeof Reflect.decorate) r = Reflect.decorate(decorators, target, key, desc); else for (var i = decorators.length - 1; i >= 0; i--) (d = decorators[i]) && (r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r);
      return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    var MathUtils_1 = require("../../../Common/Tools/MathUtils");
    var Slot_SymbolTipPanel_1 = require("../../../SlotFramework/Game/Panel/Slot_SymbolTipPanel");
    var Slot_DataManager_1 = require("../../../SlotFramework/Slot_DataManager");
    var G27_DataManager_1 = require("../../Common/G27_DataManager");
    var G27_Symbol_1 = require("./G27_Symbol");
    var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
    var G27_SymbolTipPanel = function(_super) {
      __extends(G27_SymbolTipPanel, _super);
      function G27_SymbolTipPanel() {
        var _this = null !== _super && _super.apply(this, arguments) || this;
        _this.defaulTipSymbol = null;
        _this.wildSymbol = null;
        _this.scatterSymbol = null;
        _this.data = null;
        return _this;
      }
      G27_SymbolTipPanel.prototype.init = function() {
        this.data = G27_DataManager_1.default.getInstance();
        _super.prototype.init.call(this);
      };
      G27_SymbolTipPanel.prototype.getTipScore = function(id, combo) {
        var A = MathUtils_1.MathUtils.div10000(this.data.betOdds[this.data.betOddsStartIndex] * this.data.oneOddsScore);
        var B = 50;
        var C = this.data.magnificationTable[id][combo];
        var D = 1;
        console.warn("\nsymbol: " + Slot_DataManager_1.SYMBOL_NAME[id] + "\ncombo: " + combo + "\nA = " + A + ", B = " + B + ", C = " + C + ", D = " + D);
        return A / B * C * D;
      };
      __decorate([ property({
        type: G27_Symbol_1.default,
        override: true
      }) ], G27_SymbolTipPanel.prototype, "defaulTipSymbol", void 0);
      __decorate([ property({
        type: G27_Symbol_1.default,
        override: true
      }) ], G27_SymbolTipPanel.prototype, "wildSymbol", void 0);
      __decorate([ property({
        type: G27_Symbol_1.default,
        override: true
      }) ], G27_SymbolTipPanel.prototype, "scatterSymbol", void 0);
      G27_SymbolTipPanel = __decorate([ ccclass ], G27_SymbolTipPanel);
      return G27_SymbolTipPanel;
    }(Slot_SymbolTipPanel_1.default);
    exports.default = G27_SymbolTipPanel;
    cc._RF.pop();
  }, {
    "../../../Common/Tools/MathUtils": void 0,
    "../../../SlotFramework/Game/Panel/Slot_SymbolTipPanel": void 0,
    "../../../SlotFramework/Slot_DataManager": void 0,
    "../../Common/G27_DataManager": "G27_DataManager",
    "./G27_Symbol": "G27_Symbol"
  } ],
  G27_Symbol: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "8d865wg/WlH4qGP+0ymK+Lt", "G27_Symbol");
    "use strict";
    var __extends = this && this.__extends || function() {
      var extendStatics = function(d, b) {
        extendStatics = Object.setPrototypeOf || {
          __proto__: []
        } instanceof Array && function(d, b) {
          d.__proto__ = b;
        } || function(d, b) {
          for (var p in b) Object.prototype.hasOwnProperty.call(b, p) && (d[p] = b[p]);
        };
        return extendStatics(d, b);
      };
      return function(d, b) {
        extendStatics(d, b);
        function __() {
          this.constructor = d;
        }
        d.prototype = null === b ? Object.create(b) : (__.prototype = b.prototype, new __());
      };
    }();
    var __decorate = this && this.__decorate || function(decorators, target, key, desc) {
      var c = arguments.length, r = c < 3 ? target : null === desc ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
      if ("object" === typeof Reflect && "function" === typeof Reflect.decorate) r = Reflect.decorate(decorators, target, key, desc); else for (var i = decorators.length - 1; i >= 0; i--) (d = decorators[i]) && (r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r);
      return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    var Slot_Symbol_1 = require("../../../SlotFramework/Game/view/Slot_Symbol");
    var Slot_DataManager_1 = require("../../../SlotFramework/Slot_DataManager");
    var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
    var G27_Symbol = function(_super) {
      __extends(G27_Symbol, _super);
      function G27_Symbol() {
        var _this = null !== _super && _super.apply(this, arguments) || this;
        _this.wildPlayWin = null;
        _this.wildPlayNormal = null;
        _this.h1PlayWin = null;
        _this.h2PlayWin = null;
        _this.ScatterPlayWin = null;
        _this.ScatterPlayNormal = null;
        _this.rewardFrame = null;
        _this.preChangeSymbolEffectFX = null;
        _this.changeSymbolEffectFX = null;
        _this.symbolName = "";
        return _this;
      }
      G27_Symbol.prototype.setData = function(p_nowIndex, p_symbolValue) {
        this.symbolName = Slot_DataManager_1.SYMBOL_NAME[p_symbolValue];
        this.nowIndex = p_nowIndex;
        this.symbolID = p_symbolValue;
        this.setSymbolSize();
        this.closeAllEffect();
        this.symbolSpine.setAnimation(0, this.symbolName + "_stop", true);
      };
      G27_Symbol.prototype.setSymbolSize = function() {
        this.node.setScale(.97, .97);
      };
      G27_Symbol.prototype.playRewardOnce = function() {
        this.symbolSpine.setAnimation(0, this.symbolName + "_play_win", false);
      };
      G27_Symbol.prototype.playRewardEffect = function(p_effectLoop) {
        this.rewardFrame.active = true;
        this.rewardFrame.getComponentInChildren(cc.Animation).play();
        this.rewardFrame.getComponentInChildren(cc.ParticleSystem3D).stop();
        this.rewardFrame.getComponentInChildren(cc.ParticleSystem3D).play();
        if (this.symbolID == Slot_DataManager_1.SYMBOL_NAME.Wild) {
          this.wildPlayNormal.active = false;
          this.wildPlayWin.active = true;
          this.wildPlayWin.getComponentsInChildren(cc.ParticleSystem3D).forEach(function(item) {
            item.stop();
            item.play();
          });
        } else if (this.symbolID == Slot_DataManager_1.SYMBOL_NAME.Scatter) {
          this.ScatterPlayNormal.active = false;
          this.ScatterPlayWin.active = true;
          this.ScatterPlayWin.getComponentInChildren(cc.ParticleSystem).stopSystem();
          this.ScatterPlayWin.getComponentInChildren(cc.ParticleSystem).resetSystem();
        } else if (this.symbolID == Slot_DataManager_1.SYMBOL_NAME.H1) {
          this.h1PlayWin.active = true;
          this.h1PlayWin.getComponentInChildren(cc.Animation).play();
        } else if (this.symbolID == Slot_DataManager_1.SYMBOL_NAME.H2) {
          this.h2PlayWin.active = true;
          this.h2PlayWin.getComponentInChildren(cc.Animation).play();
        }
        this.symbolSpine.setAnimation(0, this.symbolName + "_play_win", p_effectLoop);
      };
      G27_Symbol.prototype.playScrollingEffect = function() {
        this.closeAllEffect();
        this.symbolSpine.setAnimation(0, this.symbolName + "_stop", true);
      };
      G27_Symbol.prototype.playIdleEffect = function() {
        this.closeAllEffect();
        if (this.symbolID == Slot_DataManager_1.SYMBOL_NAME.Wild) {
          this.wildPlayNormal.active = true;
          this.wildPlayNormal.getComponentsInChildren(cc.ParticleSystem3D).forEach(function(item) {
            item.stop();
            item.play();
          });
        } else if (this.symbolID == Slot_DataManager_1.SYMBOL_NAME.Scatter) {
          this.ScatterPlayNormal.active = true;
          this.ScatterPlayNormal.getComponentInChildren(cc.ParticleSystem).stopSystem();
          this.ScatterPlayNormal.getComponentInChildren(cc.ParticleSystem).resetSystem();
          this.ScatterPlayNormal.getComponentInChildren(cc.ParticleSystem3D).stop();
          this.ScatterPlayNormal.getComponentInChildren(cc.ParticleSystem3D).play();
        }
        this.symbolSpine.setAnimation(0, this.symbolName + "_play_normal", true);
      };
      G27_Symbol.prototype.closeAllEffect = function() {
        this.rewardFrame.active = false;
        this.wildPlayNormal.active = false;
        this.wildPlayWin.active = false;
        this.ScatterPlayWin.active = false;
        this.ScatterPlayNormal.active = false;
        this.h1PlayWin.active = false;
        this.h2PlayWin.active = false;
        this.preChangeSymbolEffectFX.active = false;
        this.changeSymbolEffectFX.stopAllActions();
        this.changeSymbolEffectFX.active = false;
      };
      G27_Symbol.prototype.preChangeSymbolEffect = function() {
        this.setZOrder(10);
        this.preChangeSymbolEffectFX.active = true;
        this.preChangeSymbolEffectFX.getComponentInChildren(cc.Animation).play();
        this.preChangeSymbolEffectFX.getComponentInChildren(cc.ParticleSystem3D).stop();
        this.preChangeSymbolEffectFX.getComponentInChildren(cc.ParticleSystem3D).play();
      };
      G27_Symbol.prototype.changeSymbolEffect = function(p_targetSymbol) {
        var _this = this;
        cc.tween(this.changeSymbolEffectFX).call(function() {
          _this.changeSymbolEffectFX.active = true;
          _this.changeSymbolEffectFX.getComponentsInChildren(cc.ParticleSystem3D).forEach(function(item) {
            item.stop();
            item.play();
          });
        }).delay(.35).call(function() {
          _this.setData(_this.nowIndex, p_targetSymbol);
        }).start();
        this.symbolID = p_targetSymbol;
      };
      G27_Symbol.prototype.changeSymbol = function(p_targetSymbol) {
        this.setData(this.nowIndex, p_targetSymbol);
        this.symbolID = p_targetSymbol;
      };
      G27_Symbol.prototype.getSymbolSpinePath = function() {
        var _spriteName = this.bundlePath + "/FX_Spine/Symbol/Symbol";
        return _spriteName;
      };
      __decorate([ property(cc.Node) ], G27_Symbol.prototype, "wildPlayWin", void 0);
      __decorate([ property(cc.Node) ], G27_Symbol.prototype, "wildPlayNormal", void 0);
      __decorate([ property(cc.Node) ], G27_Symbol.prototype, "h1PlayWin", void 0);
      __decorate([ property(cc.Node) ], G27_Symbol.prototype, "h2PlayWin", void 0);
      __decorate([ property(cc.Node) ], G27_Symbol.prototype, "ScatterPlayWin", void 0);
      __decorate([ property(cc.Node) ], G27_Symbol.prototype, "ScatterPlayNormal", void 0);
      __decorate([ property(cc.Node) ], G27_Symbol.prototype, "rewardFrame", void 0);
      __decorate([ property(cc.Node) ], G27_Symbol.prototype, "preChangeSymbolEffectFX", void 0);
      __decorate([ property(cc.Node) ], G27_Symbol.prototype, "changeSymbolEffectFX", void 0);
      G27_Symbol = __decorate([ ccclass ], G27_Symbol);
      return G27_Symbol;
    }(Slot_Symbol_1.default);
    exports.default = G27_Symbol;
    cc._RF.pop();
  }, {
    "../../../SlotFramework/Game/view/Slot_Symbol": void 0,
    "../../../SlotFramework/Slot_DataManager": void 0
  } ],
  G27_TestSocket: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "6a1f3jI6tBCCoOctKXlEtlV", "G27_TestSocket");
    "use strict";
    var __decorate = this && this.__decorate || function(decorators, target, key, desc) {
      var c = arguments.length, r = c < 3 ? target : null === desc ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
      if ("object" === typeof Reflect && "function" === typeof Reflect.decorate) r = Reflect.decorate(decorators, target, key, desc); else for (var i = decorators.length - 1; i >= 0; i--) (d = decorators[i]) && (r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r);
      return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    var G27_DataManager_1 = require("../G27_DataManager");
    var G27_CommonData_1 = require("../../Game/G27_CommonData");
    var ccclass = cc._decorator.ccclass;
    var G27_TestSocket = function() {
      function G27_TestSocket() {
        this.tempSpinCount = 1;
      }
      G27_TestSocket_1 = G27_TestSocket;
      G27_TestSocket.prototype.login = function() {
        var _this = this;
        return new Promise(function(resolve) {
          var _data = new G27_CommonData_1.LoginData();
          _data.nickname = "\u533f\u540dA";
          _data.language = "zh-cn";
          _data.balance = 99e3;
          _data.timestamp = 1e9;
          _data.themeType = 0;
          _data.icon = 1;
          _data.gameId = 27;
          _this.data = G27_DataManager_1.default.getInstance();
          resolve(_data);
        });
      };
      G27_TestSocket.prototype.setOnFastChoseRoomEvent = function() {
        return new Promise(function(resolve) {
          var _data = new G27_CommonData_1.FastChoseRoomData();
          _data.roomId = "\u6e2c\u8a66Room";
          resolve(_data);
        });
      };
      G27_TestSocket.prototype.setOnGetInitialReelInfoEvent = function() {
        return new Promise(function(resolve) {
          var _data = new G27_CommonData_1.GetInitialReelInfoData();
          _data.position = [ 4, 2, 3, 4, 4 ];
          _data.wheelRatioList = [ 2, 3, 5, 10, 20, 50, 100, 150, 200, 999 ];
          _data.reels = new G27_CommonData_1.ReelTable();
          var _BGTable = [];
          _BGTable[0] = [ 13, 1, 2, 14, 1, 14, 2, 11, 20, 14, 12, 11, 4, 11, 13, 14, 11, 14, 12, 14, 3, 3, 3, 3, 5, 11, 12, 13, 14, 13, 12, 13, 14, 50, 13, 1, 2, 14, 12, 11, 12, 11, 4, 11, 13, 50, 11, 14, 12, 14, 3, 12, 11, 13, 5, 13 ];
          _BGTable[1] = [ 12, 50, 50, 50, 13, 3, 20, 14, 14, 1, 11, 11, 12, 12, 13, 13, 1, 12, 2, 11, 11, 20, 2, 14, 5, 13, 2, 14, 14, 1, 3, 4, 50, 14, 11, 12, 13, 20, 12, 12, 11, 11, 13, 13, 14, 14, 20, 13, 11, 11, 14, 50, 3, 3, 4, 4, 5, 5 ];
          _BGTable[2] = [ 11, 12, 11, 4, 2, 11, 2, 12, 14, 14, 14, 20, 14, 12, 1, 50, 1, 3, 4, 5, 4, 3, 20, 11, 13, 14, 12, 5, 11, 13, 20, 12, 3, 14, 50, 12, 13, 14, 4, 5, 11, 12, 13, 14, 20, 11, 12, 13, 1, 4, 5, 11, 2, 13, 14, 50 ];
          _BGTable[3] = [ 13, 20, 14, 14, 3, 12, 1, 20, 11, 12, 13, 14, 13, 2, 11, 1, 50, 1, 14, 5, 20, 5, 14, 3, 4, 20, 5, 2, 50, 2, 3, 5, 11, 11, 12, 12, 20, 13, 13, 14, 50, 11, 11, 12, 12, 4, 4, 5, 5 ];
          _BGTable[4] = [ 3, 11, 4, 14, 5, 50, 11, 14, 4, 20, 5, 12, 3, 13, 12, 13, 5, 1, 50, 1, 14, 13, 2, 11, 12, 14, 2, 12, 13, 5, 1, 1, 50, 2, 2, 3, 3, 4, 4 ];
          var _FGTable = [];
          _FGTable[0] = [ 13, 1, 2, 14, 13, 14, 12, 11, 12, 14, 12, 11, 4, 11, 13, 14, 11, 14, 12, 14, 2, 2, 2, 2, 5, 11, 12, 13, 14, 13, 12, 13, 14, 50, 13, 1, 2, 14, 12, 11, 12, 11, 4, 11, 13, 50, 11, 14, 12, 14, 3, 12, 11, 13, 5, 13 ];
          _FGTable[1] = [ 12, 12, 12, 50, 13, 13, 20, 14, 14, 1, 11, 11, 12, 12, 13, 13, 1, 12, 2, 11, 11, 20, 2, 14, 5, 13, 2, 14, 14, 1, 3, 4, 50, 14, 11, 12, 13, 20, 12, 12, 11, 11, 13, 13, 14, 14, 20, 13, 11, 11, 14, 50, 3, 3, 4, 4, 5, 5 ];
          _FGTable[2] = [ 11, 13, 11, 13, 2, 11, 2, 12, 14, 12, 14, 20, 14, 12, 1, 50, 1, 3, 4, 5, 4, 3, 20, 11, 13, 14, 12, 5, 11, 13, 20, 12, 3, 14, 50, 12, 13, 14, 4, 5, 11, 12, 13, 14, 20, 11, 12, 13, 1, 4, 5, 11, 2, 13, 14, 50 ];
          _FGTable[3] = [ 13, 13, 14, 14, 3, 12, 1, 20, 11, 12, 13, 14, 13, 2, 11, 1, 50, 1, 14, 5, 20, 5, 14, 3, 4, 20, 5, 2, 50, 2, 3, 5, 11, 11, 12, 12, 20, 13, 13, 14, 50, 11, 11, 12, 12, 4, 4, 5, 5 ];
          _FGTable[4] = [ 3, 11, 4, 14, 5, 50, 11, 14, 4, 11, 5, 12, 3, 13, 12, 13, 5, 1, 50, 1, 14, 13, 2, 11, 12, 14, 2, 12, 13, 5, 1, 1, 50, 2, 2, 3, 3, 4, 4 ];
          _data.reels.BG = _BGTable;
          _data.reels.FG = _FGTable;
          resolve(_data);
        });
      };
      G27_TestSocket.prototype.setOnSpinBaseGameEvent = function(p_multiple) {
        var _this = this;
        return new Promise(function(resolve) {
          var _data = null;
          p_multiple *= _this.data.oneOddsScore;
          -1 != G27_TestSocket_1.assignData && (_this.tempSpinCount = G27_TestSocket_1.assignData);
          switch (_this.tempSpinCount) {
           case 1:
            _data = _this.testSpinData_1(p_multiple);
            break;

           case 2:
            _data = _this.testSpinData_2(p_multiple);
            break;

           case 3:
            _data = _this.testSpinData_3(p_multiple);
            break;

           case 4:
            _data = _this.testSpinData_4(p_multiple);
            break;

           case 5:
            _data = _this.testSpinData_5(p_multiple);
            break;

           case 6:
            _data = _this.testSpinData_6(p_multiple);
            break;

           case 7:
            _data = _this.testSpinData_7(p_multiple);
            break;

           case 8:
            _data = _this.testSpinData_8(p_multiple);
            break;

           case 9:
            _data = _this.testSpinData_9(p_multiple);
            break;

           case 10:
            _data = _this.testSpinData_10(p_multiple);
          }
          _this.tempSpinCount++;
          _this.tempSpinCount > 10 && (_this.tempSpinCount = 1);
          resolve(_data);
        });
      };
      G27_TestSocket.prototype.testSpinData_1 = function(p_betScore) {
        var _data = new G27_CommonData_1.BaseGameSpinResponeData();
        _data.currentCash = 1e4 * this.data.coin - p_betScore;
        _data.totalBet = p_betScore;
        _data.totalBonus = 0;
        _data.resultList[0] = new G27_CommonData_1.ResultData();
        _data.resultList[0].randomNumList = [ 1, 1, 1, 1, 1 ];
        _data.resultList[0].totalWinBonus = 0;
        return _data;
      };
      G27_TestSocket.prototype.testSpinData_2 = function(p_betScore) {
        var _data = new G27_CommonData_1.BaseGameSpinResponeData();
        var winNum = 3;
        var lineNum = 5;
        var freeGameOdd = 1;
        var symbolOdd = this.data.magnificationTable[winNum][lineNum];
        _data.totalBet = p_betScore;
        _data.resultList[0] = new G27_CommonData_1.ResultData();
        _data.resultList[0].randomNumList = [ 22, 10, 5, 6, 1 ];
        _data.resultList[0].changeSymbol = 3;
        _data.resultList[0].changePos = [ [ 0, 0, 0, 0 ], [ 0, 1, 0, 0 ], [ 0, 0, 0, 0 ], [ 0, 1, 1, 1 ], [ 0, 1, 0, 1 ] ];
        _data.resultList[0].totalHitPos = [ [ 1, 1, 1, 1 ], [ 0, 1, 0, 0 ], [ 0, 0, 0, 0 ], [ 0, 1, 1, 1 ], [ 0, 1, 0, 1 ] ];
        _data.resultList[0].winWayList[0] = new G27_CommonData_1.WinWayData();
        _data.resultList[0].winWayList[0].singlePos = [ [ 1, 1, 1, 1 ], [ 0, 1, 0, 0 ], [ 0, 0, 0, 0 ], [ 0, 0, 1, 0 ], [ 1, 1, 1, 1 ] ];
        _data.resultList[0].winWayList[0].winBonus = p_betScore / 100 * symbolOdd * freeGameOdd * 24;
        _data.resultList.forEach(function(result) {
          result.winWayList.forEach(function(winWay) {
            result.totalWinBonus += winWay.winBonus;
          });
          _data.totalBonus += result.totalWinBonus;
        });
        _data.currentCash = 1e4 * this.data.coin - p_betScore + _data.totalBonus;
        return _data;
      };
      G27_TestSocket.prototype.testSpinData_3 = function(p_betScore) {
        var _data = new G27_CommonData_1.BaseGameSpinResponeData();
        _data.totalBet = p_betScore;
        _data.resultList[0] = new G27_CommonData_1.ResultData();
        _data.resultList[0].randomNumList = [ 8, 4, 10, 5, 8 ];
        _data.resultList[0].totalWinBonus = 0;
        _data.resultList[0].totalHitPos = [ [ 0, 0, 1, 1 ], [ 0, 1, 0, 0 ], [ 0, 1, 1, 1 ], [ 0, 0, 0, 0 ], [ 0, 0, 0, 1 ] ];
        _data.resultList[0].wheelIdx = 9;
        _data.resultList[0].wheelBonus = p_betScore / (this.data.oneOddsScore / 100) * 2e4 * 1 * 1;
        _data.resultList[0].winWayList[0] = new G27_CommonData_1.WinWayData();
        _data.resultList[0].winWayList[0].singlePos = [ [ 0, 0, 0, 1 ], [ 0, 1, 0, 0 ], [ 0, 1, 0, 0 ], [ 0, 0, 0, 0 ], [ 0, 0, 0, 0 ] ];
        _data.resultList[0].winWayList[0].winBonus = p_betScore / 100 * 8 * 1 * 24;
        _data.resultList[0].winWayList[1] = new G27_CommonData_1.WinWayData();
        _data.resultList[0].winWayList[1].singlePos = [ [ 0, 0, 0, 1 ], [ 0, 1, 0, 0 ], [ 0, 0, 1, 0 ], [ 0, 0, 0, 0 ], [ 0, 0, 0, 0 ] ];
        _data.resultList[0].winWayList[1].winBonus = p_betScore / 100 * 8 * 1 * 24;
        _data.resultList[0].scatter = new G27_CommonData_1.ScatterResult();
        _data.resultList[0].scatter.count = 3;
        _data.resultList[0].scatter.winBonus = 9e4;
        _data.resultList[0].scatter.scatterPos = [ [ 0, 0, 1, 0 ], [ 0, 0, 0, 0 ], [ 0, 0, 0, 1 ], [ 0, 0, 0, 0 ], [ 0, 0, 0, 1 ] ];
        _data.totalBonus = _data.resultList[0].wheelBonus;
        _data.resultList.forEach(function(result) {
          result.winWayList.forEach(function(winWay) {
            result.totalWinBonus += winWay.winBonus;
          });
          _data.totalBonus += result.totalWinBonus;
        });
        _data.currentCash = 1e4 * this.data.coin - p_betScore + _data.totalBonus;
        return _data;
      };
      G27_TestSocket.prototype.testSpinData_4 = function(p_betScore) {
        var _data = new G27_CommonData_1.BaseGameSpinResponeData();
        _data.totalBet = p_betScore;
        _data.totalBonus = 0;
        _data.resultList[0] = new G27_CommonData_1.ResultData();
        _data.resultList[0].randomNumList = [ 9, 1, 11, 4, 1 ];
        _data.resultList[0].totalWinBonus = 0;
        for (var i = 0; i < _data.resultList.length; i++) {
          for (var j = 0; j < _data.resultList[i].winWayList.length; j++) _data.resultList[i].totalWinBonus += _data.resultList[i].winWayList[j].winBonus;
          _data.totalBonus += _data.resultList[i].totalWinBonus;
        }
        _data.currentCash = 1e4 * this.data.coin - p_betScore + _data.totalBonus;
        return _data;
      };
      G27_TestSocket.prototype.testSpinData_5 = function(p_betScore) {
        var _data = new G27_CommonData_1.BaseGameSpinResponeData();
        _data.totalBonus = 0;
        _data.totalBet = p_betScore;
        _data.resultList[0] = new G27_CommonData_1.ResultData();
        _data.resultList[0].randomNumList = [ 8, 4, 10, 5, 8 ];
        _data.resultList[0].totalHitPos = [ [ 0, 0, 1, 0 ], [ 0, 0, 0, 0 ], [ 0, 0, 0, 1 ], [ 0, 0, 0, 0 ], [ 0, 0, 0, 1 ] ];
        _data.resultList[0].scatter = new G27_CommonData_1.ScatterResult();
        _data.resultList[0].scatter.count = 3;
        _data.resultList[0].scatter.scatterPos = [ [ 0, 0, 1, 0 ], [ 0, 0, 0, 0 ], [ 0, 0, 0, 1 ], [ 0, 0, 0, 0 ], [ 0, 0, 0, 1 ] ];
        _data.resultList[0].wheelIdx = 10;
        _data.resultList[0].wheelBonus = 0;
        _data.resultList[1] = new G27_CommonData_1.ResultData();
        _data.resultList[1].randomNumList = [ 8, 4, 10, 5, 8 ];
        _data.resultList[1].totalHitPos = [ [ 1, 0, 0, 0 ], [ 0, 0, 0, 0 ], [ 0, 0, 1, 0 ], [ 0, 0, 0, 0 ], [ 0, 0, 0, 1 ] ];
        _data.resultList[1].scatter = new G27_CommonData_1.ScatterResult();
        _data.resultList[1].scatter.count = 3;
        _data.resultList[1].scatter.scatterPos = [ [ 1, 0, 0, 0 ], [ 0, 0, 0, 0 ], [ 0, 0, 1, 0 ], [ 0, 0, 0, 0 ], [ 0, 0, 0, 1 ] ];
        _data.resultList[2] = new G27_CommonData_1.ResultData();
        _data.resultList[2].randomNumList = [ 22, 10, 5, 6, 1 ];
        _data.resultList[2].changeSymbol = 2;
        _data.resultList[2].changePos = [ [ 0, 0, 0, 0 ], [ 0, 1, 0, 0 ], [ 0, 1, 0, 1 ], [ 0, 0, 1, 0 ], [ 1, 0, 0, 1 ] ];
        _data.resultList[2].totalHitPos = [ [ 1, 1, 1, 1 ], [ 0, 1, 0, 0 ], [ 0, 1, 0, 1 ], [ 0, 0, 1, 0 ], [ 1, 1, 0, 1 ] ];
        _data.resultList[2].winWayList[0] = new G27_CommonData_1.WinWayData();
        _data.resultList[2].winWayList[0].singlePos = [ [ 1, 1, 1, 1 ], [ 0, 1, 0, 0 ], [ 0, 1, 0, 1 ], [ 0, 0, 1, 0 ], [ 1, 1, 0, 1 ] ];
        _data.resultList[2].winWayList[0].winBonus = p_betScore / 100 * 10 * 1 * 24;
        for (var i = 3; i <= 12; i++) {
          _data.resultList[i] = new G27_CommonData_1.ResultData();
          _data.resultList[i].randomNumList = [ 1, 1, 1, 1, 1 ];
          _data.resultList[i].totalWinBonus = 0;
        }
        for (var i = 0; i < _data.resultList.length; i++) {
          for (var j = 0; j < _data.resultList[i].winWayList.length; j++) _data.resultList[i].totalWinBonus += _data.resultList[i].winWayList[j].winBonus;
          _data.totalBonus += _data.resultList[i].totalWinBonus;
        }
        _data.currentCash = 1e4 * this.data.coin - p_betScore + _data.totalBonus;
        return _data;
      };
      G27_TestSocket.prototype.testSpinData_6 = function(p_betScore) {
        var _data = new G27_CommonData_1.BaseGameSpinResponeData();
        _data.totalBet = p_betScore;
        _data.resultList[0] = new G27_CommonData_1.ResultData();
        _data.resultList[0].randomNumList = [ 5, 18, 7, 1, 1 ];
        _data.resultList[0].totalHitPos = [ [ 0, 0, 0, 1 ], [ 0, 0, 1, 0 ], [ 0, 1, 0, 0 ], [ 0, 0, 0, 0 ], [ 0, 0, 0, 0 ] ];
        _data.resultList[0].totalWinBonus = p_betScore / (this.data.oneOddsScore / 100) * 1e4 * 1 * 1;
        _data.resultList[0].winWayList[0] = new G27_CommonData_1.WinWayData();
        _data.resultList[0].winWayList[0].lineNum = 3;
        _data.resultList[0].winWayList[0].winBonus = p_betScore / (this.data.oneOddsScore / 100) * 1e4 * 1 * 1;
        _data.resultList[0].winWayList[0].winNum = 12;
        for (var i = 0; i < _data.resultList.length; i++) {
          for (var j = 0; j < _data.resultList[i].winWayList.length; j++) _data.resultList[i].totalWinBonus += _data.resultList[i].winWayList[j].winBonus;
          _data.totalBonus += _data.resultList[i].totalWinBonus;
        }
        _data.currentCash = 1e4 * this.data.coin - p_betScore + _data.totalBonus;
        return _data;
      };
      G27_TestSocket.prototype.testSpinData_7 = function(p_betScore) {
        var _data = new G27_CommonData_1.BaseGameSpinResponeData();
        var winNum = 3;
        var lineNum = 5;
        var freeGameOdd = 1;
        var symbolOdd = this.data.magnificationTable[winNum][lineNum];
        _data.totalBet = p_betScore;
        _data.resultList[0] = new G27_CommonData_1.ResultData();
        _data.resultList[0].randomNumList = [ 2, 3, 18, 8, 1 ];
        _data.resultList[0].totalHitPos = [ [ 1, 1, 1, 1 ], [ 0, 1, 1, 1 ], [ 0, 1, 1, 1 ], [ 0, 1, 1, 1 ], [ 1, 1, 1, 1 ] ];
        _data.resultList[0].winWayList[0] = new G27_CommonData_1.WinWayData();
        _data.resultList[0].winWayList[0].singlePos = [ [ 1, 1, 1, 1 ], [ 0, 1, 1, 1 ], [ 0, 1, 1, 1 ], [ 0, 1, 1, 1 ], [ 1, 1, 1, 1 ] ];
        _data.resultList[0].winWayList[0].winBonus = p_betScore / 100 * symbolOdd * freeGameOdd * 24;
        _data.resultList.forEach(function(result) {
          result.winWayList.forEach(function(winWay) {
            result.totalWinBonus += winWay.winBonus;
          });
          _data.totalBonus += result.totalWinBonus;
        });
        _data.currentCash = 1e4 * this.data.coin - p_betScore + _data.totalBonus;
        return _data;
      };
      G27_TestSocket.prototype.testSpinData_8 = function(p_betScore) {
        var _data = new G27_CommonData_1.BaseGameSpinResponeData();
        _data.totalBonus = 0;
        _data.totalBet = p_betScore;
        _data.resultList[0] = new G27_CommonData_1.ResultData();
        _data.resultList[0].randomNumList = [ 8, 4, 10, 5, 8 ];
        _data.resultList[0].totalWinBonus = 0;
        _data.resultList[0].totalHitPos = [ [ 0, 0, 1, 0 ], [ 0, 0, 0, 0 ], [ 0, 0, 0, 1 ], [ 0, 0, 0, 0 ], [ 0, 0, 0, 1 ] ];
        _data.resultList[0].scatter = new G27_CommonData_1.ScatterResult();
        _data.resultList[0].scatter.count = 3;
        _data.resultList[0].scatter.scatterPos = [ [ 0, 0, 1, 0 ], [ 0, 0, 0, 0 ], [ 0, 0, 0, 1 ], [ 0, 0, 0, 0 ], [ 0, 0, 0, 1 ] ];
        _data.resultList[0].wheelIdx = 10;
        _data.resultList[0].wheelBonus = 0;
        _data.resultList[1] = new G27_CommonData_1.ResultData();
        _data.resultList[1].randomNumList = [ 22, 10, 5, 6, 1 ];
        _data.resultList[1].changeSymbol = 2;
        _data.resultList[1].changePos = [ [ 0, 0, 0, 0 ], [ 0, 1, 0, 0 ], [ 0, 1, 0, 1 ], [ 0, 0, 1, 0 ], [ 1, 0, 0, 1 ] ];
        _data.resultList[1].totalHitPos = [ [ 1, 1, 1, 1 ], [ 0, 1, 0, 0 ], [ 0, 1, 0, 1 ], [ 0, 0, 1, 0 ], [ 1, 1, 0, 1 ] ];
        _data.resultList[1].winWayList[0] = new G27_CommonData_1.WinWayData();
        _data.resultList[1].winWayList[0].singlePos = [ [ 1, 1, 1, 1 ], [ 0, 1, 0, 0 ], [ 0, 1, 0, 1 ], [ 0, 0, 1, 0 ], [ 1, 1, 0, 1 ] ];
        _data.resultList[1].winWayList[0].winBonus = p_betScore / 100 * 1e3 * 1 * 24;
        for (var i = 2; i <= 4; i++) {
          _data.resultList[i] = new G27_CommonData_1.ResultData();
          _data.resultList[i].randomNumList = [ 1, 6, 4, 1, 1 ];
          _data.resultList[i].totalHitPos = [ [ 1, 0, 0 ], [ 1, 0, 0 ], [ 1, 0, 0 ], [ 1, 1, 1 ], [ 1, 1, 1 ] ];
          _data.resultList[i].totalWinBonus = 0;
          _data.resultList[i].winWayList = [];
          _data.resultList[i].winWayList[0] = new G27_CommonData_1.WinWayData();
          _data.resultList[i].winWayList[0].singlePos = [ [ 1, 0, 0 ], [ 1, 0, 0 ], [ 1, 0, 0 ], [ 1, 1, 1 ], [ 1, 1, 1 ] ];
          _data.resultList[i].winWayList[0].winBonus = p_betScore / 100 * 8 * 1 * 9;
          _data.resultList[i].scatter = {
            winNum: 20,
            count: 1,
            winBonus: 0,
            scatterPos: [ [ 0, 0, 0 ], [ 0, 1, 0 ], [ 0, 0, 0 ], [ 0, 0, 0 ], [ 0, 0, 0 ] ]
          };
        }
        for (var i = 0; i < _data.resultList.length; i++) {
          for (var j = 0; j < _data.resultList[i].winWayList.length; j++) _data.resultList[i].totalWinBonus += _data.resultList[i].winWayList[j].winBonus;
          _data.totalBonus += _data.resultList[i].totalWinBonus;
        }
        _data.currentCash = 1e4 * this.data.coin - p_betScore + _data.totalBonus;
        return _data;
      };
      G27_TestSocket.prototype.testSpinData_9 = function(p_betScore) {
        var _data = new G27_CommonData_1.BaseGameSpinResponeData();
        _data.totalBet = p_betScore;
        _data.resultList[0] = new G27_CommonData_1.ResultData();
        _data.resultList[0].randomNumList = [ 2, 12, 1, 5, 1 ];
        _data.resultList[0].totalHitPos = [ [ 0, 0, 0, 1 ], [ 0, 0, 0, 1 ], [ 0, 0, 1, 0 ], [ 0, 0, 1, 0 ], [ 0, 0, 0, 0 ] ];
        _data.resultList[0].winWayList[0] = new G27_CommonData_1.WinWayData();
        _data.resultList[0].winWayList[0].singlePos = [ [ 0, 0, 0, 1 ], [ 0, 0, 0, 1 ], [ 0, 0, 1, 0 ], [ 0, 0, 1, 0 ], [ 0, 0, 0, 0 ] ];
        _data.resultList[0].winWayList[0].lineNum = 4;
        _data.resultList[0].winWayList[0].winNum = 12;
        _data.resultList[0].winWayList[0].winBonus = p_betScore / 100 * 1400;
        _data.resultList.forEach(function(result) {
          result.winWayList.forEach(function(winWay) {
            result.totalWinBonus += winWay.winBonus;
          });
          _data.totalBonus += result.totalWinBonus;
        });
        _data.currentCash = 1e4 * this.data.coin - p_betScore + _data.totalBonus;
        return _data;
      };
      G27_TestSocket.prototype.testSpinData_10 = function(p_betScore) {
        var _data = new G27_CommonData_1.BaseGameSpinResponeData();
        _data.totalBet = p_betScore;
        _data.resultList[0] = new G27_CommonData_1.ResultData();
        _data.resultList[0].randomNumList = [ 2, 2, 1, 5, 1 ];
        _data.resultList[0].totalHitPos = [ [ 0, 0, 1, 1 ], [ 0, 1, 1, 1 ], [ 0, 0, 1, 1 ], [ 0, 0, 1, 1 ], [ 0, 0, 0, 0 ] ];
        _data.resultList[0].winWayList[0] = new G27_CommonData_1.WinWayData();
        _data.resultList[0].winWayList[0].singlePos = [ [ 0, 0, 0, 1 ], [ 0, 1, 1, 0 ], [ 0, 0, 1, 0 ], [ 0, 0, 1, 0 ], [ 0, 0, 0, 0 ] ];
        _data.resultList[0].winWayList[0].lineNum = 5;
        _data.resultList[0].winWayList[0].winNum = 1;
        _data.resultList[0].winWayList[0].winBonus = p_betScore / 100 * 1400;
        _data.resultList[0].winWayList[1] = new G27_CommonData_1.WinWayData();
        _data.resultList[0].winWayList[1].singlePos = [ [ 0, 0, 1, 0 ], [ 0, 0, 0, 1 ], [ 0, 0, 0, 1 ], [ 0, 0, 0, 1 ], [ 0, 0, 0, 0 ] ];
        _data.resultList[0].winWayList[0].lineNum = 4;
        _data.resultList[0].winWayList[0].winNum = 12;
        _data.resultList[0].winWayList[1].winBonus = p_betScore / 100 * 1500;
        _data.resultList.forEach(function(result) {
          result.winWayList.forEach(function(winWay) {
            result.totalWinBonus += winWay.winBonus;
          });
          _data.totalBonus += result.totalWinBonus;
        });
        _data.currentCash = 1e4 * this.data.coin - p_betScore + _data.totalBonus;
        return _data;
      };
      G27_TestSocket.prototype.setOnLeaveEvent = function() {
        return new Promise(function(resolve) {
          resolve();
        });
      };
      var G27_TestSocket_1;
      G27_TestSocket.assignData = -1;
      G27_TestSocket = G27_TestSocket_1 = __decorate([ ccclass ], G27_TestSocket);
      return G27_TestSocket;
    }();
    exports.default = G27_TestSocket;
    cc._RF.pop();
  }, {
    "../../Game/G27_CommonData": "G27_CommonData",
    "../G27_DataManager": "G27_DataManager"
  } ],
  G27_TestUI: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "142ab0F0rdCJJ6onxFvoADD", "G27_TestUI");
    "use strict";
    var __extends = this && this.__extends || function() {
      var extendStatics = function(d, b) {
        extendStatics = Object.setPrototypeOf || {
          __proto__: []
        } instanceof Array && function(d, b) {
          d.__proto__ = b;
        } || function(d, b) {
          for (var p in b) Object.prototype.hasOwnProperty.call(b, p) && (d[p] = b[p]);
        };
        return extendStatics(d, b);
      };
      return function(d, b) {
        extendStatics(d, b);
        function __() {
          this.constructor = d;
        }
        d.prototype = null === b ? Object.create(b) : (__.prototype = b.prototype, new __());
      };
    }();
    var __decorate = this && this.__decorate || function(decorators, target, key, desc) {
      var c = arguments.length, r = c < 3 ? target : null === desc ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
      if ("object" === typeof Reflect && "function" === typeof Reflect.decorate) r = Reflect.decorate(decorators, target, key, desc); else for (var i = decorators.length - 1; i >= 0; i--) (d = decorators[i]) && (r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r);
      return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    var DesktopBrowserTransform_1 = require("../../Common/Tools/DesktopBrowserTransform/DesktopBrowserTransform");
    var G27_DataManager_1 = require("../Common/G27_DataManager");
    var G27_TestSocket_1 = require("../Common/Socket/G27_TestSocket");
    var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
    var G27_TestUI = function(_super) {
      __extends(G27_TestUI, _super);
      function G27_TestUI() {
        var _this = null !== _super && _super.apply(this, arguments) || this;
        _this.menuNode = null;
        _this.gameSpeedEditBox = null;
        return _this;
      }
      G27_TestUI.prototype.start = function() {
        if (G27_DataManager_1.default.offLineMode) {
          this.node.setPosition(cc.Vec2.ZERO);
          this.menuNode.active = false;
        } else this.node.active = false;
      };
      G27_TestUI.prototype.setTestData = function(event, customEventData) {
        console.log("\u4f7f\u7528\u6e2c\u8a66\u8cc7\u6599 " + customEventData);
        G27_TestSocket_1.default.assignData = Number(customEventData);
        this.closeMenu();
      };
      G27_TestUI.prototype.setGameSpeed = function() {
        DesktopBrowserTransform_1.default.getInstance().setGameSpeed(Number(this.gameSpeedEditBox.string));
      };
      G27_TestUI.prototype.showMenu = function() {
        this.menuNode.active = true;
      };
      G27_TestUI.prototype.closeMenu = function() {
        console.log("\u95dc\u9589\u6e2c\u8a66UI");
        this.menuNode.active = false;
      };
      __decorate([ property(cc.Node) ], G27_TestUI.prototype, "menuNode", void 0);
      __decorate([ property(cc.EditBox) ], G27_TestUI.prototype, "gameSpeedEditBox", void 0);
      G27_TestUI = __decorate([ ccclass ], G27_TestUI);
      return G27_TestUI;
    }(cc.Component);
    exports.default = G27_TestUI;
    cc._RF.pop();
  }, {
    "../../Common/Tools/DesktopBrowserTransform/DesktopBrowserTransform": void 0,
    "../Common/G27_DataManager": "G27_DataManager",
    "../Common/Socket/G27_TestSocket": "G27_TestSocket"
  } ],
  G27_WheelItem: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "7e833xA+BlBSZ5KTa3Etk++", "G27_WheelItem");
    "use strict";
    var __extends = this && this.__extends || function() {
      var extendStatics = function(d, b) {
        extendStatics = Object.setPrototypeOf || {
          __proto__: []
        } instanceof Array && function(d, b) {
          d.__proto__ = b;
        } || function(d, b) {
          for (var p in b) Object.prototype.hasOwnProperty.call(b, p) && (d[p] = b[p]);
        };
        return extendStatics(d, b);
      };
      return function(d, b) {
        extendStatics(d, b);
        function __() {
          this.constructor = d;
        }
        d.prototype = null === b ? Object.create(b) : (__.prototype = b.prototype, new __());
      };
    }();
    var __decorate = this && this.__decorate || function(decorators, target, key, desc) {
      var c = arguments.length, r = c < 3 ? target : null === desc ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
      if ("object" === typeof Reflect && "function" === typeof Reflect.decorate) r = Reflect.decorate(decorators, target, key, desc); else for (var i = decorators.length - 1; i >= 0; i--) (d = decorators[i]) && (r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r);
      return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    var AudioManager_1 = require("../../../Common/Tools/AudioManager/AudioManager");
    var CommonTool_1 = require("../../../Common/Tools/CommonTool");
    var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
    var G27_WheelItem = function(_super) {
      __extends(G27_WheelItem, _super);
      function G27_WheelItem() {
        var _this = null !== _super && _super.apply(this, arguments) || this;
        _this.iconFG = null;
        _this.score = null;
        _this.fxStart = null;
        _this.index = 0;
        return _this;
      }
      G27_WheelItem.prototype.setFXStart = function(isActive) {
        if (this.fxStart.active === isActive) return;
        this.fxStart.active = isActive;
        isActive ? this.fxStart.getComponentInChildren(cc.Animation).play() : this.fxStart.getComponentInChildren(cc.Animation).stop();
        isActive && AudioManager_1.AudioManager.instance.playAudioEvent("PlayWheelItemFX");
      };
      G27_WheelItem.prototype.setItem = function(isFG, index, score) {
        this.index = index;
        this.iconFG.active = isFG;
        this.score.string = -1 === score ? "" : CommonTool_1.CommonTool.getNumText(score, 2, true, false);
      };
      G27_WheelItem.prototype.getItemIndex = function() {
        return this.index;
      };
      __decorate([ property(cc.Node) ], G27_WheelItem.prototype, "iconFG", void 0);
      __decorate([ property(cc.Label) ], G27_WheelItem.prototype, "score", void 0);
      __decorate([ property(cc.Node) ], G27_WheelItem.prototype, "fxStart", void 0);
      G27_WheelItem = __decorate([ ccclass ], G27_WheelItem);
      return G27_WheelItem;
    }(cc.Component);
    exports.default = G27_WheelItem;
    cc._RF.pop();
  }, {
    "../../../Common/Tools/AudioManager/AudioManager": void 0,
    "../../../Common/Tools/CommonTool": void 0
  } ],
  G27_Wheel: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "231b3mexOhCCa9bQW8uEyIS", "G27_Wheel");
    "use strict";
    var __extends = this && this.__extends || function() {
      var extendStatics = function(d, b) {
        extendStatics = Object.setPrototypeOf || {
          __proto__: []
        } instanceof Array && function(d, b) {
          d.__proto__ = b;
        } || function(d, b) {
          for (var p in b) Object.prototype.hasOwnProperty.call(b, p) && (d[p] = b[p]);
        };
        return extendStatics(d, b);
      };
      return function(d, b) {
        extendStatics(d, b);
        function __() {
          this.constructor = d;
        }
        d.prototype = null === b ? Object.create(b) : (__.prototype = b.prototype, new __());
      };
    }();
    var __decorate = this && this.__decorate || function(decorators, target, key, desc) {
      var c = arguments.length, r = c < 3 ? target : null === desc ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
      if ("object" === typeof Reflect && "function" === typeof Reflect.decorate) r = Reflect.decorate(decorators, target, key, desc); else for (var i = decorators.length - 1; i >= 0; i--) (d = decorators[i]) && (r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r);
      return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __awaiter = this && this.__awaiter || function(thisArg, _arguments, P, generator) {
      function adopt(value) {
        return value instanceof P ? value : new P(function(resolve) {
          resolve(value);
        });
      }
      return new (P || (P = Promise))(function(resolve, reject) {
        function fulfilled(value) {
          try {
            step(generator.next(value));
          } catch (e) {
            reject(e);
          }
        }
        function rejected(value) {
          try {
            step(generator["throw"](value));
          } catch (e) {
            reject(e);
          }
        }
        function step(result) {
          result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected);
        }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
      });
    };
    var __generator = this && this.__generator || function(thisArg, body) {
      var _ = {
        label: 0,
        sent: function() {
          if (1 & t[0]) throw t[1];
          return t[1];
        },
        trys: [],
        ops: []
      }, f, y, t, g;
      return g = {
        next: verb(0),
        throw: verb(1),
        return: verb(2)
      }, "function" === typeof Symbol && (g[Symbol.iterator] = function() {
        return this;
      }), g;
      function verb(n) {
        return function(v) {
          return step([ n, v ]);
        };
      }
      function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
          if (f = 1, y && (t = 2 & op[0] ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 
          0) : y.next) && !(t = t.call(y, op[1])).done) return t;
          (y = 0, t) && (op = [ 2 & op[0], t.value ]);
          switch (op[0]) {
           case 0:
           case 1:
            t = op;
            break;

           case 4:
            _.label++;
            return {
              value: op[1],
              done: false
            };

           case 5:
            _.label++;
            y = op[1];
            op = [ 0 ];
            continue;

           case 7:
            op = _.ops.pop();
            _.trys.pop();
            continue;

           default:
            if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (6 === op[0] || 2 === op[0])) {
              _ = 0;
              continue;
            }
            if (3 === op[0] && (!t || op[1] > t[0] && op[1] < t[3])) {
              _.label = op[1];
              break;
            }
            if (6 === op[0] && _.label < t[1]) {
              _.label = t[1];
              t = op;
              break;
            }
            if (t && _.label < t[2]) {
              _.label = t[2];
              _.ops.push(op);
              break;
            }
            t[2] && _.ops.pop();
            _.trys.pop();
            continue;
          }
          op = body.call(thisArg, _);
        } catch (e) {
          op = [ 6, e ];
          y = 0;
        } finally {
          f = t = 0;
        }
        if (5 & op[0]) throw op[1];
        return {
          value: op[0] ? op[1] : void 0,
          done: true
        };
      }
    };
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    var AudioManager_1 = require("../../../Common/Tools/AudioManager/AudioManager");
    var CommonTool_1 = require("../../../Common/Tools/CommonTool");
    var LocalizationManager_1 = require("../../../Common/Tools/Localization/LocalizationManager");
    var G27_WheelItem_1 = require("./G27_WheelItem");
    var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
    var STATUS;
    (function(STATUS) {
      STATUS[STATUS["IDLE"] = 0] = "IDLE";
      STATUS[STATUS["SCROLLING"] = 1] = "SCROLLING";
      STATUS[STATUS["SLOWING"] = 2] = "SLOWING";
      STATUS[STATUS["REWARD_EFFECTING"] = 3] = "REWARD_EFFECTING";
      STATUS[STATUS["REWARD_END"] = 4] = "REWARD_END";
    })(STATUS || (STATUS = {}));
    var G27_Wheel = function(_super) {
      __extends(G27_Wheel, _super);
      function G27_Wheel() {
        var _this = null !== _super && _super.apply(this, arguments) || this;
        _this.wheelItem = null;
        _this.wheelItemAngle = 18;
        _this.resetAngle = 126;
        _this.scrollTime = 3;
        _this.scrollSpeed = 300;
        _this.luckyWheelTitle = null;
        _this.luckyWheelBlock = null;
        _this.wheelNode = null;
        _this.fxLuckyWheelHand = null;
        _this.fxLuckyWheel = null;
        _this.fxLuckyWheelBoard = null;
        _this.wordNode = null;
        _this.getMoneyNode = null;
        _this.getFreegameNode = null;
        _this.fxLuckyWheelBoardMoney = null;
        _this.fxLuckyWheelBoardTimes = null;
        _this.wheelRatioList = [];
        _this.status = STATUS.IDLE;
        _this.wheelList = [];
        _this.wheelItemCount = 0;
        _this.wheelCenter = 0;
        _this.curScrollTime = 0;
        _this.maxScrollTime = 0;
        _this.curScrollSpeed = 0;
        _this.curNumIndex = 0;
        _this.curAngle = 0;
        _this.targetReward = 0;
        _this.betScore = 0;
        _this.startAngle = 0;
        _this.totalAngle = 0;
        _this.resultNode = null;
        _this.startFX = false;
        return _this;
      }
      G27_Wheel.prototype.init = function() {
        this.wheelList.push(this.wheelItem.getComponent(G27_WheelItem_1.default));
        for (var i = 1; i * this.wheelItemAngle < this.resetAngle; i++) {
          var _wheelItemNode = cc.instantiate(this.wheelItem);
          var _wheelItem = _wheelItemNode.getComponent(G27_WheelItem_1.default);
          _wheelItem.node.angle = i * this.wheelItemAngle;
          _wheelItem.node.position = this.wheelItem.position;
          this.wheelList.push(_wheelItem);
          _wheelItemNode.parent = this.wheelItem.parent;
        }
        this.wheelItemCount = this.wheelList.length;
        this.wheelCenter = Math.floor(this.wheelItemCount / 2) * this.wheelItemAngle;
        this.fxLuckyWheelHand.active = false;
        this.fxLuckyWheelBoard.active = false;
      };
      G27_Wheel.prototype.update = function(dt) {
        switch (this.status) {
         case STATUS.SCROLLING:
          this.scrolling(dt);
          break;

         case STATUS.SLOWING:
          this.slowing(dt);
        }
      };
      G27_Wheel.prototype.setWheelRatio = function(ratio) {
        this.wheelRatioList = ratio;
      };
      G27_Wheel.prototype.setData = function(targetIndex) {
        this.targetReward = targetIndex - 1;
      };
      G27_Wheel.prototype.scroll = function(betScore) {
        this.status = STATUS.IDLE;
        this.curScrollTime = 0;
        this.maxScrollTime = this.scrollTime + 2;
        this.curNumIndex = this.wheelRatioList.length - Math.ceil(this.wheelItemCount / 2) - 1;
        this.curScrollSpeed = 0;
        this.betScore = betScore;
        this.fxLuckyWheel.active = false;
        this.fxLuckyWheelBoard.active = false;
        this.startFX = false;
        this.getFreegameNode.active = false;
        this.getMoneyNode.active = false;
        this.setWheelItem();
        this.prepare();
      };
      G27_Wheel.prototype.setWheelItem = function() {
        var _this = this;
        this.wheelList.forEach(function(item, index) {
          item.setFXStart(false);
          item.node.angle = (_this.wheelList.length - index - 1) * _this.wheelItemAngle;
          _this.resetItem(item);
        });
      };
      G27_Wheel.prototype.prepare = function() {
        var _this = this;
        AudioManager_1.AudioManager.instance.playAudioEvent("StartLuckyWheel");
        this.wheelNode.position = cc.v3(0, -700);
        this.wheelNode.active = true;
        cc.tween(this.wheelNode).to(.5, {
          position: cc.v3(0, 0)
        }).start();
        cc.tween(this.luckyWheelTitle).to(.5, {
          opacity: 255
        }).start();
        cc.tween(this.luckyWheelBlock).to(.5, {
          opacity: 255
        }).start();
        this.fxLuckyWheelHand.position = cc.v3(0, 0);
        this.fxLuckyWheelHand.active = true;
        this.fxLuckyWheelHand.getComponentInChildren(sp.Skeleton).setAnimation(0, "play_HitAll", false);
        cc.tween(this.fxLuckyWheelHand).delay(1.8).call(function() {
          _this.curScrollSpeed = _this.scrollSpeed / 3;
          _this.status = STATUS.SCROLLING;
        }).delay(1.2).call(function() {
          _this.curScrollSpeed += _this.scrollSpeed / 3 * 2;
          _this.startFX = true;
        }).delay(.5).to(.5, {
          position: cc.v3(-300, 0)
        }).call(function() {
          _this.fxLuckyWheelHand.active = false;
        }).start();
      };
      G27_Wheel.prototype.scrolling = function(dt) {
        var _this = this;
        var arrResetItem = [];
        this.curScrollTime += dt;
        if (this.curScrollTime > this.scrollTime && this.curNumIndex === this.targetReward) {
          this.resultNode = this.wheelList.find(function(item) {
            return item.getItemIndex() === _this.targetReward;
          });
          this.startAngle = this.curAngle = -this.wheelRatioList.length * this.wheelItemAngle + this.resultNode.node.angle;
          this.totalAngle = this.wheelCenter - this.curAngle;
          this.status = STATUS.SLOWING;
        } else {
          this.curScrollTime > this.maxScrollTime && (this.curScrollSpeed -= .05 * this.scrollSpeed);
          this.wheelList.forEach(function(item) {
            item.node.angle += dt * _this.curScrollSpeed;
            if (_this.needResetItem(item.node)) {
              arrResetItem.push(item);
              item.node.angle -= _this.resetAngle;
            }
            _this.startFX && item.setFXStart(_this.isCenter(item.node));
          });
          arrResetItem.sort(function(a, b) {
            return b.node.angle - a.node.angle;
          });
          arrResetItem.forEach(function(item) {
            _this.resetItem(item);
          });
        }
      };
      G27_Wheel.prototype.slowing = function(dt) {
        var _this = this;
        if (this.curAngle < this.wheelCenter) {
          var changeAngle_1 = this.curAngle + dt * this.curScrollSpeed > this.wheelCenter ? this.wheelCenter - this.curAngle : dt * this.curScrollSpeed;
          this.wheelList.forEach(function(item) {
            item.node.angle += changeAngle_1;
            if (_this.needResetItem(item.node)) {
              item.node.angle -= _this.resetAngle;
              _this.resetItem(item);
            }
            item.setFXStart(_this.isCenter(item.node));
          });
          this.curAngle += changeAngle_1;
          this.curScrollSpeed = this.scrollSpeed - this.scrollSpeed * (this.curAngle - this.startAngle) / this.totalAngle;
          this.curScrollSpeed < 5 && (this.curScrollSpeed = 5);
        } else {
          this.status = STATUS.REWARD_EFFECTING;
          this.rewardEffecting();
        }
      };
      G27_Wheel.prototype.rewardEffecting = function() {
        return __awaiter(this, void 0, Promise, function() {
          var _this = this;
          return __generator(this, function(_a) {
            switch (_a.label) {
             case 0:
              AudioManager_1.AudioManager.instance.playAudioEvent("LuckyWheelReward");
              return [ 4, CommonTool_1.CommonTool.toNextTick() ];

             case 1:
              _a.sent();
              this.wordNode.opacity = 0;
              this.fxLuckyWheelBoard.position = cc.v3(0, 0);
              cc.tween(this.luckyWheelTitle).to(1, {
                opacity: 0
              }).start();
              this.fxLuckyWheel.getComponentInChildren(cc.Animation).once("finished", function() {
                if (_this.targetReward === _this.wheelRatioList.length - 1) {
                  _this.getFreegameNode.active = true;
                  _this.fxLuckyWheelBoardTimes.string = LocalizationManager_1.default.getInstance().get("TEXT_SLOT_FREEGAME_WORD_3", 10);
                } else {
                  _this.getMoneyNode.active = true;
                  _this.fxLuckyWheelBoardMoney.string = CommonTool_1.CommonTool.getNumText(_this.wheelRatioList[_this.targetReward] * _this.betScore, 2, true, false);
                }
                _this.fxLuckyWheelBoard.active = true;
                _this.fxLuckyWheelBoard.getComponentInChildren(sp.Skeleton).setAnimation(0, "animation", false);
                _this.fxLuckyWheelBoard.getComponentInChildren(cc.ParticleSystem3D).stop();
                _this.scheduleOnce(function() {
                  return _this.fxLuckyWheelBoard.getComponentInChildren(cc.ParticleSystem3D).play();
                });
                cc.tween(_this.wordNode).to(.5, {
                  opacity: 255
                }).start();
                _this.fxLuckyWheelBoard.getComponentInChildren(sp.Skeleton).setCompleteListener(function() {
                  setTimeout(function() {
                    cc.tween(_this.luckyWheelBlock).to(.5, {
                      opacity: 0
                    }).start();
                    cc.tween(_this.wordNode).to(.5, {
                      opacity: 0
                    }).start();
                    cc.tween(_this.fxLuckyWheelBoard).to(.25, {
                      position: cc.v3(0, 550)
                    }).start();
                    cc.tween(_this.wheelNode).to(.25, {
                      position: cc.v3(0, -700)
                    }).start();
                  }, 1e3);
                  setTimeout(function() {
                    _this.status = STATUS.REWARD_END;
                  }, 1700);
                });
              });
              this.fxLuckyWheel.active = true;
              this.scheduleOnce(function() {
                return _this.fxLuckyWheel.getComponentInChildren(cc.Animation).play();
              });
              return [ 2 ];
            }
          });
        });
      };
      G27_Wheel.prototype.checkRewardEnd = function() {
        return this.status === STATUS.REWARD_END;
      };
      G27_Wheel.prototype.isCenter = function(node) {
        return node.angle > this.wheelCenter - this.wheelItemAngle / 2 && node.angle < this.wheelCenter + this.wheelItemAngle / 2;
      };
      G27_Wheel.prototype.needResetItem = function(node) {
        return node.angle > this.resetAngle;
      };
      G27_Wheel.prototype.resetItem = function(node) {
        this.curNumIndex = (this.curNumIndex + 1) % this.wheelRatioList.length;
        this.curNumIndex === this.wheelRatioList.length - 1 ? node.setItem(true, this.curNumIndex, -1) : node.setItem(false, this.curNumIndex, this.wheelRatioList[this.curNumIndex % this.wheelRatioList.length] * this.betScore);
      };
      __decorate([ property(cc.Node) ], G27_Wheel.prototype, "wheelItem", void 0);
      __decorate([ property({
        displayName: "\u4e00\u500b\u7269\u4ef6\u5360\u7528\u89d2\u5ea6",
        type: cc.Float
      }) ], G27_Wheel.prototype, "wheelItemAngle", void 0);
      __decorate([ property({
        displayName: "\u8d85\u904e\u67d0\u500b\u89d2\u5ea6\u9700\u8981\u91cd\u88fd\u4f4d\u7f6e",
        type: cc.Float
      }) ], G27_Wheel.prototype, "resetAngle", void 0);
      __decorate([ property({
        displayName: "\u8f49\u52d5\u6642\u9593",
        type: cc.Float
      }) ], G27_Wheel.prototype, "scrollTime", void 0);
      __decorate([ property({
        displayName: "\u8f49\u52d5\u901f\u5ea6",
        type: cc.Float
      }) ], G27_Wheel.prototype, "scrollSpeed", void 0);
      __decorate([ property(cc.Node) ], G27_Wheel.prototype, "luckyWheelTitle", void 0);
      __decorate([ property(cc.Node) ], G27_Wheel.prototype, "luckyWheelBlock", void 0);
      __decorate([ property(cc.Node) ], G27_Wheel.prototype, "wheelNode", void 0);
      __decorate([ property(cc.Node) ], G27_Wheel.prototype, "fxLuckyWheelHand", void 0);
      __decorate([ property(cc.Node) ], G27_Wheel.prototype, "fxLuckyWheel", void 0);
      __decorate([ property(cc.Node) ], G27_Wheel.prototype, "fxLuckyWheelBoard", void 0);
      __decorate([ property(cc.Node) ], G27_Wheel.prototype, "wordNode", void 0);
      __decorate([ property(cc.Node) ], G27_Wheel.prototype, "getMoneyNode", void 0);
      __decorate([ property(cc.Node) ], G27_Wheel.prototype, "getFreegameNode", void 0);
      __decorate([ property(cc.Label) ], G27_Wheel.prototype, "fxLuckyWheelBoardMoney", void 0);
      __decorate([ property(cc.RichText) ], G27_Wheel.prototype, "fxLuckyWheelBoardTimes", void 0);
      G27_Wheel = __decorate([ ccclass ], G27_Wheel);
      return G27_Wheel;
    }(cc.Component);
    exports.default = G27_Wheel;
    cc._RF.pop();
  }, {
    "../../../Common/Tools/AudioManager/AudioManager": void 0,
    "../../../Common/Tools/CommonTool": void 0,
    "../../../Common/Tools/Localization/LocalizationManager": void 0,
    "./G27_WheelItem": "G27_WheelItem"
  } ],
  G27_WinScorePanel: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "ecad53XPHlB8If+laler+gb", "G27_WinScorePanel");
    "use strict";
    var __extends = this && this.__extends || function() {
      var extendStatics = function(d, b) {
        extendStatics = Object.setPrototypeOf || {
          __proto__: []
        } instanceof Array && function(d, b) {
          d.__proto__ = b;
        } || function(d, b) {
          for (var p in b) Object.prototype.hasOwnProperty.call(b, p) && (d[p] = b[p]);
        };
        return extendStatics(d, b);
      };
      return function(d, b) {
        extendStatics(d, b);
        function __() {
          this.constructor = d;
        }
        d.prototype = null === b ? Object.create(b) : (__.prototype = b.prototype, new __());
      };
    }();
    var __decorate = this && this.__decorate || function(decorators, target, key, desc) {
      var c = arguments.length, r = c < 3 ? target : null === desc ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
      if ("object" === typeof Reflect && "function" === typeof Reflect.decorate) r = Reflect.decorate(decorators, target, key, desc); else for (var i = decorators.length - 1; i >= 0; i--) (d = decorators[i]) && (r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r);
      return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    var AudioManager_1 = require("../../../Common/Tools/AudioManager/AudioManager");
    var Slot_WinScorePanel_1 = require("../../../SlotFramework/Game/Panel/Slot_WinScorePanel");
    var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
    var G27_WinScorePanel = function(_super) {
      __extends(G27_WinScorePanel, _super);
      function G27_WinScorePanel() {
        var _this = null !== _super && _super.apply(this, arguments) || this;
        _this.winText = null;
        _this.bigWinText = null;
        _this.superWinText = null;
        _this.megaWinText = null;
        _this.winEffect = null;
        _this.winStart = null;
        _this.bigWinEffect = null;
        _this.superWinEffect = null;
        _this.megaWinEffect = null;
        return _this;
      }
      G27_WinScorePanel.prototype.open = function(p_totalWinScore, p_betScore, p_isFreeGame) {
        void 0 === p_isFreeGame && (p_isFreeGame = false);
        _super.prototype.open.call(this, p_totalWinScore, p_betScore, p_isFreeGame);
        this.node.opacity = 255;
      };
      G27_WinScorePanel.prototype.closeAllWinText = function() {
        this.winText.active = false;
        this.bigWinText.active = false;
        this.superWinText.active = false;
        this.megaWinText.active = false;
      };
      G27_WinScorePanel.prototype.closeAllWinEffect = function() {
        this.winEffect.active = false;
        this.bigWinEffect.active = false;
        this.superWinEffect.active = false;
        this.megaWinEffect.active = false;
        this.winStart.active = false;
      };
      G27_WinScorePanel.prototype.showBigWinEffect = function() {
        var _this = this;
        console.log("\u3010Show Big Win\u3011 ");
        _super.prototype.showBigWinEffect.call(this);
        this.closeAllWinText();
        this.closeAllWinEffect();
        this.bigWinEffect.active = true;
        this.bigWinText.active = true;
        this.bigWinText.getComponentInChildren(sp.Skeleton).setAnimation(0, "BigWin_Start", false);
        this.bigWinText.getComponentInChildren(sp.Skeleton).setCompleteListener(function() {
          _this.bigWinText.getComponentInChildren(sp.Skeleton).setCompleteListener(null);
          _this.bigWinText.getComponentInChildren(sp.Skeleton).setAnimation(0, "BigWin_Loop", true);
        });
        this.playWinStart();
        AudioManager_1.AudioManager.instance.playAudioEvent("WinScorePanelBigWinEffect");
      };
      G27_WinScorePanel.prototype.showSuperWinEffect = function() {
        var _this = this;
        console.log("\u3010Show Super Win\u3011 ");
        _super.prototype.showSuperWinEffect.call(this);
        this.closeAllWinText();
        this.superWinEffect.active = true;
        this.superWinText.active = true;
        this.superWinText.getComponentInChildren(sp.Skeleton).setAnimation(0, "SuperWin_Start", false);
        this.superWinText.getComponentInChildren(sp.Skeleton).setCompleteListener(function() {
          _this.superWinText.getComponentInChildren(sp.Skeleton).setCompleteListener(null);
          _this.superWinText.getComponentInChildren(sp.Skeleton).setAnimation(0, "SuperWin_Loop", true);
        });
        this.playWinStart();
        AudioManager_1.AudioManager.instance.playAudioEvent("WinScorePanelSuperWinEffect");
      };
      G27_WinScorePanel.prototype.showMegaWinEffect = function() {
        var _this = this;
        console.log("\u3010Mega Win\u3011");
        _super.prototype.showMegaWinEffect.call(this);
        this.closeAllWinText();
        this.megaWinEffect.active = true;
        this.megaWinText.active = true;
        this.megaWinText.getComponentInChildren(sp.Skeleton).setAnimation(0, "MegaWin_Start", false);
        this.megaWinText.getComponentInChildren(sp.Skeleton).setCompleteListener(function() {
          _this.megaWinText.getComponentInChildren(sp.Skeleton).setCompleteListener(null);
          _this.megaWinText.getComponentInChildren(sp.Skeleton).setAnimation(0, "MegaWin_Loop", true);
        });
        this.playWinStart();
        AudioManager_1.AudioManager.instance.playAudioEvent("WinScorePanelMegaWinEffect");
      };
      G27_WinScorePanel.prototype.playWinStart = function() {
        var _this = this;
        this.winStart.active = true;
        this.winStart.getComponentsInChildren(cc.ParticleSystem3D).forEach(function(item) {
          item.stop();
          _this.scheduleOnce(function() {
            return item.play();
          });
        });
      };
      G27_WinScorePanel.prototype.close = function() {
        var _this = this;
        this.node.getComponentInChildren(cc.Button).interactable = false;
        cc.tween(this.node).to(.25, {
          opacity: 0
        }).call(function() {
          _this.node.active = false;
          _this.runScore.resetToZero();
          _this.node.opacity = 255;
          _this.node.getComponentInChildren(cc.Button).interactable = true;
        }).start();
      };
      __decorate([ property(cc.Node) ], G27_WinScorePanel.prototype, "winText", void 0);
      __decorate([ property(cc.Node) ], G27_WinScorePanel.prototype, "bigWinText", void 0);
      __decorate([ property(cc.Node) ], G27_WinScorePanel.prototype, "superWinText", void 0);
      __decorate([ property(cc.Node) ], G27_WinScorePanel.prototype, "megaWinText", void 0);
      __decorate([ property(cc.Node) ], G27_WinScorePanel.prototype, "winEffect", void 0);
      __decorate([ property(cc.Node) ], G27_WinScorePanel.prototype, "winStart", void 0);
      __decorate([ property(cc.Node) ], G27_WinScorePanel.prototype, "bigWinEffect", void 0);
      __decorate([ property(cc.Node) ], G27_WinScorePanel.prototype, "superWinEffect", void 0);
      __decorate([ property(cc.Node) ], G27_WinScorePanel.prototype, "megaWinEffect", void 0);
      G27_WinScorePanel = __decorate([ ccclass ], G27_WinScorePanel);
      return G27_WinScorePanel;
    }(Slot_WinScorePanel_1.default);
    exports.default = G27_WinScorePanel;
    cc._RF.pop();
  }, {
    "../../../Common/Tools/AudioManager/AudioManager": void 0,
    "../../../SlotFramework/Game/Panel/Slot_WinScorePanel": void 0
  } ]
}, {}, [ "G27_DataManager", "G27_DynamicPopUpPanelManager", "G27_LoadingItem", "G27_SocketConnect", "G27_SocketManager", "G27_TestSocket", "G27_CommonData", "G27_Game", "G27_TestUI", "G27_AutoGamePanel", "G27_FXWarning", "G27_FreeGameGetScorePanel", "G27_FreeGamePanel", "G27_GameUI", "G27_Line", "G27_MusicOptionPanel", "G27_Reel", "G27_RulePanel", "G27_SlotLineManager", "G27_SlotReelManager", "G27_Symbol", "G27_SymbolTipPanel", "G27_Wheel", "G27_WheelItem", "G27_WinScorePanel", "G27_Loading_InitData", "G27_Loading", "G27_LoadingUI", "G27_Lobby", "G27_LobbyUI", "G27_Language" ]);