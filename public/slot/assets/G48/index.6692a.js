window.__require = function e(t, n, r) {
  function s(o, u) {
    if (!n[o]) {
      if (!t[o]) {
        var b = o.split("/");
        b = b[b.length - 1];
        if (!t[b]) {
          var a = "function" == typeof __require && __require;
          if (!u && a) return a(b, !0);
          if (i) return i(b, !0);
          throw new Error("Cannot find module '" + o + "'");
        }
        o = b;
      }
      var f = n[o] = {
        exports: {}
      };
      t[o][0].call(f.exports, function(e) {
        var n = t[o][1][e];
        return s(n || e);
      }, f, f.exports, e, t, n, r);
    }
    return n[o].exports;
  }
  var i = "function" == typeof __require && __require;
  for (var o = 0; o < r.length; o++) s(r[o]);
  return s;
}({
  G48_AutoGamePanel: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "7833aCHNidHbogspdxIyTHC", "G48_AutoGamePanel");
    "use strict";
    var __extends = this && this.__extends || function() {
      var extendStatics = function(d, b) {
        extendStatics = Object.setPrototypeOf || {
          __proto__: []
        } instanceof Array && function(d, b) {
          d.__proto__ = b;
        } || function(d, b) {
          for (var p in b) Object.prototype.hasOwnProperty.call(b, p) && (d[p] = b[p]);
        };
        return extendStatics(d, b);
      };
      return function(d, b) {
        extendStatics(d, b);
        function __() {
          this.constructor = d;
        }
        d.prototype = null === b ? Object.create(b) : (__.prototype = b.prototype, new __());
      };
    }();
    var __decorate = this && this.__decorate || function(decorators, target, key, desc) {
      var c = arguments.length, r = c < 3 ? target : null === desc ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
      if ("object" === typeof Reflect && "function" === typeof Reflect.decorate) r = Reflect.decorate(decorators, target, key, desc); else for (var i = decorators.length - 1; i >= 0; i--) (d = decorators[i]) && (r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r);
      return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    var CommonTool_1 = require("../../../Common/Tools/CommonTool");
    var LocalizationManager_1 = require("../../../Common/Tools/Localization/LocalizationManager");
    var Slot_AutoGamePanel_1 = require("../../../SlotFramework/Game/Panel/Slot_AutoGamePanel");
    var G48_DataManager_1 = require("../../Common/G48_DataManager");
    var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
    var G48_AutoGamePanel = function(_super) {
      __extends(G48_AutoGamePanel, _super);
      function G48_AutoGamePanel() {
        var _this = null !== _super && _super.apply(this, arguments) || this;
        _this.title = null;
        _this.data = null;
        return _this;
      }
      G48_AutoGamePanel.prototype.onLoad = function() {
        this.title.node.setScale("en-us" == LocalizationManager_1.default.language ? .8 : 1);
      };
      G48_AutoGamePanel.prototype.init = function() {
        this.data = G48_DataManager_1.default.getInstance();
        _super.prototype.init.call(this);
      };
      G48_AutoGamePanel.prototype.setBetMoney = function() {
        var _betMoney = this.data.getClientMoney(this.data.betOdds[this.data.betOddsStartIndex] * this.data.oneOddsScore * this.data.drills[this.data.drillIndex]);
        this.betScore.string = CommonTool_1.CommonTool.getNumText(_betMoney, 2);
      };
      G48_AutoGamePanel.prototype.changeScoreLessStopLabel = function(slider) {
        var _index = Math.round(51 * slider.progress);
        var _value = 0;
        if (0 == _index) this.scoreLessStopLabel.string = LocalizationManager_1.default.getInstance().get("TEXT_NO"); else {
          if (this.spinCountToggles[Slot_AutoGamePanel_1.SPIN_ARRAY.UNLIMITED].isChecked) {
            var _hasMoney = this.data.coin;
            _index = Math.round(50 * slider.progress);
            _value = _hasMoney * (this.moneyLessStopSpacing[_index] + 1) / 50;
          } else {
            var _betMoney = this.data.getClientMoney(this.data.betOdds[this.data.betOddsStartIndex] * this.data.oneOddsScore * this.data.drills[this.data.drillIndex]);
            var _min = _betMoney * this.data.autoGameSpinCount / 2;
            _value = _min + this.data.autoGameSpinCount * _betMoney * this.moneyLessStopSpacing[_index] * .01;
          }
          _value = Number(_value.toFixed(2));
          this.scoreLessStopLabel.string = CommonTool_1.CommonTool.getNumText(_value);
        }
        this.data.autoGameScoreLessStop = _value;
      };
      G48_AutoGamePanel.prototype.changeWinScoreStopLabel = function(slider) {
        var _index = Math.round(50 * slider.progress);
        var _betMoney = this.data.getClientMoney(this.data.betOdds[this.data.betOddsStartIndex] * this.data.oneOddsScore * this.data.drills[this.data.drillIndex]);
        _betMoney *= this.winMoneyStopSpacing[_index];
        this.winScoreStopLabel.string = 0 == _index ? LocalizationManager_1.default.getInstance().get("TEXT_NO") : CommonTool_1.CommonTool.getNumText(_betMoney);
        this.data.autoGameWinScoreStop = _betMoney;
      };
      G48_AutoGamePanel.prototype.getCanSpinCount = function() {
        var _hasMoney = this.data.coin;
        var _betMoney = this.data.getClientMoney(this.data.betOdds[this.data.betOddsStartIndex] * this.data.oneOddsScore * this.data.drills[this.data.drillIndex]);
        var _canSpinCount = _hasMoney / _betMoney;
        return Math.floor(_canSpinCount);
      };
      __decorate([ property(cc.Sprite) ], G48_AutoGamePanel.prototype, "title", void 0);
      G48_AutoGamePanel = __decorate([ ccclass ], G48_AutoGamePanel);
      return G48_AutoGamePanel;
    }(Slot_AutoGamePanel_1.default);
    exports.default = G48_AutoGamePanel;
    cc._RF.pop();
  }, {
    "../../../Common/Tools/CommonTool": void 0,
    "../../../Common/Tools/Localization/LocalizationManager": void 0,
    "../../../SlotFramework/Game/Panel/Slot_AutoGamePanel": void 0,
    "../../Common/G48_DataManager": "G48_DataManager"
  } ],
  G48_CommonData: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "ce5dalvdKdCeZiI/jsBBRWX", "G48_CommonData");
    "use strict";
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    exports.HistoryData = exports.ScatterData = exports.WinJoinData = exports.EliminateData = exports.dragonBallData = exports.jackpotBonusData = exports.ResultData = exports.BaseGameSpinResponeData = exports.ReelTable = exports.GetInitialReelInfoData = exports.FastChoseRoomData = exports.RoomData = exports.GetRoomListData = exports.LoginData = void 0;
    var LoginData = function() {
      function LoginData() {
        this.playerId = "";
        this.nickname = "";
        this.language = "";
        this.timestamp = 0;
        this.balance = 0;
        this.gameId = 0;
        this.icon = 0;
      }
      return LoginData;
    }();
    exports.LoginData = LoginData;
    var GetRoomListData = function() {
      function GetRoomListData() {
        this.rooms = [];
      }
      return GetRoomListData;
    }();
    exports.GetRoomListData = GetRoomListData;
    var RoomData = function() {
      function RoomData() {
        this.roomId = "";
        this.status = 0;
      }
      return RoomData;
    }();
    exports.RoomData = RoomData;
    var FastChoseRoomData = function() {
      function FastChoseRoomData() {}
      return FastChoseRoomData;
    }();
    exports.FastChoseRoomData = FastChoseRoomData;
    var GetInitialReelInfoData = function() {
      function GetInitialReelInfoData() {
        this.reels = new ReelTable();
        this.position = [];
      }
      return GetInitialReelInfoData;
    }();
    exports.GetInitialReelInfoData = GetInitialReelInfoData;
    var ReelTable = function() {
      function ReelTable() {
        this.BG = [];
      }
      return ReelTable;
    }();
    exports.ReelTable = ReelTable;
    var BaseGameSpinResponeData = function() {
      function BaseGameSpinResponeData() {
        this.totalBonus = 0;
        this.totalBet = 0;
        this.allReels = [];
        this.resultList = [];
        this.currentCash = 0;
        this.getJackpot = false;
      }
      return BaseGameSpinResponeData;
    }();
    exports.BaseGameSpinResponeData = BaseGameSpinResponeData;
    var ResultData = function() {
      function ResultData() {
        this.totalWinBonus = 0;
        this.randomNumList = [ 0, 0, 0, 0, 0, 0 ];
        this.reels = [];
        this.reels_Above = [];
        this.eliminateList = [];
        this.targetList = [];
        this.dragonBall = new dragonBallData();
        this.getScatterNums = 0;
        this.jackpotBonus = new jackpotBonusData();
        this.nextScatterBetList = [];
        this.nextScatterNums = 0;
      }
      return ResultData;
    }();
    exports.ResultData = ResultData;
    var jackpotBonusData = function() {
      function jackpotBonusData() {
        this.count = 0;
        this.jackpotPos = [];
        this.winBonus = 0;
      }
      return jackpotBonusData;
    }();
    exports.jackpotBonusData = jackpotBonusData;
    var dragonBallData = function() {
      function dragonBallData() {
        this.otherBonus = [];
        this.winBonus = 0;
      }
      return dragonBallData;
    }();
    exports.dragonBallData = dragonBallData;
    var EliminateData = function() {
      function EliminateData() {
        this.isEliminate = false;
        this.winJoinList = [];
        this.scatter = new ScatterData();
        this.eliHitPos = [];
        this.eliWinBonus = 0;
      }
      return EliminateData;
    }();
    exports.EliminateData = EliminateData;
    var WinJoinData = function() {
      function WinJoinData() {
        this.joinCount = 0;
        this.winNum = 0;
        this.winBonus = 0;
      }
      return WinJoinData;
    }();
    exports.WinJoinData = WinJoinData;
    var ScatterData = function() {
      function ScatterData() {
        this.winNum = 20;
        this.count = 0;
        this.winBonus = 0;
        this.scatterPos = [ [ 0, 0, 0 ], [ 0, 0, 0 ], [ 0, 0, 0 ], [ 0, 0, 0 ], [ 0, 0, 0 ] ];
      }
      return ScatterData;
    }();
    exports.ScatterData = ScatterData;
    var HistoryData = function() {
      function HistoryData() {
        this.level = 0;
        this.scatterNum = 0;
      }
      return HistoryData;
    }();
    exports.HistoryData = HistoryData;
    cc._RF.pop();
  }, {} ],
  G48_DataManager: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "06539jM36lKJafIn6zKa1tK", "G48_DataManager");
    "use strict";
    var __extends = this && this.__extends || function() {
      var extendStatics = function(d, b) {
        extendStatics = Object.setPrototypeOf || {
          __proto__: []
        } instanceof Array && function(d, b) {
          d.__proto__ = b;
        } || function(d, b) {
          for (var p in b) Object.prototype.hasOwnProperty.call(b, p) && (d[p] = b[p]);
        };
        return extendStatics(d, b);
      };
      return function(d, b) {
        extendStatics(d, b);
        function __() {
          this.constructor = d;
        }
        d.prototype = null === b ? Object.create(b) : (__.prototype = b.prototype, new __());
      };
    }();
    var __decorate = this && this.__decorate || function(decorators, target, key, desc) {
      var c = arguments.length, r = c < 3 ? target : null === desc ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
      if ("object" === typeof Reflect && "function" === typeof Reflect.decorate) r = Reflect.decorate(decorators, target, key, desc); else for (var i = decorators.length - 1; i >= 0; i--) (d = decorators[i]) && (r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r);
      return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    var Slot_DataManager_1 = require("../../SlotFramework/Slot_DataManager");
    var ccclass = cc._decorator.ccclass;
    var G48_DataManager = function(_super) {
      __extends(G48_DataManager, _super);
      function G48_DataManager() {
        var _this = null !== _super && _super.apply(this, arguments) || this;
        _this.gameVersion = "Dev 0.1.1";
        _this.SDKVersion = "Dev 0.1.1";
        _this.isShowDrawcall = false;
        _this.drills = [ 1, 2, 3, 4, 5 ];
        _this.drillIndex = 0;
        _this.resultList = [];
        _this.reelTable_ALLBG = [];
        _this.jpData = null;
        _this.getJackpot = false;
        return _this;
      }
      G48_DataManager.prototype.init = function() {
        _super.prototype.init.call(this);
        this.gameID = 48;
        this.path = "Resources";
        this.skin = "Normal";
        this.setMagnificationTable();
      };
      G48_DataManager.prototype.saveDrillIndex = function(p_betIndex) {
        var _key = this.nickname + this.gameID.toString() + "Drill";
        cc.sys.localStorage.setItem(_key, p_betIndex);
      };
      G48_DataManager.prototype.getDrillIndex = function() {
        var _key = this.nickname + this.gameID.toString() + "Drill";
        var _betIndex = cc.sys.localStorage.getItem(_key);
        if (!cc.isValid(_betIndex)) return 4;
        return Number(_betIndex);
      };
      G48_DataManager.prototype.setMagnificationTable = function() {
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H1] = {};
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H1][4] = 20;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H1][5] = 50;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H1][6] = 100;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H1][7] = 500;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H1][8] = 1e3;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H1][9] = 2e3;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H1][10] = 5e3;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H1][11] = 2e4;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H1][12] = 5e4;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H1][13] = 6e4;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H1][14] = 8e4;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H1][15] = 20;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H1][16] = 50;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H1][17] = 100;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H1][18] = 500;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H1][19] = 1e3;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H1][20] = 2e3;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H1][21] = 5e3;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H1][22] = 2e4;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H1][23] = 5e4;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H1][24] = 8e4;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H1][25] = 1e5;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H1][26] = 20;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H1][27] = 50;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H1][28] = 100;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H1][29] = 500;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H1][30] = 1e3;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H1][31] = 2e3;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H1][32] = 5e3;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H1][33] = 2e4;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H1][34] = 5e4;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H1][35] = 1e5;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H1][36] = 1e5;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H2] = {};
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H2][4] = 10;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H2][5] = 30;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H2][6] = 50;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H2][7] = 60;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H2][8] = 100;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H2][9] = 750;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H2][10] = 1e3;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H2][11] = 1e4;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H2][12] = 2e4;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H2][13] = 5e4;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H2][14] = 6e4;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H2][15] = 10;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H2][16] = 30;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H2][17] = 50;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H2][18] = 60;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H2][19] = 100;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H2][20] = 750;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H2][21] = 1e3;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H2][22] = 1e4;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H2][23] = 2e4;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H2][24] = 5e4;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H2][25] = 7e4;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H2][26] = 10;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H2][27] = 30;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H2][28] = 50;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H2][29] = 60;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H2][30] = 100;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H2][31] = 750;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H2][32] = 1e3;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H2][33] = 1e4;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H2][34] = 2e4;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H2][35] = 5e4;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H2][36] = 8e4;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H3] = {};
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H3][4] = 5;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H3][5] = 10;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H3][6] = 20;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H3][7] = 40;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H3][8] = 80;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H3][9] = 160;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H3][10] = 500;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H3][11] = 1e3;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H3][12] = 2e3;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H3][13] = 5e3;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H3][14] = 6e3;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H3][15] = 5;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H3][16] = 10;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H3][17] = 20;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H3][18] = 40;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H3][19] = 80;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H3][20] = 160;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H3][21] = 500;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H3][22] = 1e3;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H3][23] = 2e3;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H3][24] = 5e3;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H3][25] = 7e3;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H3][26] = 5;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H3][27] = 10;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H3][28] = 20;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H3][29] = 40;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H3][30] = 80;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H3][31] = 160;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H3][32] = 500;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H3][33] = 1e3;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H3][34] = 2e3;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H3][35] = 5e3;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H3][36] = 8e3;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H4] = {};
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H4][4] = 4;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H4][5] = 5;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H4][6] = 10;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H4][7] = 20;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H4][8] = 30;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H4][9] = 50;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H4][10] = 100;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H4][11] = 250;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H4][12] = 500;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H4][13] = 750;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H4][14] = 800;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H4][15] = 4;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H4][16] = 5;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H4][17] = 10;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H4][18] = 20;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H4][19] = 30;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H4][20] = 50;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H4][21] = 100;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H4][22] = 250;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H4][23] = 500;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H4][24] = 750;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H4][25] = 1e3;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H4][26] = 4;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H4][27] = 5;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H4][28] = 10;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H4][29] = 20;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H4][30] = 30;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H4][31] = 50;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H4][32] = 100;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H4][33] = 250;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H4][34] = 500;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H4][35] = 750;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H4][36] = 1200;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H5] = {};
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H5][4] = 2;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H5][5] = 4;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H5][6] = 5;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H5][7] = 8;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H5][8] = 10;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H5][9] = 20;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H5][10] = 30;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H5][11] = 50;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H5][12] = 100;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H5][13] = 200;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H5][14] = 400;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H5][15] = 2;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H5][16] = 4;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H5][17] = 5;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H5][18] = 8;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H5][19] = 10;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H5][20] = 20;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H5][21] = 30;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H5][22] = 50;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H5][23] = 100;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H5][24] = 200;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H5][25] = 450;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H5][26] = 2;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H5][27] = 4;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H5][28] = 5;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H5][29] = 8;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H5][30] = 10;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H5][31] = 20;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H5][32] = 30;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H5][33] = 50;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H5][34] = 100;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H5][35] = 200;
        this.magnificationTable[Slot_DataManager_1.SYMBOL_NAME.H5][36] = 500;
      };
      G48_DataManager = __decorate([ ccclass ], G48_DataManager);
      return G48_DataManager;
    }(Slot_DataManager_1.default);
    exports.default = G48_DataManager;
    cc._RF.pop();
  }, {
    "../../SlotFramework/Slot_DataManager": void 0
  } ],
  G48_DynamicPopUpPanelManager: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "8c890uDNXlLRqEa0Vs3q0qZ", "G48_DynamicPopUpPanelManager");
    "use strict";
    var __extends = this && this.__extends || function() {
      var extendStatics = function(d, b) {
        extendStatics = Object.setPrototypeOf || {
          __proto__: []
        } instanceof Array && function(d, b) {
          d.__proto__ = b;
        } || function(d, b) {
          for (var p in b) Object.prototype.hasOwnProperty.call(b, p) && (d[p] = b[p]);
        };
        return extendStatics(d, b);
      };
      return function(d, b) {
        extendStatics(d, b);
        function __() {
          this.constructor = d;
        }
        d.prototype = null === b ? Object.create(b) : (__.prototype = b.prototype, new __());
      };
    }();
    var __values = this && this.__values || function(o) {
      var s = "function" === typeof Symbol && Symbol.iterator, m = s && o[s], i = 0;
      if (m) return m.call(o);
      if (o && "number" === typeof o.length) return {
        next: function() {
          o && i >= o.length && (o = void 0);
          return {
            value: o && o[i++],
            done: !o
          };
        }
      };
      throw new TypeError(s ? "Object is not iterable." : "Symbol.iterator is not defined.");
    };
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    var Slot_DynamicPopUpManager_1 = require("../../SlotFramework/Common/DynamicPopUp/Slot_DynamicPopUpManager");
    var G48_DynamicPopUpPanelManager = function(_super) {
      __extends(G48_DynamicPopUpPanelManager, _super);
      function G48_DynamicPopUpPanelManager() {
        return null !== _super && _super.apply(this, arguments) || this;
      }
      G48_DynamicPopUpPanelManager.prototype.prestore = function(prefabs) {
        var e_1, _a;
        try {
          for (var prefabs_1 = __values(prefabs), prefabs_1_1 = prefabs_1.next(); !prefabs_1_1.done; prefabs_1_1 = prefabs_1.next()) {
            var prefab = prefabs_1_1.value;
            prefab.name;
            _super.prototype.prestore.call(this, [ prefab ]);
          }
        } catch (e_1_1) {
          e_1 = {
            error: e_1_1
          };
        } finally {
          try {
            prefabs_1_1 && !prefabs_1_1.done && (_a = prefabs_1.return) && _a.call(prefabs_1);
          } finally {
            if (e_1) throw e_1.error;
          }
        }
      };
      return G48_DynamicPopUpPanelManager;
    }(Slot_DynamicPopUpManager_1.default);
    exports.default = G48_DynamicPopUpPanelManager;
    cc._RF.pop();
  }, {
    "../../SlotFramework/Common/DynamicPopUp/Slot_DynamicPopUpManager": void 0
  } ],
  G48_FreeGameGetScorePanel: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "986abytITVKXZseV2EiX5np", "G48_FreeGameGetScorePanel");
    "use strict";
    var __extends = this && this.__extends || function() {
      var extendStatics = function(d, b) {
        extendStatics = Object.setPrototypeOf || {
          __proto__: []
        } instanceof Array && function(d, b) {
          d.__proto__ = b;
        } || function(d, b) {
          for (var p in b) Object.prototype.hasOwnProperty.call(b, p) && (d[p] = b[p]);
        };
        return extendStatics(d, b);
      };
      return function(d, b) {
        extendStatics(d, b);
        function __() {
          this.constructor = d;
        }
        d.prototype = null === b ? Object.create(b) : (__.prototype = b.prototype, new __());
      };
    }();
    var __decorate = this && this.__decorate || function(decorators, target, key, desc) {
      var c = arguments.length, r = c < 3 ? target : null === desc ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
      if ("object" === typeof Reflect && "function" === typeof Reflect.decorate) r = Reflect.decorate(decorators, target, key, desc); else for (var i = decorators.length - 1; i >= 0; i--) (d = decorators[i]) && (r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r);
      return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    var Slot_FreeGameGetScorePanel_1 = require("../../../SlotFramework/Game/Panel/Slot_FreeGameGetScorePanel");
    var FXController_1 = require("../../../Common/Optional/Crosis/FXController/FXController");
    var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
    var G48_FreeGameGetScorePanel = function(_super) {
      __extends(G48_FreeGameGetScorePanel, _super);
      function G48_FreeGameGetScorePanel() {
        var _this = null !== _super && _super.apply(this, arguments) || this;
        _this.fxEnd = null;
        _this.scaleNode = null;
        return _this;
      }
      G48_FreeGameGetScorePanel.prototype.init = function() {
        _super.prototype.init.call(this);
        this.fxEnd.stop();
      };
      G48_FreeGameGetScorePanel.prototype.open = function(p_freeGameGetScore, p_betScore) {
        var _this = this;
        this.fxEnd.stop();
        this.scaleNode.scale = .01;
        _super.prototype.open.call(this, p_freeGameGetScore, p_betScore);
        cc.tween(this.scaleNode).delay(.1).call(function() {
          _this.fxEnd.play();
        }).delay(.1).to(.3, {
          scale: 1
        }, {
          easing: cc.easing.cubicInOut
        }).start();
      };
      G48_FreeGameGetScorePanel.prototype.close = function() {
        var _this = this;
        setTimeout(function() {
          _this.fxEnd.getComponentsInChildren(cc.ParticleSystem3D).forEach(function(item) {
            item.stop();
          });
        }, 250);
        return _super.prototype.close.call(this);
      };
      __decorate([ property(FXController_1.default) ], G48_FreeGameGetScorePanel.prototype, "fxEnd", void 0);
      __decorate([ property(cc.Node) ], G48_FreeGameGetScorePanel.prototype, "scaleNode", void 0);
      G48_FreeGameGetScorePanel = __decorate([ ccclass ], G48_FreeGameGetScorePanel);
      return G48_FreeGameGetScorePanel;
    }(Slot_FreeGameGetScorePanel_1.default);
    exports.default = G48_FreeGameGetScorePanel;
    cc._RF.pop();
  }, {
    "../../../Common/Optional/Crosis/FXController/FXController": void 0,
    "../../../SlotFramework/Game/Panel/Slot_FreeGameGetScorePanel": void 0
  } ],
  G48_FreeGamePanel: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "4a31cMw2T5IAakfwMcKr1WE", "G48_FreeGamePanel");
    "use strict";
    var __extends = this && this.__extends || function() {
      var extendStatics = function(d, b) {
        extendStatics = Object.setPrototypeOf || {
          __proto__: []
        } instanceof Array && function(d, b) {
          d.__proto__ = b;
        } || function(d, b) {
          for (var p in b) Object.prototype.hasOwnProperty.call(b, p) && (d[p] = b[p]);
        };
        return extendStatics(d, b);
      };
      return function(d, b) {
        extendStatics(d, b);
        function __() {
          this.constructor = d;
        }
        d.prototype = null === b ? Object.create(b) : (__.prototype = b.prototype, new __());
      };
    }();
    var __decorate = this && this.__decorate || function(decorators, target, key, desc) {
      var c = arguments.length, r = c < 3 ? target : null === desc ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
      if ("object" === typeof Reflect && "function" === typeof Reflect.decorate) r = Reflect.decorate(decorators, target, key, desc); else for (var i = decorators.length - 1; i >= 0; i--) (d = decorators[i]) && (r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r);
      return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    var Slot_FreeGamePanel_1 = require("../../../SlotFramework/Game/Panel/Slot_FreeGamePanel");
    var AudioManager_1 = require("../../../Common/Tools/AudioManager/AudioManager");
    var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
    var G48_FreeGamePanel = function(_super) {
      __extends(G48_FreeGamePanel, _super);
      function G48_FreeGamePanel() {
        var _this = null !== _super && _super.apply(this, arguments) || this;
        _this.timeLimit = null;
        _this.TIME_COUNT = 15;
        return _this;
      }
      G48_FreeGamePanel.prototype.open = function(p_freeGameCount) {
        var _this = this;
        var count = this.TIME_COUNT;
        this.timeLimit.string = count.toString();
        _super.prototype.open.call(this, p_freeGameCount);
        this.schedule(function() {
          AudioManager_1.AudioManager.instance.playAudioEvent("SecCountDown");
          --count;
          _this.timeLimit.string = count.toString();
        }, 1, 14, 0);
      };
      G48_FreeGamePanel.prototype.close = function() {
        this.unscheduleAllCallbacks();
        return _super.prototype.close.call(this);
      };
      __decorate([ property(cc.Label) ], G48_FreeGamePanel.prototype, "timeLimit", void 0);
      G48_FreeGamePanel = __decorate([ ccclass ], G48_FreeGamePanel);
      return G48_FreeGamePanel;
    }(Slot_FreeGamePanel_1.default);
    exports.default = G48_FreeGamePanel;
    cc._RF.pop();
  }, {
    "../../../Common/Tools/AudioManager/AudioManager": void 0,
    "../../../SlotFramework/Game/Panel/Slot_FreeGamePanel": void 0
  } ],
  G48_GameUI: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "fb237ZQoplGXb9I8ylRNjUQ", "G48_GameUI");
    "use strict";
    var __extends = this && this.__extends || function() {
      var extendStatics = function(d, b) {
        extendStatics = Object.setPrototypeOf || {
          __proto__: []
        } instanceof Array && function(d, b) {
          d.__proto__ = b;
        } || function(d, b) {
          for (var p in b) Object.prototype.hasOwnProperty.call(b, p) && (d[p] = b[p]);
        };
        return extendStatics(d, b);
      };
      return function(d, b) {
        extendStatics(d, b);
        function __() {
          this.constructor = d;
        }
        d.prototype = null === b ? Object.create(b) : (__.prototype = b.prototype, new __());
      };
    }();
    var __decorate = this && this.__decorate || function(decorators, target, key, desc) {
      var c = arguments.length, r = c < 3 ? target : null === desc ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
      if ("object" === typeof Reflect && "function" === typeof Reflect.decorate) r = Reflect.decorate(decorators, target, key, desc); else for (var i = decorators.length - 1; i >= 0; i--) (d = decorators[i]) && (r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r);
      return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __awaiter = this && this.__awaiter || function(thisArg, _arguments, P, generator) {
      function adopt(value) {
        return value instanceof P ? value : new P(function(resolve) {
          resolve(value);
        });
      }
      return new (P || (P = Promise))(function(resolve, reject) {
        function fulfilled(value) {
          try {
            step(generator.next(value));
          } catch (e) {
            reject(e);
          }
        }
        function rejected(value) {
          try {
            step(generator["throw"](value));
          } catch (e) {
            reject(e);
          }
        }
        function step(result) {
          result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected);
        }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
      });
    };
    var __generator = this && this.__generator || function(thisArg, body) {
      var _ = {
        label: 0,
        sent: function() {
          if (1 & t[0]) throw t[1];
          return t[1];
        },
        trys: [],
        ops: []
      }, f, y, t, g;
      return g = {
        next: verb(0),
        throw: verb(1),
        return: verb(2)
      }, "function" === typeof Symbol && (g[Symbol.iterator] = function() {
        return this;
      }), g;
      function verb(n) {
        return function(v) {
          return step([ n, v ]);
        };
      }
      function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
          if (f = 1, y && (t = 2 & op[0] ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 
          0) : y.next) && !(t = t.call(y, op[1])).done) return t;
          (y = 0, t) && (op = [ 2 & op[0], t.value ]);
          switch (op[0]) {
           case 0:
           case 1:
            t = op;
            break;

           case 4:
            _.label++;
            return {
              value: op[1],
              done: false
            };

           case 5:
            _.label++;
            y = op[1];
            op = [ 0 ];
            continue;

           case 7:
            op = _.ops.pop();
            _.trys.pop();
            continue;

           default:
            if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (6 === op[0] || 2 === op[0])) {
              _ = 0;
              continue;
            }
            if (3 === op[0] && (!t || op[1] > t[0] && op[1] < t[3])) {
              _.label = op[1];
              break;
            }
            if (6 === op[0] && _.label < t[1]) {
              _.label = t[1];
              t = op;
              break;
            }
            if (t && _.label < t[2]) {
              _.label = t[2];
              _.ops.push(op);
              break;
            }
            t[2] && _.ops.pop();
            _.trys.pop();
            continue;
          }
          op = body.call(thisArg, _);
        } catch (e) {
          op = [ 6, e ];
          y = 0;
        } finally {
          f = t = 0;
        }
        if (5 & op[0]) throw op[1];
        return {
          value: op[0] ? op[1] : void 0,
          done: true
        };
      }
    };
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    var FXController_1 = require("../../../Common/Optional/Crosis/FXController/FXController");
    var AudioManager_1 = require("../../../Common/Tools/AudioManager/AudioManager");
    var OrientationToolManager_1 = require("../../../Common/Tools/OrientationTool/OrientationToolManager");
    var Slot_GameUI_1 = require("../../../SlotFramework/Game/view/Slot_GameUI");
    var G48_DataManager_1 = require("../../Common/G48_DataManager");
    var G48_FreeGameGetScorePanel_1 = require("./G48_FreeGameGetScorePanel");
    var G48_FreeGamePanel_1 = require("./G48_FreeGamePanel");
    var G48_JackpotPanel_1 = require("./G48_JackpotPanel");
    var G48_SocketManager_1 = require("../../Common/Socket/G48_SocketManager");
    var G48_DynamicPopUpPanelManager_1 = require("../../Common/G48_DynamicPopUpPanelManager");
    var CommonTool_1 = require("../../../Common/Tools/CommonTool");
    var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
    var G48_GameUI = function(_super) {
      __extends(G48_GameUI, _super);
      function G48_GameUI() {
        var _this = null !== _super && _super.apply(this, arguments) || this;
        _this.freeGamePanel = null;
        _this.freeGameGetScorePanel = null;
        _this.baseGameBG = null;
        _this.freeGameBG = null;
        _this.tip6String = "\u4ee5\u4e0b\u70ba Menu Panel Buttons ";
        _this.exitGameButton = null;
        _this.openHistoryButton = null;
        _this.openRuleButton = null;
        _this.openMusicOptionsButton = null;
        _this.closeMenuButton = null;
        _this.copyVersionButton = null;
        _this.tip318String = "\u4ee5\u4e0b\u70ba G48 \u5ba2\u88fd\u5316\u90e8\u5206";
        _this.brickLeve1 = null;
        _this.brickLeve2 = null;
        _this.brickLeve3 = null;
        _this.infoBottom = null;
        _this.freegameScatter = null;
        _this.freegameSymbolLayer = [];
        _this.freegameSymbolLayerF = null;
        _this.FXSpinButton = null;
        _this.drillScore = null;
        _this.drillScoreBoard = null;
        _this.jackpotGrand = null;
        _this.jackpotMajor = null;
        _this.jackpotMinor = null;
        _this.jackpotMini = null;
        _this.exitGamePanel = null;
        _this.winBoard = null;
        _this.titleLevel = [];
        _this.jackpotPanel = null;
        _this.spinClose = null;
        _this.spinButtonAni = null;
        _this.spinButtonTxtAni = null;
        _this.brickFX = null;
        _this.brickFXGroup = null;
        _this.shake = null;
        _this.rewardAni = null;
        _this.isMainGameGetScatterEffecting = false;
        _this.isFreeGamePassSymbolLevelUpIng = false;
        _this.isMenuOpen = false;
        _this.isFreegameStart = false;
        _this.freegameStage = 0;
        _this.freegamePath = [];
        _this.freegameStartPosV = cc.v3(4, 455);
        _this.freegameStartPosH = cc.v3(27, 197);
        _this.freegameStartScaleV = .55;
        _this.freegameStartScaleH = .47;
        _this.winListQueue = [];
        _this.isRunBoard = false;
        _this.level = 0;
        _this.showCount = 0;
        _this.totalSize = 0;
        _this.brickPool = null;
        _this.brickShakeVal = 3;
        _this.shakeDuration = .02;
        _this._menuClosePosition = cc.v3(720, 0, 0);
        _this._menuClosePositionHorizontal = cc.v3(0, 720, 0);
        return _this;
      }
      Object.defineProperty(G48_GameUI.prototype, "socket", {
        get: function() {
          return G48_SocketManager_1.default.getInstance();
        },
        enumerable: false,
        configurable: true
      });
      Object.defineProperty(G48_GameUI.prototype, "popup", {
        get: function() {
          return G48_DynamicPopUpPanelManager_1.default.getInstance();
        },
        enumerable: false,
        configurable: true
      });
      Object.defineProperty(G48_GameUI.prototype, "data", {
        get: function() {
          return G48_DataManager_1.default.getInstance();
        },
        enumerable: false,
        configurable: true
      });
      G48_GameUI.prototype.init = function(p_data) {
        _super.prototype.init.call(this, p_data);
        this.freeGamePanel.init();
        this.freeGameGetScorePanel.init();
        this.historyPanel.init(this.data, this.popup, this.socket);
        this.webRulePanel.init(this.data, this.popup);
        this.jackpotPanel.init();
        this.setSpinArrowSpinningSpeed(Slot_GameUI_1.SPIN_ARROW_SPINNIG_TYPE.NORMAL);
        this.changeToBaseGameBackGround();
        window["GameUI"] = this;
        this.updateJackpot();
        this.brickPool = new cc.NodePool();
        this.brickPool.put(this.brickFX);
        this.exitGamePanel.active = false;
      };
      G48_GameUI.prototype.update = function(dt) {
        var _this = this;
        if (this.isRunBoard) {
          var minSize_1 = Math.max(this.totalSize, 10);
          this.winBoard.getChildByName("mask").children[0].children.forEach(function(item, index) {
            item.y += dt * minSize_1 * 57 * .62;
            _this.winBoard.getChildByName("mask").children[1].children[index].y += dt * minSize_1 * 57 * .62;
            if (item.y > 167) {
              _this.showCount--;
              item.active = false;
              _this.winBoard.getChildByName("mask").children[1].children[index].active = false;
              _this.setWinList(item, _this.winBoard.getChildByName("mask").children[1].children[index]);
              item.y -= 399;
              _this.winBoard.getChildByName("mask").children[1].children[index].y -= 399;
            }
          });
          0 === this.showCount && (this.isRunBoard = false);
        }
      };
      G48_GameUI.prototype.onEnable = function() {
        cc.systemEvent.on(OrientationToolManager_1.ORIENTATION_EVENT.RESIZE, this.switchCanvas, this);
      };
      G48_GameUI.prototype.onDisable = function() {
        _super.prototype.onDisable.call(this);
        cc.systemEvent.off(OrientationToolManager_1.ORIENTATION_EVENT.RESIZE, this.switchCanvas, this);
      };
      G48_GameUI.prototype.closeMenu = function(p_isFast) {
        var _this = this;
        this.isMenuOpen = false;
        var pos = OrientationToolManager_1.default.orientationState == OrientationToolManager_1.ORIENTATION_TYPE.HORIZONTAL ? this._menuClosePositionHorizontal : this._menuClosePosition;
        if (p_isFast) {
          this.menu.setPosition(pos);
          this.menu.active = false;
        } else {
          cc.Tween.stopAllByTarget(this.menu);
          this.menu.setPosition(cc.Vec3.ZERO);
          cc.tween(this.menu).to(this.menuCloseTime, {
            position: pos
          }).call(function() {
            _this.menu.active = false;
          }).start();
        }
      };
      G48_GameUI.prototype.openMenu = function() {
        this.isMenuOpen = true;
        var pos = OrientationToolManager_1.default.orientationState == OrientationToolManager_1.ORIENTATION_TYPE.HORIZONTAL ? this._menuClosePositionHorizontal : this._menuClosePosition;
        cc.Tween.stopAllByTarget(this.menu);
        this.menu.setPosition(pos);
        this.menu.active = true;
        cc.tween(this.menu).to(this.menuOpenTime, {
          position: cc.Vec3.ZERO
        }).start();
      };
      G48_GameUI.prototype.changeToBaseGameBackGround = function() {
        this.baseGameBG.active = true;
        this.infoBottom.active = true;
        this.freeGameBG.active = false;
        this.setBackBtnState(true);
      };
      G48_GameUI.prototype.changeToFreeGameBackGround = function(dragonBallData, freeGameAnsIndex) {
        this.baseGameBG.active = false;
        this.infoBottom.active = false;
        this.freeGameBG.active = true;
        this.setBackBtnState(false);
        this.initFreegame(dragonBallData, freeGameAnsIndex);
      };
      G48_GameUI.prototype.openFreeGamePanel = function(p_freeGameCount) {
        this.setBackBtnState(false);
        this.freeGamePanel.open(p_freeGameCount);
      };
      G48_GameUI.prototype.closeFreeGamePanel = function() {
        return __awaiter(this, void 0, Promise, function() {
          return __generator(this, function(_a) {
            return [ 2, this.freeGamePanel.close() ];
          });
        });
      };
      G48_GameUI.prototype.openFreeGameGetScorePanel = function(p_freeGameGetScore, p_betScore) {
        this.freeGameGetScorePanel.open(p_freeGameGetScore, p_betScore);
      };
      G48_GameUI.prototype.closeFreeGameGetScorePanel = function() {
        return this.freeGameGetScorePanel.close();
      };
      G48_GameUI.prototype.showSpinButtonClickEffect = function() {
        var _this = this;
        this.FXSpinButton.play();
        this.spinButtonTxtAni.play("FX_AnSpin_Once");
        this.scheduleOnce(function() {
          _this.spinArrow.active = true;
        }, .1);
      };
      G48_GameUI.prototype.showSpinButtonIdleEffect = function() {
        _super.prototype.showSpinButtonIdleEffect.call(this);
        this.spinButtonTxtAni.play("FX_AnSpin_Loop");
        this.spinButtonAni.play("FX_AnSpin_Loop");
        this.spinArrow.active = false;
      };
      G48_GameUI.prototype.setSpinArrowSpinningSpeed = function(p_type) {
        this.spinArrow.stopAllActions();
        p_type == Slot_GameUI_1.SPIN_ARROW_SPINNIG_TYPE.STOP || (p_type == Slot_GameUI_1.SPIN_ARROW_SPINNIG_TYPE.NORMAL ? cc.tween(this.spinArrow).by(this.spinArrowRotateDuration_Normal, {
          angle: -360
        }).repeatForever().start() : cc.tween(this.spinArrow).by(this.spinArrowRotateDuration_Fast, {
          angle: -360
        }).repeatForever().start());
      };
      G48_GameUI.prototype.setBetScore = function(p_score) {
        var _this = this;
        _super.prototype.setBetScore.call(this, p_score);
        this.drillScoreBoard.children[0].children.forEach(function(item, index) {
          item.active = parseInt(_this.drillScore.string) > index;
          item.active ? _this.drillScoreBoard.children[1].children[index].getComponent(cc.Label).string = _this.betScore.string : _this.drillScoreBoard.children[1].children[index].getComponent(cc.Label).string = "";
        });
      };
      G48_GameUI.prototype.setDrillScore = function(p_score) {
        var _this = this;
        this.drillScore.string = p_score.toString();
        this.drillScoreBoard.children[0].children.forEach(function(item, index) {
          item.active = parseInt(_this.drillScore.string) > index;
          item.active ? _this.drillScoreBoard.children[1].children[index].getComponent(cc.Label).string = _this.betScore.string : _this.drillScoreBoard.children[1].children[index].getComponent(cc.Label).string = "";
        });
      };
      G48_GameUI.prototype.switchCanvas = function() {
        this.setMenu();
        if (this.isFreegameStart) {
          cc.Tween.stopAllByTarget(this.freegameScatter);
          this.freegameScatter.position = this.freegameSymbolLayer[this.freegameStage].children[this.freegamePath[this.freegameStage]].position;
          if (this.freegameStage === this.freegamePath.length - 1) {
            AudioManager_1.AudioManager.instance.playAudioEvent("DragonBallScore");
            var pot = this.freegameSymbolLayer[this.freegameStage].children[this.freegamePath[this.freegameStage]];
            pot.getChildByName("An").getChildByName("An_once").active = true;
            pot.getChildByName("An").getChildByName("An_once").getComponent(cc.Animation).play();
            this.isFreegameStart = false;
          } else {
            this.freegameStage++;
            this.toNextRunScatter();
          }
        } else this.freegameScatter.position = OrientationToolManager_1.default.orientationState == OrientationToolManager_1.ORIENTATION_TYPE.HORIZONTAL ? this.freegameStartPosH : this.freegameStartPosV;
        this.freegameScatter.scale = OrientationToolManager_1.default.orientationState == OrientationToolManager_1.ORIENTATION_TYPE.HORIZONTAL ? this.freegameStartScaleH : this.freegameStartScaleV;
        this.updateJackpot();
      };
      G48_GameUI.prototype.initFreegame = function(dragonBallData, freeGameAnsIndex) {
        this.isFreegameStart = false;
        this.freegameStage = 0;
        this.freegameScatter.position = OrientationToolManager_1.default.orientationState == OrientationToolManager_1.ORIENTATION_TYPE.HORIZONTAL ? this.freegameStartPosH : this.freegameStartPosV;
        this.freegameScatter.scale = OrientationToolManager_1.default.orientationState == OrientationToolManager_1.ORIENTATION_TYPE.HORIZONTAL ? this.freegameStartScaleH : this.freegameStartScaleV;
        this.freegameScatter.getComponent(sp.Skeleton).setAnimation(0, "Scatter_play_normal", true);
        this.freegameSymbolLayerF.children.forEach(function(item, index) {
          var entireMoney = 0;
          entireMoney = index === freeGameAnsIndex ? G48_DataManager_1.default.getInstance().getClientMoney(dragonBallData.winBonus) : index < freeGameAnsIndex ? G48_DataManager_1.default.getInstance().getClientMoney(dragonBallData.otherBonus[index]) : G48_DataManager_1.default.getInstance().getClientMoney(dragonBallData.otherBonus[index - 1]);
          var AbbreviatedMoney = CommonTool_1.CommonTool.getNumText(entireMoney, 2, true, false);
          item.getComponentInChildren(cc.Label).string = AbbreviatedMoney;
        });
      };
      G48_GameUI.prototype.setBrickFX = function(node, isBottom, isPlayAudio) {
        var _this = this;
        this.nodeShake(node, this.brickShakeVal);
        var fx = null;
        fx = this.brickPool.size() < 1 ? cc.instantiate(this.brickFX).getComponent(FXController_1.default) : this.brickPool.get().getComponent(FXController_1.default);
        fx.node.position = node.position;
        fx.node.parent = this.brickFXGroup;
        fx.node.scale = isBottom ? .5 : 1;
        fx.play();
        isPlayAudio && AudioManager_1.AudioManager.instance.playAudioEvent("BrickDisappearEffect");
        this.scheduleOnce(function() {
          node.active = false;
          _this.shakeBG(0);
        }, .5);
        this.scheduleOnce(function() {
          _this.brickPool.put(fx.node);
        }, 1);
      };
      G48_GameUI.prototype.setBrick = function(scatterNum, isInit) {
        var _this = this;
        var isPlayAudio = true;
        this.brickLeve1.children.forEach(function(item, index) {
          var oldActive = item.active;
          var newActive = scatterNum <= index;
          if (isInit) item.active = newActive; else if (oldActive != newActive && !isInit) {
            _this.setBrickFX(item, false, isPlayAudio);
            isPlayAudio = false;
          }
        });
        this.brickLeve2.children.forEach(function(item, index) {
          var oldActive = item.active;
          var newActive = scatterNum <= index + 15;
          if (isInit) item.active = newActive; else if (oldActive != newActive && !isInit) {
            _this.setBrickFX(item, false, isPlayAudio);
            isPlayAudio = false;
          }
        });
        this.brickLeve3.children.forEach(function(item, index) {
          var oldActive = item.active;
          var newActive = scatterNum <= index + 30;
          if (isInit) item.active = newActive; else if (oldActive != newActive && !isInit) {
            _this.setBrickFX(item, true, isPlayAudio);
            isPlayAudio = false;
          }
        });
      };
      G48_GameUI.prototype.nodeShake = function(node, val, delay) {
        void 0 === delay && (delay = 0);
        if (node.getNumberOfRunningActions() > 0) return;
        cc.tween(node).delay(delay).by(.035, {
          position: new cc.Vec3(val, 0)
        }).by(.035, {
          position: new cc.Vec3(-val, 0)
        }).by(.035, {
          position: new cc.Vec3(-val, 0)
        }).by(.035, {
          position: new cc.Vec3(val, 0)
        }).by(.035, {
          position: new cc.Vec3(val, 0)
        }).by(.035, {
          position: new cc.Vec3(-val, 0)
        }).by(.035, {
          position: new cc.Vec3(-val, 0)
        }).by(.035, {
          position: new cc.Vec3(val, 0)
        }).start();
      };
      G48_GameUI.prototype.shakeBG = function(delay) {
        if (this.shake.getNumberOfRunningActions() > 0) return;
        cc.tween(this.shake).delay(delay).to(this.shakeDuration, {
          position: new cc.Vec3(5, 7)
        }).to(this.shakeDuration, {
          position: new cc.Vec3(-6, 7)
        }).to(this.shakeDuration, {
          position: new cc.Vec3(-13, 3)
        }).to(this.shakeDuration, {
          position: new cc.Vec3(3, -6)
        }).to(this.shakeDuration, {
          position: new cc.Vec3(-5, 5)
        }).to(this.shakeDuration, {
          position: new cc.Vec3(2, -8)
        }).to(this.shakeDuration, {
          position: new cc.Vec3(-8, -10)
        }).to(this.shakeDuration, {
          position: new cc.Vec3(3, 10)
        }).to(this.shakeDuration, {
          position: new cc.Vec3(0, 0)
        }).start();
      };
      G48_GameUI.prototype.freegameRunScatter = function(path) {
        this.isFreegameStart = true;
        this.freegameScatter.getComponent(sp.Skeleton).setAnimation(0, "Scatter_play_win", true);
        this.freegamePath = path;
        this.toNextRunScatter();
      };
      G48_GameUI.prototype.toNextRunScatter = function() {
        var _this = this;
        if (this.freegameStage < this.freegamePath.length - 1) cc.tween(this.freegameScatter).to(1, {
          position: this.freegameSymbolLayer[this.freegameStage].children[this.freegamePath[this.freegameStage]].position
        }).call(function() {
          AudioManager_1.AudioManager.instance.playAudioEvent("DragonBallCollide");
          _this.freegameStage++;
          _this.toNextRunScatter();
        }).start(); else {
          var prePos = this.freegameSymbolLayer[this.freegameStage - 1].children[this.freegamePath[this.freegameStage - 1]].position;
          var pos = this.freegameSymbolLayer[this.freegameStage].children[this.freegamePath[this.freegameStage]].position;
          cc.tween(this.freegameScatter).to(.7, {
            position: cc.v3(prePos.x + .7 * (pos.x - prePos.x), prePos.y + .7 * (pos.y - prePos.y))
          }).call(function() {
            AudioManager_1.AudioManager.instance.playAudioEvent("DragonBallScore");
            var pot = _this.freegameSymbolLayer[_this.freegameStage].children[_this.freegamePath[_this.freegameStage]];
            pot.getChildByName("An").getChildByName("An_once").active = true;
            pot.getChildByName("An").getChildByName("An_once").getComponent(cc.Animation).play();
          }).to(.3, {
            position: this.freegameSymbolLayer[this.freegameStage].children[this.freegamePath[this.freegameStage]].position
          }).call(function() {
            _this.isFreegameStart = false;
          }).start();
        }
      };
      G48_GameUI.prototype.setMenu = function() {
        cc.Tween.stopAllByTarget(this.menu);
        var pos = OrientationToolManager_1.default.orientationState == OrientationToolManager_1.ORIENTATION_TYPE.HORIZONTAL ? this._menuClosePositionHorizontal : this._menuClosePosition;
        this.isMenuOpen ? this.menu.setPosition(cc.Vec3.ZERO) : this.menu.setPosition(pos);
        this.menu.active = this.isMenuOpen;
      };
      G48_GameUI.prototype.refreshJackpot = function(data) {
        var _this = this;
        data.jackpots.forEach(function(item) {
          switch (item.name) {
           case "grand":
            _this.jackpotGrand.string = G48_DataManager_1.default.getInstance().getClientMoney(item.bonus).currency();
            break;

           case "major":
            _this.jackpotMajor.string = G48_DataManager_1.default.getInstance().getClientMoney(item.bonus).currency();
            break;

           case "minor":
            _this.jackpotMinor.string = G48_DataManager_1.default.getInstance().getClientMoney(item.bonus).currency();
            break;

           case "mini":
            _this.jackpotMini.string = G48_DataManager_1.default.getInstance().getClientMoney(item.bonus).currency();
          }
        });
      };
      G48_GameUI.prototype.openExitGamePanel = function() {
        this.exitGamePanel.setPosition(new cc.Vec2(0, 0));
        this.exitGamePanel.active = true;
      };
      G48_GameUI.prototype.closeExitGamePanel = function() {
        this.exitGamePanel.active = false;
      };
      G48_GameUI.prototype.openJackpotPanel = function(p_freeGameGetScore, level) {
        this.jackpotPanel.open(p_freeGameGetScore, level);
      };
      G48_GameUI.prototype.closeJackpotPanel = function() {
        this.jackpotPanel.close();
      };
      G48_GameUI.prototype.showWinList = function(winList, level) {
        var _this = this;
        winList.sort(function(a, b) {
          return b.winBonus - a.winBonus;
        });
        this.level = level;
        this.totalSize = winList.length;
        if (this.winListQueue.length > 0) this.winListQueue = this.winListQueue.concat(winList); else {
          this.winListQueue = winList;
          this.winBoard.getChildByName("mask").children[0].children.forEach(function(item, index) {
            item.active = false;
            _this.winBoard.getChildByName("mask").children[1].children[index].active = false;
            item.y = 110 + -57 * index;
            _this.winBoard.getChildByName("mask").children[1].children[index].y = 110 + -57 * index;
            _this.setWinList(item, _this.winBoard.getChildByName("mask").children[1].children[index]);
          });
          this.scheduleOnce(function() {
            _this.isRunBoard = true;
          }, .4);
        }
      };
      G48_GameUI.prototype.setWinList = function(sprNode, lblNode) {
        if (this.winListQueue.length > 0) {
          var data_1 = this.winListQueue.shift();
          cc.assetManager.getBundle("G48").load("Resources/image/UI/g48_symbol_H" + data_1.winNum + "_" + this.level, cc.SpriteFrame, function(err, assets) {
            if (err) return;
            sprNode.getChildByName("Symbol").getComponent(cc.Sprite).spriteFrame = assets;
            lblNode.getChildByName("Count").getComponent(cc.Label).string = "x" + data_1.joinCount.toString();
            lblNode.getChildByName("Score").getComponent(cc.Label).string = CommonTool_1.CommonTool.getNumText(G48_DataManager_1.default.getInstance().getClientMoney(data_1.winBonus), 4, true, false);
            sprNode.active = true;
            lblNode.active = true;
          });
          this.showCount++;
        }
      };
      G48_GameUI.prototype.setLevel = function(level) {
        this.titleLevel.forEach(function(item, index) {
          item.node.active = level - 1 === index;
        });
      };
      G48_GameUI.prototype.orientationAddandReduceSetParent = function() {};
      G48_GameUI.prototype.showSpinMask = function() {
        _super.prototype.showSpinMask.call(this);
        this.spinClose.opacity = 128;
        this.spinClose.getComponentInChildren(cc.BlockInputEvents).enabled = true;
      };
      G48_GameUI.prototype.hideSpinMask = function() {
        _super.prototype.hideSpinMask.call(this);
        this.spinClose.opacity = 255;
        this.spinClose.getComponentInChildren(cc.BlockInputEvents).enabled = false;
      };
      G48_GameUI.prototype.scoreCtrl = function(isOpen) {};
      G48_GameUI.prototype.showSpinArrow = function() {
        this.spinArrow.active = true;
        this.spinButtonTxtAni.stop();
        this.spinButtonTxtAni.node.children.forEach(function(item) {
          item.opacity = 0;
        });
      };
      G48_GameUI.prototype.updateJackpot = function() {
        this.jackpotGrand.getComponentInChildren(cc.Label).fontSize = this.jackpotGrand.getComponent(cc.Label).fontSize;
        this.jackpotMajor.getComponentInChildren(cc.Label).fontSize = this.jackpotMajor.getComponent(cc.Label).fontSize;
        this.jackpotMini.getComponentInChildren(cc.Label).fontSize = this.jackpotMini.getComponent(cc.Label).fontSize;
        this.jackpotMinor.getComponentInChildren(cc.Label).fontSize = this.jackpotMinor.getComponent(cc.Label).fontSize;
        this.jackpotGrand.node.children[0].width = this.jackpotGrand.node.width;
        this.jackpotMajor.node.children[0].width = this.jackpotMajor.node.width;
        this.jackpotMini.node.children[0].width = this.jackpotMini.node.width;
        this.jackpotMinor.node.children[0].width = this.jackpotMinor.node.width;
      };
      G48_GameUI.prototype.playRewardEffect = function() {
        var _this = this;
        this.rewardAni.node.active = true;
        this.rewardAni.play();
        this.rewardAni.once("finished", function() {
          _this.rewardAni.node.active = false;
        });
      };
      __decorate([ property(G48_FreeGamePanel_1.default) ], G48_GameUI.prototype, "freeGamePanel", void 0);
      __decorate([ property(G48_FreeGameGetScorePanel_1.default) ], G48_GameUI.prototype, "freeGameGetScorePanel", void 0);
      __decorate([ property(cc.Node) ], G48_GameUI.prototype, "baseGameBG", void 0);
      __decorate([ property(cc.Node) ], G48_GameUI.prototype, "freeGameBG", void 0);
      __decorate([ property({
        readonly: true,
        displayName: "========================"
      }) ], G48_GameUI.prototype, "tip6String", void 0);
      __decorate([ property(cc.Node) ], G48_GameUI.prototype, "exitGameButton", void 0);
      __decorate([ property(cc.Node) ], G48_GameUI.prototype, "openHistoryButton", void 0);
      __decorate([ property(cc.Node) ], G48_GameUI.prototype, "openRuleButton", void 0);
      __decorate([ property(cc.Node) ], G48_GameUI.prototype, "openMusicOptionsButton", void 0);
      __decorate([ property(cc.Node) ], G48_GameUI.prototype, "closeMenuButton", void 0);
      __decorate([ property(cc.Node) ], G48_GameUI.prototype, "copyVersionButton", void 0);
      __decorate([ property({
        readonly: true,
        displayName: "========================"
      }) ], G48_GameUI.prototype, "tip318String", void 0);
      __decorate([ property(cc.Node) ], G48_GameUI.prototype, "brickLeve1", void 0);
      __decorate([ property(cc.Node) ], G48_GameUI.prototype, "brickLeve2", void 0);
      __decorate([ property(cc.Node) ], G48_GameUI.prototype, "brickLeve3", void 0);
      __decorate([ property(cc.Node) ], G48_GameUI.prototype, "infoBottom", void 0);
      __decorate([ property(cc.Node) ], G48_GameUI.prototype, "freegameScatter", void 0);
      __decorate([ property([ cc.Node ]) ], G48_GameUI.prototype, "freegameSymbolLayer", void 0);
      __decorate([ property(cc.Node) ], G48_GameUI.prototype, "freegameSymbolLayerF", void 0);
      __decorate([ property(FXController_1.default) ], G48_GameUI.prototype, "FXSpinButton", void 0);
      __decorate([ property(cc.Label) ], G48_GameUI.prototype, "drillScore", void 0);
      __decorate([ property(cc.Node) ], G48_GameUI.prototype, "drillScoreBoard", void 0);
      __decorate([ property(cc.Label) ], G48_GameUI.prototype, "jackpotGrand", void 0);
      __decorate([ property(cc.Label) ], G48_GameUI.prototype, "jackpotMajor", void 0);
      __decorate([ property(cc.Label) ], G48_GameUI.prototype, "jackpotMinor", void 0);
      __decorate([ property(cc.Label) ], G48_GameUI.prototype, "jackpotMini", void 0);
      __decorate([ property(cc.Node) ], G48_GameUI.prototype, "exitGamePanel", void 0);
      __decorate([ property(cc.Node) ], G48_GameUI.prototype, "winBoard", void 0);
      __decorate([ property([ cc.Sprite ]) ], G48_GameUI.prototype, "titleLevel", void 0);
      __decorate([ property(G48_JackpotPanel_1.default) ], G48_GameUI.prototype, "jackpotPanel", void 0);
      __decorate([ property(cc.Node) ], G48_GameUI.prototype, "spinClose", void 0);
      __decorate([ property(cc.Animation) ], G48_GameUI.prototype, "spinButtonAni", void 0);
      __decorate([ property(cc.Animation) ], G48_GameUI.prototype, "spinButtonTxtAni", void 0);
      __decorate([ property(cc.Node) ], G48_GameUI.prototype, "brickFX", void 0);
      __decorate([ property(cc.Node) ], G48_GameUI.prototype, "brickFXGroup", void 0);
      __decorate([ property(cc.Node) ], G48_GameUI.prototype, "shake", void 0);
      __decorate([ property(cc.Animation) ], G48_GameUI.prototype, "rewardAni", void 0);
      G48_GameUI = __decorate([ ccclass ], G48_GameUI);
      return G48_GameUI;
    }(Slot_GameUI_1.default);
    exports.default = G48_GameUI;
    cc._RF.pop();
  }, {
    "../../../Common/Optional/Crosis/FXController/FXController": void 0,
    "../../../Common/Tools/AudioManager/AudioManager": void 0,
    "../../../Common/Tools/CommonTool": void 0,
    "../../../Common/Tools/OrientationTool/OrientationToolManager": void 0,
    "../../../SlotFramework/Game/view/Slot_GameUI": void 0,
    "../../Common/G48_DataManager": "G48_DataManager",
    "../../Common/G48_DynamicPopUpPanelManager": "G48_DynamicPopUpPanelManager",
    "../../Common/Socket/G48_SocketManager": "G48_SocketManager",
    "./G48_FreeGameGetScorePanel": "G48_FreeGameGetScorePanel",
    "./G48_FreeGamePanel": "G48_FreeGamePanel",
    "./G48_JackpotPanel": "G48_JackpotPanel"
  } ],
  G48_Game: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "724810bftJMuqxs7/0qW1wR", "G48_Game");
    "use strict";
    var __extends = this && this.__extends || function() {
      var extendStatics = function(d, b) {
        extendStatics = Object.setPrototypeOf || {
          __proto__: []
        } instanceof Array && function(d, b) {
          d.__proto__ = b;
        } || function(d, b) {
          for (var p in b) Object.prototype.hasOwnProperty.call(b, p) && (d[p] = b[p]);
        };
        return extendStatics(d, b);
      };
      return function(d, b) {
        extendStatics(d, b);
        function __() {
          this.constructor = d;
        }
        d.prototype = null === b ? Object.create(b) : (__.prototype = b.prototype, new __());
      };
    }();
    var __decorate = this && this.__decorate || function(decorators, target, key, desc) {
      var c = arguments.length, r = c < 3 ? target : null === desc ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
      if ("object" === typeof Reflect && "function" === typeof Reflect.decorate) r = Reflect.decorate(decorators, target, key, desc); else for (var i = decorators.length - 1; i >= 0; i--) (d = decorators[i]) && (r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r);
      return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __awaiter = this && this.__awaiter || function(thisArg, _arguments, P, generator) {
      function adopt(value) {
        return value instanceof P ? value : new P(function(resolve) {
          resolve(value);
        });
      }
      return new (P || (P = Promise))(function(resolve, reject) {
        function fulfilled(value) {
          try {
            step(generator.next(value));
          } catch (e) {
            reject(e);
          }
        }
        function rejected(value) {
          try {
            step(generator["throw"](value));
          } catch (e) {
            reject(e);
          }
        }
        function step(result) {
          result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected);
        }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
      });
    };
    var __generator = this && this.__generator || function(thisArg, body) {
      var _ = {
        label: 0,
        sent: function() {
          if (1 & t[0]) throw t[1];
          return t[1];
        },
        trys: [],
        ops: []
      }, f, y, t, g;
      return g = {
        next: verb(0),
        throw: verb(1),
        return: verb(2)
      }, "function" === typeof Symbol && (g[Symbol.iterator] = function() {
        return this;
      }), g;
      function verb(n) {
        return function(v) {
          return step([ n, v ]);
        };
      }
      function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
          if (f = 1, y && (t = 2 & op[0] ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 
          0) : y.next) && !(t = t.call(y, op[1])).done) return t;
          (y = 0, t) && (op = [ 2 & op[0], t.value ]);
          switch (op[0]) {
           case 0:
           case 1:
            t = op;
            break;

           case 4:
            _.label++;
            return {
              value: op[1],
              done: false
            };

           case 5:
            _.label++;
            y = op[1];
            op = [ 0 ];
            continue;

           case 7:
            op = _.ops.pop();
            _.trys.pop();
            continue;

           default:
            if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (6 === op[0] || 2 === op[0])) {
              _ = 0;
              continue;
            }
            if (3 === op[0] && (!t || op[1] > t[0] && op[1] < t[3])) {
              _.label = op[1];
              break;
            }
            if (6 === op[0] && _.label < t[1]) {
              _.label = t[1];
              t = op;
              break;
            }
            if (t && _.label < t[2]) {
              _.label = t[2];
              _.ops.push(op);
              break;
            }
            t[2] && _.ops.pop();
            _.trys.pop();
            continue;
          }
          op = body.call(thisArg, _);
        } catch (e) {
          op = [ 6, e ];
          y = 0;
        } finally {
          f = t = 0;
        }
        if (5 & op[0]) throw op[1];
        return {
          value: op[0] ? op[1] : void 0,
          done: true
        };
      }
    };
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    var AudioManager_1 = require("../../Common/Tools/AudioManager/AudioManager");
    var LocalizationManager_1 = require("../../Common/Tools/Localization/LocalizationManager");
    var MathUtils_1 = require("../../Common/Tools/MathUtils");
    var Slot_GameManager_InitData_1 = require("../../SlotFramework/Game/Data/Slot_GameManager_InitData");
    var Slot_GameUI_InitData_1 = require("../../SlotFramework/Game/Data/Slot_GameUI_InitData");
    var Slot_ReelManager_InitData_1 = require("../../SlotFramework/Game/Data/Slot_ReelManager_InitData");
    var Slot_ReelManager_ScrollData_1 = require("../../SlotFramework/Game/Data/Slot_ReelManager_ScrollData");
    var Slot_GameManager_1 = require("../../SlotFramework/Game/Slot_GameManager");
    var Slot_GameUI_1 = require("../../SlotFramework/Game/view/Slot_GameUI");
    var G48_DataManager_1 = require("../Common/G48_DataManager");
    var G48_DynamicPopUpPanelManager_1 = require("../Common/G48_DynamicPopUpPanelManager");
    var G48_SocketManager_1 = require("../Common/Socket/G48_SocketManager");
    var G48_GameUI_1 = require("./View/G48_GameUI");
    var G48_SlotReelManager_1 = require("./View/G48_SlotReelManager");
    var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
    var GAME_STATUS;
    (function(GAME_STATUS) {
      GAME_STATUS[GAME_STATUS["NONE"] = 0] = "NONE";
      GAME_STATUS[GAME_STATUS["IDLE"] = 1] = "IDLE";
      GAME_STATUS[GAME_STATUS["SERVER_REQUEST"] = 2] = "SERVER_REQUEST";
      GAME_STATUS[GAME_STATUS["SPIN"] = 3] = "SPIN";
      GAME_STATUS[GAME_STATUS["FREE_GAME_TIP"] = 4] = "FREE_GAME_TIP";
      GAME_STATUS[GAME_STATUS["FREE_GAME"] = 5] = "FREE_GAME";
      GAME_STATUS[GAME_STATUS["FREE_GAME_REWARD"] = 6] = "FREE_GAME_REWARD";
      GAME_STATUS[GAME_STATUS["CHANGE_PANEL"] = 7] = "CHANGE_PANEL";
      GAME_STATUS[GAME_STATUS["THROW_SYMBOL"] = 8] = "THROW_SYMBOL";
    })(GAME_STATUS || (GAME_STATUS = {}));
    var SPIN_STATUS;
    (function(SPIN_STATUS) {
      SPIN_STATUS[SPIN_STATUS["NONE"] = 0] = "NONE";
      SPIN_STATUS[SPIN_STATUS["REEL_SCROLL_START"] = 1] = "REEL_SCROLL_START";
      SPIN_STATUS[SPIN_STATUS["REEL_SCROLL_ING"] = 2] = "REEL_SCROLL_ING";
      SPIN_STATUS[SPIN_STATUS["REEL_SCROLL_END"] = 3] = "REEL_SCROLL_END";
      SPIN_STATUS[SPIN_STATUS["CHECK_REWARD"] = 4] = "CHECK_REWARD";
      SPIN_STATUS[SPIN_STATUS["GET_SCATTER_START"] = 5] = "GET_SCATTER_START";
      SPIN_STATUS[SPIN_STATUS["GET_SCATTER_ING"] = 6] = "GET_SCATTER_ING";
      SPIN_STATUS[SPIN_STATUS["GET_SCATTER_END"] = 7] = "GET_SCATTER_END";
      SPIN_STATUS[SPIN_STATUS["JACKPOT_START"] = 8] = "JACKPOT_START";
      SPIN_STATUS[SPIN_STATUS["JACKPOT_ING"] = 9] = "JACKPOT_ING";
      SPIN_STATUS[SPIN_STATUS["REWARD_EFFECT_START"] = 10] = "REWARD_EFFECT_START";
      SPIN_STATUS[SPIN_STATUS["REWARD_EFFECT_ING"] = 11] = "REWARD_EFFECT_ING";
      SPIN_STATUS[SPIN_STATUS["REWARD_EFFECT_END"] = 12] = "REWARD_EFFECT_END";
      SPIN_STATUS[SPIN_STATUS["WIN_SCORE_EFFECT_START"] = 13] = "WIN_SCORE_EFFECT_START";
      SPIN_STATUS[SPIN_STATUS["WIN_SCORE_EFFECT_ING"] = 14] = "WIN_SCORE_EFFECT_ING";
      SPIN_STATUS[SPIN_STATUS["WIN_SCORE_EFFECT_END"] = 15] = "WIN_SCORE_EFFECT_END";
      SPIN_STATUS[SPIN_STATUS["FINISHED_WAIT"] = 16] = "FINISHED_WAIT";
      SPIN_STATUS[SPIN_STATUS["FINISHED"] = 17] = "FINISHED";
      SPIN_STATUS[SPIN_STATUS["WAIT_REWARD"] = 18] = "WAIT_REWARD";
      SPIN_STATUS[SPIN_STATUS["SYMBOL_DISAPPEAR_START"] = 19] = "SYMBOL_DISAPPEAR_START";
      SPIN_STATUS[SPIN_STATUS["SYMBOL_DISAPPEAR_ING"] = 20] = "SYMBOL_DISAPPEAR_ING";
      SPIN_STATUS[SPIN_STATUS["SYMBOL_DISAPPEAR_END"] = 21] = "SYMBOL_DISAPPEAR_END";
      SPIN_STATUS[SPIN_STATUS["SYMBOL_DISAPPEAR_DROP_START"] = 22] = "SYMBOL_DISAPPEAR_DROP_START";
      SPIN_STATUS[SPIN_STATUS["SYMBOL_DISAPPEAR_DROP_ING"] = 23] = "SYMBOL_DISAPPEAR_DROP_ING";
      SPIN_STATUS[SPIN_STATUS["SYMBOL_DISAPPEAR_DROP_END"] = 24] = "SYMBOL_DISAPPEAR_DROP_END";
      SPIN_STATUS[SPIN_STATUS["SCATTER_WIN_START"] = 25] = "SCATTER_WIN_START";
      SPIN_STATUS[SPIN_STATUS["SCATTER_WIN_ING"] = 26] = "SCATTER_WIN_ING";
      SPIN_STATUS[SPIN_STATUS["SCATTER_WIN_END"] = 27] = "SCATTER_WIN_END";
      SPIN_STATUS[SPIN_STATUS["FG_START"] = 28] = "FG_START";
      SPIN_STATUS[SPIN_STATUS["FG_ING"] = 29] = "FG_ING";
    })(SPIN_STATUS || (SPIN_STATUS = {}));
    var G48_Game = function(_super) {
      __extends(G48_Game, _super);
      function G48_Game() {
        var _this = null !== _super && _super.apply(this, arguments) || this;
        _this.slotReelManager = null;
        _this.firstWinLineChangeTime = .4;
        _this.winLineChangeTime = 2;
        _this.winLineChangeWaitTime = .1;
        _this.ui = null;
        _this.spinCD = .5;
        _this.normalSpinFinishedStopTime = .5;
        _this.fastSpinFinishedStopTime = .2;
        _this.normalUpdateTipBroadEffectTime = .1;
        _this.autoSpinUpdateTipBroadEffectTime = .9;
        _this.normalRewardTime = 1;
        _this.autoRewardTime = 1;
        _this.scatterShowTime = 3;
        _this.data = null;
        _this.socket = null;
        _this.popup = null;
        _this.gameStatus = GAME_STATUS.NONE;
        _this.spinStatus = SPIN_STATUS.NONE;
        _this.eliminateListIndex = 0;
        _this.waitRewardTime = 0;
        _this.curLevel = 1;
        _this.curBrick = 0;
        _this.freeGameAnsIndex = 0;
        _this.freegameTimeout = null;
        _this.competitionStartFontSize1 = 30;
        _this.competitionStartFontColor1 = "#3A1304";
        _this.competitionStartFontSize2 = 30;
        _this.competitionStartFontColor2 = "#DC5050";
        _this.waitShowTime = 0;
        return _this;
      }
      G48_Game.prototype.onLoad = function() {
        this.ui.exitGameButton.on("click", this.onOpenExitGamePanel, this);
        this.ui.openHistoryButton.on("click", this.onOpenHistory, this);
        this.ui.openRuleButton.on("click", this.onOpenRule, this);
        this.ui.openMusicOptionsButton.on("click", this.onOpenMusicOptionPanel, this);
        this.ui.closeMenuButton.on("click", this.onCloseMenu, this);
        this.ui.copyVersionButton.on("click", this.OnCopyVersion, this);
      };
      G48_Game.prototype.start = function() {
        var _this = this;
        this.socket = G48_SocketManager_1.default.getInstance();
        this.data = G48_DataManager_1.default.getInstance();
        this.popup = G48_DynamicPopUpPanelManager_1.default.getInstance();
        _super.prototype.start.call(this);
        var _data = new Slot_GameManager_InitData_1.default();
        _data.bundleName = "G" + G48_DataManager_1.default.getInstance().gameID;
        _data.bundlePath = "" + G48_DataManager_1.default.getInstance().path;
        this.init(_data);
        this.socket.setOnHistoryEvent().then(function(data) {
          _this.showResponseLog("History", data);
          _this.setLevel(data.level);
          _this.setBrick(15 * (data.level - 1) + data.scatterNum, true);
          _this.socket.setOnGetInitialReelInfoEvent(data.level).then(function(p_data) {
            _this.showResponseLog("GetInitialReelInfo", p_data);
            _this.setReelInfo(p_data);
            _this.playerLogin();
            _this.gameStatus = GAME_STATUS.IDLE;
          });
        });
        true;
        window["Game"] = this;
      };
      G48_Game.prototype.init = function(p_data) {
        this.data = G48_DataManager_1.default.getInstance();
        this.data.init();
        var _data = new Slot_ReelManager_InitData_1.default();
        _data.bundleName = p_data.bundleName;
        _data.bundlePath = p_data.bundlePath;
        this.slotReelManager.init(_data);
        _super.prototype.init.call(this, p_data);
        this.slotReelManager.hideAllReelSymbol();
        var _uiData = new Slot_GameUI_InitData_1.default();
        _uiData.bundleName = p_data.bundleName;
        _uiData.bundlePath = p_data.bundlePath;
        _uiData.dataManager = this.data;
        this.ui.init(_uiData);
        this.ui.setVersion(this.data.gameVersion);
        this.ui.setRemainingFreeGameCount(0);
        this.registerEvents();
        this.data.jpData && this.ui.refreshJackpot(this.data.jpData);
        AudioManager_1.AudioManager.instance.playAudioEvent("GameInit");
      };
      G48_Game.prototype.onDisable = function() {
        _super.prototype.onDisable.call(this);
        AudioManager_1.AudioManager.instance.playAudioEvent("GameExit");
      };
      G48_Game.prototype.update = function(dt) {
        if (this.toFastStopSpin && this.tempSpinCD > this.spinCD) {
          this.stopSpin();
          this.toFastStopSpin = false;
        }
        if (this.gameStatus == GAME_STATUS.SPIN) {
          this.tempSpinCD += dt;
          switch (this.spinStatus) {
           case SPIN_STATUS.REEL_SCROLL_START:
            this.reelScrollStart();
            break;

           case SPIN_STATUS.REEL_SCROLL_ING:
            this.reelScrolling();
            break;

           case SPIN_STATUS.REEL_SCROLL_END:
            this.reelScrollEnd();
            break;

           case SPIN_STATUS.CHECK_REWARD:
            this.checkReward();
            break;

           case SPIN_STATUS.GET_SCATTER_START:
            this.GetScatterEffectStart();
            break;

           case SPIN_STATUS.GET_SCATTER_ING:
            this.GetScatterEffecting();
            break;

           case SPIN_STATUS.GET_SCATTER_END:
            this.GetScatterEffectEnd();
            break;

           case SPIN_STATUS.JACKPOT_START:
            this.getJackpotStart();
            break;

           case SPIN_STATUS.JACKPOT_ING:
            this.getJackpotIng();
            break;

           case SPIN_STATUS.REWARD_EFFECT_START:
            this.rewardEffectStart();
            break;

           case SPIN_STATUS.REWARD_EFFECT_ING:
            this.rewardEffecting();
            break;

           case SPIN_STATUS.REWARD_EFFECT_END:
            this.rewardEffectEnd();
            break;

           case SPIN_STATUS.WIN_SCORE_EFFECT_START:
            this.winScoreEffectStart();
            break;

           case SPIN_STATUS.WIN_SCORE_EFFECT_ING:
            this.winScoreEffectIng();
            break;

           case SPIN_STATUS.WIN_SCORE_EFFECT_END:
            this.winScoreEffectEnd();
            break;

           case SPIN_STATUS.FINISHED_WAIT:
            this.spinFinishedWait(dt);
            break;

           case SPIN_STATUS.FINISHED:
            this.spinFinished();
            break;

           case SPIN_STATUS.WAIT_REWARD:
            this.waitReward(dt);
            break;

           case SPIN_STATUS.SYMBOL_DISAPPEAR_START:
            this.symbolDisappearStart();
            break;

           case SPIN_STATUS.SYMBOL_DISAPPEAR_ING:
            this.symbolDisappearIng();
            break;

           case SPIN_STATUS.SYMBOL_DISAPPEAR_END:
            this.symbolDisappearEnd();
            break;

           case SPIN_STATUS.SYMBOL_DISAPPEAR_DROP_START:
            this.symbolDisappearDropStart();
            break;

           case SPIN_STATUS.SYMBOL_DISAPPEAR_DROP_ING:
            this.symbolDisappearDropIng();
            break;

           case SPIN_STATUS.SYMBOL_DISAPPEAR_DROP_END:
            this.symbolDisappearDropEnd(dt);
          }
        } else if (this.gameStatus == GAME_STATUS.FREE_GAME) switch (this.spinStatus) {
         case SPIN_STATUS.FG_START:
          this.freeGameStart();
          break;

         case SPIN_STATUS.FG_ING:
          this.freeGameIng();
        } else if (this.gameStatus == GAME_STATUS.IDLE && this.isChangeMoney) {
          this.isChangeMoney = false;
          this.data.coin = this.data.getClientMoney(this.newMoney);
          this.ui.setScore(this.data.coin);
        }
      };
      G48_Game.prototype.freeGameStart = function() {
        console.log("\u3010\u514d\u8cbb\u904a\u6232\u52d5\u756b\u3011 \u958b\u59cb");
        var ans = this.freeGameAnsIndex;
        var symbolArr = [];
        var path = [];
        this.ui.freegameSymbolLayer.forEach(function(layer, layerIndex) {
          symbolArr[layerIndex] = [];
          layer.children.forEach(function() {
            symbolArr[layerIndex].push(1);
          });
        });
        path.push(ans);
        for (var startIndex = symbolArr.length - 2; startIndex >= 0; startIndex--) {
          ans = ans > 0 ? Math.floor(2 * Math.random()) > 0 && ans < symbolArr[startIndex].length ? ans : ans - 1 : ans;
          path.unshift(ans);
        }
        this.ui.freegameRunScatter(path);
        this.spinStatus = SPIN_STATUS.FG_ING;
      };
      G48_Game.prototype.freeGameIng = function() {
        var _this = this;
        if (!this.ui.isFreegameStart) {
          this.gameStatus = GAME_STATUS.FREE_GAME_REWARD;
          this.spinStatus = SPIN_STATUS.NONE;
          this.scheduleOnce(function() {
            _this.ui.hideFreeGameOddsTip();
            console.log("\u958b\u555f\u514d\u8cbb\u904a\u6232\u7d50\u7b97\u9762\u677f");
            AudioManager_1.AudioManager.instance.playAudioEvent("OpenFreeGameGetScorePanel");
            var _freeGameWinMoney = _this.data.getClientMoney(_this.data.resultList[0].dragonBall.winBonus);
            var _betScore = _this.data.playerBetScore;
            _this.ui.openFreeGameGetScorePanel(_freeGameWinMoney, _betScore);
            _this.ui.setSpinArrowSpinningSpeed(Slot_GameUI_1.SPIN_ARROW_SPINNIG_TYPE.NORMAL);
            _this.scheduleOnce(function() {
              _this.gameStatus == GAME_STATUS.FREE_GAME_REWARD && _this.freeGameEnd();
            }, 5);
          }, .7);
        }
      };
      G48_Game.prototype.registerEvents = function() {
        var _this = this;
        G48_SocketManager_1.default.getInstance().setBalanceModifyCallback(function(data) {
          _this.onBalanceModifyEvent(data);
        });
        G48_SocketManager_1.default.getInstance().setRefreshJackpotCallback(function(data) {
          _this.ui && _this.ui.refreshJackpot(data);
        });
      };
      G48_Game.prototype.showSymbolTip = function(symbolID, position) {
        this.gameStatus == GAME_STATUS.IDLE && _super.prototype.showSymbolTip.call(this, symbolID, position);
      };
      G48_Game.prototype.baseGameSpin_Response = function(p_data) {
        this.ui.showSpinMask();
        if (this.data.isAutoSpin) {
          this.autoSpinCount++;
          this.ui.openCancelAutoSpinButton();
          var _autoSpinCount = this.data.autoGameSpinCount - this.autoSpinCount;
          -1 == this.data.autoGameSpinCount && (_autoSpinCount = -1);
          this.ui.setAutoSpinCount(_autoSpinCount);
        }
        this.tempSpinCD = 0;
        this.toFastStopSpin = false;
        this.resultListIndex = 0;
        this.data.coin = this.data.getClientMoney(p_data.currentCash);
        this.data.totalBonus = this.data.getClientMoney(p_data.totalBonus);
        this.data.playerBetScore = this.data.getClientMoney(p_data.totalBet);
        this.data.resultList = p_data.resultList;
        this.data.remainingFreeGameCount = this.data.resultList.length - 1;
        this.data.getJackpot = p_data.getJackpot;
        this.eliminateListIndex = 0;
        var _money = this.data.getClientMoney(p_data.currentCash - p_data.totalBonus);
        this.ui.setScore(_money);
        this.ui.setWinScore(0);
        this.ui.setSpinArrowSpinningSpeed(Slot_GameUI_1.SPIN_ARROW_SPINNIG_TYPE.FAST);
        this.gameStatus = GAME_STATUS.SPIN;
        this.spinStatus = SPIN_STATUS.REEL_SCROLL_START;
        this.ui.spinButton.getComponent(cc.Button).interactable = true;
      };
      G48_Game.prototype.onSpin = function() {
        var _this = this;
        console.log("Spin");
        if (this.gameStatus == GAME_STATUS.SPIN && this.spinStatus == SPIN_STATUS.REEL_SCROLL_ING) this.toFastStopSpin = true; else if (this.gameStatus == GAME_STATUS.IDLE) {
          var _betScore = this.data.betOdds[this.data.betOddsStartIndex];
          if (this.hasEnoughBetScore()) {
            this.data.isAutoSpin ? this.ui.showSpinArrow() : this.ui.showSpinButtonClickEffect();
            AudioManager_1.AudioManager.instance.playAudioEvent("StartSpin");
            _super.prototype.onSpin.call(this);
            this.gameStatus = GAME_STATUS.SERVER_REQUEST;
            this.socket.setOnSpinBaseGameEvent(_betScore, this.data.drills[this.data.drillIndex]).then(function(baseGameSpinResponeData) {
              _this.showResponseLog("SpinBaseGame", baseGameSpinResponeData);
              _this.baseGameSpin_Response(baseGameSpinResponeData);
            }).catch(function(error) {
              error.code == GameClient.errCode.RESPONSE_TIMED_OUT ? _this.socket.showErrorAlert(LocalizationManager_1.default.getInstance().get("ERROR_SOCKET_TIME_OUT"), null, function() {
                G48_SocketManager_1.default.getInstance().login();
              }) : error.code == GameClient.errCode.SLOT_INSUFFICIENT_BALANCE && _this.socket.showErrorAlert(LocalizationManager_1.default.getInstance().get("ERROR_NO_COIN"), null, function() {
                if (_this.data.isAutoSpin) {
                  _this.onCancelAutoSpinButton();
                  _this.ui.hideSpinMask();
                  _this.ui.showSpinButtonIdleEffect();
                  _this.ui.setSpinArrowSpinningSpeed(Slot_GameUI_1.SPIN_ARROW_SPINNIG_TYPE.NORMAL);
                }
              });
              _this.ui.spinButton.getComponent(cc.Button).interactable = true;
              _this.gameStatus = GAME_STATUS.IDLE;
            });
          } else {
            var requireMoney = MathUtils_1.MathUtils.times(_betScore, this.data.oneOddsScore, this.data.drills[this.data.drillIndex]);
            this.showLackMoneyPanel(requireMoney);
            this.data.isAutoSpin && this.onCancelAutoSpinButton();
          }
        }
      };
      G48_Game.prototype.stopSpin = function() {
        AudioManager_1.AudioManager.instance.playAudioEvent("StopSpin");
        this.slotReelManager.stopAllReelScroll();
      };
      G48_Game.prototype.onCloseWinEffect = function() {
        this.ui.winScoreIsEndScore() ? this.winScoreEffectEnd() : this.ui.winScoreEffectToEndScore();
      };
      G48_Game.prototype.onStartAutoGame = function() {
        console.log("\u8f38\u5206\u505c\u6b62 " + this.data.autoGameScoreLessStop);
        console.log("\u8d0f\u5206\u505c\u6b62 " + this.data.autoGameWinScoreStop);
        AudioManager_1.AudioManager.instance.playAudioEvent("StartAutoGame");
        this.data.isAutoSpin = true;
        this.ui.autoSpinSwitch(true);
        this.autoSpinCount = 0;
        this.data.autoGameStartScore = this.data.coin;
        this.onSpin();
      };
      G48_Game.prototype.startFreeGame = function() {
        var _this = this;
        if (this.gameStatus == GAME_STATUS.CHANGE_PANEL) return;
        console.log("\u95dc\u9589\u514d\u8cbb\u904a\u6232\u9762\u677f");
        this.gameStatus = GAME_STATUS.CHANGE_PANEL;
        clearTimeout(this.freegameTimeout);
        AudioManager_1.AudioManager.instance.playAudioEvent("StarFreeGame");
        this.ui.closeFreeGamePanel().then(function() {
          console.log("\u958b\u59cb\u514d\u8cbb\u904a\u6232");
          _this.gameStatus = GAME_STATUS.FREE_GAME;
          _this.spinStatus = SPIN_STATUS.FG_START;
        });
      };
      G48_Game.prototype.onAddDrill = function() {
        console.log("\u4e0b\u6ce8 \u589e\u52a0");
        if (this.data.drillIndex >= this.data.drills.length - 1) {
          AudioManager_1.AudioManager.instance.playAudioEvent("AddScoreEnd");
          return;
        }
        AudioManager_1.AudioManager.instance.playAudioEvent("AddScore");
        this.data.drillIndex++;
        this.ui.setDrillScore(this.data.drills[this.data.drillIndex]);
        this.data.saveDrillIndex(this.data.drillIndex);
      };
      G48_Game.prototype.onReduceDrill = function() {
        console.log("\u4e0b\u6ce8 \u6e1b\u5c11");
        if (0 == this.data.drillIndex) {
          AudioManager_1.AudioManager.instance.playAudioEvent("ReduceScoreEnd");
          return;
        }
        AudioManager_1.AudioManager.instance.playAudioEvent("ReduceScore");
        this.data.drillIndex--;
        this.ui.setDrillScore(this.data.drills[this.data.drillIndex]);
        this.data.saveDrillIndex(this.data.drillIndex);
      };
      G48_Game.prototype.freeGameEnd = function() {
        var _this = this;
        if (this.gameStatus == GAME_STATUS.CHANGE_PANEL) return;
        _super.prototype.freeGameEnd.call(this);
        console.log("\u95dc\u9589\u514d\u8cbb\u904a\u6232\u5f97\u5206\u9762\u677f");
        var _money = this.data.coin;
        var _winMoney = this.data.totalBonus;
        this.gameStatus = GAME_STATUS.CHANGE_PANEL;
        this.ui.closeWinScoreEffect();
        this.ui.closeFreeGameGetScorePanel().then(function() {
          console.log("\u514d\u8cbb\u904a\u6232\u7d50\u675f");
          AudioManager_1.AudioManager.instance.playAudioEvent("FreeGameEnd");
          _this.gameStatus = GAME_STATUS.IDLE;
          _this.spinStatus = SPIN_STATUS.NONE;
          _this.checkAutoGameContinue();
        });
        this.ui.hideRemainingFreeGameCount();
        this.ui.changeToBaseGameBackGround();
        this.setLevel(1);
        this.curBrick = 0;
        this.setBrick(0, true);
        this.ui.menuButtonSwitch(true);
        this.ui.buttonGroupSwitch(true);
        this.ui.setScore(_money);
        this.ui.setWinScore(_winMoney);
        this.ui.showMainGameTip();
        this.ui.hideGetScoreTip();
        this.data.isFaseSpin && this.slotReelManager.setFastMode();
      };
      G48_Game.prototype.onOpenRule = function() {
        console.log("\u958b\u555f\u898f\u5247");
        AudioManager_1.AudioManager.instance.playAudioEvent("OpenRule");
        var _betScore = this.data.betOdds[this.data.betOddsStartIndex];
        var _slotBetAmount = this.data.getClientMoney(MathUtils_1.MathUtils.times(_betScore, this.data.oneOddsScore, this.data.drills[this.data.drillIndex]));
        this.ui.openRule(_slotBetAmount);
      };
      G48_Game.prototype.reelScrollStart = function() {
        console.log("\u3010\u8f2a\u5b50\u6efe\u52d5\u3011 \u958b\u59cb");
        AudioManager_1.AudioManager.instance.playAudioEvent("ReelScrollStart");
        this.slotReelManager.setReelTable(this.data.reelTable_ALLBG[this.curLevel - 1]);
        var _targetIndex = this.data.resultList[this.resultListIndex].randomNumList;
        var _scrollData = new Slot_ReelManager_ScrollData_1.default();
        _scrollData.targetSymbolIndex = _targetIndex;
        _scrollData.hasOmenReels = [ false, false, false, false, false, false ];
        _scrollData.reels = this.data.resultList[this.resultListIndex].reels;
        _scrollData.isFreeGame = this.gameStatus == GAME_STATUS.FREE_GAME;
        _scrollData.isSkipDropOut = true;
        _scrollData.reels_Above = this.data.resultList[this.resultListIndex].reels_Above;
        this.data.isAutoSpin || this.gameStatus == GAME_STATUS.FREE_GAME ? _scrollData.rewardTime = this.autoRewardTime : _scrollData.rewardTime = this.normalRewardTime;
        this.ui.hideGetScoreTip();
        this.slotReelManager.scrollAllReels(_scrollData);
        this.spinStatus = SPIN_STATUS.REEL_SCROLL_ING;
        if (this.gameStatus == GAME_STATUS.FREE_GAME) {
          this.data.remainingFreeGameCount--;
          this.ui.setRemainingFreeGameCount(this.data.remainingFreeGameCount);
          this.ui.showFreeGameTip();
        } else this.ui.showMainGameTip();
      };
      G48_Game.prototype.reelScrolling = function() {
        this.slotReelManager.isReelSrolling() || (this.spinStatus = SPIN_STATUS.REEL_SCROLL_END);
      };
      G48_Game.prototype.reelScrollEnd = function() {
        console.log("\u3010\u8f2a\u5b50\u6efe\u52d5\u3011\u7d50\u675f");
        AudioManager_1.AudioManager.instance.playAudioEvent("AllReelStop");
        this.spinStatus = SPIN_STATUS.CHECK_REWARD;
      };
      G48_Game.prototype.checkReward = function() {
        console.log("\u6aa2\u67e5\u4e2d\u734e");
        var _winScore = this.data.getClientMoney(this.data.resultList[this.resultListIndex].totalWinBonus);
        this.hasEliminateData() && this.data.resultList[this.resultListIndex].eliminateList[this.eliminateListIndex].isEliminate || this.data.getJackpot ? this.spinStatus = SPIN_STATUS.REWARD_EFFECT_START : this.hasWinScoreEffect(_winScore) ? this.spinStatus = SPIN_STATUS.WIN_SCORE_EFFECT_START : this.spinStatus = SPIN_STATUS.FINISHED_WAIT;
      };
      G48_Game.prototype.hasEliminateData = function() {
        if (null == this.data.resultList[this.resultListIndex].eliminateList) return false;
        if (this.eliminateListIndex < this.data.resultList[this.resultListIndex].eliminateList.length) return true;
        return false;
      };
      G48_Game.prototype.GetScatterEffectStart = function() {
        return __awaiter(this, void 0, Promise, function() {
          var _scatterPos, _nextReels, _nextReelsAbove;
          return __generator(this, function(_a) {
            console.log(" \u3010Get Scatter\u3011 \u958b\u59cb");
            _scatterPos = this.data.resultList[this.resultListIndex].eliminateList[this.eliminateListIndex].scatter.scatterPos;
            _nextReels = this.data.resultList[this.resultListIndex].eliminateList[this.eliminateListIndex].nextReels;
            _nextReelsAbove = this.data.resultList[this.resultListIndex].eliminateList[this.eliminateListIndex].nextReels_Above;
            this.ui.playRewardEffect();
            this.slotReelManager.showSymbolDisappearEffect(_scatterPos, _nextReels, _nextReelsAbove);
            this.setBrick(this.data.resultList[this.resultListIndex].eliminateList[this.eliminateListIndex].scatter.count, false);
            this.spinStatus = SPIN_STATUS.GET_SCATTER_ING;
            return [ 2 ];
          });
        });
      };
      G48_Game.prototype.GetScatterEffecting = function() {
        this.slotReelManager.isSymbolDisappearEffecting() || (this.spinStatus = SPIN_STATUS.GET_SCATTER_END);
      };
      G48_Game.prototype.GetScatterEffectEnd = function() {
        console.log(" \u3010Get Scatter\u3011 \u7d50\u675f");
        this.spinStatus = SPIN_STATUS.SYMBOL_DISAPPEAR_DROP_START;
      };
      G48_Game.prototype.getJackpotStart = function() {
        return __awaiter(this, void 0, Promise, function() {
          var _winScore, count;
          var _this = this;
          return __generator(this, function(_a) {
            console.log(" \u3010Get Jackpot\u3011 \u958b\u59cb");
            _winScore = this.data.getClientMoney(this.data.resultList[this.resultListIndex].jackpotBonus.winBonus);
            count = this.data.resultList[this.resultListIndex].jackpotBonus.count;
            switch (count) {
             case 1:
              AudioManager_1.AudioManager.instance.playAudioEvent("JackPotMini");
              break;

             case 2:
              AudioManager_1.AudioManager.instance.playAudioEvent("JackPotMinor");
              break;

             case 3:
              AudioManager_1.AudioManager.instance.playAudioEvent("JackPotMajor");
              break;

             case 4:
              AudioManager_1.AudioManager.instance.playAudioEvent("JackPotGrand");
            }
            this.ui.openJackpotPanel(_winScore, count);
            this.scheduleOnce(function() {
              _this.slotReelManager.hideMaskEffect();
              _this.ui.closeJackpotPanel();
            }, 5);
            this.spinStatus = SPIN_STATUS.JACKPOT_ING;
            return [ 2 ];
          });
        });
      };
      G48_Game.prototype.getJackpotIng = function() {
        if (!this.ui.jackpotPanel.node.active) {
          AudioManager_1.AudioManager.instance.playAudioEvent("FreeGameEnd");
          this.spinStatus = SPIN_STATUS.FINISHED;
        }
      };
      G48_Game.prototype.rewardEffectStart = function() {
        console.log("\u3010\u4e2d\u734e\u7279\u6548\u3011 \u958b\u59cb");
        var _winScore = this.data.getClientMoney(this.data.resultList[this.resultListIndex].eliminateList[this.eliminateListIndex].eliWinBonus);
        var _winPositstion = this.data.resultList[this.resultListIndex].eliminateList[this.eliminateListIndex].eliAllPos;
        if (this.data.getJackpot) {
          _winPositstion = this.data.resultList[this.resultListIndex].jackpotBonus.jackpotPos;
          _winScore = this.data.getClientMoney(this.data.resultList[this.resultListIndex].jackpotBonus.winBonus);
        }
        var _winJoinList = this.data.resultList[this.resultListIndex].eliminateList[this.eliminateListIndex].winJoinList;
        this.ui.addScore(_winScore);
        this.ui.addWinScore(_winScore);
        this.ui.showWinList(_winJoinList, this.curLevel);
        this.ui.hideGameTip();
        this.slotReelManager.showSymbolReWardEffect(_winPositstion, true);
        this.playRewardSound();
        this.spinStatus = SPIN_STATUS.REWARD_EFFECT_ING;
      };
      G48_Game.prototype.playRewardSound = function() {
        var rewardAudio = this.eliminateListIndex > 1 ? "PlayRewardMain3" : this.eliminateListIndex > 0 ? "PlayRewardMain2" : "PlayRewardMain";
        AudioManager_1.AudioManager.instance.playAudioEvent(rewardAudio);
        console.log("\u64ad\u653e\u4e2d\u734e\u97f3\u6548");
      };
      G48_Game.prototype.rewardEffecting = function() {
        this.slotReelManager.isRewardEffecting() || (this.spinStatus = SPIN_STATUS.REWARD_EFFECT_END);
      };
      G48_Game.prototype.rewardEffectEnd = function() {
        console.log("\u3010\u4e2d\u734e\u7279\u6548\u3011 \u7d50\u675f");
        var _isEliminate = this.data.resultList[this.resultListIndex].eliminateList[this.eliminateListIndex].isEliminate;
        var _winBouns = this.data.getClientMoney(this.data.resultList[this.resultListIndex].totalWinBonus);
        if (_isEliminate) this.data.resultList[this.resultListIndex].eliminateList[this.eliminateListIndex].scatter.count > 0 ? this.spinStatus = SPIN_STATUS.GET_SCATTER_START : this.spinStatus = SPIN_STATUS.SYMBOL_DISAPPEAR_START; else if (this.data.getJackpot) this.spinStatus = SPIN_STATUS.JACKPOT_START; else {
          this.eliminateListIndex++;
          this.hasEliminateData() ? this.spinStatus = SPIN_STATUS.WAIT_REWARD : !this.hasFreeGame() && this.hasWinScoreEffect(_winBouns) ? this.spinStatus = SPIN_STATUS.WIN_SCORE_EFFECT_START : this.hasWinScoreEffect(_winBouns) ? this.spinStatus = SPIN_STATUS.WIN_SCORE_EFFECT_START : this.spinStatus = SPIN_STATUS.FINISHED_WAIT;
        }
      };
      G48_Game.prototype.waitReward = function(dt) {
        this.waitRewardTime += dt;
        if (this.waitRewardTime > 1) {
          this.slotReelManager.allSymbolPlayIdleEffect();
          this.spinStatus = SPIN_STATUS.CHECK_REWARD;
          this.waitRewardTime = 0;
        }
      };
      G48_Game.prototype.symbolDisappearStart = function() {
        console.log("\u3010\u7b26\u865f\u6d88\u9664\u3011 \u958b\u59cb");
        var _winPositstion = this.data.resultList[this.resultListIndex].eliminateList[this.eliminateListIndex].eliHitPos;
        var _nextReels = this.data.resultList[this.resultListIndex].eliminateList[this.eliminateListIndex].nextReels;
        var _nextReelsAbove = this.data.resultList[this.resultListIndex].eliminateList[this.eliminateListIndex].nextReels_Above;
        AudioManager_1.AudioManager.instance.playAudioEvent("BubbleEffect");
        this.slotReelManager.showSymbolDisappearEffect(_winPositstion, _nextReels, _nextReelsAbove);
        var delayTime = .15;
        for (var i = 0; i < _winPositstion.length; i++) {
          if (-1 !== _winPositstion[i].indexOf(1)) break;
          delayTime += .05;
        }
        this.ui.shakeBG(delayTime);
        this.ui.playRewardEffect();
        this.spinStatus = SPIN_STATUS.SYMBOL_DISAPPEAR_ING;
      };
      G48_Game.prototype.symbolDisappearIng = function() {
        this.slotReelManager.isSymbolDisappearEffecting() || (this.spinStatus = SPIN_STATUS.SYMBOL_DISAPPEAR_END);
      };
      G48_Game.prototype.symbolDisappearEnd = function() {
        console.log("\u3010\u7b26\u865f\u6d88\u9664\u3011 \u7d50\u675f");
        this.spinStatus = SPIN_STATUS.SYMBOL_DISAPPEAR_DROP_START;
      };
      G48_Game.prototype.symbolDisappearDropStart = function() {
        console.log("\u3010\u7b26\u865f\u6d88\u9664\u5f8c\u6389\u843d\u3011 \u958b\u59cb");
        AudioManager_1.AudioManager.instance.playAudioEvent("ReelScrollStartAgain");
        var _winPositstion = this.data.resultList[this.resultListIndex].eliminateList[this.eliminateListIndex].eliAllPos;
        this.slotReelManager.ShowSymbolDisappearDropEffect(_winPositstion);
        this.spinStatus = SPIN_STATUS.SYMBOL_DISAPPEAR_DROP_ING;
      };
      G48_Game.prototype.symbolDisappearDropIng = function() {
        this.slotReelManager.isSymbolDisappearDropEffecting() || (this.spinStatus = SPIN_STATUS.SYMBOL_DISAPPEAR_DROP_END);
      };
      G48_Game.prototype.symbolDisappearDropEnd = function(dt) {
        this.waitShowTime += dt;
        if (this.waitShowTime > .5) {
          console.log("\u3010\u7b26\u865f\u6d88\u9664\u5f8c\u6389\u843d\u3011 \u7d50\u675f");
          this.eliminateListIndex++;
          this.spinStatus = SPIN_STATUS.CHECK_REWARD;
          this.waitShowTime = 0;
        }
      };
      G48_Game.prototype.winScoreEffectStart = function() {
        console.log("\u3010 \u8d0f\u9322\u6548\u679c \u3011\u958b\u59cb");
        this.spinStatus = SPIN_STATUS.WIN_SCORE_EFFECT_ING;
        var _totalWinScore = this.data.getClientMoney(this.data.resultList[this.resultListIndex].totalWinBonus);
        var _betScore = this.data.playerBetScore;
        var _isFreeGame = this.gameStatus === GAME_STATUS.FREE_GAME;
        this.ui.showWinScoreEffect(_totalWinScore, _betScore, _isFreeGame);
      };
      G48_Game.prototype.winScoreEffectIng = function() {
        this.ui.isWinScoreEffecting() || (this.spinStatus = SPIN_STATUS.WIN_SCORE_EFFECT_END);
      };
      G48_Game.prototype.winScoreEffectEnd = function() {
        console.log("\u3010 \u8d0f\u9322\u6548\u679c \u3011\u7d50\u675f");
        this.spinStatus = SPIN_STATUS.FINISHED_WAIT;
        this.gameStatus == GAME_STATUS.FREE_GAME ? AudioManager_1.AudioManager.instance.playAudioEvent("CloseWinScorePanelFreeGame") : AudioManager_1.AudioManager.instance.playAudioEvent("CloseWinScorePanel");
        this.ui.closeWinScoreEffect();
      };
      G48_Game.prototype.spinFinishedWait = function(p_dt) {
        if (0 == this.tempWaitTime) {
          var _totalWinScore = this.data.getClientMoney(this.data.resultList[this.resultListIndex].totalWinBonus);
          this.data.isAutoSpin || this.gameStatus == GAME_STATUS.FREE_GAME ? this.tempSpinFinishedStopTime = this.fastSpinFinishedStopTime : this.tempSpinFinishedStopTime = this.normalSpinFinishedStopTime;
          if (_totalWinScore > 0) {
            AudioManager_1.AudioManager.instance.playAudioEvent("WinScore");
            this.ui.showTotalGetScore(_totalWinScore);
            this.data.isAutoSpin ? this.tempSpinFinishedStopTime += this.autoSpinUpdateTipBroadEffectTime : this.tempSpinFinishedStopTime += this.normalUpdateTipBroadEffectTime;
          }
        }
        this.tempWaitTime += p_dt;
        if (this.tempWaitTime > this.tempSpinFinishedStopTime) {
          this.spinStatus = SPIN_STATUS.FINISHED;
          this.tempWaitTime = 0;
        }
      };
      G48_Game.prototype.spinFinished = function() {
        return __awaiter(this, void 0, Promise, function() {
          var _this = this;
          return __generator(this, function(_a) {
            console.log("\u3010Spin\u3011 \u7d50\u675f");
            if (this.gameStatus !== GAME_STATUS.FREE_GAME) {
              this.slotReelManager.node.getComponentInChildren(cc.Mask).enabled = false;
              this.slotReelManager.node.setSiblingIndex(999);
              this.gameStatus = GAME_STATUS.THROW_SYMBOL;
              AudioManager_1.AudioManager.instance.playAudioEvent("SymbolDropStart");
              this.slotReelManager.showAllSymbolDropOutEffect().then(function() {
                _this.ui.setWinScore(0);
                _this.slotReelManager.node.getComponentInChildren(cc.Mask).enabled = true;
                _this.slotReelManager.node.setSiblingIndex(1);
                if (_this.hasFreeGame()) {
                  _this.gameStatus = GAME_STATUS.FREE_GAME_TIP;
                  var _freeGameCount = _this.data.resultList.length - 1;
                  _this.freeGameAnsIndex = Math.floor(5 * Math.random());
                  console.log("\u958b\u8d77\u514d\u8cbb\u904a\u6232\u9762\u677f");
                  AudioManager_1.AudioManager.instance.playAudioEvent("OpenFreeGamePanel");
                  _this.ui.openFreeGamePanel(_freeGameCount);
                  _this.ui.setLevel(4);
                  _this.ui.changeToFreeGameBackGround(_this.data.resultList[_this.resultListIndex].dragonBall, _this.freeGameAnsIndex);
                  _this.slotReelManager.resetLevel1();
                  _this.ui.menuButtonSwitch(false);
                  _this.ui.buttonGroupSwitch(false);
                  _this.slotReelManager.hideAllReelSymbol();
                  _this.freegameTimeout = window.setTimeout(function() {
                    _this.gameStatus == GAME_STATUS.FREE_GAME_TIP && _this.startFreeGame();
                  }, 15e3);
                } else {
                  1 === _this.curLevel && _this.curBrick > 14 ? _this.setLevel(2) : 2 === _this.curLevel && _this.curBrick > 29 && _this.setLevel(3);
                  _this.gameStatus = GAME_STATUS.IDLE;
                  _this.spinStatus = SPIN_STATUS.NONE;
                  _this.slotReelManager.hideAllReelSymbol();
                  _this.checkAutoGameContinue();
                }
              });
            }
            return [ 2 ];
          });
        });
      };
      G48_Game.prototype.hasFreeGame = function() {
        if (3 === this.curLevel && this.curBrick > 44) return true;
        return false;
      };
      G48_Game.prototype.setReelInfo = function(p_data) {
        this.data.reelStartIndex = p_data.position;
        this.data.reelTable_BG = p_data.reels.BG[this.curLevel - 1];
        this.data.reelTable_ALLBG = p_data.reels.BG;
        this.slotReelManager.setReelTable(this.data.reelTable_BG);
        this.slotReelManager.setReelStartIndex(this.data.reelStartIndex);
      };
      G48_Game.prototype.onErrorEvent = function(_a) {
        var errCode = _a.errCode;
        console.error("\u6536\u5230\u932f\u8aa4\u8a0a\u606f", errCode);
      };
      G48_Game.prototype.playerLogin = function() {
        this.data.betOddsStartIndex = this.data.getBetIndex();
        this.data.drillIndex = this.data.getDrillIndex();
        var _money = this.data.coin;
        var _betMoney = this.data.getClientMoney(this.data.betOdds[this.data.betOddsStartIndex] * this.data.oneOddsScore);
        this.ui.setScore(_money);
        this.ui.setBetScore(_betMoney);
        this.ui.setDrillScore(this.data.drills[this.data.drillIndex]);
        this.ui.setWinScore(0);
      };
      G48_Game.prototype.setLevel = function(level) {
        this.curLevel = level;
        this.ui.setLevel(level);
        this.slotReelManager.upLevel(this.curLevel);
      };
      G48_Game.prototype.setBrick = function(scatterNum, isInit) {
        this.curBrick += scatterNum;
        this.ui.setBrick(this.curBrick, isInit);
      };
      G48_Game.prototype.onOpenExitGamePanel = function() {
        this.ui.openExitGamePanel();
      };
      G48_Game.prototype.onCloseExitGamePanel = function() {
        this.ui.closeExitGamePanel();
      };
      G48_Game.prototype.saveGameAndExit = function() {
        var _this = this;
        this.socket.setOnStashEvent().then(function() {
          _this.onExitGame();
        });
      };
      G48_Game.prototype.hasEnoughBetScore = function() {
        var _betScore = this.data.betOdds[this.data.betOddsStartIndex];
        var drill = this.data.drills[this.data.drillIndex];
        if (this.data.coin < this.data.getClientMoney(_betScore * this.data.oneOddsScore * drill)) return false;
        return true;
      };
      __decorate([ property({
        type: G48_SlotReelManager_1.default,
        override: true
      }) ], G48_Game.prototype, "slotReelManager", void 0);
      __decorate([ property({
        type: cc.Float,
        displayName: "\u4e2d\u734e\u7b2c\u4e00\u6b21\u63db\u7dda\u7684\u6642\u9593"
      }) ], G48_Game.prototype, "firstWinLineChangeTime", void 0);
      __decorate([ property({
        type: cc.Float,
        displayName: "\u4e2d\u734e\u7dda\u66f4\u63db\u6642\u9593"
      }) ], G48_Game.prototype, "winLineChangeTime", void 0);
      __decorate([ property({
        type: cc.Float,
        displayName: "\u4e2d\u734e\u7dda\u66f4\u63db\u7b49\u5f85\u6642\u9593"
      }) ], G48_Game.prototype, "winLineChangeWaitTime", void 0);
      __decorate([ property({
        type: G48_GameUI_1.default,
        override: true
      }) ], G48_Game.prototype, "ui", void 0);
      __decorate([ property({
        type: cc.Float,
        displayName: "spin\u51b7\u537b\u6642\u9593",
        tooltip: "\u958b\u59cbSpin\u5f8c\u7684\u51b7\u537b\u6642\u9593\uff0c\u54ea\u4f86\u8b93\u6efe\u8f2a\u6703\u7a0d\u5fae\u6efe\u52d5\u5728\u505c\u6b62"
      }) ], G48_Game.prototype, "spinCD", void 0);
      __decorate([ property({
        type: cc.Float,
        displayName: "\u4e00\u822cspin\u7d50\u675f\u505c\u6b62\u7b49\u5f85\u6642\u9593"
      }) ], G48_Game.prototype, "normalSpinFinishedStopTime", void 0);
      __decorate([ property({
        type: cc.Float,
        displayName: "\u5feb\u901fspin\u7d50\u675f\u505c\u6b62\u7b49\u5f85\u6642\u9593"
      }) ], G48_Game.prototype, "fastSpinFinishedStopTime", void 0);
      __decorate([ property({
        type: cc.Float,
        displayName: "\u624b\u52d5Spin \u4e2d\u734e\u66f4\u65b0\u63d0\u793a\u9762\u677f\u7279\u6548\u6642\u9593",
        tooltip: "\u53ea\u6709\u5728\u4e2d\u734e\u7684\u6642\u5019\u624d\u6703\u5403\u9019\u500b\u53c3\u6578"
      }) ], G48_Game.prototype, "normalUpdateTipBroadEffectTime", void 0);
      __decorate([ property({
        type: cc.Float,
        displayName: "\u81ea\u52d5Spin \u4e2d\u734e\u66f4\u65b0\u63d0\u793a\u9762\u677f\u7279\u6548\u6642\u9593",
        tooltip: "\u53ea\u6709\u5728\u4e2d\u734e\u7684\u6642\u5019\u624d\u6703\u5403\u9019\u500b\u53c3\u6578"
      }) ], G48_Game.prototype, "autoSpinUpdateTipBroadEffectTime", void 0);
      __decorate([ property({
        type: cc.Float,
        displayName: "\u624b\u52d5Spin\u4e2d\u734e\u6642\u9593"
      }) ], G48_Game.prototype, "normalRewardTime", void 0);
      __decorate([ property({
        type: cc.Float,
        displayName: "\u81ea\u52d5Spin\u4e2d\u734e\u6642\u9593"
      }) ], G48_Game.prototype, "autoRewardTime", void 0);
      __decorate([ property({
        type: cc.Float,
        displayName: "scatter\u986f\u793a\u7684\u6642\u9593"
      }) ], G48_Game.prototype, "scatterShowTime", void 0);
      G48_Game = __decorate([ ccclass ], G48_Game);
      return G48_Game;
    }(Slot_GameManager_1.default);
    exports.default = G48_Game;
    cc._RF.pop();
  }, {
    "../../Common/Tools/AudioManager/AudioManager": void 0,
    "../../Common/Tools/Localization/LocalizationManager": void 0,
    "../../Common/Tools/MathUtils": void 0,
    "../../SlotFramework/Game/Data/Slot_GameManager_InitData": void 0,
    "../../SlotFramework/Game/Data/Slot_GameUI_InitData": void 0,
    "../../SlotFramework/Game/Data/Slot_ReelManager_InitData": void 0,
    "../../SlotFramework/Game/Data/Slot_ReelManager_ScrollData": void 0,
    "../../SlotFramework/Game/Slot_GameManager": void 0,
    "../../SlotFramework/Game/view/Slot_GameUI": void 0,
    "../Common/G48_DataManager": "G48_DataManager",
    "../Common/G48_DynamicPopUpPanelManager": "G48_DynamicPopUpPanelManager",
    "../Common/Socket/G48_SocketManager": "G48_SocketManager",
    "./View/G48_GameUI": "G48_GameUI",
    "./View/G48_SlotReelManager": "G48_SlotReelManager"
  } ],
  G48_JackpotPanel: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "8d7112IdbJGS4u0Cdk/IJlh", "G48_JackpotPanel");
    "use strict";
    var __extends = this && this.__extends || function() {
      var extendStatics = function(d, b) {
        extendStatics = Object.setPrototypeOf || {
          __proto__: []
        } instanceof Array && function(d, b) {
          d.__proto__ = b;
        } || function(d, b) {
          for (var p in b) Object.prototype.hasOwnProperty.call(b, p) && (d[p] = b[p]);
        };
        return extendStatics(d, b);
      };
      return function(d, b) {
        extendStatics(d, b);
        function __() {
          this.constructor = d;
        }
        d.prototype = null === b ? Object.create(b) : (__.prototype = b.prototype, new __());
      };
    }();
    var __decorate = this && this.__decorate || function(decorators, target, key, desc) {
      var c = arguments.length, r = c < 3 ? target : null === desc ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
      if ("object" === typeof Reflect && "function" === typeof Reflect.decorate) r = Reflect.decorate(decorators, target, key, desc); else for (var i = decorators.length - 1; i >= 0; i--) (d = decorators[i]) && (r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r);
      return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    var FXController_1 = require("../../../Common/Optional/Crosis/FXController/FXController");
    var CommonTool_1 = require("../../../Common/Tools/CommonTool");
    var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
    var G48_JackpotPanel = function(_super) {
      __extends(G48_JackpotPanel, _super);
      function G48_JackpotPanel() {
        var _this = null !== _super && _super.apply(this, arguments) || this;
        _this.fxJackpotLevel = [];
        _this.score = null;
        return _this;
      }
      G48_JackpotPanel.prototype.init = function() {
        this.node.active = false;
      };
      G48_JackpotPanel.prototype.open = function(p_freeGameGetScore, level) {
        this.fxJackpotLevel.forEach(function(item) {
          return item.stop();
        });
        this.score.node.scale = 0;
        this.fxJackpotLevel[level - 1].play();
        this.fadeIn();
        this.node.setPosition(cc.Vec2.ZERO);
        this.score.string = CommonTool_1.CommonTool.getNumText(p_freeGameGetScore, 2, true, false);
        cc.tween(this.score.node).to(.3, {
          scale: 1
        }, {
          easing: cc.easing.cubicInOut
        }).start();
      };
      G48_JackpotPanel.prototype.close = function() {
        this.fadeOut();
      };
      G48_JackpotPanel.prototype.fadeIn = function() {
        this.node.opacity = 0;
        this.node.active = true;
        cc.tween(this.node).to(.3, {
          opacity: 255
        }, {
          easing: "cubicInOut"
        }).start();
      };
      G48_JackpotPanel.prototype.fadeOut = function() {
        var _this = this;
        cc.tween(this.node).to(.5, {
          opacity: 0
        }, {
          easing: "cubicInOut"
        }).call(function() {
          _this.node.active = false;
        }).start();
      };
      __decorate([ property([ FXController_1.default ]) ], G48_JackpotPanel.prototype, "fxJackpotLevel", void 0);
      __decorate([ property(cc.Label) ], G48_JackpotPanel.prototype, "score", void 0);
      G48_JackpotPanel = __decorate([ ccclass ], G48_JackpotPanel);
      return G48_JackpotPanel;
    }(cc.Component);
    exports.default = G48_JackpotPanel;
    cc._RF.pop();
  }, {
    "../../../Common/Optional/Crosis/FXController/FXController": void 0,
    "../../../Common/Tools/CommonTool": void 0
  } ],
  G48_Language: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "04c77DvDt1L5qR2Kse1rV3H", "G48_Language");
    "use strict";
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    exports.G48_Language = void 0;
    exports.G48_Language = {
      "zh-cn": {
        TEXT_SLOT_MONEY_LESS_SETTING: "\u6301\u6709\u91d1\u989d\u4f4e\u4e8e\u4f59\u989d\u51cf\u5c11\u8bbe\u5b9a\u91d1\u989d\uff0c\u8bf7\u8c03\u6574\u8bbe\u5b9a",
        TEXT_G48_LOADING: "\u73a9\u547d\u52a0\u8f7d\u4e2d...",
        TEXT_G48_SYMBOL_TIP_WILD: "WILD",
        TEXT_G48_SYMBOL_TIP_BONUS: "BOUNS",
        RULE_G48_GAME_ERROR: "\u6e38\u620f\u51fa\u73b0\u6545\u969c\u65f6\uff0c\u6240\u6709\u8d54\u4ed8\u548c\u6e38\u620f\u90fd\u89c6\u4e3a\u65e0\u6548",
        RULE_G48_1_1: "\u70b9\u51fb\u52a0\u51cf\u53f7\u6309\u94ae\uff0c\u8c03\u6574\u6295\u6ce8\u7684\u6570\u91cf\u53ca\u6295\u6ce8\u91d1\u989d",
        RULE_G48_1_2: "\u70b9\u51fb\u65cb\u8f6c\u6309\u94ae\u4ee5\u5f53\u524d\u7684\u6295\u6ce8\u8fdb\u884c\u6e38\u620f",
        RULE_G48_1_3: "\u70b9\u51fb\u81ea\u52a8\u6e38\u620f\u6309\u94ae\u4f1a\u5f00\u542f\u81ea\u52a8\u6e38\u620f\u8bbe\u7f6e\uff0c\u81ea\u52a8\u6e38\u620f\u542f\u7528\u65f6\uff0c\u70b9\u51fb\u65cb\u8f6c\u6309\u94ae\u53ef\u505c\u6b62\u81ea\u52a8\u6a21\u5f0f",
        RULE_G48_1_4: "\u663e\u793a\u6295\u6ce8\u7684\u6570\u91cf\uff0c\u6700\u5c0f\u4e3a 1\uff0c\u6700\u5927\u4e3a 5",
        RULE_G48_1_5: "\u663e\u793a\u6bcf\u6ce8\u7684\u6295\u6ce8\u91d1\u989d",
        RULE_G48_1_6: "A\uff1d\u6295\u6ce8\u6570\u91cf\nB\uff1d\u5355\u6ce8\u6295\u6ce8\u91d1\u989d\nC\uff1d\u56fe\u6807\u8fde\u7ebf\u8d54\u7387\uff08\u8bf7\u53c2\u8003\u8d54\u7387\u8868\uff09",
        RULE_G48_1_10: "\u4e2d\u5956\u91d1\u989d\uff1dB / 10 * C * A",
        RULE_G48_2_1: "\u76d8\u9762\u51fa\u73b01~4\u4e2a\u5f69\u91d1\u56fe\u6807\u53ca\u83b7\u5f97\u5927\u5956\u5f69\u91d1\uff0c\u6d3e\u5f69\u540e\u7ed3\u675f\u6b64\u5c40",
        RULE_G48_2_2: "\u56fe\u6807\u6570\u91cf",
        RULE_G48_2_3: "\u5927\u5956\u5f69\u91d1",
        RULE_G48_3_1: "\u540c\u4e00\u5173\u5185\u7d2f\u79ef\u6d88\u9664 15 \u4e2a\u4ee5\u4e0a\u94bb\u5934\u56fe\u6807\u5373\u53ef\u8fdb\u5165\u4e0b\u4e00\u5173\uff0c\u5f53\u5c40\u5269\u9980\u7684\u94bb\u5934\u53ef\u7d2f\u79ef\u81f3\u4e0b\u4e00\u5173\u4f7f\u7528\uff0c\u4f46\u7b2c\u4e09\u5173\u7ed3\u675f\u540e\u6240\u6709\u94bb\u5934\u4f1a\u6e05\u7a7a",
        RULE_G48_3_2: "\u6d88\u9664\u8fc7\u7a0b\u4e2d\u4f1a\u5148\u6d88\u9664\u94bb\u5934\uff0c\u5f85\u6e38\u620f\u6389\u843d\u65b0\u7684\u56fe\u6807\u540e\uff0c\u518d\u7ee7\u7eed\u6d88\u9664\u56fe\u6807",
        RULE_G48_3_3: "\u5173\u5361\u63d0\u5347\u540e\u4f1a\u8c03\u6574\u76d8\u9762\u5927\u5c0f\uff0c\n\u5177\u4f53\u5982\u4e0b\uff1a",
        RULE_G48_3_4: "\u7b2c\u4e00\u5173\uff1a4 x 4\n\u7b2c\u4e8c\u5173\uff1a5 x 5\n\u7b2c\u4e09\u5173\uff1a6 x 6",
        RULE_G48_4_1: "\u76f8\u8fde\u4e2a\u6570",
        RULE_G48_7_1: "\u6e38\u620f\u7b2c\u4e09\u5173\u7d2f\u79ef\u8d85\u8fc7 15 \u4e2a\u4ee5\u4e0a\u94bb\u5934\u56fe\u6807\uff0c\u5c31\u4f1a\u968f\u673a\u8fdb\u5165\u9f99\u73e0\u63a2\u5b9d\u6e38\u620f\uff0c\u4f9d\u7167\u9f99\u73e0\u6389\u843d\u7684\u533a\u57df\u83b7\u5f97\u5956\u91d1",
        RULE_G48_7_2: "\u53ef\u900f\u8fc7\u70b9\u51fb\u529f\u80fd\u5217\u7684\u79bb\u5f00\u6e38\u620f\u6309\u94ae\uff0c\u9009\u62e9\u4fdd\u7559\u72b6\u6001\u9000\u51fa\u6216\u653e\u5f03\u72b6\u6001\u9000\u51fa\uff1b\u4fdd\u7559\u72b6\u6001\u9000\u51fa\u53ef\u4fdd\u7559\u5173\u5361\u53ca\u94bb\u5934\u6570\uff1b\u653e\u5f03\u72b6\u6001\u9000\u51fa\u5373\u4e0d\u4fdd\u7559\u6e38\u620f\u5f53\u524d\u72b6\u6001",
        RULE_G48_7_3: "\u203b\u76f4\u63a5\u5173\u95ed\u6e38\u620f\u89c6\u4e3a\u81ea\u613f\u653e\u5f03\u6e38\u620f\u72b6\u6001",
        RULE_G48_8_1: "\u76d8\u9762\u4e0a\u76f8\u8fde\u6307\u5b9a\u6570\u91cf\u7684\u76f8\u540c\u56fe\u6807\uff0c\u5373\u53ef\u83b7\u5f97\u8be5\u56fe\u6807\u7684\u5f97\u5206\u5956\u52b1\uff0c\u659c\u7ebf\u4e0d\u7b97\u76f8\u8fde\uff0c\u4f9d\u5173\u5361\u4e0d\u540c\u6700\u4f4e\u4e2d\u5956\u7684\u76f8\u90bb\u6570\u91cf\u4e0d\u540c",
        TEXT_G48_EXITGAME_1: "\u9009\u62e9\u300c\u4fdd\u7559\u72b6\u6001\u9000\u51fa\u300d\u53ef\u4ee5\u4fdd\u7559\u5f53\u524d\u72b6\u6001\u5305\u542b\u5173\u5361\u53ca\u94bb\u5934\u6570\u3002",
        TEXT_G48_EXITGAME_2: "\u203b\u4fdd\u7559\u72b6\u6001\u6700\u591a\u4fdd\u5b58 24 \u5c0f\u65f6\uff01",
        TEXT_G48_EXITGAME_3: "\u9009\u62e9\u300c\u653e\u5f03\u72b6\u6001\u9000\u51fa\u300d\u5c06\u89c6\u4e3a\u60a8\u81ea\u613f\u653e\u5f03\u6e38\u620f\u5f53\u524d\u72b6\u6001\u53ca\u672c\u573a\u6e38\u620f\u70b9\u6570\u3002"
      },
      "zh-tw": {
        TEXT_SLOT_MONEY_LESS_SETTING: "\u6301\u6709\u91d1\u984d\u4f4e\u65bc\u9918\u984d\u6e1b\u5c11\u8a2d\u5b9a\u91d1\u984d\uff0c\u8acb\u8abf\u6574\u8a2d\u5b9a",
        TEXT_G48_LOADING: "\u73a9\u547d\u52a0\u8f09\u4e2d...",
        TEXT_G48_SYMBOL_TIP_WILD: "WILD",
        TEXT_G48_SYMBOL_TIP_BONUS: "BOUNS",
        RULE_G48_GAME_ERROR: "\u904a\u6232\u51fa\u73fe\u6545\u969c\u6642\uff0c\u6240\u6709\u8ce0\u4ed8\u548c\u904a\u6232\u90fd\u8996\u70ba\u7121\u6548",
        RULE_G48_1_1: "\u9ede\u64ca\u52a0\u6e1b\u865f\u6309\u9215\uff0c\u8abf\u6574\u6295\u6ce8\u7684\u6578\u91cf\u53ca\u6295\u6ce8\u91d1\u984d",
        RULE_G48_1_2: "\u9ede\u64ca\u65cb\u8f49\u6309\u9215\u4ee5\u7576\u524d\u7684\u6295\u6ce8\u9032\u884c\u904a\u6232",
        RULE_G48_1_3: "\u9ede\u64ca\u81ea\u52d5\u904a\u6232\u6309\u9215\u6703\u958b\u555f\u81ea\u52d5\u904a\u6232\u8a2d\u7f6e\uff0c\u81ea\u52d5\u904a\u6232\u555f\u7528\u6642\uff0c\u9ede\u64ca\u65cb\u8f49\u6309\u9215\u53ef\u505c\u6b62\u81ea\u52d5\u6a21\u5f0f",
        RULE_G48_1_4: "\u986f\u793a\u6295\u6ce8\u7684\u6578\u91cf\uff0c\u6700\u5c0f\u70ba 1\uff0c\u6700\u5927\u70ba 5",
        RULE_G48_1_5: "\u986f\u793a\u6bcf\u6ce8\u7684\u6295\u6ce8\u91d1\u984d",
        RULE_G48_1_6: "A\uff1d\u6295\u6ce8\u6578\u91cf\nB\uff1d\u55ae\u8a3b\u6295\u6ce8\u91d1\u984d\nC\uff1d\u5716\u6a19\u9023\u7dda\u8ce0\u7387\uff08\u8acb\u53c3\u8003\u8ce0\u7387\u8868\uff09",
        RULE_G48_1_10: "\u4e2d\u734e\u91d1\u984d\uff1dB / 10 * C * A",
        RULE_G48_2_1: "\u76e4\u9762\u51fa\u73fe1~4\u500b\u5f69\u91d1\u5716\u6a19\u53ca\u7372\u5f97\u5927\u734e\u5f69\u91d1\uff0c\u6d3e\u5f69\u5f8c\u7d50\u675f\u6b64\u5c40",
        RULE_G48_2_2: "\u5716\u6a19\u6578\u91cf",
        RULE_G48_2_3: "\u5927\u734e\u5f69\u91d1",
        RULE_G48_3_1: "\u540c\u4e00\u95dc\u5167\u7d2f\u7a4d\u6d88\u9664 15 \u500b\u4ee5\u4e0a\u947d\u982d\u5716\u6a19\u5373\u53ef\u9032\u5165\u4e0b\u4e00\u95dc\uff0c\u7576\u5c40\u5269\u9980\u7684\u947d\u982d\u53ef\u7d2f\u7a4d\u81f3\u4e0b\u4e00\u95dc\u4f7f\u7528\uff0c\u4f46\u7b2c\u4e09\u95dc\u7d50\u675f\u5f8c\u6240\u6709\u947d\u982d\u6703\u6e05\u7a7a",
        RULE_G48_3_2: "\u6d88\u9664\u904e\u7a0b\u4e2d\u6703\u5148\u6d88\u9664\u947d\u982d\uff0c\u5f85\u904a\u6232\u6389\u843d\u65b0\u7684\u5716\u6a19\u5f8c\uff0c\u518d\u7e7c\u7e8c\u6d88\u9664\u5716\u6a19",
        RULE_G48_3_3: "\u95dc\u5361\u63d0\u5347\u5f8c\u6703\u8abf\u6574\u76e4\u9762\u5927\u5c0f\uff0c\n\u5177\u9ad4\u5982\u4e0b\uff1a",
        RULE_G48_3_4: "\u7b2c\u4e00\u95dc\uff1a4 x 4\n\u7b2c\u4e8c\u95dc\uff1a5 x 5\n\u7b2c\u4e09\u95dc\uff1a6 x 6",
        RULE_G48_4_1: "\u76f8\u9023\u500b\u6578",
        RULE_G48_7_1: "\u904a\u6232\u7b2c\u4e09\u95dc\u7d2f\u7a4d\u8d85\u904e 15 \u500b\u4ee5\u4e0a\u947d\u982d\u5716\u6a19\uff0c\u5c31\u6703\u96a8\u6a5f\u9032\u5165\u9f8d\u73e0\u63a2\u5bf6\u904a\u6232\uff0c\u4f9d\u7167\u9f8d\u73e0\u6389\u843d\u7684\u5340\u57df\u7372\u5f97\u734e\u91d1",
        RULE_G48_7_2: "\u53ef\u900f\u904e\u9ede\u64ca\u529f\u80fd\u5217\u7684\u96e2\u958b\u904a\u6232\u6309\u9215\uff0c\u9078\u64c7\u4fdd\u7559\u72c0\u614b\u9000\u51fa\u6216\u653e\u68c4\u72c0\u614b\u9000\u51fa\uff1b\u4fdd\u7559\u72c0\u614b\u9000\u51fa\u53ef\u4fdd\u7559\u95dc\u5361\u53ca\u947d\u982d\u6578\uff1b\u653e\u68c4\u72c0\u614b\u9000\u51fa\u5373\u4e0d\u4fdd\u7559\u904a\u6232\u7576\u524d\u72c0\u614b",
        RULE_G48_7_3: "\u203b\u76f4\u63a5\u95dc\u9589\u904a\u6232\u8996\u70ba\u81ea\u9858\u653e\u68c4\u904a\u6232\u72c0\u614b",
        RULE_G48_8_1: "\u76e4\u9762\u4e0a\u76f8\u9023\u6307\u5b9a\u6578\u91cf\u7684\u76f8\u540c\u5716\u6a19\uff0c\u5373\u53ef\u7372\u5f97\u8a72\u5716\u6a19\u7684\u5f97\u5206\u734e\u52f5\uff0c\u659c\u7dda\u4e0d\u7b97\u76f8\u9023\uff0c\u4f9d\u95dc\u5361\u4e0d\u540c\u6700\u4f4e\u4e2d\u734e\u7684\u76f8\u9130\u6578\u91cf\u4e0d\u540c",
        TEXT_G48_EXITGAME_1: "\u9078\u64c7\u300c\u4fdd\u7559\u72c0\u614b\u9000\u51fa\u300d\u53ef\u4ee5\u4fdd\u7559\u7576\u524d\u72c0\u614b\u5305\u542b\u95dc\u5361\u53ca\u947d\u982d\u6578\u3002",
        TEXT_G48_EXITGAME_2: "\u203b\u4fdd\u7559\u72c0\u614b\u6700\u591a\u4fdd\u5b58 24 \u5c0f\u6642\uff01",
        TEXT_G48_EXITGAME_3: "\u9078\u64c7\u300c\u653e\u68c4\u72c0\u614b\u9000\u51fa\u300d\u5c07\u8996\u70ba\u60a8\u81ea\u9858\u653e\u68c4\u904a\u6232\u7576\u524d\u72c0\u614b\u53ca\u672c\u5834\u904a\u6232\u9ede\u6578\u3002"
      },
      "en-us": {
        TEXT_SLOT_MONEY_LESS_SETTING: "null",
        TEXT_G48_LOADING: "null",
        TEXT_G48_SYMBOL_TIP_WILD: "null",
        TEXT_G48_SYMBOL_TIP_BONUS: "null",
        RULE_G48_GAME_ERROR: "null",
        RULE_G48_1_1: "null",
        RULE_G48_1_2: "null",
        RULE_G48_1_3: "null",
        RULE_G48_1_4: "null",
        RULE_G48_1_5: "null",
        RULE_G48_1_6: "null",
        RULE_G48_1_10: "null",
        RULE_G48_2_1: "null",
        RULE_G48_2_2: "null",
        RULE_G48_2_3: "null",
        RULE_G48_3_1: "null",
        RULE_G48_3_2: "null",
        RULE_G48_3_3: "null",
        RULE_G48_3_4: "null",
        RULE_G48_4_1: "null",
        RULE_G48_7_1: "null",
        RULE_G48_7_2: "null",
        RULE_G48_7_3: "null",
        RULE_G48_8_1: "null",
        TEXT_G48_EXITGAME_1: "null",
        TEXT_G48_EXITGAME_2: "null",
        TEXT_G48_EXITGAME_3: "null"
      }
    };
    cc._RF.pop();
  }, {} ],
  G48_LoadingItem: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "3be17OA2wtLB7h9C0a0xjME", "G48_LoadingItem");
    "use strict";
    var __extends = this && this.__extends || function() {
      var extendStatics = function(d, b) {
        extendStatics = Object.setPrototypeOf || {
          __proto__: []
        } instanceof Array && function(d, b) {
          d.__proto__ = b;
        } || function(d, b) {
          for (var p in b) Object.prototype.hasOwnProperty.call(b, p) && (d[p] = b[p]);
        };
        return extendStatics(d, b);
      };
      return function(d, b) {
        extendStatics(d, b);
        function __() {
          this.constructor = d;
        }
        d.prototype = null === b ? Object.create(b) : (__.prototype = b.prototype, new __());
      };
    }();
    var __decorate = this && this.__decorate || function(decorators, target, key, desc) {
      var c = arguments.length, r = c < 3 ? target : null === desc ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
      if ("object" === typeof Reflect && "function" === typeof Reflect.decorate) r = Reflect.decorate(decorators, target, key, desc); else for (var i = decorators.length - 1; i >= 0; i--) (d = decorators[i]) && (r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r);
      return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    var Slot_LoadingItem_1 = require("../../SlotFramework/Common/Slot_LoadingItem");
    var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
    var G48_LoadingItem = function(_super) {
      __extends(G48_LoadingItem, _super);
      function G48_LoadingItem() {
        var _this = null !== _super && _super.apply(this, arguments) || this;
        _this.loadingBarFX = null;
        return _this;
      }
      G48_LoadingItem.prototype.setLoadingBar = function(p_percent) {
        _super.prototype.setLoadingBar.call(this, p_percent);
        this.loadingBarFX.position = cc.v3(631 * p_percent + 10, 0);
      };
      __decorate([ property(cc.Node) ], G48_LoadingItem.prototype, "loadingBarFX", void 0);
      G48_LoadingItem = __decorate([ ccclass ], G48_LoadingItem);
      return G48_LoadingItem;
    }(Slot_LoadingItem_1.default);
    exports.default = G48_LoadingItem;
    cc._RF.pop();
  }, {
    "../../SlotFramework/Common/Slot_LoadingItem": void 0
  } ],
  G48_LoadingUI: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "d4bf7TbFHRAZI63ITC7W3Sd", "G48_LoadingUI");
    "use strict";
    var __extends = this && this.__extends || function() {
      var extendStatics = function(d, b) {
        extendStatics = Object.setPrototypeOf || {
          __proto__: []
        } instanceof Array && function(d, b) {
          d.__proto__ = b;
        } || function(d, b) {
          for (var p in b) Object.prototype.hasOwnProperty.call(b, p) && (d[p] = b[p]);
        };
        return extendStatics(d, b);
      };
      return function(d, b) {
        extendStatics(d, b);
        function __() {
          this.constructor = d;
        }
        d.prototype = null === b ? Object.create(b) : (__.prototype = b.prototype, new __());
      };
    }();
    var __decorate = this && this.__decorate || function(decorators, target, key, desc) {
      var c = arguments.length, r = c < 3 ? target : null === desc ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
      if ("object" === typeof Reflect && "function" === typeof Reflect.decorate) r = Reflect.decorate(decorators, target, key, desc); else for (var i = decorators.length - 1; i >= 0; i--) (d = decorators[i]) && (r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r);
      return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    var ccclass = cc._decorator.ccclass;
    var G48_LoadingUI = function(_super) {
      __extends(G48_LoadingUI, _super);
      function G48_LoadingUI() {
        return null !== _super && _super.apply(this, arguments) || this;
      }
      G48_LoadingUI = __decorate([ ccclass ], G48_LoadingUI);
      return G48_LoadingUI;
    }(cc.Component);
    exports.default = G48_LoadingUI;
    cc._RF.pop();
  }, {} ],
  G48_Loading_InitData: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "d1642PDU2hJ6L8awyIaZDFa", "G48_Loading_InitData");
    "use strict";
    var __decorate = this && this.__decorate || function(decorators, target, key, desc) {
      var c = arguments.length, r = c < 3 ? target : null === desc ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
      if ("object" === typeof Reflect && "function" === typeof Reflect.decorate) r = Reflect.decorate(decorators, target, key, desc); else for (var i = decorators.length - 1; i >= 0; i--) (d = decorators[i]) && (r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r);
      return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    var ccclass = cc._decorator.ccclass;
    var G48_Loading_InitData = function() {
      function G48_Loading_InitData() {}
      G48_Loading_InitData = __decorate([ ccclass ], G48_Loading_InitData);
      return G48_Loading_InitData;
    }();
    exports.default = G48_Loading_InitData;
    cc._RF.pop();
  }, {} ],
  G48_Loading: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "ad918YYbQJB1YeVz43iXrnZ", "G48_Loading");
    "use strict";
    var __extends = this && this.__extends || function() {
      var extendStatics = function(d, b) {
        extendStatics = Object.setPrototypeOf || {
          __proto__: []
        } instanceof Array && function(d, b) {
          d.__proto__ = b;
        } || function(d, b) {
          for (var p in b) Object.prototype.hasOwnProperty.call(b, p) && (d[p] = b[p]);
        };
        return extendStatics(d, b);
      };
      return function(d, b) {
        extendStatics(d, b);
        function __() {
          this.constructor = d;
        }
        d.prototype = null === b ? Object.create(b) : (__.prototype = b.prototype, new __());
      };
    }();
    var __decorate = this && this.__decorate || function(decorators, target, key, desc) {
      var c = arguments.length, r = c < 3 ? target : null === desc ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
      if ("object" === typeof Reflect && "function" === typeof Reflect.decorate) r = Reflect.decorate(decorators, target, key, desc); else for (var i = decorators.length - 1; i >= 0; i--) (d = decorators[i]) && (r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r);
      return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __awaiter = this && this.__awaiter || function(thisArg, _arguments, P, generator) {
      function adopt(value) {
        return value instanceof P ? value : new P(function(resolve) {
          resolve(value);
        });
      }
      return new (P || (P = Promise))(function(resolve, reject) {
        function fulfilled(value) {
          try {
            step(generator.next(value));
          } catch (e) {
            reject(e);
          }
        }
        function rejected(value) {
          try {
            step(generator["throw"](value));
          } catch (e) {
            reject(e);
          }
        }
        function step(result) {
          result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected);
        }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
      });
    };
    var __generator = this && this.__generator || function(thisArg, body) {
      var _ = {
        label: 0,
        sent: function() {
          if (1 & t[0]) throw t[1];
          return t[1];
        },
        trys: [],
        ops: []
      }, f, y, t, g;
      return g = {
        next: verb(0),
        throw: verb(1),
        return: verb(2)
      }, "function" === typeof Symbol && (g[Symbol.iterator] = function() {
        return this;
      }), g;
      function verb(n) {
        return function(v) {
          return step([ n, v ]);
        };
      }
      function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
          if (f = 1, y && (t = 2 & op[0] ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 
          0) : y.next) && !(t = t.call(y, op[1])).done) return t;
          (y = 0, t) && (op = [ 2 & op[0], t.value ]);
          switch (op[0]) {
           case 0:
           case 1:
            t = op;
            break;

           case 4:
            _.label++;
            return {
              value: op[1],
              done: false
            };

           case 5:
            _.label++;
            y = op[1];
            op = [ 0 ];
            continue;

           case 7:
            op = _.ops.pop();
            _.trys.pop();
            continue;

           default:
            if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (6 === op[0] || 2 === op[0])) {
              _ = 0;
              continue;
            }
            if (3 === op[0] && (!t || op[1] > t[0] && op[1] < t[3])) {
              _.label = op[1];
              break;
            }
            if (6 === op[0] && _.label < t[1]) {
              _.label = t[1];
              t = op;
              break;
            }
            if (t && _.label < t[2]) {
              _.label = t[2];
              _.ops.push(op);
              break;
            }
            t[2] && _.ops.pop();
            _.trys.pop();
            continue;
          }
          op = body.call(thisArg, _);
        } catch (e) {
          op = [ 6, e ];
          y = 0;
        } finally {
          f = t = 0;
        }
        if (5 & op[0]) throw op[1];
        return {
          value: op[0] ? op[1] : void 0,
          done: true
        };
      }
    };
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    var AudioManager_1 = require("../../Common/Tools/AudioManager/AudioManager");
    var DesktopBrowserTransform_1 = require("../../Common/Tools/DesktopBrowserTransform/DesktopBrowserTransform");
    var Slot_LoadingManager_1 = require("../../SlotFramework/Loading/Slot_LoadingManager");
    var G48_DataManager_1 = require("../Common/G48_DataManager");
    var G48_DynamicPopUpPanelManager_1 = require("../Common/G48_DynamicPopUpPanelManager");
    var G48_LoadingItem_1 = require("../Common/G48_LoadingItem");
    var G48_SocketManager_1 = require("../Common/Socket/G48_SocketManager");
    var G48_Language_1 = require("../Localization/G48_Language");
    var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
    var PRELOAD_RESOURCE_LIST = [ "Resources" ];
    var PRELOAD_SCENE_LIST = [ "G48_Lobby", "G48_Game" ];
    var preloadResources = [ {
      bundle: "G48",
      scene: {
        path: PRELOAD_SCENE_LIST
      },
      resource: {
        path: PRELOAD_RESOURCE_LIST
      },
      audio: {
        csvPath: "Resources/Audio - G48",
        path: [ "Resources/music", "Resources/sound" ]
      },
      loadRate: .97
    } ];
    var G48_Loading = function(_super) {
      __extends(G48_Loading, _super);
      function G48_Loading() {
        var _this = null !== _super && _super.apply(this, arguments) || this;
        _this.languageMap = G48_Language_1.G48_Language;
        _this.loadingItem = null;
        return _this;
      }
      G48_Loading.prototype.onLoad = function() {
        DesktopBrowserTransform_1.default.needFixScreen = true;
        _super.prototype.onLoad.call(this);
        cc.game.setFrameRate(60);
        cc.view.enableAntiAlias(true);
        cc.dynamicAtlasManager.enabled = false;
        cc.macro.ENABLE_MULTI_TOUCH = false;
      };
      G48_Loading.prototype.start = function() {
        _super.prototype.start.call(this);
        this.loadingItem.show();
        this.processAsync().catch(function(err) {
          return console.error(err);
        });
      };
      G48_Loading.prototype.processAsync = function() {
        return __awaiter(this, void 0, Promise, function() {
          return __generator(this, function(_a) {
            switch (_a.label) {
             case 0:
              AudioManager_1.AudioManager.delInstance();
              G48_DataManager_1.default.deleteInstance();
              return [ 4, G48_SocketManager_1.default.getInstance().connect() ];

             case 1:
              _a.sent();
              return [ 4, this.initGameConfig(G48_DataManager_1.default.getInstance()) ];

             case 2:
              _a.sent();
              return [ 4, _super.prototype.processAsync.call(this, preloadResources) ];

             case 3:
              _a.sent();
              this.processLogin(G48_SocketManager_1.default.getInstance());
              return [ 2 ];
            }
          });
        });
      };
      G48_Loading.prototype.onAssetLoadComplete = function(prefabs) {
        G48_DynamicPopUpPanelManager_1.default.getInstance().prestore(prefabs);
      };
      __decorate([ property({
        type: G48_LoadingItem_1.default,
        override: true
      }) ], G48_Loading.prototype, "loadingItem", void 0);
      G48_Loading = __decorate([ ccclass ], G48_Loading);
      return G48_Loading;
    }(Slot_LoadingManager_1.default);
    exports.default = G48_Loading;
    cc._RF.pop();
  }, {
    "../../Common/Tools/AudioManager/AudioManager": void 0,
    "../../Common/Tools/DesktopBrowserTransform/DesktopBrowserTransform": void 0,
    "../../SlotFramework/Loading/Slot_LoadingManager": void 0,
    "../Common/G48_DataManager": "G48_DataManager",
    "../Common/G48_DynamicPopUpPanelManager": "G48_DynamicPopUpPanelManager",
    "../Common/G48_LoadingItem": "G48_LoadingItem",
    "../Common/Socket/G48_SocketManager": "G48_SocketManager",
    "../Localization/G48_Language": "G48_Language"
  } ],
  G48_LobbyUI: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "4adb0vTuINJ5ajQxthjedka", "G48_LobbyUI");
    "use strict";
    var __extends = this && this.__extends || function() {
      var extendStatics = function(d, b) {
        extendStatics = Object.setPrototypeOf || {
          __proto__: []
        } instanceof Array && function(d, b) {
          d.__proto__ = b;
        } || function(d, b) {
          for (var p in b) Object.prototype.hasOwnProperty.call(b, p) && (d[p] = b[p]);
        };
        return extendStatics(d, b);
      };
      return function(d, b) {
        extendStatics(d, b);
        function __() {
          this.constructor = d;
        }
        d.prototype = null === b ? Object.create(b) : (__.prototype = b.prototype, new __());
      };
    }();
    var __decorate = this && this.__decorate || function(decorators, target, key, desc) {
      var c = arguments.length, r = c < 3 ? target : null === desc ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
      if ("object" === typeof Reflect && "function" === typeof Reflect.decorate) r = Reflect.decorate(decorators, target, key, desc); else for (var i = decorators.length - 1; i >= 0; i--) (d = decorators[i]) && (r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r);
      return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    var Slot_LobbyUI_1 = require("../../../SlotFramework/Lobby/Slot_LobbyUI");
    var ccclass = cc._decorator.ccclass;
    var G48_LobbyUI = function(_super) {
      __extends(G48_LobbyUI, _super);
      function G48_LobbyUI() {
        return null !== _super && _super.apply(this, arguments) || this;
      }
      G48_LobbyUI.prototype.init = function(p_data) {
        _super.prototype.init.call(this, p_data);
      };
      G48_LobbyUI.prototype.setPlayerIcon = function() {};
      G48_LobbyUI = __decorate([ ccclass ], G48_LobbyUI);
      return G48_LobbyUI;
    }(Slot_LobbyUI_1.default);
    exports.default = G48_LobbyUI;
    cc._RF.pop();
  }, {
    "../../../SlotFramework/Lobby/Slot_LobbyUI": void 0
  } ],
  G48_Lobby: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "1b68cMj9RhI6bWZfrKSQyFR", "G48_Lobby");
    "use strict";
    var __extends = this && this.__extends || function() {
      var extendStatics = function(d, b) {
        extendStatics = Object.setPrototypeOf || {
          __proto__: []
        } instanceof Array && function(d, b) {
          d.__proto__ = b;
        } || function(d, b) {
          for (var p in b) Object.prototype.hasOwnProperty.call(b, p) && (d[p] = b[p]);
        };
        return extendStatics(d, b);
      };
      return function(d, b) {
        extendStatics(d, b);
        function __() {
          this.constructor = d;
        }
        d.prototype = null === b ? Object.create(b) : (__.prototype = b.prototype, new __());
      };
    }();
    var __decorate = this && this.__decorate || function(decorators, target, key, desc) {
      var c = arguments.length, r = c < 3 ? target : null === desc ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
      if ("object" === typeof Reflect && "function" === typeof Reflect.decorate) r = Reflect.decorate(decorators, target, key, desc); else for (var i = decorators.length - 1; i >= 0; i--) (d = decorators[i]) && (r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r);
      return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    var LocalizationManager_1 = require("../../Common/Tools/Localization/LocalizationManager");
    var Slot_LobbyManager_InitData_1 = require("../../SlotFramework/Lobby/Data/Slot_LobbyManager_InitData");
    var Slot_LobbyUI_InitData_1 = require("../../SlotFramework/Lobby/Data/Slot_LobbyUI_InitData");
    var Slot_LobbyManager_1 = require("../../SlotFramework/Lobby/Slot_LobbyManager");
    var G48_DataManager_1 = require("../Common/G48_DataManager");
    var G48_SocketManager_1 = require("../Common/Socket/G48_SocketManager");
    var G48_LobbyUI_1 = require("./View/G48_LobbyUI");
    var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
    var G48_Lobby = function(_super) {
      __extends(G48_Lobby, _super);
      function G48_Lobby() {
        var _this = null !== _super && _super.apply(this, arguments) || this;
        _this.ui = null;
        _this.data = null;
        _this.socket = null;
        return _this;
      }
      G48_Lobby.prototype.start = function() {
        _super.prototype.start.call(this);
        this.socket = G48_SocketManager_1.default.getInstance();
        var _data = new Slot_LobbyManager_InitData_1.default();
        _data.bundleName = "G" + G48_DataManager_1.default.getInstance().gameID;
        _data.bundlePath = "" + G48_DataManager_1.default.getInstance().path;
        this.init(_data);
        this.onFastJoinGame();
      };
      G48_Lobby.prototype.onDestroy = function() {
        this.removeEvents();
      };
      G48_Lobby.prototype.init = function(p_data) {
        this.data = G48_DataManager_1.default.getInstance();
        this.data.init();
        this.registerEvents();
        _super.prototype.init.call(this, p_data);
        var _data = new Slot_LobbyUI_InitData_1.default();
        _data.bundleName = p_data.bundleName;
        _data.bundlePath = p_data.bundlePath;
        this.ui.init(_data);
      };
      G48_Lobby.prototype.registerEvents = function() {
        var _this = this;
        G48_SocketManager_1.default.getInstance().setSelfEnterEventCallback(function(data) {
          _this.onSelfEnterEvent(data);
        });
      };
      G48_Lobby.prototype.removeEvents = function() {
        G48_SocketManager_1.default.getInstance().setSelfEnterEventCallback(null);
      };
      G48_Lobby.prototype.onSelfEnterEvent = function(data) {
        G48_DataManager_1.default.getInstance().coin = G48_DataManager_1.default.getInstance().getClientMoney(data.Coin);
      };
      G48_Lobby.prototype.onBackButton = function() {
        console.log("\u96e2\u958b");
      };
      G48_Lobby.prototype.onOpenHistoryButton = function() {
        console.log("\u958b\u555f\u6b77\u53f2\u7d00\u9304");
      };
      G48_Lobby.prototype.onOpenMenuButton = function() {
        this.ui.openMenu();
      };
      G48_Lobby.prototype.onCloseMenuButton = function() {
        this.ui.closeMenu();
      };
      G48_Lobby.prototype.onChangeMachine = function() {
        console.log("\u63db\u4e00\u6279");
      };
      G48_Lobby.prototype.onFastJoinGame = function() {
        var _this = this;
        console.log("\u5feb\u901f\u52a0\u5165");
        this.socket.setOnFastChoseRoomEvent().then(function() {
          cc.director.loadScene("G48_Game");
        }).catch(function(error) {
          error.code == GameClient.errCode.RESPONSE_TIMED_OUT && _this.socket.showErrorAlert(LocalizationManager_1.default.getInstance().get("ERROR_SOCKET_TIME_OUT"), null, function() {
            G48_SocketManager_1.default.getInstance().login();
          });
        });
      };
      G48_Lobby.prototype.onChangePlayerHead = function() {
        console.log("\u63db\u73a9\u5bb6\u982d\u50cf");
      };
      G48_Lobby.prototype.setPlayerInfo = function() {
        var _money = this.data.coin;
        this.ui.setPlayerInfo(this.data.nickname, this.data.avatarID, _money);
      };
      G48_Lobby.prototype.setRoomData = function() {
        console.log("\u8a2d\u5b9a\u623f\u9593\u8cc7\u6599");
      };
      __decorate([ property(G48_LobbyUI_1.default) ], G48_Lobby.prototype, "ui", void 0);
      G48_Lobby = __decorate([ ccclass ], G48_Lobby);
      return G48_Lobby;
    }(Slot_LobbyManager_1.default);
    exports.default = G48_Lobby;
    cc._RF.pop();
  }, {
    "../../Common/Tools/Localization/LocalizationManager": void 0,
    "../../SlotFramework/Lobby/Data/Slot_LobbyManager_InitData": void 0,
    "../../SlotFramework/Lobby/Data/Slot_LobbyUI_InitData": void 0,
    "../../SlotFramework/Lobby/Slot_LobbyManager": void 0,
    "../Common/G48_DataManager": "G48_DataManager",
    "../Common/Socket/G48_SocketManager": "G48_SocketManager",
    "./View/G48_LobbyUI": "G48_LobbyUI"
  } ],
  G48_MusicOptionPanel: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "3d3e0QcXq1Hsbhj/4P9Qwhk", "G48_MusicOptionPanel");
    "use strict";
    var __extends = this && this.__extends || function() {
      var extendStatics = function(d, b) {
        extendStatics = Object.setPrototypeOf || {
          __proto__: []
        } instanceof Array && function(d, b) {
          d.__proto__ = b;
        } || function(d, b) {
          for (var p in b) Object.prototype.hasOwnProperty.call(b, p) && (d[p] = b[p]);
        };
        return extendStatics(d, b);
      };
      return function(d, b) {
        extendStatics(d, b);
        function __() {
          this.constructor = d;
        }
        d.prototype = null === b ? Object.create(b) : (__.prototype = b.prototype, new __());
      };
    }();
    var __decorate = this && this.__decorate || function(decorators, target, key, desc) {
      var c = arguments.length, r = c < 3 ? target : null === desc ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
      if ("object" === typeof Reflect && "function" === typeof Reflect.decorate) r = Reflect.decorate(decorators, target, key, desc); else for (var i = decorators.length - 1; i >= 0; i--) (d = decorators[i]) && (r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r);
      return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    var Slot_MusicOptionPanel_1 = require("../../../SlotFramework/Game/Panel/Slot_MusicOptionPanel");
    var ccclass = cc._decorator.ccclass;
    var G48_MusicOptionPanel = function(_super) {
      __extends(G48_MusicOptionPanel, _super);
      function G48_MusicOptionPanel() {
        return null !== _super && _super.apply(this, arguments) || this;
      }
      G48_MusicOptionPanel = __decorate([ ccclass ], G48_MusicOptionPanel);
      return G48_MusicOptionPanel;
    }(Slot_MusicOptionPanel_1.default);
    exports.default = G48_MusicOptionPanel;
    cc._RF.pop();
  }, {
    "../../../SlotFramework/Game/Panel/Slot_MusicOptionPanel": void 0
  } ],
  G48_Reel: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "5bae45Uc51HsoJhKsHcVXc+", "G48_Reel");
    "use strict";
    var __extends = this && this.__extends || function() {
      var extendStatics = function(d, b) {
        extendStatics = Object.setPrototypeOf || {
          __proto__: []
        } instanceof Array && function(d, b) {
          d.__proto__ = b;
        } || function(d, b) {
          for (var p in b) Object.prototype.hasOwnProperty.call(b, p) && (d[p] = b[p]);
        };
        return extendStatics(d, b);
      };
      return function(d, b) {
        extendStatics(d, b);
        function __() {
          this.constructor = d;
        }
        d.prototype = null === b ? Object.create(b) : (__.prototype = b.prototype, new __());
      };
    }();
    var __decorate = this && this.__decorate || function(decorators, target, key, desc) {
      var c = arguments.length, r = c < 3 ? target : null === desc ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
      if ("object" === typeof Reflect && "function" === typeof Reflect.decorate) r = Reflect.decorate(decorators, target, key, desc); else for (var i = decorators.length - 1; i >= 0; i--) (d = decorators[i]) && (r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r);
      return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    var Slot_Reel_1 = require("../../../SlotFramework/Game/view/Slot_Reel");
    var G48_Symbol_1 = require("./G48_Symbol");
    var AudioManager_1 = require("../../../Common/Tools/AudioManager/AudioManager");
    var Slot_Reel_InitData_1 = require("../../../SlotFramework/Game/Data/Slot_Reel_InitData");
    var OrientationToolManager_1 = require("../../../Common/Tools/OrientationTool/OrientationToolManager");
    var Slot_DataManager_1 = require("../../../SlotFramework/Slot_DataManager");
    var ccclass = cc._decorator.ccclass;
    var STATUS;
    (function(STATUS) {
      STATUS[STATUS["IDLE"] = 0] = "IDLE";
      STATUS[STATUS["SCROLLING_UP"] = 1] = "SCROLLING_UP";
      STATUS[STATUS["SCROLLING"] = 2] = "SCROLLING";
      STATUS[STATUS["OMEN_SCROLLING"] = 3] = "OMEN_SCROLLING";
      STATUS[STATUS["REBOUND_ALIGNMENT"] = 4] = "REBOUND_ALIGNMENT";
      STATUS[STATUS["SLOWING"] = 5] = "SLOWING";
      STATUS[STATUS["SLOW_WAIT"] = 6] = "SLOW_WAIT";
      STATUS[STATUS["REBOUND"] = 7] = "REBOUND";
      STATUS[STATUS["REBOUND_WAIT"] = 8] = "REBOUND_WAIT";
      STATUS[STATUS["REWARD_EFFECTING"] = 9] = "REWARD_EFFECTING";
      STATUS[STATUS["DROP_OUT_START"] = 10] = "DROP_OUT_START";
      STATUS[STATUS["DROP_OUT_WAIT"] = 11] = "DROP_OUT_WAIT";
      STATUS[STATUS["DROP_IN_START"] = 12] = "DROP_IN_START";
      STATUS[STATUS["DROP_IN_WAIT"] = 13] = "DROP_IN_WAIT";
      STATUS[STATUS["SYMBOL_DISAPPEAR_EFFECTING"] = 14] = "SYMBOL_DISAPPEAR_EFFECTING";
      STATUS[STATUS["SYMBOL_DISAPPEAR_DROP_EFFECTING"] = 15] = "SYMBOL_DISAPPEAR_DROP_EFFECTING";
      STATUS[STATUS["THROW_SYMBOL_EFFECTING"] = 16] = "THROW_SYMBOL_EFFECTING";
    })(STATUS || (STATUS = {}));
    var G48_Reel = function(_super) {
      __extends(G48_Reel, _super);
      function G48_Reel() {
        var _this = null !== _super && _super.apply(this, arguments) || this;
        _this.symbolsList = [];
        _this.nextReels = [];
        _this.nextReelsAbove = 0;
        _this.tempSymbolNode = null;
        _this.level = 0;
        _this.data = null;
        _this.isPlayDropInAudio = false;
        return _this;
      }
      G48_Reel_1 = G48_Reel;
      G48_Reel.prototype.init = function(p_data) {
        this.data = p_data;
        _super.prototype.init.call(this, p_data);
        this.tempSymbolNode = this.symbolsList[0].node;
      };
      G48_Reel.prototype.update = function(dt) {
        switch (this.status) {
         case STATUS.SYMBOL_DISAPPEAR_EFFECTING:
          this.symbolDisappearEffecting();
          break;

         case STATUS.SYMBOL_DISAPPEAR_DROP_EFFECTING:
          this.symbolDisappearDropEffecting();
          break;

         case STATUS.DROP_IN_WAIT:
          this.dropInWait();
          break;

         default:
          _super.prototype.update.call(this, dt);
        }
      };
      G48_Reel.prototype.showSymbolDisappearEffect = function(p_winPosition, p_nextReels, p_nextReelsAbove, p_reelTime, p_symbolTime, p_symbolDisappearFinishDelayTime) {
        this.nextReels = p_nextReels;
        this.nextReelsAbove = p_nextReelsAbove;
        var _delayCount = 0;
        var isPlayAudio = false;
        for (var i = 0; i < p_winPosition.length; i++) if (1 == p_winPosition[i]) for (var j = 0; j < this.symbolsList.length; j++) if (this.symbolsList[j].getOrder() - 2 == i) {
          this.symbolsList[j].setZOrder(10);
          this.symbolsList[j].playDisappearEffect(p_reelTime + p_symbolTime * _delayCount, p_symbolDisappearFinishDelayTime);
          _delayCount++;
          isPlayAudio = true;
        }
        isPlayAudio && this.scheduleOnce(function() {
          AudioManager_1.AudioManager.instance.playAudioEvent("SymbolDisappearEffect");
        }, p_reelTime);
        this.status = STATUS.SYMBOL_DISAPPEAR_EFFECTING;
      };
      G48_Reel.prototype.symbolDisappearEffecting = function() {
        for (var i = 0; i < this.symbolsList.length; i++) if (this.symbolsList[i].isDisappearEffecting()) return;
        this.status = STATUS.IDLE;
      };
      G48_Reel.prototype.isSymbolDisappearEffecting = function() {
        return this.status == STATUS.SYMBOL_DISAPPEAR_EFFECTING;
      };
      G48_Reel.prototype.ShowSymbolDisappearDropEffect = function(p_winPosition) {
        var _disppearSymbolOrder = [];
        for (var i = 0; i < p_winPosition.length; i++) 1 == p_winPosition[i] && _disppearSymbolOrder.push(i + 2);
        p_winPosition.unshift(0);
        this.undisappearDroping(_disppearSymbolOrder, p_winPosition);
        this.disppearSymbolToTop(_disppearSymbolOrder);
        this.changeDisppear(_disppearSymbolOrder);
        this.disappearDroping(_disppearSymbolOrder);
        this.status = STATUS.SYMBOL_DISAPPEAR_DROP_EFFECTING;
      };
      G48_Reel.prototype.symbolDisappearDropEffecting = function() {
        for (var i = 0; i < this.symbolsList.length; i++) if (this.symbolsList[i].isDropEffecting()) return;
        this.status = STATUS.IDLE;
      };
      G48_Reel.prototype.isSymbolDisappearDropEffecting = function() {
        return this.status == STATUS.SYMBOL_DISAPPEAR_DROP_EFFECTING;
      };
      G48_Reel.prototype.undisappearDroping = function(p_disppearSymbolOrder, p_winPosition) {
        var _symbolHeight = this.symbolsList[0].node.getContentSize().height * this.symbolsList[0].node.scaleY;
        for (var i = 1; i < this.symbolsList.length; i++) if (0 == p_winPosition[i - 1]) {
          var _symbolOrder = this.getSymbolOrder(i);
          var targetOreder = _symbolOrder;
          var _targetPosition = null;
          var _dropCount = 0;
          for (var j = 0; j < p_disppearSymbolOrder.length; j++) if (i < p_disppearSymbolOrder[j]) {
            targetOreder++;
            _targetPosition = this.getSymbolPosition(targetOreder);
            _dropCount++;
          }
          if (_dropCount > 0) {
            _targetPosition.y += _symbolHeight + this.symbolSpacing;
            this.symbolsList[_symbolOrder].playDropEffect(0, _targetPosition);
          }
        }
      };
      G48_Reel.prototype.disppearSymbolToTop = function(p_disppearSymbolOrder) {
        var _symbolHeight = this.symbolsList[0].node.getContentSize().height * this.symbolsList[0].node.scaleY;
        for (var i = 0; i < p_disppearSymbolOrder.length; i++) {
          var _targetPosition = this.getSymbolPosition(0);
          _targetPosition.y += (p_disppearSymbolOrder.length - i) * (_symbolHeight + this.symbolSpacing);
          var _symbolOrder = this.getSymbolOrder(p_disppearSymbolOrder[i]);
          this.symbolsList[_symbolOrder].node.setPosition(_targetPosition);
        }
      };
      G48_Reel.prototype.changeDisppear = function(p_disppearSymbolOrder) {
        this.nextReels.unshift(this.nextReelsAbove);
        for (var i = 0; i < p_disppearSymbolOrder.length; i++) {
          var _newSymbolIndex = this.nextReels[i];
          var _symbolOrder = this.getSymbolOrder(p_disppearSymbolOrder[i]);
          var _symbolValue = _newSymbolIndex;
          this.symbolsList[_symbolOrder].setData(_newSymbolIndex, _symbolValue, this.isFreeGame, this.level);
        }
      };
      G48_Reel.prototype.disappearDroping = function(p_disppearSymbolOrder) {
        var _symbolHeight = this.symbolsList[0].node.getContentSize().height * this.symbolsList[0].node.scaleY;
        for (var i = 0; i < p_disppearSymbolOrder.length; i++) {
          var _targetPosition = this.getSymbolPosition(i + 1);
          var _symbolOrder = this.getSymbolOrder(p_disppearSymbolOrder[i]);
          _targetPosition.y += _symbolHeight + this.symbolSpacing;
          this.symbolsList[_symbolOrder].playDropEffect(0, _targetPosition);
        }
      };
      G48_Reel.prototype.correctionSymbolOrder = function() {
        for (var i = 0; i < this.symbolsList.length; i++) for (var j = 0; j < this.symbolsList.length; j++) {
          var _target = this.symbolPrefab.getContentSize().height * this.symbolsList[0].node.scaleY + this.symbolSpacing;
          Math.abs(this.symbolsList[i].node.position.y - (this.getSymbolPosition(j).y + _target)) < 10 && this.symbolsList[i].setOrder(j);
        }
        this.symbolsList.sort(function(a, b) {
          return a.getOrder() - b.getOrder();
        });
      };
      G48_Reel.prototype.isSymbolDropOutEffecting = function() {
        return this.status == STATUS.THROW_SYMBOL_EFFECTING;
      };
      G48_Reel.prototype.showAllSymbolDropOutEffect = function() {
        var _this = this;
        this.status = STATUS.THROW_SYMBOL_EFFECTING;
        var count = 0;
        var outSize = OrientationToolManager_1.default.orientationState === OrientationToolManager_1.ORIENTATION_TYPE.VERTICAL ? cc.v3(960, -866) : cc.v3(1680, -300);
        var height = OrientationToolManager_1.default.orientationState === OrientationToolManager_1.ORIENTATION_TYPE.VERTICAL ? 1680 : 720;
        for (var i = 0; i < this.symbolsList.length; i++) {
          var symbol = this.symbolsList[i];
          if (0 === symbol.getOrder() || 1 === symbol.getOrder()) symbol.node.opacity = 0; else {
            count++;
            var baseX = this.node.x < 0 ? -1 : 1;
            var target = cc.v2(baseX * Math.random() * outSize.x / 2, outSize.y + Math.random() * outSize.y / 10 - symbol.node.height / 2);
            var v1 = cc.v2(target.x / 3, symbol.node.y + height / 2);
            var v2 = cc.v2(target.x, target.y + target.y / 2);
            cc.tween(symbol.node).bezierTo(.8 + .4 * Math.random(), v1, v2, target).call(function() {
              count--;
              0 === count && (_this.status = STATUS.IDLE);
            }).start();
          }
        }
      };
      G48_Reel.prototype.dropInStart = function(p_dt) {
        var _this = this;
        this.isPlayDropInAudio = false;
        this.tempDropInTime += p_dt;
        if (this.tempDropInTime < this.dropInStartDelayTime) return;
        this.status = STATUS.DROP_IN_WAIT;
        var _loop_1 = function(i) {
          var _symbolHeight = this_1.symbolsList[i].node.getContentSize().height * this_1.symbolsList[0].node.scaleY;
          var _targetPosition = this_1.getSymbolPosition(i);
          var _movePositionY = (_symbolHeight + this_1.symbolSpacing) * (this_1.symbolsList.length + 5);
          var _shakeAngle = Math.random() > .5 ? this_1.dropInReboundAngle : -this_1.dropInReboundAngle;
          var _reboundTime = .5 * this_1.dropInReboundTime;
          var _reboundByPosOut = cc.v3(0, -this_1.dropInReboundHeight);
          var _reboundByPosIn = cc.v3(0, +this_1.dropInReboundHeight);
          var _reboundAngleTime = .5 * this_1.dropInReboundAngleTime;
          _targetPosition.y += _symbolHeight + this_1.symbolSpacing;
          this_1.symbolsList[i].node.stopAllActions();
          this_1.symbolsList[i].node.setPosition(_targetPosition.x, _targetPosition.y + _movePositionY);
          this_1.symbolsList[i].node.opacity = 255;
          this_1.symbolsList[i].setOrder(i);
          this_1.changeSymbol(i, true, this_1.reel, this_1.isFreeGame);
          0 === i && this_1.allSymbolPlayIdleEffect(true);
          cc.tween(this_1.symbolsList[i].node).delay((this_1.symbolsList.length - i) * this_1.dropInDelayTime).call(function() {
            if (_this.symbolsList[i].getSymbolID() === Slot_DataManager_1.SYMBOL_NAME.Jackpot) {
              AudioManager_1.AudioManager.instance.playAudioEvent("JackpotSymbol" + G48_Reel_1.jackpotCountSound);
              G48_Reel_1.jackpotCountSound++;
            }
            i === _this.symbolsList.length - 1 && AudioManager_1.AudioManager.instance.playAudioEvent("ReelStop");
          }).to(this_1.dropInTime, {
            position: cc.v3(_targetPosition.x, _targetPosition.y)
          }, {
            easing: this_1.dropInEasing
          }).parallel(cc.tween().sequence(cc.tween().by(_reboundTime, {
            position: _reboundByPosOut
          }, {
            easing: this_1.dropInReboundEasingOut
          }), cc.tween().by(_reboundTime, {
            position: _reboundByPosIn
          }, {
            easing: this_1.dropInReboundEasingIn
          })), cc.tween().sequence(cc.tween().by(_reboundAngleTime, {
            angle: _shakeAngle
          }, {
            easing: this_1.dropInReboundEasingOut
          }), cc.tween().by(_reboundAngleTime, {
            angle: -_shakeAngle
          }, {
            easing: this_1.dropInReboundEasingIn
          }))).call(function() {
            if (1 == i) {
              _this.status = STATUS.IDLE;
              _this.hideOutRangeSymbol();
            }
          }).start();
        };
        var this_1 = this;
        for (var i = this.symbolsList.length - 1; i >= 0; i--) _loop_1(i);
      };
      G48_Reel.prototype.dropInWait = function() {
        if (!this.isPlayDropInAudio) {
          this.isPlayDropInAudio = true;
          if (1 != this.data.reelIndex) return;
        }
      };
      G48_Reel.prototype.getSymbolPosition = function(p_order) {
        var _addtion = 0;
        var _symbolCount = this.symbolsList.length;
        var _offset = this.symbolPrefab.getContentSize().height * this.symbolsList[0].node.scaleY / 2 * (this.symbolsList.length - 6);
        _offset > 0 && (_offset += this.symbolSpacing);
        _symbolCount % 2 == 0 && (_addtion = -this.symbolPrefab.getContentSize().height * this.symbolsList[0].node.scaleY / 2);
        var _middleOrder = Math.floor(_symbolCount / 2);
        var _times = -(p_order - _middleOrder);
        var _spacing = this.symbolPrefab.getContentSize().height * this.symbolsList[0].node.scaleY + this.symbolSpacing;
        var _vec2 = new cc.Vec2(0, _spacing * _times + _addtion + _offset);
        _vec2 = _vec2.add(new cc.Vec2(this.node.position.x, this.node.position.y));
        _vec2 = _vec2.add(new cc.Vec2(this.reelOffset.x, this.reelOffset.y));
        1 === p_order ? _vec2.y = 4.5 * _spacing : 0 === p_order && (_vec2.y = 6 * _spacing);
        return _vec2;
      };
      G48_Reel.prototype.changeSymbol = function(p_symbolOrder, p_Alignment, p_reel, p_isFreeGame) {
        void 0 === p_reel && (p_reel = []);
        void 0 === p_isFreeGame && (p_isFreeGame = false);
        if (p_reel.length > 0) {
          var _reel = p_reel;
          this.reelMode !== Slot_Reel_InitData_1.ReelMode.DROP && this.reelMode !== Slot_Reel_InitData_1.ReelMode.SCROLL || (_reel = this.reelAbove ? [ 1, this.reelAbove ].concat(p_reel) : [ 1, 1 ].concat(p_reel));
          for (var j = 0; j < this.symbolsList.length; j++) if (this.symbolsList[j].getOrder() == p_symbolOrder) {
            var _symbolIndex = this.symbolsList[j].getNowIndex();
            this.symbolsList[j].setData(_symbolIndex, _reel[p_symbolOrder], p_isFreeGame, this.level);
          }
        }
      };
      G48_Reel.prototype.reelsSpacingY = function() {
        switch (this.status) {
         case STATUS.SCROLLING_UP:
         case STATUS.SCROLLING:
         case STATUS.OMEN_SCROLLING:
         case STATUS.REBOUND_ALIGNMENT:
          this.orientationSyncY();
          break;

         case STATUS.REBOUND_WAIT:
         case STATUS.REBOUND:
          this.rebound();
          break;

         case STATUS.SLOWING:
         case STATUS.SLOW_WAIT:
          this.slowing();
          break;

         case STATUS.THROW_SYMBOL_EFFECTING:
          this.hideAllSymbol();
          break;

         case STATUS.DROP_IN_START:
         case STATUS.DROP_IN_WAIT:
         case STATUS.SYMBOL_DISAPPEAR_DROP_EFFECTING:
          this.symbolsList.forEach(function(symbol) {
            return symbol.skipDropEffect();
          });
          if (this.status === STATUS.DROP_IN_START || this.status === STATUS.DROP_IN_WAIT) {
            var index = this.getSymbolOrder(1);
            this.symbolsList[index].setData(index, this.reelAbove, this.isFreeGame, this.level);
            for (var i = 2, j = 0; i < this.symbolsList.length; i++, j++) {
              var index_1 = this.getSymbolOrder(i);
              this.symbolsList[index_1].setData(index_1, this.reel[j], this.isFreeGame, this.level);
            }
          } else for (var i = 1, j = 0; i < this.symbolsList.length; i++, j++) {
            var index = this.getSymbolOrder(i);
            this.symbolsList[index].setData(index, this.nextReels[j], this.isFreeGame, this.level);
          }

         default:
          for (var i = 0; i < this.symbolsList.length; i++) {
            var _addtion = 0;
            var _symbolCount = this.symbolsList.length;
            var _offset = this.symbolPrefab.getContentSize().height * this.symbolsList[0].node.scaleY / 2 * (this.symbolsList.length - 6);
            _offset > 0 && (_offset += this.symbolSpacing);
            _symbolCount % 2 == 0 && (_addtion = -this.symbolPrefab.getContentSize().height * this.symbolsList[0].node.scaleY / 2);
            var _middleOrder = Math.floor(_symbolCount / 2);
            var _times = -(this.symbolsList[i].getOrder() - _middleOrder);
            var _spacing = this.symbolPrefab.getContentSize().height * this.symbolsList[0].node.scaleY + this.symbolSpacing;
            var offset = _spacing * _times + _addtion + _offset;
            1 === this.symbolsList[i].getOrder() ? offset = 4.5 * _spacing : 0 === this.symbolsList[i].getOrder() && (offset = 6 * _spacing);
            this.symbolsList[i].setSymbolY(offset + this.symbolPrefab.getContentSize().height * this.symbolsList[0].node.scaleY + this.symbolSpacing);
          }
          this.status !== STATUS.DROP_IN_START && this.status !== STATUS.DROP_IN_WAIT || (this.status = STATUS.IDLE);
        }
      };
      G48_Reel.prototype.addSymbol = function() {
        var symbolNode = cc.instantiate(this.tempSymbolNode);
        var symbol = symbolNode.getComponent(G48_Symbol_1.default);
        this.symbolsList.push(symbol);
        symbolNode.setScale(this.data.symbolScale);
        symbolNode.opacity = 0;
        symbolNode.setParent(this.node.parent);
        symbolNode.setSiblingIndex(0);
      };
      G48_Reel.prototype.removeSymbol = function() {
        this.symbolsList.pop();
      };
      G48_Reel.prototype.setLevel = function(level) {
        this.level = level;
      };
      G48_Reel.prototype.getSymbolListLen = function() {
        return this.symbolsList.length;
      };
      var G48_Reel_1;
      G48_Reel.jackpotCountSound = 1;
      G48_Reel = G48_Reel_1 = __decorate([ ccclass ], G48_Reel);
      return G48_Reel;
    }(Slot_Reel_1.default);
    exports.default = G48_Reel;
    cc._RF.pop();
  }, {
    "../../../Common/Tools/AudioManager/AudioManager": void 0,
    "../../../Common/Tools/OrientationTool/OrientationToolManager": void 0,
    "../../../SlotFramework/Game/Data/Slot_Reel_InitData": void 0,
    "../../../SlotFramework/Game/view/Slot_Reel": void 0,
    "../../../SlotFramework/Slot_DataManager": void 0,
    "./G48_Symbol": "G48_Symbol"
  } ],
  G48_RulePanel: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "cf1e3afkddHx7gJxp3MfyFp", "G48_RulePanel");
    "use strict";
    var __extends = this && this.__extends || function() {
      var extendStatics = function(d, b) {
        extendStatics = Object.setPrototypeOf || {
          __proto__: []
        } instanceof Array && function(d, b) {
          d.__proto__ = b;
        } || function(d, b) {
          for (var p in b) Object.prototype.hasOwnProperty.call(b, p) && (d[p] = b[p]);
        };
        return extendStatics(d, b);
      };
      return function(d, b) {
        extendStatics(d, b);
        function __() {
          this.constructor = d;
        }
        d.prototype = null === b ? Object.create(b) : (__.prototype = b.prototype, new __());
      };
    }();
    var __decorate = this && this.__decorate || function(decorators, target, key, desc) {
      var c = arguments.length, r = c < 3 ? target : null === desc ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
      if ("object" === typeof Reflect && "function" === typeof Reflect.decorate) r = Reflect.decorate(decorators, target, key, desc); else for (var i = decorators.length - 1; i >= 0; i--) (d = decorators[i]) && (r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r);
      return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    var Slot_RulePanel_1 = require("../../../SlotFramework/Game/Panel/Slot_RulePanel");
    var AudioManager_1 = require("../../../Common/Tools/AudioManager/AudioManager");
    var G48_DataManager_1 = require("../../Common/G48_DataManager");
    var ccclass = cc._decorator.ccclass;
    var G48_RulePanel = function(_super) {
      __extends(G48_RulePanel, _super);
      function G48_RulePanel() {
        var _this = null !== _super && _super.apply(this, arguments) || this;
        _this.data = null;
        return _this;
      }
      G48_RulePanel.prototype.init = function() {
        this.data = G48_DataManager_1.default.getInstance();
        _super.prototype.init.call(this);
      };
      G48_RulePanel.prototype.onNextRulePage = function() {
        if (this.status == Slot_RulePanel_1.STATUS.CHANGE_PAGE) return;
        AudioManager_1.AudioManager.instance.playAudioEvent("NextRulePage");
        _super.prototype.onNextRulePage.call(this);
      };
      G48_RulePanel.prototype.onPreRulePage = function() {
        if (this.status == Slot_RulePanel_1.STATUS.CHANGE_PAGE) return;
        AudioManager_1.AudioManager.instance.playAudioEvent("PreRulePage");
        _super.prototype.onPreRulePage.call(this);
      };
      G48_RulePanel.prototype.open = function() {
        _super.prototype.open.call(this);
      };
      G48_RulePanel.prototype.close = function() {
        _super.prototype.close.call(this);
        AudioManager_1.AudioManager.instance.playAudioEvent("CloseRule");
      };
      G48_RulePanel.prototype.initMagnification = function() {
        for (var i = 0; i < this.oddsLabelGroup.length; i++) {
          var oddsLabel = this.oddsLabelGroup[i];
          var symbolID = parseInt(oddsLabel.node.name.split("_")[1]);
          var level = parseInt(oddsLabel.node.name.split("_")[2]);
          this.changeMagnification(symbolID, i, level - 1);
        }
      };
      G48_RulePanel.prototype.changeMagnification = function(symbolID, labelIndex, level) {
        void 0 === level && (level = 0);
        var levelCount = 11;
        var keys = Object.keys(this.data.magnificationTable[symbolID]).sort(function(a, b) {
          return Number(a) - Number(b);
        });
        keys = keys.slice(level * levelCount, level * levelCount + levelCount);
        var combotStr = "";
        var scoreStr = "";
        for (var i = 0; i < keys.length; i++) {
          combotStr = combotStr + "" + keys[i];
          scoreStr = scoreStr + "" + this.data.magnificationTable[symbolID][keys[i]];
          if (i < keys.length - 1) {
            combotStr += "\n";
            scoreStr += "\n";
          }
        }
        null != this.oddsTitleLabelGroup[labelIndex] && (this.oddsTitleLabelGroup[labelIndex].string = combotStr);
        null != this.oddsLabelGroup[labelIndex] && (this.oddsLabelGroup[labelIndex].string = scoreStr);
      };
      G48_RulePanel = __decorate([ ccclass ], G48_RulePanel);
      return G48_RulePanel;
    }(Slot_RulePanel_1.default);
    exports.default = G48_RulePanel;
    cc._RF.pop();
  }, {
    "../../../Common/Tools/AudioManager/AudioManager": void 0,
    "../../../SlotFramework/Game/Panel/Slot_RulePanel": void 0,
    "../../Common/G48_DataManager": "G48_DataManager"
  } ],
  G48_SlotReelManager: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "3e397KalGdJaptB2S8pB6iu", "G48_SlotReelManager");
    "use strict";
    var __extends = this && this.__extends || function() {
      var extendStatics = function(d, b) {
        extendStatics = Object.setPrototypeOf || {
          __proto__: []
        } instanceof Array && function(d, b) {
          d.__proto__ = b;
        } || function(d, b) {
          for (var p in b) Object.prototype.hasOwnProperty.call(b, p) && (d[p] = b[p]);
        };
        return extendStatics(d, b);
      };
      return function(d, b) {
        extendStatics(d, b);
        function __() {
          this.constructor = d;
        }
        d.prototype = null === b ? Object.create(b) : (__.prototype = b.prototype, new __());
      };
    }();
    var __decorate = this && this.__decorate || function(decorators, target, key, desc) {
      var c = arguments.length, r = c < 3 ? target : null === desc ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
      if ("object" === typeof Reflect && "function" === typeof Reflect.decorate) r = Reflect.decorate(decorators, target, key, desc); else for (var i = decorators.length - 1; i >= 0; i--) (d = decorators[i]) && (r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r);
      return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    var Slot_ReelManager_1 = require("../../../SlotFramework/Game/view/Slot_ReelManager");
    var G48_Reel_1 = require("./G48_Reel");
    var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
    var STATUS;
    (function(STATUS) {
      STATUS[STATUS["IDLE"] = 0] = "IDLE";
      STATUS[STATUS["SCROLL_ING"] = 1] = "SCROLL_ING";
      STATUS[STATUS["SCROLL_END"] = 2] = "SCROLL_END";
      STATUS[STATUS["RWARD_EFFECTING_ING"] = 3] = "RWARD_EFFECTING_ING";
      STATUS[STATUS["RWARD_EFFECTING_END"] = 4] = "RWARD_EFFECTING_END";
      STATUS[STATUS["SYMBOL_DISAPPEAR_EFFECTING"] = 5] = "SYMBOL_DISAPPEAR_EFFECTING";
      STATUS[STATUS["SYMBOL_DISAPPEAR_DROP_EFFECTING"] = 6] = "SYMBOL_DISAPPEAR_DROP_EFFECTING";
    })(STATUS || (STATUS = {}));
    var G48_SlotReelManager = function(_super) {
      __extends(G48_SlotReelManager, _super);
      function G48_SlotReelManager() {
        var _this = null !== _super && _super.apply(this, arguments) || this;
        _this.tipDisappearString = "\u4ee5\u4e0b\u70baG48\u6d88\u9664\u8a2d\u5b9a";
        _this.reelDisappearDelayTime = .05;
        _this.symbolDisappearDelayTime = .05;
        _this.symbolDisappearFinishDelayTime = -.5;
        _this.reelsList = [];
        _this.reelLevel2 = null;
        _this.reelLevel3 = null;
        return _this;
      }
      G48_SlotReelManager.prototype.update = function(_dt) {
        switch (this.status) {
         case STATUS.SYMBOL_DISAPPEAR_EFFECTING:
          this.symbolDisappearEffecting();
          break;

         case STATUS.SYMBOL_DISAPPEAR_DROP_EFFECTING:
          this.symbolDisappearDropEffecting();
          break;

         default:
          _super.prototype.update.call(this, _dt);
        }
      };
      G48_SlotReelManager.prototype.init = function(p_data) {
        _super.prototype.init.call(this, p_data);
        this.resetLevel1();
      };
      G48_SlotReelManager.prototype.scrollAllReels = function(p_data) {
        G48_Reel_1.default.jackpotCountSound = 1;
        _super.prototype.scrollAllReels.call(this, p_data);
      };
      G48_SlotReelManager.prototype.showSymbolDisappearEffect = function(p_winPosition, p_nextReels, p_nextReelsAbove) {
        for (var i = 0; i < p_winPosition.length; i++) {
          this.reelsList[i] && this.reelsList[i].SetAllSymbolZOrder(0);
          this.reelsList[i] && this.reelsList[i].showSymbolDisappearEffect(p_winPosition[i], p_nextReels[i], p_nextReelsAbove[i], this.reelDisappearDelayTime * i, this.symbolDisappearDelayTime, this.symbolDisappearFinishDelayTime);
        }
        this.status = STATUS.SYMBOL_DISAPPEAR_EFFECTING;
      };
      G48_SlotReelManager.prototype.symbolDisappearEffecting = function() {
        for (var i = 0; i < this.reelsList.length; i++) if (this.reelsList[i].isSymbolDisappearEffecting()) return;
        this.status = STATUS.IDLE;
      };
      G48_SlotReelManager.prototype.isSymbolDisappearEffecting = function() {
        return this.status == STATUS.SYMBOL_DISAPPEAR_EFFECTING;
      };
      G48_SlotReelManager.prototype.ShowSymbolDisappearDropEffect = function(p_winPosition) {
        this.hideMaskEffect();
        for (var i = 0; i < p_winPosition.length; i++) this.reelsList[i] && this.reelsList[i].ShowSymbolDisappearDropEffect(p_winPosition[i]);
        this.status = STATUS.SYMBOL_DISAPPEAR_DROP_EFFECTING;
      };
      G48_SlotReelManager.prototype.symbolDisappearDropEffecting = function() {
        for (var i = 0; i < this.reelsList.length; i++) if (this.reelsList[i].isSymbolDisappearDropEffecting()) return;
        this.correctionSymbolOrder();
        this.allSymbolPlayIdleEffect();
        this.status = STATUS.IDLE;
      };
      G48_SlotReelManager.prototype.isSymbolDisappearDropEffecting = function() {
        return this.status == STATUS.SYMBOL_DISAPPEAR_DROP_EFFECTING;
      };
      G48_SlotReelManager.prototype.correctionSymbolOrder = function() {
        for (var i = 0; i < this.reelsList.length; i++) this.reelsList[i].correctionSymbolOrder();
      };
      G48_SlotReelManager.prototype.showAllSymbolDropOutEffect = function() {
        var _this = this;
        return new Promise(function(resolve) {
          for (var i = 0; i < _this.reelsList.length; i++) _this.reelsList[i].showAllSymbolDropOutEffect();
          _this.schedule(function() {
            return _this.checkAllSymbolDropOutEffect(resolve);
          }, .1);
        });
      };
      G48_SlotReelManager.prototype.checkAllSymbolDropOutEffect = function(resolve) {
        for (var i = 0; i < this.reelsList.length; i++) if (this.reelsList[i].isSymbolDropOutEffecting()) return;
        this.unscheduleAllCallbacks();
        resolve();
      };
      G48_SlotReelManager.prototype.upLevel = function(level) {
        if (level > 1 && this.reelLevel2) {
          this.reelsList.push(this.reelLevel2);
          this.reelLevel2 = null;
          this.setDisplaySymbolCount([ 6, 6, 6, 6, 6 ]);
          this.reelCount++;
          this.reelsList.forEach(function(item) {
            item.addSymbol();
            item.setLevel(level);
          });
          this.reelLevel3 && this.reelLevel3.addSymbol();
        }
        if (level > 2 && this.reelLevel3) {
          this.reelsList.unshift(this.reelLevel3);
          this.reelLevel3 = null;
          this.setDisplaySymbolCount([ 7, 7, 7, 7, 7, 7 ]);
          this.reelCount++;
          this.reelsList.forEach(function(item) {
            item.addSymbol();
            item.setLevel(level);
          });
        }
        this.spacingVH();
      };
      G48_SlotReelManager.prototype.getReelPosition = function(p_Index, p_reelCount, p_reelSpacing) {
        var _addtion = 0;
        p_reelCount < 6 && p_Index++;
        p_reelCount = 6;
        p_reelCount % 2 == 0 && (_addtion = this.reelPrefab.getContentSize().width / 2);
        var _middleIndex = Math.floor(p_reelCount / 2);
        var _times = p_Index - _middleIndex;
        var _spacing = this.reelPrefab.getContentSize().width + p_reelSpacing;
        var _vec2 = new cc.Vec2(_spacing * _times + _addtion, 0);
        return _vec2;
      };
      G48_SlotReelManager.prototype.resetLevel1 = function() {
        this.setDisplaySymbolCount([ 5, 5, 5, 5 ]);
        this.reelsList.forEach(function(item) {
          item.setLevel(1);
          while (item.getSymbolListLen() > 6) item.removeSymbol();
        });
        this.reelLevel2 = this.reelsList.pop();
        this.reelLevel2.hideAllSymbol();
        this.reelLevel3 = this.reelsList.shift();
        this.reelLevel3.hideAllSymbol();
        this.reelCount -= 2;
      };
      __decorate([ property({
        readonly: true,
        displayName: "============ \u3010 \u4ee5\u4e0b\u70baG48\u6d88\u9664\u8a2d\u5b9a \u3011 ============"
      }) ], G48_SlotReelManager.prototype, "tipDisappearString", void 0);
      __decorate([ property({
        displayName: "\u6d88\u9664\u9593\u9694(Reel)",
        type: cc.Float
      }) ], G48_SlotReelManager.prototype, "reelDisappearDelayTime", void 0);
      __decorate([ property({
        displayName: "\u6d88\u9664\u9593\u9694(Symbol)",
        type: cc.Float
      }) ], G48_SlotReelManager.prototype, "symbolDisappearDelayTime", void 0);
      __decorate([ property({
        displayName: "\u6d88\u9664\u5f8c\u518d\u6389\u843d\u9593\u9694\u5fae\u8abf(Symbol)",
        type: cc.Float
      }) ], G48_SlotReelManager.prototype, "symbolDisappearFinishDelayTime", void 0);
      G48_SlotReelManager = __decorate([ ccclass ], G48_SlotReelManager);
      return G48_SlotReelManager;
    }(Slot_ReelManager_1.default);
    exports.default = G48_SlotReelManager;
    cc._RF.pop();
  }, {
    "../../../SlotFramework/Game/view/Slot_ReelManager": void 0,
    "./G48_Reel": "G48_Reel"
  } ],
  G48_SocketConnect: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "3c807yae6lFg5QDIei32Q7t", "G48_SocketConnect");
    "use strict";
    var __extends = this && this.__extends || function() {
      var extendStatics = function(d, b) {
        extendStatics = Object.setPrototypeOf || {
          __proto__: []
        } instanceof Array && function(d, b) {
          d.__proto__ = b;
        } || function(d, b) {
          for (var p in b) Object.prototype.hasOwnProperty.call(b, p) && (d[p] = b[p]);
        };
        return extendStatics(d, b);
      };
      return function(d, b) {
        extendStatics(d, b);
        function __() {
          this.constructor = d;
        }
        d.prototype = null === b ? Object.create(b) : (__.prototype = b.prototype, new __());
      };
    }();
    var __decorate = this && this.__decorate || function(decorators, target, key, desc) {
      var c = arguments.length, r = c < 3 ? target : null === desc ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
      if ("object" === typeof Reflect && "function" === typeof Reflect.decorate) r = Reflect.decorate(decorators, target, key, desc); else for (var i = decorators.length - 1; i >= 0; i--) (d = decorators[i]) && (r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r);
      return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    exports.G48_SocketConnect = void 0;
    var Slot_SocketConnect_1 = require("../../../SlotFramework/Common/Socket/Slot_SocketConnect");
    var G48_DataManager_1 = require("../G48_DataManager");
    var G48_DynamicPopUpPanelManager_1 = require("../G48_DynamicPopUpPanelManager");
    var G48_SocketManager_1 = require("./G48_SocketManager");
    var ccclass = cc._decorator.ccclass;
    var G48_SocketConnect = function(_super) {
      __extends(G48_SocketConnect, _super);
      function G48_SocketConnect() {
        return null !== _super && _super.apply(this, arguments) || this;
      }
      Object.defineProperty(G48_SocketConnect.prototype, "socketManager", {
        get: function() {
          return G48_SocketManager_1.default.getInstance();
        },
        enumerable: false,
        configurable: true
      });
      Object.defineProperty(G48_SocketConnect.prototype, "popupManager", {
        get: function() {
          return G48_DynamicPopUpPanelManager_1.default.getInstance();
        },
        enumerable: false,
        configurable: true
      });
      Object.defineProperty(G48_SocketConnect.prototype, "dataManager", {
        get: function() {
          return G48_DataManager_1.default.getInstance();
        },
        enumerable: false,
        configurable: true
      });
      G48_SocketConnect.prototype.init = function() {
        _super.prototype.init.call(this);
      };
      G48_SocketConnect = __decorate([ ccclass ], G48_SocketConnect);
      return G48_SocketConnect;
    }(Slot_SocketConnect_1.Slot_SocketConnect);
    exports.G48_SocketConnect = G48_SocketConnect;
    cc._RF.pop();
  }, {
    "../../../SlotFramework/Common/Socket/Slot_SocketConnect": void 0,
    "../G48_DataManager": "G48_DataManager",
    "../G48_DynamicPopUpPanelManager": "G48_DynamicPopUpPanelManager",
    "./G48_SocketManager": "G48_SocketManager"
  } ],
  G48_SocketManager: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "5ec55FUEgVKObZyyrJw11Oc", "G48_SocketManager");
    "use strict";
    var __extends = this && this.__extends || function() {
      var extendStatics = function(d, b) {
        extendStatics = Object.setPrototypeOf || {
          __proto__: []
        } instanceof Array && function(d, b) {
          d.__proto__ = b;
        } || function(d, b) {
          for (var p in b) Object.prototype.hasOwnProperty.call(b, p) && (d[p] = b[p]);
        };
        return extendStatics(d, b);
      };
      return function(d, b) {
        extendStatics(d, b);
        function __() {
          this.constructor = d;
        }
        d.prototype = null === b ? Object.create(b) : (__.prototype = b.prototype, new __());
      };
    }();
    var __decorate = this && this.__decorate || function(decorators, target, key, desc) {
      var c = arguments.length, r = c < 3 ? target : null === desc ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
      if ("object" === typeof Reflect && "function" === typeof Reflect.decorate) r = Reflect.decorate(decorators, target, key, desc); else for (var i = decorators.length - 1; i >= 0; i--) (d = decorators[i]) && (r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r);
      return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    var G48_TestSocket_1 = require("./G48_TestSocket");
    var G48_DataManager_1 = require("../G48_DataManager");
    var G48_DynamicPopUpPanelManager_1 = require("../G48_DynamicPopUpPanelManager");
    var G48_SocketConnect_1 = require("./G48_SocketConnect");
    var Slot_SocketManager_1 = require("../../../SlotFramework/Common/Socket/Slot_SocketManager");
    var ccclass = cc._decorator.ccclass;
    var G48_SocketManager = function(_super) {
      __extends(G48_SocketManager, _super);
      function G48_SocketManager() {
        var _this = null !== _super && _super.apply(this, arguments) || this;
        _this.testSocket = null;
        _this.isOffline = false;
        _this.selfEnterEventCallback = null;
        _this.refreshJackpotCallback = null;
        _this.socketConnect = null;
        return _this;
      }
      G48_SocketManager.prototype.init = function() {
        this.socketConnect = new G48_SocketConnect_1.G48_SocketConnect();
        this.socketConnect.init();
        if (G48_DataManager_1.default.offLineMode) {
          this.testSocket = new G48_TestSocket_1.default();
          this.isOffline = true;
        } else {
          console.log("Socket Init");
          this.setErrorAlertCallback(this.showErrorAlert.bind(this));
          this.initGameClient();
          this.client && this.registerEvents();
          this.registerOnSelfEnterEvent();
          this.registerRefreshJackpotNotice();
        }
      };
      G48_SocketManager.prototype.setSelfEnterEventCallback = function(selfEnterCallback) {
        this.selfEnterEventCallback = selfEnterCallback;
      };
      G48_SocketManager.prototype.setRefreshJackpotCallback = function(jackpotCallback) {
        this.refreshJackpotCallback = jackpotCallback;
      };
      G48_SocketManager.prototype.login = function() {
        var _this = this;
        if (G48_DataManager_1.default.offLineMode) {
          console.log("\u55ae\u6a5f\u6a21\u5f0f\u767b\u5165");
          this.testSocket = new G48_TestSocket_1.default();
          this.isOffline = true;
          this.init();
          this.testSocket.login().then(function(loginRes) {
            _this.handleOnOpen(loginRes);
          });
        } else {
          console.log("\u9023\u7dda\u6a21\u5f0f\u767b\u5165");
          _super.prototype.login.call(this);
        }
      };
      G48_SocketManager.prototype.showErrorAlert = function(message, title, confirmCallback) {
        G48_DynamicPopUpPanelManager_1.default.getInstance().confirmSystemMsgDialogBox.show(message, function() {
          confirmCallback && confirmCallback();
        });
      };
      G48_SocketManager.prototype.showErrorYesNoAlert = function(message, title, yesCallback, noCallback) {
        G48_DynamicPopUpPanelManager_1.default.getInstance().ynDialogBox.show(message, function() {
          yesCallback && yesCallback();
        }, function() {
          noCallback && noCallback();
        });
      };
      G48_SocketManager.prototype.setOnGetInitialReelInfoEvent = function(level) {
        if (this.isOffline) return this.sendTestSocket("GetInitialReelInfo", this.testSocket.setOnGetInitialReelInfoEvent.bind(this.testSocket));
        return this.request("GetInitialReelInfo", this.client.setOnGetInitialReelInfoEvent.bind(this.client), {
          level: level
        });
      };
      G48_SocketManager.prototype.setOnGetRoomListEvent = function() {
        return this.request("GetRoomList", this.client.setOnGetInitialReelInfoEvent.bind(this.client), null);
      };
      G48_SocketManager.prototype.setOnChoseRoomEvent = function(roomId) {
        return this.request("ChoseRoom", this.client.setOnChoseRoomEvent.bind(this.client), roomId);
      };
      G48_SocketManager.prototype.setOnFastChoseRoomEvent = function() {
        if (this.isOffline) return this.sendTestSocket("FastChoseRoom", this.testSocket.setOnFastChoseRoomEvent.bind(this.testSocket));
        return this.request("FastChoseRoom", this.client.setOnFastChoseRoomEvent.bind(this.client), null);
      };
      G48_SocketManager.prototype.setOnSpinBaseGameEvent = function(multiple, betNums) {
        if (this.isOffline) return this.sendTestSocket("SpinBaseGame", this.testSocket.setOnSpinBaseGameEvent.bind(this.testSocket), multiple);
        return this.request("SpinBaseGame", this.client.setOnSpinBaseGameEvent.bind(this.client), {
          multiple: multiple,
          betNums: betNums
        });
      };
      G48_SocketManager.prototype.setOnLeaveEvent = function() {
        if (this.isOffline) return this.sendTestSocket("Leave", this.testSocket.setOnLeaveEvent.bind(this.testSocket));
        return this.request("Leave", this.client.setOnLeaveEvent.bind(this.client), null);
      };
      G48_SocketManager.prototype.setOnHistoryEvent = function() {
        if (this.isOffline) return this.sendTestSocket("Leave", this.testSocket.setOnHistoryEvent.bind(this.testSocket));
        return this.request("History", this.client.setOnHistoryEvent.bind(this.client), null);
      };
      G48_SocketManager.prototype.setOnStashEvent = function() {
        if (this.isOffline) return this.sendTestSocket("Leave", this.testSocket.setOnLeaveEvent.bind(this.testSocket));
        return this.request("Stash", this.client.setOnStashEvent.bind(this.client), null);
      };
      G48_SocketManager.prototype.registerOnSelfEnterEvent = function() {
        var _this = this;
        this.client.setSelfEnterEvent(function(data) {
          _this.selfEnterEventCallback(data);
        });
      };
      G48_SocketManager.prototype.registerRefreshJackpotNotice = function() {
        var _this = this;
        this.client.setOnRefreshJackpotNotice(function(data) {
          G48_DataManager_1.default.getInstance().jpData = data;
          _this.refreshJackpotCallback && _this.refreshJackpotCallback(data);
        });
      };
      G48_SocketManager.prototype.sendTestSocket = function(eventName, event, content) {
        var _this = this;
        this.showSendLog(eventName, null);
        return new Promise(function(resolve) {
          event.bind(_this)(content).then(function(data) {
            _this.showResponseLog(eventName, data);
            resolve(data);
          });
        });
      };
      G48_SocketManager = __decorate([ ccclass ], G48_SocketManager);
      return G48_SocketManager;
    }(Slot_SocketManager_1.default);
    exports.default = G48_SocketManager;
    cc._RF.pop();
  }, {
    "../../../SlotFramework/Common/Socket/Slot_SocketManager": void 0,
    "../G48_DataManager": "G48_DataManager",
    "../G48_DynamicPopUpPanelManager": "G48_DynamicPopUpPanelManager",
    "./G48_SocketConnect": "G48_SocketConnect",
    "./G48_TestSocket": "G48_TestSocket"
  } ],
  G48_SymbolTipPanel: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "ed8faHP8/dDtLTeQWbCy02c", "G48_SymbolTipPanel");
    "use strict";
    var __extends = this && this.__extends || function() {
      var extendStatics = function(d, b) {
        extendStatics = Object.setPrototypeOf || {
          __proto__: []
        } instanceof Array && function(d, b) {
          d.__proto__ = b;
        } || function(d, b) {
          for (var p in b) Object.prototype.hasOwnProperty.call(b, p) && (d[p] = b[p]);
        };
        return extendStatics(d, b);
      };
      return function(d, b) {
        extendStatics(d, b);
        function __() {
          this.constructor = d;
        }
        d.prototype = null === b ? Object.create(b) : (__.prototype = b.prototype, new __());
      };
    }();
    var __decorate = this && this.__decorate || function(decorators, target, key, desc) {
      var c = arguments.length, r = c < 3 ? target : null === desc ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
      if ("object" === typeof Reflect && "function" === typeof Reflect.decorate) r = Reflect.decorate(decorators, target, key, desc); else for (var i = decorators.length - 1; i >= 0; i--) (d = decorators[i]) && (r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r);
      return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    var Slot_SymbolTipPanel_1 = require("../../../SlotFramework/Game/Panel/Slot_SymbolTipPanel");
    var ccclass = cc._decorator.ccclass;
    var G48_SymbolTipPanel = function(_super) {
      __extends(G48_SymbolTipPanel, _super);
      function G48_SymbolTipPanel() {
        var _this = null !== _super && _super.apply(this, arguments) || this;
        _this.data = null;
        return _this;
      }
      G48_SymbolTipPanel.prototype.init = function() {};
      G48_SymbolTipPanel = __decorate([ ccclass ], G48_SymbolTipPanel);
      return G48_SymbolTipPanel;
    }(Slot_SymbolTipPanel_1.default);
    exports.default = G48_SymbolTipPanel;
    cc._RF.pop();
  }, {
    "../../../SlotFramework/Game/Panel/Slot_SymbolTipPanel": void 0
  } ],
  G48_Symbol: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "8f41b5zkptA8674BKVanFaA", "G48_Symbol");
    "use strict";
    var __extends = this && this.__extends || function() {
      var extendStatics = function(d, b) {
        extendStatics = Object.setPrototypeOf || {
          __proto__: []
        } instanceof Array && function(d, b) {
          d.__proto__ = b;
        } || function(d, b) {
          for (var p in b) Object.prototype.hasOwnProperty.call(b, p) && (d[p] = b[p]);
        };
        return extendStatics(d, b);
      };
      return function(d, b) {
        extendStatics(d, b);
        function __() {
          this.constructor = d;
        }
        d.prototype = null === b ? Object.create(b) : (__.prototype = b.prototype, new __());
      };
    }();
    var __decorate = this && this.__decorate || function(decorators, target, key, desc) {
      var c = arguments.length, r = c < 3 ? target : null === desc ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
      if ("object" === typeof Reflect && "function" === typeof Reflect.decorate) r = Reflect.decorate(decorators, target, key, desc); else for (var i = decorators.length - 1; i >= 0; i--) (d = decorators[i]) && (r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r);
      return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    var Slot_Symbol_1 = require("../../../SlotFramework/Game/view/Slot_Symbol");
    var Slot_DataManager_1 = require("../../../SlotFramework/Slot_DataManager");
    var AudioManager_1 = require("../../../Common/Tools/AudioManager/AudioManager");
    var FXController_1 = require("../../../Common/Optional/Crosis/FXController/FXController");
    var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
    var EXTRA_STATUS;
    (function(EXTRA_STATUS) {
      EXTRA_STATUS[EXTRA_STATUS["IDLE"] = 0] = "IDLE";
      EXTRA_STATUS[EXTRA_STATUS["DISAPPEAR_EFFECTING"] = 1] = "DISAPPEAR_EFFECTING";
      EXTRA_STATUS[EXTRA_STATUS["DROP_EFFECTING"] = 2] = "DROP_EFFECTING";
    })(EXTRA_STATUS || (EXTRA_STATUS = {}));
    var G48_Symbol = function(_super) {
      __extends(G48_Symbol, _super);
      function G48_Symbol() {
        var _this = null !== _super && _super.apply(this, arguments) || this;
        _this.playWin = null;
        _this.nodeScale = null;
        _this.playWinNode = null;
        _this.extraStatus = EXTRA_STATUS.IDLE;
        _this.symbolName = "";
        return _this;
      }
      G48_Symbol.prototype.setData = function(p_nowIndex, p_symbolValue, p_isFreeGame, level) {
        void 0 === p_isFreeGame && (p_isFreeGame = false);
        void 0 === level && (level = 1);
        p_symbolValue === Slot_DataManager_1.SYMBOL_NAME.Scatter ? this.symbolName = Slot_DataManager_1.SYMBOL_NAME[Slot_DataManager_1.SYMBOL_NAME.Bonus] : p_symbolValue >= Slot_DataManager_1.SYMBOL_NAME.H1 && p_symbolValue <= Slot_DataManager_1.SYMBOL_NAME.H5 ? this.symbolName = Slot_DataManager_1.SYMBOL_NAME[p_symbolValue] + "_" + level : this.symbolName = Slot_DataManager_1.SYMBOL_NAME[p_symbolValue];
        this.nowIndex = p_nowIndex;
        this.symbolID = p_symbolValue;
        this.nodeScale.setScale(1);
        this.nodeScale.opacity = 255;
        this.setSymbolSize();
        this.closeAllEffect();
        this.symbolSpine.setAnimation(0, this.symbolName + "_play_normal", true);
      };
      G48_Symbol.prototype.playRewardOnce = function() {};
      G48_Symbol.prototype.playRewardEffect = function() {
        this.symbolID === Slot_DataManager_1.SYMBOL_NAME.Jackpot && this.symbolSpine.setAnimation(0, this.symbolName + "_play_win", false);
      };
      G48_Symbol.prototype.setScale = function(p_Size) {
        this.node.setScale(p_Size);
      };
      G48_Symbol.prototype.playScrollingEffect = function() {
        this.closeAllEffect();
      };
      G48_Symbol.prototype.playIdleEffect = function() {
        this.closeAllEffect();
        this.symbolSpine.setAnimation(0, this.symbolName + "_play_normal", true);
      };
      G48_Symbol.prototype.closeAllEffect = function() {
        cc.Tween.stopAllByTarget(this.symbolSpine.node);
      };
      G48_Symbol.prototype.playDisappearEffect = function(p_startDelay, p_symbolDisappearFinishDelayTime) {
        var _this = this;
        this.extraStatus = EXTRA_STATUS.DISAPPEAR_EFFECTING;
        var aniName = this.symbolName + "_play_win";
        cc.Tween.stopAllByTarget(this.symbolSpine.node);
        cc.tween(this.symbolSpine.node).delay(p_startDelay).call(function() {
          _this.symbolName === Slot_DataManager_1.SYMBOL_NAME[Slot_DataManager_1.SYMBOL_NAME.Bonus] && AudioManager_1.AudioManager.instance.playAudioEvent("MainGameBonus");
          _this.symbolSpine.setAnimation(0, aniName, false);
          _this.playWinNode.active = true;
          _this.playWin.play();
        }).delay(.5 + p_symbolDisappearFinishDelayTime).call(function() {
          _this.playWinNode.active = false;
          _this.extraStatus = EXTRA_STATUS.IDLE;
          _this.closeAllEffect();
        }).start();
      };
      G48_Symbol.prototype.isDisappearEffecting = function() {
        return this.extraStatus == EXTRA_STATUS.DISAPPEAR_EFFECTING;
      };
      G48_Symbol.prototype.playDropEffect = function(p_startDelay, p_targetPosition) {
        var _this = this;
        this.setZOrder(0);
        this.extraStatus = EXTRA_STATUS.DROP_EFFECTING;
        this.node.opacity = 255;
        this.symbolSpine.node.opacity = 255;
        cc.tween(this.node).delay(p_startDelay).to(.3, {
          position: cc.v3(p_targetPosition)
        }, {
          easing: "sineIn"
        }).by(.05, {
          position: cc.v3(0, 15)
        }, {
          easing: "cubicOut"
        }).delay(.1).by(.05, {
          position: cc.v3(0, -15)
        }, {
          easing: "cubicIn"
        }).call(function() {
          _this.extraStatus = EXTRA_STATUS.IDLE;
          _this.playIdleEffect();
        }).start();
      };
      G48_Symbol.prototype.isDropEffecting = function() {
        return this.extraStatus == EXTRA_STATUS.DROP_EFFECTING;
      };
      G48_Symbol.prototype.skipDropEffect = function() {
        cc.Tween.stopAllByTarget(this.node);
        this.extraStatus = EXTRA_STATUS.IDLE;
        this.playIdleEffect();
      };
      G48_Symbol.prototype.getSymbolSpinePath = function() {
        var _spriteName = this.bundlePath + "/G48_FX/FX_Spine/Symbol";
        return _spriteName;
      };
      __decorate([ property(FXController_1.default) ], G48_Symbol.prototype, "playWin", void 0);
      __decorate([ property(cc.Node) ], G48_Symbol.prototype, "nodeScale", void 0);
      __decorate([ property(cc.Node) ], G48_Symbol.prototype, "playWinNode", void 0);
      G48_Symbol = __decorate([ ccclass ], G48_Symbol);
      return G48_Symbol;
    }(Slot_Symbol_1.default);
    exports.default = G48_Symbol;
    cc._RF.pop();
  }, {
    "../../../Common/Optional/Crosis/FXController/FXController": void 0,
    "../../../Common/Tools/AudioManager/AudioManager": void 0,
    "../../../SlotFramework/Game/view/Slot_Symbol": void 0,
    "../../../SlotFramework/Slot_DataManager": void 0
  } ],
  G48_TestSocket: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "bce34NkM+BHmZY7k+Txsgn9", "G48_TestSocket");
    "use strict";
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    var G48_DataManager_1 = require("../G48_DataManager");
    var G48_CommonData_1 = require("../../Game/G48_CommonData");
    var G48_TestSocket = function() {
      function G48_TestSocket() {
        this.tempSpinCount = 1;
      }
      G48_TestSocket.prototype.login = function() {
        var _this = this;
        return new Promise(function(resolve) {
          var _data = new G48_CommonData_1.LoginData();
          _data.nickname = "\u533f\u540dA";
          _data.language = "zh-cn";
          _data.balance = 1e9;
          _data.timestamp = 1e9;
          _data.icon = 1;
          _data.gameId = 48;
          _this.data = G48_DataManager_1.default.getInstance();
          resolve(_data);
        });
      };
      G48_TestSocket.prototype.setOnFastChoseRoomEvent = function() {
        return new Promise(function(resolve) {
          var _data = new G48_CommonData_1.FastChoseRoomData();
          _data.roomId = "\u6e2c\u8a66Room";
          resolve(_data);
        });
      };
      G48_TestSocket.prototype.setOnGetInitialReelInfoEvent = function() {
        return new Promise(function(resolve) {
          var _data = new G48_CommonData_1.GetInitialReelInfoData();
          _data.position = [ 3, 3, 3, 3, 3, 3 ];
          _data.reels.BG = [ [ [ 4, 2, 3, 1, 2, 5, 2, 2 ], [ 4, 1, 1, 2, 3, 5, 3, 3 ], [ 4, 3, 4, 1, 2, 5, 2, 2 ], [ 4, 2, 3, 4, 1, 5, 1, 1 ], [ 4, 4, 2, 3, 4, 5, 4, 4 ], [ 4, 1, 2, 4, 1, 5, 1, 1 ] ], [ [ 4, 2, 3, 1, 2, 5, 2, 2 ], [ 4, 1, 1, 2, 3, 5, 3, 3 ], [ 4, 3, 4, 1, 2, 5, 2, 2 ], [ 4, 2, 3, 4, 1, 5, 1, 1 ], [ 4, 4, 2, 3, 4, 5, 4, 4 ], [ 4, 1, 2, 4, 1, 5, 1, 1 ] ], [ [ 4, 2, 3, 1, 2, 5, 2, 2 ], [ 4, 1, 1, 2, 3, 5, 3, 3 ], [ 4, 3, 4, 1, 2, 5, 2, 2 ], [ 4, 2, 3, 4, 1, 5, 1, 1 ], [ 4, 4, 2, 3, 4, 5, 4, 4 ], [ 4, 1, 2, 4, 1, 5, 1, 1 ] ] ];
          resolve(_data);
        });
      };
      G48_TestSocket.prototype.setOnSpinBaseGameEvent = function(p_multiple) {
        var _this = this;
        return new Promise(function(resolve) {
          var _data = null;
          p_multiple *= _this.data.oneOddsScore;
          -1 != G48_TestSocket.assignData && (_this.tempSpinCount = G48_TestSocket.assignData);
          switch (_this.tempSpinCount) {
           case 1:
            _data = _this.testSpinData_1(p_multiple);
            break;

           case 2:
            _data = _this.testSpinData_2(p_multiple);
            break;

           case 8:
            _data = _this.testSpinData_8(p_multiple);
            break;

           case 9:
            _data = _this.testSpinData_9(p_multiple);
            break;

           case 10:
            _data = _this.testSpinData_10(p_multiple);
          }
          _this.tempSpinCount++;
          _this.tempSpinCount > 10 && (_this.tempSpinCount = 1);
          resolve(_data);
        });
      };
      G48_TestSocket.prototype.testSpinData_1 = function(p_betScore) {
        var _data = new G48_CommonData_1.BaseGameSpinResponeData();
        _data.resultList[0] = new G48_CommonData_1.ResultData();
        _data.resultList[0].reels_Above = [ 1, 1, 1, 1, 1, 1, 1, 1 ];
        _data.resultList[0].reels = [ [ 4, 2, 3, 1, 2, 5, 2, 2 ], [ 4, 1, 1, 2, 3, 5, 3, 3 ], [ 4, 3, 4, 1, 2, 5, 2, 2 ], [ 4, 2, 3, 4, 1, 5, 1, 1 ], [ 4, 4, 2, 3, 4, 5, 4, 4 ], [ 4, 1, 2, 4, 1, 5, 1, 1 ] ];
        _data.resultList[0].eliminateList[0] = new G48_CommonData_1.EliminateData();
        _data.resultList[0].eliminateList[0].isEliminate = false;
        _data.resultList[0].eliminateList[0].scatter = new G48_CommonData_1.ScatterData();
        _data.resultList[0].eliminateList[0].scatter.count = 1;
        _data.resultList[0].eliminateList[0].scatter.scatterPos = [ [ 0, 0, 0, 0, 0, 0, 0, 0 ], [ 0, 0, 0, 0, 0, 0, 0, 0 ], [ 0, 0, 0, 0, 0, 0, 0, 0 ], [ 0, 0, 0, 0, 0, 0, 0, 0 ], [ 0, 0, 0, 0, 0, 0, 0, 0 ], [ 0, 0, 0, 0, 0, 0, 0, 0 ] ];
        return this.processResultData(_data, p_betScore);
      };
      G48_TestSocket.prototype.testSpinData_2 = function(p_betScore) {
        var _data = new G48_CommonData_1.BaseGameSpinResponeData();
        _data.resultList[0] = new G48_CommonData_1.ResultData();
        _data.resultList[0].reels_Above = [ 1, 1, 1, 1, 1, 1, 1, 1 ];
        _data.resultList[0].reels = [ [ 4, 2, 3, 1, 2, 5, 2, 2 ], [ 4, 1, 1, 2, 3, 5, 3, 3 ], [ 4, 3, 4, 1, 2, 5, 2, 2 ], [ 4, 2, 3, 4, 1, 5, 1, 1 ], [ 4, 4, 2, 3, 4, 5, 4, 4 ], [ 4, 1, 2, 4, 1, 5, 1, 1 ] ];
        _data.resultList[0].eliminateList[0] = new G48_CommonData_1.EliminateData();
        _data.resultList[0].eliminateList[0].isEliminate = true;
        _data.resultList[0].eliminateList[0].eliAllPos = [ [ 0, 0, 0, 0, 0, 0, 0, 0 ], [ 0, 0, 0, 0, 0, 0, 0, 0 ], [ 0, 0, 0, 0, 0, 0, 0, 0 ], [ 0, 0, 0, 0, 0, 0, 0, 0 ] ];
        _data.resultList[0].eliminateList[0].eliHitPos = [ [ 0, 0, 0, 0, 0, 0, 0, 0 ], [ 0, 0, 0, 0, 0, 0, 0, 0 ], [ 0, 0, 0, 0, 0, 0, 0, 0 ], [ 0, 0, 0, 0, 0, 0, 0, 0 ], [ 0, 0, 0, 0, 0, 0, 0, 0 ], [ 0, 0, 0, 0, 0, 0, 0, 0 ] ];
        _data.resultList[0].eliminateList[0].nextReels_Above = [ 1, 1, 1, 1, 1, 1, 1, 1 ];
        _data.resultList[0].eliminateList[0].nextReels = [ [ 4, 2, 3, 1, 2, 5, 2, 2 ], [ 4, 1, 1, 2, 3, 5, 3, 3 ], [ 4, 3, 4, 1, 2, 5, 2, 2 ], [ 4, 2, 3, 4, 1, 5, 1, 1 ], [ 4, 4, 2, 3, 4, 5, 4, 4 ], [ 4, 1, 2, 4, 1, 5, 1, 1 ] ];
        _data.resultList[0].eliminateList[0].scatter = new G48_CommonData_1.ScatterData();
        _data.resultList[0].eliminateList[0].scatter.count = 15;
        _data.resultList[0].eliminateList[0].scatter.scatterPos = [ [ 0, 0, 0, 0, 0, 0, 0, 0 ], [ 0, 0, 0, 0, 0, 0, 0, 0 ], [ 0, 0, 0, 0, 0, 0, 0, 0 ], [ 0, 0, 0, 0, 0, 0, 0, 0 ], [ 0, 0, 0, 0, 0, 0, 0, 0 ], [ 0, 0, 0, 0, 0, 0, 0, 0 ] ];
        _data.resultList[0].eliminateList[1] = new G48_CommonData_1.EliminateData();
        _data.resultList[0].eliminateList[1].isEliminate = false;
        return this.processResultData(_data, p_betScore);
      };
      G48_TestSocket.prototype.testSpinData_8 = function(p_betScore) {
        var _data = new G48_CommonData_1.BaseGameSpinResponeData();
        _data.resultList[0] = new G48_CommonData_1.ResultData();
        _data.resultList[0].reels_Above = [ 1, 1, 1, 1, 1, 1, 1, 1 ];
        _data.resultList[0].reels = [ [ 4, 2, 3, 1, 2, 5, 2, 2 ], [ 4, 1, 1, 2, 3, 5, 3, 3 ], [ 4, 3, 4, 1, 2, 5, 2, 2 ], [ 4, 2, 3, 4, 1, 5, 1, 1 ], [ 4, 4, 2, 3, 4, 5, 4, 4 ], [ 4, 1, 2, 4, 1, 5, 1, 1 ] ];
        _data.resultList[0].eliminateList[0] = new G48_CommonData_1.EliminateData();
        _data.resultList[0].eliminateList[0].eliWinBonus = 30 * p_betScore;
        _data.resultList[0].eliminateList[1] = new G48_CommonData_1.EliminateData();
        _data.resultList[0].eliminateList[1].isEliminate = false;
        return this.processResultData(_data, p_betScore);
      };
      G48_TestSocket.prototype.testSpinData_9 = function(p_betScore) {
        var _data = new G48_CommonData_1.BaseGameSpinResponeData();
        _data.resultList[0] = new G48_CommonData_1.ResultData();
        _data.resultList[0].reels_Above = [ 1, 1, 1, 1, 1, 1, 1, 1 ];
        _data.resultList[0].reels = [ [ 4, 2, 3, 1, 2, 5, 2, 2 ], [ 4, 1, 1, 2, 3, 5, 3, 3 ], [ 4, 3, 4, 1, 2, 5, 2, 2 ], [ 4, 2, 3, 4, 1, 5, 1, 1 ], [ 4, 4, 2, 3, 4, 5, 4, 4 ], [ 4, 1, 2, 4, 1, 5, 1, 1 ] ];
        _data.resultList[0].eliminateList[0] = new G48_CommonData_1.EliminateData();
        _data.resultList[0].eliminateList[0].eliWinBonus = 60 * p_betScore;
        _data.resultList[0].eliminateList[1] = new G48_CommonData_1.EliminateData();
        _data.resultList[0].eliminateList[1].isEliminate = false;
        return this.processResultData(_data, p_betScore);
      };
      G48_TestSocket.prototype.testSpinData_10 = function(p_betScore) {
        var _data = new G48_CommonData_1.BaseGameSpinResponeData();
        _data.resultList[0] = new G48_CommonData_1.ResultData();
        _data.resultList[0].reels_Above = [ 1, 1, 1, 1, 1, 1, 1, 1 ];
        _data.resultList[0].reels = [ [ 4, 2, 3, 1, 2, 5, 2, 2 ], [ 4, 1, 1, 2, 3, 5, 3, 3 ], [ 4, 3, 4, 1, 2, 5, 2, 2 ], [ 4, 2, 3, 4, 1, 5, 1, 1 ], [ 4, 4, 2, 3, 4, 5, 4, 4 ], [ 4, 1, 2, 4, 1, 5, 1, 1 ] ];
        _data.resultList[0].eliminateList[0] = new G48_CommonData_1.EliminateData();
        _data.resultList[0].eliminateList[0].eliWinBonus = 200 * p_betScore;
        _data.resultList[0].eliminateList[1] = new G48_CommonData_1.EliminateData();
        _data.resultList[0].eliminateList[1].isEliminate = false;
        return this.processResultData(_data, p_betScore);
      };
      G48_TestSocket.prototype.processResultData = function(_data, p_betScore) {
        _data.totalBet = p_betScore;
        _data.resultList.forEach(function(result) {
          result.eliminateList.forEach(function(eliminate) {
            result.totalWinBonus += eliminate.eliWinBonus;
          });
          _data.totalBonus += result.totalWinBonus;
        });
        _data.currentCash = 1e4 * this.data.coin - p_betScore + _data.totalBonus;
        return _data;
      };
      G48_TestSocket.prototype.setOnLeaveEvent = function() {
        return new Promise(function(resolve) {
          resolve();
        });
      };
      G48_TestSocket.prototype.setOnHistoryEvent = function() {
        return new Promise(function(resolve) {
          var _data = new G48_CommonData_1.HistoryData();
          _data.level = 3;
          _data.scatterNum = 0;
          resolve(_data);
        });
      };
      G48_TestSocket.assignData = -1;
      return G48_TestSocket;
    }();
    exports.default = G48_TestSocket;
    cc._RF.pop();
  }, {
    "../../Game/G48_CommonData": "G48_CommonData",
    "../G48_DataManager": "G48_DataManager"
  } ],
  G48_TestUI: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "c36a5dnZitFhICl75FkFJtF", "G48_TestUI");
    "use strict";
    var __extends = this && this.__extends || function() {
      var extendStatics = function(d, b) {
        extendStatics = Object.setPrototypeOf || {
          __proto__: []
        } instanceof Array && function(d, b) {
          d.__proto__ = b;
        } || function(d, b) {
          for (var p in b) Object.prototype.hasOwnProperty.call(b, p) && (d[p] = b[p]);
        };
        return extendStatics(d, b);
      };
      return function(d, b) {
        extendStatics(d, b);
        function __() {
          this.constructor = d;
        }
        d.prototype = null === b ? Object.create(b) : (__.prototype = b.prototype, new __());
      };
    }();
    var __decorate = this && this.__decorate || function(decorators, target, key, desc) {
      var c = arguments.length, r = c < 3 ? target : null === desc ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
      if ("object" === typeof Reflect && "function" === typeof Reflect.decorate) r = Reflect.decorate(decorators, target, key, desc); else for (var i = decorators.length - 1; i >= 0; i--) (d = decorators[i]) && (r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r);
      return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    var DesktopBrowserTransform_1 = require("../../Common/Tools/DesktopBrowserTransform/DesktopBrowserTransform");
    var G48_DataManager_1 = require("../Common/G48_DataManager");
    var G48_TestSocket_1 = require("../Common/Socket/G48_TestSocket");
    var G48_SlotReelManager_1 = require("./View/G48_SlotReelManager");
    var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
    var G48_TestUI = function(_super) {
      __extends(G48_TestUI, _super);
      function G48_TestUI() {
        var _this = null !== _super && _super.apply(this, arguments) || this;
        _this.menuNode = null;
        _this.gameSpeedEditBox = null;
        _this.pageList = null;
        _this.reelMgrValueList = [ "dropOutTime", "dropOutEasing", "dropOutDelayTime", "dropOutStartIntervalTime", "dropInTime", "dropInEasing", "dropInDelayTime", "dropInStartIntervalTime", "dropInReboundHeight", "dropInReboundTime", "dropInReboundEasingOut", "dropInReboundEasingIn", "dropInReboundAngle", "dropInReboundAngleTime" ];
        _this.valueUpdateHis = [ "===== Update History =====" ];
        _this.updateIdx = 0;
        return _this;
      }
      G48_TestUI.prototype.start = function() {
        if (G48_DataManager_1.default.offLineMode) {
          this.node.setPosition(cc.Vec2.ZERO);
          this.menuNode.active = false;
        } else this.node.active = false;
      };
      G48_TestUI.prototype.setTestData = function(event, customEventData) {
        console.log("\u4f7f\u7528\u6e2c\u8a66\u8cc7\u6599 " + customEventData);
        G48_TestSocket_1.default.assignData = Number(customEventData);
        this.closeMenu();
      };
      G48_TestUI.prototype.setGameSpeed = function() {
        DesktopBrowserTransform_1.default.getInstance().setGameSpeed(Number(this.gameSpeedEditBox.string));
      };
      G48_TestUI.prototype.showMenu = function() {
        this.menuNode.active = true;
      };
      G48_TestUI.prototype.closeMenu = function() {
        console.log("\u95dc\u9589\u6e2c\u8a66UI");
        this.menuNode.active = false;
      };
      G48_TestUI.prototype.onToggleClick = function(p_toggle) {
        var _index = Number(p_toggle.node.name) - 1;
        this.pageList.children.forEach(function(node, index) {
          node.active = _index == index;
          node.position = cc.Vec3.ZERO;
        });
        1 == _index && this.syncSlotReelManagerValue();
      };
      G48_TestUI.prototype.syncSlotReelManagerValue = function() {
        var _this = this;
        var _reelMgr = cc.Canvas.instance.node.getComponentInChildren(G48_SlotReelManager_1.default);
        this.reelMgrValueList.forEach(function(value) {
          var editBoxList = _this.node.getComponentsInChildren(cc.EditBox);
          editBoxList.forEach(function(editBox) {
            editBox.node.name === value && 0 == editBox.string.length && (null != _reelMgr[value] ? editBox.string = "" + _reelMgr[value] : console.warn("updateReelPage " + value + " \u66f4\u65b0\u5931\u6557"));
          });
        });
        cc.sys.isMobile || (this.pageList.children[1].getComponentInChildren(cc.WebView).node.active = true);
      };
      G48_TestUI.prototype.updateSlotReelManagerValue = function() {
        var _this = this;
        var _reelMgr = cc.Canvas.instance.node.getComponentInChildren(G48_SlotReelManager_1.default);
        this.valueUpdateHis.push("----- \u66f4\u65b0 " + ++this.updateIdx + " -----");
        var _nowUpdate = [ "----- \u66f4\u65b0 " + this.updateIdx + " -----" ];
        this.reelMgrValueList.forEach(function(valueName) {
          var editBoxList = _this.node.getComponentsInChildren(cc.EditBox);
          editBoxList.forEach(function(editBox) {
            if (editBox.node.name === valueName && editBox.string.length > 0) {
              var valueLabelZh = editBox.node.parent.getComponent(cc.Label).string;
              if (_reelMgr[valueName] && "number" == typeof _reelMgr[valueName]) {
                var _updateValue = Number(editBox.string);
                if (!Number.isNaN(_updateValue) && _updateValue != _reelMgr[valueName]) {
                  _this.valueUpdateHis.push("    " + valueLabelZh + " from " + _reelMgr[valueName] + " to " + _updateValue);
                  _nowUpdate.push("    " + valueLabelZh + " from " + _reelMgr[valueName] + " to " + _updateValue);
                  _reelMgr[valueName] = _updateValue;
                }
              } else {
                var _updateValue = editBox.string;
                if (_updateValue != _reelMgr[valueName]) {
                  _this.valueUpdateHis.push("    " + valueLabelZh + " from " + _reelMgr[valueName] + " to " + _updateValue);
                  _nowUpdate.push("    " + valueLabelZh + " from " + _reelMgr[valueName] + " to " + _updateValue);
                  _reelMgr[valueName] = _updateValue;
                }
              }
            }
          });
        });
        console.warn(JSON.stringify(_nowUpdate, null, 2));
        this.closeMenu();
      };
      G48_TestUI.prototype.showUpdateHisroty = function() {
        var _this = this;
        var _reelMgr = cc.Canvas.instance.node.getComponentInChildren(G48_SlotReelManager_1.default);
        var _nowValue = [ "", "===== Now Value =====" ];
        this.reelMgrValueList.forEach(function(valueName) {
          var editBoxList = _this.node.getComponentsInChildren(cc.EditBox);
          editBoxList.forEach(function(editBox) {
            if (editBox.node.name === valueName && editBox.string.length > 0) {
              var valueLabelZh = editBox.node.parent.getComponent(cc.Label).string;
              _nowValue.push("    " + valueLabelZh + " = " + _reelMgr[valueName]);
            }
          });
        });
        console.warn(JSON.stringify(this.valueUpdateHis.concat(_nowValue), null, 2));
      };
      G48_TestUI.prototype.resetTip = function(_editBox) {
        _editBox.string = _editBox.placeholder;
      };
      __decorate([ property(cc.Node) ], G48_TestUI.prototype, "menuNode", void 0);
      __decorate([ property(cc.EditBox) ], G48_TestUI.prototype, "gameSpeedEditBox", void 0);
      __decorate([ property(cc.Node) ], G48_TestUI.prototype, "pageList", void 0);
      G48_TestUI = __decorate([ ccclass ], G48_TestUI);
      return G48_TestUI;
    }(cc.Component);
    exports.default = G48_TestUI;
    cc._RF.pop();
  }, {
    "../../Common/Tools/DesktopBrowserTransform/DesktopBrowserTransform": void 0,
    "../Common/G48_DataManager": "G48_DataManager",
    "../Common/Socket/G48_TestSocket": "G48_TestSocket",
    "./View/G48_SlotReelManager": "G48_SlotReelManager"
  } ],
  G48_WinScorePanel: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "4afed+jloVN1oi1+komXu/B", "G48_WinScorePanel");
    "use strict";
    var __extends = this && this.__extends || function() {
      var extendStatics = function(d, b) {
        extendStatics = Object.setPrototypeOf || {
          __proto__: []
        } instanceof Array && function(d, b) {
          d.__proto__ = b;
        } || function(d, b) {
          for (var p in b) Object.prototype.hasOwnProperty.call(b, p) && (d[p] = b[p]);
        };
        return extendStatics(d, b);
      };
      return function(d, b) {
        extendStatics(d, b);
        function __() {
          this.constructor = d;
        }
        d.prototype = null === b ? Object.create(b) : (__.prototype = b.prototype, new __());
      };
    }();
    var __decorate = this && this.__decorate || function(decorators, target, key, desc) {
      var c = arguments.length, r = c < 3 ? target : null === desc ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
      if ("object" === typeof Reflect && "function" === typeof Reflect.decorate) r = Reflect.decorate(decorators, target, key, desc); else for (var i = decorators.length - 1; i >= 0; i--) (d = decorators[i]) && (r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r);
      return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    var Slot_WinScorePanel_1 = require("../../../SlotFramework/Game/Panel/Slot_WinScorePanel");
    var AudioManager_1 = require("../../../Common/Tools/AudioManager/AudioManager");
    var FXController_1 = require("../../../Common/Optional/Crosis/FXController/FXController");
    var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
    var G48_WinScorePanel = function(_super) {
      __extends(G48_WinScorePanel, _super);
      function G48_WinScorePanel() {
        var _this = null !== _super && _super.apply(this, arguments) || this;
        _this.fxBigWin = null;
        _this.fxSuperWin = null;
        _this.fxMegaWin = null;
        return _this;
      }
      G48_WinScorePanel.prototype.open = function(p_totalWinScore, p_betScore, p_isFreeGame) {
        void 0 === p_isFreeGame && (p_isFreeGame = false);
        this.winScore.node.scale = 0;
        _super.prototype.open.call(this, p_totalWinScore, p_betScore, p_isFreeGame);
        cc.tween(this.winScore.node).to(.3, {
          scale: 1
        }, {
          easing: cc.easing.cubicInOut
        }).start();
      };
      G48_WinScorePanel.prototype.closeAllWinEffect = function() {
        this.fxBigWin.getComponent(FXController_1.default).stop();
        this.fxSuperWin.getComponent(FXController_1.default).stop();
        this.fxMegaWin.getComponent(FXController_1.default).stop();
        this.fxBigWin.active = false;
        this.fxSuperWin.active = false;
        this.fxMegaWin.active = false;
      };
      G48_WinScorePanel.prototype.showBigWinEffect = function() {
        console.log("\u3010Show Big Win\u3011 ");
        AudioManager_1.AudioManager.instance.playAudioEvent("WinScorePanelBigWinEffect");
        _super.prototype.showBigWinEffect.call(this);
        this.closeAllWinEffect();
        this.fxBigWin.getComponent(FXController_1.default).play();
      };
      G48_WinScorePanel.prototype.showSuperWinEffect = function() {
        console.log("\u3010Show Super Win\u3011 ");
        AudioManager_1.AudioManager.instance.playAudioEvent("WinScorePanelSuperWinEffect");
        _super.prototype.showSuperWinEffect.call(this);
        this.closeAllWinEffect();
        this.fxSuperWin.getComponent(FXController_1.default).play();
      };
      G48_WinScorePanel.prototype.showMegaWinEffect = function() {
        console.log("\u3010Mega Win\u3011");
        AudioManager_1.AudioManager.instance.playAudioEvent("WinScorePanelMegaWinEffect");
        _super.prototype.showMegaWinEffect.call(this);
        this.closeAllWinEffect();
        this.fxMegaWin.getComponent(FXController_1.default).play();
      };
      G48_WinScorePanel.prototype.runScoreEnd = function() {
        _super.prototype.runScoreEnd.call(this);
        cc.tween(this.winScore.node).by(.15, {
          y: 20
        }).by(.15, {
          y: -20
        }).start();
      };
      __decorate([ property(cc.Node) ], G48_WinScorePanel.prototype, "fxBigWin", void 0);
      __decorate([ property(cc.Node) ], G48_WinScorePanel.prototype, "fxSuperWin", void 0);
      __decorate([ property(cc.Node) ], G48_WinScorePanel.prototype, "fxMegaWin", void 0);
      G48_WinScorePanel = __decorate([ ccclass ], G48_WinScorePanel);
      return G48_WinScorePanel;
    }(Slot_WinScorePanel_1.default);
    exports.default = G48_WinScorePanel;
    cc._RF.pop();
  }, {
    "../../../Common/Optional/Crosis/FXController/FXController": void 0,
    "../../../Common/Tools/AudioManager/AudioManager": void 0,
    "../../../SlotFramework/Game/Panel/Slot_WinScorePanel": void 0
  } ]
}, {}, [ "G48_DataManager", "G48_DynamicPopUpPanelManager", "G48_LoadingItem", "G48_SocketConnect", "G48_SocketManager", "G48_TestSocket", "G48_CommonData", "G48_Game", "G48_TestUI", "G48_AutoGamePanel", "G48_FreeGameGetScorePanel", "G48_FreeGamePanel", "G48_GameUI", "G48_JackpotPanel", "G48_MusicOptionPanel", "G48_Reel", "G48_RulePanel", "G48_SlotReelManager", "G48_Symbol", "G48_SymbolTipPanel", "G48_WinScorePanel", "G48_Loading_InitData", "G48_Loading", "G48_LoadingUI", "G48_Lobby", "G48_LobbyUI", "G48_Language" ]);