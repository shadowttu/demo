window._CCSettings = {
    platform: "web-mobile",
    groupList: [
        "default",
        "2D",
        "3D",
        "3D_MahjongTile",
        "Fish",
        "Bullet"
    ],
    collisionMatrix: [
        [
            false
        ],
        [
            false,
            false,
            false
        ],
        [
            false,
            false,
            false
        ],
        [
            false,
            false,
            false,
            false
        ],
        [
            false,
            false,
            false,
            false,
            false,
            true
        ],
        [
            false,
            false,
            false,
            false,
            true,
            false
        ]
    ],
    hasResourcesBundle: false,
    hasStartSceneBundle: false,
    remoteBundles: [],
    subpackages: [],
    launchScene: "db://assets/KKGame/resources/scene/KKGame.fire",
    orientation: "landscape",
    debug: true,
    jsList: [],
    bundleVers: {
        internal: "cb4f5",
        main: "ca90b"
    }
};
